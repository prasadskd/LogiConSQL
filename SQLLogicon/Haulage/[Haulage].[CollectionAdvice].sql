
-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CollectionAdvice] Record based on [CollectionAdvice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_CollectionAdviceSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceSelect] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceSelect] 
    @BranchID SMALLINT,
    @ContainerKey NVARCHAR(50),
    @MovementCode NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [ContainerKey], [MovementCode], [JobNo], [OrderNo], [CallingDate], [RequestDate], 
			[Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [ConfirmBy], [ConfirmedOn] 
	FROM	[Haulage].[CollectionAdvice]
	WHERE  [BranchID] = @BranchID  
	       AND [ContainerKey] = @ContainerKey  
	       AND [MovementCode] = @MovementCode  
END
-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CollectionAdvice] Records from [CollectionAdvice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_CollectionAdviceList]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceList] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceList] 
    @BranchID SMALLINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT	[BranchID], [ContainerKey], [MovementCode], [JobNo], [OrderNo], [CallingDate], [RequestDate], 
			[Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [ConfirmBy], [ConfirmedOn] 
	FROM	[Haulage].[CollectionAdvice]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo


END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdvicePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CollectionAdvice] Records from [CollectionAdvice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_CollectionAdvicePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdvicePageView] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdvicePageView] 
    @BranchID SMALLINT,
    @OrderNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [ContainerKey], [MovementCode], [JobNo], [OrderNo], [CallingDate], [RequestDate], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [ConfirmBy], [ConfirmedOn] 
	FROM   [Haulage].[CollectionAdvice]
		WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo
	ORDER BY  [ContainerKey] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdvicePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CollectionAdvice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_CollectionAdviceRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceRecordCount] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceRecordCount] 
    @BranchID SMALLINT,
    @OrderNo NVARCHAR(50)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Haulage].[CollectionAdvice]
		WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo

	

END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CollectionAdvice] Record based on [CollectionAdvice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_CollectionAdviceAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceAutoCompleteSearch] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo NVARCHAR(50)

AS 

BEGIN

	SELECT [BranchID], [ContainerKey], [MovementCode], [JobNo], [OrderNo], [CallingDate], [RequestDate], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [ConfirmBy], [ConfirmedOn] 
	FROM   [Haulage].[CollectionAdvice]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo
END
-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CollectionAdvice] Record Into [CollectionAdvice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_CollectionAdviceInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceInsert] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceInsert] 
    @BranchID smallint,
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @JobNo nvarchar(50),
    @OrderNo nvarchar(50),
    @CallingDate datetime,
    @RequestDate datetime,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Haulage].[CollectionAdvice] (
			[BranchID], [ContainerKey], [MovementCode], [JobNo], [OrderNo], [CallingDate], [RequestDate], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ContainerKey, @MovementCode, @JobNo, @OrderNo, @CallingDate, @RequestDate, @Remarks, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CollectionAdvice] Record Into [CollectionAdvice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_CollectionAdviceUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceUpdate] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceUpdate] 
    @BranchID smallint,
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @JobNo nvarchar(50),
    @OrderNo nvarchar(50),
    @CallingDate datetime,
    @RequestDate datetime,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Haulage].[CollectionAdvice]
	SET		[JobNo] = @JobNo, [OrderNo] = @OrderNo, [CallingDate] = @CallingDate, [RequestDate] = @RequestDate, [Remarks] = @Remarks, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [ContainerKey] = @ContainerKey
			AND [MovementCode] = @MovementCode
	
END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CollectionAdvice] Record Into [CollectionAdvice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_CollectionAdviceSave]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceSave] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceSave] 
    @BranchID smallint,
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @JobNo nvarchar(50),
    @OrderNo nvarchar(50),
    @CallingDate datetime,
    @RequestDate datetime,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Haulage].[CollectionAdvice] 
		WHERE 	[BranchID] = @BranchID
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode)>0
	BEGIN
	    Exec [Haulage].[usp_CollectionAdviceUpdate] 
		@BranchID, @ContainerKey, @MovementCode, @JobNo, @OrderNo, @CallingDate, @RequestDate, @Remarks, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Haulage].[usp_CollectionAdviceInsert] 
		@BranchID, @ContainerKey, @MovementCode, @JobNo, @OrderNo, @CallingDate, @RequestDate, @Remarks, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Haulage].usp_[CollectionAdviceSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Haulage].[usp_CollectionAdviceDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CollectionAdvice] Record  based on [CollectionAdvice]

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_CollectionAdviceDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_CollectionAdviceDelete] 
END 
GO
CREATE PROC [Haulage].[usp_CollectionAdviceDelete] 
    @BranchID smallint,
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50)
AS 

	
BEGIN

 
	DELETE
	FROM   [Haulage].[CollectionAdvice]
	WHERE  [BranchID] = @BranchID
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode
	 


END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_CollectionAdviceDelete]
-- ========================================================================================================================================

GO
 