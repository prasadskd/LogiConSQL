
-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_OrderHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderSelect] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderSelect] 
    @BranchID SMALLINT,
    @JobOrderNo NVARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Haulage].[OrderHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [JobOrderNo] = @JobOrderNo  
END
-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderHeader] Records from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_OrderHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderList] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderList] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT	[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], 
			[IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Haulage].[OrderHeader]
	WHERE	[BranchID] = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderHeader] Records from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_OrderHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderPageView] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderPageView] 
	@BranchID SMALLINT,
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Haulage].[OrderHeader]
	WHERE  [BranchID] = @BranchID
	ORDER BY [JobOrderNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_OrderHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderRecordCount] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderRecordCount] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Haulage].[OrderHeader]
	WHERE  [BranchID] = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Haulage].[usp_OrderHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderAutoCompleteSearch] 
    @BranchID SMALLINT,
    @JobOrderNo NVARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Haulage].[OrderHeader]
	WHERE  [BranchID] = @BranchID 
	       AND [JobOrderNo] LIKE '%' +  @JobOrderNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderInsert] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderInsert] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Haulage].[OrderHeader] (
			[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderUpdate] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderUpdate] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE [Haulage].[OrderHeader]
	SET    [OrderNo] = @OrderNo, [Remarks] = @Remarks,  [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
	
END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderSave] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderSave] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Haulage].[OrderHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo)>0
	BEGIN
	    Exec [Haulage].[usp_OrderHeaderUpdate] 
		@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Haulage].[usp_OrderHeaderInsert] 
		@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, @ModifiedBy


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Haulage].usp_[OrderHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderDelete] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderDelete] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
	@ClosedBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Haulage].[OrderHeader]
	SET	[IsClosed] = CAST(1 as bit) , ClosedBy = @ClosedBy, ClosedOn=GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo

	 


END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderClose]
-- ========================================================================================================================================

GO
 

-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderCancel]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderCancel]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderCancel] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderCancel] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
	@CancelledBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Haulage].[OrderHeader]
	SET	IsCancel = CAST(1 as bit), CancelledBy=@CancelledBy, CancelledOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo

	 


END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderCancel]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Haulage].[usp_OrderHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Haulage].[usp_OrderHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Haulage].[usp_OrderHeaderDelete] 
END 
GO
CREATE PROC [Haulage].[usp_OrderHeaderDelete] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Haulage].[OrderHeader]
	WHERE  [BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
	 


END

-- ========================================================================================================================================
-- END  											 [Haulage].[usp_OrderHeaderDelete]
-- ========================================================================================================================================

GO
  