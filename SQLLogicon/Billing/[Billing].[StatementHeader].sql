
-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementHeader] Record based on [StatementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderSelect] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderSelect] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50)
AS 

BEGIN

	
	SELECT	Hd.[BranchID], Hd.[StatementNo], Hd.[StatementDate], Hd.[AccountNo], Hd.[AccountBranchID], Hd.[CreditTerm], Hd.[CurrencyCode], Hd.[StatementAmount], 
			Hd.[GSTCode], Hd.[GSTRate], Hd.[GSTAmount], Hd.[TotalAmount], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn], Hd.[IsApproved], 
			Hd.[ApprovedBy], Hd.[ApprovedOn], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifedOn] ,
			A.CompanyName As AccountName
	FROM	[Billing].[StatementHeader] Hd
	Left Outer Join Master.Company A On 
		Hd.AccountNo = A.CompanyCode
	WHERE  Hd.[BranchID] = @BranchID  
	       AND Hd.[StatementNo] = @StatementNo  
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementHeader] Records from [StatementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderList] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderList] 
    @BranchID BIGINT

AS 
BEGIN

	SELECT	Hd.[BranchID], Hd.[StatementNo], Hd.[StatementDate], Hd.[AccountNo], Hd.[AccountBranchID], Hd.[CreditTerm], Hd.[CurrencyCode], Hd.[StatementAmount], 
			Hd.[GSTCode], Hd.[GSTRate], Hd.[GSTAmount], Hd.[TotalAmount], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn], Hd.[IsApproved], 
			Hd.[ApprovedBy], Hd.[ApprovedOn], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifedOn] ,
			A.CompanyName As AccountName
	FROM	[Billing].[StatementHeader] Hd
	Left Outer Join Master.Company A On 
		Hd.AccountNo = A.CompanyCode
	WHERE  Hd.[BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementHeader] Records from [StatementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderPageView] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderPageView] 
    @BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	SELECT	Hd.[BranchID], Hd.[StatementNo], Hd.[StatementDate], Hd.[AccountNo], Hd.[AccountBranchID], Hd.[CreditTerm], Hd.[CurrencyCode], Hd.[StatementAmount], 
			Hd.[GSTCode], Hd.[GSTRate], Hd.[GSTAmount], Hd.[TotalAmount], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn], Hd.[IsApproved], 
			Hd.[ApprovedBy], Hd.[ApprovedOn], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifedOn] ,
			A.CompanyName As AccountName
	FROM	[Billing].[StatementHeader] Hd
	Left Outer Join Master.Company A On 
		Hd.AccountNo = A.CompanyCode
	WHERE  Hd.[BranchID] = @BranchID  
	ORDER BY  [StatementNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [StatementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderRecordCount] 
    @BranchID BIGINT

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[StatementHeader]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementHeader] Record based on [StatementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderAutoCompleteSearch] 
    @BranchID BIGINT,
    @AccountName NVARCHAR(100)
AS 

BEGIN

	SELECT	Hd.[BranchID], Hd.[StatementNo], Hd.[StatementDate], Hd.[AccountNo], Hd.[AccountBranchID], Hd.[CreditTerm], Hd.[CurrencyCode], Hd.[StatementAmount], 
			Hd.[GSTCode], Hd.[GSTRate], Hd.[GSTAmount], Hd.[TotalAmount], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn], Hd.[IsApproved], 
			Hd.[ApprovedBy], Hd.[ApprovedOn], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifedOn] ,
			A.CompanyName As AccountName
	FROM	[Billing].[StatementHeader] Hd
	Left Outer Join Master.Company A On 
		Hd.AccountNo = A.CompanyCode
	WHERE  Hd.[BranchID] = @BranchID  
	       AND A.CompanyName Like '%' +  @AccountName  + '%'
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [StatementHeader] Record Into [StatementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderInsert] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderInsert] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountNo int,
    @AccountBranchID bigint,
    @CreditTerm smallint,
    @CurrencyCode nvarchar(3),
    @StatementAmount numeric(18, 4),
    @GSTCode nvarchar(10),
    @GSTRate numeric(18, 4),
    @GSTAmount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NewStatementNo varchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Billing].[StatementHeader] (
			[BranchID], [StatementNo], [StatementDate], [AccountNo], [AccountBranchID], [CreditTerm], [CurrencyCode], [StatementAmount], 
			[GSTCode], [GSTRate], [GSTAmount], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @StatementNo, @StatementDate, @AccountNo, @AccountBranchID, @CreditTerm, @CurrencyCode, @StatementAmount, 
			@GSTCode, @GSTRate, @GSTAmount, @CreatedBy, GETUTCDATE()

	Select @NewStatementNo = @StatementNo
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [StatementHeader] Record Into [StatementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderUpdate] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderUpdate] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountNo int,
    @AccountBranchID bigint,
    @CreditTerm smallint,
    @CurrencyCode nvarchar(3),
    @StatementAmount numeric(18, 4),
    @GSTCode nvarchar(10),
    @GSTRate numeric(18, 4),
    @GSTAmount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NewStatementNo varchar(50) OUTPUT

AS 
 
	
BEGIN

	UPDATE	[Billing].[StatementHeader]
	SET		[StatementDate] = @StatementDate, [AccountNo] = @AccountNo, [AccountBranchID] = @AccountBranchID, [CreditTerm] = @CreditTerm, 
			[CurrencyCode] = @CurrencyCode, [StatementAmount] = @StatementAmount, [GSTCode] = @GSTCode, [GSTRate] = @GSTRate, 
			[GSTAmount] = @GSTAmount, 
			[ModifiedBy] = @ModifiedBy, [ModifedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [StatementNo] = @StatementNo

	Select @NewStatementNo = @StatementNo
	
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [StatementHeader] Record Into [StatementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderSave] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderSave] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountNo int,
    @AccountBranchID bigint,
    @CreditTerm smallint,
    @CurrencyCode nvarchar(3),
    @StatementAmount numeric(18, 4),
    @GSTCode nvarchar(10),
    @GSTRate numeric(18, 4),
    @GSTAmount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NewStatementNo varchar(50) OUTPUT

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[StatementHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo)>0
	BEGIN
	    Exec [Billing].[usp_StatementHeaderUpdate] 
				@BranchID, @StatementNo, @StatementDate, @AccountNo, @AccountBranchID, @CreditTerm, @CurrencyCode, @StatementAmount, 
				@GSTCode, @GSTRate, @GSTAmount, @CreatedBy, @ModifiedBy ,@NewStatementNo = @NewStatementNo OUTPUT


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@StatementNo='',@DocID='Bill\Statement'
		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Bill\Statement', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @StatementNo OUTPUT


	    Exec [Billing].[usp_StatementHeaderInsert] 
				@BranchID, @StatementNo, @StatementDate, @AccountNo, @AccountBranchID, @CreditTerm, @CurrencyCode, @StatementAmount, 
				@GSTCode, @GSTRate, @GSTAmount, @CreatedBy, @ModifiedBy ,@NewStatementNo = @NewStatementNo OUTPUT

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[StatementHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [StatementHeader] Record  based on [StatementHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderDelete] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderDelete] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
	@CancelledBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Billing].[StatementHeader]
	SET	IsCancel = CAST(1 as bit),[CancelBy] = @CancelledBy, CancelOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
 

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderDelete]
-- ========================================================================================================================================

GO
 



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementHeaderApprove]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Approve the [StatementHeader] Record  based on [StatementNo]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementHeaderApprove]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementHeaderApprove] 
END 
GO
CREATE PROC [Billing].[usp_StatementHeaderApprove] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
	@ApprovedBy nvarchar(60)
AS 

	
BEGIN

	If Exists
	(
		Select 1
		From [Billing].[StatementHeader]
		Where BranchID =@BranchID
		And StatementNo = @StatementNo
		And IsCancel = Cast(1 as bit)
	)
	Begin
		raiserror('Statement is already cancelled. Cannot proceed to approve!', 16, 1);
		return(-1);
	End

	UPDATE	[Billing].[StatementHeader]
	SET	IsApproved = CAST(1 as bit),[ApprovedBy] = @ApprovedBy, ApprovedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
		   
 

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementHeaderApprove]
-- ========================================================================================================================================

GO
 