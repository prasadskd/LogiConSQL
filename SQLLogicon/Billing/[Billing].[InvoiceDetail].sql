
-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [InvoiceDetail] Record based on [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailSelect] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailSelect] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25),
    @Module SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @JobNo VARCHAR(50),
    @ChargeCode NVARCHAR(20),
    @BillingUnit SMALLINT,
    @ProductCode NVARCHAR(20),
    @MovementCode NVARCHAR(50),
    @SlabFrom SMALLINT,
    @SlabTo SMALLINT
AS 

BEGIN

	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Dt.[BranchID], Dt.[InvoiceNo], Dt.[Module], Dt.[OrderNo], Dt.[ContainerKey], Dt.[JobNo], Dt.[ChargeCode], Dt.[BillingUnit], 
			Dt.[ProductCode], Dt.[MovementCode], Dt.[Size], Dt.[Type], Dt.[ProductCategory], 
			Dt.[CargoCategory], Dt.[TruckCategory], Dt.[CurrencyCode], Dt.[ExRate], Dt.[Qty], Dt.[Price], Dt.[CostRate], 
			Dt.[ForeignAmount], Dt.[LocalAmount], Dt.[DiscountType], Dt.[DiscountAmount], Dt.[ActualAmount], Dt.[TaxAmount], 
			Dt.[TotalAmount], Dt.[IsSlabRate],Dt.[SlabFrom], Dt.[SlabTo],  Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			Chg.ChargeDescription As ChargeCodeDescription,
			ISNULL(Dt.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Dt.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Dt.OutputGSTRate,Dt.InputGSTRate
	FROM	[Billing].[InvoiceDetail] Dt
	Left Outer Join Master.ChargeMaster Chg On 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Dt.BillingUnit = Bu.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		Dt.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Dt.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Dt.DiscountType = Dis.LookupID
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstOutput.GSTCode
	WHERE	Dt.[BranchID] = @BranchID  
			AND Dt.[InvoiceNo] = @InvoiceNo  
			AND Dt.[Module] = @Module  
			AND Dt.[OrderNo] = @OrderNo 
			AND Dt.[ContainerKey] = @ContainerKey  
			AND Dt.[JobNo] = @JobNo  
			AND Dt.[ChargeCode] = @ChargeCode  
			AND Dt.[BillingUnit] = @BillingUnit 
			AND Dt.[ProductCode] = @ProductCode  
			AND Dt.[MovementCode] = @MovementCode 
			AND Dt.[SlabFrom] = @SlabFrom  
			AND Dt.[SlabTo] = @SlabTo  
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [InvoiceDetail] Records from [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailList] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailList] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25)

AS 
BEGIN

	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Dt.[BranchID], Dt.[InvoiceNo], Dt.[Module], Dt.[OrderNo], Dt.[ContainerKey], Dt.[JobNo], Dt.[ChargeCode], Dt.[BillingUnit], 
			Dt.[ProductCode], Dt.[MovementCode], Dt.[Size], Dt.[Type], Dt.[ProductCategory], 
			Dt.[CargoCategory], Dt.[TruckCategory], Dt.[CurrencyCode], Dt.[ExRate], Dt.[Qty], Dt.[Price], Dt.[CostRate], 
			Dt.[ForeignAmount], Dt.[LocalAmount], Dt.[DiscountType], Dt.[DiscountAmount], Dt.[ActualAmount], Dt.[TaxAmount], 
			Dt.[TotalAmount], Dt.[IsSlabRate],Dt.[SlabFrom], Dt.[SlabTo],  Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			Chg.ChargeDescription As ChargeCodeDescription,
			ISNULL(Dt.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Dt.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Dt.OutputGSTRate,Dt.InputGSTRate
	FROM	[Billing].[InvoiceDetail] Dt
	Left Outer Join Master.ChargeMaster Chg On 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Dt.BillingUnit = Bu.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		Dt.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Dt.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Dt.DiscountType = Dis.LookupID
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstOutput.GSTCode
	WHERE	Dt.[BranchID] = @BranchID  
			AND Dt.[InvoiceNo] = @InvoiceNo 

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [InvoiceDetail] Records from [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceDetailPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailPageView] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailPageView] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25),
	@fetchrows bigint
AS 
BEGIN

	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Dt.[BranchID], Dt.[InvoiceNo], Dt.[Module], Dt.[OrderNo], Dt.[ContainerKey], Dt.[JobNo], Dt.[ChargeCode], Dt.[BillingUnit], 
			Dt.[ProductCode], Dt.[MovementCode], Dt.[Size], Dt.[Type], Dt.[ProductCategory], 
			Dt.[CargoCategory], Dt.[TruckCategory], Dt.[CurrencyCode], Dt.[ExRate], Dt.[Qty], Dt.[Price], Dt.[CostRate], 
			Dt.[ForeignAmount], Dt.[LocalAmount], Dt.[DiscountType], Dt.[DiscountAmount], Dt.[ActualAmount], Dt.[TaxAmount], 
			Dt.[TotalAmount], Dt.[IsSlabRate], Dt.[SlabFrom], Dt.[SlabTo], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			Chg.ChargeDescription As ChargeCodeDescription,
			ISNULL(Dt.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Dt.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Dt.OutputGSTRate,Dt.InputGSTRate
	FROM	[Billing].[InvoiceDetail] Dt
	Left Outer Join Master.ChargeMaster Chg On 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Dt.BillingUnit = Bu.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		Dt.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Dt.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Dt.DiscountType = Dis.LookupID
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstOutput.GSTCode
	WHERE	Dt.[BranchID] = @BranchID  
			AND Dt.[InvoiceNo] = @InvoiceNo 
	ORDER BY [ContainerKey] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceDetailRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailRecordCount] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[InvoiceDetail] Dt
	WHERE	Dt.[BranchID] = @BranchID  
			AND Dt.[InvoiceNo] = @InvoiceNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [InvoiceDetail] Record based on [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceDetailAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailAutoCompleteSearch] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25)
AS 

BEGIN

		;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Dt.[BranchID], Dt.[InvoiceNo], Dt.[Module], Dt.[OrderNo], Dt.[ContainerKey], Dt.[JobNo], Dt.[ChargeCode], Dt.[BillingUnit], 
			Dt.[ProductCode], Dt.[MovementCode], Dt.[Size], Dt.[Type], Dt.[ProductCategory], 
			Dt.[CargoCategory], Dt.[TruckCategory], Dt.[CurrencyCode], Dt.[ExRate], Dt.[Qty], Dt.[Price], Dt.[CostRate], 
			Dt.[ForeignAmount], Dt.[LocalAmount], Dt.[DiscountType], Dt.[DiscountAmount], Dt.[ActualAmount], Dt.[TaxAmount], 
			Dt.[TotalAmount], Dt.[IsSlabRate],Dt.[SlabFrom], Dt.[SlabTo], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			Chg.ChargeDescription As ChargeCodeDescription,
			ISNULL(Dt.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Dt.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Dt.OutputGSTRate,Dt.InputGSTRate
	FROM	[Billing].[InvoiceDetail] Dt
	Left Outer Join Master.ChargeMaster Chg On 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Dt.BillingUnit = Bu.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		Dt.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Dt.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Dt.DiscountType = Dis.LookupID
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstOutput.GSTCode
	WHERE	Dt.[BranchID] = @BranchID  
			AND Dt.[InvoiceNo] = @InvoiceNo 
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [InvoiceDetail] Record Into [InvoiceDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailInsert] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailInsert] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @CurrencyCode varchar(3),
    @ExRate numeric(18, 4),
    @Qty numeric(18, 4),
    @Price numeric(18, 4),
    @CostRate numeric(18, 4),
    @ForeignAmount numeric(18, 4),
    @LocalAmount numeric(18, 4),
    @DiscountType smallint,
    @DiscountAmount numeric(18, 4),
    @ActualAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Billing].[InvoiceDetail] (
			[BranchID], [InvoiceNo], [Module], [OrderNo], [ContainerKey], [JobNo], [ChargeCode], [BillingUnit], [ProductCode], [MovementCode], 
			[SlabFrom], [SlabTo], [Size], [Type], [ProductCategory], [CargoCategory], [TruckCategory], [CurrencyCode], [ExRate], [Qty], 
			[Price], [CostRate], [ForeignAmount], [LocalAmount], [DiscountType], [DiscountAmount], [ActualAmount], [TaxAmount], 
			[IsSlabRate], [CreatedBy], [CreatedOn],InputGSTCode,InputGSTRate,OutputGSTCode,OutputGSTRate)
	SELECT	@BranchID, @InvoiceNo, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, 
			@SlabFrom, @SlabTo, @Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @CurrencyCode, @ExRate, @Qty, 
			@Price, @CostRate, @ForeignAmount, @LocalAmount, @DiscountType, @DiscountAmount, @ActualAmount, @TaxAmount, 
			@IsSlabRate, @CreatedBy, GETUTCDATE(),@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [InvoiceDetail] Record Into [InvoiceDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailUpdate] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailUpdate] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @CurrencyCode varchar(3),
    @ExRate numeric(18, 4),
    @Qty numeric(18, 4),
    @Price numeric(18, 4),
    @CostRate numeric(18, 4),
    @ForeignAmount numeric(18, 4),
    @LocalAmount numeric(18, 4),
    @DiscountType smallint,
    @DiscountAmount numeric(18, 4),
    @ActualAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Billing].[InvoiceDetail]
	SET		[SlabFrom] = @SlabFrom, [SlabTo] = @SlabTo, [Size] = @Size, [Type] = @Type, [ProductCategory] = @ProductCategory, 
			[CargoCategory] = @CargoCategory, [TruckCategory] = @TruckCategory, [CurrencyCode] = @CurrencyCode, [ExRate] = @ExRate, [Qty] = @Qty, 
			[Price] = @Price, [CostRate] = @CostRate, [ForeignAmount] = @ForeignAmount, [LocalAmount] = @LocalAmount, [DiscountType] = @DiscountType, 
			[DiscountAmount] = @DiscountAmount, [ActualAmount] = @ActualAmount, [TaxAmount] = @TaxAmount, [IsSlabRate] = @IsSlabRate, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),InputGSTCode=@InputGSTCode,InputGSTRate=@InputGSTRate,
			OutputGSTCode=@OutputGSTCode,OutputGSTRate=@OutputGSTRate
	WHERE	[BranchID] = @BranchID
			AND [InvoiceNo] = @InvoiceNo
			AND [Module] = @Module
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [JobNo] = @JobNo
			AND [ChargeCode] = @ChargeCode
			AND [BillingUnit] = @BillingUnit
			AND [ProductCode] = @ProductCode
			AND [MovementCode] = @MovementCode
			AND [SlabFrom] = @SlabFrom
			AND [SlabTo] = @SlabTo
	
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [InvoiceDetail] Record Into [InvoiceDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailSave] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailSave] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @CurrencyCode varchar(3),
    @ExRate numeric(18, 4),
    @Qty numeric(18, 4),
    @Price numeric(18, 4),
    @CostRate numeric(18, 4),
    @ForeignAmount numeric(18, 4),
    @LocalAmount numeric(18, 4),
    @DiscountType smallint,
    @DiscountAmount numeric(18, 4),
    @ActualAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[InvoiceDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [InvoiceNo] = @InvoiceNo
	       AND [Module] = @Module
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [JobNo] = @JobNo
	       AND [ChargeCode] = @ChargeCode
	       AND [BillingUnit] = @BillingUnit
	       AND [ProductCode] = @ProductCode
	       AND [MovementCode] = @MovementCode
	       AND [SlabFrom] = @SlabFrom
	       AND [SlabTo] = @SlabTo)>0
	BEGIN
	    Exec [Billing].[usp_InvoiceDetailUpdate] 
		@BranchID, @InvoiceNo, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, 
		@Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @CurrencyCode, @ExRate, @Qty, 
		@Price, @CostRate, @ForeignAmount, @LocalAmount, @DiscountType, @DiscountAmount, @ActualAmount, @TaxAmount, 
		@IsSlabRate,@SlabFrom, @SlabTo,@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate,  @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Billing].[usp_InvoiceDetailInsert] 
		@BranchID, @InvoiceNo, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, 
		@Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @CurrencyCode, @ExRate, @Qty, 
		@Price, @CostRate, @ForeignAmount, @LocalAmount, @DiscountType, @DiscountAmount, @ActualAmount, @TaxAmount, 
		@IsSlabRate,@SlabFrom, @SlabTo,@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate,  @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[InvoiceDetailSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [InvoiceDetail] Record  based on [InvoiceDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceDetailDelete] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceDetailDelete] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25)
AS 

	
BEGIN

	
	DELETE
	FROM   [Billing].[InvoiceDetail]
	WHERE  [BranchID] = @BranchID
	       AND [InvoiceNo] = @InvoiceNo
	        
	 


END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceDetailDelete]
-- ========================================================================================================================================

GO
 
  