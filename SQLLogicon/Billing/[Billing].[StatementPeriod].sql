

-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementPeriod] Record based on [StatementPeriod] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementPeriodSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodSelect] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodSelect] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [StatementNo], [StatementDate], [AccountCode], [AccountBranchID], [DueDate], 
			[CreditTerm], [FromPeriod], [ToPeriod], [StatementAmountPreviousOutstanding], [StatementAmountCharged], [StatementAmountPaid], [ReferenceNo] 
	FROM	[Billing].[StatementPeriod]
	WHERE	[BranchID] = @BranchID  
			AND [StatementNo] = @StatementNo  
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementPeriod] Records from [StatementPeriod] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementPeriodList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodList] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodList] 
    @BranchID BIGINT,
	@StatementNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [StatementNo], [StatementDate], [AccountCode], [AccountBranchID], [DueDate], [CreditTerm], [FromPeriod], 
			[ToPeriod], [StatementAmountPreviousOutstanding], [StatementAmountCharged], [StatementAmountPaid], [ReferenceNo] 
	FROM	[Billing].[StatementPeriod]
	WHERE	[BranchID] = @BranchID  
		And [StatementNo]=@StatementNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementPeriod] Records from [StatementPeriod] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementPeriodPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodPageView] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodPageView] 
    @BranchID BIGINT,
	@StatementNo varchar(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [StatementNo], [StatementDate], [AccountCode], [AccountBranchID], [DueDate], [CreditTerm], 
			[FromPeriod], [ToPeriod], [StatementAmountPreviousOutstanding], [StatementAmountCharged], [StatementAmountPaid], [ReferenceNo] 
	FROM	[Billing].[StatementPeriod]
	WHERE	[BranchID] = @BranchID  
		And [StatementNo]=@StatementNo

	ORDER BY [StatementNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [StatementPeriod] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementPeriodRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodRecordCount] 
    @BranchID BIGINT,
	@StatementNo varchar(50)


AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[StatementPeriod]
	WHERE  [BranchID] = @BranchID  
	And [StatementNo]=@StatementNo
	

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementPeriod] Record based on [StatementPeriod] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementPeriodAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodAutoCompleteSearch] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [StatementNo], [StatementDate], [AccountCode], [AccountBranchID], [DueDate], [CreditTerm], [FromPeriod], [ToPeriod], [StatementAmountPreviousOutstanding], [StatementAmountCharged], [StatementAmountPaid], [ReferenceNo] 
	FROM   [Billing].[StatementPeriod]
	WHERE  [BranchID] = @BranchID  
	       AND [StatementNo] LIKE '%' +  @StatementNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [StatementPeriod] Record Into [StatementPeriod] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementPeriodInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodInsert] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodInsert] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountCode int,
    @AccountBranchID bigint,
    @DueDate datetime,
    @CreditTerm smallint,
    @FromPeriod datetime,
    @ToPeriod datetime,
    @StatementAmountPreviousOutstanding numeric(18, 4),
    @StatementAmountCharged numeric(18, 4),
    @StatementAmountPaid numeric(18, 4),
    @ReferenceNo nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Billing].[StatementPeriod] (
			[BranchID], [StatementNo], [StatementDate], [AccountCode], [AccountBranchID], [DueDate], [CreditTerm], 
			[FromPeriod], [ToPeriod], [StatementAmountPreviousOutstanding], [StatementAmountCharged], [StatementAmountPaid], [ReferenceNo])
	SELECT	@BranchID, @StatementNo, @StatementDate, @AccountCode, @AccountBranchID, @DueDate, @CreditTerm, 
			@FromPeriod, @ToPeriod, @StatementAmountPreviousOutstanding, @StatementAmountCharged, @StatementAmountPaid, @ReferenceNo
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [StatementPeriod] Record Into [StatementPeriod] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementPeriodUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodUpdate] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodUpdate] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountCode int,
    @AccountBranchID bigint,
    @DueDate datetime,
    @CreditTerm smallint,
    @FromPeriod datetime,
    @ToPeriod datetime,
    @StatementAmountPreviousOutstanding numeric(18, 4),
    @StatementAmountCharged numeric(18, 4),
    @StatementAmountPaid numeric(18, 4),
    @ReferenceNo nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Billing].[StatementPeriod]
	SET		[StatementDate] = @StatementDate, [AccountCode] = @AccountCode, [AccountBranchID] = @AccountBranchID, [DueDate] = @DueDate, 
			[CreditTerm] = @CreditTerm, [FromPeriod] = @FromPeriod, [ToPeriod] = @ToPeriod, 
			[StatementAmountPreviousOutstanding] = @StatementAmountPreviousOutstanding, [StatementAmountCharged] = @StatementAmountCharged, 
			[StatementAmountPaid] = @StatementAmountPaid, [ReferenceNo] = @ReferenceNo
	WHERE	[BranchID] = @BranchID
			AND [StatementNo] = @StatementNo
	
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [StatementPeriod] Record Into [StatementPeriod] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementPeriodSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodSave] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodSave] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @StatementDate datetime,
    @AccountCode int,
    @AccountBranchID bigint,
    @DueDate datetime,
    @CreditTerm smallint,
    @FromPeriod datetime,
    @ToPeriod datetime,
    @StatementAmountPreviousOutstanding numeric(18, 4),
    @StatementAmountCharged numeric(18, 4),
    @StatementAmountPaid numeric(18, 4),
    @ReferenceNo nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[StatementPeriod] 
		WHERE 	[BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo)>0
	BEGIN
	    Exec [Billing].[usp_StatementPeriodUpdate] 
		@BranchID, @StatementNo, @StatementDate, @AccountCode, @AccountBranchID, @DueDate, @CreditTerm, @FromPeriod, @ToPeriod, 
		@StatementAmountPreviousOutstanding, @StatementAmountCharged, @StatementAmountPaid, @ReferenceNo


	END
	ELSE
	BEGIN
	    Exec [Billing].[usp_StatementPeriodInsert] 
		@BranchID, @StatementNo, @StatementDate, @AccountCode, @AccountBranchID, @DueDate, @CreditTerm, @FromPeriod, @ToPeriod, 
		@StatementAmountPreviousOutstanding, @StatementAmountCharged, @StatementAmountPaid, @ReferenceNo


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[StatementPeriodSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementPeriodDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [StatementPeriod] Record  based on [StatementPeriod]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementPeriodDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementPeriodDelete] 
END 
GO
CREATE PROC [Billing].[usp_StatementPeriodDelete] 
    @BranchID bigint,
    @StatementNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Billing].[StatementPeriod]
	WHERE  [BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
	 


END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementPeriodDelete]
-- ========================================================================================================================================

GO
 