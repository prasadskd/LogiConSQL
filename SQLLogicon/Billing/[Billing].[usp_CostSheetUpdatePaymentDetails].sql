
-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetUpdatePaymentDetails]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	10-May-2017
-- Description:	updates the payment details  Record Into [CostSheet] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetUpdatePaymentDetails]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetUpdatePaymentDetails] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetUpdatePaymentDetails] 
    @BranchID BIGINT,
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @PaidQty decimal(18, 4),
    @PaidAmount decimal(18, 4),
	@IsDeleted bit =0
AS 
 
	
BEGIN

	If @IsDeleted = Cast(1 as bit)
	Begin
		Select @InvoiceNo='', @InvoiceDate=NULL, @PaidQty=0, @PaidAmount=0
	End

	UPDATE	[Billing].[CostSheet]
	SET		[InvoiceNo] = @InvoiceNo, [InvoiceDate] = @InvoiceDate, [PaidQty] = @PaidQty, [PaidAmount] = @PaidAmount
	WHERE	[BranchID] = @BranchID
			AND [Module] = @Module
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [JobNo] = @JobNo
			AND [ChargeCode] = @ChargeCode
			AND [BillingUnit] = @BillingUnit
			AND [ProductCode] = @ProductCode
			AND [MovementCode] = @MovementCode


END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetUpdatePaymentDetails]
-- ========================================================================================================================================

GO
