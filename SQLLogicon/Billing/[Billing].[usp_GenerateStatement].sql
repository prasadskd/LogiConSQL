
-- ========================================================================================================================================
-- START											 [Billing].[usp_GenerateStatement]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementHeader] Record based on [StatementHeader] table

/*
Declare @NewStatementNo varchar(50)
Exec [Billing].[usp_GenerateStatement] 1000,'1003','SYSTEM', @NewStatementNo = @NewStatementNo OUTPUT
Select @NewStatementNo
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_GenerateStatement]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_GenerateStatement] 
END 
GO
CREATE PROC [Billing].[usp_GenerateStatement] 
    @CompanyCode INT,
    @CustomerCode int,
	@CreatedBy nvarchar(50),
	@NewStatementNo varchar(50) OUTPUT

AS 

BEGIN

	


	Declare @ChargeCode nvarchar(20),
			@StmtAmount numeric(18,4),
			@ChargeAmount numeric(18,4),
			@GstCode nvarchar(20),
			@GstRate decimal(18,2),
			@taxAmt decimal(18,4),
			@dt datetime,
			@BillingUnit smallint, 
			@Quantity smallint =0,
			@StatementNo varchar(50)='',
			@BranchID bigint

	Select @ChargeAmount=100.00,  @dt = GetutcDate()


	If Exists
	(
		Select 1 From 
		Billing.StatementHeader 
		Where AccountNo = @CustomerCode
		And Month(StatementDate) = Month(@dt)
		And Year (StatementDate) = Year(@dt)
		And IsCancel = Cast(0 as bit)
	)
	Begin
		raiserror('Statement is already generated for this account for the specified statement date!', 16, 1);
		return(-1);
	End

	If Exists
	(
		Select 1 From 
		Billing.StatementHeader s
		Inner Join Billing.StatementPeriod sp on 
			s.StatementNo = sp.StatementNo
		Where AccountNo = @CustomerCode
		And Month(s.StatementDate) = Month(@dt)
		And Year (s.StatementDate) = Year(@dt)
		And IsCancel = Cast(0 as bit)
		And @dt between sp.FromPeriod And sp.ToPeriod
	)
	Begin
		raiserror('Statement is already generated for this account for the specified statement date!', 16, 1);
		return(-1);
	End


	Select @BranchID = BranchID From 
	Master.Branch Where CompanyCode = @CompanyCode And BranchCode='HQ'

	Select @ChargeCode = ChargeCode ,@GstCode = ISNULL(OutPutGSTCode,'SR'), @BillingUnit =BillingUnit
	From Master.ChargeMaster Where ChargeCode ='SUBFEE'


	select @GstRate=Rate  
	From master.GSTRate
	Where GSTCode=@GstCode

	Select @Quantity = COUNT(0)
	From [Security].[User]
	Where CompanyCode = @CustomerCode
	And IsActive = Cast(1 as bit)


	Select @StmtAmount = (@ChargeAmount * @Quantity)
	Select @taxAmt = (@stmtAmount * @GstRate)/100
	

	Exec [Billing].[usp_StatementHeaderSave] 
    @BranchID ,
	@StatementNo ,
    @dt,
    @CustomerCode,
    @CustomerCode,
    0,
    'MYR',
    @StmtAmount,
    @GstCode,
    @GstRate,
    @taxAmt,
    @CreatedBy,
    @CreatedBy,
	@NewStatementNo = @NewStatementNo OUTPUT


	Exec [Billing].[usp_StatementDetailSave] 
    @BranchID ,
    @NewStatementNo,
    @ChargeCode,
    @BillingUnit,
    @Quantity,
    @ChargeAmount,
    @CreatedBy ,
    @CreatedBy

	Declare @FirstDayOfMonth datetime,
			@LastDayOfMonth datetime,
			@DueDate datetime,
			@CreditTerm smallint = 30,
			@StatementAmountCharged numeric(18,4)=0.00

	

	Select	@FirstDayOfMonth =DATEADD(dd,-(DAY(@dt)-1),@dt),
			@LastDayOfMonth = DATEADD(dd,-(DAY(DATEADD(mm,1,@dt))),DATEADD(mm,1,@dt)),
			@DueDate = DateAdd(dd,-5,@LastDayOfMonth)

	Select @StatementAmountCharged = (@StmtAmount + @taxAmt)


	Exec [Billing].[usp_StatementPeriodSave] 
    @BranchID,
    @NewStatementNo,
    @dt,
    @CustomerCode,
    @CustomerCode,
    @DueDate,
    @CreditTerm,
    @FirstDayOfMonth,
    @LastDayOfMonth,
    0.00,
    @StatementAmountCharged,
    0.00,
    ''


END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_GenerateStatement]
-- ========================================================================================================================================

GO




 
 