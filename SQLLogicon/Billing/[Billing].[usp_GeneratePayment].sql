
-- ========================================================================================================================================
-- START											 [Billing].[usp_GeneratePayment]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementHeader] Record based on [StatementHeader] table

/*
Declare @@NewPaymentNo varchar(50)
Exec [Billing].[usp_GeneratePayment] 1000,'1003','SYSTEM', @NewStatementNo = @NewStatementNo OUTPUT
Select @@NewPaymentNo
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_GeneratePayment]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_GeneratePayment] 
END 
GO
CREATE PROC [Billing].[usp_GeneratePayment] 
    @BranchID BIGINT,
    @StatementNo varchar(50),
	@CreatedBy nvarchar(50),
	@NewPaymentNo varchar(50) OUTPUT

AS 

BEGIN

	


	Declare @ChargeCode nvarchar(20),
			@StmtAmount numeric(18,4),
			@ChargeAmount numeric(18,4),
			@GstCode nvarchar(20),
			@GstRate decimal(18,2),
			@taxAmt decimal(18,4),
			@dt datetime,
			@BillingUnit smallint, 
			@Quantity smallint =0,
			@PaymentNo varchar(50)='',
			@AccountNo int,
			@StatementAmount decimal(18,4),
			@PaymentTerm smallint

	Select @ChargeAmount=100.00,  @dt = GetutcDate()

	If Exists
	(
		Select 1 From 
		Billing.StatementHeader 
		Where BranchID = @BranchID
		And StatementNo = @StatementNo
		And IsCancel = Cast(1 as bit)
	)
	Begin
		raiserror('Statement is cancelled, cannot proceed to generate payment', 16, 1);
		return(-1);
	End

	If Exists
	(
		Select 1 From 
		Billing.Payment 
		Where BranchID = @BranchID
		And StatementNo = @StatementNo
		And IsCancel = Cast(0 as bit)
	)
	Begin
		raiserror('Payment is already generated against the statement, unable to proceed! ' , 16, 1);
		return(-1);
	End


	Select @PaymentTerm=LookupID 
	From config.Lookup 
	Where LookupCategory='PaymentTerm' And LookupCode='CREDIT'
	

	Select @AccountNo = AccountNo,@StatementAmount = StatementAmount
	From Billing.StatementHeader 
	Where 
	BranchID = @BranchID
	And StatementNo = @StatementNo
	

	Exec [Billing].[usp_PaymentSave] 
    @BranchID ,
    @PaymentNo ,
    @StatementNo ,
    @dt ,
    @AccountNo,
    @StatementAmount,
    @StatementAmount,
    @PaymentTerm,
    '' ,
    @CreatedBy ,
    @CreatedBy,
	@NewPaymentNo = @NewPaymentNo OUTPUT

	 
	

END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_GeneratePayment]
-- ========================================================================================================================================

GO




 
 