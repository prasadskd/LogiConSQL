
-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementInquiry]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Payment] Records from [Payment] table


-- Exec [Billing].[usp_StatementInquiry] 1000001,NULL,NULL,'2016-12-01','2016-12-30'
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementInquiry]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementInquiry] 
END 
GO
CREATE PROC [Billing].[usp_StatementInquiry] 
	@BranchID BIGINT,
	@AccountNo bigint=null,
	@SubscriptionType smallint=null,
	@StatementDateFrom datetime=null,
	@StatementDateTo datetime=null


AS 
BEGIN

	SELECT	Hd.[BranchID], Hd.[StatementNo], Hd.[StatementDate], Hd.[AccountNo], Hd.[AccountBranchID], Hd.[CreditTerm], Hd.[CurrencyCode], Hd.[StatementAmount], 
			Hd.[GSTCode], Hd.[GSTRate], Hd.[GSTAmount], Hd.[TotalAmount], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn], Hd.[IsApproved], 
			Hd.[ApprovedBy], Hd.[ApprovedOn], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifedOn] ,
			A.CompanyName As AccountName
	FROM	[Billing].[StatementHeader] Hd
	Left Outer Join Master.Company A On 
		Hd.AccountNo = A.CompanyCode
	WHERE  Hd.[BranchID] = @BranchID  
	And Hd.AccountNo = ISNULL(@AccountNo,Hd.AccountNo)
	And A.RegistrationType = ISNULL(@SubscriptionType,A.RegistrationType)
	And Hd.[StatementDate] Between @StatementDateFrom And @StatementDateTo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementInquiry] 
-- ========================================================================================================================================

GO

