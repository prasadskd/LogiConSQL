-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementDetail] Record based on [StatementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailSelect] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailSelect] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50),
    @ChargeCode NVARCHAR(20)
AS 

BEGIN

	;With BillingUnitDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType')
	SELECT	Dt.[BranchID], Dt.[StatementNo], Dt.[ChargeCode], Dt.[BillingUnit], Dt.[Quantity], Dt.[Amount], Dt.[TotalAmount], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,
			Chg.ChargeDescription ,ISNULL(Bu.LookupDescription,'') BillingUnitDescription
	FROM	[Billing].[StatementDetail] Dt
	Left Outer Join [Master].[ChargeMaster] Chg ON 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitDescription Bu ON 
		Dt.BillingUnit = Bu.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[StatementNo] = @StatementNo  
	       AND Dt.[ChargeCode] = @ChargeCode  
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementDetail] Records from [StatementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailList] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailList] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50)

AS 
BEGIN

	;With BillingUnitDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType')
	SELECT	Dt.[BranchID], Dt.[StatementNo], Dt.[ChargeCode], Dt.[BillingUnit], Dt.[Quantity], Dt.[Amount], Dt.[TotalAmount], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,
			Chg.ChargeDescription ,ISNULL(Bu.LookupDescription,'') BillingUnitDescription
	FROM	[Billing].[StatementDetail] Dt
	Left Outer Join [Master].[ChargeMaster] Chg ON 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitDescription Bu ON 
		Dt.BillingUnit = Bu.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[StatementNo] = @StatementNo 

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StatementDetail] Records from [StatementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementDetailPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailPageView] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailPageView] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;With BillingUnitDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType')
	SELECT	Dt.[BranchID], Dt.[StatementNo], Dt.[ChargeCode], Dt.[BillingUnit], Dt.[Quantity], Dt.[Amount], Dt.[TotalAmount], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,
			Chg.ChargeDescription ,ISNULL(Bu.LookupDescription,'') BillingUnitDescription
	FROM	[Billing].[StatementDetail] Dt
	Left Outer Join [Master].[ChargeMaster] Chg ON 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitDescription Bu ON 
		Dt.BillingUnit = Bu.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[StatementNo] = @StatementNo 
	ORDER BY Dt.[ChargeCode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [StatementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementDetailRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailRecordCount] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[StatementDetail] Dt
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[StatementNo] = @StatementNo 
	

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StatementDetail] Record based on [StatementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_StatementDetailAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailAutoCompleteSearch] 
    @BranchID BIGINT,
    @StatementNo NVARCHAR(50) 
AS 

BEGIN

	;With BillingUnitDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType')
	SELECT	Dt.[BranchID], Dt.[StatementNo], Dt.[ChargeCode], Dt.[BillingUnit], Dt.[Quantity], Dt.[Amount], Dt.[TotalAmount], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,
			Chg.ChargeDescription ,ISNULL(Bu.LookupDescription,'') BillingUnitDescription
	FROM	[Billing].[StatementDetail] Dt
	Left Outer Join [Master].[ChargeMaster] Chg ON 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitDescription Bu ON 
		Dt.BillingUnit = Bu.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[StatementNo] LIKE '%' +  @StatementNo  + '%'
	       
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [StatementDetail] Record Into [StatementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailInsert] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailInsert] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @Quantity smallint,
    @Amount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Billing].[StatementDetail] ([BranchID], [StatementNo], [ChargeCode], [BillingUnit], [Quantity], [Amount], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @StatementNo, @ChargeCode, @BillingUnit, @Quantity, @Amount, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [StatementDetail] Record Into [StatementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailUpdate] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailUpdate] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @Quantity smallint,
    @Amount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 
	
BEGIN

	UPDATE [Billing].[StatementDetail]
	SET    [BillingUnit] = @BillingUnit, [Quantity] = @Quantity, [Amount] = @Amount, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GetUtcDate()
	WHERE  [BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
	       AND [ChargeCode] = @ChargeCode
	
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [StatementDetail] Record Into [StatementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailSave] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailSave] 
    @BranchID bigint,
    @StatementNo nvarchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @Quantity smallint,
    @Amount numeric(18, 4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[StatementDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
	       AND [ChargeCode] = @ChargeCode)>0
	BEGIN
	    Exec [Billing].[usp_StatementDetailUpdate] 
		@BranchID, @StatementNo, @ChargeCode, @BillingUnit, @Quantity, @Amount, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Billing].[usp_StatementDetailInsert] 
		@BranchID, @StatementNo, @ChargeCode, @BillingUnit, @Quantity, @Amount, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[StatementDetailSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_StatementDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [StatementDetail] Record  based on [StatementDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_StatementDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_StatementDetailDelete] 
END 
GO
CREATE PROC [Billing].[usp_StatementDetailDelete] 
    @BranchID bigint,
    @StatementNo nvarchar(50) 
AS 

	
BEGIN

 
	DELETE
	FROM   [Billing].[StatementDetail]
	WHERE  [BranchID] = @BranchID
	       AND [StatementNo] = @StatementNo
	 

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_StatementDetailDelete]
-- ========================================================================================================================================

GO

