
-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Payment] Record based on [Payment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentSelect] 
END 
GO
CREATE PROC [Billing].[usp_PaymentSelect] 
    @BranchID BIGINT,
    @PaymentNo NVARCHAR(50),
    @StatementNo NVARCHAR(50)
AS 

BEGIN

	;With PaymentTermDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='PaymentTerm')
	SELECT	P.[BranchID], P.[PaymentNo], P.[StatementNo], P.[PaymentDate], P.[AccountNo], P.[StatementAmount], P.[PaymentAmount], P.[PaymentTerm], 
			P.[IsApproved], P.[ApprovedBy], P.[ApprovedOn], P.[IsCancel], P.[CancelledBy], P.[CancelledOn], P.[Remarks], 
			P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(PT.LookupDescription,'') As PaymentTermDescription,
			A.CompanyName As AccountName
	FROM	[Billing].[Payment] P
	Left Outer Join PaymentTermDescription PT ON 
		P.PaymentTerm = PT.LookupID
	Left Outer Join Master.Company A On 
		P.AccountNo = A.CompanyCode
	WHERE  [BranchID] = @BranchID  
	       AND [PaymentNo] = @PaymentNo  
	       AND [StatementNo] = @StatementNo  
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Payment] Records from [Payment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentList] 
END 
GO
CREATE PROC [Billing].[usp_PaymentList] 
	@BranchID BIGINT

AS 
BEGIN

	;With PaymentTermDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='PaymentTerm')
	SELECT	P.[BranchID], P.[PaymentNo], P.[StatementNo], P.[PaymentDate], P.[AccountNo], P.[StatementAmount], P.[PaymentAmount], P.[PaymentTerm], 
			P.[IsApproved], P.[ApprovedBy], P.[ApprovedOn], P.[IsCancel], P.[CancelledBy], P.[CancelledOn], P.[Remarks], 
			P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(PT.LookupDescription,'') As PaymentTermDescription,
			A.CompanyName As AccountName
	FROM	[Billing].[Payment] P
	Left Outer Join PaymentTermDescription PT ON 
		P.PaymentTerm = PT.LookupID
	Left Outer Join Master.Company A On 
		P.AccountNo = A.CompanyCode
	WHERE  [BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Payment] Records from [Payment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentPageView] 
END 
GO
CREATE PROC [Billing].[usp_PaymentPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;With PaymentTermDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='PaymentTerm')
	SELECT	P.[BranchID], P.[PaymentNo], P.[StatementNo], P.[PaymentDate], P.[AccountNo], P.[StatementAmount], P.[PaymentAmount], P.[PaymentTerm], 
			P.[IsApproved], P.[ApprovedBy], P.[ApprovedOn], P.[IsCancel], P.[CancelledBy], P.[CancelledOn], P.[Remarks], 
			P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(PT.LookupDescription,'') As PaymentTermDescription,
			A.CompanyName As AccountName
	FROM	[Billing].[Payment] P
	Left Outer Join PaymentTermDescription PT ON 
		P.PaymentTerm = PT.LookupID
	Left Outer Join Master.Company A On 
		P.AccountNo = A.CompanyCode
	WHERE  [BranchID] = @BranchID  
	ORDER BY [PaymentNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Payment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_PaymentRecordCount] 
    @BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[Payment]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Payment] Record based on [Payment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_PaymentAutoCompleteSearch] 
    @BranchID BIGINT,
    @PaymentNo NVARCHAR(50) 
AS 

BEGIN

	;With PaymentTermDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='PaymentTerm')
	SELECT	P.[BranchID], P.[PaymentNo], P.[StatementNo], P.[PaymentDate], P.[AccountNo], P.[StatementAmount], P.[PaymentAmount], P.[PaymentTerm], 
			P.[IsApproved], P.[ApprovedBy], P.[ApprovedOn], P.[IsCancel], P.[CancelledBy], P.[CancelledOn], P.[Remarks], 
			P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(PT.LookupDescription,'') As PaymentTermDescription,
			A.CompanyName As AccountName
	FROM	[Billing].[Payment] P
	Left Outer Join PaymentTermDescription PT ON 
		P.PaymentTerm = PT.LookupID
	Left Outer Join Master.Company A On 
		P.AccountNo = A.CompanyCode
	WHERE  [BranchID] = @BranchID   
	       AND [PaymentNo] Like '%' +  @PaymentNo + '%'
	       
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Payment] Record Into [Payment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_PaymentInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentInsert] 
END 
GO
CREATE PROC [Billing].[usp_PaymentInsert] 
    @BranchID bigint,
    @PaymentNo nvarchar(50),
    @StatementNo nvarchar(50),
    @PaymentDate datetime,
    @AccountNo int,
    @StatementAmount decimal(18, 4),
    @PaymentAmount decimal(18, 4),
    @PaymentTerm smallint,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewPaymentNo nvarchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Billing].[Payment] (
			[BranchID], [PaymentNo], [StatementNo], [PaymentDate], [AccountNo], [StatementAmount], [PaymentAmount], [PaymentTerm], 
			[Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @PaymentNo, @StatementNo, @PaymentDate, @AccountNo, @StatementAmount, @PaymentAmount, @PaymentTerm, 
			@Remarks, @CreatedBy, GetUtcDate()
               
	Select @NewPaymentNo = @PaymentNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Payment] Record Into [Payment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_PaymentUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentUpdate] 
END 
GO
CREATE PROC [Billing].[usp_PaymentUpdate] 
    @BranchID bigint,
    @PaymentNo nvarchar(50),
    @StatementNo nvarchar(50),
    @PaymentDate datetime,
    @AccountNo int,
    @StatementAmount decimal(18, 4),
    @PaymentAmount decimal(18, 4),
    @PaymentTerm smallint,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewPaymentNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Billing].[Payment]
	SET		[PaymentDate] = @PaymentDate, [AccountNo] = @AccountNo, [StatementAmount] = @StatementAmount, [PaymentAmount] = @PaymentAmount, 
			[PaymentTerm] = @PaymentTerm, [Remarks] = @Remarks, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GetUtcDate()
	WHERE	[BranchID] = @BranchID
			AND [PaymentNo] = @PaymentNo
			AND [StatementNo] = @StatementNo
	
	Select @NewPaymentNo = @PaymentNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Payment] Record Into [Payment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_PaymentSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentSave] 
END 
GO
CREATE PROC [Billing].[usp_PaymentSave] 
    @BranchID bigint,
    @PaymentNo nvarchar(50),
    @StatementNo nvarchar(50),
    @PaymentDate datetime,
    @AccountNo int,
    @StatementAmount decimal(18, 4),
    @PaymentAmount decimal(18, 4),
    @PaymentTerm smallint,
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewPaymentNo nvarchar(50) OUTPUT
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[Payment] 
		WHERE 	[BranchID] = @BranchID
	       AND [PaymentNo] = @PaymentNo
	       AND [StatementNo] = @StatementNo)>0
	BEGIN
	    Exec [Billing].[usp_PaymentUpdate] 
		@BranchID, @PaymentNo, @StatementNo, @PaymentDate, @AccountNo, @StatementAmount, @PaymentAmount, @PaymentTerm, 
		@Remarks, @CreatedBy, @ModifiedBy ,@NewPaymentNo = @NewPaymentNo OUTPUT


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@PaymentNo='',@DocID='Bill\Payment'
		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Bill\Payment', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @PaymentNo OUTPUT


	    Exec [Billing].[usp_PaymentInsert] 
		@BranchID, @PaymentNo, @StatementNo, @PaymentDate, @AccountNo, @StatementAmount, @PaymentAmount, @PaymentTerm, 
		@Remarks, @CreatedBy, @ModifiedBy ,@NewPaymentNo = @NewPaymentNo OUTPUT

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[PaymentSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Payment] Record  based on [Payment]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_PaymentDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentDelete] 
END 
GO
CREATE PROC [Billing].[usp_PaymentDelete] 
    @BranchID bigint,
    @PaymentNo nvarchar(50),
    @StatementNo nvarchar(50),
	@CancelledBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Billing].[Payment]
	SET	[IsCancel] = CAST(1 as bit),CancelledBy = @CancelledBy,CancelledOn = GetUtcDate()
	WHERE 	[BranchID] = @BranchID
	       AND [PaymentNo] = @PaymentNo
	       AND [StatementNo] = @StatementNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentDelete]
-- ========================================================================================================================================

GO
 



-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentApprove]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Approve the [Payment] Record  based on [PaymentNo]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_PaymentApprove]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentApprove] 
END 
GO
CREATE PROC [Billing].[usp_PaymentApprove] 
    @BranchID bigint,
    @PaymentNo nvarchar(50),
    @StatementNo nvarchar(50),
	@ApprovedBy nvarchar(60)
AS 

	
BEGIN

	If Exists
	(
		Select 1
		From [Billing].[Payment]
		Where BranchID =@BranchID
		And [PaymentNo] = @PaymentNo
		AND [StatementNo] = @StatementNo
		And IsCancel = Cast(1 as bit)
	)
	Begin
		raiserror('Payment is already cancelled. Cannot proceed to approve!', 16, 1);
		return(-1);
	End

	UPDATE	[Billing].[Payment]
	SET	[IsApproved] = CAST(1 as bit),ApprovedBy = @ApprovedBy,ApprovedOn = GetUtcDate()
	WHERE 	[BranchID] = @BranchID
	       AND [PaymentNo] = @PaymentNo
	       AND [StatementNo] = @StatementNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentDelete]
-- ========================================================================================================================================

GO
 