

-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CostSheet] Record based on [CostSheet] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_CostSheetSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetSelect] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetSelect] 
    @BranchID BIGINT,
    @Module SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @JobNo VARCHAR(50),
    @ChargeCode NVARCHAR(20),
    @BillingUnit SMALLINT,
    @ProductCode NVARCHAR(20),
    @MovementCode NVARCHAR(50)
AS 

BEGIN


	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Cs.[BranchID], Cs.[Module], Cs.[OrderNo], Cs.[ContainerKey], Cs.[JobNo], Cs.[ChargeCode], Cs.[BillingUnit], Cs.[ProductCode], Cs.[MovementCode], 
			Cs.[PaymentTerm], Cs.[PaymentTo], Cs.[Size], Cs.[Type], Cs.[ProductCategory], Cs.[CargoCategory], Cs.[TruckCategory], Cs.[VendorCode], Cs.[CurrencyCode], 
			Cs.[ExRate], Cs.[Qty], Cs.[Price], Cs.[CostRate], Cs.[ForeignAmount], Cs.[LocalAmount], Cs.[DiscountType],Cs.[DiscountRate], Cs.[DiscountAmount], Cs.[ActualAmount], 
			Cs.[TaxAmount], Cs.[TotalAmount], Cs.[IsSlabRate], Cs.[SlabFrom], Cs.[SlabTo], Cs.[IsVAS], Cs.[IsWaived], Cs.[IsAutoLoad], Cs.[IsInternalCost], 
			Cs.[IsUserInput], Cs.[QuotationNo], Cs.[Remark], Cs.[InvoiceNo], Cs.[InvoiceDate], Cs.[PaidQty], Cs.[PaidAmount], Cs.[Status], 
			Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			ISNULL(Chg.ChargeCode + ' - ' + Chg.ChargeDescription,'')  As ChargeCodeDescription,
			ISNULL(Mr.MerchantName,'') As VendorName,
			ISNULL(Cs.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Cs.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Cs.OutputGSTRate,Cs.InputGSTRate
	FROM	[Billing].[CostSheet] Cs
	Left Outer Join Master.ChargeMaster Chg On 
		Cs.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Cs.BillingUnit = Bu.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Cs.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Cs.PaymentTerm = Pm.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		cs.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Cs.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Cs.DiscountType = Dis.LookupID
	Left Outer Join Master.Merchant Mr On
		Cs.VendorCode = Mr.MerchantCode
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstInput.GSTCode
	WHERE  Cs.[BranchID] = @BranchID  
	       AND Cs.[Module] = @Module  
	       AND Cs.[OrderNo] = @OrderNo  
	       AND Cs.[ContainerKey] = @ContainerKey  
	       AND Cs.[JobNo] = @JobNo  
	       AND Cs.[ChargeCode] = @ChargeCode  
	       AND Cs.[BillingUnit] = @BillingUnit 
	       AND Cs.[ProductCode] = @ProductCode  
	       AND Cs.[MovementCode] = @MovementCode 
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CostSheet] Records from [CostSheet] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_CostSheetList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetList] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetList] 
	@BranchID BIGINT,
	@Module smallint,
	@OrderNo nvarchar(50)
	
AS 
BEGIN

	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Cs.[BranchID], Cs.[Module], Cs.[OrderNo], Cs.[ContainerKey], Cs.[JobNo], Cs.[ChargeCode], Cs.[BillingUnit], Cs.[ProductCode], Cs.[MovementCode], 
			Cs.[PaymentTerm], Cs.[PaymentTo], Cs.[Size], Cs.[Type], Cs.[ProductCategory], Cs.[CargoCategory], Cs.[TruckCategory], Cs.[VendorCode], Cs.[CurrencyCode], 
			Cs.[ExRate], Cs.[Qty], Cs.[Price], Cs.[CostRate], Cs.[ForeignAmount], Cs.[LocalAmount], Cs.[DiscountType],Cs.[DiscountRate], Cs.[DiscountAmount], Cs.[ActualAmount], 
			Cs.[TaxAmount], Cs.[TotalAmount], Cs.[IsSlabRate], Cs.[SlabFrom], Cs.[SlabTo], Cs.[IsVAS], Cs.[IsWaived], Cs.[IsAutoLoad], Cs.[IsInternalCost], 
			Cs.[IsUserInput], Cs.[QuotationNo], Cs.[Remark], Cs.[InvoiceNo], Cs.[InvoiceDate], Cs.[PaidQty], Cs.[PaidAmount], Cs.[Status], 
			Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			ISNULL(Chg.ChargeCode + ' - ' + Chg.ChargeDescription,'')  As ChargeCodeDescription,
			ISNULL(Mr.MerchantName,'') As VendorName,
			ISNULL(Cs.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Cs.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Cs.OutputGSTRate,Cs.InputGSTRate
	FROM	[Billing].[CostSheet] Cs
	Left Outer Join Master.ChargeMaster Chg On 
		Cs.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Cs.BillingUnit = Bu.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Cs.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Cs.PaymentTerm = Pm.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		cs.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Cs.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Cs.DiscountType = Dis.LookupID
	Left Outer Join Master.Merchant Mr On
		Cs.VendorCode = Mr.MerchantCode
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstInput.GSTCode
	WHERE  Cs.[BranchID] = @BranchID 
			And Cs.Module = @Module
			And Cs.OrderNo = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CostSheet] Records from [CostSheet] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_CostSheetPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetPageView] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetPageView] 
	@BranchID BIGINT,
	@Module smallint,
	@fetchrows bigint
AS 
BEGIN

	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Cs.[BranchID], Cs.[Module], Cs.[OrderNo], Cs.[ContainerKey], Cs.[JobNo], Cs.[ChargeCode], Cs.[BillingUnit], Cs.[ProductCode], Cs.[MovementCode], 
			Cs.[PaymentTerm], Cs.[PaymentTo], Cs.[Size], Cs.[Type], Cs.[ProductCategory], Cs.[CargoCategory], Cs.[TruckCategory], Cs.[VendorCode], Cs.[CurrencyCode], 
			Cs.[ExRate], Cs.[Qty], Cs.[Price], Cs.[CostRate], Cs.[ForeignAmount], Cs.[LocalAmount], Cs.[DiscountType],Cs.[DiscountRate], Cs.[DiscountAmount], Cs.[ActualAmount], 
			Cs.[TaxAmount], Cs.[TotalAmount], Cs.[IsSlabRate], Cs.[SlabFrom], Cs.[SlabTo], Cs.[IsVAS], Cs.[IsWaived], Cs.[IsAutoLoad], Cs.[IsInternalCost], 
			Cs.[IsUserInput], Cs.[QuotationNo], Cs.[Remark], Cs.[InvoiceNo], Cs.[InvoiceDate], Cs.[PaidQty], Cs.[PaidAmount], Cs.[Status], 
			Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			ISNULL(Chg.ChargeCode + ' - ' + Chg.ChargeDescription,'')  As ChargeCodeDescription,
			ISNULL(Mr.MerchantName,'') As VendorName,
			ISNULL(Cs.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Cs.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Cs.OutputGSTRate,Cs.InputGSTRate
	FROM	[Billing].[CostSheet] Cs
	Left Outer Join Master.ChargeMaster Chg On 
		Cs.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Cs.BillingUnit = Bu.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Cs.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Cs.PaymentTerm = Pm.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		cs.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Cs.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Cs.DiscountType = Dis.LookupID
	Left Outer Join Master.Merchant Mr On
		Cs.VendorCode = Mr.MerchantCode
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstInput.GSTCode
	WHERE  Cs.[BranchID] = @BranchID 
	And Cs.Module = @Module
	ORDER BY [OrderNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CostSheet] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_CostSheetRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetRecordCount] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetRecordCount] 
	@BranchID BIGINT,
	@Module smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Billing].[CostSheet]
	Where BranchID = @BranchID
		And Module = @Module
	

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CostSheet] Record based on [CostSheet] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_CostSheetAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetAutoCompleteSearch] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetAutoCompleteSearch] 
    @BranchID BIGINT,
	@Module smallint,
    @OrderNo VARCHAR(50)
    
AS 

BEGIN

	
	;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Cs.[BranchID], Cs.[Module], Cs.[OrderNo], Cs.[ContainerKey], Cs.[JobNo], Cs.[ChargeCode], Cs.[BillingUnit], Cs.[ProductCode], Cs.[MovementCode], 
			Cs.[PaymentTerm], Cs.[PaymentTo], Cs.[Size], Cs.[Type], Cs.[ProductCategory], Cs.[CargoCategory], Cs.[TruckCategory], Cs.[VendorCode], Cs.[CurrencyCode], 
			Cs.[ExRate], Cs.[Qty], Cs.[Price], Cs.[CostRate], Cs.[ForeignAmount], Cs.[LocalAmount], Cs.[DiscountType],Cs.[DiscountRate], Cs.[DiscountAmount], Cs.[ActualAmount], 
			Cs.[TaxAmount], Cs.[TotalAmount], Cs.[IsSlabRate], Cs.[SlabFrom], Cs.[SlabTo], Cs.[IsVAS], Cs.[IsWaived], Cs.[IsAutoLoad], Cs.[IsInternalCost], 
			Cs.[IsUserInput], Cs.[QuotationNo], Cs.[Remark], Cs.[InvoiceNo], Cs.[InvoiceDate], Cs.[PaidQty], Cs.[PaidAmount], Cs.[Status], 
			Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			ISNULL(Chg.ChargeCode + ' - ' + Chg.ChargeDescription,'')  As ChargeCodeDescription,
			ISNULL(Mr.MerchantName,'') As VendorName,
			ISNULL(Cs.OutputGSTCode,'') As OutputGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Cs.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription,
			Cs.OutputGSTRate,Cs.InputGSTRate
	FROM	[Billing].[CostSheet] Cs
	Left Outer Join Master.ChargeMaster Chg On 
		Cs.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Cs.BillingUnit = Bu.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Cs.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Cs.PaymentTerm = Pm.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		cs.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Cs.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Cs.DiscountType = Dis.LookupID
	Left Outer Join Master.Merchant Mr On
		Cs.VendorCode = Mr.MerchantCode
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstInput.GSTCode
	WHERE  Cs.[BranchID] = @BranchID
		   AND Cs.Module = @Module
	       AND [OrderNo] LIKE '%' + @OrderNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CostSheet] Record Into [CostSheet] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetInsert] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetInsert] 
    @BranchID BIGINT,
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @VendorCode nvarchar(10),
    @CurrencyCode varchar(3),
    @ExRate decimal(18, 4),
    @Qty decimal(18, 4),
    @Price decimal(18, 4),
    @CostRate decimal(18, 4),
    @ForeignAmount decimal(18, 4),
    @LocalAmount decimal(18, 4),
    @DiscountType smallint,
    @DiscountRate decimal(5,2),
	@DiscountAmount decimal(18, 4),
    @ActualAmount decimal(18, 4),
    @TaxAmount decimal(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
    @IsVAS bit,
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsInternalCost bit,
    @IsUserInput bit,
    @QuotationNo varchar(50),
    @Remark nvarchar(100),
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @PaidQty decimal(18, 4),
    @PaidAmount decimal(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2)
AS 
  

BEGIN

	
	INSERT INTO [Billing].[CostSheet] (
			[BranchID], [Module], [OrderNo], [ContainerKey], [JobNo], [ChargeCode], [BillingUnit], [ProductCode], [MovementCode], 
			[PaymentTerm], [PaymentTo], [Size], [Type], [ProductCategory], [CargoCategory], [TruckCategory], [VendorCode], [CurrencyCode], 
			[ExRate], [Qty], [Price], [CostRate], [ForeignAmount], [LocalAmount], [DiscountType],[DiscountRate], [DiscountAmount], [ActualAmount], [TaxAmount], 
			[IsSlabRate], [SlabFrom], [SlabTo], [IsVAS], [IsWaived], [IsAutoLoad], [IsInternalCost], [IsUserInput], [QuotationNo], [Remark], 
			[InvoiceNo], [InvoiceDate], [PaidQty], [PaidAmount], [Status], [CreatedBy], [CreatedOn],InputGSTCode,InputGSTRate,OutputGSTCode,OutputGSTRate)
	SELECT	@BranchID, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, 
			@PaymentTerm, @PaymentTo, @Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @VendorCode, @CurrencyCode, 
			@ExRate, @Qty, @Price, @CostRate, @ForeignAmount, @LocalAmount, @DiscountType,@DiscountRate, @DiscountAmount, @ActualAmount, @TaxAmount, 
			@IsSlabRate, @SlabFrom, @SlabTo, @IsVAS, @IsWaived, @IsAutoLoad, @IsInternalCost, @IsUserInput, @QuotationNo, @Remark, 
			@InvoiceNo, @InvoiceDate, @PaidQty, @PaidAmount, cast(1 as bit), @CreatedBy, GETUTCDATE(),@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CostSheet] Record Into [CostSheet] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetUpdate] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetUpdate] 
    @BranchID BIGINT,
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @VendorCode nvarchar(10),
    @CurrencyCode varchar(3),
    @ExRate decimal(18, 4),
    @Qty decimal(18, 4),
    @Price decimal(18, 4),
    @CostRate decimal(18, 4),
    @ForeignAmount decimal(18, 4),
    @LocalAmount decimal(18, 4),
    @DiscountType smallint,
    @DiscountRate decimal(5,2),
    @DiscountAmount decimal(18, 4),
    @ActualAmount decimal(18, 4),
    @TaxAmount decimal(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
    @IsVAS bit,
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsInternalCost bit,
    @IsUserInput bit,
    @QuotationNo varchar(50),
    @Remark nvarchar(100),
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @PaidQty decimal(18, 4),
    @PaidAmount decimal(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2)
AS 
 
	
BEGIN

	UPDATE	[Billing].[CostSheet]
	SET		[BranchID] = @BranchID, [Module] = @Module, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [JobNo] = @JobNo, 
			[ChargeCode] = @ChargeCode, [BillingUnit] = @BillingUnit, [ProductCode] = @ProductCode, [MovementCode] = @MovementCode, 
			[PaymentTerm] = @PaymentTerm, [PaymentTo] = @PaymentTo, [Size] = @Size, [Type] = @Type, [ProductCategory] = @ProductCategory, 
			[CargoCategory] = @CargoCategory, [TruckCategory] = @TruckCategory, [VendorCode] = @VendorCode, [CurrencyCode] = @CurrencyCode, 
			[ExRate] = @ExRate, [Qty] = @Qty, [Price] = @Price, [CostRate] = @CostRate, [ForeignAmount] = @ForeignAmount, 
			[LocalAmount] = @LocalAmount, [DiscountType] = @DiscountType,DiscountRate=@DiscountRate, [DiscountAmount] = @DiscountAmount, [ActualAmount] = @ActualAmount, 
			[TaxAmount] = @TaxAmount, [IsSlabRate] = @IsSlabRate, [SlabFrom] = @SlabFrom, [SlabTo] = @SlabTo, [IsVAS] = @IsVAS, [IsWaived] = @IsWaived, 
			[IsAutoLoad] = @IsAutoLoad, [IsInternalCost] = @IsInternalCost, [IsUserInput] = @IsUserInput, [QuotationNo] = @QuotationNo, 
			[Remark] = @Remark, [InvoiceNo] = @InvoiceNo, [InvoiceDate] = @InvoiceDate, [PaidQty] = @PaidQty, [PaidAmount] = @PaidAmount, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),
			InputGSTCode=@InputGSTCode,InputGSTRate=@InputGSTRate,
			OutputGSTCode=@OutputGSTCode,OutputGSTRate=@OutputGSTRate
	WHERE	[BranchID] = @BranchID
			AND [Module] = @Module
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [JobNo] = @JobNo
			AND [ChargeCode] = @ChargeCode
			AND [BillingUnit] = @BillingUnit
			AND [ProductCode] = @ProductCode
			AND [MovementCode] = @MovementCode
	
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CostSheet] Record Into [CostSheet] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetSave] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetSave] 
    @BranchID BIGINT,
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @Size varchar(2),
    @Type varchar(2),
    @ProductCategory smallint,
    @CargoCategory smallint,
    @TruckCategory smallint,
    @VendorCode nvarchar(10),
    @CurrencyCode varchar(3),
    @ExRate decimal(18, 4),
    @Qty decimal(18, 4),
    @Price decimal(18, 4),
    @CostRate decimal(18, 4),
    @ForeignAmount decimal(18, 4),
    @LocalAmount decimal(18, 4),
    @DiscountType smallint,
    @DiscountRate decimal(5,2),
    @DiscountAmount decimal(18, 4),
    @ActualAmount decimal(18, 4),
    @TaxAmount decimal(18, 4),
    @IsSlabRate bit,
    @SlabFrom smallint,
    @SlabTo smallint,
    @IsVAS bit,
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsInternalCost bit,
    @IsUserInput bit,
    @QuotationNo varchar(50),
    @Remark nvarchar(100),
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @PaidQty decimal(18, 4),
    @PaidAmount decimal(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@InputGSTCode nvarchar(10),
	@InputGSTRate decimal(18,2),
	@OutputGSTCode nvarchar(10),
	@OutputGSTRate decimal(18,2)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Billing].[CostSheet] 
		WHERE 	[BranchID] = @BranchID
	       AND [Module] = @Module
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [JobNo] = @JobNo
	       AND [ChargeCode] = @ChargeCode
	       AND [BillingUnit] = @BillingUnit
	       AND [ProductCode] = @ProductCode
	       AND [MovementCode] = @MovementCode)>0
	BEGIN
	    Exec [Billing].[usp_CostSheetUpdate] 
			@BranchID, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, @PaymentTerm, 
			@PaymentTo, @Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @VendorCode, @CurrencyCode, @ExRate, @Qty, @Price, 
			@CostRate, @ForeignAmount, @LocalAmount, @DiscountType,@DiscountRate, @DiscountAmount, @ActualAmount, @TaxAmount, @IsSlabRate, @SlabFrom, @SlabTo, 
			@IsVAS, @IsWaived, @IsAutoLoad, @IsInternalCost, @IsUserInput, @QuotationNo, @Remark, @InvoiceNo, @InvoiceDate, @PaidQty, 
			@PaidAmount, @CreatedBy, @ModifiedBy ,@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate


	END
	ELSE
	BEGIN
	    Exec [Billing].[usp_CostSheetInsert] 
			@BranchID, @Module, @OrderNo, @ContainerKey, @JobNo, @ChargeCode, @BillingUnit, @ProductCode, @MovementCode, @PaymentTerm, 
			@PaymentTo, @Size, @Type, @ProductCategory, @CargoCategory, @TruckCategory, @VendorCode, @CurrencyCode, @ExRate, @Qty, @Price, 
			@CostRate, @ForeignAmount, @LocalAmount, @DiscountType,@DiscountRate, @DiscountAmount, @ActualAmount, @TaxAmount, @IsSlabRate, @SlabFrom, @SlabTo, 
			@IsVAS, @IsWaived, @IsAutoLoad, @IsInternalCost, @IsUserInput, @QuotationNo, @Remark, @InvoiceNo, @InvoiceDate, @PaidQty, 
			@PaidAmount, @CreatedBy, @ModifiedBy ,@InputGSTCode,@InputGSTRate,@OutputGSTCode,@OutputGSTRate

	END

	Declare @dt datetime = GetUTCDate()

	Exec [Billing].[usp_CostSheetUpdatePaymentDetails] 
			@BranchID,@Module,@OrderNo,@ContainerKey,@JobNo,@ChargeCode,@BillingUnit,@ProductCode,@MovementCode,@InvoiceNo,@dt,@Qty,@LocalAmount,0

	
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[CostSheetSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CostSheet] Record  based on [CostSheet]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetDelete] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetDelete] 
    @BranchID BIGINT,
    @Module smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @JobNo varchar(50),
    @ChargeCode nvarchar(20),
    @BillingUnit smallint,
    @ProductCode nvarchar(20),
    @MovementCode nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Billing].[CostSheet]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [Module] = @Module
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [JobNo] = @JobNo
	       AND [ChargeCode] = @ChargeCode
	       AND [BillingUnit] = @BillingUnit
	       AND [ProductCode] = @ProductCode
	       AND [MovementCode] = @MovementCode

	 Exec [Billing].[usp_CostSheetUpdatePaymentDetails] 
			@BranchID,@Module,@OrderNo,@ContainerKey,@JobNo,@ChargeCode,@BillingUnit,@ProductCode,@MovementCode,'',NULL,0,0, 1
     



END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetDelete]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_CostSheetDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CostSheet] Record  based on [CostSheet]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_CostSheetDeleteAll]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_CostSheetDeleteAll] 
END 
GO
CREATE PROC [Billing].[usp_CostSheetDeleteAll] 
    @BranchID BIGINT,
    @OrderNo varchar(50)
AS 

	
BEGIN

	DELETE [Billing].[CostSheet]
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_CostSheetDeleteAll]
-- ========================================================================================================================================

GO
