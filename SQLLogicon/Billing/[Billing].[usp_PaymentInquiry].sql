
-- ========================================================================================================================================
-- START											 [Billing].[usp_PaymentInquiry]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Payment] Records from [Payment] table


-- Exec [Billing].[usp_PaymentInquiry] 1000001,NULL,NULL,'2016-12-01','2016-12-30'
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_PaymentInquiry]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_PaymentInquiry] 
END 
GO
CREATE PROC [Billing].[usp_PaymentInquiry] 
	@BranchID BIGINT,
	@AccountNo bigint=null,
	@SubscriptionType smallint=null,
	@PaymentDateFrom datetime=null,
	@PaymentDateTo datetime=null


AS 
BEGIN

	;With PaymentTermDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='PaymentTerm')
	SELECT	P.[BranchID], P.[PaymentNo], P.[StatementNo], P.[PaymentDate], P.[AccountNo], P.[StatementAmount], P.[PaymentAmount], P.[PaymentTerm], 
			P.[IsApproved], P.[ApprovedBy], P.[ApprovedOn], P.[IsCancel], P.[CancelledBy], P.[CancelledOn], P.[Remarks], 
			P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(PT.LookupDescription,'') As PaymentTermDescription,
			A.CompanyName As AccountName
	FROM	[Billing].[Payment] P
	Left Outer Join PaymentTermDescription PT ON 
		P.PaymentTerm = PT.LookupID
	Left Outer Join Master.Company A On 
		P.AccountNo = A.CompanyCode
	WHERE  P.[BranchID] = @BranchID  
	And P.AccountNo = ISNULL(@AccountNo,P.AccountNo)
	And A.RegistrationType = ISNULL(@SubscriptionType,A.RegistrationType)
	And P.PaymentDate Between @PaymentDateFrom And @PaymentDateTo

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_PaymentInquiry] 
-- ========================================================================================================================================

GO

