

-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [InvoiceHeader] Record based on [InvoiceHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderSelect] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderSelect] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25)
AS 

SET NOCOUNT ON; 

BEGIN
	;with
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='Module'),
	InvoiceTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='InvoiceType')
	SELECT	[BranchID], [InvoiceNo], [InvoiceDate], [InvoiceType], [Module], [CustomerCode], [CreditTerm], 
			[InvoiceAmount], [TaxAmount], [TotalAmount], [IsWHTax], [WithHoldingAmount], [NetAmount], [CashAmount], [ChequeAmount], 
			[ChequeNo], [ChequeDate], [BankCharges], [Remark], [IsCancel], [CancelBy], [CancelOn], 
			[IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], 
			[IsPrinted], [PrintedBy], [PrintedOn] ,
			ISNULL(ML.LookupDescription,'') As ModuleTypeDescription,
			ISNULL(IL.LookupDescription,'') As InvoiceTypeDescription
	FROM	[Billing].[InvoiceHeader] Hd
	Left Outer Join ModuleLookup ML ON 
		Hd.Module = ML.LookupID
	Left Outer Join InvoiceTypeLookup IL ON 
		Hd.InvoiceType = IL.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [InvoiceNo] = @InvoiceNo 
		   And IsCancel=CAST(0 as bit) 

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [InvoiceHeader] Records from [InvoiceHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Billing].[usp_InvoiceHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderList] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderList] 
	@BranchID BIGINT
AS 
BEGIN

	;with
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='Module'),
	InvoiceTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='InvoiceType')
	SELECT	[BranchID], [InvoiceNo], [InvoiceDate], [InvoiceType], [Module], [CustomerCode], [CreditTerm], 
			[InvoiceAmount], [TaxAmount], [TotalAmount], [IsWHTax], [WithHoldingAmount], [NetAmount], [CashAmount], [ChequeAmount], 
			[ChequeNo], [ChequeDate], [BankCharges], [Remark], [IsCancel], [CancelBy], [CancelOn], 
			[IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], 
			[IsPrinted], [PrintedBy], [PrintedOn] ,
			ISNULL(ML.LookupDescription,'') As ModuleTypeDescription,
			ISNULL(IL.LookupDescription,'') As InvoiceTypeDescription
	FROM	[Billing].[InvoiceHeader] Hd
	Left Outer Join ModuleLookup ML ON 
		Hd.Module = ML.LookupID
	Left Outer Join InvoiceTypeLookup IL ON 
		Hd.InvoiceType = IL.LookupID

	WHERE  [BranchID] = @BranchID  
	       And IsCancel=CAST(0 as bit)	


END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceHeaderList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [InvoiceHeader] Record Into [InvoiceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderInsert] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderInsert] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @InvoiceType smallint,
    @Module smallint,
    @CustomerCode nvarchar(10),
    @CreditTerm smallint,
    @InvoiceAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @TotalAmount numeric(18, 4),
    @IsWHTax bit,
    @WithHoldingAmount numeric(18, 4),
    @NetAmount numeric(18, 4),
    @CashAmount numeric(18, 4),
    @ChequeAmount numeric(18, 4),
    @ChequeNo nvarchar(20),
    @ChequeDate datetime,
    @BankCharges numeric(18, 4),
    @Remark nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo varchar(25) OUTPUT

AS 
  

BEGIN
SET NOCOUNT ON;
	
	INSERT INTO [Billing].[InvoiceHeader] (
			[BranchID], [InvoiceNo], [InvoiceDate], [InvoiceType], [Module], [CustomerCode], [CreditTerm], [InvoiceAmount], [TaxAmount], 
			[TotalAmount], [IsWHTax], [WithHoldingAmount], [NetAmount], [CashAmount], [ChequeAmount], [ChequeNo], [ChequeDate], [BankCharges], 
			[Remark], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @InvoiceNo, @InvoiceDate, @InvoiceType, @Module, @CustomerCode, @CreditTerm, @InvoiceAmount, @TaxAmount, 
			@TotalAmount, @IsWHTax, @WithHoldingAmount, @NetAmount, @CashAmount, @ChequeAmount, @ChequeNo, @ChequeDate, @BankCharges, @Remark, 
			@CreatedBy, GETUTCDATE()

	Select @NewDocumentNo = @InvoiceNo

SET NOCOUNT OFF;	
               
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [InvoiceHeader] Record Into [InvoiceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderUpdate] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderUpdate] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @InvoiceType smallint,
    @Module smallint,
    @CustomerCode nvarchar(10),
    @CreditTerm smallint,
    @InvoiceAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @TotalAmount numeric(18, 4),
    @IsWHTax bit,
    @WithHoldingAmount numeric(18, 4),
    @NetAmount numeric(18, 4),
    @CashAmount numeric(18, 4),
    @ChequeAmount numeric(18, 4),
    @ChequeNo nvarchar(20),
    @ChequeDate datetime,
    @BankCharges numeric(18, 4),
    @Remark nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo varchar(25) OUTPUT

AS 
 
	
BEGIN

SET NOCOUNT ON;

	UPDATE	[Billing].[InvoiceHeader]
	SET		[InvoiceDate] = @InvoiceDate, [InvoiceType] = @InvoiceType, [Module] = @Module, [CustomerCode] = @CustomerCode, [CreditTerm] = @CreditTerm, 
			[InvoiceAmount] = @InvoiceAmount, [TaxAmount] = @TaxAmount, [TotalAmount] = @TotalAmount, [IsWHTax] = @IsWHTax, 
			[WithHoldingAmount] = @WithHoldingAmount, [NetAmount] = @NetAmount, [CashAmount] = @CashAmount, [ChequeAmount] = @ChequeAmount, 
			[ChequeNo] = @ChequeNo, [ChequeDate] = @ChequeDate, [BankCharges] = @BankCharges, [Remark] = @Remark, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [InvoiceNo] = @InvoiceNo

	Select @NewDocumentNo = @InvoiceNo
	
SET NOCOUNT OFF;
END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [InvoiceHeader] Record Into [InvoiceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderSave] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderSave] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
    @InvoiceDate datetime,
    @InvoiceType smallint,
    @Module smallint,
    @CustomerCode nvarchar(10),
    @CreditTerm smallint,
    @InvoiceAmount numeric(18, 4),
    @TaxAmount numeric(18, 4),
    @TotalAmount numeric(18, 4),
    @IsWHTax bit,
    @WithHoldingAmount numeric(18, 4),
    @NetAmount numeric(18, 4),
    @CashAmount numeric(18, 4),
    @ChequeAmount numeric(18, 4),
    @ChequeNo nvarchar(20),
    @ChequeDate datetime,
    @BankCharges numeric(18, 4),
    @Remark nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo varchar(25) OUTPUT
AS 
 

BEGIN
SET NOCOUNT ON;
	IF (SELECT COUNT(0) FROM [Billing].[InvoiceHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [InvoiceNo] = @InvoiceNo)>0
	BEGIN
	    Exec [Billing].[usp_InvoiceHeaderUpdate] 
				@BranchID, @InvoiceNo, @InvoiceDate, @InvoiceType, @Module, @CustomerCode, @CreditTerm, @InvoiceAmount, @TaxAmount, @TotalAmount, 
				@IsWHTax, @WithHoldingAmount, @NetAmount, @CashAmount, @ChequeAmount, @ChequeNo, @ChequeDate, @BankCharges, @Remark, 
				@CreatedBy, @ModifiedBy ,@NewDocumentNo = @NewDocumentNo OUTPUT


	END
	ELSE
	BEGIN


	Declare @Dt datetime,
				@InvoiceTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@InvoiceTypeDesc = LookupDescription,@InvoiceNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@InvoiceType
		
		--print @BookingTypeDesc
		
		IF @InvoiceTypeDesc='Customer Invoice' 
			SET @DocID ='Bill\Invoice(Credit)'
		ELSE IF @InvoiceTypeDesc='Customer CashBill' 
			SET @DocID ='Bill\Invoice(Cash)'
		 
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Bill\Invoice(Cash)', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @InvoiceNo OUTPUT

	    Exec [Billing].[usp_InvoiceHeaderInsert] 
				@BranchID, @InvoiceNo, @InvoiceDate, @InvoiceType, @Module, @CustomerCode, @CreditTerm, @InvoiceAmount, @TaxAmount, @TotalAmount, 
				@IsWHTax, @WithHoldingAmount, @NetAmount, @CashAmount, @ChequeAmount, @ChequeNo, @ChequeDate, @BankCharges, @Remark, 
				@CreatedBy, @ModifiedBy ,@NewDocumentNo = @NewDocumentNo OUTPUT

	END
	
SET NOCOUNT OFF;
END

	

-- ========================================================================================================================================
-- END  											 [Billing].usp_[InvoiceHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Billing].[usp_InvoiceHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [InvoiceHeader] Record  based on [InvoiceHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Billing].[usp_InvoiceHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Billing].[usp_InvoiceHeaderDelete] 
END 
GO
CREATE PROC [Billing].[usp_InvoiceHeaderDelete] 
    @BranchID BIGINT,
    @InvoiceNo varchar(25),
	@CancelBy nvarchar(50)
AS 

	
BEGIN
SET NOCOUNT ON;
	UPDATE	[Billing].[InvoiceHeader]
	SET	IsCancel = CAST(0 as bit),CancelOn = GETUTCDATE(), CancelBy = @CancelBy
	WHERE 	[BranchID] = @BranchID
	       AND [InvoiceNo] = @InvoiceNo

	

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Billing].[usp_InvoiceHeaderDelete]
-- ========================================================================================================================================

GO
