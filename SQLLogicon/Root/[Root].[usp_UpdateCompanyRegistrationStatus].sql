
-- ========================================================================================================================================
-- START											 [Root].[usp_UpdateCompanyRegistrationStatus]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Update the Registration Status of the New Company.

-- [Root].[usp_UpdateCompanyRegistrationStatus] 1021,1,'ADMIN',''

--select * from [security].[user] where userid =''
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_UpdateCompanyRegistrationStatus]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_UpdateCompanyRegistrationStatus] 
END 
GO

CREATE PROC [Root].[usp_UpdateCompanyRegistrationStatus] 
    @CompanyID INT,
	@RegistrationStatus bit=null,
	@ApprovedBy nvarchar(50),
	@Remarks nvarchar(255),
	@NewCompanyID varchar(25) OUTPUT
     
AS 

BEGIN

	Declare @activityDate datetime,
			@StatusApprove smallint=0,
			@vRegistrationStatus smallint,
			@vMasterBranchID bigint=1000001, -- DNEX BRANCHID,
			@vRegistrationStatusONQUERY smallint=0;


	Select @activityDate = GETUTCDATE()
	
	If @RegistrationStatus IS NULL 
	Begin
		 
		Select @vRegistrationStatus = LookupID
		From Config.Lookup where LookupDescription ='On Query' And LookupCategory='RegistrationStatus'
		Select @StatusApprove= 0
	End
	Else
	Begin

		If @RegistrationStatus = cast(1 as bit)
		Begin
			Select @vRegistrationStatus = LookupID
			From Config.Lookup where LookupDescription ='Approved' And LookupCategory='RegistrationStatus'

			Select @StatusApprove = @vRegistrationStatus
		End
		Else 
		Begin
			Select @vRegistrationStatus = LookupID
			From Config.Lookup where LookupDescription ='Rejected' And LookupCategory='RegistrationStatus'

			Select @StatusApprove =0
		End
	End 
	

	Update Root.RegisteredCompany Set RegistrationStatus = @vRegistrationStatus
	Where CompanyID = @CompanyID
	
	/*Insert Activity Log */
	Exec [Root].[usp_CompanyRegistrationActivityInsert]	@CompanyID, @vRegistrationStatus, @activityDate, @ApprovedBy
	

	
	 

	If	@StatusApprove = @vRegistrationStatus
	Begin

	Declare @CompanyCode Int,
		@CompanyName nvarchar(100),
		@RegNo nvarchar(50),
		@GSTNo nvarchar(50),
		@RegistrationType smallint,
		@Logo nvarchar(100),
		@TaxId nvarchar(50),
		@IsActive bit,
		@RegistrationReferenceNo INT,
		@CreatedBy nvarchar(50),
		@ModifiedBy nvarchar(50),
		@NewCompanyCode int,
		@NewBranchID bigint,
		@UserID int,
		@UserName nvarchar(50),
		@Password nvarchar(100),
		@UserGroup smallint,
		@UserDesignation smallint,
		@EmployeeID nvarchar(20),
		@ICNo nvarchar(20),
		@ContactNo nvarchar(20),
		@EmailID nvarchar(60)

Declare @MerchantCode BigInt =0,
		@BillingCustomer nvarchar(10)='',
		@PortCode varchar(10)='',
		@OwnerCode nvarchar(10)='',
		@AgentCode nvarchar(10)='',
		@YardCode nvarchar(10)='',
		@CreditTerm int=0,
		@CreditLimit numeric(18, 4)=0.00,
		@DefaultCurrency nvarchar(3)='MYR',
		@CreditorCode nvarchar(10)='',
		@DebtorCode nvarchar(10)='',
		@EDIMappingCode nvarchar(20)='',
		@WebSite nvarchar(100)='',
		@IsBillingCustomer bit=0,
		@IsShipper bit=0,
		@IsConsignee bit=0,
		@IsLiner bit=0,
		@IsForwarder bit=0,
		@IsYard bit=0,
		@IsCFS bit=0,
		@IsRailOperator bit=0,
		@IsTerminal bit=0,
		@IsPrincipal bit=0,
		@IsVMIVendor bit=0,
		@IsHaulier bit=0,
		@IsTransporter bit=0,
		@IsVendor bit=0,
		@IsRailYard bit=0,
		@IsTaxable bit=1,
		@IsSelectContainer bit=0,
		@BookingRemarks nvarchar(100)='',
		@EDIMailBoxNo nvarchar(15)='',
		@BusinessType smallint =0,
		@IsFreightForwarder bit=0,
		@IsOverseasAgent bit=0,
		@IsSubscriber bit=1,
		@IsOriginal bit=1,
		@SubscriberCompanyCode BigInt = 0

		

		Select 
			@CompanyCode=0,@CompanyName =CompanyName,@RegNo=RegistrationNo,@GSTNo =[GSTNo],@RegistrationType=0,
			@Logo='',@TaxID='',@IsActive=Cast(1 as bit),@CreatedBy = @ApprovedBy,@ModifiedBy=@ApprovedBy,@UserID = UserID,@AgentCode=AgentName,@EDIMailBoxNo = ''
		From [Root].[RegisteredCompany] Rc 
		Where CompanyID = @CompanyID
		And IsDeclared = Cast(1 as bit)

		Select @UserName=FullName,@Password=Password,@EmployeeID='',@ICNo=NRIC,@EmailID =Email,@ContactNo=MobileNo
		From [Root].[Registration]
		Where UserID = @UserID

		/*Create a New Company Information */
		Exec [Master].[usp_CompanySave] 
		@CompanyCode,@CompanyName,@RegNo,@GSTNo,@RegistrationType,@Logo,@TaxId,@IsActive,@CompanyID,@AgentCode,@EDIMailBoxNo,
		@CreatedBy,@ModifiedBy,@NewCompanyCode = @NewCompanyCode OUTPUT

		set @NewCompanyID = @NewCompanyCode

		Insert Into Master.Address
		(LinkID,SeqNo,AddressType,Address1,Address2,Address3,Address4,City,State,CountryCode,ZipCode,
		TelNo,FaxNo,CreatedBy,ModifiedBy) 
		Select @NewCompanyCode,1,'Company',Address1,Address2,Address3,'',City,State,CountryCode,PostCode,
		'','',@CreatedBy,@CreatedBy
		From [Root].[CompanyAddress]
		Where CompanyID = @CompanyID

		/*Create a New Branch Information */

		Exec [Master].[usp_BranchSave] 
			0,@NewCompanyCode ,'HQ', @CompanyName,@RegNo ,@GSTNo ,@TaxID,@AgentCode,@EDIMailBoxNo ,@CreatedBy ,@ModifiedBy ,@NewBranchID =@NewBranchID OUTPUT

		Insert Into Master.Address
		(LinkID,SeqNo,AddressType,Address1,Address2,Address3,Address4,City,State,CountryCode,ZipCode,
		TelNo,FaxNo,CreatedBy,ModifiedBy) 
		Select @NewBranchID,1,'Branch',Address1,Address2,Address3,'',City,State,CountryCode,PostCode,
		'','',@CreatedBy,@CreatedBy
		From [Root].[CompanyAddress]
		Where CompanyID = @CompanyID

		Declare @BillingMethod smallint,
				@Trader smallint,
				@Haulage smallint,
				@Transport smallint,
				@Warehouse smallint,
				@Depot smallint,
				@Agentforwarder smallint,
				@NewMerchantCode bigint

		select @BillingMethod = LookupID from config.lookup where lookupcategory ='BillingMethodType' And LookupDescription='Order Completed';
		select  @Trader=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Trader';
		select  @Haulage=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Haulage';
		select  @Warehouse=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Warehouse';
		select  @Transport=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Transporter';
		select  @Depot=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Depot';
		select  @Agentforwarder=LookupID from config.lookup where lookupcategory like '%RegistrationType%' And LookupDescription='Agent';

		

		--IF(Select COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID

		/* Update the Registration Type into master.CompanySubscription table */


		Insert Into Master.CompanySubscription(CompanyCode,ModuleID,IsActive,CreatedBy,CreatedOn) 
		Select @NewCompanyCode,RegistrationType,Cast(1 as bit),@CreatedBy,GETUTCDATE() 
		From Root.CompanyRegistrationType 
		where CompanyId = @CompanyID

		insert into Master.CompanySubscriptionBillingStatus
		(CompanyCode, ModuleID, IsBillable, ActiveDate, InActiveDate, CreatedBy, CreatedOn, ModifedBy, ModifiedOn)
		Select @NewCompanyCode, RegistrationType, Cast(1 as bit),getutcdate(), null, 'ADMIN', getutcdate(), null, null 
		From Root.CompanyRegistrationType Where CompanyID = @CompanyID

		Declare @vTrader smallint = 0 
		

		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Trader) > 0 
			Select @IsShipper = Cast(1 as bit),@IsConsignee = Cast(1 as bit),@IsForwarder= Cast(0 as bit),@IsHaulier= Cast(0 as bit), @IsTransporter= Cast(0 as bit),@IsTerminal= Cast(0 as bit)
			 

		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Haulage) > 0 
			Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(0 as bit),@IsHaulier= Cast(1 as bit), @IsTransporter= Cast(0 as bit),@IsTerminal= Cast(0 as bit)

		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Warehouse) > 0 
			Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(0 as bit),@IsHaulier= Cast(0 as bit), @IsTransporter= Cast(0 as bit),@IsTerminal= Cast(0 as bit)

		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Transport) > 0 
			Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(0 as bit),@IsHaulier= Cast(0 as bit), @IsTransporter= Cast(1 as bit),@IsTerminal= Cast(0 as bit)

		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Depot) > 0 
			Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(0 as bit),@IsHaulier= Cast(0 as bit), @IsTransporter= Cast(0 as bit),@IsTerminal= Cast(0 as bit)
		
		
		If ( Select  COUNT(0) From Root.CompanyRegistrationType Where CompanyID = @CompanyID And RegistrationType = @Agentforwarder) > 0 
			Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(1 as bit),@IsHaulier= Cast(0 as bit), @IsTransporter= Cast(0 as bit),@IsTerminal= Cast(0 as bit)




		--Else
		--	Select @IsShipper = Cast(0 as bit),@IsConsignee = Cast(0 as bit),@IsForwarder= Cast(1 as bit)
		
		/* Save the CompanyCode in the EDIMapping Code for Merchant	*/
		Select @EDIMappingCode = Convert(nvarchar(20),@NewCompanyCode)

		/*
			Added BusinessType parameter in the below statement [Master].[usp_MerchantInsert] .
			by default 0 will be inserted.
		*/

		/* Add Merchant	*/
		Exec [Master].[usp_MerchantInsert] 
				@MerchantCode,@CompanyName,@BillingCustomer,@PortCode,@RegNo, @GSTNo ,@OwnerCode,@AgentCode,@YardCode,
				@BillingMethod,@CreditTerm,@CreditLimit,@DefaultCurrency,@CreditorCode, @DebtorCode, 
				@EDIMappingCode, @WebSite, @BookingRemarks, @IsBillingCustomer, @IsShipper, @IsConsignee, @IsLiner, @IsForwarder, @IsYard, @IsCFS, 
				@IsRailOperator, @IsTerminal, @IsPrincipal, @IsVMIVendor, @IsHaulier, @IsTransporter, @IsVendor, @IsRailYard, @IsTaxable, 
				@IsSelectContainer,@CreatedBy,@CreatedBy,@NewCompanyCode,@BusinessType,@IsFreightForwarder,@IsOverseasAgent,@IsSubscriber,@IsOriginal,@SubscriberCompanyCode, @NewMerchantCode = @NewMerchantCode OUTPUT

		Insert Into Master.Address
		(LinkID,SeqNo,AddressType,Address1,Address2,Address3,Address4,City,State,CountryCode,ZipCode,
		TelNo,FaxNo,CreatedBy,ModifiedBy) 
		Select @NewMerchantCode,1,'Merchant',Address1,Address2,Address3,'',City,State,CountryCode,PostCode,
		'','',@CreatedBy,@CreatedBy
		From [Root].[CompanyAddress]
		Where CompanyID = @CompanyID


		Update Master.Merchant Set IsVerified =Cast(1 as bit) ,VerifiedBy = @CreatedBy,VerifiedOn = GETUTCDATE()
		Where MerchantCode = @NewMerchantCode


		/*Create a New Admin User */

		Select @UserGroup=LookupID From config.lookup where lookupcategory ='UserGroupType' And LookupDescription ='ADMINISTRATION'

		Select @UserDesignation = LookupID From config.lookup where Lookupcategory ='UserDesignationType' And LookupDescription ='SYSTEM ADMINISTRATOR'

		insert into [Security].[Roles](CompanyCode, RoleCode, RoleDescription, IsActive, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)
		Values(@NewCompanyCode, 'ADMIN','ADMIN', Cast(1 as BIT), 'ADMIN', GETUTCDATE(), NULL, NULL)

		insert into [Security].[RoleRights](CompanyCode, RoleCode, SecurableItem)
		Select @NewCompanyCode as CompanyCode, 'ADMIN', SecurableItem From [Security].[Securables]

		Exec [Security].[usp_UserSave] 
			@NewCompanyCode,@EmailID,@UserName,@Password,@UserGroup,@UserDesignation,@EmployeeID,@ICNo,@EmailID,@ContactNo,@ApprovedBy,@ApprovedBy,@NewBranchID,'ADMIN' 

		/*
		Insert Into [Security].[UserRights]
		Select @EmailID,SecurableItem,LinkGroup,LinkID
		From [Security].[UserRights]
		Where UserID = 'trader@dnex.com.my'*/

		/*Create DocumentNumber Settings */
		
		Delete From Utility.DocumentNumberHeader Where BranchID = @NewBranchID

		Insert Into Utility.DocumentNumberHeader
		Select	@NewBranchID, [DocumentID], [DocumentKey], [DocumentPrefix], [NumberLength], [LastNumber], [UseCompany], [UseBranch], 
				[UseYear], [UseMonth], @CreatedBy, GETUTCDATE(), @CreatedBy, GETUTCDATE()
		From Utility.DocumentNumberHeader
		Where BranchID = @vMasterBranchID

	End
	Else
	Begin
		Update Root.RegisteredCompany Set Remarks = @Remarks
		Where CompanyID = @CompanyID

		set @NewCompanyID = @CompanyID
		
	End
	        
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_UpdateCompanyRegistrationStatus]
-- ========================================================================================================================================
GO