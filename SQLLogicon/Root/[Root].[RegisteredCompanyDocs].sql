-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [RegisteredCompanyDocs] Record based on [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsSelect] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsSelect] 
    @CompanyID INT,
    @RegistrationDocType SMALLINT,
	@ItemNo smallint
AS 

BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationDocType')
	SELECT [CompanyID], [RegistrationDocType],[ItemNo], [FileName], [IPAddress], [CreatedOn], [IsVerified], [VerifiedBy], [VerifiedOn],
	ISNULL(RT.LookupDescription,'') As RegistrationDocTypeDescription 
	FROM   [Root].[RegisteredCompanyDocs] RC 
	Left Outer Join RegistrationDocTypeDescription RT ON 
		RC.RegistrationDocType = RT.LookupID
	WHERE  [CompanyID] = @CompanyID  
	       AND [RegistrationDocType] = @RegistrationDocType  
		   And [ItemNo] = @ItemNo
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompanyDocs] Records from [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsList] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsList] 
	@CompanyID INT

AS 
BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationDocType')
	SELECT [CompanyID], [RegistrationDocType],[ItemNo], [FileName], [IPAddress], [CreatedOn], [IsVerified], [VerifiedBy], [VerifiedOn],
	ISNULL(RT.LookupDescription,'') As RegistrationDocTypeDescription 
	FROM   [Root].[RegisteredCompanyDocs] RC 
	Left Outer Join RegistrationDocTypeDescription RT ON 
		RC.RegistrationDocType = RT.LookupID
	WHERE  [CompanyID] = @CompanyID  

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompanyDocs] Records from [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsPageView] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsPageView] 
	@CompanyID INT,
	@fetchrows bigint
AS 
BEGIN

		;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationDocType')
	SELECT [CompanyID], [RegistrationDocType],[ItemNo], [FileName], [IPAddress], [CreatedOn], [IsVerified], [VerifiedBy], [VerifiedOn],
	ISNULL(RT.LookupDescription,'') As RegistrationDocTypeDescription 
	FROM   [Root].[RegisteredCompanyDocs] RC 
	Left Outer Join RegistrationDocTypeDescription RT ON 
		RC.RegistrationDocType = RT.LookupID
	WHERE  [CompanyID] = @CompanyID  
	ORDER BY [FileName]
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsRecordCount] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsRecordCount] 
	@CompanyID INT

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[RegisteredCompanyDocs]
	WHERE  [CompanyID] = @CompanyID  


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [RegisteredCompanyDocs] Record based on [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsAutoCompleteSearch] 
    @CompanyID INT,
	@FileName nvarchar(255)
AS 

BEGIN

		;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationDocType')
	SELECT [CompanyID], [RegistrationDocType],[ItemNo], [FileName], [IPAddress], [CreatedOn], [IsVerified], [VerifiedBy], [VerifiedOn],
	ISNULL(RT.LookupDescription,'') As RegistrationDocTypeDescription 
	FROM   [Root].[RegisteredCompanyDocs] RC 
	Left Outer Join RegistrationDocTypeDescription RT ON 
		RC.RegistrationDocType = RT.LookupID
	WHERE  [CompanyID] = @CompanyID  
	       AND [FileName] = @FileName
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [RegisteredCompanyDocs] Record Into [RegisteredCompanyDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsInsert] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsInsert] 
    @CompanyID int,
    @RegistrationDocType smallint,
	@ItemNo smallint,
    @FileName nvarchar(255),
    @IPAddress nvarchar(50)
AS 
  

BEGIN

	
	
	INSERT INTO [Root].[RegisteredCompanyDocs] (
			[CompanyID], [RegistrationDocType],[ItemNo], [FileName], [IPAddress], [CreatedOn])
	SELECT	@CompanyID, @RegistrationDocType,@ItemNo, @FileName, @IPAddress, GetUTCDate()

	
               
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [RegisteredCompanyDocs] Record Into [RegisteredCompanyDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsUpdate] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsUpdate] 
    @CompanyID int,
    @RegistrationDocType smallint,
	@ItemNo smallint,
    @FileName nvarchar(255),
    @IPAddress nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Root].[RegisteredCompanyDocs]
	SET    [FileName] = @FileName, [IPAddress] = @IPAddress
	WHERE  [CompanyID] = @CompanyID
	       AND [RegistrationDocType] = @RegistrationDocType
		   And [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [RegisteredCompanyDocs] Record Into [RegisteredCompanyDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsSave] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsSave] 
    @CompanyID int,
    @RegistrationDocType smallint,
	@ItemNo smallint,
    @FileName nvarchar(255),
    @IPAddress nvarchar(50)
 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Root].[RegisteredCompanyDocs] 
		WHERE 	[CompanyID] = @CompanyID
	       AND [RegistrationDocType] = @RegistrationDocType)>0
	BEGIN
	    Exec [Root].[usp_RegisteredCompanyDocsUpdate] 
		@CompanyID, @RegistrationDocType,@ItemNo, @FileName, @IPAddress 


	END
	ELSE
	BEGIN
	    Exec [Root].[usp_RegisteredCompanyDocsInsert] 
		@CompanyID, @RegistrationDocType,@ItemNo, @FileName, @IPAddress 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[RegisteredCompanyDocsSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [RegisteredCompanyDocs] Record  based on [RegisteredCompanyDocs]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsDelete] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsDelete] 
    @CompanyID int,
    @RegistrationDocType smallint,
	@ItemNo smallint
AS 

	
BEGIN

	 
	DELETE
	FROM   [Root].[RegisteredCompanyDocs]
	WHERE  [CompanyID] = @CompanyID
	       AND [RegistrationDocType] = @RegistrationDocType
		   And [ItemNo] = @ItemNo
	 


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsDelete]
-- ========================================================================================================================================

GO
 


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDocsDeleteALL]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [RegisteredCompanyDocs] Record  based on [RegisteredCompanyDocs]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDocsDeleteALL]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDocsDeleteALL] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDocsDeleteALL] 
    @CompanyID int
AS 

	
BEGIN

	 
	DELETE
	FROM   [Root].[RegisteredCompanyDocs]
	WHERE  [CompanyID] = @CompanyID
	        


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDocsDeleteALL]
-- ========================================================================================================================================

GO
 
 
-- ========================================================================================================================================
-- START											 [Root].[usp_GetRegisteredCompanyInfo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompanyDocs] Records from [RegisteredCompanyDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_GetRegisteredCompanyInfo]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_GetRegisteredCompanyInfo] 
END 
GO
CREATE PROC [Root].[usp_GetRegisteredCompanyInfo] 
	@CompanyID INT

AS 
BEGIN


select R.Email, R.FullName, C.CompanyName from Master.Company C
	INNER JOIN ROOT.REGISTEREDCOMPANY RC ON
		C.RegistrationReferenceNo = RC.CompanyId
	INNER JOIN root.registration R ON 
		R.UserID = RC.UserID
	WHERE C.Companycode =  @CompanyID

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_GetRegisteredCompanyInfo] 
-- ========================================================================================================================================

GO


		
 
		
