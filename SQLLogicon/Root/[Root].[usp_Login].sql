
-- ========================================================================================================================================
-- START											 [Root].[usp_Login]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	21-Nov-2016
-- Description:	Insert Registered User Details
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_Login]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_Login] 
END 
GO
CREATE PROC [Root].[usp_Login]
@Email nvarchar(60),
@Password nvarchar(100)

As
Begin
 
 

	SELECT	[UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], 
			[IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy],
			[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM	[Root].[Registration]
	Where	Email = @Email and Password = @Password


End

 

