
-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanySearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompany] Records from [RegisteredCompany] table

/*
Exec [Root].[usp_RegisteredCompanySearch] 'Zest',NULL,'2016-12-01','2017-01-10'
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanySearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanySearch] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanySearch] 
    @CompanyName nvarchar(255),
    @RegistrationNo nvarchar(50),
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL,
	@RegistrationStatus int,
	@Email nvarchar(100)
AS 
BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory=''OrganizationType''),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory=''RegistrationStatus'')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'''') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'''') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID
	Where 1 = 1 '
	

	if (len(rtrim(@CompanyName)) > 0) set @sql = @sql + ' AND  Rc.[CompanyName] LIKE ''%' + @CompanyName + '%'''
	if (len(rtrim(@RegistrationNo)) > 0) set @sql = @sql + ' AND  Rc.[RegistrationNo] LIKE ''%' + @RegistrationNo + '%'''	
	if (ISNULL(@RegistrationStatus,'') > 0) set @sql = @sql + ' AND  Rc.[RegistrationStatus] = ISNULL(' + CAST(@RegistrationStatus AS varchar(6)) + ',Rc.RegistrationStatus)'
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Rc.CreatedOn,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Rc.CreatedOn)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Rc.CreatedOn,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Rc.CreatedOn)'
	if (len(rtrim(@Email)) > 0) set @sql = @sql + ' AND  RegUsr.[Email] LIKE ''%' + @Email + '%'''

	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanySearch] 
-- ========================================================================================================================================

GO

