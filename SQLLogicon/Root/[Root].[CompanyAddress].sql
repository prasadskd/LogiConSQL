-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyAddress] Record based on [CompanyAddress] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyAddressSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressSelect] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressSelect] 
    @CompanyID INT
AS 

BEGIN

	SELECT	[CompanyID], [Address1], [Address2], [Address3], [City], [State], [PostCode], [CountryCode], [ContactPerson1], 
			[Designation1], [Phone1], [Extension1], [Mobile1], [ContactEmailID1], [ContactPerson2], [Designation2], [Phone2], 
			[Extension2], [Mobile2], [ContactEmailID2] ,
			[Phone1CountryCode],[Mobile1CountryCode],[Phone2CountryCode],[Mobile2CountryCode]

	FROM	[Root].[CompanyAddress]
	WHERE	[CompanyID] = @CompanyID  
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyAddress] Records from [CompanyAddress] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyAddressList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressList] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressList] 
    @CompanyID INT

AS 
BEGIN

	SELECT	[CompanyID], [Address1], [Address2], [Address3], [City], [State], [PostCode], [CountryCode], [ContactPerson1], 
			[Designation1], [Phone1], [Extension1], [Mobile1], [ContactEmailID1], [ContactPerson2], [Designation2], [Phone2], [Extension2], 
			[Mobile2], [ContactEmailID2] ,
			[Phone1CountryCode],[Mobile1CountryCode],[Phone2CountryCode],[Mobile2CountryCode]
	FROM	[Root].[CompanyAddress]
	Where CompanyID = @CompanyID
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyAddress] Records from [CompanyAddress] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyAddressPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressPageView] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressPageView] 
    @CompanyID INT,
	@fetchrows bigint
AS 
BEGIN

	SELECT	[CompanyID], [Address1], [Address2], [Address3], [City], [State], [PostCode], [CountryCode], [ContactPerson1], [Designation1], 
			[Phone1], [Extension1], [Mobile1], [ContactEmailID1], [ContactPerson2], [Designation2], [Phone2], [Extension2], [Mobile2], [ContactEmailID2] ,
			[Phone1CountryCode],[Mobile1CountryCode],[Phone2CountryCode],[Mobile2CountryCode]
	FROM	[Root].[CompanyAddress]
	Where CompanyID = @CompanyID
	ORDER BY Address1
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanyAddress] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyAddressRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressRecordCount] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressRecordCount] 
    @CompanyID INT

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[CompanyAddress]
	WHERE  [CompanyID] = @CompanyID 

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyAddress] Record based on [CompanyAddress] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyAddressAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressAutoCompleteSearch] 
    @CompanyID INT
AS 

BEGIN

	SELECT	[CompanyID], [Address1], [Address2], [Address3], [City], [State], [PostCode], [CountryCode], [ContactPerson1], 
			[Designation1], [Phone1], [Extension1], [Mobile1], [ContactEmailID1], [ContactPerson2], [Designation2], [Phone2], [Extension2], 
			[Mobile2], [ContactEmailID2] ,
			[Phone1CountryCode],[Mobile1CountryCode],[Phone2CountryCode],[Mobile2CountryCode]
	FROM	[Root].[CompanyAddress]
	WHERE  [CompanyID] = @CompanyID 
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanyAddress] Record Into [CompanyAddress] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyAddressInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressInsert] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressInsert] 
    @CompanyID int,
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @City nvarchar(100),
    @State nvarchar(100),
    @PostCode nvarchar(10),
    @CountryCode nvarchar(3),
    @ContactPerson1 nvarchar(50),
    @Designation1 nvarchar(50),
    @Phone1 nvarchar(20),
    @Extension1 nvarchar(5),
    @Mobile1 nvarchar(20),
    @ContactEmailID1 nvarchar(50),
    @ContactPerson2 nvarchar(50),
    @Designation2 nvarchar(50),
    @Phone2 nvarchar(20),
    @Extension2 nvarchar(5),
    @Mobile2 nvarchar(20),
    @ContactEmailID2 nvarchar(50),
	@Phone1CountryCode nvarchar(3),
	@Mobile1CountryCode nvarchar(3),
	@Phone2CountryCode nvarchar(3),
	@Mobile2CountryCode nvarchar(3)
AS 
  

BEGIN

	
	INSERT INTO [Root].[CompanyAddress] (
			CompanyID,[Address1], [Address2], [Address3], [City], [State], [PostCode], [CountryCode], [ContactPerson1], [Designation1], 
			[Phone1], [Extension1], [Mobile1], [ContactEmailID1], [ContactPerson2], [Designation2], [Phone2], [Extension2], 
			[Mobile2], [ContactEmailID2],[Phone1CountryCode],[Mobile1CountryCode],[Phone2CountryCode],[Mobile2CountryCode])
	SELECT	@CompanyID,@Address1, @Address2, @Address3, @City, @State, @PostCode, @CountryCode, @ContactPerson1, @Designation1, 
			@Phone1, @Extension1, @Mobile1, @ContactEmailID1, @ContactPerson2, @Designation2, @Phone2, @Extension2, 
			@Mobile2, @ContactEmailID2,@Phone1CountryCode,@Mobile1CountryCode,@Phone2CountryCode,@Mobile2CountryCode
               
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanyAddress] Record Into [CompanyAddress] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyAddressUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressUpdate] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressUpdate] 
    @CompanyID int,
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @City nvarchar(100),
    @State nvarchar(100),
    @PostCode nvarchar(10),
    @CountryCode nvarchar(3),
    @ContactPerson1 nvarchar(50),
    @Designation1 nvarchar(50),
    @Phone1 nvarchar(20),
    @Extension1 nvarchar(5),
    @Mobile1 nvarchar(20),
    @ContactEmailID1 nvarchar(50),
    @ContactPerson2 nvarchar(50),
    @Designation2 nvarchar(50),
    @Phone2 nvarchar(20),
    @Extension2 nvarchar(5),
    @Mobile2 nvarchar(20),
    @ContactEmailID2 nvarchar(50),
	@Phone1CountryCode nvarchar(3),
	@Mobile1CountryCode nvarchar(3),
	@Phone2CountryCode nvarchar(3),
	@Mobile2CountryCode nvarchar(3)

AS 
 
	
BEGIN

	UPDATE	[Root].[CompanyAddress]
	SET		[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [City] = @City, [State] = @State, [PostCode] = @PostCode, 
			[CountryCode] = @CountryCode, [ContactPerson1] = @ContactPerson1, [Designation1] = @Designation1, [Phone1] = @Phone1, 
			[Extension1] = @Extension1, [Mobile1] = @Mobile1, [ContactEmailID1] = @ContactEmailID1, [ContactPerson2] = @ContactPerson2, 
			[Designation2] = @Designation2, [Phone2] = @Phone2, [Extension2] = @Extension2, [Mobile2] = @Mobile2, [ContactEmailID2] = @ContactEmailID2,
			[Phone1CountryCode]= @Phone1CountryCode,[Mobile1CountryCode]=@Mobile1CountryCode,
			[Phone2CountryCode]= @Phone2CountryCode,[Mobile2CountryCode]=@Mobile2CountryCode
	WHERE	[CompanyID] = @CompanyID
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanyAddress] Record Into [CompanyAddress] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyAddressSave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressSave] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressSave] 
    @CompanyID int,
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @City nvarchar(100),
    @State nvarchar(100),
    @PostCode nvarchar(10),
    @CountryCode nvarchar(3),
    @ContactPerson1 nvarchar(50),
    @Designation1 nvarchar(50),
    @Phone1 nvarchar(20),
    @Extension1 nvarchar(5),
    @Mobile1 nvarchar(20),
    @ContactEmailID1 nvarchar(50),
    @ContactPerson2 nvarchar(50),
    @Designation2 nvarchar(50),
    @Phone2 nvarchar(20),
    @Extension2 nvarchar(5),
    @Mobile2 nvarchar(20),
    @ContactEmailID2 nvarchar(50),
	@Phone1CountryCode nvarchar(3),
	@Mobile1CountryCode nvarchar(3),
	@Phone2CountryCode nvarchar(3),
	@Mobile2CountryCode nvarchar(3)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Root].[CompanyAddress] 
		WHERE 	[CompanyID] = @CompanyID)>0
	BEGIN
	    Exec [Root].[usp_CompanyAddressUpdate] 
		@CompanyID, @Address1, @Address2, @Address3, @City, @State, @PostCode, @CountryCode, @ContactPerson1, @Designation1, 
		@Phone1, @Extension1, @Mobile1, @ContactEmailID1, @ContactPerson2, @Designation2, @Phone2, @Extension2, @Mobile2, @ContactEmailID2,
		@Phone1CountryCode,@Mobile1CountryCode,@Phone2CountryCode,@Mobile2CountryCode

	END
	ELSE
	BEGIN
	    Exec [Root].[usp_CompanyAddressInsert] 
		@CompanyID, @Address1, @Address2, @Address3, @City, @State, @PostCode, @CountryCode, @ContactPerson1, @Designation1, @Phone1, 
		@Extension1, @Mobile1, @ContactEmailID1, @ContactPerson2, @Designation2, @Phone2, @Extension2, @Mobile2, @ContactEmailID2,
		@Phone1CountryCode,@Mobile1CountryCode,@Phone2CountryCode,@Mobile2CountryCode

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[CompanyAddressSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyAddressDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanyAddress] Record  based on [CompanyAddress]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyAddressDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyAddressDelete] 
END 
GO
CREATE PROC [Root].[usp_CompanyAddressDelete] 
    @CompanyID int
AS 

	
BEGIN

	
	DELETE
	FROM   [Root].[CompanyAddress]
	WHERE  [CompanyID] = @CompanyID
	 


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyAddressDelete]
-- ========================================================================================================================================

GO
 