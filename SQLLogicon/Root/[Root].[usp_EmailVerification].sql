
-- ========================================================================================================================================
-- START											 [Root].[usp_EmailVerification]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	21-Nov-2016
-- Description:	Insert Registered User Details
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_EmailVerification]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_EmailVerification]
END 
GO
CREATE PROC  [Root].[usp_EmailVerification]
	@EmailToken nvarchar(50)
As
Begin
 
 Declare @result int =0,
		 @userID nvarchar(50),
		 @activityDate datetime,
		 @vregistrationStatus smallint,
		 @vEmailVerifiedStatus smallint,
		 @vCompanyID int

 if (Select Count(0) From Root.Registration Where EmailToken = @EmailToken and IsEmailVerified = Cast(0 as bit)) >0
 	Begin
		Update Root.Registration Set IsEmailVerified = 1, VerifiedOn = GETUTCDATE()  Where EmailToken = @EmailToken 
		Set @result =1;

		/* Insert a Record into Activity Table. */

		Select @userID = UserID From [Root].[Registration] Where EmailToken = @EmailToken
		Select @vCompanyID = CompanyID From [Root].[RegisteredCompany] Where UserID = @userID


		Select @activityDate = GETUTCDATE(),@vEmailVerifiedStatus = LookupID 
		From Config.Lookup 
		where LookupCategory ='RegistrationStatus' And LookupDescription ='Email Verified';
	
		Exec [Root].[usp_CompanyRegistrationActivityInsert]	@vCompanyID, @vEmailVerifiedStatus, @activityDate, @UserID
		
		Select @activityDate = GETUTCDATE(),@vregistrationStatus = LookupID 
		From Config.Lookup 
		where LookupCategory ='RegistrationStatus' And LookupDescription ='Awaiting Verification';
	
		Exec [Root].[usp_CompanyRegistrationActivityInsert]	@vCompanyID, @vregistrationStatus, @activityDate, @UserID

		Update [Root].[RegisteredCompany] Set RegistrationStatus = @vregistrationStatus Where CompanyID = @vCompanyID
	End
	else
	Begin
		RaisError('EMAIL ALREADY VERIFIED OR INVALID EMAILTOKEN',16,1)
	End

 Select @result;

End
GO

