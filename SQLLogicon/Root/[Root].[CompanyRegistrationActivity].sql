-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivitySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationActivity] Record based on [CompanyRegistrationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivitySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivitySelect] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivitySelect] 
    @CompanyID INT,
    @RegistrationStatus SMALLINT,
    @ActivityDate DATETIME
AS 

BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT [CompanyID], [RegistrationStatus], [ActivityDate], [CreatedBy] ,
	ISNULL(RC.LookupDescription,'') As  RegistrationDocTypeDescription
	FROM   [Root].[CompanyRegistrationActivity] CA
	Left Outer Join RegistrationDocTypeDescription RC ON 
		CA.RegistrationStatus = RC.LookupID
	WHERE  [CompanyID] = @CompanyID  
	       AND [RegistrationStatus] = @RegistrationStatus  
	       AND [ActivityDate] = @ActivityDate
	Order By  CA.[ActivityDate]
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivitySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationActivity] Records from [CompanyRegistrationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityList] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityList] 
    @CompanyID INT

AS 
BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT [CompanyID], [RegistrationStatus], CONVERT(VARCHAR(24), CONVERT(DATETIME, [ActivityDate], 103), 121), [CreatedBy] ,
	ISNULL(RC.LookupDescription,'') As  RegistrationDocTypeDescription
	FROM   [Root].[CompanyRegistrationActivity] CA
	Left Outer Join RegistrationDocTypeDescription RC ON 
		CA.RegistrationStatus = RC.LookupID
	WHERE  [CompanyID] = @CompanyID  
	Order By  CA.[ActivityDate]


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationActivity] Records from [CompanyRegistrationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityPageView] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityPageView] 
    @CompanyID INT,
	@fetchrows bigint
AS 
BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT [CompanyID], [RegistrationStatus], [ActivityDate], [CreatedBy] ,
	ISNULL(RC.LookupDescription,'') As  RegistrationDocTypeDescription
	FROM   [Root].[CompanyRegistrationActivity] CA
	Left Outer Join RegistrationDocTypeDescription RC ON 
		CA.RegistrationStatus = RC.LookupID
	WHERE  [CompanyID] = @CompanyID  
	ORDER BY [ActivityDate]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanyRegistrationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityRecordCount] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityRecordCount] 
    @CompanyID INT

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[CompanyRegistrationActivity]
	WHERE  [CompanyID] = @CompanyID  


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationActivity] Record based on [CompanyRegistrationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityAutoCompleteSearch] 
    @CompanyID INT 
AS 

BEGIN

	;With RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT [CompanyID], [RegistrationStatus], [ActivityDate], [CreatedBy] ,
	ISNULL(RC.LookupDescription,'') As  RegistrationDocTypeDescription
	FROM   [Root].[CompanyRegistrationActivity] CA
	Left Outer Join RegistrationDocTypeDescription RC ON 
		CA.RegistrationStatus = RC.LookupID
	WHERE  [CompanyID] = @CompanyID  
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanyRegistrationActivity] Record Into [CompanyRegistrationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityInsert] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityInsert] 
    @CompanyID int,
    @RegistrationStatus smallint,
    @ActivityDate datetime,
    @CreatedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Root].[CompanyRegistrationActivity] (
			[CompanyID], [RegistrationStatus], [ActivityDate], [CreatedBy])
	SELECT	@CompanyID, @RegistrationStatus, @ActivityDate, @CreatedBy
               
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanyRegistrationActivity] Record Into [CompanyRegistrationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityUpdate] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityUpdate] 
    @CompanyID int,
    @RegistrationStatus smallint,
    @ActivityDate datetime,
    @CreatedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Root].[CompanyRegistrationActivity]
	SET    [CompanyID] = @CompanyID, [RegistrationStatus] = @RegistrationStatus, [ActivityDate] = @ActivityDate, [CreatedBy] = @CreatedBy
	WHERE  [CompanyID] = @CompanyID
	       AND [RegistrationStatus] = @RegistrationStatus
	       AND [ActivityDate] = @ActivityDate
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivitySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanyRegistrationActivity] Record Into [CompanyRegistrationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivitySave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivitySave] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivitySave] 
    @CompanyID int,
    @RegistrationStatus smallint,
    @ActivityDate datetime,
    @CreatedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Root].[CompanyRegistrationActivity] 
		WHERE 	[CompanyID] = @CompanyID
	       AND [RegistrationStatus] = @RegistrationStatus
	       AND [ActivityDate] = @ActivityDate)>0
	BEGIN
	    Exec [Root].[usp_CompanyRegistrationActivityUpdate] 
		@CompanyID, @RegistrationStatus, @ActivityDate, @CreatedBy


	END
	ELSE
	BEGIN
	    Exec [Root].[usp_CompanyRegistrationActivityInsert] 
		@CompanyID, @RegistrationStatus, @ActivityDate, @CreatedBy


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[CompanyRegistrationActivitySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationActivityDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanyRegistrationActivity] Record  based on [CompanyRegistrationActivity]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationActivityDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationActivityDelete] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationActivityDelete] 
    @CompanyID int 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Root].[CompanyRegistrationActivity]
	WHERE  [CompanyID] = @CompanyID
	        

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationActivityDelete]
-- ========================================================================================================================================

GO
 
