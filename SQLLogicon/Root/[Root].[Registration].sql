-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Registration] Record based on [Registration] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegistrationSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationSelect] 
END 
GO
CREATE PROC [Root].[usp_RegistrationSelect] 
    @UserID INT
AS 

BEGIN

	SELECT [UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], [IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy],
	[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM   [Root].[Registration]
	WHERE  [UserID] = @UserID 
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationSelect]
-- ========================================================================================================================================

GO

IF OBJECT_ID('[Root].[usp_RegistrationSelectByEmail]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationSelectByEmail] 
END 
GO
CREATE PROC [Root].[usp_RegistrationSelectByEmail] 
    @Email varchar(50)
AS 

BEGIN

	SELECT [UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], [IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy],
	[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM   [Root].[Registration]
	WHERE  [Email] = @Email 
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Registration] Records from [Registration] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegistrationList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationList] 
END 
GO
CREATE PROC [Root].[usp_RegistrationList] 

AS 
BEGIN

	SELECT [UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], [IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy] 
	,[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM   [Root].[Registration]

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Registration] Records from [Registration] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegistrationPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationPageView] 
END 
GO
CREATE PROC [Root].[usp_RegistrationPageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], [IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy] 
	,[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM   [Root].[Registration]
	ORDER BY [UserID]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Registration] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegistrationRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationRecordCount] 
END 
GO
CREATE PROC [Root].[usp_RegistrationRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[Registration]
	

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Registration] Record based on [Registration] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegistrationAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_RegistrationAutoCompleteSearch] 
    @Email   nvarchar(60)
AS 

BEGIN

	SELECT [UserID], [Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC], [EmailToken], [IsEmailVerified], [VerifiedOn], [IsEmailSent], [EmailOn], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy] 
	,[MobileNo],[OTPNo],[OTPSentDate],[OTPSentCount],[IsOTPSent],[IsOTPReSent],[CountryCode], [IsOTPVerified]
	FROM   [Root].[Registration]
	WHERE  Email like '%' + @Email + '%'
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Registration] Record Into [Registration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegistrationInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationInsert] 
END 
GO
CREATE PROC [Root].[usp_RegistrationInsert] 
    @Email nvarchar(60),
    @Password nvarchar(50),
    @FullName nvarchar(50),
    @SecurityQuestion smallint,
    @Answer nvarchar(200),
    @NRIC nvarchar(50),
	@EmailToken nvarchar(50),
	@MobileNo nvarchar(20),
	@CountryCode varchar(2),
    @CreatedBy nvarchar(15),
    @ModifiedBy nvarchar(15),
	@OTPNo nvarchar(5),
	@IsOTPSent bit,
	@IsOTPSendDate datetime,
	@IsOTPVerified bit
AS 
  

BEGIN

	
	INSERT INTO [Root].[Registration] (
			[Email], [Password], [FullName], [SecurityQuestion], [Answer], [NRIC],EmailToken, [CreatedBy], [CreatedOn],[MobileNo],[CountryCode], [OTPNo], [IsOTPSent], [OTPSentDate], [IsOTPVerified])
	SELECT @Email, @Password, @FullName, @SecurityQuestion, @Answer, @NRIC,@EmailToken, @CreatedBy, GetUTCDate(),@MobileNo,@CountryCode, @OTPNo, Cast(1 as bit), GETUTCDATE(), @IsOTPVerified
               
	 
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Registration] Record Into [Registration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegistrationUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationUpdate] 
END 
GO
CREATE PROC [Root].[usp_RegistrationUpdate] 
    @Email nvarchar(60),
    @FullName nvarchar(50),
    @SecurityQuestion smallint,
    @Answer nvarchar(200),
	@MobileNo nvarchar(20),
	@CountryCode varchar(2),
    @CreatedBy nvarchar(15),
    @ModifiedBy nvarchar(15) 
AS 
 
	
BEGIN

	UPDATE [Root].[Registration]
	SET    [FullName] = @FullName, [SecurityQuestion] = @SecurityQuestion, [Answer] = @Answer, [ModifiedOn] = GetUTCDate(), [ModifiedBy] = @ModifiedBy
	,MobileNo = @MobileNo,	CountryCode=@CountryCode
	WHERE  Email = @Email

	 
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Registration] Record Into [Registration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegistrationSave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationSave] 
END 
GO
CREATE PROC [Root].[usp_RegistrationSave] 
    @Email nvarchar(60),
    @Password nvarchar(50),
    @FullName nvarchar(50),
    @SecurityQuestion smallint,
    @Answer nvarchar(200),
    @NRIC nvarchar(50),
	@MobileNo nvarchar(20),
	@CountryCode varchar(2),
    @CreatedBy nvarchar(15),
    @ModifiedBy nvarchar(15),
	@OTPNo nvarchar(5),
	@IsOTPSent bit,
	@IsOTPSendDate datetime,
	@IsOTPVerified bit,
	@UserID INT OUTPUT
AS 
 

BEGIN
	Declare @EmailToken nvarchar(50)

	IF (SELECT COUNT(0) FROM [Root].[Registration] 
		WHERE 	Email = @Email And Status=Cast(1 as bit))>0
	BEGIN

	    Exec [Root].[usp_RegistrationUpdate] 
			@Email, @FullName, @SecurityQuestion, @Answer,@MobileNo,@CountryCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
		
		Set @EmailToken = NEWID()

	    Exec [Root].[usp_RegistrationInsert] 
			@Email, @Password, @FullName, @SecurityQuestion, @Answer, @NRIC,@EmailToken,@MobileNo,@CountryCode, @CreatedBy, @ModifiedBy, @OTPNo, @IsOTPSent, @IsOTPSendDate, @IsOTPVerified

	END

	Select @UserID = UserID From [Root].[Registration] Where Email = @Email
	
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[RegistrationSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Root].[usp_RegistrationDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Registration] Record  based on [Registration]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegistrationDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationDelete] 
END 
GO
CREATE PROC [Root].[usp_RegistrationDelete] 
    @UserID int
AS 

	
BEGIN

	UPDATE	[Root].[Registration]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[UserID] = @UserID

	 


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationDelete]
-- ========================================================================================================================================

GO

IF OBJECT_ID('[Root].[usp_RegistrationUpdateIsOTPVerified]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationUpdateIsOTPVerified] 
END 
GO
CREATE PROC [Root].[usp_RegistrationUpdateIsOTPVerified] 
    @UserID int
AS 

	
BEGIN

	UPDATE	[Root].[Registration]
	SET	[IsOTPVerified] = CAST(1 as bit)
	WHERE 	[UserID] = @UserID

	 


END



GO

IF OBJECT_ID('[Root].[usp_RegistrationUpdateOTPInfo]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegistrationUpdateOTPInfo] 
END 
GO
CREATE PROC [Root].[usp_RegistrationUpdateOTPInfo] 
    @Email varchar(50),
	@OTPNo varchar(10),
	@IsOTPSent bit,
	@OTPSentDate datetime,
	@IsOTPVerified bit
AS 

BEGIN
	UPDATE [Root].[Registration]
	SET [OTPNo] = @OTPNo, 
	    [IsOTPSent] = @IsOTPSent, 
		[OTPSentDate] = @OTPSentDate, 
		[IsOTPVerified] = @IsOTPVerified
	WHERE  [Email] = @Email 
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegistrationSelect]
-- ========================================================================================================================================

GO

IF OBJECT_ID('[Root].[usp_UpdateOTPInfo]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_UpdateOTPInfo] 
END 
GO
 Create Procedure [Root].[usp_UpdateOTPInfo]
 @Email varchar(50),
 @OTPNo varchar(10),
 @IsOTPSent bit,
 @OTPSentDate datetime,
 @IsOTPVerified bit
 AS
 BEGIN
	IF (SELECT COUNT(0) FROM [Security].[User] WHERE UserID = @Email) > 0
		BEGIN
			--PRINT '[Security].[User]'

			UPDATE [Security].[User] SET OTPNo = @OTPNo, IsOTPSent = @IsOTPSent, OTPSentDate = @OTPSentDate, IsOTPVerified = @IsOTPVerified WHERE UserID = @Email
		END
	ELSE IF (SELECT COUNT(0) FROM [Root].[Registration] WHERE Email = @Email) > 0
		BEGIN
			--PRINT '[Root].[Registration]'
			UPDATE [Root].[Registration] SET OTPNo = @OTPNo, IsOTPSent = @IsOTPSent, OTPSentDate = @OTPSentDate, IsOTPVerified = @IsOTPVerified WHERE Email = @Email
		END
 END

 GO


IF OBJECT_ID('[Root].[usp_VerifyOTP]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_VerifyOTP] 
END 
GO
CREATE PROC [Root].[usp_VerifyOTP]
@Email varchar(60),
@OTP varchar(10)
As
BEGIN
	IF (SELECT COUNT(0) FROM [Security].[User] WHERE UserID = @Email AND OTPNo = @OTP) > 0
		BEGIN
			--PRINT '[Security].[User]'
			UPDATE [Security].[User] SET IsOTPVerified = CAST(1 AS BIT) WHERE UserID = @Email
		END
	ELSE IF (SELECT COUNT(0) FROM [Root].[Registration] WHERE Email = @Email AND OTPNo = @OTP) > 0
		BEGIN
			--PRINT '[Root].[Registration]'
			UPDATE [Root].[Registration] SET IsOTPVerified = CAST(1 AS BIT) WHERE Email = @Email
		END
END


GO


IF OBJECT_ID('[Root].[usp_PasswordUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_PasswordUpdate]  
END 
GO
CREATE PROC [Root].[usp_PasswordUpdate] 
    @Email nvarchar(60),
    @Password nvarchar(50),
    @ModifiedBy nvarchar(15) 
AS 
 
	
BEGIN
    
	IF (SELECT COUNT (0) FROM [Security].[User] Where UserID = @Email) > 0
		BEGIN
		 UPDATE [Security].[User] Set [Password] = @Password, [ModifiedOn] = GetUTCDate(), [ModifiedBy] = @ModifiedBy WHERE  UserID = @Email
		END
	ELSE IF (SELECT COUNT (0) FROM [Root].[Registration] Where Email = @Email) > 0
		BEGIN
			UPDATE [Root].[Registration] SET [Password] = @Password, [ModifiedOn] = GetUTCDate(), [ModifiedBy] = @ModifiedBy WHERE  Email = @Email
		END
END

 
