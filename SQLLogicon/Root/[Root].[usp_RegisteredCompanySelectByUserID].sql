
-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanySelectByUserID]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	21-Nov-2016
-- Description:	Insert Registered User Details
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanySelectByUserID]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanySelectByUserID]
END 
GO
CREATE PROC   [Root].[usp_RegisteredCompanySelectByUserID] 
    @UserID INT
AS 
BEGIN

	;with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OrganizationType'),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID
 WHERE Rc.[UserID] = @UserID

END

-- ========================================================================================================================================
-- END              [Root].[usp_RegisteredCompanySelect]
-- ========================================================================================================================================
