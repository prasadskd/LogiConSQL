
-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [RegisteredCompany] Record based on [RegisteredCompany] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanySelect] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanySelect] 
    @CompanyID INT
     
AS 

BEGIN

	

	;with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OrganizationType'),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID
	WHERE	[CompanyID] = @CompanyID
	        
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompany] Records from [RegisteredCompany] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyList] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyList] 

AS 
BEGIN

	;with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OrganizationType'),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegisteredCompany] Records from [RegisteredCompany] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyPageView] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyPageView]
	@RegistrationStatus smallint,
	@fetchrows bigint
AS 
BEGIN

	;with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OrganizationType'),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID
	Where Rc.RegistrationStatus = ISNULL(@RegistrationStatus,Rc.RegistrationStatus)
	ORDER BY CompanyName
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [RegisteredCompany] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyRecordCount] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyRecordCount] 
	@RegistrationStatus smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[RegisteredCompany]
	Where RegistrationStatus = ISNULL(@RegistrationStatus,RegistrationStatus)

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [RegisteredCompany] Record based on [RegisteredCompany] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_RegisteredCompanyAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyAutoCompleteSearch] 
	@CompanyName nvarchar(255)  
AS 

BEGIN

	;with OrganisationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OrganizationType'),
	RegistrationStatusDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationStatus')
	SELECT	Rc.[CompanyID], Rc.[RegistrationNo],Rc.[GSTNo],Rc.[GSTRegistrationDate], Rc.[CompanyName], Rc.[OrganisationType],Rc.[AgentName], Rc.[UserID], 
			Rc.[CPAMAccountNo], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn], Rc.[RegistrationStatus],Rc.[IsDeclared],
			Rc.[DeclaredDate],Rc.[Remarks],
			ISNULL(OT.LookupDescription,'') As OrganisationTypeDescription,
			ISNULL(RST.LookupDescription,'') As RegistrationStatusDescription, RegUsr.Email As RegisteredEmailID
	FROM	[Root].[RegisteredCompany] Rc
	Left Outer Join OrganisationTypeDescription OT ON
		Rc.OrganisationType = OT.LookupID
	Left Outer Join RegistrationStatusDescription RST ON
		Rc.RegistrationStatus = RST.LookupID
	Left Outer Join [Root].[Registration] RegUsr ON 
		Rc.UserID = RegUsr.UserID
	WHERE  Rc.CompanyName LIKE '%' + @CompanyName + '%'
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyAutoCompleteSearch]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [RegisteredCompany] Record Into [RegisteredCompany] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyInsert] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyInsert] 
    @RegistrationNo nvarchar(50),
	@GSTNo nvarchar(20),
	@GSTRegistrationDate datetime,
    @CompanyName nvarchar(255),
    @OrganisationType smallint,
	@AgentName nvarchar(100),
    @UserID int,
    @CPAMAccountNo nvarchar(50),
    @CreatedBy nvarchar(20),
    @ModifiedBy nvarchar(20),
    @RegistrationStatus smallint,
	@IsDeclared bit 
	
AS 
  

BEGIN
	
	INSERT INTO [Root].[RegisteredCompany] (
			[RegistrationNo], [CompanyName], [GSTNo],[GSTRegistrationDate], [OrganisationType],[AgentName], [UserID], [CPAMAccountNo], 
			[CreatedBy], [CreatedOn], [RegistrationStatus],IsDeclared,DeclaredDate)
	SELECT	@RegistrationNo, @CompanyName,@GSTNo,@GSTRegistrationDate, @OrganisationType,@AgentName, @UserID, @CPAMAccountNo, 
			@CreatedBy, GETUTCDATE(), @RegistrationStatus,@IsDeclared,GETUTCDATE()


	

               
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [RegisteredCompany] Record Into [RegisteredCompany] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyUpdate] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyUpdate] 
    @RegistrationNo nvarchar(50),
	@GSTNo nvarchar(20),
	@GSTRegistrationDate datetime,
    @CompanyName nvarchar(255),
    @OrganisationType smallint,
	@AgentName nvarchar(100),
    @UserID int,
    @CPAMAccountNo nvarchar(50),
    @CreatedBy nvarchar(20),
    @ModifiedBy nvarchar(20),
    @RegistrationStatus smallint,
	@IsDeclared bit
AS 
 
	
BEGIN

	UPDATE	[Root].[RegisteredCompany]
	SET		[CompanyName] = @CompanyName, [GSTNo] = @GSTNo,[GSTRegistrationDate] = @GSTRegistrationDate,[OrganisationType] = @OrganisationType, [UserID] = @UserID, 
			[CPAMAccountNo] = @CPAMAccountNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(), [RegistrationStatus] = @RegistrationStatus,
			[AgentName]= @AgentName
	WHERE	[RegistrationNo] = @RegistrationNo
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyUpdate]
-- ========================================================================================================================================

GO





-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [RegisteredCompany] Record Into [RegisteredCompany] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanySave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanySave] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanySave] 
    @CompanyID int,
    @RegistrationNo nvarchar(50),
	@GSTNo nvarchar(20),
	@GSTRegistrationDate datetime,
    @CompanyName nvarchar(255),
    @OrganisationType smallint,
	@AgentName nvarchar(100),
    @UserID int,
    @CPAMAccountNo nvarchar(50),
    @CreatedBy nvarchar(20),
    @ModifiedBy nvarchar(20),
    @RegistrationStatus smallint,
	@IsDeclared bit,
	@NewCompanyID int OUTPUT
AS 
 

BEGIN

	Declare @vID INT,
			@isNewRecord bit=0

	IF (SELECT COUNT(0) FROM [Root].[RegisteredCompany] 
		WHERE 	[CompanyID] = @CompanyID
	       AND [RegistrationNo] = @RegistrationNo)>0
	BEGIN
	    Exec [Root].[usp_RegisteredCompanyUpdate] 
		@RegistrationNo,@GSTNo,@GSTRegistrationDate, @CompanyName, @OrganisationType,@AgentName, @UserID, @CPAMAccountNo, @CreatedBy, @ModifiedBy, @RegistrationStatus,@IsDeclared

		Select @NewCompanyID = @CompanyID

	END
	ELSE
	BEGIN

	    Exec [Root].[usp_RegisteredCompanyInsert] 
		@RegistrationNo,@GSTNo,@GSTRegistrationDate, @CompanyName, @OrganisationType,@AgentName, @UserID, @CPAMAccountNo, @CreatedBy, @ModifiedBy, @RegistrationStatus,@IsDeclared
	
		Set @isNewRecord = 1;
	END
	Select @NewCompanyID = IDENT_CURRENT('[Root].[RegisteredCompany]')
	
	Declare @activityDate datetime,
			@vregistrationStatus smallint;

	/* Insert a Record into Activity Table. */

	If @isNewRecord = CAST(1 as bit)
	Begin

	Select @activityDate = GETUTCDATE(),@vregistrationStatus = LookupID 
	From Config.Lookup 
	where LookupCategory ='RegistrationStatus' And LookupDescription ='Draft Submitted';
	
	Exec [Root].[usp_CompanyRegistrationActivityInsert]	@NewCompanyID, @vregistrationStatus, @activityDate, @UserID

	Select @activityDate = GETUTCDATE(),@vregistrationStatus = LookupID 
	From Config.Lookup 
	where LookupCategory ='RegistrationStatus' And LookupDescription ='Awaiting Email Verification';
	
	Exec [Root].[usp_CompanyRegistrationActivityInsert]	@NewCompanyID, @vregistrationStatus, @activityDate, @UserID


	Update [Root].[RegisteredCompany] Set RegistrationStatus = @vregistrationStatus Where CompanyID = @NewCompanyID

	End

	Select @NewCompanyID 
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[RegisteredCompanySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_RegisteredCompanyDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [RegisteredCompany] Record  based on [RegisteredCompany]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_RegisteredCompanyDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_RegisteredCompanyDelete] 
END 
GO
CREATE PROC [Root].[usp_RegisteredCompanyDelete] 
    @CompanyID int 
AS 

	
BEGIN

	UPDATE	[Root].[RegisteredCompany]
	SET	[RegistrationStatus] = CAST(0 as bit)
	WHERE 	[CompanyID] = @CompanyID
	        

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_RegisteredCompanyDelete]
-- ========================================================================================================================================

GO
 
  