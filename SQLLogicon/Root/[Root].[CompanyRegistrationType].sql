
-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationType] Record based on [CompanyRegistrationType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeSelect] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeSelect] 
    @CompanyID INT,
    @RegistrationType SMALLINT
AS 

BEGIN

	;With RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	SELECT RT.[CompanyID], RT.[RegistrationType],ISNULL(RTD.LookupDescription,'') As RegistrationTypeDescription 
	FROM   [Root].[CompanyRegistrationType] RT
	Left Outer Join RTD ON 
		Rt.RegistrationType = RTD.LookupID
	WHERE  [CompanyID] = @CompanyID  
	       AND [RegistrationType] = @RegistrationType  
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationType] Records from [CompanyRegistrationType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeList]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeList] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeList] 
	@CompanyID int
AS 
BEGIN

	;With RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	SELECT RT.[CompanyID], RT.[RegistrationType],ISNULL(RTD.LookupDescription,'') As RegistrationTypeDescription 
	FROM   [Root].[CompanyRegistrationType] RT
	Left Outer Join RTD ON 
		Rt.RegistrationType = RTD.LookupID
	Where RT.CompanyID = @CompanyID
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationType] Records from [CompanyRegistrationType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypePageView] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypePageView] 
	@CompanyID int,
	@fetchrows bigint
AS 
BEGIN

	;With RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	SELECT RT.[CompanyID], RT.[RegistrationType],ISNULL(RTD.LookupDescription,'') As RegistrationTypeDescription 
	FROM   [Root].[CompanyRegistrationType] RT
	Left Outer Join RTD ON 
		Rt.RegistrationType = RTD.LookupID
	Where RT.CompanyID = @CompanyID
	ORDER BY  RTD.LookupDescription
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanyRegistrationType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeRecordCount] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeRecordCount] 
	@CompanyID int
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Root].[CompanyRegistrationType]
	Where CompanyID = @CompanyID
	

END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationType] Record based on [CompanyRegistrationType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeAutoCompleteSearch] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeAutoCompleteSearch] 
    @CompanyID INT,
    @RegistrationTypeDescription nvarchar(50)
AS 

BEGIN

	;With RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	SELECT RT.[CompanyID], RT.[RegistrationType],ISNULL(RTD.LookupDescription,'') As RegistrationTypeDescription 
	FROM   [Root].[CompanyRegistrationType] RT
	Left Outer Join RTD ON 
		Rt.RegistrationType = RTD.LookupID
	WHERE  [CompanyID] = @CompanyID  
	       AND RTD.[LookupDescription] Like '%' +  @RegistrationTypeDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanyRegistrationType] Record Into [CompanyRegistrationType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeInsert] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeInsert] 
    @CompanyID int,
    @RegistrationType smallint
AS 
  

BEGIN

	
	INSERT INTO [Root].[CompanyRegistrationType] ([CompanyID], [RegistrationType])
	SELECT @CompanyID, @RegistrationType
               
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanyRegistrationType] Record Into [CompanyRegistrationType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeUpdate] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeUpdate] 
    @CompanyID int,
    @RegistrationType smallint
AS 
 
	
BEGIN

	UPDATE [Root].[CompanyRegistrationType]
	SET    [CompanyID] = @CompanyID, [RegistrationType] = @RegistrationType
	WHERE  [CompanyID] = @CompanyID
	       AND [RegistrationType] = @RegistrationType
	
END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanyRegistrationType] Record Into [CompanyRegistrationType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeSave] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeSave] 
    @CompanyID int,
    @RegistrationType smallint
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Root].[CompanyRegistrationType] 
		WHERE 	[CompanyID] = @CompanyID
	       AND [RegistrationType] = @RegistrationType)>0
	BEGIN
	    Exec [Root].[usp_CompanyRegistrationTypeUpdate] 
		@CompanyID, @RegistrationType


	END
	ELSE
	BEGIN
	    Exec [Root].[usp_CompanyRegistrationTypeInsert] 
		@CompanyID, @RegistrationType


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Root].usp_[CompanyRegistrationTypeSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Root].[usp_CompanyRegistrationTypeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanyRegistrationType] Record  based on [CompanyRegistrationType]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_CompanyRegistrationTypeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_CompanyRegistrationTypeDelete] 
END 
GO
CREATE PROC [Root].[usp_CompanyRegistrationTypeDelete] 
    @CompanyID int 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Root].[CompanyRegistrationType]
	WHERE  [CompanyID] = @CompanyID
	       


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_CompanyRegistrationTypeDelete]
-- ========================================================================================================================================

GO
 