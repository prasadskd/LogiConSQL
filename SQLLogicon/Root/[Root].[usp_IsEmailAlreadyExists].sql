
-- ========================================================================================================================================
-- START											 [Root].[usp_IsEmailAlreadyExists]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	21-Nov-2016
-- Description:	Insert Registered User Details
-- ========================================================================================================================================


IF OBJECT_ID('[Root].[usp_IsEmailAlreadyExists]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_IsEmailAlreadyExists] 
END 
GO
CREATE PROC [Root].[usp_IsEmailAlreadyExists]
@Email nvarchar(50)
As
Begin
	Select COUNT(0) From Root.Registration
	Where Email = @Email	
End
GO

