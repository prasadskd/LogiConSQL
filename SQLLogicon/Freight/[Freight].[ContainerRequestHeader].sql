
-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerRequestHeader] Record based on [ContainerRequestHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderSelect] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderSelect] 
    @BranchID SMALLINT,
    @RequestNo VARCHAR(20)
AS 

BEGIN

	SELECT	[BranchID], [RequestNo], [HaulierCode], [HaulierName], [JobType], [PickupFrom], [DropOffCode], 
			[RequestDate], [RequestBy], [OrderNo], [Remark], [ROTNo], [BillToCode], [BillToName], [IsBillable], 
			[IsCancel], [CancelBy], [CancelOn], [IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[ContainerRequestHeader]
	WHERE	[BranchID] = @BranchID  
	       AND [RequestNo] = @RequestNo
END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerRequestHeader] Records from [ContainerRequestHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderList] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderList] 
    @BranchID SMALLINT,
    @RequestNo VARCHAR(20)

AS 
BEGIN

	SELECT	[BranchID], [RequestNo], [HaulierCode], [HaulierName], [JobType], [PickupFrom], [DropOffCode], 
			[RequestDate], [RequestBy], [OrderNo], [Remark], [ROTNo], [BillToCode], [BillToName], [IsBillable], 
			[IsCancel], [CancelBy], [CancelOn], [IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[ContainerRequestHeader]
	WHERE	[BranchID] = @BranchID  
	       AND [RequestNo] = @RequestNo

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerRequestHeader] Records from [ContainerRequestHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderPageView] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderPageView] 
    @BranchID SMALLINT,
    @RequestNo VARCHAR(20),
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [RequestNo], [HaulierCode], [HaulierName], [JobType], [PickupFrom], [DropOffCode], 
			[RequestDate], [RequestBy], [OrderNo], [Remark], [ROTNo], [BillToCode], [BillToName], [IsBillable], 
			[IsCancel], [CancelBy], [CancelOn], [IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[ContainerRequestHeader]
	WHERE	[BranchID] = @BranchID  
	       AND [RequestNo] = @RequestNo
	ORDER BY [RequestNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerRequestHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderRecordCount] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderRecordCount] 
    @BranchID SMALLINT,
    @RequestNo VARCHAR(20)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Freight].[ContainerRequestHeader]
		WHERE	[BranchID] = @BranchID  
	       AND [RequestNo] = @RequestNo


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerRequestHeader] Record based on [ContainerRequestHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderAutoCompleteSearch] 
    @BranchID SMALLINT,
    @RequestNo VARCHAR(20)
AS 

BEGIN

	SELECT	[BranchID], [RequestNo], [HaulierCode], [HaulierName], [JobType], [PickupFrom], [DropOffCode], [RequestDate], [RequestBy], 
			[OrderNo], [Remark], [ROTNo], [BillToCode], [BillToName], [IsBillable], [IsCancel], [CancelBy], [CancelOn], [IsApproved], [ApprovedBy], [ApprovedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[ContainerRequestHeader]
	WHERE	[BranchID] = @BranchID  
	       AND [RequestNo] LIKE '%' +  @RequestNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerRequestHeader] Record Into [ContainerRequestHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderInsert] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderInsert] 
    @BranchID smallint,
    @RequestNo varchar(20),
    @HaulierCode nvarchar(10),
    @HaulierName nvarchar(255),
    @JobType smallint,
    @PickupFrom nvarchar(10),
    @DropOffCode nvarchar(10),
    @RequestDate datetime,
    @RequestBy nvarchar(50),
    @OrderNo nvarchar(50),
    @Remark nvarchar(100),
    @ROTNo nvarchar(50),
    @BillToCode nvarchar(10),
    @BillToName nvarchar(255),
    @IsBillable bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT

AS 
  

BEGIN

	
	INSERT INTO [Freight].[ContainerRequestHeader] (
			[BranchID], [RequestNo], [HaulierCode], [HaulierName], [JobType], [PickupFrom], [DropOffCode], [RequestDate], [RequestBy], 
			[OrderNo], [Remark], [ROTNo], [BillToCode], [BillToName], [IsBillable], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @RequestNo, @HaulierCode, @HaulierName, @JobType, @PickupFrom, @DropOffCode, @RequestDate, @RequestBy, 
			@OrderNo, @Remark, @ROTNo, @BillToCode, @BillToName, @IsBillable, @CreatedBy, GETUTCDATE()
       
	Select @NewDocumentNo = @RequestNo           
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerRequestHeader] Record Into [ContainerRequestHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderUpdate] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderUpdate] 
    @BranchID smallint,
    @RequestNo varchar(20),
    @HaulierCode nvarchar(10),
    @HaulierName nvarchar(255),
    @JobType smallint,
    @PickupFrom nvarchar(10),
    @DropOffCode nvarchar(10),
    @RequestDate datetime,
    @RequestBy nvarchar(50),
    @OrderNo nvarchar(50),
    @Remark nvarchar(100),
    @ROTNo nvarchar(50),
    @BillToCode nvarchar(10),
    @BillToName nvarchar(255),
    @IsBillable bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT

AS 
 
	
BEGIN

	UPDATE	[Freight].[ContainerRequestHeader]
	SET		[HaulierCode] = @HaulierCode, [HaulierName] = @HaulierName, [JobType] = @JobType, [PickupFrom] = @PickupFrom, 
			[DropOffCode] = @DropOffCode, [RequestDate] = @RequestDate, [RequestBy] = @RequestBy, [OrderNo] = @OrderNo, [Remark] = @Remark, 
			[ROTNo] = @ROTNo, [BillToCode] = @BillToCode, [BillToName] = @BillToName, [IsBillable] = @IsBillable, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [RequestNo] = @RequestNo
	
	Select @NewDocumentNo = @RequestNo

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerRequestHeader] Record Into [ContainerRequestHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderSave] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderSave] 
    @BranchID smallint,
    @RequestNo varchar(20),
    @HaulierCode nvarchar(10),
    @HaulierName nvarchar(255),
    @JobType smallint,
    @PickupFrom nvarchar(10),
    @DropOffCode nvarchar(10),
    @RequestDate datetime,
    @RequestBy nvarchar(50),
    @OrderNo nvarchar(50),
    @Remark nvarchar(100),
    @ROTNo nvarchar(50),
    @BillToCode nvarchar(10),
    @BillToName nvarchar(255),
    @IsBillable bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Freight].[ContainerRequestHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [RequestNo] = @RequestNo)>0
	BEGIN
	    Exec [Freight].[usp_ContainerRequestHeaderUpdate] 
			@BranchID, @RequestNo, @HaulierCode, @HaulierName, @JobType, @PickupFrom, @DropOffCode, @RequestDate, @RequestBy, @OrderNo, 
			@Remark, @ROTNo, @BillToCode, @BillToName, @IsBillable, @CreatedBy, @ModifiedBy ,@NewDocumentNo = @NewDocumentNo OUTPUT


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@DocID= 'Freight\ContainerRequest',@RequestNo=''
		 
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 10, 'Freight\ContainerRequest', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @OrderNo OUTPUT

		 


	    Exec [Freight].[usp_ContainerRequestHeaderInsert] 
			@BranchID, @RequestNo, @HaulierCode, @HaulierName, @JobType, @PickupFrom, @DropOffCode, @RequestDate, @RequestBy, @OrderNo, 
			@Remark, @ROTNo, @BillToCode, @BillToName, @IsBillable, @CreatedBy, @ModifiedBy ,@NewDocumentNo = @NewDocumentNo OUTPUT

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Freight].usp_[ContainerRequestHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerRequestHeader] Record  based on [ContainerRequestHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestHeaderDelete] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestHeaderDelete] 
    @BranchID smallint,
    @RequestNo varchar(20),
	@CancelBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Freight].[ContainerRequestHeader]
	SET	IsCancel = CAST(0 as bit),CancelBy = @CancelBy, CancelOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [RequestNo] = @RequestNo
 


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestHeaderDelete]
-- ========================================================================================================================================

GO
