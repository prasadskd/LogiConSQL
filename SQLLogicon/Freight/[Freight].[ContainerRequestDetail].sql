

-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerRequestDetail] Record based on [ContainerRequestDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailSelect] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailSelect] 
    @BranchID SMALLINT,
    @RequestNo NVARCHAR(20),
    @ContainerKey NVARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [RequestNo], [ContainerKey], [Remarks], [IsCancel], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Freight].[ContainerRequestDetail]
	WHERE  [BranchID] = @BranchID 
	       AND [RequestNo] = @RequestNo  
	       AND [ContainerKey] = @ContainerKey 
END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerRequestDetail] Records from [ContainerRequestDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailList] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailList] 
    @BranchID SMALLINT,
    @RequestNo NVARCHAR(20)

AS 
BEGIN

	SELECT [BranchID], [RequestNo], [ContainerKey], [Remarks], [IsCancel], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Freight].[ContainerRequestDetail]
		WHERE  [BranchID] = @BranchID 
	       AND [RequestNo] = @RequestNo  

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerRequestDetail] Records from [ContainerRequestDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailPageView] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailPageView] 
    @BranchID SMALLINT,
    @RequestNo NVARCHAR(20),
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [RequestNo], [ContainerKey], [Remarks], [IsCancel], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Freight].[ContainerRequestDetail]
	WHERE  [BranchID] = @BranchID 
	       AND [RequestNo] = @RequestNo  
	ORDER BY [ContainerKey]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerRequestDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailRecordCount] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailRecordCount] 
    @BranchID SMALLINT,
    @RequestNo NVARCHAR(20)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Freight].[ContainerRequestDetail]
	WHERE  [BranchID] = @BranchID 
	       AND [RequestNo] = @RequestNo  
	

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerRequestDetail] Record based on [ContainerRequestDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailAutoCompleteSearch] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailAutoCompleteSearch] 
    @BranchID SMALLINT,
    @RequestNo NVARCHAR(20) 
AS 

BEGIN

	SELECT [BranchID], [RequestNo], [ContainerKey], [Remarks], [IsCancel], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Freight].[ContainerRequestDetail]
	WHERE  [BranchID] = @BranchID  
	       AND [RequestNo] LIKE '%' +  @RequestNo  + '%'

END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerRequestDetail] Record Into [ContainerRequestDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailInsert] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailInsert] 
    @BranchID smallint,
    @RequestNo nvarchar(20),
    @ContainerKey nvarchar(50),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Freight].[ContainerRequestDetail] ([BranchID], [RequestNo], [ContainerKey], [Remarks], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @RequestNo, @ContainerKey, @Remarks, @CreatedBy, GETUTCDATE() 
               
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerRequestDetail] Record Into [ContainerRequestDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailUpdate] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailUpdate] 
    @BranchID smallint,
    @RequestNo nvarchar(20),
    @ContainerKey nvarchar(50),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Freight].[ContainerRequestDetail]
	SET    [Remarks] = @Remarks, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [RequestNo] = @RequestNo
	       AND [ContainerKey] = @ContainerKey
	
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerRequestDetail] Record Into [ContainerRequestDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailSave] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailSave] 
    @BranchID smallint,
    @RequestNo nvarchar(20),
    @ContainerKey nvarchar(50),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Freight].[ContainerRequestDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [RequestNo] = @RequestNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Freight].[usp_ContainerRequestDetailUpdate] 
		@BranchID, @RequestNo, @ContainerKey, @Remarks, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Freight].[usp_ContainerRequestDetailInsert] 
		@BranchID, @RequestNo, @ContainerKey, @Remarks, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Freight].usp_[ContainerRequestDetailSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_ContainerRequestDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerRequestDetail] Record  based on [ContainerRequestDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_ContainerRequestDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_ContainerRequestDetailDelete] 
END 
GO
CREATE PROC [Freight].[usp_ContainerRequestDetailDelete] 
    @BranchID smallint,
    @RequestNo nvarchar(20),
    @ContainerKey nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Freight].[ContainerRequestDetail]
	SET	IsCancel = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [RequestNo] = @RequestNo
	       AND [ContainerKey] = @ContainerKey

	 


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_ContainerRequestDetailDelete]
-- ========================================================================================================================================

GO
 