-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_OrderHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderSelect] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderSelect] 
    @BranchID SMALLINT,
    @JobOrderNo NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], 
			[IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], 
			[IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[OrderHeader]
	WHERE	[BranchID] = @BranchID  
			AND [JobOrderNo] = @JobOrderNo  
END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderHeader] Records from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_OrderHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderList] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderList] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT	[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], 
			[IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[OrderHeader]
	WHERE	[BranchID] = @BranchID  
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderHeader] Records from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_OrderHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderPageView] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderPageView] 
	@BranchID SMALLINT,	
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], 
			[IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], 
			[IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Freight].[OrderHeader]
	WHERE	[BranchID] = @BranchID  
	ORDER BY [JobOrderNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_OrderHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderRecordCount] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderRecordCount] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Freight].[OrderHeader]
	WHERE  [BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Freight].[usp_OrderHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderAutoCompleteSearch] 
    @BranchID SMALLINT,
    @JobOrderNo NVARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [IsCancel], [CancelledBy], [CancelledOn], [IsClosed], [ClosedBy], [ClosedOn], [IsPrinted], [PrintedBy], [PrintedOn], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Freight].[OrderHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [JobOrderNo] LIKE '%' +  @JobOrderNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderInsert] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderInsert] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50) 
AS 
  

BEGIN

	
	INSERT INTO [Freight].[OrderHeader] (
			[BranchID], [JobOrderNo], [JobOrderDate], [OrderNo], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, GETUTCDATE()
   
			   
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderUpdate] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderUpdate] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50) 
AS 
 
	
BEGIN

	UPDATE [Freight].[OrderHeader]
	SET    [BranchID] = @BranchID, [JobOrderNo] = @JobOrderNo, [JobOrderDate] = @JobOrderDate, [OrderNo] = @OrderNo, [Remarks] = @Remarks, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
	
	 
END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderSave] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderSave] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
    @JobOrderDate datetime,
    @OrderNo nvarchar(50),
    @Remarks nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50) 

AS 
BEGIN
	
	/*
	NOTE : JobOrderNo is same as OrderNo(from Operation.OrderHeader)
	
	*/
	
	IF (SELECT COUNT(0) FROM [Freight].[OrderHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo)>0
	BEGIN
	    Exec [Freight].[usp_OrderHeaderUpdate] 
		@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, @ModifiedBy  


	END
	ELSE
	BEGIN
	    Exec [Freight].[usp_OrderHeaderInsert] 
		@BranchID, @JobOrderNo, @JobOrderDate, @OrderNo, @Remarks, @CreatedBy, @ModifiedBy  

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Freight].usp_[OrderHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderCancelled]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderCancelled]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderCancelled] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderCancelled] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
	@CancelledBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Freight].[OrderHeader]
	SET	[IsCancel] = CAST(1 as bit), CancelledBy = @CancelledBy, CancelledOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
		   


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderCancelled]
-- ========================================================================================================================================

GO
 


-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderClose]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderClose]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderClose] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderClose] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
	@ClosedBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Freight].[OrderHeader]
	SET	[IsClosed] = CAST(1 as bit), ClosedBy = @ClosedBy, ClosedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
		   


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderClose]
-- ========================================================================================================================================

GO
 



-- ========================================================================================================================================
-- START											 [Freight].[usp_OrderHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Freight].[usp_OrderHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Freight].[usp_OrderHeaderDelete] 
END 
GO
CREATE PROC [Freight].[usp_OrderHeaderDelete] 
    @BranchID smallint,
    @JobOrderNo nvarchar(50),
	@CancelledBy nvarchar(50)
AS 

	
BEGIN

	DELETE	[Freight].[OrderHeader]
	WHERE 	[BranchID] = @BranchID
	       AND [JobOrderNo] = @JobOrderNo
		   


END

-- ========================================================================================================================================
-- END  											 [Freight].[usp_OrderHeaderDelete]
-- ========================================================================================================================================

GO
 