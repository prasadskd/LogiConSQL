
-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerMovement] Record based on [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementSelect] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementSelect] 
    @BranchID SMALLINT,
    @DocumentNo NVARCHAR(50)
AS 

BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mv.[BranchID], Mv.[DocumentNo], Mv.[TruckTransactionNo], Mv.[TransactionKey], Mv.[OrderNo], Mv.[ContainerKey], Mv.[AgentCode], Mv.[OwnerCode], Mv.[CustomerCode], 
			Mv.[ContainerNo], Mv.[Size], Mv.[Type], Mv.[MovementCode], Mv.[MovementDate], Mv.[ContainerGrade], Mv.[TareWeight], Mv.[CargoWeight], Mv.[MaxGrossWeight], 
			Mv.[Seal1], Mv.[Seal2], Mv.[Temperature], Mv.[TemperatureMode], Mv.[Vent], Mv.[VentMode], Mv.[ContainerHeight], Mv.[Material], Mv.[GensetIndicator], 
			Mv.[MovementIndicator], Mv.[EFIndicator], Mv.[CargoCategory], Mv.[ClipOnNo], Mv.[ReeferUnit], Mv.[IsGenset], Mv.[GensetNo], Mv.[CustomPermitNo], 
			Mv.[YardLocation], Mv.[Remarks], Mv.[Status], Mv.Surveyor,Mv.YearBuilt,Mv.NextLocation,Mv.PreClear,Mv.PaperlessCode,
			Mv.ContainerStatus1,Mv.ContainerStatus2,Mv.ContainerLockStatus,
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn], Mv.[EDIDateTime], Mv.[IsOriginal],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cus.MerchantName,'') As CustomerName,
			'' As VesselName,
			'' As VoyageNo
	FROM	[Depot].[ContainerMovement] Mv
	Left Outer Join ContainerGradeLookup Cg On 
		Mv.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mv.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mv.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mv.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mv.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mv.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mv.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	Left Outer Join master.Merchant Cus ON 
		Mv.CustomerCode = Cus.MerchantCode
		And (Cus.IsShipper=Cast(1 as bit) OR Cus.IsConsignee=cast(1 as bit))
	WHERE  Mv.[BranchID] = @BranchID  
	       AND Mv.[DocumentNo] = @DocumentNo  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerMovement] Records from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementList] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementList] 
	@BranchID smallint
AS 
BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mv.[BranchID], Mv.[DocumentNo], Mv.[TruckTransactionNo], Mv.[TransactionKey], Mv.[OrderNo], Mv.[ContainerKey], Mv.[AgentCode], Mv.[OwnerCode], Mv.[CustomerCode], 
			Mv.[ContainerNo], Mv.[Size], Mv.[Type], Mv.[MovementCode], Mv.[MovementDate], Mv.[ContainerGrade], Mv.[TareWeight], Mv.[CargoWeight], Mv.[MaxGrossWeight], 
			Mv.[Seal1], Mv.[Seal2], Mv.[Temperature], Mv.[TemperatureMode], Mv.[Vent], Mv.[VentMode], Mv.[ContainerHeight], Mv.[Material], Mv.[GensetIndicator], 
			Mv.[MovementIndicator], Mv.[EFIndicator], Mv.[CargoCategory], Mv.[ClipOnNo], Mv.[ReeferUnit], Mv.[IsGenset], Mv.[GensetNo], Mv.[CustomPermitNo], 
			Mv.[YardLocation], Mv.[Remarks], Mv.[Status], Mv.Surveyor,Mv.YearBuilt,Mv.NextLocation,Mv.PreClear,Mv.PaperlessCode,
			Mv.ContainerStatus1,Mv.ContainerStatus2,Mv.ContainerLockStatus,
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn], Mv.[EDIDateTime], Mv.[IsOriginal],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cus.MerchantName,'') As CustomerName,
			'' As VesselName,
			'' As VoyageNo

	FROM	[Depot].[ContainerMovement] Mv
	Left Outer Join ContainerGradeLookup Cg On 
		Mv.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mv.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mv.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mv.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mv.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mv.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mv.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	Left Outer Join master.Merchant Cus ON 
		Mv.CustomerCode = Cus.MerchantCode
		And (Cus.IsShipper=Cast(1 as bit) OR Cus.IsConsignee=cast(1 as bit))
	WHERE  Mv.[BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerMovement] Records from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMovementPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementPageView] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementPageView]
	@BranchID smallint, 
	@fetchrows bigint
AS 
BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mv.[BranchID], Mv.[DocumentNo], Mv.[TruckTransactionNo], Mv.[TransactionKey], Mv.[OrderNo], Mv.[ContainerKey], Mv.[AgentCode], Mv.[OwnerCode], Mv.[CustomerCode], 
			Mv.[ContainerNo], Mv.[Size], Mv.[Type], Mv.[MovementCode], Mv.[MovementDate], Mv.[ContainerGrade], Mv.[TareWeight], Mv.[CargoWeight], Mv.[MaxGrossWeight], 
			Mv.[Seal1], Mv.[Seal2], Mv.[Temperature], Mv.[TemperatureMode], Mv.[Vent], Mv.[VentMode], Mv.[ContainerHeight], Mv.[Material], Mv.[GensetIndicator], 
			Mv.[MovementIndicator], Mv.[EFIndicator], Mv.[CargoCategory], Mv.[ClipOnNo], Mv.[ReeferUnit], Mv.[IsGenset], Mv.[GensetNo], Mv.[CustomPermitNo], 
			Mv.[YardLocation], Mv.[Remarks], Mv.[Status], Mv.Surveyor,Mv.YearBuilt,Mv.NextLocation,Mv.PreClear,Mv.PaperlessCode,
			Mv.ContainerStatus1,Mv.ContainerStatus2,Mv.ContainerLockStatus,
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn], Mv.[EDIDateTime], Mv.[IsOriginal],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cus.MerchantName,'') As CustomerName,
			'' As VesselName,
			'' As VoyageNo

	FROM	[Depot].[ContainerMovement] Mv
	Left Outer Join ContainerGradeLookup Cg On 
		Mv.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mv.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mv.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mv.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mv.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mv.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mv.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	Left Outer Join master.Merchant Cus ON 
		Mv.CustomerCode = Cus.MerchantCode
		And (Cus.IsShipper=Cast(1 as bit) OR Cus.IsConsignee=cast(1 as bit))
	WHERE  Mv.[BranchID] = @BranchID  
	ORDER BY Mv.[DocumentNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMovementRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementRecordCount] 
	@BranchID smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[ContainerMovement]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerMovement] Record based on [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMovementAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementAutoCompleteSearch] 
    @BranchID SMALLINT,
    @DocumentNo NVARCHAR(50)
AS 

BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mv.[BranchID], Mv.[DocumentNo], Mv.[TruckTransactionNo], Mv.[TransactionKey], Mv.[OrderNo], Mv.[ContainerKey], Mv.[AgentCode], Mv.[OwnerCode], Mv.[CustomerCode], 
			Mv.[ContainerNo], Mv.[Size], Mv.[Type], Mv.[MovementCode], Mv.[MovementDate], Mv.[ContainerGrade], Mv.[TareWeight], Mv.[CargoWeight], Mv.[MaxGrossWeight], 
			Mv.[Seal1], Mv.[Seal2], Mv.[Temperature], Mv.[TemperatureMode], Mv.[Vent], Mv.[VentMode], Mv.[ContainerHeight], Mv.[Material], Mv.[GensetIndicator], 
			Mv.[MovementIndicator], Mv.[EFIndicator], Mv.[CargoCategory], Mv.[ClipOnNo], Mv.[ReeferUnit], Mv.[IsGenset], Mv.[GensetNo], Mv.[CustomPermitNo], 
			Mv.[YardLocation], Mv.[Remarks], Mv.[Status], Mv.Surveyor,Mv.YearBuilt,Mv.NextLocation,Mv.PreClear,Mv.PaperlessCode,
			Mv.ContainerStatus1,Mv.ContainerStatus2,Mv.ContainerLockStatus,
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn], Mv.[EDIDateTime], Mv.[IsOriginal],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cus.MerchantName,'') As CustomerName,
			'' As VesselName,
			'' As VoyageNo

	FROM	[Depot].[ContainerMovement] Mv
	Left Outer Join ContainerGradeLookup Cg On 
		Mv.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mv.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mv.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mv.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mv.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mv.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mv.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	Left Outer Join master.Merchant Cus ON 
		Mv.CustomerCode = Cus.MerchantCode
		And (Cus.IsShipper=Cast(1 as bit) OR Cus.IsConsignee=cast(1 as bit))
	WHERE  Mv.[BranchID] = @BranchID   
	       AND Mv.[DocumentNo] LIKE '%' +  @DocumentNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementInsert] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementInsert] 
    @BranchID smallint,
    @DocumentNo nvarchar(50),
    @TruckTransactionNo nvarchar(50),
    @TransactionKey smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @CustomerCode nvarchar(10),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @MovementDate datetime,
    @ContainerGrade smallint,
    @TareWeight varchar(10),
    @CargoWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @Seal1 nvarchar(20),
    @Seal2 nvarchar(20),
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @Material smallint,
    @GensetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @IsGenset bit,
    @GensetNo nvarchar(20),
    @CustomPermitNo nvarchar(20),
    @YardLocation nchar(10),
    @Remarks nvarchar(100),
	@Surveyor nvarchar(50),
	@YearBuilt nvarchar(10),
	@NextLocation nvarchar(10),
	@PreClear nvarchar(50),
	@PaperlessCode nvarchar(50),
	@ContainerStatus1 nvarchar(10),
	@ContainerStatus2 nvarchar(10),
	@ContainerLockStatus nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Depot].[ContainerMovement] (
			[BranchID], [DocumentNo], [TruckTransactionNo], [TransactionKey], [OrderNo], [ContainerKey], [AgentCode], [OwnerCode], 
			[CustomerCode], [ContainerNo], [Size], [Type], [MovementCode], [MovementDate], [ContainerGrade], [TareWeight], [CargoWeight], 
			[MaxGrossWeight], [Seal1], [Seal2], [Temperature], [TemperatureMode], [Vent], [VentMode], [ContainerHeight], [Material], 
			[GensetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], [ClipOnNo], [ReeferUnit], [IsGenset], [GensetNo], 
			[CustomPermitNo], [YardLocation], [Remarks], [Status], Surveyor,YearBuilt,NextLocation,PreClear,PaperlessCode,
			ContainerStatus1,ContainerStatus2,ContainerLockStatus, [CreatedBy], [CreatedOn],[IsOriginal])
	SELECT	@BranchID, @DocumentNo, @TruckTransactionNo, @TransactionKey, @OrderNo, @ContainerKey, @AgentCode, @OwnerCode, 
			@CustomerCode, @ContainerNo, @Size, @Type, @MovementCode, @MovementDate, @ContainerGrade, @TareWeight, @CargoWeight, 
			@MaxGrossWeight, @Seal1, @Seal2, @Temperature, @TemperatureMode, @Vent, @VentMode, @ContainerHeight, @Material, 
			@GensetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @ClipOnNo, @ReeferUnit, @IsGenset, @GensetNo, 
			@CustomPermitNo, @YardLocation, @Remarks, CAST(1 AS BIT), @Surveyor,@YearBuilt,@NextLocation,@PreClear,@PaperlessCode,
			@ContainerStatus1,@ContainerStatus2,@ContainerLockStatus, @CreatedBy, GETUTCDATE(), CAST(1 AS BIT)

	Select @NewDocumentNo = @DocumentNo
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementUpdate] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementUpdate] 
    @BranchID smallint,
    @DocumentNo nvarchar(50),
    @TruckTransactionNo nvarchar(50),
    @TransactionKey smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @CustomerCode nvarchar(10),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @MovementDate datetime,
    @ContainerGrade smallint,
    @TareWeight varchar(10),
    @CargoWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @Seal1 nvarchar(20),
    @Seal2 nvarchar(20),
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @Material smallint,
    @GensetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @IsGenset bit,
    @GensetNo nvarchar(20),
    @CustomPermitNo nvarchar(20),
    @YardLocation nchar(10),
    @Remarks nvarchar(100),
	@Surveyor nvarchar(50),
	@YearBuilt nvarchar(10),
	@NextLocation nvarchar(10),
	@PreClear nvarchar(50),
	@PaperlessCode nvarchar(50),
	@ContainerStatus1 nvarchar(10),
	@ContainerStatus2 nvarchar(10),
	@ContainerLockStatus nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Depot].[ContainerMovement]
	SET		[TruckTransactionNo] = @TruckTransactionNo, [TransactionKey] = @TransactionKey, 
			[OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [AgentCode] = @AgentCode, [OwnerCode] = @OwnerCode, [CustomerCode] = @CustomerCode, 
			[ContainerNo] = @ContainerNo, [Size] = @Size, [Type] = @Type, [MovementCode] = @MovementCode, [MovementDate] = @MovementDate, 
			[ContainerGrade] = @ContainerGrade, [TareWeight] = @TareWeight, [CargoWeight] = @CargoWeight, [MaxGrossWeight] = @MaxGrossWeight, 
			[Seal1] = @Seal1, [Seal2] = @Seal2, [Temperature] = @Temperature, [TemperatureMode] = @TemperatureMode, [Vent] = @Vent, 
			[VentMode] = @VentMode, [ContainerHeight] = @ContainerHeight, [Material] = @Material, [GensetIndicator] = @GensetIndicator, 
			[MovementIndicator] = @MovementIndicator, [EFIndicator] = @EFIndicator, [CargoCategory] = @CargoCategory, [ClipOnNo] = @ClipOnNo, 
			[ReeferUnit] = @ReeferUnit, [IsGenset] = @IsGenset, [GensetNo] = @GensetNo, [CustomPermitNo] = @CustomPermitNo, 
			[YardLocation] = @YardLocation, [Remarks] = @Remarks, 
			Surveyor = @Surveyor,YearBuilt=@YearBuilt,NextLocation=@NextLocation,PreClear=@PreClear,PaperlessCode=@PaperlessCode,
			ContainerStatus1 = @ContainerStatus1,ContainerStatus2 = @ContainerStatus2,ContainerLockStatus=@ContainerLockStatus,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [DocumentNo] = @DocumentNo

	Select @NewDocumentNo = @DocumentNo
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementSave] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementSave] 
    @BranchID smallint,
    @DocumentNo nvarchar(50),
    @TruckTransactionNo nvarchar(50),
    @TransactionKey smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @CustomerCode nvarchar(10),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @MovementDate datetime,
    @ContainerGrade smallint,
    @TareWeight varchar(10),
    @CargoWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @Seal1 nvarchar(20),
    @Seal2 nvarchar(20),
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @Material smallint,
    @GensetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @IsGenset bit,
    @GensetNo nvarchar(20),
    @CustomPermitNo nvarchar(20),
    @YardLocation nchar(10),
    @Remarks nvarchar(100),
	@Surveyor nvarchar(50),
	@YearBuilt nvarchar(10),
	@NextLocation nvarchar(10),
	@PreClear nvarchar(50),
	@PaperlessCode nvarchar(50),
	@ContainerStatus1 nvarchar(10),
	@ContainerStatus2 nvarchar(10),
	@ContainerLockStatus nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewDocumentNo nvarchar(50) OUTPUT
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[ContainerMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [DocumentNo] = @DocumentNo)>0
	BEGIN
	    Exec [Depot].[usp_ContainerMovementUpdate] 
			@BranchID, @DocumentNo, @TruckTransactionNo, @TransactionKey, @OrderNo, @ContainerKey, @AgentCode, @OwnerCode, 
			@CustomerCode, @ContainerNo, @Size, @Type, @MovementCode, @MovementDate, @ContainerGrade, @TareWeight, @CargoWeight, 
			@MaxGrossWeight, @Seal1, @Seal2, @Temperature, @TemperatureMode, @Vent, @VentMode, @ContainerHeight, @Material, 
			@GensetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @ClipOnNo, @ReeferUnit, @IsGenset, @GensetNo, 
			@CustomPermitNo, @YardLocation, @Remarks, @Surveyor,@YearBuilt,@NextLocation,@PreClear,@PaperlessCode,
			@ContainerStatus1,@ContainerStatus2,@ContainerLockStatus, @CreatedBy, @ModifiedBy, @NewDocumentNo = @NewDocumentNo OUTPUT 


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@MovementIndicatorDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@MovementIndicatorDesc = LookupDescription,@DocumentNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@MovementIndicator
		
		 
		
		IF @MovementIndicatorDesc='INWARD' 
			SET @DocID ='Depot\EIRIN'
		ELSE IF @MovementIndicatorDesc='OUTWARD' 
			SET @DocID ='Depot\EIROUT'
		
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 10, 'Depot\EIRIN', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @OrderNo OUTPUT



	    Exec [Depot].[usp_ContainerMovementInsert] 
			@BranchID, @DocumentNo, @TruckTransactionNo, @TransactionKey, @OrderNo, @ContainerKey, @AgentCode, @OwnerCode, 
			@CustomerCode, @ContainerNo, @Size, @Type, @MovementCode, @MovementDate, @ContainerGrade, @TareWeight, @CargoWeight, 
			@MaxGrossWeight, @Seal1, @Seal2, @Temperature, @TemperatureMode, @Vent, @VentMode, @ContainerHeight, @Material, 
			@GensetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @ClipOnNo, @ReeferUnit, @IsGenset, @GensetNo, 
			@CustomPermitNo, @YardLocation, @Remarks, @Surveyor,@YearBuilt,@NextLocation,@PreClear,@PaperlessCode,
			@ContainerStatus1,@ContainerStatus2,@ContainerLockStatus, @CreatedBy, @ModifiedBy, @NewDocumentNo = @NewDocumentNo OUTPUT 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[ContainerMovementSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerMovement] Record  based on [ContainerMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMovementDelete] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMovementDelete] 
    @BranchID smallint,
    @DocumentNo nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Depot].[ContainerMovement]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [DocumentNo] = @DocumentNo


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMovementDelete]
-- ========================================================================================================================================

GO

