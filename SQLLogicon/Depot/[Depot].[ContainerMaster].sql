
-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerMaster] Record based on [ContainerMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMasterSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterSelect] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterSelect] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15)
AS 

BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mst.[BranchID], Mst.[ContainerNo], Mst.[Size], Mst.[Type], Mst.[MovementIndicator], Mst.[AgentCode], Mst.[OwnerCode], Mst.[ContainerGrade], 
			Mst.[OrderNo], Mst.[ContainerKey], Mst.[DocumentNo], Mst.[MovementCode], Mst.[MovementDate], Mst.[EFIndicator], Mst.[StuffUnStuffDate], 
			Mst.[StatusDate], Mst.[SealNo1], Mst.[SealNo2], Mst.[Material], Mst.[ContainerHeight], Mst.[Temperature], Mst.[TemperatureMode], Mst.[Vent], 
			Mst.[VentMode], Mst.[ClipOnNo], Mst.[ReeferUnit], Mst.[TareWeight], Mst.[MaxGrossWeight], 
			Mst.[ContainerStatus1], Mst.[ContainerStatus2], Mst.[ContainerStatus3], Mst.[Remarks1], Mst.[Remarks2], Mst.[Remarks3], Mst.[IsHold], 
			Mst.[HireMode], Mst.[YearBuilt], Mst.[UsedBy], Mst.[Lesse], Mst.[Lessor], Mst.[Insurer], Mst.[Authorize], Mst.[Amount], 
			Mst.[CreatedBy], Mst.[CreatedOn], Mst.[ModifiedBy], Mst.[ModifiedOn],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName 
	FROM	[Depot].[ContainerMaster] Mst
	Left Outer Join ContainerGradeLookup Cg On 
		Mst.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mst.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mst.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mst.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mst.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mst.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mst.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mst.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	WHERE  Mst.[BranchID] = @BranchID  
	       AND [ContainerNo] = @ContainerNo  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerMaster] Records from [ContainerMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMasterList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterList] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterList] 
	@BranchID smallint
AS 
BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mst.[BranchID], Mst.[ContainerNo], Mst.[Size], Mst.[Type], Mst.[MovementIndicator], Mst.[AgentCode], Mst.[OwnerCode], Mst.[ContainerGrade], 
			Mst.[OrderNo], Mst.[ContainerKey], Mst.[DocumentNo], Mst.[MovementCode], Mst.[MovementDate], Mst.[EFIndicator], Mst.[StuffUnStuffDate], 
			Mst.[StatusDate], Mst.[SealNo1], Mst.[SealNo2], Mst.[Material], Mst.[ContainerHeight], Mst.[Temperature], Mst.[TemperatureMode], Mst.[Vent], 
			Mst.[VentMode], Mst.[ClipOnNo], Mst.[ReeferUnit], Mst.[TareWeight], Mst.[MaxGrossWeight], 
			Mst.[ContainerStatus1], Mst.[ContainerStatus2], Mst.[ContainerStatus3], Mst.[Remarks1], Mst.[Remarks2], Mst.[Remarks3], Mst.[IsHold], 
			Mst.[HireMode], Mst.[YearBuilt], Mst.[UsedBy], Mst.[Lesse], Mst.[Lessor], Mst.[Insurer], Mst.[Authorize], Mst.[Amount], 
			Mst.[CreatedBy], Mst.[CreatedOn], Mst.[ModifiedBy], Mst.[ModifiedOn],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName 
	FROM	[Depot].[ContainerMaster] Mst
	Left Outer Join ContainerGradeLookup Cg On 
		Mst.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mst.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mst.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mst.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mst.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mst.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mst.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mst.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	WHERE  Mst.[BranchID] = @BranchID
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerMaster] Records from [ContainerMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMasterPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterPageView] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterPageView] 
	@BranchID smallint,
	@fetchrows bigint
AS 
BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mst.[BranchID], Mst.[ContainerNo], Mst.[Size], Mst.[Type], Mst.[MovementIndicator], Mst.[AgentCode], Mst.[OwnerCode], Mst.[ContainerGrade], 
			Mst.[OrderNo], Mst.[ContainerKey], Mst.[DocumentNo], Mst.[MovementCode], Mst.[MovementDate], Mst.[EFIndicator], Mst.[StuffUnStuffDate], 
			Mst.[StatusDate], Mst.[SealNo1], Mst.[SealNo2], Mst.[Material], Mst.[ContainerHeight], Mst.[Temperature], Mst.[TemperatureMode], Mst.[Vent], 
			Mst.[VentMode], Mst.[ClipOnNo], Mst.[ReeferUnit], Mst.[TareWeight], Mst.[MaxGrossWeight], 
			Mst.[ContainerStatus1], Mst.[ContainerStatus2], Mst.[ContainerStatus3], Mst.[Remarks1], Mst.[Remarks2], Mst.[Remarks3], Mst.[IsHold], 
			Mst.[HireMode], Mst.[YearBuilt], Mst.[UsedBy], Mst.[Lesse], Mst.[Lessor], Mst.[Insurer], Mst.[Authorize], Mst.[Amount], 
			Mst.[CreatedBy], Mst.[CreatedOn], Mst.[ModifiedBy], Mst.[ModifiedOn],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName 
	FROM	[Depot].[ContainerMaster] Mst
	Left Outer Join ContainerGradeLookup Cg On 
		Mst.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mst.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mst.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mst.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mst.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mst.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mst.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mst.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	WHERE  Mst.[BranchID] = @BranchID
	ORDER BY Mst.[ContainerNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMasterRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterRecordCount] 
	@BranchID smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[ContainerMaster]
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerMaster] Record based on [ContainerMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerMasterAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterAutoCompleteSearch] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15)
AS 

BEGIN

	;with ContainerGradeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerGrade'),
	TemperatureModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='TempratureType'),
	VentModeLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='VentilationType'),
	ContainerHeightLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerHeight'),
	MaterialLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='ContainerMaterial'),
	MovementIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='MovementIndicator'),
	EFIndicatorLookup As (select LookupID,LookupDescription From config.lookup where LookupCategory='EFIndicator')
	SELECT	Mst.[BranchID], Mst.[ContainerNo], Mst.[Size], Mst.[Type], Mst.[MovementIndicator], Mst.[AgentCode], Mst.[OwnerCode], Mst.[ContainerGrade], 
			Mst.[OrderNo], Mst.[ContainerKey], Mst.[DocumentNo], Mst.[MovementCode], Mst.[MovementDate], Mst.[EFIndicator], Mst.[StuffUnStuffDate], 
			Mst.[StatusDate], Mst.[SealNo1], Mst.[SealNo2], Mst.[Material], Mst.[ContainerHeight], Mst.[Temperature], Mst.[TemperatureMode], Mst.[Vent], 
			Mst.[VentMode], Mst.[ClipOnNo], Mst.[ReeferUnit], Mst.[TareWeight], Mst.[MaxGrossWeight], 
			Mst.[ContainerStatus1], Mst.[ContainerStatus2], Mst.[ContainerStatus3], Mst.[Remarks1], Mst.[Remarks2], Mst.[Remarks3], Mst.[IsHold], 
			Mst.[HireMode], Mst.[YearBuilt], Mst.[UsedBy], Mst.[Lesse], Mst.[Lessor], Mst.[Insurer], Mst.[Authorize], Mst.[Amount], 
			Mst.[CreatedBy], Mst.[CreatedOn], Mst.[ModifiedBy], Mst.[ModifiedOn],
			ISNULL(Tm.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(Vt.LookupDescription,'') As VentModeDescription,
			ISNULL(Cg.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(Hg.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(Mt.LookupDescription,'') As MaterialDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName 
	FROM	[Depot].[ContainerMaster] Mst
	Left Outer Join ContainerGradeLookup Cg On 
		Mst.ContainerGrade = Cg.LookupID
	Left Outer Join TemperatureModeLookup Tm On 
		Mst.TemperatureMode= Tm.LookupID
	Left Outer Join VentModeLookup Vt On 
		Mst.VentMode = Vt.LookupID
	Left Outer Join ContainerHeightLookup Hg On 
		Mst.ContainerHeight = Hg.LookupID
	Left Outer Join MaterialLookup Mt On 
		Mst.Material = Mt.LookupID
	Left Outer Join MovementIndicatorLookup Mi On 
		Mst.MovementIndicator = Mi.LookupID
	Left Outer Join EFIndicatorLookup Ef On 
		Mst.EFIndicator = Ef.LookupID
	Left Outer Join Master.Merchant Agnt ON 
		Mst.AgentCode = Agnt.MerchantCode
		And Agnt.IsLiner = cast(1 as bit)
	WHERE  Mst.[BranchID] = @BranchID 
	       AND Mst.[ContainerNo] LIKE '%' +  @ContainerNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerMaster] Record Into [ContainerMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMasterInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterInsert] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterInsert] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementIndicator smallint,
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @ContainerGrade smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @DocumentNo nvarchar(50),
    @MovementCode varchar(20),
    @MovementDate datetime,
    @EFIndicator smallint,
    @StuffUnStuffDate datetime,
    @StatusDate datetime,
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Material smallint,
    @ContainerHeight smallint,
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @TareWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @ContainerStatus1 nvarchar(10),
    @ContainerStatus2 nvarchar(10),
    @ContainerStatus3 nvarchar(10),
    @Remarks1 nvarchar(100),
    @Remarks2 nvarchar(100),
    @Remarks3 nvarchar(100),
    @IsHold bit,
    @HireMode smallint,
    @YearBuilt nvarchar(10),
    @UsedBy nvarchar(20),
    @Lesse nvarchar(20),
    @Lessor nvarchar(20),
    @Insurer nvarchar(20),
    @Authorize nvarchar(20),
    @Amount nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Depot].[ContainerMaster] (
			[BranchID], [ContainerNo], [Size], [Type], [MovementIndicator], [AgentCode], [OwnerCode], [ContainerGrade], [OrderNo], 
			[ContainerKey], [DocumentNo], [MovementCode], [MovementDate], [EFIndicator], [StuffUnStuffDate], [StatusDate], 
			[SealNo1], [SealNo2], [Material], [ContainerHeight], [Temperature], [TemperatureMode], [Vent], [VentMode], [ClipOnNo], 
			[ReeferUnit], [TareWeight], [MaxGrossWeight], [ContainerStatus1], [ContainerStatus2], [ContainerStatus3], 
			[Remarks1], [Remarks2], [Remarks3], [IsHold], [HireMode], [YearBuilt], [UsedBy], [Lesse], [Lessor], [Insurer], [Authorize], 
			[Amount], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ContainerNo, @Size, @Type, @MovementIndicator, @AgentCode, @OwnerCode, @ContainerGrade, @OrderNo, 
			@ContainerKey, @DocumentNo, @MovementCode, @MovementDate, @EFIndicator, @StuffUnStuffDate, @StatusDate, 
			@SealNo1, @SealNo2, @Material, @ContainerHeight, @Temperature, @TemperatureMode, @Vent, @VentMode, @ClipOnNo, 
			@ReeferUnit, @TareWeight, @MaxGrossWeight, @ContainerStatus1, @ContainerStatus2, @ContainerStatus3, 
			@Remarks1, @Remarks2, @Remarks3, @IsHold, @HireMode, @YearBuilt, @UsedBy, @Lesse, @Lessor, @Insurer, @Authorize, 
			@Amount, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerMaster] Record Into [ContainerMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMasterUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterUpdate] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterUpdate] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementIndicator smallint,
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @ContainerGrade smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @DocumentNo nvarchar(50),
    @MovementCode varchar(20),
    @MovementDate datetime,
    @EFIndicator smallint,
    @StuffUnStuffDate datetime,
    @StatusDate datetime,
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Material smallint,
    @ContainerHeight smallint,
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @TareWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @ContainerStatus1 nvarchar(10),
    @ContainerStatus2 nvarchar(10),
    @ContainerStatus3 nvarchar(10),
    @Remarks1 nvarchar(100),
    @Remarks2 nvarchar(100),
    @Remarks3 nvarchar(100),
    @IsHold bit,
    @HireMode smallint,
    @YearBuilt nvarchar(10),
    @UsedBy nvarchar(20),
    @Lesse nvarchar(20),
    @Lessor nvarchar(20),
    @Insurer nvarchar(20),
    @Authorize nvarchar(20),
    @Amount nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Depot].[ContainerMaster]
	SET		[Size] = @Size, [Type] = @Type, [MovementIndicator] = @MovementIndicator, [AgentCode] = @AgentCode, [OwnerCode] = @OwnerCode, 
			[ContainerGrade] = @ContainerGrade, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [DocumentNo] = @DocumentNo, 
			[MovementCode] = @MovementCode, [MovementDate] = @MovementDate, [EFIndicator] = @EFIndicator, 
			[StuffUnStuffDate] = @StuffUnStuffDate, [StatusDate] = @StatusDate, [SealNo1] = @SealNo1, [SealNo2] = @SealNo2, 
			[Material] = @Material, [ContainerHeight] = @ContainerHeight, [Temperature] = @Temperature, [TemperatureMode] = @TemperatureMode, 
			[Vent] = @Vent, [VentMode] = @VentMode, [ClipOnNo] = @ClipOnNo, [ReeferUnit] = @ReeferUnit, [TareWeight] = @TareWeight, 
			[MaxGrossWeight] = @MaxGrossWeight, [ContainerStatus1] = @ContainerStatus1, [ContainerStatus2] = @ContainerStatus2, [ContainerStatus3] = @ContainerStatus3, 
			[Remarks1] = @Remarks1, [Remarks2] = @Remarks2, [Remarks3] = @Remarks3, [IsHold] = @IsHold, [HireMode] = @HireMode, 
			[YearBuilt] = @YearBuilt, [UsedBy] = @UsedBy, [Lesse] = @Lesse, [Lessor] = @Lessor, [Insurer] = @Insurer, 
			[Authorize] = @Authorize, [Amount] = @Amount, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [ContainerNo] = @ContainerNo
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerMaster] Record Into [ContainerMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMasterSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterSave] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterSave] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementIndicator smallint,
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @ContainerGrade smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @DocumentNo nvarchar(50),
    @MovementCode varchar(20),
    @MovementDate datetime,
    @EFIndicator smallint,
    @StuffUnStuffDate datetime,
    @StatusDate datetime,
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Material smallint,
    @ContainerHeight smallint,
    @Temperature numeric(5, 2),
    @TemperatureMode smallint,
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ClipOnNo nvarchar(10),
    @ReeferUnit smallint,
    @TareWeight varchar(10),
    @MaxGrossWeight varchar(10),
    @ContainerStatus1 nvarchar(10),
    @ContainerStatus2 nvarchar(10),
    @ContainerStatus3 nvarchar(10),
    @Remarks1 nvarchar(100),
    @Remarks2 nvarchar(100),
    @Remarks3 nvarchar(100),
    @IsHold bit,
    @HireMode smallint,
    @YearBuilt nvarchar(10),
    @UsedBy nvarchar(20),
    @Lesse nvarchar(20),
    @Lessor nvarchar(20),
    @Insurer nvarchar(20),
    @Authorize nvarchar(20),
    @Amount nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[ContainerMaster] 
		WHERE 	[BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo)>0
	BEGIN
	    Exec [Depot].[usp_ContainerMasterUpdate] 
			@BranchID, @ContainerNo, @Size, @Type, @MovementIndicator, @AgentCode, @OwnerCode, @ContainerGrade, @OrderNo, 
			@ContainerKey, @DocumentNo, @MovementCode, @MovementDate, @EFIndicator, @StuffUnStuffDate, @StatusDate, 
			@SealNo1, @SealNo2, @Material, @ContainerHeight, @Temperature, @TemperatureMode, @Vent, @VentMode, @ClipOnNo, 
			@ReeferUnit, @TareWeight, @MaxGrossWeight, @ContainerStatus1, @ContainerStatus2, @ContainerStatus3, 
			@Remarks1, @Remarks2, @Remarks3, @IsHold, @HireMode, @YearBuilt, @UsedBy, @Lesse, @Lessor, @Insurer, @Authorize, 
			@Amount, @CreatedBy,@ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_ContainerMasterInsert] 
			@BranchID, @ContainerNo, @Size, @Type, @MovementIndicator, @AgentCode, @OwnerCode, @ContainerGrade, @OrderNo, 
			@ContainerKey, @DocumentNo, @MovementCode, @MovementDate, @EFIndicator, @StuffUnStuffDate, @StatusDate, 
			@SealNo1, @SealNo2, @Material, @ContainerHeight, @Temperature, @TemperatureMode, @Vent, @VentMode, @ClipOnNo, 
			@ReeferUnit, @TareWeight, @MaxGrossWeight, @ContainerStatus1, @ContainerStatus2, @ContainerStatus3, 
			@Remarks1, @Remarks2, @Remarks3, @IsHold, @HireMode, @YearBuilt, @UsedBy, @Lesse, @Lessor, @Insurer, @Authorize, 
			@Amount, @CreatedBy,@ModifiedBy

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[ContainerMasterSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerMasterDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerMaster] Record  based on [ContainerMaster]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerMasterDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerMasterDelete] 
END 
GO
CREATE PROC [Depot].[usp_ContainerMasterDelete] 
    @BranchID smallint,
    @ContainerNo varchar(15)
AS 

	
BEGIN

	
	DELETE
	FROM   [Depot].[ContainerMaster]
	WHERE  [BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	 


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerMasterDelete]
-- ========================================================================================================================================

GO

