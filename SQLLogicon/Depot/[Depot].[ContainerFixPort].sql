

-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerFixPort] Record based on [ContainerFixPort] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerFixPortSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortSelect] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortSelect] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15),
    @PortCode NVARCHAR(10)
AS 

BEGIN

	SELECT	Cf.[BranchID], Cf.[ContainerNo], Cf.[PortCode], Cf.[CreatedBy], Cf.[CreatedOn], Cf.[ModifiedBy], Cf.[ModifiedOn],
			Pr.PortName
	FROM   [Depot].[ContainerFixPort] Cf
	Left Outer Join Master.Port Pr ON 
		Cf.PortCode = Pr.PortCode
	WHERE  Cf.[BranchID] = @BranchID  
	       AND Cf.[ContainerNo] = @ContainerNo  
	       AND Cf.[PortCode] = @PortCode  

END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerFixPort] Records from [ContainerFixPort] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerFixPortList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortList] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortList] 
	@BranchID smallint,
	@ContainerNo varchar(15)
AS 
BEGIN

	SELECT	Cf.[BranchID], Cf.[ContainerNo], Cf.[PortCode], Cf.[CreatedBy], Cf.[CreatedOn], Cf.[ModifiedBy], Cf.[ModifiedOn],
			Pr.PortName
	FROM   [Depot].[ContainerFixPort] Cf
	Left Outer Join Master.Port Pr ON 
		Cf.PortCode = Pr.PortCode
	WHERE  Cf.[BranchID] = @BranchID  
	       AND Cf.[ContainerNo] = @ContainerNo 

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerFixPort] Records from [ContainerFixPort] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerFixPortPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortPageView] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortPageView] 
	@BranchID smallint,
	@ContainerNo varchar(15),
	@fetchrows bigint
AS 
BEGIN

	SELECT	Cf.[BranchID], Cf.[ContainerNo], Cf.[PortCode], Cf.[CreatedBy], Cf.[CreatedOn], Cf.[ModifiedBy], Cf.[ModifiedOn],
			Pr.PortName
	FROM   [Depot].[ContainerFixPort] Cf
	Left Outer Join Master.Port Pr ON 
		Cf.PortCode = Pr.PortCode
	WHERE  Cf.[BranchID] = @BranchID  
		And Cf.ContainerNo = @ContainerNo
	ORDER By Cf.[PortCode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerFixPort] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerFixPortRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortRecordCount] 
	@BranchID smallint,
	@ContainerNo varchar(15)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[ContainerFixPort]
	where BranchID = @BranchID
	And ContainerNo = @ContainerNo

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerFixPort] Record based on [ContainerFixPort] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerFixPortAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortAutoCompleteSearch] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15),
    @PortCode NVARCHAR(10)
AS 

BEGIN

	SELECT	Cf.[BranchID], Cf.[ContainerNo], Cf.[PortCode], Cf.[CreatedBy], Cf.[CreatedOn], Cf.[ModifiedBy], Cf.[ModifiedOn],
			Pr.PortName
	FROM   [Depot].[ContainerFixPort] Cf
	Left Outer Join Master.Port Pr ON 
		Cf.PortCode = Pr.PortCode
	WHERE  Cf.[BranchID] = @BranchID 
	       AND Cf.[ContainerNo] = @ContainerNo  
	       AND Cf.[PortCode] LIKE '%' +  @PortCode  + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerFixPort] Record Into [ContainerFixPort] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerFixPortInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortInsert] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortInsert] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @PortCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Depot].[ContainerFixPort] (
			[BranchID], [ContainerNo], [PortCode], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ContainerNo, @PortCode, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerFixPort] Record Into [ContainerFixPort] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerFixPortUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortUpdate] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortUpdate] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @PortCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Depot].[ContainerFixPort]
	SET    [PortCode] = @PortCode, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [PortCode] = @PortCode
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerFixPort] Record Into [ContainerFixPort] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerFixPortSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortSave] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortSave] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @PortCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[ContainerFixPort] 
		WHERE 	[BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [PortCode] = @PortCode)>0
	BEGIN
	    Exec [Depot].[usp_ContainerFixPortUpdate] 
		@BranchID, @ContainerNo, @PortCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_ContainerFixPortInsert] 
		@BranchID, @ContainerNo, @PortCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[ContainerFixPortSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerFixPortDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerFixPort] Record  based on [ContainerFixPort]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerFixPortDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerFixPortDelete] 
END 
GO
CREATE PROC [Depot].[usp_ContainerFixPortDelete] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @PortCode nvarchar(10)
AS 

	
BEGIN


	DELETE
	FROM   [Depot].[ContainerFixPort]
	WHERE  [BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [PortCode] = @PortCode


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerFixPortDelete]
-- ========================================================================================================================================

GO
