

-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TruckMovementDetails] Record based on [TruckMovementDetails] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsSelect] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsSelect] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50),
    @TransctionKey SMALLINT
AS 

BEGIN

	;With	TripTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckActivityIndicator'),
			EFIndicatorLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EFIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[TransctionKey], Dt.[TripType], Dt.[DocumentNo], Dt.[OrderNo], Dt.[ContainerKey], 
			Dt.[EFIndicator], Dt.[BookingType], Dt.[OrderType], Dt.[Size], Dt.[Type], Dt.[MovementCode], Dt.[DateIn], Dt.[DateOut], Dt.[TotalMinutes], 
			Dt.[Status], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Trp.LookupDescription,'') As TripTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription
	FROM	[Depot].[TruckMovementDetails] Dt
	Left Outer Join TripTypeLookup Trp ON
		Dt.TripType = Trp.LookupID
	Left Outer Join EFIndicatorLookup EF ON
		Dt.EFIndicator = EF.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[TransactionNo] = @TransactionNo  
	       AND Dt.[TransctionKey] = @TransctionKey  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TruckMovementDetails] Records from [TruckMovementDetails] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsList] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsList] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50)
AS 
BEGIN

	;With	TripTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckActivityIndicator'),
			EFIndicatorLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EFIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[TransctionKey], Dt.[TripType], Dt.[DocumentNo], Dt.[OrderNo], Dt.[ContainerKey], 
			Dt.[EFIndicator], Dt.[BookingType], Dt.[OrderType], Dt.[Size], Dt.[Type], Dt.[MovementCode], Dt.[DateIn], Dt.[DateOut], Dt.[TotalMinutes], 
			Dt.[Status], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Trp.LookupDescription,'') As TripTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription
	FROM	[Depot].[TruckMovementDetails] Dt
	Left Outer Join TripTypeLookup Trp ON
		Dt.TripType = Trp.LookupID
	Left Outer Join EFIndicatorLookup EF ON
		Dt.EFIndicator = EF.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
		AND Dt.[TransactionNo] = @TransactionNo  


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TruckMovementDetails] Records from [TruckMovementDetails] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsPageView] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsPageView] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;With	TripTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckActivityIndicator'),
			EFIndicatorLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EFIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[TransctionKey], Dt.[TripType], Dt.[DocumentNo], Dt.[OrderNo], Dt.[ContainerKey], 
			Dt.[EFIndicator], Dt.[BookingType], Dt.[OrderType], Dt.[Size], Dt.[Type], Dt.[MovementCode], Dt.[DateIn], Dt.[DateOut], Dt.[TotalMinutes], 
			Dt.[Status], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Trp.LookupDescription,'') As TripTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription
	FROM	[Depot].[TruckMovementDetails] Dt
	Left Outer Join TripTypeLookup Trp ON
		Dt.TripType = Trp.LookupID
	Left Outer Join EFIndicatorLookup EF ON
		Dt.EFIndicator = EF.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
		AND Dt.[TransactionNo] = @TransactionNo  
	ORDER BY Dt.[TransactionNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [TruckMovementDetails] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsRecordCount] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[TruckMovementDetails]
	WHERE  [BranchID] = @BranchID  
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TruckMovementDetails] Record based on [TruckMovementDetails] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsAutoCompleteSearch] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50) 
AS 

BEGIN

	;With	TripTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckActivityIndicator'),
			EFIndicatorLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EFIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[TransctionKey], Dt.[TripType], Dt.[DocumentNo], Dt.[OrderNo], Dt.[ContainerKey], 
			Dt.[EFIndicator], Dt.[BookingType], Dt.[OrderType], Dt.[Size], Dt.[Type], Dt.[MovementCode], Dt.[DateIn], Dt.[DateOut], Dt.[TotalMinutes], 
			Dt.[Status], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Trp.LookupDescription,'') As TripTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription
	FROM	[Depot].[TruckMovementDetails] Dt
	Left Outer Join TripTypeLookup Trp ON
		Dt.TripType = Trp.LookupID
	Left Outer Join EFIndicatorLookup EF ON
		Dt.EFIndicator = EF.LookupID
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[TransactionNo] LIKE '%' +  @TransactionNo  + '%'
	       
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [TruckMovementDetails] Record Into [TruckMovementDetails] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsInsert] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsInsert] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
    @TransctionKey smallint,
    @TripType smallint,
    @DocumentNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @EFIndicator smallint,
    @BookingType smallint,
    @OrderType nvarchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @DateIn datetime,
    @DateOut datetime,
    @TotalMinutes int,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Depot].[TruckMovementDetails] (
			[BranchID], [TransactionNo], [TransctionKey], [TripType], [DocumentNo], [OrderNo], [ContainerKey], [EFIndicator], [BookingType], 
			[OrderType], [Size], [Type], [MovementCode], [DateIn], [DateOut], [TotalMinutes], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @TransactionNo, @TransctionKey, @TripType, @DocumentNo, @OrderNo, @ContainerKey, @EFIndicator, @BookingType, 
			@OrderType, @Size, @Type, @MovementCode, @DateIn, @DateOut, @TotalMinutes, Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [TruckMovementDetails] Record Into [TruckMovementDetails] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsUpdate] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsUpdate] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
    @TransctionKey smallint,
    @TripType smallint,
    @DocumentNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @EFIndicator smallint,
    @BookingType smallint,
    @OrderType nvarchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @DateIn datetime,
    @DateOut datetime,
    @TotalMinutes int,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Depot].[TruckMovementDetails]
	SET		[TripType] = @TripType, [DocumentNo] = @DocumentNo, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [EFIndicator] = @EFIndicator, 
			[BookingType] = @BookingType, [OrderType] = @OrderType, [Size] = @Size, [Type] = @Type, [MovementCode] = @MovementCode, 
			[DateIn] = @DateIn, [DateOut] = @DateOut, [TotalMinutes] = @TotalMinutes, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [TransactionNo] = @TransactionNo
			AND [TransctionKey] = @TransctionKey
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [TruckMovementDetails] Record Into [TruckMovementDetails] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsSave] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsSave] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
    @TransctionKey smallint,
    @TripType smallint,
    @DocumentNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @EFIndicator smallint,
    @BookingType smallint,
    @OrderType nvarchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @MovementCode nvarchar(50),
    @DateIn datetime,
    @DateOut datetime,
    @TotalMinutes int,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[TruckMovementDetails] 
		WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo
	       AND [TransctionKey] = @TransctionKey)>0
	BEGIN
	    Exec [Depot].[usp_TruckMovementDetailsUpdate] 
			@BranchID, @TransactionNo, @TransctionKey, @TripType, @DocumentNo, @OrderNo, @ContainerKey, @EFIndicator, @BookingType, 
			@OrderType, @Size, @Type, @MovementCode, @DateIn, @DateOut, @TotalMinutes, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_TruckMovementDetailsInsert] 
			@BranchID, @TransactionNo, @TransctionKey, @TripType, @DocumentNo, @OrderNo, @ContainerKey, @EFIndicator, @BookingType, 
			@OrderType, @Size, @Type, @MovementCode, @DateIn, @DateOut, @TotalMinutes, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[TruckMovementDetailsSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementDetailsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [TruckMovementDetails] Record  based on [TruckMovementDetails]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementDetailsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementDetailsDelete] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementDetailsDelete] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
    @TransctionKey smallint
AS 

	
BEGIN

	UPDATE	[Depot].[TruckMovementDetails]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo
	       AND [TransctionKey] = @TransctionKey



END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementDetailsDelete]
-- ========================================================================================================================================

GO

 