 


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderSelect] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderSelect] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
AS 

BEGIN


	;WITH BookingTypeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup]WHERE [LookupCategory] ='BookingType'),
	TradeModeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup] WHERE [LookupCategory] ='TradeMode')
	SELECT	BkHd.[BranchID], BkHd.[OrderNo], BkHd.[BookingBLNo], BkHd.[SubBookingBLNo], BkHd.[BookingDate], BkHd.[BookingType], BkHd.[OrderType], 
			BkHd.[AgentCode], BkHd.[OwnerCode], BkHd.[FFCode], BkHd.[CustomerCode], BkHd.[VesselScheduleID], 
			BkHd.[VesselID], BkHd.[VoyageNo], BkHd.[Terminal], BkHd.[LoadingPort], BkHd.[DischargePort], BkHd.[DestinationPort], BkHd.[ETA], BkHd.[ETD], 
			BkHd.[PortCutOff], BkHd.[PortCutOffReefer], BkHd.[YardCutOffDry], BkHd.[YardCutOffReefer], BkHd.[CFSCutOffDry], BkHd.[CFSCutOffReefer], 
			BkHd.[NextPrevLoc], BkHd.[AllowLateGate], BkHd.[TotalQty], BkHd.[UOM], BkHd.[TotalVolume], BkHd.[TotalWeight], 
			BkHd.[IsEdi], BkHd.[IsCancel], BkHd.[IsTranferred], BkHd.[IsApproved], BkHd.[IsClosed], BkHd.[CreatedBy], BkHd.[CreatedOn], BkHd.[ModifiedBy], BkHd.[ModifiedOn], 
			ISNULL(LdPrt.PortName,'') As LoadingPortName,ISNULL(DisPrt.PortName,'') As DischargePortName,ISNULL(DstPrt.PortName,'') As DestinationPortName,
			ISNULL(Agnt.MerchantName,'') As AgentName, ISNULL(COALESCE(NULLIF(BkHd.FFName,''),FF.MerchantName),'') As FFName, ISNULL(COALESCE(NULLIF(BkHd.CustomerName,''),Cust.MerchantName),'') As CustomerName,
			ISNULL(Ownr.MerchantName,'') As OwnerName, ISNULL(vsl.VesselName,'') As VesselName,
			ISNULL(BkDesc.LookUpCode,'') As BookingTypeDescription,
			ISNULL(TdDesc.LookupCode,'') As TradeMode	
	FROM	[Depot].[BookingHeader] BkHd
	LEFT OUTER JOIN [Master].[Port] LdPrt ON
		BkHd.LoadingPort = LdPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DisPrt ON
		BkHd.DischargePort = DisPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DstPrt ON
		BkHd.DestinationPort = DstPrt.PortCode
	LEFT OUTER JOIN [Master].Merchant Agnt ON
		BkHd.AgentCode = Agnt.MerchantCode
	LEFT OUTER JOIN [Master].Merchant FF ON
		BkHd.FFCode = FF.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Cust ON
		BkHd.CustomerCode = Cust.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Ownr ON
		BkHd.OwnerCode = Ownr.MerchantCode
	LEFT OUTER JOIN [Master].[Vessel] Vsl ON
		BkHd.VesselID = Vsl.VesselID
	LEFT OUTER JOIN BookingTypeDescription BkDesc ON
		BkHd.BookingType = BkDesc.LookupID
	LEFT OUTER JOIN TradeModeDescription TdDesc ON
		DstPrt.TradeMode = TdDesc.LookupID
	WHERE	BkHd.[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderSelect]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingHeader] Records from [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderList] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderList] 
	@BranchID SMALLINT
AS 
BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup]WHERE [LookupCategory] ='BookingType'),
	TradeModeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup] WHERE [LookupCategory] ='TradeMode')
	SELECT	BkHd.[BranchID], BkHd.[OrderNo], BkHd.[BookingBLNo], BkHd.[SubBookingBLNo], BkHd.[BookingDate], BkHd.[BookingType], BkHd.[OrderType], 
			BkHd.[AgentCode], BkHd.[OwnerCode], BkHd.[FFCode], BkHd.[CustomerCode], BkHd.[VesselScheduleID], 
			BkHd.[VesselID], BkHd.[VoyageNo], BkHd.[Terminal], BkHd.[LoadingPort], BkHd.[DischargePort], BkHd.[DestinationPort], BkHd.[ETA], BkHd.[ETD], 
			BkHd.[PortCutOff], BkHd.[PortCutOffReefer], BkHd.[YardCutOffDry], BkHd.[YardCutOffReefer], BkHd.[CFSCutOffDry], BkHd.[CFSCutOffReefer], 
			BkHd.[NextPrevLoc], BkHd.[AllowLateGate], BkHd.[TotalQty], BkHd.[UOM], BkHd.[TotalVolume], BkHd.[TotalWeight], 
			BkHd.[IsEdi], BkHd.[IsCancel], BkHd.[IsTranferred], BkHd.[IsApproved], BkHd.[IsClosed], BkHd.[CreatedBy], BkHd.[CreatedOn], BkHd.[ModifiedBy], BkHd.[ModifiedOn], 
			ISNULL(LdPrt.PortName,'') As LoadingPortName,ISNULL(DisPrt.PortName,'') As DischargePortName,ISNULL(DstPrt.PortName,'') As DestinationPortName,
			ISNULL(Agnt.MerchantName,'') As AgentName, ISNULL(COALESCE(NULLIF(BkHd.FFName,''),FF.MerchantName),'') As FFName, ISNULL(COALESCE(NULLIF(BkHd.CustomerName,''),Cust.MerchantName),'') As CustomerName,
			ISNULL(Ownr.MerchantName,'') As OwnerName, ISNULL(vsl.VesselName,'') As VesselName,
			ISNULL(BkDesc.LookUpCode,'') As BookingTypeDescription,
			ISNULL(TdDesc.LookupCode,'') As TradeMode	
	FROM	[Depot].[BookingHeader] BkHd
	LEFT OUTER JOIN [Master].[Port] LdPrt ON
		BkHd.LoadingPort = LdPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DisPrt ON
		BkHd.DischargePort = DisPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DstPrt ON
		BkHd.DestinationPort = DstPrt.PortCode
	LEFT OUTER JOIN [Master].Merchant Agnt ON
		BkHd.AgentCode = Agnt.MerchantCode
	LEFT OUTER JOIN [Master].Merchant FF ON
		BkHd.FFCode = FF.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Cust ON
		BkHd.CustomerCode = Cust.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Ownr ON
		BkHd.OwnerCode = Ownr.MerchantCode
	LEFT OUTER JOIN [Master].[Vessel] Vsl ON
		BkHd.VesselID = Vsl.VesselID
	LEFT OUTER JOIN BookingTypeDescription BkDesc ON
		BkHd.BookingType = BkDesc.LookupID
	LEFT OUTER JOIN TradeModeDescription TdDesc ON
		DstPrt.TradeMode = TdDesc.LookupID
	WHERE	BkHd.[BranchID] = @BranchID 

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingHeader] Records from [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderPageView] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderPageView] 
	@BranchID SMALLINT,
	@fetchrows bigint
AS 
BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup]WHERE [LookupCategory] ='BookingType'),
	TradeModeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup] WHERE [LookupCategory] ='TradeMode')
	SELECT	BkHd.[BranchID], BkHd.[OrderNo], BkHd.[BookingBLNo], BkHd.[SubBookingBLNo], BkHd.[BookingDate], BkHd.[BookingType], BkHd.[OrderType], 
			BkHd.[AgentCode], BkHd.[OwnerCode], BkHd.[FFCode], BkHd.[CustomerCode], BkHd.[VesselScheduleID], 
			BkHd.[VesselID], BkHd.[VoyageNo], BkHd.[Terminal], BkHd.[LoadingPort], BkHd.[DischargePort], BkHd.[DestinationPort], BkHd.[ETA], BkHd.[ETD], 
			BkHd.[PortCutOff], BkHd.[PortCutOffReefer], BkHd.[YardCutOffDry], BkHd.[YardCutOffReefer], BkHd.[CFSCutOffDry], BkHd.[CFSCutOffReefer], 
			BkHd.[NextPrevLoc], BkHd.[AllowLateGate], BkHd.[TotalQty], BkHd.[UOM], BkHd.[TotalVolume], BkHd.[TotalWeight], 
			BkHd.[IsEdi], BkHd.[IsCancel], BkHd.[IsTranferred], BkHd.[IsApproved], BkHd.[IsClosed], BkHd.[CreatedBy], BkHd.[CreatedOn], BkHd.[ModifiedBy], BkHd.[ModifiedOn], 
			ISNULL(LdPrt.PortName,'') As LoadingPortName,ISNULL(DisPrt.PortName,'') As DischargePortName,ISNULL(DstPrt.PortName,'') As DestinationPortName,
			ISNULL(Agnt.MerchantName,'') As AgentName, ISNULL(COALESCE(NULLIF(BkHd.FFName,''),FF.MerchantName),'') As FFName, ISNULL(COALESCE(NULLIF(BkHd.CustomerName,''),Cust.MerchantName),'') As CustomerName,
			ISNULL(Ownr.MerchantName,'') As OwnerName, ISNULL(vsl.VesselName,'') As VesselName,
			ISNULL(BkDesc.LookUpCode,'') As BookingTypeDescription,
			ISNULL(TdDesc.LookupCode,'') As TradeMode	
	FROM	[Depot].[BookingHeader] BkHd
	LEFT OUTER JOIN [Master].[Port] LdPrt ON
		BkHd.LoadingPort = LdPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DisPrt ON
		BkHd.DischargePort = DisPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DstPrt ON
		BkHd.DestinationPort = DstPrt.PortCode
	LEFT OUTER JOIN [Master].Merchant Agnt ON
		BkHd.AgentCode = Agnt.MerchantCode
	LEFT OUTER JOIN [Master].Merchant FF ON
		BkHd.FFCode = FF.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Cust ON
		BkHd.CustomerCode = Cust.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Ownr ON
		BkHd.OwnerCode = Ownr.MerchantCode
	LEFT OUTER JOIN [Master].[Vessel] Vsl ON
		BkHd.VesselID = Vsl.VesselID
	LEFT OUTER JOIN BookingTypeDescription BkDesc ON
		BkHd.BookingType = BkDesc.LookupID
	LEFT OUTER JOIN TradeModeDescription TdDesc ON
		DstPrt.TradeMode = TdDesc.LookupID
	WHERE	BkHd.[BranchID] = @BranchID
	ORDER BY [OrderNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderRecordCount] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[BookingHeader]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
AS 

BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup]WHERE [LookupCategory] ='BookingType'),
	TradeModeDescription As (Select LookupID,LookUpCode FROM [Config].[Lookup] WHERE [LookupCategory] ='TradeMode')
	SELECT	BkHd.[BranchID], BkHd.[OrderNo], BkHd.[BookingBLNo], BkHd.[SubBookingBLNo], BkHd.[BookingDate], BkHd.[BookingType], BkHd.[OrderType], 
			BkHd.[AgentCode], BkHd.[OwnerCode], BkHd.[FFCode], BkHd.[CustomerCode], BkHd.[VesselScheduleID], 
			BkHd.[VesselID], BkHd.[VoyageNo], BkHd.[Terminal], BkHd.[LoadingPort], BkHd.[DischargePort], BkHd.[DestinationPort], BkHd.[ETA], BkHd.[ETD], 
			BkHd.[PortCutOff], BkHd.[PortCutOffReefer], BkHd.[YardCutOffDry], BkHd.[YardCutOffReefer], BkHd.[CFSCutOffDry], BkHd.[CFSCutOffReefer], 
			BkHd.[NextPrevLoc], BkHd.[AllowLateGate], BkHd.[TotalQty], BkHd.[UOM], BkHd.[TotalVolume], BkHd.[TotalWeight], 
			BkHd.[IsEdi], BkHd.[IsCancel], BkHd.[IsTranferred], BkHd.[IsApproved], BkHd.[IsClosed], BkHd.[CreatedBy], BkHd.[CreatedOn], BkHd.[ModifiedBy], BkHd.[ModifiedOn], 
			ISNULL(LdPrt.PortName,'') As LoadingPortName,ISNULL(DisPrt.PortName,'') As DischargePortName,ISNULL(DstPrt.PortName,'') As DestinationPortName,
			ISNULL(Agnt.MerchantName,'') As AgentName, ISNULL(COALESCE(NULLIF(BkHd.FFName,''),FF.MerchantName),'') As FFName, ISNULL(COALESCE(NULLIF(BkHd.CustomerName,''),Cust.MerchantName),'') As CustomerName,
			ISNULL(Ownr.MerchantName,'') As OwnerName, ISNULL(vsl.VesselName,'') As VesselName,
			ISNULL(BkDesc.LookUpCode,'') As BookingTypeDescription,
			ISNULL(TdDesc.LookupCode,'') As TradeMode	
	FROM	[Depot].[BookingHeader] BkHd
	LEFT OUTER JOIN [Master].[Port] LdPrt ON
		BkHd.LoadingPort = LdPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DisPrt ON
		BkHd.DischargePort = DisPrt.PortCode
	LEFT OUTER JOIN [Master].[Port] DstPrt ON
		BkHd.DestinationPort = DstPrt.PortCode
	LEFT OUTER JOIN [Master].Merchant Agnt ON
		BkHd.AgentCode = Agnt.MerchantCode
	LEFT OUTER JOIN [Master].Merchant FF ON
		BkHd.FFCode = FF.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Cust ON
		BkHd.CustomerCode = Cust.MerchantCode
	LEFT OUTER JOIN [Master].Merchant Ownr ON
		BkHd.OwnerCode = Ownr.MerchantCode
	LEFT OUTER JOIN [Master].[Vessel] Vsl ON
		BkHd.VesselID = Vsl.VesselID
	LEFT OUTER JOIN BookingTypeDescription BkDesc ON
		BkHd.BookingType = BkDesc.LookupID
	LEFT OUTER JOIN TradeModeDescription TdDesc ON
		DstPrt.TradeMode = TdDesc.LookupID
	WHERE  BkHd.[BranchID] = @BranchID  
	       AND BkHd.[OrderNo] LIKE '%' + @OrderNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [BookingHeader] Record Into [BookingHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderInsert] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderInsert] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @BookingBLNo nvarchar(50),
    @SubBookingBLNo nvarchar(50),
    @BookingDate datetime,
    @BookingType smallint,
    @OrderType varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @FFCode nvarchar(10),
    @FFName nvarchar(255),
    @CustomerCode nvarchar(10),
    @CustomerName nvarchar(255),
    @VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNo nvarchar(20),
    @Terminal nvarchar(50),
    @LoadingPort nvarchar(50),
    @DischargePort nvarchar(50),
    @DestinationPort nvarchar(50),
    @ETA datetime,
    @ETD datetime,
    @PortCutOff datetime,
    @PortCutOffReefer datetime,
    @YardCutOffDry datetime,
    @YardCutOffReefer datetime,
    @CFSCutOffDry datetime,
    @CFSCutOffReefer datetime,
    @NextPrevLoc nvarchar(50),
    @AllowLateGate bit,
    @TotalQty varchar(10),
    @UOM varchar(10),
    @TotalVolume varchar(10),
    @TotalWeight varchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewOrderNo varchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Depot].[BookingHeader] (
			[BranchID], [OrderNo], [BookingBLNo], [SubBookingBLNo], [BookingDate], [BookingType], [OrderType], [AgentCode], [OwnerCode], 
			[FFCode], [FFName], [CustomerCode], [CustomerName], [VesselScheduleID], [VesselID], [VoyageNo], [Terminal], [LoadingPort], 
			[DischargePort], [DestinationPort], [ETA], [ETD], [PortCutOff], [PortCutOffReefer], [YardCutOffDry], [YardCutOffReefer], 
			[CFSCutOffDry], [CFSCutOffReefer], [NextPrevLoc], [AllowLateGate], [TotalQty], [UOM], [TotalVolume], [TotalWeight], 
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @BookingBLNo, @SubBookingBLNo, @BookingDate, @BookingType, @OrderType, @AgentCode, @OwnerCode, 
			@FFCode, @FFName, @CustomerCode, @CustomerName, @VesselScheduleID, @VesselID, @VoyageNo, @Terminal, @LoadingPort, 
			@DischargePort, @DestinationPort, @ETA, @ETD, @PortCutOff, @PortCutOffReefer, @YardCutOffDry, @YardCutOffReefer, 
			@CFSCutOffDry, @CFSCutOffReefer, @NextPrevLoc, @AllowLateGate, @TotalQty, @UOM, @TotalVolume, @TotalWeight, 
			@CreatedBy, GETUTCDATE()
            
	Select @NewOrderNo = @OrderNo			
			   
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [BookingHeader] Record Into [BookingHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderUpdate] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderUpdate] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @BookingBLNo nvarchar(50),
    @SubBookingBLNo nvarchar(50),
    @BookingDate datetime,
    @BookingType smallint,
    @OrderType varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @FFCode nvarchar(10),
    @FFName nvarchar(255),
    @CustomerCode nvarchar(10),
    @CustomerName nvarchar(255),
    @VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNo nvarchar(20),
    @Terminal nvarchar(50),
    @LoadingPort nvarchar(50),
    @DischargePort nvarchar(50),
    @DestinationPort nvarchar(50),
    @ETA datetime,
    @ETD datetime,
    @PortCutOff datetime,
    @PortCutOffReefer datetime,
    @YardCutOffDry datetime,
    @YardCutOffReefer datetime,
    @CFSCutOffDry datetime,
    @CFSCutOffReefer datetime,
    @NextPrevLoc nvarchar(50),
    @AllowLateGate bit,
    @TotalQty varchar(10),
    @UOM varchar(10),
    @TotalVolume varchar(10),
    @TotalWeight varchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewOrderNo varchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Depot].[BookingHeader]
	SET		[BookingBLNo] = @BookingBLNo, [SubBookingBLNo] = @SubBookingBLNo, [BookingDate] = @BookingDate, [BookingType] = @BookingType, 
			[OrderType] = @OrderType, [AgentCode] = @AgentCode, [OwnerCode] = @OwnerCode, [FFCode] = @FFCode, [FFName] = @FFName, 
			[CustomerCode] = @CustomerCode, [CustomerName] = @CustomerName, [VesselScheduleID] = @VesselScheduleID, [VesselID] = @VesselID, 
			[VoyageNo] = @VoyageNo, [Terminal] = @Terminal, [LoadingPort] = @LoadingPort, [DischargePort] = @DischargePort, 
			[DestinationPort] = @DestinationPort, [ETA] = @ETA, [ETD] = @ETD, 
			[PortCutOff] = @PortCutOff, [PortCutOffReefer] = @PortCutOffReefer, [YardCutOffDry] = @YardCutOffDry, [YardCutOffReefer] = @YardCutOffReefer, 
			[CFSCutOffDry] = @CFSCutOffDry, [CFSCutOffReefer] = @CFSCutOffReefer, [NextPrevLoc] = @NextPrevLoc, [AllowLateGate] = @AllowLateGate, 
			[TotalQty] = @TotalQty, [UOM] = @UOM, [TotalVolume] = @TotalVolume, [TotalWeight] = @TotalWeight, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo

	Select @NewOrderNo = @OrderNo
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [BookingHeader] Record Into [BookingHeader] Table.
-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderSave] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderSave] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @BookingBLNo nvarchar(50),
    @SubBookingBLNo nvarchar(50),
    @BookingDate datetime,
    @BookingType smallint,
    @OrderType varchar(50),
    @AgentCode nvarchar(10),
    @OwnerCode nvarchar(10),
    @FFCode nvarchar(10),
    @FFName nvarchar(255),
    @CustomerCode nvarchar(10),
    @CustomerName nvarchar(255),
    @VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNo nvarchar(20),
    @Terminal nvarchar(50),
    @LoadingPort nvarchar(50),
    @DischargePort nvarchar(50),
    @DestinationPort nvarchar(50),
    @ETA datetime,
    @ETD datetime,
    @PortCutOff datetime,
    @PortCutOffReefer datetime,
    @YardCutOffDry datetime,
    @YardCutOffReefer datetime,
    @CFSCutOffDry datetime,
    @CFSCutOffReefer datetime,
    @NextPrevLoc nvarchar(50),
    @AllowLateGate bit,
    @TotalQty varchar(10),
    @UOM varchar(10),
    @TotalVolume varchar(10),
    @TotalWeight varchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewOrderNo varchar(50) OUTPUT
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[BookingHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo)>0
	BEGIN
	    Exec [Depot].[usp_BookingHeaderUpdate] 
			@BranchID, @OrderNo, @BookingBLNo, @SubBookingBLNo, @BookingDate, @BookingType, @OrderType, @AgentCode, @OwnerCode, 
			@FFCode, @FFName, @CustomerCode, @CustomerName, @VesselScheduleID, @VesselID, @VoyageNo, @Terminal, 
			@LoadingPort, @DischargePort, @DestinationPort, @ETA, @ETD, @PortCutOff, @PortCutOffReefer, @YardCutOffDry, @YardCutOffReefer, 
			@CFSCutOffDry, @CFSCutOffReefer, @NextPrevLoc, @AllowLateGate, @TotalQty, @UOM, @TotalVolume, @TotalWeight, 
			@CreatedBy, @ModifiedBy ,@NewOrderNo = @NewOrderNo OUTPUT


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@BookingTypeDesc = LookupDescription,@OrderNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@BookingType
		
		print @BookingTypeDesc
		
		IF @BookingTypeDesc='Import' 
			SET @DocID ='Depot\Order(Import)'
		ELSE IF @BookingTypeDesc='Export' 
			SET @DocID ='Depot\Order(Export)'
		ELSE IF @BookingTypeDesc='Pos-In' 
			SET @DocID ='Depot\Order(Pos-In)'
		ELSE IF @BookingTypeDesc='Pos-Out' 
			SET @DocID ='Depot\Order(Pos-Out)'
		ELSE IF @BookingTypeDesc='Local' 
			SET @DocID ='Depot\Order(Local)'
		ELSE IF @BookingTypeDesc='Repo' 
			SET @DocID ='Depot\Order(Repo)'
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 10, 'Depot\Order(Import)', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @OrderNo OUTPUT

		 


	    Exec [Depot].[usp_BookingHeaderInsert] 
			@BranchID, @OrderNo, @BookingBLNo, @SubBookingBLNo, @BookingDate, @BookingType, @OrderType, @AgentCode, @OwnerCode, 
			@FFCode, @FFName, @CustomerCode, @CustomerName, @VesselScheduleID, @VesselID, @VoyageNo, @Terminal, 
			@LoadingPort, @DischargePort, @DestinationPort, @ETA, @ETD, @PortCutOff, @PortCutOffReefer, @YardCutOffDry, @YardCutOffReefer, 
			@CFSCutOffDry, @CFSCutOffReefer, @NextPrevLoc, @AllowLateGate, @TotalQty, @UOM, @TotalVolume, @TotalWeight, 
			@CreatedBy, @ModifiedBy ,@NewOrderNo = @NewOrderNo OUTPUT

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[BookingHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [BookingHeader] Record  based on [BookingHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingHeaderDelete] 
END 
GO
CREATE PROC [Depot].[usp_BookingHeaderDelete] 
    @BranchID smallint,
    @OrderNo varchar(50)
AS 

	
BEGIN

	UPDATE	[Depot].[BookingHeader]
	SET	IsCancel = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo

	 


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingHeaderDelete]
-- ========================================================================================================================================

GO
 