
-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TruckMovementHeader] Record based on [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderSelect] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderSelect] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50)
AS 

BEGIN

	;With TruckCategoryLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.TruckNo, Hd.[TransactionDate], Hd.[HaulierCode], Hd.[TruckCategory], Hd.[YardLocation], Hd.[Status], 
			Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn],
			ISNULL(Hlr.MerchantName,'') As HaulierName, ISNULL(Yrd.MerchantName,'') As YardLocationName, 
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription
	FROM	[Depot].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hlr ON 
		Hd.HaulierCode = Hlr.MerchantCode
		And Hlr.IsHaulier = CAST(1 as bit)
	Left Outer Join Master.Merchant Yrd ON 
		Hd.YardLocation = Yrd.MerchantCode
		And Yrd.IsYard = CAST(1 as bit)
	Left Outer Join TruckCategoryLookup Trk ON 
		Hd.TruckCategory = Trk.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[TransactionNo] = @TransactionNo  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TruckMovementHeader] Records from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderList] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderList] 
	@BranchID SMALLINT
AS 
BEGIN

	;With TruckCategoryLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.TruckNo,Hd.[TransactionDate], Hd.[HaulierCode], Hd.[TruckCategory], Hd.[YardLocation], Hd.[Status], 
			Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn],
			ISNULL(Hlr.MerchantName,'') As HaulierName, ISNULL(Yrd.MerchantName,'') As YardLocationName, 
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription
	FROM	[Depot].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hlr ON 
		Hd.HaulierCode = Hlr.MerchantCode
		And Hlr.IsHaulier = CAST(1 as bit)
	Left Outer Join Master.Merchant Yrd ON 
		Hd.YardLocation = Yrd.MerchantCode
		And Yrd.IsYard = CAST(1 as bit)
	Left Outer Join TruckCategoryLookup Trk ON 
		Hd.TruckCategory = Trk.LookupID
	WHERE	Hd.[BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TruckMovementHeader] Records from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderPageView] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderPageView] 
	@BranchID SMALLINT,
	@fetchrows bigint
AS 
BEGIN

	;With TruckCategoryLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo],Hd.TruckNo, Hd.[TransactionDate], Hd.[HaulierCode], Hd.[TruckCategory], Hd.[YardLocation], Hd.[Status], 
			Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn],
			ISNULL(Hlr.MerchantName,'') As HaulierName, ISNULL(Yrd.MerchantName,'') As YardLocationName, 
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription
	FROM	[Depot].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hlr ON 
		Hd.HaulierCode = Hlr.MerchantCode
		And Hlr.IsHaulier = CAST(1 as bit)
	Left Outer Join Master.Merchant Yrd ON 
		Hd.YardLocation = Yrd.MerchantCode
		And Yrd.IsYard = CAST(1 as bit)
	Left Outer Join TruckCategoryLookup Trk ON 
		Hd.TruckCategory = Trk.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
	ORDER BY [TransactionNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderRecordCount] 
	@BranchID SMALLINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[TruckMovementHeader]
	WHERE	[BranchID] = @BranchID  
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TruckMovementHeader] Record based on [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderAutoCompleteSearch] 
    @BranchID SMALLINT,
    @TransactionNo NVARCHAR(50)
AS 

BEGIN

	;With TruckCategoryLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo],Hd.TruckNo, Hd.[TransactionDate], Hd.[HaulierCode], Hd.[TruckCategory], Hd.[YardLocation], Hd.[Status], 
			Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn],
			ISNULL(Hlr.MerchantName,'') As HaulierName, ISNULL(Yrd.MerchantName,'') As YardLocationName, 
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription
	FROM	[Depot].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hlr ON 
		Hd.HaulierCode = Hlr.MerchantCode
		And Hlr.IsHaulier = CAST(1 as bit)
	Left Outer Join Master.Merchant Yrd ON 
		Hd.YardLocation = Yrd.MerchantCode
		And Yrd.IsYard = CAST(1 as bit)
	Left Outer Join TruckCategoryLookup Trk ON 
		Hd.TruckCategory = Trk.LookupID
	WHERE  Hd.[BranchID] = @BranchID  
	       AND Hd.[TransactionNo] LIKE '%' +  @TransactionNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderInsert] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderInsert] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
	@TruckNo nvarchar(20),
    @TransactionDate datetime,
    @HaulierCode nvarchar(10),
    @TruckCategory smallint,
    @YardLocation nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewTransactionNo nvarchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Depot].[TruckMovementHeader] (
			[BranchID], [TransactionNo],TruckNo, [TransactionDate], [HaulierCode], [TruckCategory], [YardLocation], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @TransactionNo,@TruckNo, @TransactionDate, @HaulierCode, @TruckCategory, @YardLocation, CAST(1 AS BIT), @CreatedBy, GETUTCDATE() 
         
	Select @NewTransactionNo=@TransactionNo
		       
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderUpdate] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderUpdate] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
	@TruckNo nvarchar(20),
    @TransactionDate datetime,
    @HaulierCode nvarchar(10),
    @TruckCategory smallint,
    @YardLocation nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewTransactionNo nvarchar(50) OUTPUT

AS 
 
	
BEGIN

	UPDATE [Depot].[TruckMovementHeader]
	SET		[TransactionDate] = @TransactionDate, [HaulierCode] = @HaulierCode, [TruckCategory] = @TruckCategory, 
			[YardLocation] = @YardLocation, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo
	
	Select @NewTransactionNo=@TransactionNo

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderSave] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderSave] 
    @BranchID smallint,
    @TransactionNo nvarchar(50),
	@TruckNo nvarchar(20),
    @TransactionDate datetime,
    @HaulierCode nvarchar(10),
    @TruckCategory smallint,
    @YardLocation nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewTransactionNo nvarchar(50) OUTPUT
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[TruckMovementHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo)>0
	BEGIN
	    Exec [Depot].[usp_TruckMovementHeaderUpdate] 
				@BranchID, @TransactionNo,@TruckNo, @TransactionDate, @HaulierCode, @TruckCategory, @YardLocation, @CreatedBy, @ModifiedBy ,@NewTransactionNo= @NewTransactionNo OUTPUT 


	END
	ELSE
	BEGIN

		
		Declare @Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)='Depot\TruckIn'
		
		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 10, 'Depot\TruckIn', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		
		Select @TransactionNo='',@Dt=GetUTCDate()

		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @TransactionNo OUTPUT

		 

	    Exec [Depot].[usp_TruckMovementHeaderInsert] 
				@BranchID, @TransactionNo,@TruckNo, @TransactionDate, @HaulierCode, @TruckCategory, @YardLocation, @CreatedBy, @ModifiedBy ,@NewTransactionNo= @NewTransactionNo OUTPUT


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[TruckMovementHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_TruckMovementHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [TruckMovementHeader] Record  based on [TruckMovementHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_TruckMovementHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_TruckMovementHeaderDelete] 
END 
GO
CREATE PROC [Depot].[usp_TruckMovementHeaderDelete] 
    @BranchID smallint,
    @TransactionNo nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Depot].[TruckMovementHeader]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo

	


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_TruckMovementHeaderDelete]
-- ========================================================================================================================================

GO
 
