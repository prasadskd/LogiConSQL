


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingMovement] Record based on [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementSelect] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementSelect] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @MovementCode VARCHAR(20)
AS 

BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [TransactionNo], [TransactionDate], [Status], [LocationCode], [TruckTransactionNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 
	       AND [ContainerKey] = @ContainerKey  
	       AND [MovementCode] = @MovementCode  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingMovement] Records from [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementList] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementList] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)

AS 
BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [TransactionNo], [TransactionDate], [Status], [LocationCode], [TruckTransactionNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingMovement] Records from [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingMovementPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementPageView] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementPageView] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [TransactionNo], [TransactionDate], [Status], [LocationCode], [TruckTransactionNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo
	ORDER BY [OrderNo] 
	 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingMovementRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementRecordCount] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[BookingMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingMovement] Record based on [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingMovementAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
     
AS 

BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [TransactionNo], [TransactionDate], [Status], [LocationCode], [TruckTransactionNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] LIKE '%' + @OrderNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementInsert] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementInsert] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey varchar(50),
    @MovementCode varchar(20),
    @Sequence smallint,
    @TransactionNo varchar(25),
    @TransactionDate datetime,
    @Status bit,
    @LocationCode varchar(10),
    @TruckTransactionNo varchar(30),
    @CreatedBy varchar(25),
    @CreatedOn datetime,
    @ModifiedBy varchar(25),
    @ModifiedOn datetime
AS 
  

BEGIN

	
	INSERT INTO [Depot].[BookingMovement] (
			[BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [TransactionNo], [TransactionDate], 
			[Status], [LocationCode], [TruckTransactionNo], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @TransactionNo, @TransactionDate, 
			@Status, @LocationCode, @TruckTransactionNo, @CreatedBy, getutcdate()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementUpdate] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementUpdate] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey varchar(50),
    @MovementCode varchar(20),
    @Sequence smallint,
    @TransactionNo varchar(25),
    @TransactionDate datetime,
    @Status bit,
    @LocationCode varchar(10),
    @TruckTransactionNo varchar(30),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)

AS 
 
	
BEGIN

	UPDATE	[Depot].[BookingMovement]
	SET		[BranchID] = @BranchID, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [MovementCode] = @MovementCode, [Sequence] = @Sequence, 
			[TransactionNo] = @TransactionNo, [TransactionDate] = @TransactionDate, [Status] = @Status, [LocationCode] = @LocationCode, 
			[TruckTransactionNo] = @TruckTransactionNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = Getutcdate()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [MovementCode] = @MovementCode
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementSave] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementSave] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey varchar(50),
    @MovementCode varchar(20),
    @Sequence smallint,
    @TransactionNo varchar(25),
    @TransactionDate datetime,
    @Status bit,
    @LocationCode varchar(10),
    @TruckTransactionNo varchar(30),
    @CreatedBy varchar(25),
    @CreatedOn datetime,
    @ModifiedBy varchar(25),
    @ModifiedOn datetime
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[BookingMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode)>0
	BEGIN
	    Exec [Depot].[usp_BookingMovementUpdate] 
			@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @TransactionNo, @TransactionDate, @Status, @LocationCode, 
			@TruckTransactionNo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_BookingMovementInsert] 
		@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @TransactionNo, @TransactionDate, @Status, @LocationCode, 
			@TruckTransactionNo, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[BookingMovementSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [BookingMovement] Record  based on [BookingMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingMovementDelete] 
END 
GO
CREATE PROC [Depot].[usp_BookingMovementDelete] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey varchar(50),
    @MovementCode varchar(20)
AS 

	
BEGIN

	UPDATE	[Depot].[BookingMovement]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode

 


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingMovementDelete]
-- ========================================================================================================================================

GO

 
