
-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerStatusUpdate] Record based on [ContainerStatusUpdate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateSelect] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateSelect] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15),
    @ContainerStatus NVARCHAR(10),
    @DocumentNo NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [ContainerNo], [ContainerStatus], [DocumentNo], [StartDateTime], [EndDateTime], [ApprovalDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[ContainerStatusUpdate]
	WHERE  [BranchID] = @BranchID  
	       AND [ContainerNo] = @ContainerNo  
	       AND [ContainerStatus] = @ContainerStatus  
	       AND [DocumentNo] = @DocumentNo  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerStatusUpdate] Records from [ContainerStatusUpdate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateList] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateList] 
	@BranchID SMALLINT,
    @ContainerNo VARCHAR(15)

AS 
BEGIN

	SELECT	[BranchID], [ContainerNo], [ContainerStatus], [DocumentNo], [StartDateTime], [EndDateTime], [ApprovalDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[ContainerStatusUpdate]
	WHERE	[BranchID] = @BranchID  
			AND [ContainerNo] = @ContainerNo

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdatePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerStatusUpdate] Records from [ContainerStatusUpdate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdatePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdatePageView] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdatePageView] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15),
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [ContainerNo], [ContainerStatus], [DocumentNo], [StartDateTime], [EndDateTime], [ApprovalDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[ContainerStatusUpdate]
	Where [BranchID] = @BranchID  
	       AND [ContainerNo] = @ContainerNo  
	Order By ContainerNo
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdatePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerStatusUpdate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateRecordCount] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[ContainerStatusUpdate]
	WHERE  [BranchID] = @BranchID  
	       AND [ContainerNo] = @ContainerNo
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerStatusUpdate] Record based on [ContainerStatusUpdate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateAutoCompleteSearch] 
    @BranchID SMALLINT,
    @ContainerNo VARCHAR(15)
    
AS 

BEGIN

	SELECT [BranchID], [ContainerNo], [ContainerStatus], [DocumentNo], [StartDateTime], [EndDateTime], [ApprovalDateTime], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[ContainerStatusUpdate]
	WHERE  [BranchID] = @BranchID  
	       AND [ContainerNo] LIKE '%' + @ContainerNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerStatusUpdate] Record Into [ContainerStatusUpdate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateInsert] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateInsert] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @ContainerStatus nvarchar(10),
    @DocumentNo nvarchar(50),
    @StartDateTime datetime,
    @EndDateTime datetime,
    @ApprovalDateTime datetime,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Depot].[ContainerStatusUpdate] (
			[BranchID], [ContainerNo], [ContainerStatus], [DocumentNo], [StartDateTime], [EndDateTime], [ApprovalDateTime], 
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ContainerNo, @ContainerStatus, @DocumentNo, @StartDateTime, @EndDateTime, @ApprovalDateTime, 
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerStatusUpdate] Record Into [ContainerStatusUpdate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateUpdate] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateUpdate] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @ContainerStatus nvarchar(10),
    @DocumentNo nvarchar(50),
    @StartDateTime datetime,
    @EndDateTime datetime,
    @ApprovalDateTime datetime,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)
AS 
 
	
BEGIN

	UPDATE	[Depot].[ContainerStatusUpdate]
	SET		[ContainerStatus] = @ContainerStatus, [DocumentNo] = @DocumentNo, [StartDateTime] = @StartDateTime, 
			[EndDateTime] = @EndDateTime, [ApprovalDateTime] = @ApprovalDateTime, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [ContainerStatus] = @ContainerStatus
	       AND [DocumentNo] = @DocumentNo
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerStatusUpdate] Record Into [ContainerStatusUpdate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateSave] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateSave] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @ContainerStatus nvarchar(10),
    @DocumentNo nvarchar(50),
    @StartDateTime datetime,
    @EndDateTime datetime,
    @ApprovalDateTime datetime,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[ContainerStatusUpdate] 
		WHERE 	[BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [ContainerStatus] = @ContainerStatus
	       AND [DocumentNo] = @DocumentNo)>0
	BEGIN
	    Exec [Depot].[usp_ContainerStatusUpdateUpdate] 
				@BranchID, @ContainerNo, @ContainerStatus, @DocumentNo, @StartDateTime, @EndDateTime, @ApprovalDateTime, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_ContainerStatusUpdateInsert] 
				@BranchID, @ContainerNo, @ContainerStatus, @DocumentNo, @StartDateTime, @EndDateTime, @ApprovalDateTime, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[ContainerStatusUpdateSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_ContainerStatusUpdateDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerStatusUpdate] Record  based on [ContainerStatusUpdate]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_ContainerStatusUpdateDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_ContainerStatusUpdateDelete] 
END 
GO
CREATE PROC [Depot].[usp_ContainerStatusUpdateDelete] 
    @BranchID smallint,
    @ContainerNo varchar(15),
    @ContainerStatus nvarchar(10),
    @DocumentNo nvarchar(50)
AS 

	
BEGIN

 	DELETE
	FROM   [Depot].[ContainerStatusUpdate]
	WHERE  [BranchID] = @BranchID
	       AND [ContainerNo] = @ContainerNo
	       AND [ContainerStatus] = @ContainerStatus
	       AND [DocumentNo] = @DocumentNo
	 


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_ContainerStatusUpdateDelete]
-- ========================================================================================================================================

GO
 
