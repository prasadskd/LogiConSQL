

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingContainer] Record based on [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingContainerSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerSelect] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerSelect] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [RequiredDate], [IMOCode], [UNNumber], 
			[Weight], [Volume], [SealNo1], [SealNo2], [Temperature], [TempratureMode], [OverSize], [Humidity], [Vent], [VentMode], 
			[ContainerHeight], [GenSetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], [PickupDropOffMode], [TransportMode], 
			[StateIndicator], [Remarks], [EmptyInDate], [EmptyOutDate], [LoadedInDate], [LoadedOutDate], [StuffUnStuffDate], 
			[EmptyStorageStartDate], [LoadedStorageStartDate], [EmptyStorageEndDate], [LoadedStorageEndDate], 
			[Status], [NextMovement], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingContainer]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo  
	       AND [ContainerKey] = @ContainerKey  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingContainer] Records from [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingContainerList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerList] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerList] 
	@BranchID smallint,
	@OrderNo varchar(50)
AS 
BEGIN

	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [RequiredDate], [IMOCode], [UNNumber], 
			[Weight], [Volume], [SealNo1], [SealNo2], [Temperature], [TempratureMode], [OverSize], [Humidity], [Vent], [VentMode], 
			[ContainerHeight], [GenSetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], [PickupDropOffMode], [TransportMode], 
			[StateIndicator], [Remarks], [EmptyInDate], [EmptyOutDate], [LoadedInDate], [LoadedOutDate], [StuffUnStuffDate], 
			[EmptyStorageStartDate], [LoadedStorageStartDate], [EmptyStorageEndDate], [LoadedStorageEndDate], [Status], [NextMovement], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingContainer]
	Where BranchID = @BranchID
	And OrderNo = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingContainer] Records from [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingContainerPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerPageView] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerPageView] 
	@BranchID smallint,
	@OrderNo nvarchar(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [RequiredDate], [IMOCode], 
			[UNNumber], [Weight], [Volume], [SealNo1], [SealNo2], [Temperature], [TempratureMode], [OverSize], [Humidity], [Vent], 
			[VentMode], [ContainerHeight], [GenSetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], [PickupDropOffMode], 
			[TransportMode], [StateIndicator], [Remarks], [EmptyInDate], [EmptyOutDate], [LoadedInDate], [LoadedOutDate], 
			[StuffUnStuffDate], [EmptyStorageStartDate], [LoadedStorageStartDate], [EmptyStorageEndDate], [LoadedStorageEndDate], 
			[Status], [NextMovement], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingContainer]
	WHERE BranchID = @BranchID
	And OrderNo = @OrderNo
	ORDER BY [ContainerKey]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingContainerRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerRecordCount] 
	@BranchID smallint,
	@OrderNo varchar(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[BookingContainer]
	Where BranchID = @BranchID
		And OrderNo = @OrderNo
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingContainer] Record based on [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingContainerAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
     
AS 

BEGIN

	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [RequiredDate], [IMOCode], 
			[UNNumber], [Weight], [Volume], [SealNo1], [SealNo2], [Temperature], [TempratureMode], [OverSize], [Humidity], 
			[Vent], [VentMode], [ContainerHeight], [GenSetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], 
			[PickupDropOffMode], [TransportMode], [StateIndicator], [Remarks], [EmptyInDate], [EmptyOutDate], 
			[LoadedInDate], [LoadedOutDate], [StuffUnStuffDate], [EmptyStorageStartDate], [LoadedStorageStartDate], 
			[EmptyStorageEndDate], [LoadedStorageEndDate], [Status], [NextMovement], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingContainer]
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo]  LIKE '%' +  @OrderNo   + '%'
			
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingContainerInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerInsert] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerInsert] 
    @BranchID smallint,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @ContainerGrade smallint,
    @RequiredDate datetime,
    @IMOCode varchar(10),
    @UNNumber varchar(10),
    @Weight numeric(8, 2),
    @Volume numeric(8, 2),
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Temperature numeric(5, 2),
    @TempratureMode smallint,
    @OverSize varchar(20),
    @Humidity numeric(8, 2),
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @GenSetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @PickupDropOffMode smallint,
    @TransportMode smallint,
    @StateIndicator smallint,
    @Remarks nvarchar(MAX),
    @EmptyInDate datetime,
    @EmptyOutDate datetime,
    @LoadedInDate datetime,
    @LoadedOutDate datetime,
    @StuffUnStuffDate datetime,
    @EmptyStorageStartDate datetime,
    @LoadedStorageStartDate datetime,
    @EmptyStorageEndDate datetime,
    @LoadedStorageEndDate datetime,
    @Status smallint,
    @NextMovement varchar(25),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)

AS 
  

BEGIN

	
	INSERT INTO [Depot].[BookingContainer] (
			[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [RequiredDate], [IMOCode], [UNNumber], 
			[Weight], [Volume], [SealNo1], [SealNo2], [Temperature], [TempratureMode], [OverSize], [Humidity], [Vent], [VentMode], 
			[ContainerHeight], [GenSetIndicator], [MovementIndicator], [EFIndicator], [CargoCategory], [PickupDropOffMode], [TransportMode], 
			[StateIndicator], [Remarks], [EmptyInDate], [EmptyOutDate], [LoadedInDate], [LoadedOutDate], [StuffUnStuffDate], 
			[EmptyStorageStartDate], [LoadedStorageStartDate], [EmptyStorageEndDate], [LoadedStorageEndDate], [Status], 
			[NextMovement], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @RequiredDate, @IMOCode, @UNNumber, 
			@Weight, @Volume, @SealNo1, @SealNo2, @Temperature, @TempratureMode, @OverSize, @Humidity, @Vent, @VentMode, 
			@ContainerHeight, @GenSetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @PickupDropOffMode, @TransportMode, 
			@StateIndicator, @Remarks, @EmptyInDate, @EmptyOutDate, @LoadedInDate, @LoadedOutDate, @StuffUnStuffDate, 
			@EmptyStorageStartDate, @LoadedStorageStartDate, @EmptyStorageEndDate, @LoadedStorageEndDate, @Status, 
			@NextMovement, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingContainerUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerUpdate] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerUpdate] 
    @BranchID smallint,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @ContainerGrade smallint,
    @RequiredDate datetime,
    @IMOCode varchar(10),
    @UNNumber varchar(10),
    @Weight numeric(8, 2),
    @Volume numeric(8, 2),
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Temperature numeric(5, 2),
    @TempratureMode smallint,
    @OverSize varchar(20),
    @Humidity numeric(8, 2),
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @GenSetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @PickupDropOffMode smallint,
    @TransportMode smallint,
    @StateIndicator smallint,
    @Remarks nvarchar(MAX),
    @EmptyInDate datetime,
    @EmptyOutDate datetime,
    @LoadedInDate datetime,
    @LoadedOutDate datetime,
    @StuffUnStuffDate datetime,
    @EmptyStorageStartDate datetime,
    @LoadedStorageStartDate datetime,
    @EmptyStorageEndDate datetime,
    @LoadedStorageEndDate datetime,
    @Status smallint,
    @NextMovement varchar(25),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)
AS 
 
	
BEGIN

	UPDATE	[Depot].[BookingContainer]
	SET		[ContainerNo] = @ContainerNo, [Size] = @Size, [Type] = @Type, [ContainerGrade] = @ContainerGrade, [RequiredDate] = @RequiredDate, 
			[IMOCode] = @IMOCode, [UNNumber] = @UNNumber, [Weight] = @Weight, [Volume] = @Volume, [SealNo1] = @SealNo1, [SealNo2] = @SealNo2, 
			[Temperature] = @Temperature, [TempratureMode] = @TempratureMode, [OverSize] = @OverSize, [Humidity] = @Humidity, [Vent] = @Vent, 
			[VentMode] = @VentMode, [ContainerHeight] = @ContainerHeight, [GenSetIndicator] = @GenSetIndicator, [MovementIndicator] = @MovementIndicator, 
			[EFIndicator] = @EFIndicator, [CargoCategory] = @CargoCategory, [PickupDropOffMode] = @PickupDropOffMode, [TransportMode] = @TransportMode, 
			[StateIndicator] = @StateIndicator, [Remarks] = @Remarks, [EmptyInDate] = @EmptyInDate, [EmptyOutDate] = @EmptyOutDate, 
			[LoadedInDate] = @LoadedInDate, [LoadedOutDate] = @LoadedOutDate, [StuffUnStuffDate] = @StuffUnStuffDate, 
			[EmptyStorageStartDate] = @EmptyStorageStartDate, [LoadedStorageStartDate] = @LoadedStorageStartDate, 
			[EmptyStorageEndDate] = @EmptyStorageEndDate, [LoadedStorageEndDate] = @LoadedStorageEndDate, [Status] = @Status, 
			[NextMovement] = @NextMovement, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingContainerSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerSave] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerSave] 
    @BranchID smallint,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @ContainerGrade smallint,
    @RequiredDate datetime,
    @IMOCode varchar(10),
    @UNNumber varchar(10),
    @Weight numeric(8, 2),
    @Volume numeric(8, 2),
    @SealNo1 varchar(20),
    @SealNo2 varchar(20),
    @Temperature numeric(5, 2),
    @TempratureMode smallint,
    @OverSize varchar(20),
    @Humidity numeric(8, 2),
    @Vent numeric(8, 2),
    @VentMode smallint,
    @ContainerHeight smallint,
    @GenSetIndicator bit,
    @MovementIndicator smallint,
    @EFIndicator smallint,
    @CargoCategory smallint,
    @PickupDropOffMode smallint,
    @TransportMode smallint,
    @StateIndicator smallint,
    @Remarks nvarchar(MAX),
    @EmptyInDate datetime,
    @EmptyOutDate datetime,
    @LoadedInDate datetime,
    @LoadedOutDate datetime,
    @StuffUnStuffDate datetime,
    @EmptyStorageStartDate datetime,
    @LoadedStorageStartDate datetime,
    @EmptyStorageEndDate datetime,
    @LoadedStorageEndDate datetime,
    @Status smallint,
    @NextMovement varchar(25),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[BookingContainer] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Depot].[usp_BookingContainerUpdate] 
			@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @RequiredDate, @IMOCode, @UNNumber, 
			@Weight, @Volume, @SealNo1, @SealNo2, @Temperature, @TempratureMode, @OverSize, @Humidity, @Vent, @VentMode, 
			@ContainerHeight, @GenSetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @PickupDropOffMode, 
			@TransportMode, @StateIndicator, @Remarks, @EmptyInDate, @EmptyOutDate, @LoadedInDate, @LoadedOutDate, @StuffUnStuffDate, 
			@EmptyStorageStartDate, @LoadedStorageStartDate, @EmptyStorageEndDate, @LoadedStorageEndDate, @Status, 
			@NextMovement, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_BookingContainerInsert] 
			@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @RequiredDate, @IMOCode, @UNNumber, 
			@Weight, @Volume, @SealNo1, @SealNo2, @Temperature, @TempratureMode, @OverSize, @Humidity, @Vent, @VentMode, 
			@ContainerHeight, @GenSetIndicator, @MovementIndicator, @EFIndicator, @CargoCategory, @PickupDropOffMode, 
			@TransportMode, @StateIndicator, @Remarks, @EmptyInDate, @EmptyOutDate, @LoadedInDate, @LoadedOutDate, @StuffUnStuffDate, 
			@EmptyStorageStartDate, @LoadedStorageStartDate, @EmptyStorageEndDate, @LoadedStorageEndDate, @Status, 
			@NextMovement, @CreatedBy, @ModifiedBy

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[BookingContainerSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingContainerDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [BookingContainer] Record  based on [BookingContainer]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingContainerDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingContainerDelete] 
END 
GO
CREATE PROC [Depot].[usp_BookingContainerDelete] 
    @BranchID smallint,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50)
AS 

	
BEGIN

	UPDATE	[Depot].[BookingContainer]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey

	 

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingContainerDelete]
-- ========================================================================================================================================

GO

 
