

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingStatement] Record based on [BookingStatement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingStatementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementSelect] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementSelect] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50),
    @ChargeCode NVARCHAR(50),
    @PaymentTerm SMALLINT,
    @PaymentTo SMALLINT,
    @MovementCode VARCHAR(20),
    @Qty DECIMAL(10, 3),
    @Price NUMERIC(12, 2)
AS 

BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [ChargeCode], [PaymentTerm], [PaymentTo], [MovementCode], [Qty], [Price], [Size], [Type], [IsWaived], [IsAutoLoad], [IsVAS], [DiscountType], [DiscountAmount], [ActualAmount], [IsSlabRate], [SlabRateFrom], [SlabRateTo], [TruckCategory], [CargoCategory], [BillingUnit], [ChargeType], [PaidQty], [PaidAmount], [RefundAmount], [Status], [LoadFrom], [IsSellRateChanged], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingStatement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo  
	       AND [ContainerKey] = @ContainerKey  
	       AND [ChargeCode] = @ChargeCode  
	       AND [PaymentTerm] = @PaymentTerm  
	       AND [PaymentTo] = @PaymentTo  
	       AND [MovementCode] = @MovementCode 
	       AND [Qty] = @Qty 
	       AND [Price] = @Price 
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingStatement] Records from [BookingStatement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingStatementList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementList] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementList] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
AS 
BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [ChargeCode], [PaymentTerm], [PaymentTo], [MovementCode], [Qty], [Price], [Size], [Type], [IsWaived], [IsAutoLoad], [IsVAS], [DiscountType], [DiscountAmount], [ActualAmount], [IsSlabRate], [SlabRateFrom], [SlabRateTo], [TruckCategory], [CargoCategory], [BillingUnit], [ChargeType], [PaidQty], [PaidAmount], [RefundAmount], [Status], [LoadFrom], [IsSellRateChanged], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingStatement]

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingStatement] Records from [BookingStatement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingStatementPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementPageView] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementPageView] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [ChargeCode], [PaymentTerm], [PaymentTo], [MovementCode], [Qty], [Price], [Size], [Type], [IsWaived], [IsAutoLoad], [IsVAS], [DiscountType], [DiscountAmount], [ActualAmount], [IsSlabRate], [SlabRateFrom], [SlabRateTo], [TruckCategory], [CargoCategory], [BillingUnit], [ChargeType], [PaidQty], [PaidAmount], [RefundAmount], [Status], [LoadFrom], [IsSellRateChanged], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingStatement]
	Where [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo  
	ORDER BY  [OrderNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingStatement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingStatementRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementRecordCount] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[BookingStatement]
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingStatement] Record based on [BookingStatement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingStatementAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [OrderNo], [ContainerKey], [ChargeCode], [PaymentTerm], [PaymentTo], [MovementCode], [Qty], [Price], [Size], [Type], [IsWaived], [IsAutoLoad], [IsVAS], [DiscountType], [DiscountAmount], [ActualAmount], [IsSlabRate], [SlabRateFrom], [SlabRateTo], [TruckCategory], [CargoCategory], [BillingUnit], [ChargeType], [PaidQty], [PaidAmount], [RefundAmount], [Status], [LoadFrom], [IsSellRateChanged], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingStatement]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] LIKE '%' +  @OrderNo  + '%'
	        
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [BookingStatement] Record Into [BookingStatement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingStatementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementInsert] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementInsert] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ChargeCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @MovementCode varchar(20),
    @Qty decimal(10, 3),
    @Price numeric(12, 2),
    @Size varchar(2),
    @Type varchar(2),
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsVAS bit,
    @DiscountType smallint,
    @DiscountAmount numeric(12, 2),
    @ActualAmount numeric(12, 2),
    @IsSlabRate bit,
    @SlabRateFrom smallint,
    @SlabRateTo smallint,
    @TruckCategory smallint,
    @CargoCategory smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @PaidQty decimal(10, 3),
    @PaidAmount numeric(12, 2),
    @RefundAmount numeric(12, 2),
    @LoadFrom varchar(50),
    @IsSellRateChanged bit,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Depot].[BookingStatement] (
			[BranchID], [OrderNo], [ContainerKey], [ChargeCode], [PaymentTerm], [PaymentTo], [MovementCode], [Qty], [Price], [Size], [Type], 
			[IsWaived], [IsAutoLoad], [IsVAS], [DiscountType], [DiscountAmount], [ActualAmount], [IsSlabRate], [SlabRateFrom], [SlabRateTo], 
			[TruckCategory], [CargoCategory], [BillingUnit], [ChargeType], [PaidQty], [PaidAmount], [RefundAmount], [Status], [LoadFrom], 
			[IsSellRateChanged], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey, @ChargeCode, @PaymentTerm, @PaymentTo, @MovementCode, @Qty, @Price, @Size, @Type, 
			@IsWaived, @IsAutoLoad, @IsVAS, @DiscountType, @DiscountAmount, @ActualAmount, @IsSlabRate, @SlabRateFrom, @SlabRateTo, 
			@TruckCategory, @CargoCategory, @BillingUnit, @ChargeType, @PaidQty, @PaidAmount, @RefundAmount, Cast(1 as bit), @LoadFrom, 
			@IsSellRateChanged, @CreatedBy, getutcdate()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [BookingStatement] Record Into [BookingStatement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingStatementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementUpdate] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementUpdate] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ChargeCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @MovementCode varchar(20),
    @Qty decimal(10, 3),
    @Price numeric(12, 2),
    @Size varchar(2),
    @Type varchar(2),
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsVAS bit,
    @DiscountType smallint,
    @DiscountAmount numeric(12, 2),
    @ActualAmount numeric(12, 2),
    @IsSlabRate bit,
    @SlabRateFrom smallint,
    @SlabRateTo smallint,
    @TruckCategory smallint,
    @CargoCategory smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @PaidQty decimal(10, 3),
    @PaidAmount numeric(12, 2),
    @RefundAmount numeric(12, 2),
    @LoadFrom varchar(50),
    @IsSellRateChanged bit,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)
AS 
 
	
BEGIN

	UPDATE	[Depot].[BookingStatement]
	SET		[Size] = @Size, [Type] = @Type, [IsWaived] = @IsWaived, [IsAutoLoad] = @IsAutoLoad, [IsVAS] = @IsVAS, [DiscountType] = @DiscountType, 
			[DiscountAmount] = @DiscountAmount, [ActualAmount] = @ActualAmount, [IsSlabRate] = @IsSlabRate, [SlabRateFrom] = @SlabRateFrom, 
			[SlabRateTo] = @SlabRateTo, [TruckCategory] = @TruckCategory, [CargoCategory] = @CargoCategory, [BillingUnit] = @BillingUnit, 
			[ChargeType] = @ChargeType, [PaidQty] = @PaidQty, [PaidAmount] = @PaidAmount, [RefundAmount] = @RefundAmount, 
			[LoadFrom] = @LoadFrom, [IsSellRateChanged] = @IsSellRateChanged, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = Getutcdate()
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [ChargeCode] = @ChargeCode
	       AND [PaymentTerm] = @PaymentTerm
	       AND [PaymentTo] = @PaymentTo
	       AND [MovementCode] = @MovementCode
	       AND [Qty] = @Qty
	       AND [Price] = @Price
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [BookingStatement] Record Into [BookingStatement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingStatementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementSave] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementSave] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ChargeCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @MovementCode varchar(20),
    @Qty decimal(10, 3),
    @Price numeric(12, 2),
    @Size varchar(2),
    @Type varchar(2),
    @IsWaived bit,
    @IsAutoLoad bit,
    @IsVAS bit,
    @DiscountType smallint,
    @DiscountAmount numeric(12, 2),
    @ActualAmount numeric(12, 2),
    @IsSlabRate bit,
    @SlabRateFrom smallint,
    @SlabRateTo smallint,
    @TruckCategory smallint,
    @CargoCategory smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @PaidQty decimal(10, 3),
    @PaidAmount numeric(12, 2),
    @RefundAmount numeric(12, 2),
    @LoadFrom varchar(50),
    @IsSellRateChanged bit,
    @CreatedBy varchar(50),
    @ModifiedBy varchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[BookingStatement] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [ChargeCode] = @ChargeCode
	       AND [PaymentTerm] = @PaymentTerm
	       AND [PaymentTo] = @PaymentTo
	       AND [MovementCode] = @MovementCode
	       AND [Qty] = @Qty
	       AND [Price] = @Price)>0
	BEGIN
	    Exec [Depot].[usp_BookingStatementUpdate] 
		@BranchID, @OrderNo, @ContainerKey, @ChargeCode, @PaymentTerm, @PaymentTo, @MovementCode, @Qty, @Price, @Size, @Type, 
			@IsWaived, @IsAutoLoad, @IsVAS, @DiscountType, @DiscountAmount, @ActualAmount, @IsSlabRate, @SlabRateFrom, @SlabRateTo, 
			@TruckCategory, @CargoCategory, @BillingUnit, @ChargeType, @PaidQty, @PaidAmount, @RefundAmount, @LoadFrom, 
			@IsSellRateChanged, @CreatedBy,@ModifiedBy

	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_BookingStatementInsert] 
			@BranchID, @OrderNo, @ContainerKey, @ChargeCode, @PaymentTerm, @PaymentTo, @MovementCode, @Qty, @Price, @Size, @Type, 
			@IsWaived, @IsAutoLoad, @IsVAS, @DiscountType, @DiscountAmount, @ActualAmount, @IsSlabRate, @SlabRateFrom, @SlabRateTo, 
			@TruckCategory, @CargoCategory, @BillingUnit, @ChargeType, @PaidQty, @PaidAmount, @RefundAmount, @LoadFrom, 
			@IsSellRateChanged, @CreatedBy,@ModifiedBy

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[BookingStatementSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingStatementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [BookingStatement] Record  based on [BookingStatement]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingStatementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingStatementDelete] 
END 
GO
CREATE PROC [Depot].[usp_BookingStatementDelete] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ChargeCode nvarchar(50),
    @PaymentTerm smallint,
    @PaymentTo smallint,
    @MovementCode varchar(20),
    @Qty decimal(10, 3),
    @Price numeric(12, 2)
AS 

	
BEGIN

	UPDATE	[Depot].[BookingStatement]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [ChargeCode] = @ChargeCode
	       AND [PaymentTerm] = @PaymentTerm
	       AND [PaymentTo] = @PaymentTo
	       AND [MovementCode] = @MovementCode
	       AND [Qty] = @Qty
	       AND [Price] = @Price
 


END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingStatementDelete]
-- ========================================================================================================================================

GO

 
