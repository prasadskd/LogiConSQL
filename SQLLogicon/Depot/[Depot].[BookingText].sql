


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingText] Record based on [BookingText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingTextSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextSelect] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextSelect] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [OrderNo], [CommodityDesc], [Remarks], [MarksAndNos], [SpecialInstruction], [ReferenceNo], [PageNo], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingText]
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo  
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingText] Records from [BookingText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingTextList]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextList] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextList] 
	@BranchID SMALLINT,
	@OrderNo VARCHAR(50)
AS 
BEGIN

	SELECT	[BranchID], [OrderNo], [CommodityDesc], [Remarks], [MarksAndNos], [SpecialInstruction], [ReferenceNo], [PageNo], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Depot].[BookingText]
	WHERE	[BranchID] = @BranchID 
			And OrderNo = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingText] Records from [BookingText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingTextPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextPageView] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextPageView] 
	@BranchID SMALLINT,
	@OrderNo VARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [OrderNo], [CommodityDesc], [Remarks], [MarksAndNos], [SpecialInstruction], [ReferenceNo], [PageNo], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingText]
	WHERE	[BranchID] = @BranchID
			And OrderNo = @OrderNo 
	ORDER BY [OrderNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingTextRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextRecordCount] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextRecordCount] 
	@BranchID SMALLINT,
	@OrderNo VARCHAR(50)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Depot].[BookingText]
	WHERE	[BranchID] = @BranchID
			And OrderNo = @OrderNo
	

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingText] Record based on [BookingText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Depot].[usp_BookingTextAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextAutoCompleteSearch] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextAutoCompleteSearch] 
    @BranchID SMALLINT,
    @OrderNo VARCHAR(25)
AS 

BEGIN

	SELECT [BranchID], [OrderNo], [CommodityDesc], [Remarks], [MarksAndNos], [SpecialInstruction], [ReferenceNo], [PageNo], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Depot].[BookingText]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] LIKE '%' +  @OrderNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [BookingText] Record Into [BookingText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingTextInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextInsert] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextInsert] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @CommodityDesc nvarchar(MAX),
    @Remarks nvarchar(MAX),
    @MarksAndNos nvarchar(MAX),
    @SpecialInstruction nvarchar(MAX),
    @ReferenceNo nvarchar(50),
    @PageNo nvarchar(50),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)
AS 
  

BEGIN

	
	INSERT INTO [Depot].[BookingText] (
			[BranchID], [OrderNo], [CommodityDesc], [Remarks], [MarksAndNos], [SpecialInstruction], [ReferenceNo], [PageNo], 
			[Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @CommodityDesc, @Remarks, @MarksAndNos, @SpecialInstruction, @ReferenceNo, @PageNo, 
			Cast(1 as bit) , @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [BookingText] Record Into [BookingText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingTextUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextUpdate] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextUpdate] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @CommodityDesc nvarchar(MAX),
    @Remarks nvarchar(MAX),
    @MarksAndNos nvarchar(MAX),
    @SpecialInstruction nvarchar(MAX),
    @ReferenceNo nvarchar(50),
    @PageNo nvarchar(50),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25)
AS 
 
	
BEGIN

	UPDATE	[Depot].[BookingText]
	SET		[CommodityDesc] = @CommodityDesc, [Remarks] = @Remarks, [MarksAndNos] = @MarksAndNos, [SpecialInstruction] = @SpecialInstruction, 
			[ReferenceNo] = @ReferenceNo, [PageNo] = @PageNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
	
END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [BookingText] Record Into [BookingText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingTextSave]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextSave] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextSave] 
    @BranchID smallint,
    @OrderNo varchar(50),
    @CommodityDesc nvarchar(MAX),
    @Remarks nvarchar(MAX),
    @MarksAndNos nvarchar(MAX),
    @SpecialInstruction nvarchar(MAX),
    @ReferenceNo nvarchar(50),
    @PageNo nvarchar(50),
    @CreatedBy varchar(25),
    @ModifiedBy varchar(25) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Depot].[BookingText] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo)>0
	BEGIN
	    Exec [Depot].[usp_BookingTextUpdate] 
			@BranchID, @OrderNo, @CommodityDesc, @Remarks, @MarksAndNos, @SpecialInstruction, @ReferenceNo, @PageNo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Depot].[usp_BookingTextInsert] 
			@BranchID, @OrderNo, @CommodityDesc, @Remarks, @MarksAndNos, @SpecialInstruction, @ReferenceNo, @PageNo, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Depot].usp_[BookingTextSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Depot].[usp_BookingTextDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [BookingText] Record  based on [BookingText]

-- ========================================================================================================================================

IF OBJECT_ID('[Depot].[usp_BookingTextDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Depot].[usp_BookingTextDelete] 
END 
GO
CREATE PROC [Depot].[usp_BookingTextDelete] 
    @BranchID smallint,
    @OrderNo varchar(25)
AS 

	
BEGIN

	UPDATE	[Depot].[BookingText]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
 

END

-- ========================================================================================================================================
-- END  											 [Depot].[usp_BookingTextDelete]
-- ========================================================================================================================================

GO
 