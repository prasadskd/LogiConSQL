/****** Object:  UserDefinedFunction [EDI].[udf_GenerateCusRepReferenceNo]    Script Date: 02-01-2017 18:35:03 ******/
DROP FUNCTION [EDI].[udf_GenerateCusRepReferenceNo]
GO

/****** Object:  UserDefinedFunction [EDI].[udf_GenerateCusRepReferenceNo]    Script Date: 02-01-2017 18:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Tee Chee Chen
-- Create Date: 10-Oct-2008
-- Description:	This function returns a formatted system statement number
-- select Revenue.udf_GenerateSystemStatementNumber('10001', '2009-04-01')
-- ======================================================================
Create function [EDI].[udf_GenerateCusRepReferenceNo]
(
	@BranchID bigInt,
	@EdiDate datetime
)
returns bigint
begin
	declare @ediReferenceNumber bigint
	declare @counter int;

	set @counter = 0;

	while (@counter < 100)
	begin
		select @ediReferenceNumber = 
			cast(
				(convert(varchar, year(@EdiDate)) + 
				case 
					when month(@EdiDate) < 10 then '0'
					else ''
				end + 
				convert(varchar, month(@EdiDate)) + 
				case 
					when day(@EdiDate) < 10 then '0'
					else ''	
				end + 
				convert(varchar, day(@EdiDate)) + 
				convert(varchar, @BranchID) + 
				case 
					when (@counter = 0) or (@counter % 10 > 0) then '0' 
					else '' 
				end  + 
				convert(varchar, @counter))
			as bigint);

		if not exists (
			select 1 from EDI.CUSREPHeader 
			where 
			BranchID = @BranchID
			And ReferenceNo = @ediReferenceNumber
		)
		begin
			break; -- exit for
		end

		set @counter = @counter + 1;
	end 

	return @ediReferenceNumber;
end;

GO


