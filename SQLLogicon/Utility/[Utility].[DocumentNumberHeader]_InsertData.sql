--Truncate Table [Utility].[DocumentNumberHeader] 
--Select * From [Utility].[DocumentNumberHeader] 

INSERT [Utility].[DocumentNumberHeader] ([BranchID], [DocumentID], [DocumentKey], [DocumentPrefix], [NumberLength], [LastNumber], [UseCompany], [UseBranch], [UseYear], [UseMonth], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
Select  1000001, N'Bill\Invoice(Cash)', N'BILL.INVCASH', N'CA', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Bill\Invoice(Credit)', N'BILL.INVCREDIT', N'CR', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Bill\Payment', N'BILL.PMT', N'PY', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Bill\Statement', N'BILL.STMT', N'ST', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\EIRIN', N'DEP.EIRIN', N'EI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\EIROUT', N'DEP.EIROUT', N'EO', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\Order(Export)', N'DEP.EXP', N'E', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\Order(Import)', N'DEP.IMP', N'I', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\Order(Local)', N'DEP.LOCAL', N'L', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Depot\TruckIn', N'DEP.TRKIN', N'T', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Freight\Order(Export)', N'FRG.EXP', N'FRE', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Freight\Order(Import)', N'FRG.IMP', N'FRI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Freight\Order(Local)', N'FRG.LOCAL', N'FRL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Haulage\Order(Export)', N'HLG.EXP', N'HLE', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Haulage\Order(Import)', N'HLG.IMP', N'HLI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Haulage\Order(Local)', N'HLG.LOCAL', N'HLL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Mst\Quotation', N'MST.QUOTATION', N'Q', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Opr\Order(Export)', N'OPR.EXP', N'E', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Opr\Order(Import)', N'OPR.IMP', N'I', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Opr\Order(Local)', N'OPR.LOCAL', N'L', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Opr\VesselSchedule', N'OPR.VSLSCH', N'VS', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1000001, N'Opr\Declaration', N'OPR.DEC', N'DL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL

Select  1001001, N'Bill\Invoice(Cash)', N'BILL.INVCASH', N'CA', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Bill\Invoice(Credit)', N'BILL.INVCREDIT', N'CR', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Bill\Payment', N'BILL.PMT', N'PT', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Bill\Statement', N'BILL.STMT', N'ST', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\EIRIN', N'DEP.EIRIN', N'EI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\EIROUT', N'DEP.EIROUT', N'EO', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\Order(Export)', N'DEP.EXP', N'E', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\Order(Import)', N'DEP.IMP', N'I', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\Order(Local)', N'DEP.LOCAL', N'L', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Depot\TruckIn', N'DEP.TRKIN', N'T', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Freight\ContainerRequest', N'FRG.CREQ', N'FCR', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Freight\Order(Export)', N'FRG.EXP', N'FRE', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Freight\Order(Import)', N'FRG.IMP', N'FRI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Freight\Order(Local)', N'FRG.LOCAL', N'FRL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Haulage\Order(Export)', N'HLG.EXP', N'HLE', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Haulage\Order(Import)', N'HLG.IMP', N'HLI', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Haulage\Order(Local)', N'HLG.LOCAL', N'HLL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Mst\Quotation', N'MST.QUOTATION', N'Q', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Opr\Order(Export)', N'OPR.EXP', N'E', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Opr\Order(Import)', N'OPR.IMP', N'I', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Opr\Order(Local)', N'OPR.LOCAL', N'L', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Opr\VesselSchedule', N'OPR.VSLSCH', N'VS', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select  1001001, N'Opr\Declaration', N'OPR.DEC', N'DL', 5, 0, 0, 1, 1, 1, N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


 