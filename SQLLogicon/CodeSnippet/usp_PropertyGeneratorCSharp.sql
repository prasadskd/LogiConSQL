
/* 

Select * From Master.ClassGenerator
Exec usp_PropertyGeneratorCSharp 'EDI','Lookup','D:\Project\Logicon\Logicon\' 
Select * From Master.ClassGenerator

*/

IF OBJECT_ID('usp_PropertyGeneratorCSharp') IS NOT NULL
BEGIN 
    DROP PROC usp_PropertyGeneratorCSharp
END 
GO
CREATE PROC usp_PropertyGeneratorCSharp
    @SchemaName varchar(50),
	@tblName varchar(50),
	@folderPath varchar(100)
AS 
 

BEGIN

SET NOCOUNT ON;

Declare @table table (id int identity,tblName varchar(200),tblSchema varchar(50))


Declare @tblResult table (textvalue nvarchar(max))

insert into @table
select TABLE_NAME,TABLE_SCHEMA from INFORMATION_SCHEMA.TABLES 
where TABLE_SCHEMA=@SchemaName
AND TABLE_NAME=@tblName
order by TABLE_NAME

BEGIN TRANSACTION
Truncate Table  Logicon.Master.ClassGenerator
COMMIT TRANSACTION;

DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
DECLARE @Tab as CHAR(1) = Char(9)
DECLARE @Count as int=0
DECLARE @iCount as int
DECLARE @privateVariables as varchar(max)
DECLARE @publicMembers as varchar(max)
DECLARE @tableName as varchar(50)
DECLARE @tableSchema as varchar(50)
DECLARE @ClassStart as varchar(max)
Declare @ClassEnd as varchar(max)
declare @ClassConstructor as varchar(500)

declare @bigData as nvarchar(max)

Select @iCount=1,@Count = COUNT(0) from @table
 

IF @Count=0 
	RETURN;
	
	
set @Count =1
	

 

select @tableName = tblName,@tableSchema=tblSchema from @table where id=@iCount



select @ClassStart = 'using System;' +
@NewLineChar + 'using System.Collections.Generic;' +
@NewLineChar + 'using System.Linq;' +
@NewLineChar + 'using System.Text;' +
@NewLineChar + 'using '+ @SchemaName + '.Contract;' + 
@NewLineChar + 
@NewLineChar + 'namespace '+ @SchemaName + '.Contract' +
@NewLineChar + '{' +
@NewLineChar + @tab + 'public class ' + @tableName + ':IContract' +
@NewLineChar + @tab + '{' 

insert into Logicon.Master.ClassGenerator
select (@classstart)

--print @classstart
-- NOTE : FOR BETTER RESULTS , RUN THIS QUERY IN "RESULTS TO TEXT" MODE (PRESS ctrl+T)

--Print @tab + @tab + '// Constructor '

set @ClassConstructor = @tab + @tab + 'public ' + @tableName + '() { }' + @NewLineChar + @NewLineChar
--print @ClassConstructor
insert into Logicon.Master.ClassGenerator
select ( @ClassConstructor )

--###Prorperty  Declartions

--Print  @tab + @tab + '// Public Members '

insert into Logicon.Master.ClassGenerator 
Select  @tab + @tab + 'public ' + 
	case when data_type='varchar' then 'string '
	when data_type='char' then 'string '
	when data_type='tinyint' then 'Int16 '
	when data_type='bit' then 'bool '
	when data_type='datetime' then 'DateTime '
	when data_type='date' then 'DateTime '
	when data_type='money' then 'decimal '
	when data_type='int' then 'Int32 '
	when data_type='bigint' then 'Int64 '
	when data_type='float' then 'double '
	when data_type='text' then 'string '	
	when data_type='smallint' then 'Int16 '	
	when data_type='smallmoney' then 'decimal '	
	when data_type='numeric' then 'decimal '	
	when data_type='nvarchar' then 'string'			
	when data_type='xml' then 'string'			
	when data_type='image' then 'object'
	when data_type='decimal' then 'decimal'
	when data_type='uniqueidentifier' then 'Guid'
		
end + ' ' +  column_name + ' { get; set; }' + @NewLineChar + @NewLineChar 
	

from information_schema.columns where table_name=@tableName And TABLE_SCHEMA = @tableSchema
--And column_name NOT IN ('CreatedOn','ModifiedOn')
order by ordinal_position
 


select @ClassEnd = @tab + '}' + @NewLineChar + '}'+ @NewLineChar+ @NewLineChar+ @NewLineChar+ @NewLineChar


--print @classend

insert into Logicon.Master.ClassGenerator
select (@ClassEnd)
 

--select * from Master.Company


DECLARE @command VARCHAR(2000)
SET @command = 'BCP "Select ContractClass From Logicon.Master.ClassGenerator " queryout "'+ @folderPath  + @tableName + '.cs" -c -S laptop-ilofhpm7\sqlexpress -U sa -P manager -t "|" '
print @command;
exec master.sys.xp_cmdshell @command
SET NOCOUNT OFF;

END
