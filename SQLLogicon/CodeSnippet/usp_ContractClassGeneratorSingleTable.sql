




 

/* Exec usp_ContractClassGeneratorSingleTable 'Master','Process','D:\Project\Logicon\Logicon\' */

IF OBJECT_ID('usp_ContractClassGeneratorSingleTable') IS NOT NULL
BEGIN 
    DROP PROC usp_ContractClassGeneratorSingleTable
END 
GO
CREATE PROC usp_ContractClassGeneratorSingleTable
    @SchemaName varchar(50),
	@TableName varchar(50),
	@folderPath varchar(100)
AS 
 
Begin  

Declare @tblUsertable table(id smallint primary key identity(1,1),schemaName varchar(50),tableName varchar(50))

insert into @tblUsertable
Select TABLE_SCHEMA,TABLE_NAME from INFORMATION_SCHEMA.TABLES
Where TABLE_SCHEMA = ISNULL(@SchemaName,TABLE_SCHEMA)
And TABLE_NAME = @TableName

declare @vschema varchar(50),
		@vtable varchar(50),
		@vcount smallint,
		@vcounter smallint
		
select @vcount=count(0),@vcounter=1 from @tblUsertable

Set @folderPath = @folderPath + @SchemaName + '.Contract\'

while @vcounter<=@vcount
begin

	Select @vschema=schemaname,@vtable = tablename 
	from @tblUserTable where id=@vcounter

	

	Exec usp_PropertyGeneratorCSharp @vschema,@vtable,@folderPath


	set @vcounter=@vcounter+1
End		
			

End
