 

DELETE CONFIG.Lookup where LookupID >=10000
GO

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10000,'929','Import Declaration (K1)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10001,'830','Export Declaration (K2)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10002,'667','Application Permit to Transport Goods within Federation (K3)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10003,'669','Customs Application to remove/tranship goods (K8)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10004,'668','Approval to release dutiable goods  (K9)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10005,'810','Export Permit/Licence, Apply for','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10006,'910','Import Permit/Licence, Apply for','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10007,'817','Export (STA) ','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10008,'818','Export Transhipment (STA)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10009,'819','Export Transit (STA)','MessageType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10100,'1','Cancellation','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10101,'2','Add','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10102,'3','Delete','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10103,'5','Replace','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10104,'9','Original','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10105,'RE','Renewal','MessageFunction',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10200,'DD','DFS to DFS','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10201,'LH','Labuan','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10202,'EE','Re-Export','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10203,'LO','Langkawi/Labuan to overseas','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10204,'FE','FZ to Export Station','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10205,'LP','LMW to PCA (local sale)','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10206,'FF','FZ to FZ','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10207,'OL','Oversea to Langkawi/Labuan','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10208,'FL','FZ to LMW','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10209,'PF','PCA to FZ','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10210,'FP','FZ to PCA (local release)','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10211,'PL','PCA to Langkawi/Labuan','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10212,'FW','FZ to WH','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10213,'PW','Import Station (PCA) to WH','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10214,'HL','Highsea to Labuan','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10215,'T1','Transhipment','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10216,'ID','Import Station to DFS','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10217,'T2','Transit','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10218,'IF','Import Station to FZ','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10219,'WE','WH to Export Station','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10220,'II','Re-Import','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10221,'WF','WH to FZ','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10222,'IL','Import Station to LMW','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10223,'WI','WH to Import Station (PCA)','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10224,'IW','Import Station to WH','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10225,'WP','WH to PCA (partial release)','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10226,'LF','LMW to FZ','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10227,'WW','WH to WH','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  UNION ALL
Select 10228,'IN','Import Normal (To PCA)','EDIMovementType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10150,'911','IMPORT','PermitTransactionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10151,'811','EXPORT','PermitTransactionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10152,'611','RELEASE LETTER','PermitTransactionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10160,'Engineering','Mechanical Engineering and Fabrication','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10161,'Mining','Mining','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10162,'Oil and Gas','Oil and Gas','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10163,'Refinery','Refinery','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10164,'Structure','Structure','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10165,'Others','Others (Please specify)','SectorType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10170,'COA','Method 1(A) : Consignment for Industrial Product','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10171,'TCOA','Method 1(B) : Consignment with One-Off Clearance','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10172,'1C','Method 1(C) : Consignment By Road','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10173,'1D','Method 1(D) : Consignment for Construction Product','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10174,'1E','Method 1(E) : Consignment Less Than 250MT for Industrial Product','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10175,'CTR','Method 2(B) : Consignment with Full Type Test Report','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10176,'2C','Method 2(C) : Consignment with Patented Material','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10177,'PCOA','Method 3(A) : Consignment with Product Certification License - Direct Discharge','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10178,'PCL','Method 3(B) : Consignment with Product Certification License','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10179,'4','Method 4    : Consignment for Local Sales (from Bonded Warehouse/FCZ/FIZ) - Master COA from FTTR SIRIM only','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10180,'OEP','Method 5    : Consignment with COA Exemption','COAType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10181,'2','Commonwealth','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10182,'3','ATIGA (Form D)','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10183,'4','ASEAN � CHINA (Form E)','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10184,'5','MPCEPA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10185,'6','MJEPA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10186,'7','AKFTA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10187,'B','MNZFTA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10188,'A','ASEAN � INDIA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10189,'8','AJCEP','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10190,'C','MICECA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10191,'D','MCFTA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10192,'E','MAFTA','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10193,'F','Developing Eight - Preferential Trade Agreement (D-8 PTA) ','SpecialTreatmentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10300,'D1','Diplomatic and Consular Service and  Privilege Ordinance','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10301,'G1','Custom Duties (Exemption) Order 1988 Ordinance','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10302,'G2','Custom Duties(Exemption)  (Process Palm Oil Products) Order 1988','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10303,'G3','Custom Duties (Exemption) (Sabah) Order 1988','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10304,'G4','Custom Duties (Exemption) (Sarawak) Order 1988','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10305,'L2','LMW Gen. goods','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10306,'OE','Others','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10307,'TG','Treasury Exemption (General)','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10308,'TI ','Treasury Exemption (Industry)','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10309,'TV','Treasury Exemption (Incentive)','ExcemptionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10350,'C2','Exemption Under ADM 2A - Finish Product','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10351,'C3','Exemption Under ADM 2B - Raw Material','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10352,'C5','Exemption Under CJ5 (Import)','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10353,'C6','Exemption Under CJ5A (Import)','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10354,'GE','Exemption, Schedule B, Sales Tax (Exemption) Order 1980','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10355,'OE','Others','SalesTaxExcemption',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10370,'P01','Private','PermitPurposeType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10371,'P02','Business','PermitPurposeType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10372,'P03','Research','PermitPurposeType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10373,'P04','Exhibition','PermitPurposeType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10374,'P05','Others','PermitPurposeType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10380,'4','Import for Manufacturing Component (100% for Export)','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10381,'5','Import for Manufacturing Component (Local Market)','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10382,'7','Import for Research (Factory)','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10383,'8','Import for Research (Market / Quality)','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10384,'9','Import for Re-Work and Re-Export','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10385,'10','Import for Non-Regulated Items','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10386,'11','Import for Specific Purpose','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10387,'12','Import for Concert','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10388,'13','Import for Transit','ReleaseLetterType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10390,'01','Sales','K1ATransactionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10391,'02','Hire','K1ATransactionType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10400,'CID','CONS. INDUS. DEV. BOARD (CIDB) MAL','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10401,'LTA','LEMBAGA PERLESENAN TENAGA ATOM','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10402,'MIT','MINISTRY OF INT. TRADE AND INDUSTRY','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10403,'SIR','SIRIM QAS INTERNATIONAL SDN. BHD.','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10404,'SJT','SURUHANJAYA TENAGA','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10405,'SRC','SIRIM CERTIFICATE OF APPROVAL','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  UNION ALL
Select 10406,'JPK','JPK','OGACode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10450,'0','LIQUID BULK','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10451,'1','BREAK BULK/GENERAL BULK','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10452,'2','CONTAINER','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10453,'9','GENERAL','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10454,'A','DG CLASS I','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10455,'B','DG CLASS II','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10456,'C','DG CLASS III','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10457,'D','DRY BULK','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10458,'S','SPECIAL','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10459,'V','VALUABLE','CargoClassCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10470,'1','Maritime / Sea','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10471,'2','Rail','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10472,'3','Road','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10473,'4','Air','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10474,'6','Multimodal transport','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10475,'7','Fixed transport installation','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10476,'8','Inland water transport','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10477,'9','Transport mode not applicable','TransportModeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10500,'0','None','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10501,'1','Plastics','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10502,'2','Paper and Fiberboard','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10503,'3','Wood','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10504,'5','Metal','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10505,'6','Glass, Porcelain, Ceramic, Stoneware','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10506,'7','Textile','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10507,'9','Unknown or not otherwise enumerated','PackagingMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10550,'1','Inner','PackingLevelCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10551,'2','Intermediate','PackingLevelCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10552,'3','Outer','PackingLevelCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10600,'TE','Treasury exemption','ItemTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10601,'PM','Iimport /Export Permit','ItemTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10602,'C2','CJ2','ItemTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10603,'C5','CJ5','ItemTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10650,'1','SIRIM QAS INT SDN. BHD.','CertificationBody',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10651,'2','IKRAM QA SERVICES SDN. BHD.','CertificationBody',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10652,'3','Others','CertificationBody',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10700,'ADD','Anti-Dumping Duty','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10701,'CDE','Customs Duty Exemption','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10702,'CDP','Customs Duty Paid','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10703,'CUD','Customs Duty','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10704,'ECE','Excise Duty Exemption','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10705,'EXC','Excise Duty','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10706,'OTE','Other Tax Exemption','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10707,'OTH','Other Tax','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10708,'OTP','Other Tax Paid','TaxDutyType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10750,'A','Mixed Tax Rate','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10751,'H','Higher Rate','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10752,'SP','Standard Percentage','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10753,'SA','Standard Rate By Amount','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10754,'AP','ASEAN Preferential Exemption Rate','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10755,'SPP','Standard Preferential Rate','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10756,'SAP','Standard Preferential Rate (Amount)','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10757,'AAP','Mixed Preferential Tax Rate','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10758,'VPL','Volume per Litre','TaxDutyRate',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10800,'2','IMPORT','EQDStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10801,'3','EXPORT','EQDStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10802,'6','TRANSHIPMENT','EQDStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 10850,'1','FCL (local)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10851,'2','FCL (foreign)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10852,'3','LCL (local)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10853,'4','LCL (foreign)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10854,'5','Empty (local)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10855,'6','Empty (foreign)','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10856,'7','Empty','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 10857,'8','Conventional Cargo','EQDContainerStatus',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  





INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 20001,'10','ANTIMONY OXIDE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20002,'12','AUSAB RIA','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20003,'1','Ac-227:Ag-111','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20004,'2','Ag-110m','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20005,'4','Am-241','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20006,'6','Am-241/Be','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20007,'7','Am-241/Pu-293','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20008,'9','Am-243','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20009,'10','Americium-241 (Am-241)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20010,'11','Au-198','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20011,'15','BALTIC SEDIMENT','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20012,'13','Ba-131','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20013,'14','Ba-133','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20014,'16','Bi-207','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20015,'17','Bi-210','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20016,'11','Bromine-82 (Br-82)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20017,'19','C-14','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20018,'21','Cd-109','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20019,'22','Cd-109/Fe-55','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20020,'24','Cd-115','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20021,'25','Ce-139','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20022,'26','Cf-245','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20023,'28','Cf-252','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20024,'30','Cl-36','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20025,'31','Cm-243','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20026,'32','Cm-244','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20027,'33','Co-57','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20028,'34','Co-59','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20029,'34','Co-60','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20030,'35','Co-60/Na-22','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20031,'36','Cr-14','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20032,'37','Cr-51','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20033,'38','Cs-134','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20034,'42','Cs-137','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20035,'43','Cs-137/Ba-137','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20036,'44','Cs-137/Zn 65','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20037,'48','Eu-152','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20038,'49','Eu-154','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20039,'50','F-18','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20040,'51','Fe-55','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20041,'52','Fe-59','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20042,'174','Ga-67','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20043,'175','Ga-68','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20044,'53','Gd-153','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20045,'176','Ge-68','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20046,'54','H-3','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20047,'56','Hg-203','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20048,'177','I-125','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20049,'58','I-129','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20050,'59','I-131','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20051,'178','ILMENITE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20052,'63','IRIDIUM OXIDE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20053,'179','In-111','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20054,'62','Ir-192','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20055,'180','Iron Oxide (Fe2O3)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20056,'64','K-40/U-238/Th-232','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20057,'65','K-42','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20058,'66','Kr-85','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20059,'67','Kr-89','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20060,'68','MARINE SEDIMENT','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20061,'69','MIXED RADIONUCLIDE Co-60 & Cs-137','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20062,'70','MIXED SEAWEED','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20063,'181','MONAZITE ORE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20064,'72','MULTINUCLIDE STANDARD SOURCE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20065,'71','Mn-54','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20066,'182','Mo-99','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20067,'183','Mo-99/Tc-99m','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20068,'73','N-63','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20069,'75','NAT UR','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20070,'74','Na-22','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20071,'76','Ni-63','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20072,'184','Np-237','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20073,'77','P-32','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20074,'79','P-33','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20075,'185','PEKATAN LANTANID','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20076,'84','PERUVIAN SOIL','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20077,'81','Pa-234','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20078,'82','Pb-210','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20079,'85','Pm-147','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20080,'86','Po-209','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20081,'87','Po-210','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20082,'88','Pu 238','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20083,'90','Pu-239','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20084,'186','Pu-242','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20085,'98','RADON GENERATOR','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20086,'100','RIVER SEDIMENT','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20087,'187','RUTILE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20088,'92','Ra-220','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20089,'93','Ra-226','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20090,'96','Ra-226/Be','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20091,'97','Ra-228','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20092,'188','Rb-82','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20093,'99','Rb-86','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20094,'189','Re-186','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20095,'190','Re-188/W-188','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20096,'101','S-35','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20097,'103','SANGA TIMAH','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20098,'104','SANGA TIMAH (BERTANTALUM)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20099,'105','SANGA TIMAH (TANPA TANTALUM)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20100,'106','SCANDIUM OXIDE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20101,'108','SISA ENAPCEMAR MINYAK','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20102,'115','SR-90/Y','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20103,'191','STRUVERITE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20104,'192','Samarium-153 (Sm-153)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20105,'107','Se-75','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20106,'109','Sn-113','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20107,'110','Sn-119','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20108,'112','Sr-85','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20109,'113','Sr-89','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20110,'193','Sr-90','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20111,'116','Sr-90/Y-90','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20112,'129','THORIUM OXIDE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20113,'131','TIN SLAG','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20114,'132','TIN SLAG (U-238 & Th-232)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20115,'139','TOLUEN C-14','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20116,'194','Tb-160','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20117,'195','Tc-98','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20118,'119','Tc-99','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20119,'120','Tc-99m','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20120,'121','Te-123m','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20121,'122','Th (THORIUM ORE)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20122,'123','Th-228','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20123,'124','Th-228/Be','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20124,'125','Th-229','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20125,'126','Th-230','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20126,'127','Th-232','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20127,'128','ThO2','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20128,'134','Tl-201','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20129,'135','Tl-204','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20130,'136','Tl-230','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20131,'137','Tl-234','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20132,'118','Tl-240','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20133,'138','Tn-22','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20134,'140','U (URANIUM ORE)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20135,'141','U-232','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20136,'142','U-235','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20137,'143','U-236','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20138,'144','U-238','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20139,'148','U-238/ Th-232','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20140,'151','U-308','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20141,'153','URANIUM ACETATE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20142,'154','URANIUM OXIDE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20143,'196','XENOTIME ORE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20144,'197','Y (XENOTIME ORE)','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20145,'156','Y-88','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20146,'198','Y-90','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20147,'199','Yb-169','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20148,'200','ZIRCON','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20149,'157','ZIRCON SAND','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20150,'158','ZIRCONIUM SILIKAT','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20151,'159','ZIRCOSIL FIVE','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20152,'161','Zn-65','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20153,'160','ZnO4','RadioActiveMaterialCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 20500,'1A','DRUM, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20501,'1B','DRUM, ALUMINIUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20502,'1C','COLLIE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20503,'1D','DRUM, PLYWOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20504,'1F','FLEXIBLE CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20505,'1G','DRUM, FIBRE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20506,'1I','ISO CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20507,'1Q','QUARTERS OF BEEF','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20508,'1S','DRUM, SHRINKWRAPPED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20509,'1W','DRUM, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20510,'2C','BARREL, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20511,'2P','PAIR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20512,'3A','JERRICAN, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20513,'3B','BARES','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20514,'3H','JERRICAN, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20515,'43','BAG, SUPER BULK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20516,'44','POLYBAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20517,'4A','BOX, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20518,'4B','BOX, ALUMINIUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20519,'4C','BOX, NATURAL WOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20520,'4D','BOX, PLYWOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20521,'4F','BOX, RECONSTITUTED WOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20522,'4G','BOX, FIBREBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20523,'4H','BOX, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20524,'5H','BAG, WOVEN PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20525,'5L','BAG, TEXTILE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20526,'5M','BAG, PAPER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20527,'6H','COMPOSITE PACKAGING, PLASTIC RECEPTACLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20528,'6P','COMPOSITE PACKAGING, GLASS RECEPTACLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20529,'7A','CAR CASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20530,'7B','WOODEN CASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20531,'8A','WOODEN PALLET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20532,'8B','WOODEN CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20533,'8C','WOODEN BUNDLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20534,'9A','YACHT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20535,'AA','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20536,'AB','RECEPTACLE, FIBRE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20537,'AC','RECEPTACLE, PAPER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20538,'AD','RECEPTACLE, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20539,'AE','AEROSOL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20540,'AF','PALLET, MODULAR, COLLAR 80cms * 60cms','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20541,'AG','PALLET, SHRINKWRAPPED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20542,'AH','PALLET, 100cms * 110cms','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20543,'AI','CLAMSHELL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20544,'AJ','CONE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20545,'AL','BALL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20546,'AM','AMPOULE, NON-PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20547,'AP','AMPOULE, PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20548,'AT','ATOMIZER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20549,'AU','AUTER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20550,'AV','CAPSULE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20551,'B1','BAYS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20552,'B3','BALLOTS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20553,'B6','Bus','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20554,'BA','BARREL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20555,'BB','BOBBIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20556,'BC','BOTTLECRATE, BOTTLERACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20557,'BD','BOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20558,'BE','BUNDLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20559,'BF','BALLOON, NON-PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20560,'BG','BAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20561,'BH','BUNCH','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20562,'BI','BIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20563,'BJ','BUCKET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20564,'BK','BASKET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20565,'BL','BALE, COMPRESSED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20566,'BM','BASIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20567,'BN','BALE, NON-COMPRESSED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20568,'BO','BOTTLE, NON-PROTECTED, CYLINDRIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20569,'BP','BALLOON, PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20570,'BQ','BOTTLE, PROTECTED CYLINDRICAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20571,'BR','BAR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20572,'BS','BOTTLE, NON-PROTECTED, BULBOUS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20573,'BT','BOLT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20574,'BU','BUTT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20575,'BV','BOTTLE, PROTECTED BULBOUS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20576,'BW','BOX, FOR LIQUIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20577,'BX','BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20578,'BY','BOARD, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20579,'BZ','BARS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20580,'C1','CUBE, UNICUBE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20581,'C2','Chassis','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20582,'CA','CAN, RECTANGULAR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20583,'CB','BEER CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20584,'CC','CHURN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20585,'CD','CAN, WITH HANDLE AND SPOUT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20586,'CE','CREEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20587,'CF','COFFER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20588,'CG','CAGE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20589,'CH','CHEST','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20590,'CI','CANISTER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20591,'CJ','COFFIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20592,'CK','CASK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20593,'CL','COIL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20594,'CM','CARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20595,'CN','CONTAINER, NOT OTHERWISE SPECIFIED AS TRANSPORT EQUIPMENT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20596,'CO','CARBOY, NON-PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20597,'CP','CARBOY, PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20598,'CQ','CARTRIDGE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20599,'CR','CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20600,'CS','CASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20601,'CT','CARTON','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20602,'CU','CUP','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20603,'CV','COVER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20604,'CW','CAGE, ROLL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20605,'CX','CAN, CYLINDRICAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20606,'CY','CYLINDER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20607,'CZ','CANVAS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20608,'DA','CRATE, MULTIPLE LAYER, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20609,'DB','CRATE, MULTIPLE LAYER, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20610,'DC','CRATE, MULTIPLE LAYER, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20611,'DE','DECKER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20612,'DG','CAGE, COMMONWEALTH HANDLING EQUIPMENT POOL (CHEP)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20613,'DH','BOX, COMMONWEALTH HANDLING EQUIPMENT POOL (CHEP), EUROBOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20614,'DI','DRUM, IRON','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20615,'DJ','DEMIJOHN, NON-PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20616,'DK','CRATE, BULK, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20617,'DL','CRATE, BULK, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20618,'DM','CRATE, BULK, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20619,'DN','DISPENSER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20620,'DP','DEMIJOHN, PROTECTED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20621,'DR','DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20622,'DS','TRAY, ONE LAYER NO COVER, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20623,'DT','TRAY, ONE LAYER NO COVER, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20624,'DU','TRAY, ONE LAYER NO COVER, POLYSTYRENE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20625,'DV','TRAY, ONE LAYER NO COVER, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20626,'DW','TRAY, TWO LAYER NO COVER, PLASTIC TRAY','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20627,'DX','TRAY, TWO LAYER NO COVER, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20628,'DY','TRAY, TWO LAYER NO COVER, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20629,'DZ','DOZEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20630,'EC','BAG, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20631,'ED','CASE, WITH PALLET BASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20632,'EE','CASE, WITH PALLET BASE, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20633,'EF','CASE, WITH PALLET BASE, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20634,'EG','CASE, WITH PALLET BASE, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20635,'EH','CASE, WITH PALLET BASE, METAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20636,'EI','CASE, ISOTHERMIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20637,'EN','ENVELOPE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20638,'FB','FLEXI BAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20639,'FC','FRUIT CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20640,'FD','FRAMED CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20641,'FE','FLEXITANK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20642,'FI','FIRKIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20643,'FL','FLASK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20644,'FO','FOOTLOCKER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20645,'FP','FILMPACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20646,'FR','FRAME','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20647,'FT','FOODTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20648,'FX','BAG, FLEXIBLE CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20649,'GB','GAS BOTTLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20650,'GI','GIRDER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20651,'GL','GALLON','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20652,'GR','RECEPTACLE, GLASS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20653,'GY','GUNNY BAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20654,'GZ','GIRDERS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20655,'HA','BASKET, WITH HANDLE, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20656,'HB','BASKET, WITH HANDLE, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20657,'HC','BASKET, WITH HANDLE, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20658,'HD','HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20659,'HG','HOGSHEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20660,'HN','HANGER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20661,'HR','HAMPER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20662,'IA','PACKAGE, DISPLAY, WOODEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20663,'IB','PACKAGE, DISPLAY, CARDBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20664,'IC','PACKAGE, DISPLAY, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20665,'ID','PACKAGE, DISPLAY, METAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20666,'IE','PACKAGE, SHOW','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20667,'IF','PACKAGE, FLOW','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20668,'IG','PACKAGE, PAPER WRAPPED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20669,'IH','DRUM, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20670,'IK','PACKAGE, CARDBOARD, WITH BOTTLE GRIP-HOLES','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20671,'IN','INGOT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20672,'IZ','INGOTS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20673,'JB','JUMBO BAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20674,'JC','JERRICAN, RECTANGULAR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20675,'JG','JUG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20676,'JR','JAR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20677,'JT','JUTEBAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20678,'JY','JERRICAN, CYLINDRICAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20679,'KG','KEG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20680,'KI','KIT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20681,'LD','LOAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20682,'LE','LUGGAGE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20683,'LF','LIFTS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20684,'LG','LOG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20685,'LN','Length','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20686,'LS','Loose','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20687,'LT','LOT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20688,'LU','Lugs','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20689,'LV','LIFTVAN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20690,'LZ','LOGS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20691,'MA','METAL CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20692,'MB','MULTIPLY BAG','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20693,'MC','MILK CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20694,'MD','MODULE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20695,'ME','METAL CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20696,'MR','RECEPTACLE, METAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20697,'MS','MULTIWALL SACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20698,'MT','MAT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20699,'MW','RECEPTACLE, PLASTIC WRAPPED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20700,'MX','MATCH BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20701,'NA','NOT AVAILABLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20702,'NE','UNPACKED OR UNPACKAGED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20703,'NF','UNPACKED OR UNPACKAGED, SINGLE UNIT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20704,'NG','UNPACKED OR UNPACKAGED, MULTIPLE UNITS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20705,'NL','NUMBER OF SOLIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20706,'NO','NOS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20707,'NS','NEST','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20708,'NT','NET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20709,'NU','NET, TUBE, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20710,'NV','NET, TUBE, TEXTILE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20711,'OK','BLOCK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20712,'OT','Octabin','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20713,'OU','OUTER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20714,'P2','PAN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20715,'PA','PACKET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20716,'PB','PALLET, BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20717,'PC','PARCEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20718,'PD','PALLET, MODULAR, COLLAR 80cms * 100cms','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20719,'PE','PALLET, MODULAR, COLLAR 80cms * 120cms','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20720,'PF','PEN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20721,'PG','PLATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20722,'PH','PITCHER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20723,'PI','PIPE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20724,'PJ','PUNNET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20725,'PK','PACKAGE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20726,'PL','PAIL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20727,'PN','PLANK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20728,'PO','POUCH','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20729,'PP','PIECE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20730,'PR','RECEPTACLE, PLASTIC','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20731,'PS','Packs','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20732,'PT','POT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20733,'PU','TRAY / TRAY PACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20734,'PV','PIPES, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20735,'PX','PALLET (GENERAL)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20736,'PY','PLATES, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20737,'PZ','PIPES/PLANKS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20738,'QA','DRUM, STEEL, NON-REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20739,'QB','DRUM, STEEL, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20740,'QC','DRUM, ALUMINIUM, NON-REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20741,'QD','DRUM, ALUMINIUM, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20742,'QF','DRUM, PLASTIC, NON-REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20743,'QG','DRUM, PLASTIC, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20744,'QH','BARREL, WOODEN, BUNG TYPE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20745,'QJ','BARREL, WOODEN, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20746,'QK','JERRICAN, STEEL, NON-REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20747,'QL','JERRICAN, STEEL, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20748,'QM','JERRICAN, PLASTIC, NON-REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20749,'QN','JERRICAN, PLASTIC, REMOVABLE HEAD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20750,'QP','BOX, WOODEN, NATURAL WOOD, ORDINARY','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20751,'QQ','BOX, WOODEN, NATURAL WOOD, WITH SIFT PROOF WALLS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20752,'QR','BOX, PLASTIC, EXPENDED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20753,'QS','BOX, PLASTIC, SOLID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20754,'RD','ROD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20755,'RG','RING','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20756,'RJ','RACK, CLOTHING HANGER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20757,'RK','RACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20758,'RL','REEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20759,'RM','REAM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20760,'RO','ROLL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20761,'RS','RACK,STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20762,'RT','REDNET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20763,'RZ','RODS, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20764,'SA','SACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20765,'SB','SLAB','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20766,'SC','SHALLOW CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20767,'SD','SPINDLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20768,'SE','SEA-CHEST','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20769,'SF','Sides of Beef','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20770,'SH','SACHET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20771,'SI','SKID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20772,'SK','SKELETON CASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20773,'SL','SLIPSHEET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20774,'SM','SHEETMETAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20775,'SO','SPOOLS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20776,'SP','SHEET, PLASTIC WRAPPING','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20777,'SS','CASE, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20778,'ST','SHEET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20779,'SU','SUITCASE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20780,'SV','ENVELOPE, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20781,'SW','SHRINKWRAPPED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20782,'SX','SET','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20783,'SY','SLEEVE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20784,'SZ','SHEET (SHEETS), IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20785,'TB','TUB','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20786,'TC','TEA-CHEST','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20787,'TD','COLLAPSIBLE TUBE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20788,'TE','TYRE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20789,'TG','TANK GENERIC CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20790,'TI','TIERCE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20791,'TK','TANK,RECTANGULAR','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20792,'TL','TUB, WITH LIB','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20793,'TN','TIN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20794,'TO','TUN','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20795,'TP','TIMBER PACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20796,'TR','TRUNK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20797,'TS','TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20798,'TT','TOTE(S)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20799,'TU','TUBE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20800,'TV','TUBE, WITH NOZZLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20801,'TW','TRIWALL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20802,'TX','TROLLEY','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20803,'TY','TANK,CYLINDRICAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20804,'TZ','TUBES, IN BUNDLE/BUNCH/TRUSS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20805,'UC','UNCAGED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20806,'UN','UNIT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20807,'VA','VAT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20808,'VG','BULK, GAS (AT 1031 MBAR AND 15C)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20809,'VI','VIAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20810,'VK','VANPACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20811,'VL','BULK, LIQUID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20812,'VM','Bulk, Monoisopropyelamine','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20813,'VN','VEHICLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20814,'VO','BULK, SOLID, LARGE PARTICLES ( "NODULES")','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20815,'VP','VACUUMPACKED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20816,'VQ','BULK, LIQUEFIED GAS (AT ABNORMAL TEMPERATURE/PRESURE)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20817,'VR','BULK, SOLID, GRANULAR PARTICLES ("GRAINS")','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20818,'VS','BULK, SCRAP (IRON REMELTING)','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20819,'VY','BULK, SOLID, FINE PARTICLES ("POWDERS")','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20820,'WA','INTERMEDIATE BULK CONTAINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20821,'WB','WICKERBOTTLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20822,'WC','INTERMEDIATE BULK CONTAINER, STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20823,'WD','INTERMEDIATE BULK CONTAINER, ALUMINIUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20824,'WF','INTERMEDIATE BULK CONTAINER, METAL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20825,'WG','INTERMEDIATE BULK CONTAINER, STEEL, PRESSURISED > 10 KPA','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20826,'WH','INTERMEDIATE BULK CONTAINER, ALUMINIUM, PRESSURISED > 10 KPA','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20827,'WJ','INTERMEDIATE BULK CONTAINER, METAL, PRESSURE 10 KPA','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20828,'WK','INTERMEDIATE BULK CONTAINER, STEEL, LIQUID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20829,'WL','INTERMEDIATE BULK CONTAINER, ALUMINIUM, LIQUID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20830,'WM','INTERMEDIATE BULK CONTAINER, METAL, LIQUID','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20831,'WN','INTERMEDIATE BULK CONTAINER, WOVEN PLASTIC, WITHOUT COAT/LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20832,'WP','INTERMEDIATE BULK CONTAINER, WOVEN PLASTIC, COATED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20833,'WQ','INTERMEDIATE BULK CONTAINER, WOVEN PLASTIC, WITH LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20834,'WR','INTERMEDIATE BULK CONTAINER, WOVEN PLASTIC, COATED AND LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20835,'WS','INTERMEDIATE BULK CONTAINER, PLASTIC FILM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20836,'WT','INTERMEDIATE BULK CONTAINER, TEXTILE WITHOUT COAT/LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20837,'WU','INTERMEDIATE BULK CONTAINER, NATURAL WOOD, WITH INNER LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20838,'WV','INTERMEDIATE BULK CONTAINER, TEXTILE, COATED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20839,'WW','INTERMEDIATE BULK CONTAINER, TEXTILE, WITH LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20840,'WX','INTERMEDIATE BULK CONTAINER, TEXTILE, COATED AND LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20841,'WY','INTERMEDIATE BULK CONTAINER, PLYWOOD, WITH INNER LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20842,'WZ','INTERMEDIATE BULK CONTAINER, RECONSTITUTED WOOD, WITH INNER LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20843,'XA','BAG, WOVEN PLASTIC, WITHOUT INNER COAT/LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20844,'XB','BAG, WOVEN PLASTIC, SIFT PROOF','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20845,'XC','BAG, WOVEN PLASTIC, WATER RESISTANT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20846,'XD','BAG, PLASTIC FILM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20847,'XF','BAG, TEXTILE, WITHOUT INNER COAT/LINER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20848,'XG','BAG, TEXTILE, SIFT PROOF','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20849,'XH','BAG, TEXTILE, WATER RESISTANT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20850,'XJ','BAG, PAPER, MULTI-WALL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20851,'XK','BAG, PAPER, MULTI-WALL, WATER RESISTANT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20852,'YA','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN STEEL DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20853,'YB','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN STEEL CRATE BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20854,'YC','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN ALUMINIUM DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20855,'YD','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN ALUMINIUM CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20856,'YF','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN WOODEN BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20857,'YG','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLYWOOD DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20858,'YH','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLYWOOD BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20859,'YJ','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN FIBRE DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20860,'YK','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN FIBREBOARD BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20861,'YL','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLASTIC DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20862,'YM','COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN SOLID PLASTIC BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20863,'YN','COMPOSITE PACKAGING, GLASS RECEPTACLE IN STEEL DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20864,'YP','COMPOSITE PACKAGING, GLASS RECEPTACLE IN STEEL CRATE BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20865,'YQ','COMPOSITE PACKAGING, GLASS RECEPTACLE IN ALUMINIUM DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20866,'YR','COMPOSITE PACKAGING, GLASS RECEPTACLE IN ALUMINIUM CRATE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20867,'YS','COMPOSITE PACKAGING, GLASS RECEPTACLE IN WOODEN BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20868,'YT','COMPOSITE PACKAGING, GLASS RECEPTACLE IN PLYWOOD DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20869,'YV','COMPOSITE PACKAGING, GLASS RECEPTACLE IN WICKERWORK HAMPER','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20870,'YW','COMPOSITE PACKAGING, GLASS RECEPTACLE IN FIBRE DRUM','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20871,'YX','COMPOSITE PACKAGING, GLASS RECEPTACLE IN FIBREBOARD BOX','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20872,'YY','COMPOSITE PACKAGING, GLASS RECEPTACLE IN EXPANDABLE PLASTIC PACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20873,'YZ','COMPOSITE PACKAGING, GLASS RECEPTACLE IN SOLID PLASTIC PACK','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20874,'ZA','INTERMEDIATE BULK CONTAINER, PAPER, MULTI-WALL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20875,'ZB','BAG  LARGE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20876,'ZC','INTERMEDIATE BULK CONTAINER, PAPER, MULTI-WALL, WATER RESISTANT','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20877,'ZD','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, WITH STRUCTURAL EQUIPMENT, SOLIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20878,'ZF','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, FREESTANDING, SOLIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20879,'ZG','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, WITH STRUCTURAL EQUIPMENT, PRESSURISED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20880,'ZH','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, FREESTANDING, PRESSURISED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20881,'ZJ','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, WITH STRUCTURAL EQUPMENT, LIQUIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20882,'ZK','INTERMEDIATE BULK CONTAINER, RIGID PLASTIC, FREESTANDING, LIQUIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20883,'ZL','INTERMEDIATE BULK CONTAINER, COMPOSITE, RIGID PLASTIC, SOLIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20884,'ZM','INTERMEDIATE BULK CONTAINER, COMPOSITE, FLEXIBLE PLASTIC, SOLIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20885,'ZN','INTERMEDIATE BULK CONTAINER, COMPOSITE, RIGID PLASTIC, PRESSURISED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20886,'ZP','INTERMEDIATE BULK CONTAINER, COMPOSITE, FLEXIBLE PLASTIC, PRESSURISED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20887,'ZQ','INTERMEDIATE BULK CONTAINER, COMPOSITE, LIQUIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20888,'ZR','INTERMEDIATE BULK CONTAINER, COMPOSITE, FLEXIBLE PLASTIC, LIQUIDS','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20889,'ZS','INTERMEDIATE BULK CONTAINER, COMPOSITE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20890,'ZT','INTERMEDIATE BULK CONTAINER, FIBREBOARD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20891,'ZU','INTERMEDIATE BULK CONTAINER, FLEXIBLE','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20892,'ZV','INTERMEDIATE BULK CONTAINER, METAL, OTHER THAN STEEL','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20893,'ZW','INTERMEDIATE BULK CONTAINER, NATURAL WOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20894,'ZX','INTERMEDIATE BULK CONTAINER, PLYWOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20895,'ZY','INTERMEDIATE BULK CONTAINER, RECONSTITUTED WOOD','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 20896,'ZZ','MUTUALLY DEFINED','PackingTypeCode',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25001,'^F','Final Declaration','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25002,'^P','Provisional Declaration (Duty Paid)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25003,'AD','Anti Dumping','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25004,'B^','Barter Trade Import','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25005,'C','Import Under Credit System','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25006,'DD','Deferred Payment','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25007,'F^','Import from FIZ','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25008,'F1','Import from FCZ','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25009,'F2','Import from FIZ (Sub-Contract Works)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25010,'F4','Import from FZ (Trade acceptance after subcontract works are completed)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25011,'FC','Final Declaration (Beer, Liquor & Cigarettes)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25012,'FD','Import to Duty Free Island','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25013,'FF','Import Vehicle from Duty Free Island (Final)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25014,'FT','Temporary Import Vehicle from Duty Free Island','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25015,'GF','Import Under BG (Final)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25016,'GP','Provisional Declaration (under Bank Guarantee)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25017,'I^','Normal Import (to PCA)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25018,'L^','LMW Import','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25019,'LQ','Importation of Tobacco','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25020,'LI','LMW � International Procurement Centre (IPC)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25021,'LR','LMW � Regional Procurement Centre (RPC)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25022,'LV','LMW � Value Added, Re-Manufacturing, Servicing & Repairing','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25023,'M^','Temporary Import (X-ref with Exemption Types, ST Exemption)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25024,'MS','MSC Import','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25025,'P^','Petroleum (Payment)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25026,'PC','Provisional Declaration (Beer, Liquor & Cigarettes)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25027,'PK','PCKP Sales Form 1','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25028,'PS','Partial Shipment','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25029,'R^','Rubber (Remiling/ Reprocessing)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25030,'S^','Import with Preferential Tariffs (X-ref with Special Treatment)(ASEAN, CEPT, Commonwealth)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25031,'ST','Import with Preferential (Self � Certification)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25032,'SC','Manufacturing Waste (SCRAP)','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25033,'SP','SSPT','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25034,'T^','Re-Import','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25035,'TE','Locally Assembled Vehicle to Duty Free Island','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25036,'TF','Locally Assembled Vehicle from Duty Free Island','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25037,'TH','Others','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25038,'U^','Payment Under Protest','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25039,'X^','Import with Exemption','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25040,'K8','GST K8 Transhipment','TransactionTypeK1',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  

 
INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25100,'^F','Final Declaration','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25101,'^P','Provisional Declaration (Duty Paid)','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25102,'B^','Barter Trade Export','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25103,'C^','Export Under Credit System','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25104,'D^','Export with Drawback Section 99 Customs Act','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25105,'D1','Export with Drawback Section 93 Customs Act 1967','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25106,'D2','Export with Refund Section 96 Customs Act 1967','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25107,'E^','Normal Export','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25108,'F^','Export to FCZ from PCA','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25109,'FD','Export from Duty Free Island','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25110,'F1','Export to FCZ from PCA (Return of subcontract work)','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25111,'F2','Export to FIZ from PCA','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25112,'F3','Export to FIZ from PCA (Return of Subcontract Work) Updated:16/03/2015','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25113,'FR','Re-Export Vehicle to Free Duty Island','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25114,'H^','Export with reconciliation/Deposit','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25115,'L^','LMW Export','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25116,'M^','Re-Export','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25117,'P^','Bunkering (Duty Paid)','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25118,'R^','Rubber (Remilled/Reprocessed) Updated:16/03/2015','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25119,'RD','Reconciliation/Deposit','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25120,'SS','Ships�stores/Provisions/Spares','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25121,'ST','Export (Self � Certification)','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25122,'TE','Temporary export','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25123,'TH','Others','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25124,'X^','Export with exemption','TransactionTypeK2',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25200,'D^','Dutiable on Export (Required BG - i.e. Vegetables, Fish, Fruit and etc) ','TransactionTypeK3',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25201,'DE','Excise dutiable goods  (i.e.: Car, Liquor, Playing Card, Mahjong, Petroleum & Tobacco)','TransactionTypeK3',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25202,'P^','Prohibited on Export (i.e. Turtle�s egg, Rotan [except to Sabah/Sarawak] & Poison Chemical)','TransactionTypeK3',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25203,'R^','Restriction of Movement (i.e. K12 : Jadual I and Jadual 2)','TransactionTypeK3',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25204,'T^','Normal Transportation (TC without Duty)','TransactionTypeK3',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25500,'B','Bunkering via Others modes (Duty not Paid)','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25501,'C','Bunkering via pipeline (Duty not Paid)','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25502,'E','Export transshipment','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25503,'I','Import transshipment','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25504,'M','Master (K8)','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25505,'MU','MOU THAILAND/MALAYSIA','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25506,'S','Ship�s Spares','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25507,'SP','SSPT (Sistem Skim Perlepasan Terpilih)','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25508,'T','Normal transshipment','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25509,'TH','Others','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25510,'U','Duty unpaid spare�s','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25511,'Z','Other','TransactionTypeK8',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25600,'^F','Final Declaration','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25601,'^P','Provisional Declaration (Duty Paid)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25602,'AD','Anti Dumping','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25603,'B^','Barter Trade Import','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25604,'D^','Completely Knock Down (Normal)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25605,'D1','Completely Knock Down (Other/FZ Local Sales)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25606,'DD','Deferred Payment','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25607,'E^','LWM Local Sales','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25608,'F^','Import from FZ','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25609,'F1','Import to PCA from FZ (Domestic Consumption)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25610,'F2','Import to PCA from FZ (Sub-Contract Works)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25611,'FA','Final Declaration (Anti Dumping)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25612,'GF','Import Under BG (Final)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25613,'GP','Provisional Declaration (Under Bank Guarantee)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25614,'I^','Normal Import (to PCA)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25615,'L^','LMW Import','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25616,'LS','LMW Local Sales with Preferential Tariffs (ASEAN, CEPT, COMM)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25617,'LX','LMW Local Sales with Exemption','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25618,'LI   ','LMW � International Procurement Centre (IPC)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25619,'LR ','LMW � Regional Procurement Centre (RPC)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25620,'LV','LMW � Value Added, Re-Manufacturing, Servicing & Repairing','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25621,'SC','Manufacturing Waste','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25622,'M^','Temporary Import (X-ref with Exemption Types, ST Exemption) ','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25623,'P^','Petroleum (Payment)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25624,'P1','Petroleum Evaporative Losses (Payment)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25625,'R^','Imported goods for remilling/reprocessing','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25626,'S^','Import with Preferential Tariffs (X-ref with Special Treatment)(ASEAN, CEPT, Commonwealth)','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25627,'T^','Re-Import','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25628,'TH','Others','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25629,'U^','Payment Under Protest','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25630,'X^','Import with Exemption','TransactionTypeK9',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25700,'SM1','verification method 1 - weighing of packed container','SOLAS',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25701,'SM2','verification method 2 - Calculation (formula=cargo+dunnage+container tare)','SOLAS',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25750,'WR','WAREHOUSE RECEIPT NO.','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25751,'TRL','TREASURY LETTER OF EXEMPTION AND DRAWBACK','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25752,'TIN','TEMPORARY IMPORT NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25753,'TEX','TEX SECTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25754,'TEN','TEMPORARY EXPORT NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25755,'STE','SALES TAX EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25756,'SRN','CUSTOMS DUTY RECEIPT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25757,'SPT','SPECIAL TREATMENT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25758,'SLR','SELLER OR MANUFACTURER NAME','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25759,'REN','RESIT NO.','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25760,'LMW','RUJUKAN LESEN GUDANG MENGILANG','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25761,'LMS','RUJUKAN LESEN GUDANG','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25762,'LB','LOCK BOX','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25763,'KRN','ORIGINAL CUSTOM STATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25764,'KPW','K.P.W.X NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25765,'KPN','JK78 NUMBER (POST)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25766,'KEO','GENERAL EXEMPTION NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25767,'K2N','K2 REGISTRATION NO/DATE:','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25768,'K1N','K1 NO.','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25769,'K12','LICENSE NO FOR TOBACCO AND LIQUOR','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25770,'G03','DIRECT DELIVERY DECLARATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25771,'G02','DANGEROUS GOODS DECLARATIONS','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25772,'G01','AMENDED DECLARATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25773,'FZS','FREE ZONE SUBCONTRACT NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25774,'FZN','FREE ZONE APPROVAL NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25775,'EC9','EC9-ASEAN PREFERENTIAL','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25776,'EC8','EC8-CUKAI TERTANGGUH','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25777,'EC7','EC7-PENDING TREASURY EXP. W/BANK GUARANTE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25778,'EC6','EC6-CUSTOMS EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25779,'EC5','EC5-LMW EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25780,'EC4','EC4-LMW EXEMPTION(MACHINERY/ACCESSORIES)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25781,'EC3','EC3-TREASURY & CJ EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25782,'EC2','EC2-SALES TAX EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25783,'EC1','EC1-TREASURY EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25784,'E12','ASEAN (CEPT) EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25785,'E11','PENDING EXEMPTION WITH B/G & CJ ','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25786,'E10','E10-RETURN CARGO EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25787,'DB6','DRAWBACK SECTIONS 99,29 AND 14 (2)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25788,'DB5','DRAWBACK SECTION 99 AND 19','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25789,'DB4','DRAWBACK SECTION 99 AND 29','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25790,'DB3','DRAWBACKS SECTIONS 93 AND 29','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25791,'DB2','DRAWBACK SECTIONS 93 AND 29','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25792,'DB1','DRAWBACK SECTION 93','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25793,'D26','DRAWBACK SECTION 99, 29 AND 19','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25794,'CJ','CJ REFERENCE NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25795,'CE9','CE9 - <K2> DRAWBACKS (SEKSYEN 99,29 & 14)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25796,'CE8','CE8 - <K2>DRAWBACKS (SEKSYEN 99 & 19)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25797,'CE7','CE7 - <K2>DRAWBACKS (SEKSYEN 99 & CJ 29)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25798,'CE6','CE6 - <K2> DRAWBACKS (SEKSYEN 99)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25799,'CE5','CE5 - <K2> DRAWBACKS (SEKSYEN 93 & CJ 29)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25800,'CE4','CE4 - <K2> DRAWBACKS (SEKSYEN 93)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25801,'CE3','CE3 - <K2> SALES TAX EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25802,'CE2','CE2 - <K2> GPB/LMW','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25803,'CE1','CE1 - <K2> TREASURY EXEMPTION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25804,'C2A','CUKAI JUALAN 2A (CJ2A)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25805,'C14','C14 DUTIES EXEMPTION SECTION 14(2)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25806,'C10','C10-<K2>DRAWBACKS(SEKSYEN 99,19 & 29)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25807,'BVN','BONDED VEHICLE REGISTRATION NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25808,'AVN','APPROVED VEHICLE REGISTRATION NUMBER ','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25809,'APN','ASIAN PREFERENTIAL NUMBER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25810,'AAS','TRANSPORT DOC� NO/MOVEMENT PRMT ','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25811,'970','PERMISSION FROM CUSTOM TO CARRY OUT ACTIVITIES','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25812,'929','GOODS DECL. FOR IMPORTATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25813,'911','IMPORT LICENCE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25814,'861','CERTIFICATE OF ORIGIN','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25815,'830','GOODS DECL. FOR EXPORTATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25816,'819','EXPORT TRANSIT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25817,'818','EXPORT TRANSHIPMENT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25818,'817','EXPORT (STA)','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25819,'811','EXPORT LICENCE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25820,'673','APPLICATION/PERMIT TO TRANSSHIP GOODS','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25821,'672','DECLARATION OF GOODS TAKEN OUT OF FREE ZONE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25822,'671','DECLARATION OF GOODS BROUGHT INTO FREE ZONE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25823,'669','CUSTOMS APPLICATION TO REMOVE/TRANSSHIP GOODS','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25824,'668','APPROVAL TO RELEASE DUTIABLE GOODS','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25825,'667','APPL PERMIT TO TRANSPORT GOODS WITHIN FEDERATION','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25826,'666','WILD LIFE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25827,'665','FEDERAL AGRICULTURE MKTG. AUTHORITY','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25828,'664','MIDA','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25829,'663','NETERINATY DEP.','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25830,'662','FISHERY DEPARTMENT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25831,'661','POLICE DEPARTMENT','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25832,'660','MINISTRY OF HEALTH','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25833,'640','DELIVERY ORDER','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25834,'636','LMW LICENSE NO','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25835,'430','BANKERS GUARANTEE','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select 25836,'271','PACKING LIST','SupportingDocumentType',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25850, N'FULL', N'FULL', N'DeclarationShipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 25851, N'PARTIAL', N'PARTIAL', N'DeclarationShipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25860, N'IMPORT', N'IMPORT', N'ContainerTransportType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 25861, N'EXPORT', N'EXPORT', N'ContainerTransportType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 25862, N'TRANSHIPMENT', N'TRANSHIPMENT', N'ContainerTransportType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 25870, N'KGM', N'KGM', N'WeightType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 25871, N'LBS', N'LBS', N'WeightType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 



INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 25880, N'GENERAL', N'GENERAL CARGO', N'CargoCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25881, N'DANGEROUS', N'DANGEROUS CARGO', N'CargoCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25882, N'REF GEN', N'REEFER GENERAL CARGO', N'CargoCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25883, N'REF DGS', N'REEFER DANGEROUS CARGO', N'CargoCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25884, N'MAR-POL', N'MARINE POLLUTANT', N'CargoCategory', 0, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25885, N'X MAR-POL', N'SEVERE MARINE POLLUTANT', N'CargoCategory', 0, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25886, N'CONSOL', N'CONSOLIDATION CARGO', N'CargoCategory', 1, N'', N'', N'System', '20120221 10:12:59.807', N'System', '20120221 10:12:59.807' UNION ALL
SELECT 25887, N'RUBBER', N'RUBBER', N'CargoCategory', 1, N'', N'', N'ADMIN', '20120301 16:18:21.947', N'ADMIN', '20120301 16:28:32.890' UNION ALL
SELECT 25888, N'CAR', N'CAR', N'CargoCategory', 1, N'', N'', N'ADMIN', '20120607 14:53:11.983', N'ADMIN', '20120607 14:53:11.983' UNION ALL
SELECT 25889, N'BONDED', N'BONDED', N'CargoCategory', 1, N'', N'', N'ADMIN', '20120607 15:02:58.277', N'ADMIN', '20120607 15:02:58.277' UNION ALL
SELECT 25890, N'PARTICLE', N'PARTICLE BOARD', N'CargoCategory', 1, N'', N'', N'ADMIN', '20120614 15:34:33.610', N'ADMIN', '20120614 15:34:33.610' UNION ALL
SELECT 25891, N'CAR (DG)', N'CAR (DG)', N'CargoCategory', 1, N'', N'', N'ADMIN', '20120625 15:38:58.360', N'ADMIN', '20120625 15:42:50.880' UNION ALL
SELECT 25892, N'ENGINE', N'ENGINE', N'CargoCategory', 1, N'', N'', N'NOPPASIT', '20140409 10:54:36.877', N'NOPPASIT', '20140410 11:55:45.210' UNION ALL
SELECT 25893, N'USED ENGINE', N'USED ENGINE', N'CargoCategory', 1, N'', N'', N'NOPPASIT', '20140410 11:51:10.853', N'NOPPASIT', '20140410 11:51:10.853' UNION ALL
SELECT 25894, N'SCRAP', N'SCRAP', N'CargoCategory', 1, N'', N'', N'SONGKRAN', '20151109 13:13:34.953', N'SONGKRAN', '20151109 13:13:34.953'


--INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
--SELECT 25900, N'NORMAL', N'NORMAL', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
--SELECT 25901, N'EXCEMPTION', N'EXEMPTION', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
--SELECT 25902, N'PAYABLE', N'PAYABLE', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
--SELECT 25903, N'TOTALDUTY', N'TOTALDUTY', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 25930, N'TC-NA', N'', N'TariffCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25931, N'TC-DOP', N'DUTY ON PERCENTAGE', N'TariffCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25932, N'TC-DON', N'DUTY ON VALUE', N'TariffCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25933, N'TC-DOA', N'DUTY ON BOTH OF ABOVE', N'TariffCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25934, N'TC-DOW', N'DUTY ON WITH', N'TariffCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 25940, N'TC-DOP', N'DUTY ON PERCENTAGE', N'DutyVehicle', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25941, N'TC-DON', N'DUTY ON VALUE', N'DutyVehicle', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25942, N'TC-DOA', N'DUTY ON BOTH OF ABOVE', N'DutyVehicle', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25943, N'TC-DOW', N'DUTY ON WITH', N'DutyVehicle', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 25950, N'C2', N'CJ2', N'DutyItemCode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25951, N'C5', N'CJ5', N'DutyItemCode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25952, N'PM', N'I/E PERMIT', N'DutyItemCode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 25953, N'TE', N'TRESURY EXEMPTION', N'DutyItemCode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL 
SELECT 25954, N'GS', N'CS', N'DutyItemCode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 26100, N'TM-STD', N'STANDARD', N'TariffType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26101, N'TM-ASS', N'ASSOCIATION', N'TariffType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  
--SELECT 26102, N'TM-DIS', N'DISCOUNT', N'TariffType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  



INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 26150, N'MOD', N'Modules', N'TariffDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26151, N'TM-TRA', N'Transactions', N'TariffDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26152, N'TM-USE', N'Users', N'TariffDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 26200, N'MOD', N'Kilo Bytes', N'TransDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26201, N'TM-TRA', N'Documents', N'TransDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26202, N'TM-USE', N'M3 / MT', N'TransDiscountType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 26220, N'OPT-TERMINAL', N'Terminal', N'OperatorType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26221, N'OPT-RAILYARD', N'Rail Operator', N'OperatorType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26222, N'OPT-EMPTYPARK', N'Yard/Empty Park', N'OperatorType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26223, N'OPT-WAREHOUSE', N'CFS/Warehouse', N'OperatorType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26224, N'OPT-RAILOPERATOR', N'Rail Operator', N'OperatorType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 26300, N'0', N'NO DUTY', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26301, N'1', N'DUTY BY AD VALOREM', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26302, N'2', N'DUTY BY SPECIFIC', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26303, N'3', N'DUTY BY WHICH EVER IS HIGHER', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26304, N'4', N'DUTY BY BOTH', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 26305, N'5', N'DUTY BY QUOTA', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 26306, N'6', N'DUTY BY VOLUME PER LITER', N'DutyMethod', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
Select 26400,'A','REGISTRAR OF BUSINESS', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26401,'B','REGISTRAR OF COMPANY', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26402,'C','REGISTRAR OF SOCIETIES', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26403,'D','REGISTRAR OF COOPERATIVE', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26404,'E','NATIONAL REGN DEPT(USE BY PENCEGAH)', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26405,'F','DEPARTMENT OF JUSTICE KKK', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26406,'G','STATUTORY BODIES', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26407,'H','STATE ECONOMIC DEVELOPMENT CORP', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26408,'I','INDIVIDUAL', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26409,'J','REGISTRAR OF PROF. BODIES', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26410,'K','FOREIGN NATIONAL (PASSPORT)', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26411,'L','POLICE', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26412,'M','ARMED FORCES', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26413,'N','DIPLOMATIC/AMBASSIES', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26414,'P','GOVERNMENT DEPARTMENT', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26415,'R','RULERS', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26416,'S','DOOR TO DOOR SERVICE', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26417,'X','FOREIGN COMPANY', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26418,'Z','INTERNATIONAL ORGANIZATION', N'BusinessType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


 
INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
Select 26501,'FCL','Full container load', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26502,'LCL','Less than container load', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26503,'CVC','Conventional cargo (On-deck)', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26504,'CVU','Conventional cargo (Under deck)', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26505,'LOC','Loose cargo/Odd size cargo (On-deck)', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26506,'BUC','Bulk cargo', N'StowageCodeType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  
 

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
Select 26551,'CKD','Completely Knocked Down', N'CustomVehicleType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
Select 26552,'CBU','Completely Build Unit', N'CustomVehicleType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 
