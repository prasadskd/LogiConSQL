--select * from master.company

TRUNCATE TABLE ROOT.REGISTRATION
TRUNCATE TABLE ROOT.RegisteredCompany
TRUNCATE TABLE ROOT.CompanyAddress
TRUNCATE TABLE ROOT.CompanyRegistrationActivity
TRUNCATE TABLE ROOT.CompanyRegistrationType
TRUNCATE TABLE ROOT.RegisteredCompanyDocs

Delete From Master.Company Where CompanyCode > 1000
Delete From Master.Branch Where  CompanyCode > 1000
Delete From Master.Merchant
Delete From Master.MerchantRelation

TRUNCATE TABLE OPERATION.DeclarationActivity
TRUNCATE TABLE OPERATION.DeclarationClause
TRUNCATE TABLE OPERATION.DeclarationContainer
TRUNCATE TABLE OPERATION.DeclarationDocuments
TRUNCATE TABLE OPERATION.DeclarationExcemption
TRUNCATE TABLE OPERATION.DeclarationHeader
TRUNCATE TABLE OPERATION.DeclarationInvoice
TRUNCATE TABLE OPERATION.DeclarationItem

TRUNCATE TABLE OPERATION.DeclarationShipment
TRUNCATE TABLE OPERATION.DeclarationSubItem
TRUNCATE TABLE OPERATION.OrderActivity
TRUNCATE TABLE OPERATION.OrderCargo
TRUNCATE TABLE OPERATION.OrderContainer
TRUNCATE TABLE OPERATION.OrderHeader
TRUNCATE TABLE OPERATION.OrderText
TRUNCATE TABLE OPERATION.OrderTruckMovement



DELETE SECURITY.[USER] WHERE USERID NOT IN ( 'admin@dnex.com.my','trader@dnex.com.my')

Delete FROM SECURITY.USERRIGHTS  WHERE USERID NOT IN ( 'admin@dnex.com.my','trader@dnex.com.my')