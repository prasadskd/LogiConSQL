INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1000, N'HLG', N'Haulage', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1001, N'TPT', N'Transport', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1002, N'WH', N'WareHouse', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1003, N'CFS', N'CFS', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1004, N'CY', N'Container Yard', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1005, N'FWD', N'FORWARDING', N'Module', 1, N'', N'', N'ADMIN', CAST(0x0000A6730088EFFB AS DateTime), N'ADMIN', CAST(0x0000A6730088EFFB AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1010, N'COLLECTION', N'Collection', N'ProcessType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1011, N'DELIVERY', N'Delivery', N'ProcessType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1012, N'DIVERSION', N'Diversion', N'ProcessType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1013, N'SHUNTING', N'Shunting', N'ProcessType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1020, N'AIR', N'Air', N'TransportMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1021, N'SEA', N'Sea', N'TransportMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1022, N'RAIL', N'Rail', N'TransportMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1023, N'ROAD', N'Road', N'TransportMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1030, N'CONTAINER', N'Containerized', N'FreightMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1031, N'BREAKBULK', N'BreakBulk', N'FreightMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D3 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1040, N'EMPTY', N'Empty', N'EFIndicator', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1041, N'FULL', N'Full', N'EFIndicator', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1050, N'CLIENT', N'Client', N'PUDOMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1051, N'DEPOT', N'Depot', N'PUDOMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1052, N'WHARF', N'Wharf', N'PUDOMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1053, N'CFS', N'Cfs', N'PUDOMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1054, N'YARD', N'Yard', N'PUDOMode', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1060, N'IMPORT', N'Import', N'JobType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1061, N'EXPORT', N'Export', N'JobType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1062, N'LOCAL', N'Local', N'JobType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1070, N'FCL', N'Fcl', N'ShipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1071, N'LCL', N'Lcl', N'ShipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1072, N'EMPTY', N'Empty', N'ShipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1073, N'CONV', N'CONV', N'ShipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1080, N'ORD.CMP', N'Order Completed', N'BillingRuleType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1081, N'CNT.DEL', N'Container Delivered', N'BillingRuleType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1082, N'CNT.CMP', N'Container Completed', N'BillingRuleType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1083, N'POD.REC', N'POD Received', N'BillingRuleType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1090, N'ORD', N'Order Completed', N'BillingMethodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1091, N'POD', N'POD Received', N'BillingMethodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1100, N'DAILY', N'Daily', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1101, N'WEEKLY', N'Weekly', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1102, N'BIMONTHLY', N'Bi-Monthly', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1103, N'MONTHLY', N'Monthly', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D4 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1110, N'ORD-STO', N'Order Based', N'StorageType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1111, N'JOB-STO', N'Job Based', N'StorageType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1120, N'18 WHEEL', N'18 Wheels', N'TrailerType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1130, N'B2B', N'B2B', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1131, N'LOADING', N'Loading', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1132, N'DELIVERY', N'Delivery', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1133, N'HANDLING', N'Handling', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1134, N'PIPELINE', N'Pipeline', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1140, N'LOOSE', N'Loose', N'CarHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1141, N'PALLET', N'Pallet', N'CarHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1142, N'SLIP', N'Slip', N'CarHandlingType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime), N'ADMIN', CAST(0x0000A65C007C07D5 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1150, N'ZIM', N'ZIM', N'CarType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1151, N'ZIMUS', N'ZIM USA', N'CarType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1152, N'APL', N'APL', N'CarType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1160, N'PKG', N'Package', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1161, N'PLT', N'Pallet', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1162, N'TON', N'Ton', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1163, N'UNIT', N'Unit', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1164, N'TRIP', N'Trip', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1165, N'TRUCK', N'Truck', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1166, N'CNT', N'Container', N'BillingUnitType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1170, N'AMT', N'Amount', N'DiscountType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1171, N'PER', N'Percent', N'DiscountType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DC AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1180, N'PLASTIC', N'Plastic', N'ProductCatery', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1181, N'FOOD', N'Food', N'ProductCatery', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1190, N'CUSTINV', N'Customer Invoice', N'InvoiceType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1191, N'CUSTCASH', N'Customer CashBill', N'InvoiceType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1200, N'FAH', N'FAHRENHEIT', N'TempratureType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1201, N'CEL', N'CELSISUS', N'TempratureType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1210, N'VEN', N'VENTILATION', N'VentilationType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1211, N'PER', N'PERFUSION', N'VentilationType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1212, N'CMH', N'CONTAINER', N'VentilationType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1220, N'A', N'A', N'ContainerGradeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1221, N'A+', N'A (+)', N'ContainerGradeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1222, N'B', N'B', N'ContainerGradeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1223, N'C', N'C', N'ContainerGradeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1240, N'HRD-CHG', N'Hard Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1241, N'FGT-CHG', N'Freight Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1242, N'STR-CHG', N'Storage Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1243, N'CNT-CHG', N'Container Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1244, N'PKG-CHG', N'Package Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1245, N'GEN-CHG', N'General Charge', N'ChargeType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1260, N'ORD-CM', N'Order', N'ChargeMethod', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1261, N'CNT-CM', N'Container', N'ChargeMethod', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1280, N'FWD-MR', N'Forwarder', N'RelationshipType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1281, N'YARD-MR', N'Yard', N'RelationshipType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1282, N'TRK-MR', N'Transporter', N'RelationshipType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1283, N'LINER-MR', N'Shipping Liner', N'RelationshipType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1284, N'WHR-MR', N'Warehouse', N'RelationshipType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1300, N'SEAPORT', N'Sea Port', N'PortType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1301, N'AIRPORT', N'Air Port', N'PortType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1302, N'HAULAGE', N'Haulage', N'PortType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1303, N'WAREHOUSE', N'WareHouse', N'PortType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1304, N'TRANSPORT', N'Transport', N'PortType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1320, N'CONTAINER', N'CONTAINER', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1321, N'TRUCK', N'TRUCK', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1322, N'CHASSIS', N'CHASSIS', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1323, N'FORKLIFT', N'FORKLIFT', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1324, N'REACHSTACKER', N'REACH-STACKER', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1325, N'TOPLOAD', N'TOP-LOADER', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1326, N'PRIMEMOVE', N'PRIME-MOVER', N'EquipmentType', 1, N'', N'', N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime), N'ADMIN', CAST(0x0000A65C007C07DD AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1350, N'CASH', N'CASH', N'PaymentType', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1351, N'CREDIT', N'CREDIT', N'PaymentType', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1360, N'AGENT', N'AGENT', N'PaymentTo', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1361, N'CUSTOMER', N'CUSTOMER', N'PaymentTo', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1362, N'PRINCIPAL', N'PRINCIPAL', N'PaymentTo', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1363, N'HAULIER', N'HAULIER', N'PaymentTo', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCatery], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1364, N'FORWARDER', N'FORWARDER', N'PaymentTo', 1, N'', N'', N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime), N'ADMIN', CAST(0x0000A67700DE1113 AS DateTime))

