/* Exec usp_BOGenerator 'Master','Process','D:\Project\Logicon\Logicon\' */

IF OBJECT_ID('usp_BOGenerator') IS NOT NULL
BEGIN 
    DROP PROC usp_BOGenerator
END 
GO
CREATE PROC usp_BOGenerator
    @schemaName varchar(50),
	@tableName varchar(50),
	@folderPath varchar(100)
AS 
 

BEGIN



SET NOCOUNT ON
 
		

DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
DECLARE @Tab as CHAR(1) = Char(9)
DECLARE @ClassStart as varchar(max)



delete Master.ClassGenerator


select @ClassStart = 'using System;' +
@NewLineChar + 'using System.Collections.Generic;' +
@NewLineChar + 'using '+ @schemaName + '.Contract;' +
@NewLineChar + 'using '+ @schemaName + '.DataFactory;' +
@NewLineChar + 
@NewLineChar + 'namespace '+ @schemaName + '.BusinessFactory' +
@NewLineChar + '{' +
@NewLineChar + @Tab + 'public class ' + @tableName + 'BO ' + 
@NewLineChar + @Tab + '{'+
@NewLineChar + @Tab + '		private ' + @tableName + 'DAL ' + LOWER(@tableName) + 'DAL; '+
@NewLineChar + @Tab + '        public ' + @tableName + 'BO() ' +
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab +  @Tab +	LOWER(@tableName) + 'DAL = new ' + @tableName +'DAL();'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '        public List<' + @tableName +'> GetList(Int64 branchID)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.GetList(branchID);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '        public List<' + @tableName +'> GetPageView(Int64 branchID,Int64 fetchRows)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.GetPageView(branchID,fetchRows);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '        public Int64 GetRecordCount(Int64 branchID)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.GetRecordCount(branchID);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + 
@NewLineChar + @Tab + '        public List<' + @tableName +'> Search' + @tableName +'(Int64 branchID)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.Search' + @tableName +'(branchID);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '       public bool Save' + @tableName +'(' + @tableName +' newItem)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + 
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.Save(newItem);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '        public bool Delete' + @tableName +'(' + @tableName +' item)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return ' + LOWER(@tableName) + 'DAL.Delete(item);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + 
@NewLineChar + @Tab + '        public ' + @tableName +' Get' + @tableName +'(' + @tableName +' item)'+
@NewLineChar + @Tab + '        {'+
@NewLineChar + @Tab + '            return (' + + @tableName +')' + LOWER(@tableName) + 'DAL.GetItem<' + @tableName + '>(item);'+
@NewLineChar + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '    }' +
@NewLineChar + '    }'

truncate table Master.ClassGenerator

Insert into Master.ClassGenerator
Select @ClassStart


print @ClassStart



DECLARE @command NVARCHAR(1000)
SET @command = 'BCP "Select * From Logicon.Master.ClassGenerator" queryout "'+ @folderPath + @SchemaName + '.BusinessFactory\' + @tableName + 'BO.cs" -c -S laptop-ilofhpm7\sqlexpress -U sa -P manager -t "|" '
exec master.sys.xp_cmdshell @command
SET NOCOUNT OFF;

Select * From Logicon.Master.ClassGenerator

END
 