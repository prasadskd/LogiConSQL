/* Exec usp_DALGenerator @schemaName='Master',@tableName='CustomDeclarant',@folderPath='F:\Project\Logicon\LogiconApi\LogiconAPI\' */

IF OBJECT_ID('usp_DALGenerator') IS NOT NULL
BEGIN 
    DROP PROC usp_DALGenerator
END 
GO
CREATE PROC usp_DALGenerator
    @schemaName varchar(50),
	@tableName varchar(50),
	@folderPath varchar(100)
AS 
 

BEGIN



SET NOCOUNT ON
 
		

DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
DECLARE @Tab as CHAR(1) = Char(9)
DECLARE @ClassStart as varchar(max)

DECLARE @saveCommandParameter varchar(max)
DECLARE @deleteCommandParameter varchar(max)


 
Exec usp_CommandParameterGenerator @schemaName,@tableName,1
Select @saveCommandParameter= ContractClass from Master.ClassGenerator

Print @saveCommandParameter

delete Master.ClassGenerator
Exec usp_CommandParameterGenerator @schemaName,@tableName,0
Select @deleteCommandParameter= ContractClass from Master.ClassGenerator

print '----------------'
Print @deleteCommandParameter

delete Master.ClassGenerator



select @ClassStart = 'using System;' +
@NewLineChar + 'using System.Collections.Generic;' +
@NewLineChar + 'using System.Linq;' +
@NewLineChar + 'using System.Text;' +
@NewLineChar + 'using System.Threading.Tasks;' +
@NewLineChar + 'using System.Data.Common;' +
@NewLineChar + 'using Microsoft.Practices.EnterpriseLibrary.Common;' +
@NewLineChar + 'using Microsoft.Practices.EnterpriseLibrary.Data;' +
@NewLineChar + 'using Microsoft.Practices.EnterpriseLibrary.Data.Sql;' +
@NewLineChar + 'using '+ @schemaName + '.Contract;' +
@NewLineChar + 'using '+ @schemaName + '.DataFactory;' +
@NewLineChar + 
@NewLineChar + 'namespace '+ @schemaName + '.DataFactory' +
@NewLineChar + '{' +
@NewLineChar + @Tab + 'public class ' + @tableName + 'DAL ' + 
@NewLineChar + @Tab + '{'+
@NewLineChar + @Tab + @Tab + '		  private Database db; '+
@NewLineChar + @Tab + @Tab + '        private DbTransaction currentTransaction = null;'+
@NewLineChar + @Tab + @Tab + '        private DbConnection connection = null;'+
@NewLineChar + @Tab + @Tab + '        /// <summary>'+
@NewLineChar + @Tab + @Tab + '        /// Constructor'+
@NewLineChar + @Tab + @Tab + '        /// </summary>'+
@NewLineChar + @Tab + @Tab + '        public ' + @tableName + 'DAL()'+
@NewLineChar + @Tab + @Tab + '        {'+
@NewLineChar + 
@NewLineChar + @Tab + @Tab + '            db = DatabaseFactory.CreateDatabase("LogiCon");'+
@NewLineChar + 
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        #region IDataFactory Members'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '		  public List<'+@tableName+'> GetList(Int64 branchID)'+
@NewLineChar + @Tab + @Tab + '		  {'+
@NewLineChar + @Tab + @Tab + '            return db.ExecuteSprocAccessor(DBRoutine.LIST'+UPPER(@tableName)+', MapBuilder<'+@tableName+'>.BuildAllProperties(),branchID).ToList();'+
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '		  public List<'+@tableName+'> GetPageView(Int64 branchID,Int64 fetchRows)'+
@NewLineChar + @Tab + @Tab + '		  {'+
@NewLineChar + @Tab + @Tab + '            return db.ExecuteSprocAccessor(DBRoutine.PAGEVIEW'+UPPER(@tableName)+', MapBuilder<'+@tableName+'>.BuildAllProperties(),branchID,fetchRows).ToList();'+
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '		  public Int64 GetRecordCount(Int64 branchID)'+
@NewLineChar + @Tab + @Tab + '		  {'+
@NewLineChar + @Tab + @Tab + '            var recordcommand = db.GetStoredProcCommand(DBRoutine.RECORDCOUNT'+UPPER(@tableName)+ ');' +
@NewLineChar + @Tab + @Tab + '			  db.AddInParameter(recordcommand, "BranchID", System.Data.DbType.Int64, branchID);' +          
@NewLineChar + @Tab + @Tab + '            var recordCount = Convert.ToInt64(db.ExecuteScalar(recordcommand));' +
@NewLineChar + @Tab + @Tab + '			  return recordCount; ' +
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '		  public List<'+@tableName+'> Search'+@tableName+'(Int64 branchID)'+
@NewLineChar + @Tab + @Tab + '		  {'+
@NewLineChar + @Tab + @Tab + '            return db.ExecuteSprocAccessor(DBRoutine.AUTOCOMPLETESEARCH'+UPPER(@tableName)+',MapBuilder<'+@tableName+'>' +
@NewLineChar + @Tab + @Tab + '                                                    .MapAllProperties()'+
@NewLineChar + @Tab + @Tab + '                                                    .Build(),branchID).ToList();'+
@NewLineChar + @Tab + @Tab + '		  }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        public bool Save<T>(T item, DbTransaction parentTransaction) where T : IContract'+
@NewLineChar + @Tab + @Tab + '        {'+
@NewLineChar + @Tab + @Tab + '            currentTransaction = parentTransaction;'+
@NewLineChar + @Tab + @Tab + '            return Save(item);'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        public bool Save<T>(T item) where T : IContract'+
@NewLineChar + @Tab + @Tab + '        {'+
@NewLineChar + @Tab + @Tab + '            var result = 0;'+
@NewLineChar + @Tab + @Tab + '            var '+LOWER(@tableName)+' = ('+@tableName+')(object)item;'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            if (currentTransaction == null)'+
@NewLineChar + @Tab + @Tab + '            {'+
@NewLineChar + @Tab + @Tab + '                connection = db.CreateConnection();'+
@NewLineChar + @Tab + @Tab + '                connection.Open();'+
@NewLineChar + @Tab + @Tab + '            }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            var transaction = (currentTransaction == null ? connection.BeginTransaction() : currentTransaction);'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            try'+
@NewLineChar + @Tab + @Tab + '            {               '+
@NewLineChar + @Tab + @Tab +
@NewLineChar + @Tab + @Tab + '				  var savecommand = db.GetStoredProcCommand(DBRoutine.SAVE'+UPPER(@tableName)+');'+
@NewLineChar + @Tab + @Tab + @Tab + @Tab + @Tab + @Tab +  @saveCommandParameter +
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '                result = db.ExecuteNonQuery(savecommand, transaction);'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '                if (currentTransaction == null)'+
@NewLineChar + @Tab + @Tab + '                    transaction.Commit();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            }'+
@NewLineChar + @Tab + @Tab + '            catch (Exception)'+
@NewLineChar + @Tab + @Tab + '            {'+
@NewLineChar + @Tab + @Tab + '                if (currentTransaction == null)'+
@NewLineChar + @Tab + @Tab + '                    transaction.Rollback();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '                throw;'+
@NewLineChar + @Tab + @Tab + '            }'+
@NewLineChar + @Tab + @Tab + '			  finally {'+
@NewLineChar + @Tab + @Tab + '                if (currentTransaction == null)'+
@NewLineChar + @Tab + @Tab + '                {'+
@NewLineChar + @Tab + @Tab + '                    transaction.Dispose();'+
@NewLineChar + @Tab + @Tab + '                    connection.Close();'+
@NewLineChar + @Tab + @Tab + '                }'+
@NewLineChar + @Tab + @Tab + '            } '+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            return (result > 0 ? true : false);'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        public bool Delete<T>(T item) where T : IContract'+
@NewLineChar + @Tab + @Tab + '        {'+
@NewLineChar + @Tab + @Tab + '            var result = false;'+
@NewLineChar + @Tab + @Tab + '            var '+LOWER(@tableName)+' = ('+@tableName+')(object)item;'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            connection = db.CreateConnection();'+
@NewLineChar + @Tab + @Tab + '            connection.Open();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            var transaction = connection.BeginTransaction();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            try'+
@NewLineChar + @Tab + @Tab + '            {'+
@NewLineChar + @Tab + @Tab + '                var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETE'+UPPER(@tableName)+');'+
@NewLineChar + @Tab + @Tab + @Tab + @Tab + @Tab + @Tab + @deleteCommandParameter +
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '                result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '                transaction.Commit();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            }'+
@NewLineChar + @Tab + @Tab + '            catch (Exception ex)'+
@NewLineChar + @Tab + @Tab + '            {'+
@NewLineChar + @Tab + @Tab + '                transaction.Rollback();'+
@NewLineChar + @Tab + @Tab + '                throw ex;'+
@NewLineChar + @Tab + @Tab + '            }'+
@NewLineChar + @Tab + @Tab + '			  finally {'+
@NewLineChar + @Tab + @Tab + '                    transaction.Dispose();'+
@NewLineChar + @Tab + @Tab + '                    connection.Close();'+
@NewLineChar + @Tab + @Tab + '            } '+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            return result;'+
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        public IContract GetItem<T>(IContract lookupItem) where T : IContract'+
@NewLineChar + @Tab + @Tab + '        {'+
@NewLineChar + @Tab + @Tab + '            var item = (('+@tableName+')lookupItem);'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            var '+LOWER(@tableName)+'Item = db.ExecuteSprocAccessor(DBRoutine.SELECT'+UPPER(@tableName)+','+
@NewLineChar + @Tab + @Tab + '                                                    MapBuilder<'+@tableName+'>.BuildAllProperties(),'+
@NewLineChar + @Tab + @Tab + '                                                    item.Param).FirstOrDefault();'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            if ('+LOWER(@tableName)+'Item == null) return null;'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '            return '+LOWER(@tableName)+'Item;'+
@NewLineChar + @Tab + @Tab + '        }'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '        #endregion'+
@NewLineChar + @Tab + @Tab + 
@NewLineChar + @Tab + @Tab + '    }' +
@NewLineChar + '    }'

truncate table Master.ClassGenerator

Insert into Master.ClassGenerator
Select @ClassStart


print @ClassStart



DECLARE @command NVARCHAR(1000)
SET @command = 'BCP "Select * From Logicon.Master.ClassGenerator" queryout "'+ @folderPath + @SchemaName + '.DataFactory\' + @tableName + 'DAL.cs" -c -S laptop-ilofhpm7\sqlexpress -U sa -P manager -t "|" '
exec master.sys.xp_cmdshell @command
SET NOCOUNT OFF;

Select * From Logicon.Master.ClassGenerator

END
 