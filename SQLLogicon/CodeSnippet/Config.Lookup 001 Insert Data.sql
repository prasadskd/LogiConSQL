 

DELETE CONFIG.Lookup where LookupID <10000
GO

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1000, N'HLG', N'HAULAGE', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1001, N'TPT', N'TRANSPORT', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1002, N'WH', N'WAREHOUSE', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1003, N'CFS', N'CFS', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1004, N'CY', N'CONTAINER YARD', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL
Select 1005, N'FWD', N'FREIGHT/FWD', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL
Select 1006, N'RAIL', N'RAIL', N'Module', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1010, N'COLLECTION', N'COLLECTION', N'ProcessType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1011, N'DELIVERY', N'DELIVERY', N'ProcessType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1012, N'DIVERSION', N'DIVERSION', N'ProcessType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1013, N'SHUNTING', N'SHUNTING', N'ProcessType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1014, N'STAGING', N'STAGING', N'ProcessType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1020, N'AIR', N'AIR', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1021, N'SEA', N'SEA', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1022, N'RAIL', N'RAIL', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1023, N'ROAD', N'ROAD', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1024, N'POST', N'POST', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1025, N'FIXTRANSPORT', N'FIX TRANSPORT INSTALLATION', N'TransportMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1030, N'CONTAINER', N'CONTAINERIZED', N'FreightMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1031, N'BREAKBULK', N'BREAKBULK', N'FreightMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1040, N'EMPTY', N'EMPTY', N'EFIndicator', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1041, N'FULL', N'FULL', N'EFIndicator', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1050, N'CLIENT', N'CLIENT', N'PUDOMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1051, N'DEPOT', N'DEPOT', N'PUDOMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1052, N'WHARF', N'WHARF', N'PUDOMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1053, N'CFS', N'CFS', N'PUDOMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1054, N'YARD', N'YARD', N'PUDOMode', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1060, N'IMPORT', N'IMPORT', N'JobType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1061, N'EXPORT', N'EXPORT', N'JobType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1062, N'LOCAL', N'LOCAL', N'JobType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1063, N'TRANSHIPMENT', N'TRANSHIPMENT', N'JobType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()
 
INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1070,'FCL (local)','FCL (local)','ShipmentType',1,'1','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1071,'FCL (foreign)','FCL (foreign)','ShipmentType',1,'2','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1072,'LCL (local)','LCL (local)','ShipmentType',1,'3','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1073,'LCL (foreign)','LCL (foreign)','ShipmentType',1,'4','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1074,'Empty (local)','Empty (local)','ShipmentType',1,'5','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1075,'Empty (foreign)','Empty (foreign)','ShipmentType',1,'6','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1076,'Empty','Empty','ShipmentType',1,'7','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1077,'Conventional Cargo','Conventional Cargo','ShipmentType',1,'8','','ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 




INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1080, N'ORD.CMP', N'ORDER COMPLETED', N'BillingRuleType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1081, N'CNT.DEL', N'CONTAINER DELIVERED', N'BillingRuleType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1082, N'CNT.CMP', N'CONTAINER COMPLETED', N'BillingRuleType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1083, N'POD.REC', N'POD RECEIVED', N'BillingRuleType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1090, N'ORD', N'ORDER COMPLETED', N'BillingMethodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1091, N'POD', N'POD RECEIVED', N'BillingMethodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1100, N'DAILY', N'DAILY', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1101, N'WEEKLY', N'WEEKLY', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL 
Select 1102, N'BIMONTHLY', N'BI-MONTHLY', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1103, N'MONTHLY', N'MONTHLY', N'CalculationPeriodType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1110, N'ORD-STO', N'ORDER BASED', N'StorageType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1111, N'JOB-STO', N'JOB BASED', N'StorageType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1120, N'TRL-LOW', N'LOW LOADER', N'TrailerType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1121, N'TRL-NORMAL', N'NORMAL TRAILER', N'TrailerType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1122, N'TRL-SIDE', N'SIDE LOADER TRAILER', N'TrailerType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1123, N'TRL-TIPPER', N'TIPPER TRAILER', N'TrailerType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1130, N'B2B', N'B2B', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1131, N'LOADING', N'LOADING', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1132, N'DELIVERY', N'DELIVERY', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1133, N'HANDLING', N'HANDLING', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1134, N'PIPELINE', N'PIPELINE', N'SpecialHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1140, N'LOOSE', N'LOOSE', N'CargoHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1141, N'PALLET', N'PALLET', N'CargoHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1142, N'SLIP', N'SLIP', N'CargoHandlingType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()




INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1150, N'ZIM', N'ZIM', N'CargoType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1151, N'ZIMUS', N'ZIM USA', N'CargoType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1152, N'APL', N'APL', N'CargoType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1160, N'PKG', N'PACKAGE', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1161, N'PLT', N'PALLET', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1162, N'TON', N'TON', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1163, N'UNIT', N'UNIT', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1164, N'TRIP', N'TRIP', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1165, N'TRUCK', N'TRUCK', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1166, N'CNT', N'CONTAINER', N'BillingUnitType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1170, N'AMT', N'AMOUNT', N'DiscountType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1171, N'PER', N'PERCENTAGE', N'DiscountType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1180, N'PLASTIC', N'PLASTIC', N'ProductCategory', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1181, N'FOOD', N'FOOD', N'ProductCategory', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1190, N'CUSTINV', N'CUSTOMER INVOICE', N'InvoiceType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1191, N'CUSTCASH', N'CUSTOMER CASHBILL', N'InvoiceType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1200, N'FAH', N'FAHRENHEIT', N'TempratureType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1201, N'CEL', N'CELSISUS', N'TempratureType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 



INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1210, N'VEN', N'VENTILATION', N'VentilationType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1211, N'PER', N'PERFUSION', N'VentilationType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1212, N'CMH', N'CONTAINER', N'VentilationType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


 
INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1240, N'HRD-CHG', N'HARD CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1241, N'FGT-CHG', N'FREIGHT CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1242, N'STR-CHG', N'STORAGE CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1243, N'CNT-CHG', N'CONTAINER CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1244, N'PKG-CHG', N'PACKAGE CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL
Select 1245, N'GEN-CHG', N'GENERAL CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1246, N'VAS-CHG', N'VAS CHARGE', N'ChargeType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()   


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1260, N'ORD-CM', N'ORDER', N'ChargeMethod', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1261, N'CNT-CM', N'CONTAINER', N'ChargeMethod', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1280, N'FWD-MR', N'FORWARDER', N'RelationshipType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1281, N'YARD-MR', N'YARD', N'RelationshipType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1282, N'TRK-MR', N'TRANSPORTER', N'RelationshipType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1283, N'LINER-MR', N'SHIPPING LINER', N'RelationshipType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1284, N'WHR-MR', N'WAREHOUSE', N'RelationshipType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1300, N'SEAPORT', N'SEA PORT', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1301, N'AIRPORT', N'AIR PORT', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1302, N'HAULAGE', N'HAULAGE', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1303, N'WAREHOUSE', N'WAREHOUSE', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1304, N'TRANSPORT', N'TRANSPORT', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL 
Select 1305, N'WHARF', N'WHARF', N'PortType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1311, N'OWN', N'OWN', N'ContainerHireMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1312, N'ON-HIRE', N'ON-HIRE', N'ContainerHireMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1313, N'OFF-HIRE', N'OFF-HIRE', N'ContainerHireMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1320, N'CONTAINER', N'CONTAINER', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1321, N'TRUCK', N'TRUCK', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1322, N'CHASSIS', N'CHASSIS', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1323, N'FORKLIFT', N'FORKLIFT', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1324, N'REACHSTACKER', N'REACH-STACKER', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL
Select 1325, N'TOPLOAD', N'TOP-LOADER', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()  UNION ALL
Select 1326, N'PRIMEMOVE', N'PRIME-MOVER', N'EquipmentType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 



INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1330, N'STL', N'STEEL', N'ContainerMaterial', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1331, N'ALM', N'ALUMINIUM', N'ContainerMaterial', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1340, N'NONE', N'NONE', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1341, N'A', N'FOOD GRADE', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1342, N'A+', N'BUILD GRADE', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1343, N'B', N'GENERAL CARGO GRADE', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1344, N'C', N'ENGINE, PALLET CARGO GRADE', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1345, N'STD', N'STANDARD CONTAINER', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1346, N'UPP', N'UPGRADE CONTAINER', N'ContainerGrade', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1347, N'C+', N'CONDITION', N'ContainerGrade', 1, N'', N'', N'SONGKRAN', '20140828 11:12:54.283', N'SONGKRAN', '20140828 11:12:54.283'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1351, N'8''6"', N'8''6"', N'ContainerHeight', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1352, N'9''6"', N'9''6"', N'ContainerHeight', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1360, N'TRST-NONE', N'NONE', N'TrailerStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1361, N'TRST-AVAILABLE', N'AVAILABLE', N'TrailerStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1362, N'TRST-UNAVAILABLE', N'UNAVAILABLE', N'TrailerStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1363, N'TRST-UNCOUPLE', N'UNCOUPLE', N'TrailerStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE()

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1370, N'TRK-GEN', N'GENERAL PRIME MOVER', N'TruckType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 

INSERT [Config].[Lookup] ([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])  
Select 1380, N'TRKST-AVAILABLE', N'AVAILABLE', N'TruckStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1381, N'TRKST-UNAVAILABLE', N'UNAVAILABLE', N'TruckStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() UNION ALL
Select 1382, N'TRKST-BREAKDOWN', N'BREAKDOWN', N'TruckStatusType', 1, N'', N'', N'ADMIN', GETUTCDATE(), N'ADMIN', GETUTCDATE() 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1401, N'P/U OWN', N'P/U OWN', N'PickupMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1402, N'P/U OTHER', N'P/U OTHER', N'PickupMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1403, N'P/U ONLY', N'P/U ONLY', N'PickupMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1404, N'P/U PORT', N'PICK UP FULL TO PORT ONLY', N'PickupMode', 1, N'', N'', N'ADMIN', '20120705 14:31:48.723', N'ADMIN', '20120705 14:31:48.723'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1411, N'D/O OWN', N'D/O OWN', N'DropOffMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1412, N'D/O OTHER', N'D/O OTHER', N'DropOffMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1413, N'D/O ONLY', N'D/O OTHER', N'DropOffMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1414, N'D/O CUS', N'D/O CUS', N'DropOffMode', 1, N'', N'', N'ADMIN', '20120705 14:33:13.823', N'ADMIN', '20120705 14:33:13.823'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1501, N'IN', N'INWARD', N'MovementIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1502, N'OUT', N'OUTWARD', N'MovementIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1511, N'PICK-UP CONT', N'PICK-UP CONT', N'TruckActivityIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1512, N'DROP-OFF CONT', N'DROP-OFF CONT', N'TruckActivityIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1513, N'PICK-UP CARGO', N'PICK-UP CARGO', N'TruckActivityIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1514, N'DROP-OFF CARGO', N'DROP-OFF CARGO', N'TruckActivityIndicator', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1521, N'INC-PER', N'PERCENTAGE', N'IncentiveType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1522, N'INC-FIX', N'FIX RATE', N'IncentiveType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1531, N'TRN-NT', N'NORMAL TRAILER', N'TransportType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1532, N'TRN-SL', N'SIDE LOADER', N'TransportType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1541, N'HML-COLLECTION', N'COLLECTION', N'HaulageMovementType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1542, N'HMT-DELIVERY', N'DELIVERY', N'HaulageMovementType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1543, N'HML-STAGING', N'STAGING', N'HaulageMovementType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1544, N'HMT-DIVESION', N'DIVERSION', N'HaulageMovementType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1545, N'HML-SHUNTING', N'SHUNTING', N'HaulageMovementType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1551, N'HHT-B2B', N'B2B', N'HaulageHandlingType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1552, N'HHT-OH', N'OVER HEIGHT', N'HaulageHandlingType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1553, N'HHL-OL', N'OVER LENGTH', N'HaulageHandlingType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1554, N'HHT-SCAN', N'SCAN', N'HaulageHandlingType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1555, N'HHL-TRIPPING', N'TIPPING', N'HaulageHandlingType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1561, N'UG-ACC', N'ACCOUNTS', N'UserGroupType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1562, N'UG-ADMIN', N'ADMINISTRATION', N'UserGroupType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1563, N'UG-MANAGER', N'MANAGERIAL', N'UserGroupType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1564, N'UG-OPR', N'OPERATIONS',N'UserGroupType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1565, N'UG-TECH', N'TECHNICAL',N'UserGroupType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1571, N'UD-CLERK', N'CLERK', N'UserDesignationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1572, N'UD-MANAGER', N'MANAGER', N'UserDesignationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1573, N'UD-SYSADMIN', N'SYSTEM ADMINISTRATOR', N'UserDesignationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1574, N'UD-EXECUTIVE', N'EXECUTIVE',N'UserDesignationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1575, N'UD-OPERATOR', N'OPERATOR',N'UserDesignationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1580, N'JR-MANDATORY', N'MANDATORY', N'JobCategoryRuleType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1581, N'JR-BUSINESSRULE', N'BUSINESS RULES', N'JobCategoryRuleType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1582, N'JR-VISIBILITY', N'VISIBILITIES', N'JobCategoryRuleType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1590, N'PCT-NA', N'MANDATORY', N'ProductControl', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1591, N'PCT-OPTIONAL', N'OPTIONAL', N'ProductControl', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1592, N'PCT-MANDATORYIN', N'MANDATORY(INWARD)', N'ProductControl', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1593, N'PCT-MANDATORYOUT', N'MANDATORY(OUTWARD)', N'ProductControl', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1600, N'PCA-A', N'A', N'ProductCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1601, N'PCA-B', N'B', N'ProductCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1602, N'PCA-C', N'C', N'ProductCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1603, N'PCA-D', N'D', N'ProductCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 1610, N'CREATED', N'CREATED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1611, N'PROGRESS', N'PROGRESS', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1612, N'PENDING', N'PENDING', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1613, N'UNPLANNED', N'UNPLANNED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1614, N'PLANNED', N'PLANNED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 1615, N'COMPLETED', N'COMPLETED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1616, N'CANCELLED', N'CANCELLED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 1617, N'CLOSED', N'CLOSED', N'StateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
Select  1620,'1','BREAK BULK','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1621,'2','CONTAINER','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1622,'6','PASSENGGER','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1623,'7','VEHICLES','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1624,'9','GENERAL','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1625,'A','AUTOMOBILES (VEHICLE)','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1626,'B','BULK CARGO','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1627,'B1','BREAK BULK (LPK)','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1628,'B2','SOLID BULK','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1629,'B3','LIQUID BULK','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1630,'B4','DRY BULK','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1631,'C','DOCUMENTS & PUBLICATIONS','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1632,'D','DOC & PUBLIC','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1633,'E','EMPTY CONTAINER','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1634,'F','FOOD PRODUCTS','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1635,'G','DANGEROUS/EXPLOSIVE','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1636,'H','PERSONAL','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1637,'L','LIVESTOCK','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1638,'N','CONTAINERIZED','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1639,'O','OTHERS','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1640,'P','FROZEN','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1641,'S','SPECIAL','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1642,'V','VALUABLES','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1643,'W','PACKAGES','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() UNION ALL
Select  1644,'X','EXPRESS CARGO','CargoClass',1,'','','ADMIN',GETUTCDATE(),'ADMIN',GETUTCDATE() 




INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 2170, N'D-01', N'DOOR PANEL ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:12:01.020', N'JANTIRA', '20120519 18:12:01.020' UNION ALL
SELECT 2171, N'D-02', N'DOOR HINGE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:12:45.427', N'JANTIRA', '20120519 18:12:45.427' UNION ALL
SELECT 2172, N'D-03', N'DOOR GASKET ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:13:22.017', N'JANTIRA', '20120519 18:13:22.017' UNION ALL
SELECT 2173, N'D-04', N'DOOR HOLDER ROPE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:13:52.460', N'JANTIRA', '20120519 18:13:52.460' UNION ALL
SELECT 2174, N'D-05', N'LOCKING ROD ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:16:54.733', N'JANTIRA', '20120519 18:16:54.733' UNION ALL
SELECT 2175, N'D-06', N'LOCK ROD CAM', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:17:26.277', N'JANTIRA', '20120519 18:17:26.277' UNION ALL
SELECT 2176, N'D-07', N'LOCK ROD BRACKET', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:17:55.827', N'JANTIRA', '20120519 18:17:55.827' UNION ALL
SELECT 2177, N'D-08', N'LOCK ROD GUAIDE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:18:17.137', N'JANTIRA', '20120519 18:18:17.137' UNION ALL
SELECT 2178, N'D-09', N'LOCK ROD HANDLE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:18:45.543', N'JANTIRA', '20120519 18:18:45.543' UNION ALL
SELECT 2179, N'D-10', N'DOOR HEADER', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:19:05.750', N'JANTIRA', '20120519 18:19:05.750' UNION ALL
SELECT 2180, N'D-11', N'DOOR SILL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:19:21.943', N'JANTIRA', '20120519 18:19:21.943' UNION ALL
SELECT 2181, N'D-12', N'DOOR CORNER POST (RIGHT) ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:19:43.807', N'JANTIRA', '20120519 18:19:43.807' UNION ALL
SELECT 2182, N'D-13', N'DOOR CORNER POST (LEFT)', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:20:03.447', N'JANTIRA', '20120519 18:20:03.447' UNION ALL
SELECT 2183, N'F-01', N'FRONT PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:21:06.557', N'JANTIRA', '20120519 18:21:06.557' UNION ALL
SELECT 2184, N'F-02', N'SAFETY PIN', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:21:34.407', N'JANTIRA', '20120519 18:21:34.407' UNION ALL
SELECT 2185, N'F-03', N'POWER CABLE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:21:50.783', N'JANTIRA', '20120519 18:21:50.783' UNION ALL
SELECT 2186, N'F-04', N'POWER PLUG', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:22:08.050', N'JANTIRA', '20120519 18:22:08.050' UNION ALL
SELECT 2187, N'F-05', N'COVER FAN', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:22:24.203', N'JANTIRA', '20120519 18:22:24.203' UNION ALL
SELECT 2188, N'F-06', N'BOX CABLE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:22:38.900', N'JANTIRA', '20120519 18:22:38.900' UNION ALL
SELECT 2189, N'F-07', N'COVER GRAPH ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:22:55.560', N'JANTIRA', '20120519 18:22:55.560' UNION ALL
SELECT 2190, N'F-08', N'COMPRESSOR', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:23:12.407', N'JANTIRA', '20120519 18:23:12.407' UNION ALL
SELECT 2191, N'F-09', N'FRONT HEADER', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:23:26.920', N'JANTIRA', '20120519 18:23:26.920' UNION ALL
SELECT 2192, N'F-10', N'FRONT SILL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:23:41.027', N'JANTIRA', '20120519 18:23:41.027' UNION ALL
SELECT 2193, N'F-11', N'FRONT CORNER POST (RIGHT) ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:23:56.227', N'JANTIRA', '20120519 18:23:56.227' UNION ALL
SELECT 2194, N'F-12', N'FRONT CORNER POST (LEFT) ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:24:11.347', N'JANTIRA', '20120519 18:24:11.347' UNION ALL
SELECT 2195, N'RS-01', N'RIGHT SIDE PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:25:24.790', N'JANTIRA', '20120519 18:25:24.790' UNION ALL
SELECT 2196, N'RS-02', N'RIGHT SIDE VENTILATOR ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:25:42.767', N'JANTIRA', '20120519 18:25:42.767' UNION ALL
SELECT 2197, N'RS-03', N'RIGHT SIDE TOP-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:25:59.673', N'JANTIRA', '20120519 18:25:59.673' UNION ALL
SELECT 2198, N'RS-04', N'RIGHT SIDE BOTTOM-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:26:17.353', N'JANTIRA', '20120519 18:26:17.353' UNION ALL
SELECT 2199, N'LS-01', N'LEFT SIDE PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:27:29.000', N'JANTIRA', '20120519 18:27:29.000' UNION ALL
SELECT 2200, N'LS-02', N'LEFT SIDE VENTILATOR', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:27:44.413', N'JANTIRA', '20120519 18:27:44.413' UNION ALL
SELECT 2201, N'LS-03', N'LEFT SIDE TOP-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:28:00.257', N'JANTIRA', '20120519 18:28:00.257' UNION ALL
SELECT 2202, N'LS-04', N'LEFT SIDE BOTTOM-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:28:17.693', N'JANTIRA', '20120519 18:28:17.693' UNION ALL
SELECT 2203, N'ROOF', N'ROOF PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:29:00.800', N'JANTIRA', '20120519 18:29:00.800' UNION ALL
SELECT 2204, N'CROSS', N'CROSSMEMBER', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:29:35.743', N'JANTIRA', '20120519 18:29:35.743' UNION ALL
SELECT 2205, N'IN-01', N'RIGHT INSIDE PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:30:31.973', N'JANTIRA', '20120519 18:30:31.973' UNION ALL
SELECT 2206, N'IN-02', N'LEFT INSIDE PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:30:48.330', N'JANTIRA', '20120519 18:30:48.330' UNION ALL
SELECT 2207, N'IN-03', N'INSIDE ROOF PANEL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:31:05.963', N'JANTIRA', '20120519 18:31:05.963' UNION ALL
SELECT 2208, N'IN-04', N'AIR BAFFLE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:31:20.817', N'JANTIRA', '20120519 18:31:20.817' UNION ALL
SELECT 2209, N'IN-05', N'DRAIN PLUG ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:31:44.140', N'JANTIRA', '20120519 18:31:44.140' UNION ALL
SELECT 2210, N'IN-06', N'FLOOR', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:32:00.630', N'JANTIRA', '20120519 18:32:00.630' UNION ALL
SELECT 2211, N'IN-07', N'SEAMRIM-RIGHT TOP-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:32:15.220', N'JANTIRA', '20120519 18:32:15.220' UNION ALL
SELECT 2212, N'IN-08', N'SEAMRIM-LEFT TOP-RAIL', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:32:30.783', N'JANTIRA', '20120519 18:32:30.783' UNION ALL
SELECT 2213, N'IN-09', N'FRONT INNER PLATE', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:32:45.610', N'JANTIRA', '20120519 18:32:45.610' UNION ALL
SELECT 2214, N'IN-10', N'SEAMRIM-LEFT BOTTOM-RAIL ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:33:06.740', N'JANTIRA', '20120519 18:33:06.740' UNION ALL
SELECT 2215, N'IN-11', N'SEAMRIM-RIGHT BOTTOM-RAIL ', N'DamageIndicator', 1, N'', N'', N'JANTIRA', '20120519 18:33:21.100', N'JANTIRA', '20120519 18:33:21.100'
 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3001, N'CASH', N'CASH', N'PaymentTerm', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3002, N'CREDIT', N'CREDIT', N'PaymentTerm', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3003, N'FREE', N'FREE', N'PaymentTerm', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3501, N'AGENT', N'AGENT', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3502, N'CUSTOMER', N'SHIPPER/CONSIGNEE', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3503, N'FWD', N'FORWARDING AGENT', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3504, N'HAULIER', N'HAULIER', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3505, N'DRIVER', N'DRIVER', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3506, N'THIRDPARTY', N'THIRDPARTY TRANSPORTER', N'PaymentTo', 1, N'', N'', N'SYSTEM', '20121017 14:46:30.013', N'SYSTEM', '20121017 14:46:30.013'




INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3850, N'NONE', N'NONE', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3851, N'ASIA', N'ASIA', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3852, N'EUROPE', N'EUROPE', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3853, N'AMERICA', N'AMERICA', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3854, N'AUSTRALIA', N'AUSTRALIA', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3855, N'AFRICA', N'AFRICA', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3856, N'MIDDLEEAST', N'MIDDLE EAST', N'TradeMode', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3860, N'STANDARD', N'STANDARD', N'QuotationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3861, N'CUSTOMER', N'CUSTOMER', N'QuotationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3862, N'VENDOR', N'VENDOR', N'QuotationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3870, N'DRV-CON', N'CONTRACT', N'DriverType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3871, N'DRV-PMT', N'PERMANENT', N'DriverType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3880, N'DRV-CAT', N'DRIVER', N'DriverCategoryType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3881, N'DRV-ATD', N'ATTENDANT', N'DriverCategoryType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 3890, N'INCO-EXW', N'EXW (Ex-Works)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3891, N'INCO-FCA', N'FCA (Free Carrier)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3892, N'INCO-CPT', N'CPT (Carriage Paid To)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3893, N'INCO-CIP', N'CIP (Carriage and Insurance Paid To)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3894, N'INCO-DDU', N'DDU (Delivered Duty Unpaid)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3895, N'INCO-DAF', N'DAF (Delivered At Frontier)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3896, N'INCO-DDP', N'DDP (Delivered Duty Paid)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3897, N'INCO-FAS', N'FAS (Free Alongside Ship)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3898, N'INCO-FOB', N'FOB (Free On Board)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3899, N'INCO-CFR', N'CFR (Cost and Freight)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3900, N'INCO-CIF', N'CIF (Cost, Insurance and Freight)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3901, N'INCO-DES', N'DES (Delivered Ex Ship)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 3902, N'INCO-DEQ', N'DEQ (Delivered Ex Quay)', N'IncoTerms', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'




INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4001, N'4 WHEELS', N'4 WHEELS', N'TruckCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4002, N'6 WHEELS', N'6 WHEELS', N'TruckCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4003, N'10 WHEELS', N'10 WHEELS', N'TruckCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4005, N'18 WHEELS', N'18 WHEELS', N'TruckCategory', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4006, N'MULTI-TRUCK', N'MULTI-TRUCK', N'TruckCategory', 1, N'', N'', N'System', '20120220 15:58:42.043', N'System', '20120220 15:58:42.043' UNION ALL
SELECT 4008, N'22 WHEELS', N'22 WHEELS', N'TruckCategory', 1, N'', N'', N'System', '20120227 10:23:22.203', N'System', '20120227 10:23:22.203'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4021, N'SQ-01',N'What is the first and last name of your first boyfriend or girlfriend?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4022, N'SQ-02',N'Which phone number do you remember most from your childhood?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4023, N'SQ-03',N'What was your favourite place to visit as a child?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4024, N'SQ-04',N'Who is your favourite actor, musician, or artist?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4025, N'SQ-05',N'What is the name of your favourite pet?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4026, N'SQ-06',N'In what city were you born?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4027, N'SQ-07',N'What high school did you attend?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4028, N'SQ-08',N'What is the name of your first school?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4029, N'SQ-09',N'What is your favourite movie?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4030, N'SQ-10',N'What is your mother''s maiden name?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4031, N'SQ-11',N'What street did you grow up on?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4032, N'SQ-12',N'What was the make of your first car?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4033, N'SQ-13',N'When is your anniversary?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4034, N'SQ-14',N'What is your favourite colour?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4035, N'SQ-15',N'What is your father''s middle name?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4036, N'SQ-16',N'What is the name of your first grade teacher?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4037, N'SQ-17',N'What was your high school mascot?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4038, N'SQ-18',N'Which is your favourite web browser?', N'SecurityQuestion', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4041, N'ORG-SP',N'SOLE PROPRIETORSHIP', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4042, N'ORG-PP',N'PARTNERSHIP', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4043, N'ORG-SB',N'SENDIRIAN BERHAD', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4044, N'ORG-BH',N'BERHAD', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4045, N'ORG-FC',N'FOREIGN COMPANY', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4046, N'ORG-LP',N'LIMITED LIABILITY PARTNERSHIP', N'OrganizationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

 
INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4051, N'REG-TR',N'TRADER', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4052, N'REG-AG',N'AGENT', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4053, N'REG-OW',N'OWNER', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4054, N'REG-HG',N'HAULAGE', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4055, N'REG-DP',N'DEPOT', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4056, N'REG-WH',N'WAREHOUSE', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4057, N'REG-TR',N'TRANSPORT', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4058, N'REG-FR',N'FREIGHT', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4059, N'REG-MN',N'MANIFEST', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9150, N'REG-ST',N'1STATS', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9151, N'REG-CF',N'CFS', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9152, N'REG-DE',N'DECLARATION', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL 
SELECT 9153, N'REG-PT',N'PORT', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL  
SELECT 9154, N'REG-SL',N'SALES', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL 
SELECT 9155, N'REG-BL',N'BILLING', N'RegistrationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  


 

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4061, N'RST-DR',N'Draft Submitted', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4062, N'RST-EV',N'Awaiting Email Verification', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4063, N'RST-VR',N'Awaiting Verification', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4064, N'RST-PY',N'Awaiting Payment', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4065, N'RST-TR',N'Awaiting Approval', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4066, N'RST-ER',N'Rejected', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4067, N'RST-EA',N'Approved', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4068, N'RST-EQ',N'On Query', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4069, N'RST-CL',N'Awaiting Clarification', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4070, N'RST-VE',N'Email Verified', N'RegistrationStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4081, N'FORM-9',N'Form 9 � Certificate of Incorporation', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4082, N'FORM-13',N'Form 13 � Certification of Incorporation on Change of Company Name', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4083, N'FORM-49',N'Form 49 � Particulars of Directors, Managers and Secretaries', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4084, N'SP-GST',N'Surat Pendaftaran GST (GST Registration Letter)', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4085, N'SLAK-KDRM',N'Surat Lesen Agen Kastam dari KDRM (Custom Agent License Letter from KDRM)', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4086, N'LLP-CER',N'LLP Certificate', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4087, N'LLP-AGR',N'LLP Agreement', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4088, N'AEO-APP',N'AEO Approval Letter', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4089, N'ATO-APP',N'ATS Approval Letter', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 4090, N'FC-COI',N'Certified Copy of the Certificate of Incorporation or Registration of the Foreign Company', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4091, N'FC-DIR',N'Foreign Company Particulars of Directors', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4092, N'LMW-LIC',N'LMW License', N'RegistrationDocType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4101, N'S-SV',N'Save', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4102, N'S-DE',N'Delete', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4103, N'S-VW',N'View', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4104, N'S-AP',N'Approve', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4105, N'S-RE',N'Reject', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4106, N'S-VR',N'Verify', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4107, N'S-EX',N'Export', N'SecurableItem', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4200, N'LG-MD',N'MasterData', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4201, N'LG-OP',N'Operations', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4202, N'LG-FF',N'Forwarding', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4203, N'LG-HL',N'Haulage', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4204, N'LG-TR',N'Transport', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4205, N'LG-DE',N'Depot', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4206, N'LG-CF',N'CFS', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4207, N'LG-WM',N'WMS', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4208, N'LG-BL',N'Billing', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4209, N'LG-IN',N'Inquiry', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4210, N'LG-RE',N'Reports', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4211, N'LG-SS',N'SystemSettings', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4212, N'LG-AD',N'Administration', N'LinkGroup', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'

INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4250, N'VC-FG',N'FOREIGN GOING', N'VesselClass', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4300, N'WEB-NONE',N'None', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4301, N'WEB-SFA',N'Sent To Forwarding Agent', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4302, N'WEB-PAF',N'Pending Approval', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4303, N'WEB-AFA',N'Approved By Forwarding Agent', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4304, N'WEB-RFA',N'Rejected By Forwarding Agent', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 4305, N'WEB-D1',N'Declaration K1', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4306, N'WEB-D1A',N'Declaration K1A', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4307, N'WEB-D2',N'Declaration K2', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4308, N'WEB-D3',N'Declaration K3', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4309, N'WEB-D4',N'Declaration K4', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4310, N'WEB-D5',N'Declaration K5', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4311, N'WEB-D6',N'Declaration K6', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4312, N'WEB-D7',N'Declaration K7', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4313, N'WEB-D8',N'Declaration K8', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL 
SELECT 4314, N'WEB-D9',N'Declaration K9', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4315, N'WEB-ORIGINAL',N'Original Order', N'WebOrderStatus', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 4350, N'OA-TR',N'TRADER', N'OrderActivityParty', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 4351, N'OA-FA',N'FWD AGENT', N'OrderActivityParty', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 



INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 9000, N'VT-27',N'27-CONTAINER CONVENTIONAL', N'VesselType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9001, N'VT-28',N'28-BULK CARGO', N'VesselType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9002, N'VT-29',N'29-RORO', N'VesselType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 



INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 9101, N'K1',N'K1', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9102, N'K2',N'K2', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9103, N'K3',N'K3', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 9104, N'K4',N'K4', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9105, N'K5',N'K5', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9106, N'K6',N'K6', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 9107, N'K7',N'K7', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9108, N'K8',N'K8', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'  UNION ALL
SELECT 9109, N'K9',N'K9', N'DeclarationType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' 


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 9120, N'Clause1',N'Clause1', N'DeclarationClause', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9121, N'Clause2',N'Clause2', N'DeclarationClause', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'


INSERT INTO [Config].[Lookup]([LookupID], [LookupCode], [LookupDescription], [LookupCategory], [Status], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
SELECT 9130, N'DT-ISU',N'ISSUED', N'DateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9131, N'DT-EXP',N'EXPIRED', N'DateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220' UNION ALL
SELECT 9132, N'DT-REG',N'REGISTERED', N'DateType', 1, N'', N'', N'SYSTEM', '20101225 14:47:17.220', N'SYSTEM', '20101225 14:47:17.220'   
