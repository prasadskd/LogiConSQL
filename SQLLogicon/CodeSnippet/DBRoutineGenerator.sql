 


SET NOCOUNT ON
DECLARE @Tab as CHAR(1) = Char(9)
DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
DECLARE @vData nvarchar(max)


DECLARE @Schema as varchar(50) ='Depot'
--DECLARE @TableName as varchar(50) ='Company'

Declare @tblUsertable table(id smallint primary key identity(1,1),schemaName varchar(50),tableName varchar(50))

insert into @tblUsertable
Select TABLE_SCHEMA,TABLE_NAME from INFORMATION_SCHEMA.TABLES
Where TABLE_SCHEMA = ISNULL(@Schema,TABLE_SCHEMA)
And TABLE_NAME <> 'ClassGenerator'

declare @vschema varchar(50),
		@vtable varchar(50),
		@vcount smallint,
		@vcounter smallint
		
select @vcount=count(0),@vcounter=1 from @tblUsertable
 

while @vcounter<=@vcount
begin

	Select @vschema=schemaname,@vtable = tablename 
	from @tblUserTable where id=@vcounter

	
select @vData='';
	

Select  @Tab +@Tab + '/// <summary>'   +
		@NewLineChar + @Tab +@Tab + '/// [' + @vschema  + '].[' + @vtable + ']'  +
		@NewLineChar + @Tab +@Tab + '/// </summary>'  +
		@NewLineChar + @Tab +@Tab + ' '
UNION ALL
select	@Tab +@Tab + 
		
'public const string ' + 
Case when CharIndex('List',routine_name) > 0 then ' LIST' 
when CharIndex('Select',routine_name) > 0 then ' SELECT' 
when CharIndex('Delete',routine_name) > 0 then ' DELETE' 
when CharIndex('Save',routine_name) > 0 then ' SAVE' 
End 
+

Case when CharIndex('List',routine_name) > 0 then UPPER(REPLACE(REPLACE(ROUTINE_NAME,'usp_','') ,'List',''))
when CharIndex('Select',routine_name) > 0 then UPPER(REPLACE(REPLACE(ROUTINE_NAME,'usp_','') ,'Select','')) 
when CharIndex('Delete',routine_name) > 0 then UPPER(REPLACE(REPLACE(ROUTINE_NAME,'usp_','') ,'Delete',''))
when CharIndex('Save',routine_name) > 0 then UPPER(REPLACE(REPLACE(ROUTINE_NAME,'usp_','') ,'Save',''))
end  

+
' = "[' + ROUTINE_SCHEMA + '].[' + ROUTINE_NAME + ']";'   from information_schema.routines
where routine_type='PROCEDURE'
and ROUTINE_SCHEMA =@vschema 
and SPECIFIC_NAME not like '%Insert%'
and SPECIFIC_NAME not like '%Update%'
and SPECIFIC_NAME Like '%' +  @vtable + '%'

--order by ROUTINE_NAME
 


	set @vcounter=@vcounter+1
End	
 