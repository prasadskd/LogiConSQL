-- ========================================================================================================================================
-- START											 [Reports].[usp_StatementTaxInvoice]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jan-2017
-- Description:	 

-- [Reports].[usp_StatementTaxInvoice] 1000001,1003
-- ========================================================================================================================================


IF OBJECT_ID('[Reports].[usp_StatementTaxInvoice]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_StatementTaxInvoice] 
END 
GO
CREATE PROC [Reports].[usp_StatementTaxInvoice] 
    @BranchID BIGINT,
	@AccountNo INT
     
AS 

BEGIN

Select Top(1) Cr.CompanyName,BrAd.Address1,BrAd.Address2,BrAd.City,BrAd.State,BrAd.ZipCode,BrAd.TelNo,
BrAd.FaxNo,RegAdd.ContactPerson1,Cr.GSTNo,
Hd.StatementNo,Hd.StatementDate,DateAdd(M,1,Hd.StatementDate) As DueDate,Hd.TotalAmount,
0 As OverDueCharges,Hd.GSTRate
From Billing.StatementHeader Hd
Inner Join Billing.StatementDetail Dt ON
Hd.BranchID = Dt.BranchID
And Hd.StatementNo = Dt.StatementNo
Inner Join Master.Company Cr On 
	Hd.AccountNo = Cr.CompanyCode
Left Outer Join Master.Address BrAd On	
	Cr.CompanyCode = BrAd.LinkId
	And AddressType='Company'
Left Outer Join Root.CompanyAddress RegAdd On 
	Cr.RegistrationReferenceNo = RegAdd.CompanyID
Where 
Hd.BranchID = @BranchID
And Hd.AccountNo = @AccountNo
Order By Hd.StatementDate Desc
 

End