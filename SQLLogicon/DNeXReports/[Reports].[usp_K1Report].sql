
-- ========================================================================================================================================
-- START											 [Reports].[usp_K1Report]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationHeader] Record based on [DeclarationHeader] table

-- Exec [Reports].[usp_K1Report] 1007001, 'DLHQ170100002'
-- ========================================================================================================================================


IF OBJECT_ID('[Reports].[usp_K1Report]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_K1Report] 
END 
GO
CREATE PROC [Reports].[usp_K1Report]
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], 
						Hd.[ExporterState], Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.CustomStationCode,Hd.ShippingAgent,Hd.DeclarantID,
						Hd.DeclarantName,Hd.DeclarantNRIC,Hd.DeclarantDesignation,HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, 
						Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,
						ISNULL(LP.PortName,'''') As LoadingPortName,
						ISNULL(DP.PortName,'''') As DischargePortName,
						ISNULL(TP.PortName,'''') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'''') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'''') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'''') As TransactionTypeDescription,
						--Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						--LT.MerchantName As ImporterName,
						--SA.MerchantName As ShippingAgentName,
						--TR.LookupCode As TransactionTypeCode,
						LTAdd.Address1 As ImporterAddress1,LTAdd.Address2 As ImporterAddress2, 
						LTAdd.Address3 as ImporterAddress3,LTAdd.City As ImporterCity, LTAdd.State As ImporterState,
						Inv.[InvoiceNo], Inv.[InvoiceDate], Inv.[InvoiceValue], Inv.[InvoiceCurrencyCode], Inv.[LocalCurrencyCode], Inv.[CurrencyExRate],
						Inv.[IncoTerm], Inv.[PayCountry], Inv.[PortAmountPercent], Inv.[PortAmountCurrencyCode], Inv.[PortAmountExRate], Inv.[PortAmountValue], Inv.[FreightAmountPercent], 
						Inv.[IsFreightCurrency], Inv.[FreightAmountCurrencyCode], Inv.[FreightAmountExRate], Inv.[FreightAmountValue], Inv.[FreightIncoTerm], 
						Inv.[InsuranceAmountPercent], Inv.[IsInsuranceCurrency], Inv.[InsuranceAmountCurrencyCode], Inv.[InsuranceAmountExRate], 
						Inv.[InsuranceAmountValue], Inv.[InsuranceIncoTerm], Inv.[OthersAmountPercent], Inv.[OthersAmountCurrencyCode], Inv.[OthersAmountExRate], 
						Inv.[OthersAmountValue], Inv.[CargoClassCode], Inv.[CargoDescription1], Inv.[CargoDescription2], Inv.[PackageQty], Inv.[PackingTypeCode], 
						Inv.[PackingMaterialCode], Inv.[GrossWeight], Inv.[UOMWeight], Inv.[GrossVolume], Inv.[UOMVolume], Inv.[Status], 
						Inv.FOBAmount,Inv.CIFAmount,Inv.EXWAmount,Inv.CNFAmount,Inv.CNIAmount,Inv.FreightAmount,Inv.InsuranceAmount,Inv.CIFCAmount,
						DCnt.[ContainerNo], DCnt.[Size], DCnt.[Type], DCnt.[ContainerStatus], DCnt.[EQDStatus], 
						DCnt.[MarksNos]
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.DeclarationInvoice Inv ON 
		Hd.BranchId = Inv.BranchID
		And Hd.DeclarationNo = Inv.DeclarationNo
	Left Outer Join Operation.DeclarationContainer DCnt ON 
		Hd.BranchId = DCnt.BranchID
		And Hd.DeclarationNo = DCnt.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = DT.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = DT.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = Convert(varchar(10),LT.MerchantCode)
	Left OUter JOin Master.Merchant ET ON 
		Hd.Exporter = Convert(varchar(10),ET.MerchantCode)
	Left Outer Join Master.Address LTAdd ON 
		Lt.MerchantCode = LTAdd.LinkID
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = Convert(varchar(10),SA.MerchantCode)
	Left Outer Join Master.Address ETAdd ON 
		ET.MerchantCode = ETAdd.LinkID
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfImport = POI.PortCode
	Left Outer Join Master.Port sLP ON 
		Sh.LoadingPort = sLP.PortCode
	Left Outer Join Master.Port sDP ON 
			Sh.DischargePort = sDP.PortCode
	Left Outer Join Master.Port sTP ON 
			Sh.TranshipmentPort = sTP.PortCode
	Left Outer Join Master.Merchant PO ON 
			Sh.PortOperator = PO.MerchantCode
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo 



END




 