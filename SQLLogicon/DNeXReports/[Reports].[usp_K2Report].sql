-- ========================================================================================================================================
-- START											 [Reports].[usp_K2Report]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationHeader] Record Into [DeclarationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Reports].[usp_K2Report]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_K2Report] 
END 
GO
CREATE PROC [Reports].[usp_K2Report] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			IncoTermDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'IncoTerms')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ExportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],
						--Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], Hd.[ExporterState], 
						Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.CustomStationCode,Hd.ShippingAgent,Hd.DeclarantID,
						Hd.DeclarantName,Hd.DeclarantNRIC,Hd.DeclarantDesignation,HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, 
						Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,sh.PlaceOfExport,
						Sh.HouseBLNo, Sh.OceanBLNo,
						ISNULL(LP.PortName,'') As LoadingPortName,
						--ISNULL(POI.PortName,'''') As PlaceOfImportName,
						ISNULL(DP.PortName,'') As DischargePortName,
						ISNULL(TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						ISNULL(ITD.LookupDescription,'') As IncoTermDescription,
						--Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						--LT.MerchantName As ImporterName,
						--TR.LookupCode As TransactionTypeCode,
						LT.RegNo As ImporterROCNo,LT.MerchantName As ImporterName,
						LTAdd.Address1 As ImporterAddress1,LTAdd.Address2 As ImporterAddress2, 
						LTAdd.Address3 as ImporterAddress3,LTAdd.City As ImporterCity, LTAdd.State As ImporterState,
						Inv.[InvoiceNo], Inv.[InvoiceDate], Inv.[InvoiceValue], Inv.[InvoiceCurrencyCode], Inv.[LocalCurrencyCode], Inv.[ExchangeRate],
						Inv.[LocalCurrencyValue],Inv.[AmountReceived],Inv.[AmountReceivedCurrencyCode],Inv.[AmountReceivedLocalCurrencyCode],Inv.[AmountReceivedExchangeRate],
						Inv.[AmountReceivedLocalValue],Inv.[IncoTerms], Inv.[FreightAmount], Inv.[FreightCurrencyCode],Inv.[FreightExchangeRate],Inv.[IsFreightGrossWeight],
						Inv.[InsuranceAmount],Inv.[InsuranceCurrencyCode],Inv.[InsuranceExchangeRate],Inv.[InsuranceIncoTerm],
						Inv.[OtherAmount], Inv.[OtherCurrencyCode], Inv.[OtherExchangeRate], Inv.[PortAmount], Inv.[PortCurrencyCode], Inv.[PortExchangeRate],Inv.[IsrequireSTA],
						0 As FOBAmount,0 As CIFAmount,0 As EXWAmount,0 As CNFAmount,0 As CNIAmount,0 As FreightAmount1,0 As InsuranceAmount1,0 As CIFCAmount,
						0 As GrossWeight, '' UOMWeight, 0 As GrossVolume, '' UOMVolume,
						--Inv.[CargoClassCode], Inv.[CargoDescription1], Inv.[CargoDescription2], Inv.[PackageQty], Inv.[PackingTypeCode], 
						--Inv.[PackingMaterialCode], Inv.[GrossWeight], Inv.[UOMWeight], Inv.[GrossVolume], Inv.[UOMVolume], Inv.[Status], 
						--Inv.FOBAmount,Inv.CIFAmount,Inv.EXWAmount,Inv.CNFAmount,Inv.CNIAmount,Inv.FreightAmount,Inv.InsuranceAmount,Inv.CIFCAmount,
						DCnt.[ContainerNo], DCnt.[Size], DCnt.[Type], DCnt.[ContainerStatus], DCnt.[EQDStatus], 
						DCnt.[MarksNos],						
						Br.BranchName,Br.RegNo,Br.GSTNo,
						ET.MerchantName As ExporterName,
						ET.RegNo As ExporterROCNo,ETAdd.Address1 As ExporterAddress1,ETAdd.Address2 As ExporterAddress2, 
						ETAdd.Address3 as ExporterAddress3,ETAdd.City As ExporterCity, ETAdd.State As ExporterState,						
						SA.MerchantName As ShippingAgentName,
						SA.RegNo As ShippingAgentROCNo,SAAdd.Address1 As ShippingAgentAddress1,SAAdd.Address2 As ShippingAgentAddress2, 
						SAAdd.Address3 as ShippingAgentAddress3,SAAdd.City As ShippingAgentCity, SAAdd.State As ShippingAgentState,
						CSC.StationDescription

	FROM	[Operation].[DeclarationHeaderK2] Hd
	Left Outer Join Operation.DeclarationShipmentK2 Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.DeclarationInvoiceK2 Inv ON 
		Hd.BranchId = Inv.BranchID
		And Hd.DeclarationNo = Inv.DeclarationNo
	Left Outer Join IncoTermDescription ITD ON 
		Inv.IncoTerms = ITD.LookupID
	Left Outer Join Operation.DeclarationContainerK2 DCnt ON 
		Hd.BranchId = DCnt.BranchID
		And Hd.DeclarationNo = DCnt.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = Convert(varchar(10),LT.MerchantCode)
	Left OUter JOin Master.Merchant ET ON 
		Hd.Exporter = Convert(varchar(10),ET.MerchantCode)
	Left Outer Join Master.Address LTAdd ON 
		Convert(varchar(10),LT.MerchantCode) = LTAdd.LinkID
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = Convert(varchar(10),SA.MerchantCode)
	Left Outer Join Master.Address SAAdd ON 
		Convert(varchar(10),SA.MerchantCode) = SAAdd.LinkID
	Left Outer Join Master.Address ETAdd ON 
		Convert(varchar(10),ET.MerchantCode) = ETAdd.LinkID
	--Left Outer Join Master.Port POI ON 
	--	Sh.PlaceOfImport = POI.PortCode
	Left Outer Join Master.Port sLP ON 
		Sh.LoadingPort = sLP.PortCode
	Left Outer Join Master.Port sDP ON 
			Sh.DischargePort = sDP.PortCode
	Left Outer Join Master.Port sTP ON 
			Sh.TranshipmentPort = sTP.PortCode
	Left Outer Join Master.Merchant PO ON 
			Sh.PortOperator = PO.MerchantCode
	LEFT OUTER JOIN [Master].Branch Br ON 
		Hd.BranchID = Br.BranchID
	LEFT OUTER JOIN [Master].[CustomStationCode] CSC ON 
		Hd.CustomStationCode = CSC.Code
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo 



END




