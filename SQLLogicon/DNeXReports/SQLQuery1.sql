
-- ========================================================================================================================================
-- START											 [Reports].[usp_DNeXReceipt]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [InvoiceDetail] Record based on [InvoiceDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Reports].[usp_DNeXReceipt]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_DNeXReceipt] 
END 
GO
CREATE PROC [Reports].[usp_DNeXReceipt] 
    @BranchID BIGINT,
    @InvoiceNo VARCHAR(25)
As 
Begin


;with BillingUnitTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory='BillingUnitType'),
	ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	TruckCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TruckCategory'),
	DiscountTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='DiscountType')
	SELECT	Dt.[BranchID], Dt.[InvoiceNo], Dt.[Module], Dt.[OrderNo], Dt.[ContainerKey], Dt.[JobNo], Dt.[ChargeCode], Dt.[BillingUnit], 
			Dt.[ProductCode], Dt.[MovementCode], Dt.[Size], Dt.[Type], Dt.[ProductCategory], 
			Dt.[CargoCategory], Dt.[TruckCategory], Dt.[CurrencyCode], Dt.[ExRate], Dt.[Qty], Dt.[Price], Dt.[CostRate], 
			Dt.[ForeignAmount], Dt.[LocalAmount], Dt.[DiscountType], Dt.[DiscountAmount], Dt.[ActualAmount], Dt.[TaxAmount], 
			Dt.[TotalAmount], Dt.[IsSlabRate],Dt.[SlabFrom], Dt.[SlabTo],  Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(prdCat.LookupDescription,'') As ProductCategoryDescription,
			ISNULL(Trk.LookupDescription,'') As TruckCategoryDescription,
			ISNULL(Dis.LookupDescription,'') As DiscountTypeDescription,
			Chg.ChargeDescription As ChargeCodeDescription,
			ISNULL(Chg.OutPutGSTCode,'') As OutPutGSTCode,ISNULL(GstOutput.Description,'') As OutPutGSTDescription,
			ISNULL(Chg.InputGSTCode,'') As InputGSTCode,ISNULL(GstInput.Description,'') As InPutGSTDescription
	FROM [Billing].InvoiceHeader Hd

	Left Outer Join [Billing].[InvoiceDetail] Dt ON
		Hd.BranchID = Dt.BranchID
		And Hd.InvoiceNo = Dt.InvoiceNo
	Left Outer Join Master.ChargeMaster Chg On 
		Dt.ChargeCode = Chg.ChargeCode
	Left Outer Join BillingUnitTypeLookup Bu On
		Dt.BillingUnit = Bu.LookupID
	Left Outer Join ProductCategoryLookup prdCat On 
		Dt.ProductCategory = prdCat.LookupID
	Left Outer Join TruckCategoryLookup Trk ON 
		Dt.TruckCategory = Trk.LookupID
	Left Outer Join DiscountTypeLookup Dis ON
		Dt.DiscountType = Dis.LookupID
	Left Outer Join Master.GSTRate GstOutput ON
		Chg.OutPutGSTCode = GstOutput.GSTCode
	Left Outer Join Master.GSTRate GstInput ON
		Chg.InputGSTCode = GstOutput.GSTCode
	Left Outer Join Master.Merchant Cus On 
		Hd.CustomerCode = Cus.MerchantCode
	Left Outer Join Master.Address Ad ON 
		Cus.MerchantCode = Ad.LinkID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[InvoiceNo] = @InvoiceNo  

END