USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationContainerK2] Record based on [DeclarationContainerK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], 
			[EQDStatus], [MarksNos], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationContainerK2]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  
			AND [OrderNo] = @OrderNo  
			AND [ContainerKey] = @ContainerKey 
			AND [IsActive] = Cast(1 as bit) 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationContainerK2] Records from [DeclarationContainerK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], 
			[EQDStatus], [MarksNos], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationContainerK2]
	WHERE	[BranchID] = @BranchID  
		AND [DeclarationNo] = @DeclarationNo  
		AND [IsActive] = Cast(1 as bit)


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerK2List] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationContainerK2] Record Into [DeclarationContainerK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationContainerK2] (
			[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], [EQDStatus], 
			[MarksNos], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, 
			@MarksNos, CAST(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationContainerK2] Record Into [DeclarationContainerK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationContainerK2]
	SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, 
			[ContainerNo] = @ContainerNo, [IsSOC] = @IsSOC, [Size] = @Size, [Type] = @Type, [ContainerStatus] = @ContainerStatus, 
			[EQDStatus] = @EQDStatus, [MarksNos] = @MarksNos, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerK2Update]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationContainerK2] Record Into [DeclarationContainerK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationContainerK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationContainerK2Update] 
			@BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, 
			@MarksNos, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationContainerK2Insert] 
		@BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, 
		@MarksNos, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationContainerK2Save]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationContainerK2] Record  based on [DeclarationContainerK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Operation].[DeclarationContainerK2]
	SET	[IsActive] = CAST(0 as bit),ModifiedOn=GETUTCDATE(),ModifiedBy = @ModifiedBy
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       --AND [ContainerKey] = @ContainerKey

	


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerK2Delete]
-- ========================================================================================================================================

GO
 