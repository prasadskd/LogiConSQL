/****** Object:  Table [Operation].[DeclarationClauseK2]    Script Date: 22-01-2017 15:19:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationClauseK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[ClauseCode] [smallint] NOT NULL,
	[ClauseText] [nvarchar](2000) NOT NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationClauseK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[ItemNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[DeclarationClauseK2] ADD  CONSTRAINT [DF_DeclarationClauseK2_ClauseCode]  DEFAULT ((0)) FOR [ClauseCode]
GO

ALTER TABLE [Operation].[DeclarationClauseK2] ADD  CONSTRAINT [DF_DeclarationClauseK2_ClauseText]  DEFAULT ('') FOR [ClauseText]
GO

ALTER TABLE [Operation].[DeclarationClauseK2] ADD  CONSTRAINT [DF_DeclarationClauseK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationClauseK2] ADD  CONSTRAINT [DF_DeclarationClauseK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


