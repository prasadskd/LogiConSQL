
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCountByCustomer]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCountByCustomer]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCountByCustomer]
END 
GO
CREATE PROC [Operation].[usp_OrderCountByCustomer] 
	@BranchID bigint 
As
BEGIN
	Select ISNULL(MerchantName,'') As Customer,Count(0)  as y
	From Operation.OrderHeader OrdHd
	Left Outer Join Master.Merchant Cust ON 
		OrdHd.CustomerCode = Cust.MerchantCode
	Where OrdHd.BranchID = @BranchID
	Group By Cust.MerchantName
END

go
