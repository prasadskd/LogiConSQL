
/****** Object:  Table [Operation].[DeclarationSubItemK2]    Script Date: 22-01-2017 15:49:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationSubItemK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[SubItemNo] [smallint] NOT NULL,
	[ItemType] [nvarchar](20) NOT NULL,
	[ItemCode] [nvarchar](20) NOT NULL,
	[PType] [nvarchar](20) NOT NULL,
	[PMeterFigure] [decimal](18, 7) NOT NULL,
	[PTankFigure] [decimal](18, 7) NOT NULL,
	
	[POilUnitPrice] [decimal](18, 7) NOT NULL,
	[POilTotalValue] [decimal](18, 7) NOT NULL,
	[POilRate] [decimal](18, 7) NOT NULL,
	[POilAmount] [decimal](18, 7) NOT NULL,
	
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationSubItemK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[ItemNo] ASC,
	[SubItemNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_ItemType]  DEFAULT ((0)) FOR [ItemType]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_ItemCode]  DEFAULT ('') FOR [ItemCode]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_PType]  DEFAULT ('') FOR [PType]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_PMeterFigure]  DEFAULT ((0)) FOR [PMeterFigure]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_PTankFigure]  DEFAULT ((0)) FOR [PTankFigure]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationSubItemK2] ADD  CONSTRAINT [DF_DeclarationSubItemK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


