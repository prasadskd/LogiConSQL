
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationSubItem] Record based on [DeclarationSubItem] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationSubItemSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT,
    @SubItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationSubItem]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  
	       AND [SubItemNo] = @SubItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationSubItem] Records from [DeclarationSubItem] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationSubItemList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT

AS 
BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationSubItem]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationSubItem] Record Into [DeclarationSubItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationSubItem] (
			[BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationSubItem] Record Into [DeclarationSubItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationSubItem]
	SET    [ItemType] = @ItemType, [ItemCode] = @ItemCode, [PType] = @PType, [PMeterFigure] = @PMeterFigure, [PTankFigure] = @PTankFigure, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [SubItemNo] = @SubItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationSubItem] Record Into [DeclarationSubItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationSubItem] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [SubItemNo] = @SubItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationSubItemUpdate] 
		@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationSubItemInsert] 
		@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationSubItemSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationSubItem] Record  based on [DeclarationSubItem]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationSubItem]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemDelete]
-- ========================================================================================================================================

GO
 