
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationContainer] Record based on [DeclarationContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationContainerSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], [EQDStatus], 
			[MarksNos], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationContainer]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [OrderNo] = @OrderNo  
	       AND [ContainerKey] = @ContainerKey  		   
		   AND [IsActive] = Cast(1 as bit)
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationContainer] Records from [DeclarationContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationContainerList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], [EQDStatus], 
			[MarksNos], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationContainer]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  	   
		   AND [IsActive] = Cast(1 as bit)

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationContainer] Record Into [DeclarationContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationContainer] (
			[BranchID], [DeclarationNo], [OrderNo], [ContainerKey], [ContainerNo], [IsSOC], [Size], [Type], [ContainerStatus], [EQDStatus], [MarksNos], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, @MarksNos, Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationContainer] Record Into [DeclarationContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationContainer]
	SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, 
			[ContainerNo] = @ContainerNo, [IsSOC] = @IsSOC, [Size] = @Size, [Type] = @Type, [ContainerStatus] = @ContainerStatus, 
			[EQDStatus] = @EQDStatus, [MarksNos] = @MarksNos,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationContainer] Record Into [DeclarationContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @IsSOC bit,
    @Size varchar(2),
    @Type varchar(2),
    @ContainerStatus smallint,
    @EQDStatus smallint,
    @MarksNos nvarchar(MAX),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationContainer] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationContainerUpdate] 
		@BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, @MarksNos,@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationContainerInsert] 
		@BranchID, @DeclarationNo, @OrderNo, @ContainerKey, @ContainerNo, @IsSOC, @Size, @Type, @ContainerStatus, @EQDStatus, @MarksNos,@CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationContainerSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationContainerDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationContainer] Record  based on [DeclarationContainer]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationContainerDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationContainerDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationContainerDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Operation].[DeclarationContainer]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy=@ModifiedBy,ModifiedOn=GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       --AND [ContainerKey] = @ContainerKey

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Operation].[DeclarationContainer]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	*/


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationContainerDelete]
-- ========================================================================================================================================

GO

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

