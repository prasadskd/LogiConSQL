
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationInvoice] Record based on [DeclarationInvoice] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @InvoiceNo NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], 
			[InsuranceAmountPercent], [IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], 
			[InsuranceAmountValue], [InsuranceIncoTerm], [OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], 
			[OthersAmountValue], [CargoClassCode], [CargoDescription1], [CargoDescription2], [PackageQty], [PackingTypeCode], 
			[PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], [Status], 
			FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,InvoiceLocalAmount
	FROM	[Operation].[DeclarationInvoice]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  
			AND [InvoiceNo] = @InvoiceNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationInvoice] Records from [DeclarationInvoice] table

-- Exec [Operation].[usp_DeclarationInvoiceList] 
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], 
			[InsuranceAmountPercent], [IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], 
			[InsuranceAmountValue], [InsuranceIncoTerm], [OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], 
			[OthersAmountValue], [CargoClassCode], [CargoDescription1], [CargoDescription2], [PackageQty], [PackingTypeCode], 
			[PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], [Status], 
			FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],InvoiceLocalAmount 
	FROM   [Operation].[DeclarationInvoice]
		WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationInvoice] Record Into [DeclarationInvoice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationInvoice] (
			[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], [InsuranceAmountPercent], 
			[IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], [InsuranceAmountValue], [InsuranceIncoTerm], 
			[OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], [OthersAmountValue], [CargoClassCode], [CargoDescription1], 
			[CargoDescription2], [PackageQty], [PackingTypeCode], [PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], 
			[Status], 
			FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,InvoiceLocalAmount,
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, 
			@IncoTerm, @PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, 
			@IsFreightCurrency, @FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, 
			@IsInsuranceCurrency, @InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, 
			@OthersAmountPercent, @OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, 
			@CargoDescription2, @PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			Cast(1 as bit), 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationInvoice] Record Into [DeclarationInvoice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationInvoice]
	SET		[InvoiceDate] = @InvoiceDate, [InvoiceValue] = @InvoiceValue, [InvoiceCurrencyCode] = @InvoiceCurrencyCode, 
			[LocalCurrencyCode] = @LocalCurrencyCode, [CurrencyExRate] = @CurrencyExRate, [IncoTerm] = @IncoTerm, [PayCountry] = @PayCountry, 
			[PortAmountPercent] = @PortAmountPercent, [PortAmountCurrencyCode] = @PortAmountCurrencyCode, [PortAmountExRate] = @PortAmountExRate, 
			[PortAmountValue] = @PortAmountValue, [FreightAmountPercent] = @FreightAmountPercent, [IsFreightCurrency] = @IsFreightCurrency, 
			[FreightAmountCurrencyCode] = @FreightAmountCurrencyCode, [FreightAmountExRate] = @FreightAmountExRate, [FreightAmountValue] = @FreightAmountValue, 
			[FreightIncoTerm] = @FreightIncoTerm, [InsuranceAmountPercent] = @InsuranceAmountPercent, [IsInsuranceCurrency] = @IsInsuranceCurrency, 
			[InsuranceAmountCurrencyCode] = @InsuranceAmountCurrencyCode, [InsuranceAmountExRate] = @InsuranceAmountExRate, 
			[InsuranceAmountValue] = @InsuranceAmountValue, [InsuranceIncoTerm] = @InsuranceIncoTerm, 
			[OthersAmountPercent] = @OthersAmountPercent, [OthersAmountCurrencyCode] = @OthersAmountCurrencyCode, 
			[OthersAmountExRate] = @OthersAmountExRate, [OthersAmountValue] = @OthersAmountValue, [CargoClassCode] = @CargoClassCode, 
			[CargoDescription1] = @CargoDescription1, [CargoDescription2] = @CargoDescription2, [PackageQty] = @PackageQty, 
			[PackingTypeCode] = @PackingTypeCode, [PackingMaterialCode] = @PackingMaterialCode, [GrossWeight] = @GrossWeight, 
			[UOMWeight] = @UOMWeight, [GrossVolume] = @GrossVolume, [UOMVolume] = @UOMVolume, 
			FOBAmount=@FOBAmount,CIFAmount=@CIFAmount,EXWAmount=@EXWAmount,
			CNFAmount=@CNFAmount,CNIAmount=@CNIAmount,FreightAmount=@FreightAmount,InsuranceAmount=@InsuranceAmount,
			CIFCAmount=@CIFCAmount,InvoiceLocalAmount=@InvoiceLocalAmount,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = Getutcdate()
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
			AND [InvoiceNo] = @InvoiceNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationInvoice] Record Into [DeclarationInvoice] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationInvoice] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [InvoiceNo] = @InvoiceNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationInvoiceUpdate] 
			@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, @IncoTerm, 
			@PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, @IsFreightCurrency, 
			@FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, @IsInsuranceCurrency, 
			@InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, @OthersAmountPercent, 
			@OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, @CargoDescription2, 
			@PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,
			@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationInvoiceInsert] 
			@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, @IncoTerm, 
			@PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, @IsFreightCurrency, 
			@FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, @IsInsuranceCurrency, 
			@InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, @OthersAmountPercent, 
			@OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, @CargoDescription2, 
			@PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,
			@CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationInvoiceSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationInvoice] Record  based on [DeclarationInvoice]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	DELETE FROM	[Operation].[DeclarationInvoice]
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       --AND [InvoiceNo] = @InvoiceNo

	--UPDATE	[Operation].[DeclarationInvoice]
	--SET	[Status] = CAST(0 as bit),ModifiedOn = GETUTCDATE(), ModifiedBy = @ModifiedBy
	--WHERE 	[BranchID] = @BranchID
	--       AND [DeclarationNo] = @DeclarationNo
	--       AND [InvoiceNo] = @InvoiceNo

	 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceDelete]
-- ========================================================================================================================================


GO
 