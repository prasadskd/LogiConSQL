
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationExcemption] Record based on [DeclarationExcemption] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionSelect] 
END 
GO

CREATE PROC [Operation].[usp_DeclarationExcemptionSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	SELECT	DE.[BranchID], DE.[DeclarationNo], DE.[IsGSTSpecialScheme], DE.[SpecialTreatmentType], DE.[SpecialRef1], DE.[SpecialRef2], DE.[SpecialRef3], 
			DE.[IsGSTSpecialExcemption], DE.[SpecialExcemptionType], DE.[SpecialExcemptionReason], DE.[IsGSTTreasuryExcemption], DE.[TreasuryExcemptionType], 
			DE.[TreasuryExcemptionReferenceNo], DE.[IsGSTClaim],
			Cc.[ClaimantID], Cc.[Name] As ClaimantName, Cc.[Company] As ClaimantCompany, Cc.[NRIC] As ClaimantNRIC, Cc.[Designation] As ClaimantDesignation, Cc.[Status] ClaimantStatus, 
			DE.[SalesTaxExcemptionType], DE.[SalesTaxReferenceNo], DE.[SalesTaxRegistrationDate], DE.[CreatedBy], DE.[CreatedOn], DE.[ModifiedBy], DE.[ModifiedOn] 
	FROM   [Operation].[DeclarationExcemption] DE	
	Left Outer Join Master.CustomClaimant Cc ON 
		DE.BranchID = Cc.BranchID 
		And DE.ClaimantID = Cc.ClaimantID
	WHERE  DE.[BranchID] = @BranchID  
	    AND DE.[DeclarationNo] = @DeclarationNo   
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionSelect]
-- ========================================================================================================================================





GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationExcemption] Records from [DeclarationExcemption] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	DE.[BranchID], DE.[DeclarationNo], DE.[IsGSTSpecialScheme], DE.[SpecialTreatmentType], DE.[SpecialRef1], DE.[SpecialRef2], DE.[SpecialRef3], 
			DE.[IsGSTSpecialExcemption], DE.[SpecialExcemptionType], DE.[SpecialExcemptionReason], DE.[IsGSTTreasuryExcemption], DE.[TreasuryExcemptionType], 
			DE.[TreasuryExcemptionReferenceNo], DE.[IsGSTClaim], 
			Cc.[ClaimantID], Cc.[Name] As ClaimantName, Cc.[Company] As ClaimantCompany, Cc.[NRIC] As ClaimantNRIC, Cc.[Designation] As ClaimantDesignation, Cc.[Status] ClaimantStatus, 
			DE.[SalesTaxExcemptionType], DE.[SalesTaxReferenceNo], DE.[SalesTaxRegistrationDate], DE.[CreatedBy], DE.[CreatedOn], DE.[ModifiedBy], DE.[ModifiedOn] 
	FROM   [Operation].[DeclarationExcemption] DE	
	Left Outer Join Master.CustomClaimant Cc ON 
		DE.BranchID = Cc.BranchID 
		And DE.ClaimantID = Cc.ClaimantID
	WHERE  DE.[BranchID] = @BranchID  
	    AND DE.[DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionList] 
-- ========================================================================================================================================


GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationExcemption] Record Into [DeclarationExcemption] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @IsGSTSpecialScheme bit,
    @SpecialTreatmentType smallint,
    @SpecialRef1 nvarchar(50),
    @SpecialRef2 nvarchar(50),
    @SpecialRef3 nvarchar(50),
    @IsGSTSpecialExcemption bit,
    @SpecialExcemptionType smallint,
    @SpecialExcemptionReason nvarchar(50),
    @IsGSTTreasuryExcemption bit,
    @TreasuryExcemptionType smallint,
    @TreasuryExcemptionReferenceNo nvarchar(50),
    @IsGSTClaim bit,
	@ClaimantID  nvarchar(35),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @ClaimantStatus nvarchar(20),
	@SalesTaxExcemptionType smallint,
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistrationDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationExcemption] (
			[BranchID], [DeclarationNo], [IsGSTSpecialScheme], [SpecialTreatmentType], [SpecialRef1], [SpecialRef2], [SpecialRef3], 
			[IsGSTSpecialExcemption], [SpecialExcemptionType], [SpecialExcemptionReason], [IsGSTTreasuryExcemption], 
			[TreasuryExcemptionType], [TreasuryExcemptionReferenceNo], [IsGSTClaim],[ClaimantID], [ClaimantName], [ClaimantCompany], [ClaimantNRIC], 
			[ClaimantDesignation], [ClaimantStatus],[SalesTaxExcemptionType], [SalesTaxReferenceNo], [SalesTaxRegistrationDate], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @IsGSTSpecialScheme, @SpecialTreatmentType, @SpecialRef1, @SpecialRef2, @SpecialRef3, 
			@IsGSTSpecialExcemption, @SpecialExcemptionType, @SpecialExcemptionReason, @IsGSTTreasuryExcemption, 
			@TreasuryExcemptionType, @TreasuryExcemptionReferenceNo, @IsGSTClaim, @ClaimantID, @ClaimantName, @ClaimantCompany, @ClaimantNRIC, 
			@ClaimantDesignation, @ClaimantStatus,@SalesTaxExcemptionType,@SalesTaxReferenceNo, @SalesTaxRegistrationDate, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionInsert]
-- ========================================================================================================================================


GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationExcemption] Record Into [DeclarationExcemption] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @IsGSTSpecialScheme bit,
    @SpecialTreatmentType smallint,
    @SpecialRef1 nvarchar(50),
    @SpecialRef2 nvarchar(50),
    @SpecialRef3 nvarchar(50),
    @IsGSTSpecialExcemption bit,
    @SpecialExcemptionType smallint,
    @SpecialExcemptionReason nvarchar(50),
    @IsGSTTreasuryExcemption bit,
    @TreasuryExcemptionType smallint,
    @TreasuryExcemptionReferenceNo nvarchar(50),
    @IsGSTClaim bit,
	@ClaimantID  nvarchar(35),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @ClaimantStatus nvarchar(20),
	@SalesTaxExcemptionType smallint,
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistrationDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationExcemption]
	SET    [IsGSTSpecialScheme] = @IsGSTSpecialScheme, [SpecialTreatmentType] = @SpecialTreatmentType, [SpecialRef1] = @SpecialRef1, 
			[SpecialRef2] = @SpecialRef2, [SpecialRef3] = @SpecialRef3, [IsGSTSpecialExcemption] = @IsGSTSpecialExcemption, 
			[SpecialExcemptionType] = @SpecialExcemptionType, [SpecialExcemptionReason] = @SpecialExcemptionReason, 
			[IsGSTTreasuryExcemption] = @IsGSTTreasuryExcemption, [TreasuryExcemptionType] = @TreasuryExcemptionType, 
			[TreasuryExcemptionReferenceNo] = @TreasuryExcemptionReferenceNo, [IsGSTClaim] = @IsGSTClaim, 
			[ClaimantID] = @ClaimantID, [ClaimantName] = @ClaimantName, [ClaimantCompany] = @ClaimantCompany, 
			[ClaimantNRIC] = @ClaimantNRIC, [ClaimantDesignation] = @ClaimantDesignation, [ClaimantStatus] = @ClaimantStatus, 
			[SalesTaxExcemptionType]=@SalesTaxExcemptionType,[SalesTaxReferenceNo] = @SalesTaxReferenceNo, [SalesTaxRegistrationDate] = @SalesTaxRegistrationDate, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationExcemption] Record Into [DeclarationExcemption] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @IsGSTSpecialScheme bit,
    @SpecialTreatmentType smallint,
    @SpecialRef1 nvarchar(50),
    @SpecialRef2 nvarchar(50),
    @SpecialRef3 nvarchar(50),
    @IsGSTSpecialExcemption bit,
    @SpecialExcemptionType smallint,
    @SpecialExcemptionReason nvarchar(50),
    @IsGSTTreasuryExcemption bit,
    @TreasuryExcemptionType smallint,
    @TreasuryExcemptionReferenceNo nvarchar(50),
    @IsGSTClaim bit,
	@ClaimantID  nvarchar(35),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @ClaimantStatus nvarchar(20),
	@SalesTaxExcemptionType smallint,
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistrationDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationExcemption] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationExcemptionUpdate] 
		@BranchID, @DeclarationNo, @IsGSTSpecialScheme, @SpecialTreatmentType, @SpecialRef1, @SpecialRef2, @SpecialRef3, 
		@IsGSTSpecialExcemption, @SpecialExcemptionType, @SpecialExcemptionReason, @IsGSTTreasuryExcemption, @TreasuryExcemptionType, 
		@TreasuryExcemptionReferenceNo, @IsGSTClaim, @ClaimantID, @ClaimantName, @ClaimantCompany, @ClaimantNRIC, @ClaimantDesignation, 
		@ClaimantStatus,@SalesTaxExcemptionType,@SalesTaxReferenceNo, @SalesTaxRegistrationDate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationExcemptionInsert] 
		@BranchID, @DeclarationNo, @IsGSTSpecialScheme, @SpecialTreatmentType, @SpecialRef1, @SpecialRef2, @SpecialRef3, 
		@IsGSTSpecialExcemption, @SpecialExcemptionType, @SpecialExcemptionReason, @IsGSTTreasuryExcemption, @TreasuryExcemptionType, 
		@TreasuryExcemptionReferenceNo, @IsGSTClaim, @ClaimantID, @ClaimantName, @ClaimantCompany, @ClaimantNRIC, @ClaimantDesignation, 
		@ClaimantStatus,@SalesTaxExcemptionType,@SalesTaxReferenceNo, @SalesTaxRegistrationDate, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationExcemptionSave]
-- ========================================================================================================================================



GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationExcemption] Record  based on [DeclarationExcemption]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
AS 

	
BEGIN

	 

	 
	DELETE FROM   [Operation].[DeclarationExcemption]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionDelete]
-- ========================================================================================================================================

GO
 