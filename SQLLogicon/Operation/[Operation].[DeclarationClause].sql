
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationClause] Record based on [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationClauseSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationClause]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationClause] Records from [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationClauseList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationClause]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationClause] (
			[BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationClause]
	SET    [ClauseCode] = @ClauseCode, [ClauseText] = @ClauseText, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationClause] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationClauseUpdate] 
		@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationClauseInsert] 
		@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationClauseSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationClause] Record  based on [DeclarationClause]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50) 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationClause]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseDelete]
-- ========================================================================================================================================

GO
 