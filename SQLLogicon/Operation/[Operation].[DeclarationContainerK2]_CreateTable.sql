
/****** Object:  Table [Operation].[DeclarationContainerK2]    Script Date: 22-01-2017 15:21:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Operation].[DeclarationContainerK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[OrderNo] [varchar](50) NOT NULL,
	[ContainerKey] [varchar](50) NOT NULL,
	[ContainerNo] [varchar](15) NOT NULL,
	[IsSOC] [bit] NOT NULL,
	[Size] [varchar](2) NOT NULL,
	[Type] [varchar](2) NOT NULL,
	[ContainerStatus] [smallint] NOT NULL,
	[EQDStatus] [smallint] NOT NULL,
	[MarksNos] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationContainerK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_ContainerNo]  DEFAULT ('') FOR [ContainerNo]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_IsSOC]  DEFAULT ((0)) FOR [IsSOC]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_Size]  DEFAULT ('') FOR [Size]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_Type]  DEFAULT ('') FOR [Type]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_ContainerStatus]  DEFAULT ((0)) FOR [ContainerStatus]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_EQDStatus]  DEFAULT ((0)) FOR [EQDStatus]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationContainerK2] ADD  CONSTRAINT [DF_DeclarationContainerK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


