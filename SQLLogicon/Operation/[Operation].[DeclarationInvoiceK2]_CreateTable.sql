

/****** Object:  Table [Operation].[DeclarationInvoiceK2]    Script Date: 30-01-2017 18:30:50 ******/
DROP TABLE [Operation].[DeclarationInvoiceK2]
GO

/****** Object:  Table [Operation].[DeclarationInvoiceK2]    Script Date: 30-01-2017 18:30:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationInvoiceK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[InvoiceNo] [nvarchar](50) NOT NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceValue] [decimal](18, 5) NULL,
	[InvoiceCurrencyCode] [nvarchar](3) NULL,
	[LocalCurrencyCode] [nvarchar](3) NULL,
	[ExchangeRate] [decimal](18, 4) NULL,
	[LocalCurrencyValue] [decimal](18, 4) NULL,
	[AmountReceived] [decimal](18, 4) NULL,
	[AmountReceivedCurrencyCode] [nvarchar](3) NULL,
	[AmountReceivedLocalCurrencyCode] [nvarchar](3) NULL,
	[AmountReceivedExchangeRate] [decimal](18, 4) NULL,
	[AmountReceivedLocalValue] [decimal](18, 4) NULL,
	[IncoTerms] [smallint] NULL,
	[FreightAmount] [decimal](18, 4) NULL,
	[FreightCurrencyCode] [nvarchar](3) NULL,
	[FreightExchangeRate] [decimal](18, 4) NULL,
	[IsFreightGrossWeight] [bit] NULL,
	[InsuranceAmount] [decimal](18, 4) NULL,
	[InsuranceCurrencyCode] [nvarchar](3) NULL,
	[InsuranceExchangeRate] [decimal](18, 4) NULL,
	[InsuranceIncoTerm] [smallint] NULL,
	[OtherAmount] [decimal](18, 4) NULL,
	[OtherCurrencyCode] [nvarchar](3) NULL,
	[OtherExchangeRate] [decimal](18, 4) NULL,
	[PortAmount] [decimal](18, 4) NULL,
	[PortCurrencyCode] [nvarchar](3) NULL,
	[PortExchangeRate] [decimal](18, 4) NULL,
	[IsRequireSTA] [bit] NULL,
	[Status] [bit] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationInvoiceK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[InvoiceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


