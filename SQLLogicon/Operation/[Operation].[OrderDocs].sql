
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderDocs] Record based on [OrderDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderDocsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsSelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsSelect] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(70),
    @ItemNo smallint
AS 

BEGIN

	SELECT	[BranchID], [OrderNo], [ItemNo], [FileName], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[OrderDocs]
	WHERE  [BranchID] = @BranchID   
	       AND [OrderNo] = @OrderNo  
		   AND [ItemNo]= @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderDocs] Records from [OrderDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderDocsList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsList] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsList] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(70)

AS 
BEGIN

	SELECT	[BranchID], [OrderNo], [ItemNo], [FileName], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[OrderDocs]
	WHERE  [BranchID] = @BranchID   
	       AND [OrderNo] = @OrderNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsMaxItemNo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderDocs] Record based on [OrderDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderDocsMaxItemNo]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsMaxItemNo] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsMaxItemNo] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(70)
AS 

BEGIN

	SELECT	MAX([ItemNo])
	FROM	[Operation].[OrderDocs]
	WHERE  [BranchID] = @BranchID   
	       AND [OrderNo] = @OrderNo
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsMaxItemNo]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderDocs] Record Into [OrderDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderDocsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsInsert] 
    @BranchID bigint,
    @OrderNo varchar(70),
    @ItemNo smallint,
    @FileName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[OrderDocs] (
			[BranchID], [OrderNo], [ItemNo], [FileName], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ItemNo, @FileName, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderDocs] Record Into [OrderDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderDocsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsUpdate] 
    @BranchID bigint,
    @OrderNo varchar(70),
    @ItemNo smallint,
    @FileName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[OrderDocs]
	SET		[ItemNo] = @ItemNo, [FileName] = @FileName,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderDocs] Record Into [OrderDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderDocsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsSave] 
    @BranchID bigint,
    @OrderNo varchar(70),
    @ItemNo smallint,
    @FileName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[OrderDocs] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Operation].[usp_OrderDocsUpdate] 
		@BranchID, @OrderNo, @ItemNo, @FileName,@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderDocsInsert] 
		@BranchID, @OrderNo, @ItemNo, @FileName,@CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderDocsSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderDocsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderDocs] Record  based on [OrderDocs]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderDocsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderDocsDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderDocsDelete] 
    @BranchID bigint,
    @OrderNo varchar(70),
    @ItemNo varchar(255)
AS 

	
BEGIN

	--UPDATE	[Operation].[OrderDocs]
	--SET	[IsActive] = CAST(0 as bit),ModifiedBy=@ModifiedBy,ModifiedOn=GETUTCDATE()
	--WHERE 	[BranchID] = @BranchID
	--       AND [DeclarationNo] = @DeclarationNo
	--       AND [OrderNo] = @OrderNo
	       --AND [ContainerKey] = @ContainerKey

	
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Operation].[OrderDocs]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ItemNo] = @ItemNo
	


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderDocsDelete]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Root].[usp_OrderDocsDeleteALL]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderDocs] Record  based on [OrderDocs]

-- ========================================================================================================================================

IF OBJECT_ID('[Root].[usp_OrderDocsDeleteALL]') IS NOT NULL
BEGIN 
    DROP PROC [Root].[usp_OrderDocsDeleteALL] 
END 
GO
CREATE PROC [Root].[usp_OrderDocsDeleteALL] 
    @BranchID bigint,
    @OrderNo varchar(70)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[OrderDocs]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	        


END

-- ========================================================================================================================================
-- END  											 [Root].[usp_OrderDocsDeleteALL]
-- ========================================================================================================================================

GO
 

