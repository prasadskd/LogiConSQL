﻿IF OBJECT_ID('[Operation].[usp_OrderCountByDateForDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCountByDateForDashboard] 
END 
GO
CREATE PROC [Operation].[usp_OrderCountByDateForDashboard] 
    @BranchID bigint	 
As
BEGIN
	 
	Select Count(0)  as TotalOrders
	From Operation.OrderHeader OrdHd 
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),GetUtcDate(),120)
	 
END

go

IF OBJECT_ID('[Operation].[usp_PendingOrdersForPortBookingDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_PendingOrdersForPortBookingDashboard] 
END 
GO
CREATE PROC [Operation].[usp_PendingOrdersForPortBookingDashboard] 
    @BranchID bigint	 
As
BEGIN
	 
	Select Count(OrdHd.OrderNo)  as TotalOrders
	From Operation.OrderHeader OrdHd 
	Left Outer Join Port.BookingHeader BkHd ON 
		Ordhd.BranchID = BkHd.RelationBranchID
		And OrdHd.HouseBLNo = BkHd.BLNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),GETUTCDATE(),120)
	And BkHd.OrderNo IS NULL 
	 
END

go

IF OBJECT_ID('[Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard] 
END 
GO
CREATE PROC [Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard]
    @BranchID bigint	 
As
BEGIN
	 

	Declare @totalOrders smallint=0,
			@totalTime smallint=0,
			@totalTAT smallint=0;


	Select @totalOrders = ISNULL(Count(OrdHd.OrderNo),0)
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeader K1 ON
		OrdHd.BranchID = K1.BranchID
		And OrdHd.OrderNo = K1.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalOrders = @totalOrders +  ISNULL(Count(OrdHd.OrderNo),0)   
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeaderK2 K2 ON
		OrdHd.BranchID = K2.BranchID
		And OrdHd.OrderNo = K2.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	 
	Select @totalTime = ISNULL(SUM(DateDiff("M",Ordhd.OrderDate,K1.DeclarationDate)),0)
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeader K1 ON
		OrdHd.BranchID = K1.BranchID
		And OrdHd.OrderNo = K1.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalTime = @totalTime + ISNULL(SUM(DateDiff("M",Ordhd.OrderDate,K2.DeclarationDate)),0)   
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeaderK2 K2 ON
		OrdHd.BranchID = K2.BranchID
		And OrdHd.OrderNo = K2.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalTAT = (@totalTime / @totalOrders) 



	Select @totalTAT;


END

