
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationShipment] Record based on [DeclarationShipment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationShipmentSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ManifestNo NVARCHAR(50)
AS 

BEGIN

	SELECT	Sh.[BranchID], Sh.[DeclarationNo], Sh.[ManifestNo], Sh.[VesselScheduleID], Sh.[SCNNo], Sh.[VesselID], Sh.[VesselName], Sh.[VoyageNo], Sh.[LoadingPort], 
			Sh.[DischargePort], Sh.[TranshipmentPort],Sh.[PlaceOfImport], Sh.[PortOperator], Sh.[OceanBLNo], Sh.[HouseBLNo], Sh.[ETADate], Sh.[WarehouseNo], 
			Sh.[WagonNo], Sh.[VehicleNo1], 
			Sh.[VehicleNo2], Sh.[FlightNo], Sh.[ARNNo], Sh.[MasterAWBNo], Sh.[HouseAWBNo], Sh.[AIRCOLoadNo], Sh.[JKNo], Sh.[CreatedBy], 
			Sh.[CreatedOn], Sh.[ModifiedBy], Sh.[ModifiedOn],
			ISNULL(POI.PortName,'') As PlaceOfImportName,
			ISNULL(LP.PortName,'') As LoadingPortName,
			ISNULL(DP.PortName,'') As DischargePortName,
			ISNULL(TP.PortName,'') As TranshipmentPortName,
			ISNULL(Opr.OperatorName,'') As PortOperatorName
	FROM   [Operation].[DeclarationShipment] Sh
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfImport = POI.PortCode
	Left Outer Join Master.Port LP ON 
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON 
			Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON 
			Sh.TranshipmentPort = TP.PortCode
	Left Outer Join Master.Operator Opr ON 
		Sh.PortOperator = Opr.OperatorID
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ManifestNo] = @ManifestNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationShipment] Records from [DeclarationShipment] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationShipmentList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

		SELECT	Sh.[BranchID], Sh.[DeclarationNo], Sh.[ManifestNo], Sh.[VesselScheduleID], Sh.[SCNNo], Sh.[VesselID], Sh.[VesselName], Sh.[VoyageNo], Sh.[LoadingPort], 
			Sh.[DischargePort], Sh.[TranshipmentPort],Sh.[PlaceOfImport], Sh.[PortOperator], Sh.[OceanBLNo], Sh.[HouseBLNo], Sh.[ETADate], Sh.[WarehouseNo], 
			Sh.[WagonNo], Sh.[VehicleNo1], 
			Sh.[VehicleNo2], Sh.[FlightNo], Sh.[ARNNo], Sh.[MasterAWBNo], Sh.[HouseAWBNo], Sh.[AIRCOLoadNo], Sh.[JKNo], Sh.[CreatedBy], 
			Sh.[CreatedOn], Sh.[ModifiedBy], Sh.[ModifiedOn],
			ISNULL(POI.PortName,'') As PlaceOfImportName,
			ISNULL(LP.PortName,'') As LoadingPortName,
			ISNULL(DP.PortName,'') As DischargePortName,
			ISNULL(TP.PortName,'') As TranshipmentPortName,
			ISNULL(Opr.OperatorName,'') As PortOperatorName
	FROM   [Operation].[DeclarationShipment] Sh
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfImport = POI.PortCode
	Left Outer Join Master.Port LP ON 
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON 
			Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON 
			Sh.TranshipmentPort = TP.PortCode
	Left Outer Join Master.Operator Opr ON 
		Sh.PortOperator = Opr.OperatorID
	WHERE  [BranchID] = @BranchID  
	    AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationShipment] Record Into [DeclarationShipment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfImport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
		INSERT INTO [Operation].[DeclarationShipment] (
				[BranchID], [DeclarationNo], [ManifestNo], [VesselScheduleID], [SCNNo], [VesselID], [VesselName], [VoyageNo], 
				[LoadingPort], [DischargePort], [TranshipmentPort], PlaceOfImport,[PortOperator], [OceanBLNo], [HouseBLNo], [ETADate], 
				[WarehouseNo], [WagonNo], [VehicleNo1], [VehicleNo2], [FlightNo], [ARNNo], [MasterAWBNo], [HouseAWBNo], [AIRCOLoadNo], [JKNo], 
				[CreatedBy], [CreatedOn])
		SELECT	@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
				@DischargePort, @TranshipmentPort, @PlaceOfImport,@PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
				@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationShipment] Record Into [DeclarationShipment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfImport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationShipment]
		SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [ManifestNo] = @ManifestNo, [VesselScheduleID] = @VesselScheduleID, 
			[SCNNo] = @SCNNo, [VesselID] = @VesselID, [VesselName] = @VesselName, [VoyageNo] = @VoyageNo, 
			[LoadingPort] = @LoadingPort, [DischargePort] = @DischargePort, [TranshipmentPort] = @TranshipmentPort,PlaceOfImport=@PlaceOfImport,
			[PortOperator] = @PortOperator, [OceanBLNo] = @OceanBLNo, [HouseBLNo] = @HouseBLNo, [ETADate] = @ETADate, 
			[WarehouseNo] = @WarehouseNo, [WagonNo] = @WagonNo, [VehicleNo1] = @VehicleNo1, [VehicleNo2] = @VehicleNo2, 
			[FlightNo] = @FlightNo, [ARNNo] = @ARNNo, [MasterAWBNo] = @MasterAWBNo, [HouseAWBNo] = @HouseAWBNo, [AIRCOLoadNo] = @AIRCOLoadNo, 
			[JKNo] = @JKNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ManifestNo] = @ManifestNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationShipment] Record Into [DeclarationShipment] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfImport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationShipment] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ManifestNo] = @ManifestNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationShipmentUpdate] 
		@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
		@DischargePort, @TranshipmentPort,@PlaceOfImport, @PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
		@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo, 
		@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationShipmentInsert] 
		@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
		@DischargePort, @TranshipmentPort,@PlaceOfImport, @PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
		@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo, 
		@CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationShipmentSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationShipment] Record  based on [DeclarationShipment]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationShipment]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       --AND [ManifestNo] = @ManifestNo
 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentDelete]
-- ========================================================================================================================================

GO
 