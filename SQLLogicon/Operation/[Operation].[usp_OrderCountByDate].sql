


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCountByDate] 
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCountByDate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCountByDate]  
END 
GO
CREATE PROC [Operation].[usp_OrderCountByDate] 
	@BranchID bigint,
		@DateFrom datetime,
		@DateTo datetime
As
BEGIN
	;with JobTypeDescription As (Select LookupId,LookupDescription from Config.Lookup where LookupCategory='JobType')
	Select ISNULL(LookupDescription,'') As name,Count(0)  as y
	From Operation.OrderHeader OrdHd
	Left Outer Join JobTypeDescription JD On
		OrdHd.JobType = JD.LookupID
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) >= @DateFrom
	And Convert(varchar(10),OrdHd.OrderDate,120) <= @DateTo 
	And JD.LookupDescription IN ('IMPORT','EXPORT')
	Group By LookupDescription
END

go





