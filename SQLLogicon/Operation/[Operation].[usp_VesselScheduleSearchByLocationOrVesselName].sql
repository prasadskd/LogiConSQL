
-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleSearchByLocationOrVesselName]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleSearchByLocationOrVesselName]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSearchByLocationOrVesselName] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSearchByLocationOrVesselName] 
    @LoadingPort nvarchar(100) = NULL,
	@DestinationPort nvarchar(100) = NULL,
	@ETD datetime = NULL,
	@VesselName nvarchar(100)=NULL,
	@AgentCode nvarchar(10) = NULL

AS 

BEGIN

	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,	Vsl.VesselName ,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
	COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
	COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
	COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
	COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE VS.LoadingPort = ISNULL(@LoadingPort,VS.LoadingPort)
		And VS.DestinationPort = ISNULL(@DestinationPort , VS.DestinationPort)
		And Convert(char(10),VS.ETD,120) >= Convert(char(10),ISNULL(@ETD,VS.ETD),120)
		--And Vsl.VesselName = ISNULL(@VesselName,Vsl.VesselName)
	
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleSearchByLocationOrVesselName]
-- ========================================================================================================================================

GO
