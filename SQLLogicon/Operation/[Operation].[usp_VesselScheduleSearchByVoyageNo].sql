
-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleSearchByVoyageNo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


--IF OBJECT_ID('[Operation].[usp_VesselScheduleSearchByVoyageNo]') IS NOT NULL
--BEGIN 
--    DROP PROC [Operation].[usp_VesselScheduleSearchByVoyageNo] 
--END 
--GO
--CREATE PROC [Operation].[usp_VesselScheduleSearchByVoyageNo] 
-- @BranchID bigint,
--    @JobType smallint,
-- @VoyageNo nvarchar(20)
  
--AS 

--BEGIN
-- -- IMPORT
-- If @JobType = 1060 
-- Begin

--  ;with
--  JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
--  SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
--  VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
--  VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
--  VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
--  VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
--  VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
--  VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
--  VS.[ModifiedBy], VS.[ModifiedOn] ,Vsl.VesselName, VS.[BranchID],'' as CustomStationCode,'' as PlaceOfArrival,'' as EntryPoint,'' as PSACode,'' as PSANAME,'' as PSAAddress,
--  ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
--  COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
--  COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
--  COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
--  COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
--  COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
--	Vsl.CallSignNo As CallSignNo
--  FROM   [Operation].[VesselSchedule] VS
--  Left Outer Join master.Vessel Vsl On 
--   Vs.VesselID = Vsl.VesselID
--  Left Outer Join JobTypeLookup Jb ON 
--   VS.JobType = jb.LookupID
--  Left Outer Join Master.Merchant Agnt On
--   Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
--   And Agnt.IsLiner = CAST(1 as bit)
--  Left Outer Join Master.Port LP On 
--   VS.LoadingPort = LP.PortCode
--  Left Outer Join Master.Port DiP On 
--   VS.DischargePort = DiP.PortCode
--  Left Outer Join Master.Port DeP On 
--   VS.DestinationPort = DeP.PortCode
--  Left Outer Join Master.Merchant Ter ON
--   Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
--   And Ter.IsTerminal = CAST(1 as bit)
--  WHERE 
--  Vs.BranchID = @BranchID
--  And VS.[VoyageNoInWard] LIKE '%' + @VoyageNo + '%'
--  And Jb.LookupID = @JobType
-- End
-- Else
-- Begin
--  ;with
--  JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
--  SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
--  VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
--  VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
--  VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
--  VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
--  VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
--  VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
--  VS.[ModifiedBy], VS.[ModifiedOn] , Vsl.VesselName , VS.[BranchID],'' as CustomStationCode,'' as PlaceOfArrival,'' as EntryPoint,'' as PSACode,'' as PSANAME,'' as PSAAddress,
--  ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
--  COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
--  COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
--  COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
--  COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
--  COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
--  Vsl.CallSignNo As CallSignNo
--  FROM   [Operation].[VesselSchedule] VS
--  Left Outer Join master.Vessel Vsl On 
--   Vs.VesselID = Vsl.VesselID
--  Left Outer Join JobTypeLookup Jb ON 
--   VS.JobType = jb.LookupID
--  Left Outer Join Master.Merchant Agnt On
--   Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
--   And Agnt.IsLiner = CAST(1 as bit)
--  Left Outer Join Master.Port LP On 
--   VS.LoadingPort = LP.PortCode
--  Left Outer Join Master.Port DiP On 
--   VS.DischargePort = DiP.PortCode
--  Left Outer Join Master.Port DeP On 
--   VS.DestinationPort = DeP.PortCode
--  Left Outer Join Master.Merchant Ter ON
--   Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
--   And Ter.IsTerminal = CAST(1 as bit)
--  WHERE   Vs.BranchID = @BranchID
--  And VS.[VoyageNoOutWard] LIKE '%' + @VoyageNo + '%'
--  And Jb.LookupID = @JobType

-- End
  
   
 
--END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleSearchByVoyageNo]
-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_VesselScheduleSearchByVoyageNo]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSearchByVoyageNo] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSearchByVoyageNo] 
 @BranchID bigint,
    @JobType smallint,
 @VoyageNo nvarchar(20)
  
AS 

BEGIN
 /*
 -- IMPORT
 If @JobType = 1060 
 Begin

  ;with
  JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
  SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
  VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
  VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
  VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
  VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
  VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
  VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
  VS.[ModifiedBy], VS.[ModifiedOn] ,Vsl.VesselName, VS.[BranchID],'' as CustomStationCode,'' as PlaceOfArrival,'' as EntryPoint,'' as PSACode,'' as PSANAME,'' as PSAAddress,
  ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
  COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
  COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
  COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
  COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
  COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
	Vsl.CallSignNo As CallSignNo
  FROM   [Operation].[VesselSchedule] VS
  Left Outer Join master.Vessel Vsl On 
   Vs.VesselID = Vsl.VesselID
  Left Outer Join JobTypeLookup Jb ON 
   VS.JobType = jb.LookupID
  Left Outer Join Master.Merchant Agnt On
   Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
   And Agnt.IsLiner = CAST(1 as bit)
  Left Outer Join Master.Port LP On 
   VS.LoadingPort = LP.PortCode
  Left Outer Join Master.Port DiP On 
   VS.DischargePort = DiP.PortCode
  Left Outer Join Master.Port DeP On 
   VS.DestinationPort = DeP.PortCode
  Left Outer Join Master.Merchant Ter ON
   Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
   And Ter.IsTerminal = CAST(1 as bit)
  WHERE 
  Vs.BranchID = @BranchID
  And VS.[VoyageNoInWard] LIKE '%' + @VoyageNo + '%'
  And Jb.LookupID = @JobType
 End
 Else
 Begin
  ;with
  JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
  SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
  VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
  VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
  VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
  VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
  VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
  VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
  VS.[ModifiedBy], VS.[ModifiedOn] , Vsl.VesselName , VS.[BranchID],'' as CustomStationCode,'' as PlaceOfArrival,'' as EntryPoint,'' as PSACode,'' as PSANAME,'' as PSAAddress,
  ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
  COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
  COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
  COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
  COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
  COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
  Vsl.CallSignNo As CallSignNo
  FROM   [Operation].[VesselSchedule] VS
  Left Outer Join master.Vessel Vsl On 
   Vs.VesselID = Vsl.VesselID
  Left Outer Join JobTypeLookup Jb ON 
   VS.JobType = jb.LookupID
  Left Outer Join Master.Merchant Agnt On
   Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
   And Agnt.IsLiner = CAST(1 as bit)
  Left Outer Join Master.Port LP On 
   VS.LoadingPort = LP.PortCode
  Left Outer Join Master.Port DiP On 
   VS.DischargePort = DiP.PortCode
  Left Outer Join Master.Port DeP On 
   VS.DestinationPort = DeP.PortCode
  Left Outer Join Master.Merchant Ter ON
   Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
   And Ter.IsTerminal = CAST(1 as bit)
  WHERE   Vs.BranchID = @BranchID
  And VS.[VoyageNoOutWard] LIKE '%' + @VoyageNo + '%'
  And Jb.LookupID = @JobType

 End
 */

  ;with JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
  SELECT VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
  VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
  VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
  VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
  VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
  VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
  VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
  VS.[ModifiedBy], VS.[ModifiedOn] , Vsl.VesselName , VS.[BranchID],'' as CustomStationCode,'' as PlaceOfArrival,'' as EntryPoint,'' as PSACode,'' as PSANAME,'' as PSAAddress,
  ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
  COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
  COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
  COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
  COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
  COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
  Vsl.CallSignNo As CallSignNo
  FROM   [Operation].[VesselSchedule] VS
  Left Outer Join master.Vessel Vsl On 
   Vs.VesselID = Vsl.VesselID
  Left Outer Join JobTypeLookup Jb ON 
   VS.JobType = jb.LookupID
  Left Outer Join Master.Merchant Agnt On
   Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
   And Agnt.IsLiner = CAST(1 as bit)
  Left Outer Join Master.Port LP On 
   VS.LoadingPort = LP.PortCode
  Left Outer Join Master.Port DiP On 
   VS.DischargePort = DiP.PortCode
  Left Outer Join Master.Port DeP On 
   VS.DestinationPort = DeP.PortCode
  Left Outer Join Master.Merchant Ter ON
   Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
   And Ter.IsTerminal = CAST(1 as bit)
  WHERE   Vs.BranchID = @BranchID
  And (VS.[VoyageNoOutWard] LIKE '%' + @VoyageNo + '%' Or VS.[VoyageNoInWard] LIKE '%' + @VoyageNo + '%')
  --And Jb.LookupID = @JobType
 
END



 