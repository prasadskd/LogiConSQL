USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationDocumentsK2] Record based on [DeclarationDocumentsK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], [DocDate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationDocumentsK2]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationDocumentsK2] Records from [DeclarationDocumentsK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], [DocDate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationDocumentsK2]
		WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsK2List] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationDocumentsK2] Record Into [DeclarationDocumentsK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationDocumentsK2] (
			[BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], [DocDate], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationDocumentsK2] Record Into [DeclarationDocumentsK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationDocumentsK2]
	SET		[SupportingDocumentType] = @SupportingDocumentType, [CustomStationCode] = @CustomStationCode, [ReferenceNo] = @ReferenceNo, 
			[DocDateType] = @DocDateType, [DocDate] = @DocDate, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
			AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsK2Update]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationDocumentsK2] Record Into [DeclarationDocumentsK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationDocumentsK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationDocumentsK2Update] 
		@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationDocumentsK2Insert] 
		@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationDocumentsK2Save]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationDocumentsK2] Record  based on [DeclarationDocumentsK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationDocumentsK2]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       --AND [ItemNo] = @ItemNo
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsK2Delete]
-- ========================================================================================================================================

GO
 