
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderText] Record based on [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTextSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextSelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextSelect] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 

BEGIN
	;with CargoClassLookup As(Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='CargoClass')
	SELECT	OrdTx.[BranchID], OrdTx.[OrderNo], OrdTx.[NoOfpacks], OrdTx.[CargoClass], OrdTx.[MarksNumbers], OrdTx.[CargoDescription], OrdTx.[TotalM3], OrdTx.[TotalMT], 
			OrdTx.[ShipperInvoice], OrdTx.[PONo], OrdTx.[CreatedBy], OrdTx.[CreatedOn], OrdTx.[ModifiedBy], OrdTx.[ModifiedOn] ,
			ISNULL(CCL.LookupDescription,'') As CargoClassDescription
	FROM	[Operation].[OrderText] OrdTx
	Left Outer Join CargoClassLookup CCL ON 
		OrdTx.CargoClass = CCL.LookupID
	WHERE  OrdTx.[BranchID] = @BranchID  
	       AND OrdTx.[OrderNo] = @OrderNo 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderText] Records from [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTextList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextList] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextList] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	;with CargoClassLookup As(Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='CargoClass')
	SELECT	OrdTx.[BranchID], OrdTx.[OrderNo], OrdTx.[NoOfpacks], OrdTx.[CargoClass], OrdTx.[MarksNumbers], OrdTx.[CargoDescription], OrdTx.[TotalM3], OrdTx.[TotalMT], 
			OrdTx.[ShipperInvoice], OrdTx.[PONo], OrdTx.[CreatedBy], OrdTx.[CreatedOn], OrdTx.[ModifiedBy], OrdTx.[ModifiedOn] ,
			ISNULL(CCL.LookupDescription,'') As CargoClassDescription
	FROM	[Operation].[OrderText] OrdTx
	Left Outer Join CargoClassLookup CCL ON 
		OrdTx.CargoClass = CCL.LookupID
	WHERE  OrdTx.[BranchID] = @BranchID  
	       AND OrdTx.[OrderNo] = @OrderNo 
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderText] Records from [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTextPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextPageView] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextPageView] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;with CargoClassLookup As(Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='CargoClass')
	SELECT	OrdTx.[BranchID], OrdTx.[OrderNo], OrdTx.[NoOfpacks], OrdTx.[CargoClass], OrdTx.[MarksNumbers], OrdTx.[CargoDescription], OrdTx.[TotalM3], OrdTx.[TotalMT], 
			OrdTx.[ShipperInvoice], OrdTx.[PONo], OrdTx.[CreatedBy], OrdTx.[CreatedOn], OrdTx.[ModifiedBy], OrdTx.[ModifiedOn] ,
			ISNULL(CCL.LookupDescription,'') As CargoClassDescription
	FROM	[Operation].[OrderText] OrdTx
	Left Outer Join CargoClassLookup CCL ON 
		OrdTx.CargoClass = CCL.LookupID
	WHERE  OrdTx.[BranchID] = @BranchID  
	       AND OrdTx.[OrderNo] = @OrderNo  
	ORDER BY   [OrderNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTextRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextRecordCount] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[OrderText]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderText] Record based on [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTextAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextAutoCompleteSearch] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 

BEGIN

	;with CargoClassLookup As(Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='CargoClass')
	SELECT	OrdTx.[BranchID], OrdTx.[OrderNo], OrdTx.[NoOfpacks], OrdTx.[CargoClass], OrdTx.[MarksNumbers], OrdTx.[CargoDescription], OrdTx.[TotalM3], OrdTx.[TotalMT], 
			OrdTx.[ShipperInvoice], OrdTx.[PONo], OrdTx.[CreatedBy], OrdTx.[CreatedOn], OrdTx.[ModifiedBy], OrdTx.[ModifiedOn] ,
			ISNULL(CCL.LookupDescription,'') As CargoClassDescription
	FROM	[Operation].[OrderText] OrdTx
	Left Outer Join CargoClassLookup CCL ON 
		OrdTx.CargoClass = CCL.LookupID
	WHERE  OrdTx.[BranchID] = @BranchID  
	       AND OrdTx.[OrderNo] = @OrderNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderText] Record Into [OrderText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTextInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextInsert] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @NoOfpacks nvarchar(100),
    @CargoClass smallint,
    @MarksNumbers nvarchar(MAX),
    @CargoDescription nvarchar(MAX),
    @TotalM3 float,
    @TotalMT float,
    @ShipperInvoice nvarchar(MAX),
    @PONo nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[OrderText] ([BranchID], [OrderNo], [NoOfpacks], [CargoClass], [MarksNumbers], [CargoDescription], [TotalM3], [TotalMT], [ShipperInvoice], [PONo], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @OrderNo, @NoOfpacks, @CargoClass, @MarksNumbers, @CargoDescription, @TotalM3, @TotalMT, @ShipperInvoice, @PONo, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderText] Record Into [OrderText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTextUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextUpdate] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @NoOfpacks nvarchar(100),
    @CargoClass smallint,
    @MarksNumbers nvarchar(MAX),
    @CargoDescription nvarchar(MAX),
    @TotalM3 float,
    @TotalMT float,
    @ShipperInvoice nvarchar(MAX),
    @PONo nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Operation].[OrderText]
	SET		[NoOfpacks] = @NoOfpacks, [CargoClass] = @CargoClass, [MarksNumbers] = @MarksNumbers, [CargoDescription] = @CargoDescription, 
			[TotalM3] = @TotalM3, [TotalMT] = @TotalMT, [ShipperInvoice] = @ShipperInvoice, [PONo] = @PONo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderText] Record Into [OrderText] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTextSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextSave] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @NoOfpacks nvarchar(100),
    @CargoClass smallint,
    @MarksNumbers nvarchar(MAX),
    @CargoDescription nvarchar(MAX),
    @TotalM3 float,
    @TotalMT float,
    @ShipperInvoice nvarchar(MAX),
    @PONo nvarchar(MAX),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[OrderText] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo)>0
	BEGIN
	    Exec [Operation].[usp_OrderTextUpdate] 
		@BranchID, @OrderNo, @NoOfpacks, @CargoClass, @MarksNumbers, @CargoDescription, @TotalM3, @TotalMT, @ShipperInvoice, @PONo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderTextInsert] 
		@BranchID, @OrderNo, @NoOfpacks, @CargoClass, @MarksNumbers, @CargoDescription, @TotalM3, @TotalMT, @ShipperInvoice, @PONo, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderTextSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTextDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderText] Record  based on [OrderText]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTextDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTextDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderTextDelete] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[OrderText]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTextDelete]
-- ========================================================================================================================================

GO

 
