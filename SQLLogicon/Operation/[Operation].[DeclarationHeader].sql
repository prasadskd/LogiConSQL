
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationHeader] Record based on [DeclarationHeader] table

-- Exec [Operation].[usp_DeclarationHeaderSelect] 1009001,'DLHQ170200012'
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderSelect]
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			CargoClassDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'CargoClass')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], 
						Hd.[ExporterState], Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.[CreatedBy], Hd.[CreatedOn],Hd.[ModifiedBy], Hd.[ModifiedOn],Hd.CustomStationCode,Hd.ShippingAgent,
						Dc.DeclarantID,Dc.Name As DeclarantName,Dc.NRIC As DeclarantNRIC,Dc.Designation As DeclarantDesignation,
						HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,CRH.RegistrationDate,
						ISNULL(Sh.LoadingPort + ' - ' + LP.PortName,'') As LoadingPortName,
						ISNULL(sh.DischargePort + ' - ' + DP.PortName,'') As DischargePortName,
						ISNULL(sh.TranshipmentPort + ' - ' + TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As ImporterName,
						SA.MerchantName As ShippingAgentName,
						TR.LookupCode As TransactionTypeCode,
						Ex.MerchantName As ExporterName,
						Hd.CargoClass,Hd.CargoDescription,
						ISNULL(CR.LookupDescription,'') As CargoClassDescription,ISNULL(CR.LookupCode,'') As CargoClassCode,
						CustomerReferenceNo,ExtraCargoDescription,Hd.MarksAndNos,Hd.DeclarationShipmentType,Hd.IsPartial
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	Left OUter JOin Master.Merchant Ex ON 
		Hd.Exporter = Convert(varchar(10),Ex.MerchantCode)
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = SA.MerchantCode
	Left Outer Join EDI.CustomResponseHeader CRH ON
	    Hd.[BranchID]=CRH.BranchID and 
		Hd.[DeclarationNo]=CRH.DeclarationNo
	Left Outer Join CargoClassDescription CR ON
		Hd.[CargoClass]=CR.LookupID
	Left Outer Join Master.CustomDeclarant Dc ON 
		Hd.BranchID = Dc.BranchID 
		And Hd.DeclarantID = Dc.DeclarantID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo  
END


-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderSelect]
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationHeader] Records from [DeclarationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderList] 
    @BranchID BIGINT

AS 
BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			CargoClassDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'CargoClass')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], 
						Hd.[ExporterState], Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.[CreatedBy], Hd.[CreatedOn],Hd.[ModifiedBy], Hd.[ModifiedOn],Hd.CustomStationCode,Hd.ShippingAgent,
						Dc.DeclarantID,Dc.Name As DeclarantName,Dc.NRIC As DeclarantNRIC,Dc.Designation As DeclarantDesignation,
						HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,CRH.RegistrationDate,
						ISNULL(Sh.LoadingPort + ' - ' + LP.PortName,'') As LoadingPortName,
						ISNULL(sh.DischargePort + ' - ' + DP.PortName,'') As DischargePortName,
						ISNULL(sh.TranshipmentPort + ' - ' + TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As ImporterName,
						SA.MerchantName As ShippingAgentName,
						TR.LookupCode As TransactionTypeCode,
						Ex.MerchantName As ExporterName,
						Hd.CargoClass,Hd.CargoDescription,
						ISNULL(CR.LookupDescription,'') As CargoClassDescription,ISNULL(CR.LookupCode,'') As CargoClassCode,
						CustomerReferenceNo,ExtraCargoDescription,Hd.MarksAndNos,Hd.DeclarationShipmentType,Hd.IsPartial
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	Left OUter JOin Master.Merchant Ex ON 
		Hd.Exporter = Convert(varchar(10),Ex.MerchantCode)
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = SA.MerchantCode
	Left Outer Join EDI.CustomResponseHeader CRH ON
	    Hd.[BranchID]=CRH.BranchID and 
		Hd.[DeclarationNo]=CRH.DeclarationNo
	Left Outer Join CargoClassDescription CR ON
		Hd.[CargoClass]=CR.LookupID
	Left Outer Join Master.CustomDeclarant Dc ON 
		Hd.BranchID = Dc.BranchID 
		And Hd.DeclarantID = Dc.DeclarantID
	WHERE	Hd.[BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderList] 
-- ========================================================================================================================================


GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationHeader] Record Into [DeclarationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @DeclarationDate datetime,
    @DeclarationType smallint,
    @OpenDate datetime,
    @ImportDate datetime,
    @OrderNo nvarchar(50),
    @TransportMode smallint,
    @ShipmentType smallint,
    @TransactionType smallint,
    @Importer bigint,
    @Exporter nvarchar(50),
    @ExporterAddress1 nvarchar(50),
    @ExporterAddress2 nvarchar(50),
    @ExporterCity nvarchar(50),
    @ExporterState nvarchar(50),
    @ExporterCountry nvarchar(2),
    @ExporterOrganizationType smallint,
    @ExporterTelNo nvarchar(20),
    @SMKCode smallint,
    @CustomStationCode nvarchar(10),
    @ShippingAgent bigint,
    @DeclarantID nvarchar(35),
    @DeclarantName nvarchar(35),
    @DeclarantDesignation nvarchar(35),
    @DeclarantNRIC nvarchar(35),
    @DeclarantAddress1 nvarchar(35),
    @DeclarantAddress2 nvarchar(35),
    @DeclarantAddressCity nvarchar(35),
    @DeclarantAddressState nvarchar(35),
    @DeclarantAddressCountry nvarchar(2),
    @DeclarantAddressPostCode nvarchar(10),
	@CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@CustomerReferenceNo nvarchar(50),
	@ExtraCargoDescription nvarchar(280),
	@MarksAndNos nvarchar(350),
	@DeclarationShipmentType smallint,
	@IsPartial bit,
	@NewDeclarationNo nvarchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationHeader] (
			[BranchID], [DeclarationNo], [DeclarationDate], [DeclarationType], [OpenDate], [ImportDate], [OrderNo], [TransportMode], [ShipmentType], 
			[TransactionType], [Importer], [Exporter], [ExporterAddress1], [ExporterAddress2], [ExporterCity], [ExporterState], [ExporterCountry], 
			[ExporterOrganizationType], [ExporterTelNo], [SMKCode],[CustomStationCode], [ShippingAgent], [DeclarantID], [DeclarantName], [DeclarantDesignation], 
			[DeclarantNRIC], [DeclarantAddress1], [DeclarantAddress2], [DeclarantAddressCity], [DeclarantAddressState], [DeclarantAddressCountry], 
			[DeclarantAddressPostCode],[IsActive], [CreatedBy], [CreatedOn],CargoClass,CargoDescription,CustomerReferenceNo,ExtraCargoDescription,[MarksAndNos],
			DeclarationShipmentType,IsPartial)
	SELECT	@BranchID, @DeclarationNo, @DeclarationDate, @DeclarationType, @OpenDate, @ImportDate, @OrderNo, @TransportMode, @ShipmentType, 
			@TransactionType, @Importer, @Exporter, @ExporterAddress1, @ExporterAddress2, @ExporterCity, @ExporterState, @ExporterCountry, 
			@ExporterOrganizationType, @ExporterTelNo, @SMKCode, @CustomStationCode, @ShippingAgent, @DeclarantID, @DeclarantName, @DeclarantDesignation, 
			@DeclarantNRIC, @DeclarantAddress1, @DeclarantAddress2, @DeclarantAddressCity, @DeclarantAddressState, @DeclarantAddressCountry, 
			@DeclarantAddressPostCode,cast(1 as bit), @CreatedBy, GETUTCDATE(),@CargoClass,@CargoDescription,@CustomerReferenceNo,@ExtraCargoDescription,@MarksAndNos,
			@DeclarationShipmentType,@IsPartial
     
	 
	 /*	
	Insert Into EDI.K1Declaration(BranchID,DeclarationNo,MessageType,CreateDate,EDIDateTime,FileName)
	Select @BranchID,@DeclarationNo,'9',getutcdate(),NULL,NULL
	*/

	Select  @NewDeclarationNo = @DeclarationNo     
	
	    
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationHeader] Record Into [DeclarationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @DeclarationDate datetime,
    @DeclarationType smallint,
    @OpenDate datetime,
    @ImportDate datetime,
    @OrderNo nvarchar(50),
    @TransportMode smallint,
    @ShipmentType smallint,
    @TransactionType smallint,
    @Importer bigint,
    @Exporter nvarchar(50),
    @ExporterAddress1 nvarchar(50),
    @ExporterAddress2 nvarchar(50),
    @ExporterCity nvarchar(50),
    @ExporterState nvarchar(50),
    @ExporterCountry nvarchar(2),
    @ExporterOrganizationType smallint,
    @ExporterTelNo nvarchar(20),
    @SMKCode smallint,
    @CustomStationCode nvarchar(10),
    @ShippingAgent bigint,
    @DeclarantID nvarchar(35),
    @DeclarantName nvarchar(35),
    @DeclarantDesignation nvarchar(35),
    @DeclarantNRIC nvarchar(35),
    @DeclarantAddress1 nvarchar(35),
    @DeclarantAddress2 nvarchar(35),
    @DeclarantAddressCity nvarchar(35),
    @DeclarantAddressState nvarchar(35),
    @DeclarantAddressCountry nvarchar(2),
    @DeclarantAddressPostCode nvarchar(10),
    @IsActive bit,
    @IsApproved bit,
    @ApprovedBy nvarchar(60),
    @ApprovedOn datetime,
	@CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@CustomerReferenceNo nvarchar(50),
	@ExtraCargoDescription nvarchar(280),
	@MarksAndNos nvarchar(350),
	@DeclarationShipmentType smallint,
	@IsPartial bit,
	@NewDeclarationNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationHeader]
	SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [DeclarationDate] = @DeclarationDate, [DeclarationType] = @DeclarationType, 
			[OpenDate] = @OpenDate, [ImportDate] = @ImportDate, [OrderNo] = @OrderNo, [TransportMode] = @TransportMode, [ShipmentType] = @ShipmentType, 
			[TransactionType] = @TransactionType, [Importer] = @Importer, [Exporter] = @Exporter, [ExporterAddress1] = @ExporterAddress1, 
			[ExporterAddress2] = @ExporterAddress2, [ExporterCity] = @ExporterCity, [ExporterState] = @ExporterState, 
			[ExporterCountry] = @ExporterCountry, [ExporterOrganizationType] = @ExporterOrganizationType, [ExporterTelNo] = @ExporterTelNo, 
			[SMKCode] = @SMKCode, [CustomStationCode] = @CustomStationCode, [ShippingAgent] = @ShippingAgent, [DeclarantID] = @DeclarantID, 
			[DeclarantName] = @DeclarantName, [DeclarantDesignation] = @DeclarantDesignation, [DeclarantNRIC] = @DeclarantNRIC, 
			[DeclarantAddress1] = @DeclarantAddress1, [DeclarantAddress2] = @DeclarantAddress2, [DeclarantAddressCity] = @DeclarantAddressCity, 
			[DeclarantAddressState] = @DeclarantAddressState, [DeclarantAddressCountry] = @DeclarantAddressCountry, 
			[DeclarantAddressPostCode] = @DeclarantAddressPostCode,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),CargoClass=@CargoClass,CargoDescription=@CargoDescription,
			CustomerReferenceNo=@CustomerReferenceNo,ExtraCargoDescription = @ExtraCargoDescription, MarksAndNos=@MarksAndNos,
			DeclarationShipmentType = @DeclarationShipmentType,IsPartial = @IsPartial
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
	
	/*
	Insert Into EDI.K1Declaration(BranchID,DeclarationNo,MessageType,CreateDate,EDIDateTime,FileName)
	Select @BranchID,@DeclarationNo,'5',getutcdate(),NULL,NULL*/



	Select  @NewDeclarationNo = @DeclarationNo          


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationHeader] Record Into [DeclarationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @DeclarationDate datetime,
    @DeclarationType smallint,
    @OpenDate datetime,
    @ImportDate datetime,
    @OrderNo nvarchar(50),
    @TransportMode smallint,
    @ShipmentType smallint,
    @TransactionType smallint,
    @Importer bigint,
    @Exporter nvarchar(50),
    @ExporterAddress1 nvarchar(50),
    @ExporterAddress2 nvarchar(50),
    @ExporterCity nvarchar(50),
    @ExporterState nvarchar(50),
    @ExporterCountry nvarchar(2),
    @ExporterOrganizationType smallint,
    @ExporterTelNo nvarchar(20),
    @SMKCode smallint,
    @IsActive bit,
    @IsApproved bit,
    @ApprovedBy nvarchar(60),
    @ApprovedOn datetime,
    @CustomStationCode nvarchar(10),
    @ShippingAgent bigint,
    @DeclarantID nvarchar(35),
    @DeclarantName nvarchar(35),
    @DeclarantDesignation nvarchar(35),
    @DeclarantNRIC nvarchar(35),
    @DeclarantAddress1 nvarchar(35),
    @DeclarantAddress2 nvarchar(35),
    @DeclarantAddressCity nvarchar(35),
    @DeclarantAddressState nvarchar(35),
    @DeclarantAddressCountry nvarchar(2),
    @DeclarantAddressPostCode nvarchar(10),
	@CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@CustomerReferenceNo nvarchar(50),
	@ExtraCargoDescription nvarchar(280),
	@MarksAndNos nvarchar(350),
	@DeclarationShipmentType smallint,
	@IsPartial bit,
	@NewDeclarationNo nvarchar(50) OUTPUT
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationHeaderUpdate] 
		@BranchID, @DeclarationNo, @DeclarationDate, @DeclarationType, @OpenDate, @ImportDate, @OrderNo, @TransportMode, @ShipmentType, 
			@TransactionType, @Importer, @Exporter, @ExporterAddress1, @ExporterAddress2, @ExporterCity, @ExporterState, @ExporterCountry, 
			@ExporterOrganizationType, @ExporterTelNo, @SMKCode, @CustomStationCode, @ShippingAgent, @DeclarantID, @DeclarantName, @DeclarantDesignation, 
			@DeclarantNRIC, @DeclarantAddress1, @DeclarantAddress2, @DeclarantAddressCity, @DeclarantAddressState, @DeclarantAddressCountry, 
			@DeclarantAddressPostCode,@IsActive,@IsApproved,@ApprovedBy,@ApprovedOn,@CreatedBy, @ModifiedBy,@CargoClass,@CargoDescription,
			@CustomerReferenceNo,@ExtraCargoDescription,@MarksAndNos,@DeclarationShipmentType, @IsPartial, @NewDeclarationNo = @NewDeclarationNo OUTPUT 


	END
	ELSE
	BEGIN


	Declare		@Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		Select @DeclarationNo='',@DocID ='Opr\DeclarationD1',@Dt = GetUtcDate()
		
		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1001001, 'Opr\DeclarationD1', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		--select @BranchID, @DocID, @Dt ,@CreatedBy
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @DeclarationNo OUTPUT

		 

	    Exec [Operation].[usp_DeclarationHeaderInsert] 
			@BranchID, @DeclarationNo, @DeclarationDate, @DeclarationType, @OpenDate, @ImportDate, @OrderNo, @TransportMode, @ShipmentType, 
			@TransactionType, @Importer, @Exporter, @ExporterAddress1, @ExporterAddress2, @ExporterCity, @ExporterState, @ExporterCountry, 
			@ExporterOrganizationType, @ExporterTelNo, @SMKCode, @CustomStationCode, @ShippingAgent, @DeclarantID, @DeclarantName, @DeclarantDesignation, 
			@DeclarantNRIC, @DeclarantAddress1, @DeclarantAddress2, @DeclarantAddressCity, @DeclarantAddressState, @DeclarantAddressCountry, 
			@DeclarantAddressPostCode,@CreatedBy, @ModifiedBy,@CargoClass,@CargoDescription,
			@CustomerReferenceNo,@ExtraCargoDescription,@MarksAndNos,@DeclarationShipmentType, @IsPartial, @NewDeclarationNo = @NewDeclarationNo OUTPUT 

	END
	
END

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationHeader] Record  based on [DeclarationHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Operation].[DeclarationHeader]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy=@ModifiedBy,ModifiedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo

	 
	Insert Into EDI.K1Declaration(BranchID,DeclarationNo,MessageType,CreateDate,EDIDateTime,FileName)
	Select @BranchID,@DeclarationNo,'1',getutcdate(),NULL,NULL


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderDelete]
-- ========================================================================================================================================


GO
 

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationHeader] Record  based on [DeclarationHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderApproval]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderApproval] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderApproval] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@ApprovedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Operation].[DeclarationHeader]
	SET	[IsApproved] = CAST(1 as bit),ApprovedBy=@ApprovedBy,ApprovedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo

	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderApproval]
-- ========================================================================================================================================
GO


IF OBJECT_ID('[Operation].[usp_UpdateOrderNoInDeclarationHeader]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_UpdateOrderNoInDeclarationHeader] 
END 
GO
Create Procedure [Operation].[usp_UpdateOrderNoInDeclarationHeader]
@BranchID bigint,
@DeclarationNo varchar(20),
@OrderNo varchar(20)
As
Begin
 Update Operation.DeclarationHeader Set OrderNo = @OrderNo Where BranchID = @BranchID and DeclarationNo = @DeclarationNo
 Update Operation.DeclarationContainer Set OrderNo = @OrderNo Where BranchID = @BranchID and DeclarationNo = @DeclarationNo
 Update Operation.DeclarationItem Set OrderNo = @OrderNo Where BranchID = @BranchID and DeclarationNo = @DeclarationNo
End



go


IF OBJECT_ID('[EDI].[usp_k1DeclarationSaveFromK1]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_k1DeclarationSaveFromK1] 
END 
GO


CREATE PROC  [EDI].[usp_k1DeclarationSaveFromK1]
(
@BranchID bigint,
@DeclarationNo varchar(30)
)
As
Begin

Declare @ReferenceNo bigint,
		@SNRF nvarchar(50)

 IF (SELECT COUNT(0) FROM [EDI].[K1Declaration] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo and MessageType = 9) = 0
 Begin
	Insert Into EDI.K1Declaration(BranchID,DeclarationNo,MessageType,CreateDate,EDIDateTime,FileName, MessageId)
	Values(@BranchID, @DeclarationNo, '9', getutcdate(),NULL,NULL, NEWID())

	Select @ReferenceNo = IDENT_CURRENT('EDI.K1Declaration')
  
	Select @SNRF =  substring(Convert(char(4),Getutcdate(),120),3,2) + Convert(varchar(10),@BranchID) + Convert(nvarchar(10),@ReferenceNo)

	Insert Into EDI.CustomResponseHeader(BranchID, ResponseReferenceNo, DeclarationNo, RegistrationNo, RegistrationDate, Remark, Code, SNRF, Status, OfficerID, OfficeName)
	Values(@BranchID, @DeclarationNo, @DeclarationNo, '', GETUTCDATE(), '', '',@SNRF, Cast(1 as bit), '', '')

	Insert Into EDI.CustomResponseDetail(BranchID, ResponseReferenceNo, DeclarationNo, ItemNo, Code, Description, ItemLine)
	Values(@BranchID, @DeclarationNo, @DeclarationNo, 1, '', '', '')

	Update EDI.K1Declaration Set SNRF = @SNRF
	Where BranchID = @BranchID 
	And DeclarationNo = @DeclarationNo
	And ReferenceNo = @ReferenceNo

	
 End 
 Else
 Begin
	Insert Into EDI.K1Declaration(BranchID,DeclarationNo,MessageType,CreateDate,EDIDateTime,FileName, MessageId)
	Values(@BranchID, @DeclarationNo, '5', getutcdate(),NULL,NULL, NEWID())

	Select @ReferenceNo = IDENT_CURRENT('EDI.K1Declaration')
  
	Select @SNRF =  substring(Convert(char(4),Getutcdate(),120),3,2) + Convert(varchar(10),@BranchID) + Convert(nvarchar(10),@ReferenceNo)


	Update EDI.K1Declaration Set SNRF = @SNRF
	Where BranchID = @BranchID 
	And DeclarationNo = @DeclarationNo
	And ReferenceNo = @ReferenceNo

	Update EDI.CustomResponseHeader Set SNRF = @SNRF
	Where 
	BranchID = @BranchID
	And ResponseReferenceNo = @DeclarationNo
	And  DeclarationNo = @DeclarationNo

 End
End




GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_K1DataTableList]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	12-Apr-2017
-- Description:	Get Declaration header record list

-- ========================================================================================================================================
IF OBJECT_ID('[Operation].[usp_K1DataTableList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_K1DataTableList]
END 
GO


CREATE PROC [Operation].[usp_K1DataTableList]
    @BranchID BIGINT,
	@OrderNo NVARCHAR(50) =NULL,
    @DeclarationNo NVARCHAR(50) =NULL,
	@ManifestNo nvarchar(50) =NULL,
	@OceanBLNo nvarchar(50) =NULL,
	@HouseBLNo nvarchar(50) =NULL,
	@VoyageNo nvarchar(20) =NULL,
	@DeclarationType smallint=0,
	@DateFrom datetime =NULL,
	@DateTo DateTime =NULL,
	
	@limit smallint,
	@offset smallint,
	@sortColumn varchar(50),
	@sortType varchar(50)

AS 

BEGIN

	declare @sql nvarchar(max);
	if(@sortColumn='CreatedOn')
		set @sortColumn = 'Hd.CreatedOn'

	set @sql = N';with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''DeclarationType''),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''TransportMode''),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ShipmentType''),
			TransactionTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory = ''TransactionTypeK1'')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter], Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,
						ISNULL(LP.PortName,'''') As LoadingPortName,
						ISNULL(DP.PortName,'''') As DischargePortName,
						ISNULL(TP.PortName,'''') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'''') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'''') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'''') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As LocalTraderName
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = DT.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = DT.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	WHERE   Hd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND Hd.[IsActive] = CAST(1 as bit)'  

	if (@DeclarationType > 0) set @sql = @sql + ' AND  Hd.[DeclarationType]  =' + Convert(varchar(10),@DeclarationType)
	if (len(rtrim(@DeclarationNo)) > 0) set @sql = @sql + ' AND  Hd.[DeclarationNo] LIKE ''%' + @DeclarationNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And Sh.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@OceanBLNo)) > 0) set @sql = @sql + ' And Sh.OceanBLNo LIKE ''%' + @OceanBLNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And Sh.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''
	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql + ' And Hd.OrderNo LIKE ''%' + @OrderNo  + '%'''

	if @DeclarationType = 9101
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoInWard LIKE ''%' + @VoyageNo  + '%'''
	else if @DeclarationType = 9102
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoOutWard LIKE ''%' + @VoyageNo  + '%'''

	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.CreatedOn,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Hd.CreatedOn)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.CreatedOn,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Hd.CreatedOn)'

	Set @Sql += ' Order By ' + @sortColumn + ' ' + @sortType + ' OFFSET ' + Convert(varchar(5), @offset) + ' ROWS Fetch Next ' + Convert(varchar(5), @limit) + ' Rows Only';

 Exec(@Sql)


END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_K1DataTableList]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_K1RecordCount]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	12-Apr-2017
-- Description:	Get Declaration header record count

-- ========================================================================================================================================
IF OBJECT_ID('[Operation].[usp_K1RecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_K1RecordCount]
END 
GO

CREATE Procedure [Operation].[usp_K1RecordCount]
	@BranchID BIGINT,
	@OrderNo NVARCHAR(50) =NULL,
    @DeclarationNo NVARCHAR(50) =NULL,
	@ManifestNo nvarchar(50) =NULL,
	@OceanBLNo nvarchar(50) =NULL,
	@HouseBLNo nvarchar(50) =NULL,
	@VoyageNo nvarchar(20) =NULL,
	@DeclarationType smallint=0,
	@DateFrom datetime =NULL,
	@DateTo DateTime =NULL

As
Begin
 
 declare @sql nvarchar(max);

	set @sql = N';
	SELECT	COUNT(0)
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	WHERE   Hd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND Hd.[IsActive] = CAST(1 as bit)'  

	if (@DeclarationType > 0) set @sql = @sql + ' AND  Hd.[DeclarationType]  =' + Convert(varchar(10),@DeclarationType)
	if (len(rtrim(@DeclarationNo)) > 0) set @sql = @sql + ' AND  Hd.[DeclarationNo] LIKE ''%' + @DeclarationNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And Sh.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@OceanBLNo)) > 0) set @sql = @sql + ' And Sh.OceanBLNo LIKE ''%' + @OceanBLNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And Sh.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''
	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql + ' And Hd.OrderNo LIKE ''%' + @OrderNo  + '%'''

	if @DeclarationType = 9101
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoInWard LIKE ''%' + @VoyageNo  + '%'''
	else if @DeclarationType = 9102
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoOutWard LIKE ''%' + @VoyageNo  + '%'''

	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.CreatedOn,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Hd.CreatedOn)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.CreatedOn,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Hd.CreatedOn)'

 Exec(@Sql) 

End


-- ========================================================================================================================================
-- END  											 [Operation].[usp_K1RecordCount]
-- ========================================================================================================================================


GO