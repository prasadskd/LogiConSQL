
/****** Object:  Table [Operation].[DeclarationExcemptionK2]    Script Date: 22-01-2017 15:36:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationExcemptionK2](
	[BranchID] [bigint] NOT NULL,
	[Declarationno] [nvarchar](50) NOT NULL,
	[GeneralExcemptionType] [smallint] NOT NULL,
	[GeneralExcemptionRefNo] [nvarchar](50) NULL,
	[ClaimentCode] [bigint] NULL,
	[ClaimentName] [nvarchar](50) NULL,
	[ClaimentCompany] [nvarchar](50) NULL,
	[ClaimentNRIC] [nvarchar](20) NULL,
	[ClaimentDesignation] [nvarchar](50) NULL,
	[SalesTaxExcemptionTyp] [smallint] NULL,
	[CustomStationCode] [nvarchar](10) NULL,
	[SalesTaxReferenceNo] [nvarchar](50) NULL,
	[SalesTaxRegistratonDate] [datetime] NULL,
	[SpecialTreatmentType] [smallint] NULL,
	[SpecialTreatmentReferenceNo] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationExcemptionK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[Declarationno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


