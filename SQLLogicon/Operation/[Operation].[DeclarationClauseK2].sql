
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationClauseK2] Record based on [DeclarationClauseK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationClauseK2]
	WHERE  [BranchID] = @BranchID 
	       AND [DeclarationNo] = @DeclarationNo 
	       AND [ItemNo] = @ItemNo 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationClauseK2] Records from [DeclarationClauseK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT [BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationClauseK2]
		WHERE  [BranchID] = @BranchID 
	       AND [DeclarationNo] = @DeclarationNo 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseK2List] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationClauseK2] Record Into [DeclarationClauseK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationClauseK2] (
			[BranchID], [DeclarationNo], [ItemNo], [ClauseCode], [ClauseText], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationClauseK2] Record Into [DeclarationClauseK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationClauseK2]
	SET    [ClauseCode] = @ClauseCode, [ClauseText] = @ClauseText, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseK2Update]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationClauseK2] Record Into [DeclarationClauseK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @ClauseCode smallint,
    @ClauseText nvarchar(2000),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationClauseK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationClauseK2Update] 
		@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationClauseK2Insert] 
		@BranchID, @DeclarationNo, @ItemNo, @ClauseCode, @ClauseText, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationClauseK2Save]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationClauseK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationClauseK2] Record  based on [DeclarationClauseK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationClauseK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationClauseK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationClauseK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50) 
AS 

	
BEGIN

	 
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Operation].[DeclarationClauseK2]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	        
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationClauseK2Delete]
-- ========================================================================================================================================

GO
 
