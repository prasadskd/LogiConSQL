-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivitySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderActivity] Record based on [OrderActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderActivitySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivitySelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivitySelect] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
    @Activity NVARCHAR(200),
    @ActivityDate DATETIME
AS 

BEGIN

	;with ActivityDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderActivityParty')
	SELECT Oa.[BranchID], Oa.[OrderNo], Oa.[Activity], Oa.[ActivityDate], Oa.[ActivityParty], Oa.[ActiviytBy], Oa.[ActivityOn] ,
		ISNULL(Ad.LookupDescription,'') As ActivityPartyDescription
	FROM   [Operation].[OrderActivity] Oa
	Left Outer Join ActivityDescription Ad ON 
		Oa.ActivityParty = Ad.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 
	       AND [Activity] = @Activity 
	       AND [ActivityDate] = @ActivityDate
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivitySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderActivity] Records from [OrderActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderActivityList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityList] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityList] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)	
AS 
BEGIN

	;with ActivityDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderActivityParty')
	SELECT Oa.[BranchID], Oa.[OrderNo], Oa.[Activity], Oa.[ActivityDate], Oa.[ActivityParty], Oa.[ActiviytBy], Oa.[ActivityOn] ,
		ISNULL(Ad.LookupDescription,'') As ActivityPartyDescription
	FROM   [Operation].[OrderActivity] Oa
	Left Outer Join ActivityDescription Ad ON 
		Oa.ActivityParty = Ad.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderActivity] Records from [OrderActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderActivityPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityPageView] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityPageView] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;with ActivityDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderActivityParty')
	SELECT Oa.[BranchID], Oa.[OrderNo], Oa.[Activity], Oa.[ActivityDate], Oa.[ActivityParty], Oa.[ActiviytBy], Oa.[ActivityOn] ,
		ISNULL(Ad.LookupDescription,'') As ActivityPartyDescription
	FROM   [Operation].[OrderActivity] Oa
	Left Outer Join ActivityDescription Ad ON 
		Oa.ActivityParty = Ad.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 
	ORDER BY [Activity]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderActivityRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityRecordCount] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[OrderActivity]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderActivity] Record based on [OrderActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderActivityAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityAutoCompleteSearch] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 

BEGIN

	;with ActivityDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderActivityParty')
	SELECT Oa.[BranchID], Oa.[OrderNo], Oa.[Activity], Oa.[ActivityDate], Oa.[ActivityParty], Oa.[ActiviytBy], Oa.[ActivityOn] ,
		ISNULL(Ad.LookupDescription,'') As ActivityPartyDescription
	FROM   [Operation].[OrderActivity] Oa
	Left Outer Join ActivityDescription Ad ON 
		Oa.ActivityParty = Ad.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo 
	       AND [OrderNo] LIKE '%' +  @OrderNo  + '%'
 END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderActivity] Record Into [OrderActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderActivityInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityInsert] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @Activity nvarchar(200),
    @ActivityDate datetime,
    @ActivityParty smallint,
    @ActiviytBy nvarchar(50) 
AS 
  

BEGIN

	
	INSERT INTO [Operation].[OrderActivity] (
			[BranchID], [OrderNo], [Activity], [ActivityDate], [ActivityParty], [ActiviytBy], [ActivityOn])
	SELECT	@BranchID, @OrderNo, @Activity, @ActivityDate, @ActivityParty, @ActiviytBy, Getutcdate()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderActivity] Record Into [OrderActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderActivityUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityUpdate] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @Activity nvarchar(200),
    @ActivityDate datetime,
    @ActivityParty smallint,
    @ActiviytBy nvarchar(50) 
AS 
 
	
BEGIN

	UPDATE	[Operation].[OrderActivity]
	SET		[ActivityParty] = @ActivityParty, [ActiviytBy] = @ActiviytBy, [ActivityOn] = getutcdate()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [Activity] = @Activity
			AND [ActivityDate] = @ActivityDate
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivitySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderActivity] Record Into [OrderActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderActivitySave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivitySave] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivitySave] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @Activity nvarchar(200),
    @ActivityDate datetime,
    @ActivityParty smallint,
    @ActiviytBy nvarchar(50) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[OrderActivity] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [Activity] = @Activity
	       AND [ActivityDate] = @ActivityDate)>0
	BEGIN
	    Exec [Operation].[usp_OrderActivityUpdate] 
		@BranchID, @OrderNo, @Activity, @ActivityDate, @ActivityParty, @ActiviytBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderActivityInsert] 
		@BranchID, @OrderNo, @Activity, @ActivityDate, @ActivityParty, @ActiviytBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderActivitySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderActivityDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderActivity] Record  based on [OrderActivity]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderActivityDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderActivityDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderActivityDelete] 
    @BranchID bigint,
    @OrderNo nvarchar(50)
     
AS 

	
BEGIN
 
	DELETE
	FROM   [Operation].[OrderActivity]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	        


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderActivityDelete]
-- ========================================================================================================================================

GO
 