
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationShipmentK2] Record based on [DeclarationShipmentK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ManifestNo NVARCHAR(50)
AS 

BEGIN

	SELECT	Sh.[BranchID], Sh.[DeclarationNo], Sh.[ManifestNo], Sh.[VesselScheduleID], Sh.[SCNNo], Sh.[VesselID], Sh.[VesselName], Sh.[VoyageNo], Sh.[LoadingPort], 
			Sh.[DischargePort], Sh.[TranshipmentPort],Sh.[PlaceOfExport], Sh.[PortOperator], Sh.[OceanBLNo], Sh.[HouseBLNo], Sh.[ETADate], Sh.[WarehouseNo], 
			Sh.[WagonNo], Sh.[VehicleNo1], 
			Sh.[VehicleNo2], Sh.[FlightNo], Sh.[ARNNo], Sh.[MasterAWBNo], Sh.[HouseAWBNo], Sh.[AIRCOLoadNo], Sh.[JKNo], Sh.[CreatedBy], 
			Sh.[CreatedOn], Sh.[ModifiedBy], Sh.[ModifiedOn],Sh.DestinationPort,
			ISNULL(SH.PlaceOfExport + ' - ' + POI.PortName,'') As PlaceOfExportName,
			ISNULL(SH.PlaceOfExport + ' - ' + LP.PortName,'') As LoadingPortName,
			ISNULL(SH.PlaceOfExport + ' - ' + DP.PortName,'') As DischargePortName,
			ISNULL(SH.PlaceOfExport + ' - ' + TP.PortName,'') As TranshipmentPortName,
			ISNULL(SH.PlaceOfExport + ' - ' + Opr.OperatorName,'') As PortOperatorName,
			ISNULL(SH.DestinationPort + ' - ' + Dsp.PortName,'') As DestinationPortName
	FROM   [Operation].[DeclarationShipmentK2] Sh
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfExport = POI.PortCode
	Left Outer Join Master.Port LP ON 
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON 
			Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON 
			Sh.TranshipmentPort = TP.PortCode
	Left Outer Join Master.Operator Opr ON 
		Sh.PortOperator = Opr.OperatorID
	Left Outer Join Master.Port Dsp ON 
		Sh.DestinationPort = Dsp.PortCode
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ManifestNo] = @ManifestNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationShipmentK2] Records from [DeclarationShipmentK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	Sh.[BranchID], Sh.[DeclarationNo], Sh.[ManifestNo], Sh.[VesselScheduleID], Sh.[SCNNo], Sh.[VesselID], Sh.[VesselName], Sh.[VoyageNo], Sh.[LoadingPort], 
			Sh.[DischargePort], Sh.[TranshipmentPort],Sh.[PlaceOfExport], Sh.[PortOperator], Sh.[OceanBLNo], Sh.[HouseBLNo], Sh.[ETADate], Sh.[WarehouseNo], 
			Sh.[WagonNo], Sh.[VehicleNo1], 
			Sh.[VehicleNo2], Sh.[FlightNo], Sh.[ARNNo], Sh.[MasterAWBNo], Sh.[HouseAWBNo], Sh.[AIRCOLoadNo], Sh.[JKNo], Sh.[CreatedBy], 
			Sh.[CreatedOn], Sh.[ModifiedBy], Sh.[ModifiedOn],Sh.DestinationPort,
			ISNULL(SH.PlaceOfExport + ' - ' + POI.PortName,'') As PlaceOfExportName,
			ISNULL(SH.PlaceOfExport + ' - ' + LP.PortName,'') As LoadingPortName,
			ISNULL(SH.PlaceOfExport + ' - ' + DP.PortName,'') As DischargePortName,
			ISNULL(SH.PlaceOfExport + ' - ' + TP.PortName,'') As TranshipmentPortName,
			ISNULL(SH.PlaceOfExport + ' - ' + Opr.OperatorName,'') As PortOperatorName,
			ISNULL(SH.DestinationPort + ' - ' + Dsp.PortName,'') As DestinationPortName
	FROM   [Operation].[DeclarationShipmentK2] Sh
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfExport = POI.PortCode
	Left Outer Join Master.Port LP ON 
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON 
			Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON 
			Sh.TranshipmentPort = TP.PortCode
	Left Outer Join Master.Operator Opr ON 
		Sh.PortOperator = Opr.OperatorID
	Left Outer Join Master.Port Dsp ON 
		Sh.DestinationPort = Dsp.PortCode
	WHERE  [BranchID] = @BranchID  
	    AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentK2List] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationShipmentK2] Record Into [DeclarationShipmentK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfExport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
	@DestinationPort nvarchar(10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
		INSERT INTO [Operation].[DeclarationShipmentK2] (
				[BranchID], [DeclarationNo], [ManifestNo], [VesselScheduleID], [SCNNo], [VesselID], [VesselName], [VoyageNo], 
				[LoadingPort], [DischargePort], [TranshipmentPort], PlaceOfExport,[PortOperator], [OceanBLNo], [HouseBLNo], [ETADate], 
				[WarehouseNo], [WagonNo], [VehicleNo1], [VehicleNo2], [FlightNo], [ARNNo], [MasterAWBNo], [HouseAWBNo], [AIRCOLoadNo], [JKNo],DestinationPort ,
				[CreatedBy], [CreatedOn])
		SELECT	@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
				@DischargePort, @TranshipmentPort, @PlaceOfExport,@PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
				@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo,@DestinationPort, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationShipmentK2] Record Into [DeclarationShipmentK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfExport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
	@DestinationPort nvarchar(10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationShipmentK2]
		SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [ManifestNo] = @ManifestNo, [VesselScheduleID] = @VesselScheduleID, 
			[SCNNo] = @SCNNo, [VesselID] = @VesselID, [VesselName] = @VesselName, [VoyageNo] = @VoyageNo, 
			[LoadingPort] = @LoadingPort, [DischargePort] = @DischargePort, [TranshipmentPort] = @TranshipmentPort,PlaceOfExport=@PlaceOfExport,
			[PortOperator] = @PortOperator, [OceanBLNo] = @OceanBLNo, [HouseBLNo] = @HouseBLNo, [ETADate] = @ETADate, 
			[WarehouseNo] = @WarehouseNo, [WagonNo] = @WagonNo, [VehicleNo1] = @VehicleNo1, [VehicleNo2] = @VehicleNo2, 
			[FlightNo] = @FlightNo, [ARNNo] = @ARNNo, [MasterAWBNo] = @MasterAWBNo, [HouseAWBNo] = @HouseAWBNo, [AIRCOLoadNo] = @AIRCOLoadNo, 
			[JKNo] = @JKNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),DestinationPort = @DestinationPort
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ManifestNo] = @ManifestNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentK2Update]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationShipmentK2] Record Into [DeclarationShipmentK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50),
    @VesselScheduleID bigint,
    @SCNNo nvarchar(50),
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VoyageNo nvarchar(10),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @TranshipmentPort nvarchar(10),
	@PlaceOfExport nvarchar(10),
    @PortOperator bigint,
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ETADate datetime,
    @WarehouseNo nvarchar(20),
    @WagonNo nvarchar(35),
    @VehicleNo1 nvarchar(35),
    @VehicleNo2 nvarchar(35),
    @FlightNo nvarchar(35),
    @ARNNo nvarchar(35),
    @MasterAWBNo nvarchar(35),
    @HouseAWBNo nvarchar(35),
    @AIRCOLoadNo nvarchar(35),
    @JKNo nvarchar(35),
	@DestinationPort nvarchar(10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationShipmentK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ManifestNo] = @ManifestNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationShipmentK2Update] 
		@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
		@DischargePort, @TranshipmentPort,@PlaceOfExport, @PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
		@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo,@DestinationPort, 
		@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationShipmentK2Insert] 
		@BranchID, @DeclarationNo, @ManifestNo, @VesselScheduleID, @SCNNo, @VesselID, @VesselName, @VoyageNo, @LoadingPort, 
		@DischargePort, @TranshipmentPort,@PlaceOfExport, @PortOperator, @OceanBLNo, @HouseBLNo, @ETADate, @WarehouseNo, @WagonNo, 
		@VehicleNo1, @VehicleNo2, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo, @DestinationPort,
		@CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationShipmentK2Save]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationShipmentK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationShipmentK2] Record  based on [DeclarationShipmentK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationShipmentK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationShipmentK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationShipmentK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ManifestNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationShipmentK2]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	      -- AND [ManifestNo] = @ManifestNo
 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationShipmentK2Delete]
-- ========================================================================================================================================


GO
 