
-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [VesselSchedule] Records from [VesselSchedule] table

-- Exec [Operation].[usp_VesselScheduleSearch] 1001001,NULL,NULL,NULL,NULL
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSearch] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSearch] 
	@BranchID BIGINT,
	@VesselID nvarchar(20), 
	@AgentCode nvarchar(10),
	@JobType smallint,
    @VoyageNo nvarchar(20)
	 
AS 
BEGIN


	If LTRIM(RTRIM(@VesselID))='' 
		Set @VesselID = NULL
	If LTRIM(RTRIM(@AgentCode))='' 
		Set @AgentCode = NULL

	If @JobType <=0
		Set @JobType=NULL


	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	ISNULL(Vsl.VesselName,'') As VesselName ,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(LP.PortName,'') As LoadingPortName,
	ISNULL(DiP.PortName,'') As DischargePortName,
	ISNULL(DeP.PortName,'') As DestinationPortName,
	ISNULL(Ter.MerchantName,'') As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(nvarchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE VS.BranchID = @BranchID
	AND VS.VesselID = ISNULL(@VesselID,VS.VesselID)
	And VS.AgentCode = ISNULL(@AgentCode,VS.AgentCode)
	--And VS.JobType = ISNULL(@JobType,VS.JobType)
	--And VS.VoyageNo LIKE '%' + @VoyageNo + '%'
	--And Convert(Char(10),VS.ETA,120) > Convert(Char(10),GETUTCDATE(),120)
	Order By VS.ETD desc
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleSearch] 
-- ========================================================================================================================================

GO


IF OBJECT_ID('[Operation].[usp_VesselScheduleSearch2]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSearch2] 
END 
GO
CREATE Procedure [Operation].[usp_VesselScheduleSearch2] 
@BranchID bigint,
@LoadingPort varchar(40),
@DestinationPort varchar(40)
As
Begin
    
	Declare @sql varchar(max);
    
	Set @sql = ';with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory=''JobType'')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	ISNULL(Vsl.VesselName,'''') As VesselName ,
	ISNULL(Jb.LookupDescription,'''') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'''') As AgentName,
	ISNULL(LP.PortName,'''') As LoadingPortName,
	ISNULL(DiP.PortName,'''') As DischargePortName,
	ISNULL(DeP.PortName,'''') As DestinationPortName,
	ISNULL(Ter.MerchantName,'''') As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(nvarchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE VS.BranchID = ' + Convert(varchar(10), @BranchID);

	If LTRIM(RTRIM(@LoadingPort)) <> ''
	Set @sql += ' And LoadingPort = ''' + @LoadingPort + ''''
	If LTRIM(RTRIM(@DestinationPort)) <> ''
	Set @sql += ' And DestinationPort =  ''' + @DestinationPort + ''''
	--And VS.JobType = ISNULL(@JobType,VS.JobType)
	--And VS.VoyageNo LIKE '%' + @VoyageNo + '%'
	--And Convert(Char(10),VS.ETA,120) > Convert(Char(10),GETUTCDATE(),120)
	--Order By VS.ETD desc
	print @sql
	exec(@sql)
End
