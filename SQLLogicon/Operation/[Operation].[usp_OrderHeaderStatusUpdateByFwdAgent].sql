

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderStatusUpdateByFwdAgent]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	18-Dec-2016
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderStatusUpdateByFwdAgent]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderStatusUpdateByFwdAgent] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderStatusUpdateByFwdAgent] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
	@IsApproved bit,
	@Remarks nvarchar(100),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN 
	
Declare @WebOrderStatus smallint=0;
Declare @WebOrderStatusDescription nvarchar(100);
Declare @WebOrderStatusSentToAgent smallint=0;
Declare @ActivityParty smallint=0;


		Select @ActivityParty= LookupID From Config.Lookup 
		Where LookupCategory='OrderActivityParty' And LookupCode='OA-FA'



	If @IsApproved = Cast(1 as bit)
	Begin
		Select @WebOrderStatus = LookupID,@WebOrderStatusDescription= LookupDescription From Config.Lookup 
		Where LookupCategory='WebOrderStatus' And LookupCode='WEB-AFA'
	End
	Else
	Begin
		Select @WebOrderStatus = LookupID,@WebOrderStatusDescription= LookupDescription From Config.Lookup 
		Where LookupCategory='WebOrderStatus' And LookupCode='WEB-RFA'
	End

	Select @WebOrderStatusSentToAgent = LookupID From Config.Lookup 
	Where LookupCategory='WebOrderStatus' And LookupCode='WEB-SFA'
	



	UPDATE	[Operation].[OrderHeader]
	SET	WebOrderStatus = @WebOrderStatus,FwdAgentRemarks = @Remarks, FwdAgentModifiedBy = @ModifiedBy,FwdAgentModifiedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
		   And WebOrderStatus = @WebOrderStatusSentToAgent


 
	Insert Into Operation.OrderActivity
	Select @BranchID,@OrderNo,@WebOrderStatusDescription,GETUTCDATE(),@ActivityParty,@ModifiedBy,GETUTCDATE()


SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderDelete]
-- ========================================================================================================================================

GO
 