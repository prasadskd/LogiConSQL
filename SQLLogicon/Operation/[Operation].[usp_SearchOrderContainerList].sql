
-- ========================================================================================================================================
-- START											 [Operation].[usp_SearchOrderContainerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderContainer] Records from [OrderContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_SearchOrderContainerList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_SearchOrderContainerList] 
END 
GO
CREATE PROC [Operation].[usp_SearchOrderContainerList] 
    @ContainerNo nVARCHAR(15)
AS 
BEGIN

SET NOCOUNT ON;

	;with 
	TempratureLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TempratureType'),
	ContainerGradeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='ContainerGradeType'),
	VentLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='VentilationType'),
	CargoTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='CargoType'),
	TrailerTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TrailerType'),
	SpecialHandlingTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='SpecialHandlingType')
	SELECT	Dt.[BranchID], Dt.[OrderNo], Dt.[ContainerKey], Dt.[ContainerNo], Dt.[Size], Dt.[Type], Dt.[SealNo], Dt.[SealNo2], Dt.[ContainerGrade], 
			Dt.[Temprature], Dt.[TemperatureMode],Dt.[Vent], Dt.[VentMode], Dt.[GrossWeight], Dt.[CargoWeight], Dt.[VGM],Dt.[VGMType],
			Dt.[CargoType], Dt.[TrailerType], Dt.[CargoHandling],Dt.[Volume],Dt.[Remarks],
			Dt.[ContainerRef], Dt.[HaulageStatus], Dt.[CFSStatus], Dt.[DepotStatus], Dt.[FreightStatus], 
			Dt.[PickUpDropOffCode], Dt.[WagonNo], Dt.[JobNo], Dt.[SubJobNo], 
			Dt.[EmptyInDate], Dt.[EmptyStartDate], Dt.[FullInDate], Dt.[FullInStartDate], Dt.[FullOutDate], Dt.[DeclarationNo],
			Dt.[WeighingDate],Dt.[WeighingPlace],Dt.[CertificateNo],Dt.[VerificationCountry],Dt.[SOLASMethod],
			Dt.[RequiredDate],Dt.[OriginCountry],
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,0 As Selected,
			ISNULL(T.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(V.LookupDescription,'') As VentModeDescription,
			ISNULL(CG.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(CT.LookupDescription,'') As CargoTypeDescription,
			ISNULL(TT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(ST.LookupDescription,'') As SpecialHandlingTypeDescription
	FROM	[Operation].[OrderContainer] Dt
	Left Outer Join TempratureLookup T ON 
		Dt.TemperatureMode = T.LookupID
	Left Outer Join VentLookup V ON 
		Dt.VentMode = V.LookupID
	Left Outer Join ContainerGradeLookup CG ON 
		Dt.ContainerGrade= CG.LookupID
	Left Outer Join CargoTypeLookup CT ON 
		Dt.CargoType = CT.LookupID
	Left Outer Join TrailerTypeLookup TT ON 
		Dt.TrailerType = TT.LookupID
	Left Outer Join SpecialHandlingTypeLookup ST ON 
		Dt.TrailerType = ST.LookupID
	WHERE	Dt.ContainerNo Like '%' + @ContainerNo + '%'

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_SearchOrderContainerList] 
-- ========================================================================================================================================

GO