 

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationItemK2] Record based on [DeclarationItemK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationItemK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT,
    @OrderNo NVARCHAR(50),
    @ContainerKey NVARCHAR(50),
    @CargoKey NVARCHAR(50)
AS 

BEGIN

	SELECT	
		[BranchID],[DeclarationNo],[ItemNo],[OrderNo],[ContainerKey],[CargoKey],[ProductCode],[ProductDescription],[HSCode],[OriginCountryCode],[StatisticalQty],
		[StatisticalUOM],[DeclaredQty],[DeclaredUOM],[ItemAmount],[ItemDescription1],[ItemDescription2],[ExportDutyMethod],[ExportDutyRate],[ExportDutyRateAmount],
		[ExportDutySpecificRate],[ExportDutySpecificRateAmount],[ExportDutyExemptionRatePercent],[ExportDutyExemptionRate],[ExportDutyExemptionRateAmount],
		[ExportDutySpecificExemptionRatePercent],[ExportDutySpecificExemptionRate],[ExportDutySpecificExemptionRateAmount],[AlcoholMethod],[ProofOfSpirit],[VehicleType],
		[VehicleBrand],[VehicleModel],[VehicleEngineNo],[VehicleChassisNo],[VehicleCC],[VehicleYear],[UnitLocalAmount],[TotalUnitLocalAmount],[TotalDutyPayable],
		[ECCNNo],[ItemWeight],[UnitCurrency],[UnitCurrencyExchangeRate],[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn],[FOBValue],[CIFValue],[EXWValue],[CNFValue],
		[CNIValue],[FreightValue],[InsuranceValue],[CIFCValue],ExportDutyExemptionAmount,ExportDutySpecificExemptionAmount
	FROM   [Operation].[DeclarationItemK2]
	WHERE  [BranchID] = @BranchID 
	       AND [DeclarationNo] = @DeclarationNo 
	       AND [ItemNo] = @ItemNo 
	       AND [OrderNo] = @OrderNo 
	       AND [ContainerKey] = @ContainerKey  
	       AND [CargoKey] = @CargoKey  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationItemK2] Records from [DeclarationItemK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationItemK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT			
		[BranchID],[DeclarationNo],[ItemNo],[OrderNo],[ContainerKey],[CargoKey],[ProductCode],[ProductDescription],[HSCode],[OriginCountryCode],[StatisticalQty],
		[StatisticalUOM],[DeclaredQty],[DeclaredUOM],[ItemAmount],[ItemDescription1],[ItemDescription2],[ExportDutyMethod],[ExportDutyRate],[ExportDutyRateAmount],
		[ExportDutySpecificRate],[ExportDutySpecificRateAmount],[ExportDutyExemptionRatePercent],[ExportDutyExemptionRate],[ExportDutyExemptionRateAmount],
		[ExportDutySpecificExemptionRatePercent],[ExportDutySpecificExemptionRate],[ExportDutySpecificExemptionRateAmount],[AlcoholMethod],[ProofOfSpirit],[VehicleType],
		[VehicleBrand],[VehicleModel],[VehicleEngineNo],[VehicleChassisNo],[VehicleCC],[VehicleYear],[UnitLocalAmount],[TotalUnitLocalAmount],[TotalDutyPayable],
		[ECCNNo],[ItemWeight],[UnitCurrency],[UnitCurrencyExchangeRate],[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn],[FOBValue],[CIFValue],[EXWValue],[CNFValue],
		[CNIValue],[FreightValue],[InsuranceValue],[CIFCValue],ExportDutyExemptionAmount,ExportDutySpecificExemptionAmount

	FROM   [Operation].[DeclarationItemK2]
		WHERE  [BranchID] = @BranchID 
	       AND [DeclarationNo] = @DeclarationNo 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemK2List] 
-- ========================================================================================================================================


GO





-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationItemK2] Record Into [DeclarationItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2Insert] 
@BranchID bigint ,
	@DeclarationNo nvarchar(50) ,
	@ItemNo smallint ,
	@OrderNo nvarchar(50) ,
	@ContainerKey nvarchar(50) ,
	@CargoKey nvarchar(50) ,
	@ProductCode nvarchar(20) ,
	@ProductDescription nvarchar(100) ,
	@HSCode nvarchar(20) ,
	@OriginCountryCode varchar(2) ,
	@StatisticalQty decimal(18, 7) ,
	@StatisticalUOM nvarchar(20) ,
	@DeclaredQty decimal(18, 7) ,
	@DeclaredUOM nvarchar(20) ,
	@ItemAmount decimal(18, 7) ,
	@ItemDescription1 nvarchar(50) ,
	@ItemDescription2 nvarchar(50) ,
	@ExportDutyMethod smallint ,
	@ExportDutyRate decimal(18, 3) ,
	@ExportDutyRateAmount decimal(18, 7) ,
	@ExportDutySpecificRate decimal(18, 3) ,
	@ExportDutySpecificRateAmount decimal(18, 7) ,
	@ExportDutyExemptionRatePercent decimal(18, 7) ,
	@ExportDutyExemptionRate decimal(18, 7) ,
	@ExportDutyExemptionRateAmount decimal(18, 7) ,
	@ExportDutySpecificExemptionRatePercent decimal(18, 7) ,
	@ExportDutySpecificExemptionRate decimal(18, 7) ,
	@ExportDutySpecificExemptionRateAmount decimal(18, 7) ,
	@AlcoholMethod decimal(18, 7) ,
	@ProofOfSpirit decimal(18, 7) ,
	@VehicleType smallint ,
	@VehicleBrand nvarchar(50) ,
	@VehicleModel nvarchar(50) ,
	@VehicleEngineNo nvarchar(50) ,
	@VehicleChassisNo nvarchar(50) ,
	@VehicleCC nvarchar(50) ,
	@VehicleYear smallint ,
	@UnitLocalAmount decimal(18, 7) ,
	@TotalUnitLocalAmount decimal(18, 7) ,
	@TotalDutyPayable decimal(18, 7) ,
	@ECCNNo nvarchar(5) ,
	@ItemWeight decimal(18, 7) ,
	@UnitCurrency nvarchar(3) ,
	@UnitCurrencyExchangeRate decimal(18, 3) ,
	@CreatedBy nvarchar(60) ,
	@ModifiedBy nvarchar(60) ,
	@FOBValue decimal(18, 7) ,
	@CIFValue decimal(18, 7) ,
	@EXWValue decimal(18, 7) ,
	@CNFValue decimal(18, 7) ,
	@CNIValue decimal(18, 7) ,
	@FreightValue decimal(18, 7) ,
	@InsuranceValue decimal(18, 7) ,
	@CIFCValue decimal(18, 7),
@ExportDutyExemptionAmount decimal(18,7),  
@ExportDutySpecificExemptionAmount decimal(18,7)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationItemK2] (
		[BranchID],[DeclarationNo],[ItemNo],[OrderNo],[ContainerKey],[CargoKey],[ProductCode],[ProductDescription],[HSCode],[OriginCountryCode],[StatisticalQty],
		[StatisticalUOM],[DeclaredQty],[DeclaredUOM],[ItemAmount],[ItemDescription1],[ItemDescription2],[ExportDutyMethod],[ExportDutyRate],[ExportDutyRateAmount],
		[ExportDutySpecificRate],[ExportDutySpecificRateAmount],[ExportDutyExemptionRatePercent],[ExportDutyExemptionRate],[ExportDutyExemptionRateAmount],
		[ExportDutySpecificExemptionRatePercent],[ExportDutySpecificExemptionRate],[ExportDutySpecificExemptionRateAmount],[AlcoholMethod],[ProofOfSpirit],[VehicleType],
		[VehicleBrand],[VehicleModel],[VehicleEngineNo],[VehicleChassisNo],[VehicleCC],[VehicleYear],[UnitLocalAmount],[TotalUnitLocalAmount],[TotalDutyPayable],
		[ECCNNo],[ItemWeight],[UnitCurrency],[UnitCurrencyExchangeRate],[CreatedBy],[CreatedOn],[FOBValue],[CIFValue],[EXWValue],[CNFValue],
		[CNIValue],[FreightValue],[InsuranceValue],[CIFCValue],ExportDutyExemptionAmount,ExportDutySpecificExemptionAmount)
	SELECT			
		@BranchID,@DeclarationNo,@ItemNo,@OrderNo,@ContainerKey,@CargoKey,@ProductCode,@ProductDescription,@HSCode,@OriginCountryCode,@StatisticalQty,
		@StatisticalUOM,@DeclaredQty,@DeclaredUOM,@ItemAmount,@ItemDescription1,@ItemDescription2,@ExportDutyMethod,@ExportDutyRate,@ExportDutyRateAmount,
		@ExportDutySpecificRate,@ExportDutySpecificRateAmount,@ExportDutyExemptionRatePercent,@ExportDutyExemptionRate,@ExportDutyExemptionRateAmount,
		@ExportDutySpecificExemptionRatePercent,@ExportDutySpecificExemptionRate,@ExportDutySpecificExemptionRateAmount,@AlcoholMethod,@ProofOfSpirit,@VehicleType,
		@VehicleBrand,@VehicleModel,@VehicleEngineNo,@VehicleChassisNo,@VehicleCC,@VehicleYear,@UnitLocalAmount,@TotalUnitLocalAmount,@TotalDutyPayable,
		@ECCNNo,@ItemWeight,@UnitCurrency,@UnitCurrencyExchangeRate,@CreatedBy,GETUTCDATE(),@FOBValue,@CIFValue,@EXWValue,@CNFValue,
		@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,@ExportDutyExemptionAmount,@ExportDutySpecificExemptionAmount

               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemK2Insert]
-- ========================================================================================================================================


GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationItemK2] Record Into [DeclarationItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2Update] 
@BranchID bigint ,
	@DeclarationNo nvarchar(50) ,
	@ItemNo smallint ,
	@OrderNo nvarchar(50) ,
	@ContainerKey nvarchar(50) ,
	@CargoKey nvarchar(50) ,
	@ProductCode nvarchar(20) ,
	@ProductDescription nvarchar(100) ,
	@HSCode nvarchar(20) ,
	@OriginCountryCode varchar(2) ,
	@StatisticalQty decimal(18, 7) ,
	@StatisticalUOM nvarchar(20) ,
	@DeclaredQty decimal(18, 7) ,
	@DeclaredUOM nvarchar(20) ,
	@ItemAmount decimal(18, 7) ,
	@ItemDescription1 nvarchar(50) ,
	@ItemDescription2 nvarchar(50) ,
	@ExportDutyMethod smallint ,
	@ExportDutyRate decimal(18, 3) ,
	@ExportDutyRateAmount decimal(18, 7) ,
	@ExportDutySpecificRate decimal(18, 3) ,
	@ExportDutySpecificRateAmount decimal(18, 7) ,
	@ExportDutyExemptionRatePercent decimal(18, 7) ,
	@ExportDutyExemptionRate decimal(18, 7) ,
	@ExportDutyExemptionRateAmount decimal(18, 7) ,
	@ExportDutySpecificExemptionRatePercent decimal(18, 7) ,
	@ExportDutySpecificExemptionRate decimal(18, 7) ,
	@ExportDutySpecificExemptionRateAmount decimal(18, 7) ,
	@AlcoholMethod decimal(18, 7) ,
	@ProofOfSpirit decimal(18, 7) ,
	@VehicleType smallint ,
	@VehicleBrand nvarchar(50) ,
	@VehicleModel nvarchar(50) ,
	@VehicleEngineNo nvarchar(50) ,
	@VehicleChassisNo nvarchar(50) ,
	@VehicleCC nvarchar(50) ,
	@VehicleYear smallint ,
	@UnitLocalAmount decimal(18, 7) ,
	@TotalUnitLocalAmount decimal(18, 7) ,
	@TotalDutyPayable decimal(18, 7) ,
	@ECCNNo nvarchar(5) ,
	@ItemWeight decimal(18, 7) ,
	@UnitCurrency nvarchar(3) ,
	@UnitCurrencyExchangeRate decimal(18, 3) ,
	@CreatedBy nvarchar(60) ,
	@ModifiedBy nvarchar(60) ,
	@FOBValue decimal(18, 7) ,
	@CIFValue decimal(18, 7) ,
	@EXWValue decimal(18, 7) ,
	@CNFValue decimal(18, 7) ,
	@CNIValue decimal(18, 7) ,
	@FreightValue decimal(18, 7) ,
	@InsuranceValue decimal(18, 7) ,
	@CIFCValue decimal(18, 7),
	@ExportDutyExemptionAmount decimal(18,7),  
	@ExportDutySpecificExemptionAmount decimal(18,7) 
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationItemK2]
	SET		
		OrderNo = @OrderNo, ContainerKey = @ContainerKey, CargoKey = @CargoKey, ProductCode = @ProductCode, ProductDescription = @ProductDescription, HSCode = @HSCode, 
		OriginCountryCode = @OriginCountryCode,StatisticalQty = @StatisticalQty, StatisticalUOM = @StatisticalUOM, DeclaredQty = @DeclaredQty, DeclaredUOM = @DeclaredUOM, 
		ItemAmount = @ItemAmount, ItemDescription1 = @ItemDescription1, ItemDescription2 = @ItemDescription2, ExportDutyMethod = @ExportDutyMethod, ExportDutyRate = @ExportDutyRate, 
		ExportDutyRateAmount = @ExportDutyRateAmount, ExportDutySpecificRate = @ExportDutySpecificRate, ExportDutySpecificRateAmount = @ExportDutySpecificRateAmount, ExportDutyExemptionRatePercent = @ExportDutyExemptionRatePercent, 
		ExportDutyExemptionRate = @ExportDutyExemptionRate, ExportDutyExemptionRateAmount = @ExportDutyExemptionRateAmount, ExportDutySpecificExemptionRatePercent = @ExportDutySpecificExemptionRatePercent, 
		ExportDutySpecificExemptionRate = @ExportDutySpecificExemptionRate, ExportDutySpecificExemptionRateAmount = @ExportDutySpecificExemptionRateAmount, AlcoholMethod = @AlcoholMethod, 
		ProofOfSpirit = @ProofOfSpirit, VehicleType = @VehicleType, VehicleBrand = @VehicleBrand, VehicleModel = @VehicleModel, VehicleEngineNo = @VehicleEngineNo, VehicleChassisNo = @VehicleChassisNo, 
		VehicleCC = @VehicleCC, VehicleYear = @VehicleYear, UnitLocalAmount = @UnitLocalAmount, TotalUnitLocalAmount = @TotalUnitLocalAmount, TotalDutyPayable = @TotalDutyPayable, 
		ECCNNo = @ECCNNo, ItemWeight = @ItemWeight, UnitCurrency = @UnitCurrency, UnitCurrencyExchangeRate = @UnitCurrencyExchangeRate,  
		ModifiedBy = @ModifiedBy, ModifiedOn = GetUtcDate(), FOBValue = @FOBValue, CIFValue = @CIFValue, EXWValue = @EXWValue, CNFValue = @CNFValue, CNIValue = @CNIValue, 
		FreightValue = @FreightValue, InsuranceValue = @InsuranceValue, CIFCValue = @CIFCValue,
		ExportDutyExemptionAmount=@ExportDutyExemptionAmount,ExportDutySpecificExemptionAmount=@ExportDutySpecificExemptionAmount
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
			AND [ItemNo] = @ItemNo
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [CargoKey] = @CargoKey
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemK2Update]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationItemK2] Record Into [DeclarationItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2Save] 
@BranchID bigint ,
	@DeclarationNo nvarchar(50) ,
	@ItemNo smallint ,
	@OrderNo nvarchar(50) ,
	@ContainerKey nvarchar(50) ,
	@CargoKey nvarchar(50) ,
	@ProductCode nvarchar(20) ,
	@ProductDescription nvarchar(100) ,
	@HSCode nvarchar(20) ,
	@OriginCountryCode varchar(2) ,
	@StatisticalQty decimal(18, 7) ,
	@StatisticalUOM nvarchar(20) ,
	@DeclaredQty decimal(18, 7) ,
	@DeclaredUOM nvarchar(20) ,
	@ItemAmount decimal(18, 7) ,
	@ItemDescription1 nvarchar(50) ,
	@ItemDescription2 nvarchar(50) ,
	@ExportDutyMethod smallint ,
	@ExportDutyRate decimal(18, 3) ,
	@ExportDutyRateAmount decimal(18, 7) ,
	@ExportDutySpecificRate decimal(18, 3) ,
	@ExportDutySpecificRateAmount decimal(18, 7) ,
	@ExportDutyExemptionRatePercent decimal(18, 7) ,
	@ExportDutyExemptionRate decimal(18, 7) ,
	@ExportDutyExemptionRateAmount decimal(18, 7) ,
	@ExportDutySpecificExemptionRatePercent decimal(18, 7) ,
	@ExportDutySpecificExemptionRate decimal(18, 7) ,
	@ExportDutySpecificExemptionRateAmount decimal(18, 7) ,
	@AlcoholMethod decimal(18, 7) ,
	@ProofOfSpirit decimal(18, 7) ,
	@VehicleType smallint ,
	@VehicleBrand nvarchar(50) ,
	@VehicleModel nvarchar(50) ,
	@VehicleEngineNo nvarchar(50) ,
	@VehicleChassisNo nvarchar(50) ,
	@VehicleCC nvarchar(50) ,
	@VehicleYear smallint ,
	@UnitLocalAmount decimal(18, 7) ,
	@TotalUnitLocalAmount decimal(18, 7) ,
	@TotalDutyPayable decimal(18, 7) ,
	@ECCNNo nvarchar(5) ,
	@ItemWeight decimal(18, 7) ,
	@UnitCurrency nvarchar(3) ,
	@UnitCurrencyExchangeRate decimal(18, 3) ,
	@CreatedBy nvarchar(60) ,
	@ModifiedBy nvarchar(60) ,
	@FOBValue decimal(18, 7) ,
	@CIFValue decimal(18, 7) ,
	@EXWValue decimal(18, 7) ,
	@CNFValue decimal(18, 7) ,
	@CNIValue decimal(18, 7) ,
	@FreightValue decimal(18, 7) ,
	@InsuranceValue decimal(18, 7) ,
	@CIFCValue decimal(18, 7),
	@ExportDutyExemptionAmount decimal(18,7),  
	@ExportDutySpecificExemptionAmount decimal(18,7)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationItemK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [CargoKey] = @CargoKey)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationItemK2Update] 
		@BranchID,@DeclarationNo,@ItemNo,@OrderNo,@ContainerKey,@CargoKey,@ProductCode,@ProductDescription,@HSCode,@OriginCountryCode,@StatisticalQty,
		@StatisticalUOM,@DeclaredQty,@DeclaredUOM,@ItemAmount,@ItemDescription1,@ItemDescription2,@ExportDutyMethod,@ExportDutyRate,@ExportDutyRateAmount,
		@ExportDutySpecificRate,@ExportDutySpecificRateAmount,@ExportDutyExemptionRatePercent,@ExportDutyExemptionRate,@ExportDutyExemptionRateAmount,
		@ExportDutySpecificExemptionRatePercent,@ExportDutySpecificExemptionRate,@ExportDutySpecificExemptionRateAmount,@AlcoholMethod,@ProofOfSpirit,@VehicleType,
		@VehicleBrand,@VehicleModel,@VehicleEngineNo,@VehicleChassisNo,@VehicleCC,@VehicleYear,@UnitLocalAmount,@TotalUnitLocalAmount,@TotalDutyPayable,
		@ECCNNo,@ItemWeight,@UnitCurrency,@UnitCurrencyExchangeRate,@CreatedBy,@ModifiedBy,@FOBValue,@CIFValue,@EXWValue,@CNFValue,
		@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,@ExportDutyExemptionAmount,@ExportDutySpecificExemptionAmount


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationItemK2Insert] 
		@BranchID,@DeclarationNo,@ItemNo,@OrderNo,@ContainerKey,@CargoKey,@ProductCode,@ProductDescription,@HSCode,@OriginCountryCode,@StatisticalQty,
		@StatisticalUOM,@DeclaredQty,@DeclaredUOM,@ItemAmount,@ItemDescription1,@ItemDescription2,@ExportDutyMethod,@ExportDutyRate,@ExportDutyRateAmount,
		@ExportDutySpecificRate,@ExportDutySpecificRateAmount,@ExportDutyExemptionRatePercent,@ExportDutyExemptionRate,@ExportDutyExemptionRateAmount,
		@ExportDutySpecificExemptionRatePercent,@ExportDutySpecificExemptionRate,@ExportDutySpecificExemptionRateAmount,@AlcoholMethod,@ProofOfSpirit,@VehicleType,
		@VehicleBrand,@VehicleModel,@VehicleEngineNo,@VehicleChassisNo,@VehicleCC,@VehicleYear,@UnitLocalAmount,@TotalUnitLocalAmount,@TotalDutyPayable,
		@ECCNNo,@ItemWeight,@UnitCurrency,@UnitCurrencyExchangeRate,@CreatedBy,@ModifiedBy,@FOBValue,@CIFValue,@EXWValue,@CNFValue,
		@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,@ExportDutyExemptionAmount,@ExportDutySpecificExemptionAmount

	END
	
END
-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationItemK2Save]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationItemK2] Record  based on [DeclarationItemK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationItemK2]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	        
	 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemK2Delete]
-- ========================================================================================================================================


GO
 