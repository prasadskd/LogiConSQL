
-- ========================================================================================================================================
-- START											 [CargoHSCodePortOfLoadingReport]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[CargoHSCodePortOfLoadingReport]') IS NOT NULL
BEGIN 
    DROP PROC [CargoHSCodePortOfLoadingReport]
END 
GO
CREATE PROC [CargoHSCodePortOfLoadingReport](
@BranchID bigint)
As
Begin
	--Declare @BranchID bigint
	--Select @BranchID =1007001

	SELECT Distinct ISNULL(hs.Description,'') As HSCodeDescription ,OrHd.PortOfLoading,Count(cg.HsCode) As HsCodeCount
	FROM Operation.OrderHeader OrHd 
	Left Outer JOIN Operation.OrderContainer Dt ON 
	OrHd.BranchID = Dt.BranchID 
	AND OrHd.OrderNo = Dt.OrderNo 
	Left Outer JOIN Operation.OrderCargo Cg ON 
	Dt.BranchID = Cg.BranchID 
	AND Dt.OrderNo = Cg.OrderNo AND 
	Dt.ContainerKey = Cg.ContainerKey
	Left Outer Join Master.HSCode Hs On 
		Cg.HSCode = Hs.Code
	Where 
	OrHd.BranchID = @BranchID And 
	OrHd.JobType IN (1061,1060)
	Group By Hs.Description, OrHd.PortOfLoading
	Having Count(Cg.HSCode) > 0
End

go


