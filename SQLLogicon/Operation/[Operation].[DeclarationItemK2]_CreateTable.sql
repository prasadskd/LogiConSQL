
 
/****** Object:  Table [Operation].[DeclarationItemK2]    Script Date: 22-01-2017 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Operation].[DeclarationItemK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[ContainerKey] [nvarchar](50) NOT NULL,
	[CargoKey] [nvarchar](50) NOT NULL,
	[ProductCode] [nvarchar](20) NOT NULL,
	[ProductDescription] [nvarchar](100) NOT NULL,
	[HSCode] [nvarchar](20) NOT NULL,
	[OriginCountryCode] [varchar](2) NOT NULL,
	[StatisticalQty] [decimal](18, 7) NOT NULL,
	[StatisticalUOM] [nvarchar](20) NOT NULL,
	[DeclaredQty] [decimal](18, 7) NOT NULL,
	[DeclaredUOM] [nvarchar](20) NOT NULL,
	[ItemAmount] [decimal](18, 7) NOT NULL,
	[ItemDescription1] [nvarchar](50) NULL,
	[ItemDescription2] [nvarchar](50) NULL,
	
	[ExportDutyMethod] [nvarchar](20) NULL,
	[ExportDutyTariffCode] [smallint] NULL,
	[ExportDutyPercentage] [decimal](18, 7) NULL,
	[ExportDutyExcemption] [decimal](18, 7) NULL,
	[ExportDutySpecific] [decimal](18, 7) NULL,
	[ExportDutySpecificExcemption] [decimal](18, 7) NULL,
	
	[GazettedMethod] [nvarchar](20) NULL,
	[GazettedTariffCode] [smallint] NULL,
	[GazettedPercentage] [decimal](18, 7) NULL,
	[GazettedExcemption] [decimal](18, 7) NULL,
	[GazettedSpecific] [decimal](18, 7) NULL,
	[GazettedSpecificExcemption] [decimal](18, 7) NULL,
	[GazettedUnitPrice] [decimal](18, 7) NULL,

	
	[ReplantMethod] [nvarchar](20) NULL,
	[ReplantTariffCode] [smallint] NULL,
	[ReplantPercentage] [decimal](18, 7) NULL,
	[ReplantExcemption] [decimal](18, 7) NULL,
	[ReplantSpecific] [decimal](18, 7) NULL,
	[ReplantSpecificExcemption] [decimal](18, 7) NULL,
	
	[ResearchMethod] [nvarchar](20) NULL,
	[ResearchTariffCode] [smallint] NULL,
	[ResearchPercentage] [decimal](18, 7) NULL,
	[ResearchExcemption] [decimal](18, 7) NULL,
	[ResearchSpecific] [decimal](18, 7) NULL,
	[ResearchSpecificExcemption] [decimal](18, 7) NULL,
	
	[VehicleType] [smallint] NULL,
	[VehicleBrand] [nvarchar](50) NULL,
	[VehicleModel] [nvarchar](50) NULL,
	[VehicleEngineNo] [nvarchar](50) NULL,
	[VehicleChassisNo] [nvarchar](50) NULL,
	[VehicleCC] [nvarchar](50) NULL,
	[VehicleYear] [smallint] NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationItemK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[ItemNo] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC,
	[CargoKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_ProductCode]  DEFAULT ('') FOR [ProductCode]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_ProductDescription]  DEFAULT ('') FOR [ProductDescription]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_HSCode]  DEFAULT ('') FOR [HSCode]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_OriginCountryCode]  DEFAULT ('') FOR [OriginCountryCode]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_StatisticalQty]  DEFAULT ((0)) FOR [StatisticalQty]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_StatisticalUOM]  DEFAULT ('') FOR [StatisticalUOM]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_DeclaredQty]  DEFAULT ((0)) FOR [DeclaredQty]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_DeclaredUOM]  DEFAULT ('') FOR [DeclaredUOM]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_ItemAmount]  DEFAULT ((0)) FOR [ItemAmount]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationItemK2] ADD  CONSTRAINT [DF_DeclarationItemK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


