
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationInvoiceK2] Record based on [DeclarationInvoiceK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @InvoiceNo NVARCHAR(50)
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], 
			[InsuranceAmountPercent], [IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], 
			[InsuranceAmountValue], [InsuranceIncoTerm], [OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], 
			[OthersAmountValue], [CargoClassCode], [CargoDescription1], [CargoDescription2], [PackageQty], [PackingTypeCode], 
			[PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], [Status], 
			FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,InvoiceLocalAmount,
			GoodsReceiveCountry,DestinationCountry,AmountReceived,AmountReceivedExchangeRate,
			AmountReceivedLocalValue,IsSTAPermit,AmountReceivedCurrencyCode
	FROM	[Operation].[DeclarationInvoiceK2]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  
			AND [InvoiceNo] = @InvoiceNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationInvoiceK2] Records from [DeclarationInvoiceK2] table

-- Exec [Operation].[usp_DeclarationInvoiceK2List] 
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], 
			[InsuranceAmountPercent], [IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], 
			[InsuranceAmountValue], [InsuranceIncoTerm], [OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], 
			[OthersAmountValue], [CargoClassCode], [CargoDescription1], [CargoDescription2], [PackageQty], [PackingTypeCode], 
			[PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], [Status], 
			FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],InvoiceLocalAmount,
			GoodsReceiveCountry,DestinationCountry,AmountReceived,AmountReceivedExchangeRate,
			AmountReceivedLocalValue,IsSTAPermit,AmountReceivedCurrencyCode

	FROM   [Operation].[DeclarationInvoiceK2]
		WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceK2List] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationInvoiceK2] Record Into [DeclarationInvoiceK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
	@GoodsReceiveCountry nvarchar(3),
	@DestinationCountry nvarchar(3),
	@AmountReceived Decimal(18,7) NULL,
	@AmountReceivedExchangeRate Decimal(18,7),
	@AmountReceivedLocalValue Decimal(18,7),
	@IsSTAPermit bit,
	@AmountReceivedCurrencyCode nvarchar(3),
	@CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationInvoiceK2] (
			[BranchID], [DeclarationNo], [InvoiceNo], [InvoiceDate], [InvoiceValue], [InvoiceCurrencyCode], [LocalCurrencyCode], [CurrencyExRate], 
			[IncoTerm], [PayCountry], [PortAmountPercent], [PortAmountCurrencyCode], [PortAmountExRate], [PortAmountValue], [FreightAmountPercent], 
			[IsFreightCurrency], [FreightAmountCurrencyCode], [FreightAmountExRate], [FreightAmountValue], [FreightIncoTerm], [InsuranceAmountPercent], 
			[IsInsuranceCurrency], [InsuranceAmountCurrencyCode], [InsuranceAmountExRate], [InsuranceAmountValue], [InsuranceIncoTerm], 
			[OthersAmountPercent], [OthersAmountCurrencyCode], [OthersAmountExRate], [OthersAmountValue], [CargoClassCode], [CargoDescription1], 
			[CargoDescription2], [PackageQty], [PackingTypeCode], [PackingMaterialCode], [GrossWeight], [UOMWeight], [GrossVolume], [UOMVolume], 
			[Status],FOBAmount,CIFAmount,EXWAmount,CNFAmount,CNIAmount,FreightAmount,InsuranceAmount,CIFCAmount,InvoiceLocalAmount,GoodsReceiveCountry,DestinationCountry,
			AmountReceived,AmountReceivedExchangeRate,AmountReceivedLocalValue,IsSTAPermit,AmountReceivedCurrencyCode,
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, 
			@IncoTerm, @PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, 
			@IsFreightCurrency, @FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, 
			@IsInsuranceCurrency, @InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, 
			@OthersAmountPercent, @OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, 
			@CargoDescription2, @PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			Cast(1 as bit), 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,@GoodsReceiveCountry,@DestinationCountry,
			@AmountReceived,@AmountReceivedExchangeRate,@AmountReceivedLocalValue,@IsSTAPermit,@AmountReceivedCurrencyCode,
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationInvoiceK2] Record Into [DeclarationInvoiceK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
	@GoodsReceiveCountry nvarchar(3),
	@DestinationCountry nvarchar(3),
	@AmountReceived Decimal(18,7) NULL,
	@AmountReceivedExchangeRate Decimal(18,7),
	@AmountReceivedLocalValue Decimal(18,7),
	@IsSTAPermit bit,
	@AmountReceivedCurrencyCode nvarchar(3),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationInvoiceK2]
	SET		[InvoiceDate] = @InvoiceDate, [InvoiceValue] = @InvoiceValue, [InvoiceCurrencyCode] = @InvoiceCurrencyCode, 
			[LocalCurrencyCode] = @LocalCurrencyCode, [CurrencyExRate] = @CurrencyExRate, [IncoTerm] = @IncoTerm, [PayCountry] = @PayCountry, 
			[PortAmountPercent] = @PortAmountPercent, [PortAmountCurrencyCode] = @PortAmountCurrencyCode, [PortAmountExRate] = @PortAmountExRate, 
			[PortAmountValue] = @PortAmountValue, [FreightAmountPercent] = @FreightAmountPercent, [IsFreightCurrency] = @IsFreightCurrency, 
			[FreightAmountCurrencyCode] = @FreightAmountCurrencyCode, [FreightAmountExRate] = @FreightAmountExRate, [FreightAmountValue] = @FreightAmountValue, 
			[FreightIncoTerm] = @FreightIncoTerm, [InsuranceAmountPercent] = @InsuranceAmountPercent, [IsInsuranceCurrency] = @IsInsuranceCurrency, 
			[InsuranceAmountCurrencyCode] = @InsuranceAmountCurrencyCode, [InsuranceAmountExRate] = @InsuranceAmountExRate, 
			[InsuranceAmountValue] = @InsuranceAmountValue, [InsuranceIncoTerm] = @InsuranceIncoTerm, 
			[OthersAmountPercent] = @OthersAmountPercent, [OthersAmountCurrencyCode] = @OthersAmountCurrencyCode, 
			[OthersAmountExRate] = @OthersAmountExRate, [OthersAmountValue] = @OthersAmountValue, [CargoClassCode] = @CargoClassCode, 
			[CargoDescription1] = @CargoDescription1, [CargoDescription2] = @CargoDescription2, [PackageQty] = @PackageQty, 
			[PackingTypeCode] = @PackingTypeCode, [PackingMaterialCode] = @PackingMaterialCode, [GrossWeight] = @GrossWeight, 
			[UOMWeight] = @UOMWeight, [GrossVolume] = @GrossVolume, [UOMVolume] = @UOMVolume, 
			FOBAmount=@FOBAmount,CIFAmount=@CIFAmount,EXWAmount=@EXWAmount,
			CNFAmount=@CNFAmount,CNIAmount=@CNIAmount,FreightAmount=@FreightAmount,InsuranceAmount=@InsuranceAmount,
			CIFCAmount=@CIFCAmount,InvoiceLocalAmount=@InvoiceLocalAmount,
			AmountReceived=@AmountReceived,AmountReceivedExchangeRate=@AmountReceivedExchangeRate,
			AmountReceivedLocalValue=@AmountReceivedLocalValue,IsSTAPermit=@IsSTAPermit,AmountReceivedCurrencyCode = @AmountReceivedCurrencyCode,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = Getutcdate(),GoodsReceiveCountry=@GoodsReceiveCountry,DestinationCountry=@DestinationCountry
	WHERE	[BranchID] = @BranchID
			AND [DeclarationNo] = @DeclarationNo
			AND [InvoiceNo] = @InvoiceNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceK2Update]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationInvoiceK2] Record Into [DeclarationInvoiceK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5),
    @InvoiceCurrencyCode nvarchar(3),
    @LocalCurrencyCode nvarchar(3),
    @CurrencyExRate numeric(18, 4),
    @IncoTerm smallint,
    @PayCountry nvarchar(3),
    @PortAmountPercent decimal(18, 4),
    @PortAmountCurrencyCode nvarchar(3),
    @PortAmountExRate decimal(18, 4),
    @PortAmountValue decimal(18, 4),
    @FreightAmountPercent decimal(18, 4),
    @IsFreightCurrency bit,
    @FreightAmountCurrencyCode nvarchar(3),
    @FreightAmountExRate decimal(18, 4),
    @FreightAmountValue decimal(18, 4),
    @FreightIncoTerm smallint,
    @InsuranceAmountPercent decimal(18, 4),
    @IsInsuranceCurrency bit,
    @InsuranceAmountCurrencyCode nvarchar(3),
    @InsuranceAmountExRate decimal(18, 4),
    @InsuranceAmountValue decimal(18, 4),
    @InsuranceIncoTerm smallint,
    @OthersAmountPercent decimal(18, 4),
    @OthersAmountCurrencyCode nvarchar(3),
    @OthersAmountExRate decimal(18, 4),
    @OthersAmountValue decimal(18, 4),
    @CargoClassCode smallint,
    @CargoDescription1 nvarchar(50),
    @CargoDescription2 nvarchar(50),
    @PackageQty int,
    @PackingTypeCode smallint,
    @PackingMaterialCode smallint,
    @GrossWeight decimal(18, 4),
    @UOMWeight nvarchar(20),
    @GrossVolume decimal(18, 4),
    @UOMVolume nvarchar(20),
	@FOBAmount decimal(18,7),
	@CIFAmount decimal(18,7),
	@EXWAmount decimal(18,7),
	@CNFAmount decimal(18,7),
	@CNIAmount decimal(18,7),
	@FreightAmount decimal(18,7),
	@InsuranceAmount decimal(18,7),
	@CIFCAmount decimal(18,7),
	@InvoiceLocalAmount decimal(18,4),
	@GoodsReceiveCountry nvarchar(3),
	@DestinationCountry nvarchar(3),
	@AmountReceived Decimal(18,7) NULL,
	@AmountReceivedExchangeRate Decimal(18,7),
	@AmountReceivedLocalValue Decimal(18,7),
	@IsSTAPermit bit,
	@AmountReceivedCurrencyCode nvarchar(3),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationInvoiceK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [InvoiceNo] = @InvoiceNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationInvoiceK2Update] 
			@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, @IncoTerm, 
			@PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, @IsFreightCurrency, 
			@FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, @IsInsuranceCurrency, 
			@InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, @OthersAmountPercent, 
			@OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, @CargoDescription2, 
			@PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,@GoodsReceiveCountry,@DestinationCountry,
			@AmountReceived,@AmountReceivedExchangeRate,@AmountReceivedLocalValue,@IsSTAPermit,@AmountReceivedCurrencyCode,
			@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationInvoiceK2Insert] 
			@BranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, @IncoTerm, 
			@PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, @IsFreightCurrency, 
			@FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, @IsInsuranceCurrency, 
			@InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, @OthersAmountPercent, 
			@OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, @CargoDescription2, 
			@PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,@GoodsReceiveCountry,@DestinationCountry,
			@AmountReceived,@AmountReceivedExchangeRate,@AmountReceivedLocalValue,@IsSTAPermit,@AmountReceivedCurrencyCode,
			@CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationInvoiceK2Save]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationInvoiceK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationInvoiceK2] Record  based on [DeclarationInvoiceK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationInvoiceK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationInvoiceK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationInvoiceK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @InvoiceNo nvarchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	DELETE FROM	[Operation].[DeclarationInvoiceK2]
	WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       --AND [InvoiceNo] = @InvoiceNo


	--UPDATE	[Operation].[DeclarationInvoiceK2]
	--SET	[Status] = CAST(0 as bit),ModifiedOn = GETUTCDATE(), ModifiedBy = @ModifiedBy
	--WHERE 	[BranchID] = @BranchID
	--       AND [DeclarationNo] = @DeclarationNo
	--       AND [InvoiceNo] = @InvoiceNo

	 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationInvoiceK2Delete]
-- ========================================================================================================================================


GO
 