 
-- ========================================================================================================================================
-- START											[Operation].[usp_GenerateOrderHeaderFromDeclaration]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderHeader] Record Into [OrderHeader] Table.


/*
Modification History:
Modified By		:	sharma
Modified On		:	2014-04-18
Description		:	Removed the MessageID in the OrderHeaderSave and added new parameters (ExtraCargoDescription , StowageCode)

*/

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_GenerateDeclarationFromOrder]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_GenerateDeclarationFromOrder]
END 
GO

CREATE PROC  [Operation].[usp_GenerateDeclarationFromOrder] 
    @BranchID BIGINT,
	@OrderNo nvarchar(35) OUTPUT
AS   
Begin



Declare
	@vBranchID BigInt=0,
    @DeclarationNo nvarchar(50)='',
    @DeclarationDate datetime,
    @DeclarationType smallint=0,
    @OpenDate datetime,
    @ImportDate datetime,
    @TransportMode smallint=0,
    @ShipmentType smallint=0,
    @TransactionType smallint=0,
    @Importer bigint=0,
    @Exporter nvarchar(50)='',
    @ExporterAddress1 nvarchar(50)='',
    @ExporterAddress2 nvarchar(50)='',
    @ExporterCity nvarchar(50)='',
    @ExporterState nvarchar(50)='',
    @ExporterCountry nvarchar(2)='',
    @ExporterOrganizationType smallint=0,
    @ExporterTelNo nvarchar(20)='',
    @SMKCode smallint=0,
    @IsActive bit=1,
    @IsApproved bit=0,
    @ApprovedBy nvarchar(60)='',
    @ApprovedOn datetime,
    @CustomStationCode nvarchar(10)='',
    @ShippingAgent bigint=0,
    @DeclarantID nvarchar(35)='',
    @DeclarantName nvarchar(35)='',
    @DeclarantDesignation nvarchar(35)='',
    @DeclarantNRIC nvarchar(35)='',
    @DeclarantAddress1 nvarchar(35)='',
    @DeclarantAddress2 nvarchar(35)='',
    @DeclarantAddressCity nvarchar(35)='',
    @DeclarantAddressState nvarchar(35)='',
    @DeclarantAddressCountry nvarchar(2)='',
    @DeclarantAddressPostCode nvarchar(10)='',
	@CreatedBy nvarchar(60)='',
    @ModifiedBy nvarchar(60)='',
	@CargoClass smallint=0,
	@CargoDescription nvarchar(150)='',
	@CustomerReferenceNo nvarchar(50)='',
	@ExtraCargoDescription nvarchar(280)='',
	@MarksAndNos nvarchar(350)='',
	@DeclarationShipmentType smallint=0,
	@NewDeclarationNo nvarchar(50) ,
	@vDeclarationType smallint,
	@vForwarderCode BIGINT 


	IF(Select COUNT(0) From Operation.DeclarationHeader
		Where BranchID = @BranchID
		And OrderNo = @OrderNo)>0
	Begin
		Select 
			@vBranchID = BranchID,@DeclarationNo = ISNULL(DeclarationNo,''),@DeclarationDate=DeclarationDate,@DeclarationType=DeclarationType,
			@TransactionType = TransactionType,@IsActive=IsActive,@IsApproved=IsApproved,@ApprovedBy=ApprovedBy,@ApprovedOn=ApprovedOn,@CreatedBy=CreatedBy,
			@DeclarationShipmentType = DeclarationShipmentType
		From 
		Operation.DeclarationHeader
		Where BranchID = @BranchID
		And OrderNo = @OrderNo
	End
	Else
	Begin

		/*Get the Forwarder Code From Order Entry */
		Select @vForwarderCode = FwdAgent
		From Operation.BookingHeader 
		Where BranchID =@BranchID
		And @vForwarderCode = FwdAgent 

		/*Get the BranchID of the Forwarder from the DNeX Subscribers */
		Select @vBranchID = ISNULL(BranchID,0) 
		From Master.Branch A
		INNER JOIN Master.Merchant M ON 
		A.CompanyCode = M.CompanyCode 
		And A.BranchName = M.MerchantName
		Where 
		M.MerchantCode =@vForwarderCode


		Select @vDeclarationType = 
				Case LookupCode 
				When 'IMPORT' Then 'K1'
				WHEN 'EXPORT' Then 'K2'
				Else 'NONE'
				End 
		From Config.Lookup Where LookupCategory ='JobType'

		Select @DeclarationNo ='',@DeclarationType=LookupID From 
		Config.LookUp Where LookupCategory ='DeclarationType' And LookupCode = @vDeclarationType

		Select @TransactionType = LookupID From Config.Lookup Where LookupDescription ='Normal Import (to PCA)' And LookupCategory ='TransactionTypeK1'
		Select @DeclarationShipmentType = LookupID From Config.Lookup Where LookupCode ='F' And LookupCategory ='DeclarationShipmentType'
		


	End

	If @vBranchID = 0
	Begin
		RAISERROR('The forwarding-agent ' + Convert(nvarchar(50),@vForwarderCode) + ' is not registered with DNeX group!',16,1)

	End



	Select	@DeclarationDate = GetUTCDate(),@OpenDate=OrderDate,@ImportDate = OrderDate,@OrderNo = OrderNo, @TransportMode=TransportType,
			@ShipmentType = ShipmentType,@Importer = ConsigneeCode,@Exporter = Convert(nvarchar(50),ShipperCode),
			@ExporterAddress1='', @ExporterAddress2='', @ExporterCity='', @ExporterState='', @ExporterCountry='', 
			@ExporterOrganizationType='', @ExporterTelNo='',@SMKCode=0,@CustomStationCode='',@ShippingAgent=ShippingAgent,
			@IsActive=1,@IsApproved=0,@ApprovedBy='',@ApprovedOn=GetUTCDate(),@CreatedBy=CreatedBy,@ModifiedBy=ModifiedBy,
			@CargoClass = CargoClass,@CargoDescription=CargoDescription,@CustomerReferenceNo =CustomerRef,@ExtraCargoDescription=ExtraCargoDescription,
			@MarksAndNos= '',@DeclarationShipmentType,@NewDeclarationNo='',@DeclarationNo=''
	From Operation.OrderHeader
	Where BranchID =@BranchID
	And OrderNo =@OrderNo
	


	Exec [Operation].[usp_DeclarationHeaderSave]
			@vBranchID, @DeclarationNo, @DeclarationDate, @DeclarationType, @OpenDate, @ImportDate, @OrderNo, @TransportMode, @ShipmentType, 
			@TransactionType, @Importer, @Exporter, @ExporterAddress1, @ExporterAddress2, @ExporterCity, @ExporterState, @ExporterCountry, 
			@ExporterOrganizationType, @ExporterTelNo, @SMKCode, @CustomStationCode, @ShippingAgent, @DeclarantID, @DeclarantName, @DeclarantDesignation, 
			@DeclarantNRIC, @DeclarantAddress1, @DeclarantAddress2, @DeclarantAddressCity, @DeclarantAddressState, @DeclarantAddressCountry, 
			@DeclarantAddressPostCode,@IsActive,@IsApproved,@ApprovedBy,@ApprovedOn,@CreatedBy, @ModifiedBy,@CargoClass,@CargoDescription,
			@CustomerReferenceNo,@ExtraCargoDescription,@MarksAndNos,@DeclarationShipmentType,@NewDeclarationNo = @NewDeclarationNo OUTPUT 

	If LEN(@NewDeclarationNo)=0
	Begin
		RAISERROR('Unable to generate Declaration against Order No. ' + @OrderNo + '',16,1)
	End

	Select @DeclarationNo = @NewDeclarationNo

Declare 
    @InvoiceNo nvarchar(50)='',
    @InvoiceDate datetime,
    @InvoiceValue decimal(18, 5)=NULL,
    @InvoiceCurrencyCode nvarchar(3)=NULL,
    @LocalCurrencyCode nvarchar(3)=NULL,
    @CurrencyExRate numeric(18, 4)=NULL,
    @IncoTerm smallint=NULL,
    @PayCountry nvarchar(3)=NULL,
    @PortAmountPercent decimal(18, 4)=NULL,
    @PortAmountCurrencyCode nvarchar(3)=NULL,
    @PortAmountExRate decimal(18, 4)=NULL,
    @PortAmountValue decimal(18, 4)=NULL,
    @FreightAmountPercent decimal(18, 4)=NULL,
    @IsFreightCurrency bit=NULL,
    @FreightAmountCurrencyCode nvarchar(3)=NULL,
    @FreightAmountExRate decimal(18, 4)=NULL,
    @FreightAmountValue decimal(18, 4)=NULL,
    @FreightIncoTerm smallint=NULL,
    @InsuranceAmountPercent decimal(18, 4)=NULL,
    @IsInsuranceCurrency bit=NULL,
    @InsuranceAmountCurrencyCode nvarchar(3)=NULL,
    @InsuranceAmountExRate decimal(18, 4)=NULL,
    @InsuranceAmountValue decimal(18, 4)=NULL,
    @InsuranceIncoTerm smallint=NULL,
    @OthersAmountPercent decimal(18, 4)=NULL,
    @OthersAmountCurrencyCode nvarchar(3)=NULL,
    @OthersAmountExRate decimal(18, 4)=NULL,
    @OthersAmountValue decimal(18, 4)=NULL,
    @CargoClassCode smallint=NULL,
    @CargoDescription1 nvarchar(50)='',
    @CargoDescription2 nvarchar(50)='',
    @PackageQty int=NULL,
    @PackingTypeCode smallint=NULL,
    @PackingMaterialCode smallint=NULL,
    @GrossWeight decimal(18, 4)=NULL,
    @UOMWeight nvarchar(20)='',
    @GrossVolume decimal(18, 4)=NULL,
    @UOMVolume nvarchar(20)=NULL,
	@FOBAmount decimal(18,7)=NULL,
	@CIFAmount decimal(18,7)=NULL,
	@EXWAmount decimal(18,7)=NULL,
	@CNFAmount decimal(18,7)=NULL,
	@CNIAmount decimal(18,7)=NULL,
	@FreightAmount decimal(18,7)=NULL,
	@InsuranceAmount decimal(18,7)=NULL,
	@CIFCAmount decimal(18,7)=NULL,
	@InvoiceLocalAmount decimal(18,4)=NULL,
    @CreatedBy nvarchar(60)='',
    @ModifiedBy nvarchar(60)=''	



	Exec [Operation].[usp_DeclarationInvoiceSave]
			@vBranchID, @DeclarationNo, @InvoiceNo, @InvoiceDate, @InvoiceValue, @InvoiceCurrencyCode, @LocalCurrencyCode, @CurrencyExRate, @IncoTerm, 
			@PayCountry, @PortAmountPercent, @PortAmountCurrencyCode, @PortAmountExRate, @PortAmountValue, @FreightAmountPercent, @IsFreightCurrency, 
			@FreightAmountCurrencyCode, @FreightAmountExRate, @FreightAmountValue, @FreightIncoTerm, @InsuranceAmountPercent, @IsInsuranceCurrency, 
			@InsuranceAmountCurrencyCode, @InsuranceAmountExRate, @InsuranceAmountValue, @InsuranceIncoTerm, @OthersAmountPercent, 
			@OthersAmountCurrencyCode, @OthersAmountExRate, @OthersAmountValue, @CargoClassCode, @CargoDescription1, @CargoDescription2, 
			@PackageQty, @PackingTypeCode, @PackingMaterialCode, @GrossWeight, @UOMWeight, @GrossVolume, @UOMVolume, 
			@FOBAmount,@CIFAmount,@EXWAmount,@CNFAmount,@CNIAmount,@FreightAmount,@InsuranceAmount,@CIFCAmount,@InvoiceLocalAmount,
			@CreatedBy, @ModifiedBy 


END 