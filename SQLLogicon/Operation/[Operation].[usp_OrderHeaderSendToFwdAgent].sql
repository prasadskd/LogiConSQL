

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderSendToFwdAgent]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderSendToFwdAgent]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderSendToFwdAgent] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderSendToFwdAgent] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN 
	
Declare @WebOrderStatusNONE smallint=0;
Declare @WebOrderStatusSentToAgent smallint=0;
Declare @WebOrderStatusSentToAgentDescription nvarchar(100);
Declare @ActivityParty smallint=0;


	Select @ActivityParty= LookupID From Config.Lookup 
	Where LookupCategory='OrderActivityParty' And LookupCode='OA-TR'
	
	Select @WebOrderStatusNONE = LookupID From Config.Lookup 
	Where LookupCategory='WebOrderStatus' And LookupCode='WEB-NONE'

	Select @WebOrderStatusSentToAgent = LookupID,@WebOrderStatusSentToAgentDescription = LookupDescription From Config.Lookup 
	Where LookupCategory='WebOrderStatus' And LookupCode='WEB-SFA'
	

	UPDATE	[Operation].[OrderHeader]
	SET	WebOrderStatus = @WebOrderStatusSentToAgent
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
		   And WebOrderStatus = @WebOrderStatusNONE

 	Insert Into Operation.OrderActivity
	Select @BranchID,@OrderNo,@WebOrderStatusSentToAgentDescription,GETUTCDATE(),@ActivityParty,@ModifiedBy,GETUTCDATE()


SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderDelete]
-- ========================================================================================================================================

GO
 