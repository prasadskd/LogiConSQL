


-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSelect] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSelect]
	@BranchID BIGINT, 
    @VesselScheduleID BIGINT
AS 

BEGIN

	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	ISNULL(Vsl.VesselName,'') As VesselName,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(LP.PortName,'') As LoadingPortName,
	ISNULL(DiP.PortName,'') As DischargePortName,
	ISNULL(DeP.PortName,'') As DestinationPortName,
	ISNULL(Ter.MerchantName,'') As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE  VS.BranchID = ISNULL(@BranchID,VS.BranchID)
		AND [VesselScheduleID] = @VesselScheduleID  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [VesselSchedule] Records from [VesselSchedule] table

/*
Modification History:

Modification By	: Sharma
Modification On	: 2017-04-11
Modification	: Removed the BranchID in the where clause.


*/



-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleList] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleList] 
	@BranchID BIGINT
AS 
BEGIN


	/* TODO: Filter only with those vesselSchedules who's Manifests are closed.  	*/
	;with JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	Vsl.VesselName , Vsl.CallSignNo,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(LP.PortName,'') As LoadingPortName,
	ISNULL(DiP.PortName,'') As DischargePortName,
	ISNULL(DeP.PortName,'') As DestinationPortName,
	ISNULL(Ter.MerchantName,'') As TerminalName
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	--WHERE VS.BranchID = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleList] 
-- ========================================================================================================================================

GO





-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselSchedulePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderText] Records from [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselSchedulePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselSchedulePageView] 
END 
GO
CREATE PROC [Operation].[usp_VesselSchedulePageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	Vsl.VesselName ,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(LP.PortName,'') As LoadingPortName,
	ISNULL(DiP.PortName,'') As DischargePortName,
	ISNULL(DeP.PortName,'') As DestinationPortName,
	ISNULL(Ter.MerchantName,'') As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE  VS.BranchID = @BranchID  
	        
	ORDER BY VesselScheduleID Desc
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselSchedulePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleRecordCount] 
	@BranchID BIGINT 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[VesselSchedule]
	WHERE  [BranchID] = @BranchID  
	        

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderText] Record based on [OrderText] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleAutoCompleteSearch] 
    @BranchID BIGINT,
    @VoyageNo NVARCHAR(50)
AS 

BEGIN

	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT VS.BranchID, VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard] ,vs.[AgentCode],VS.[JobType], 
	VS.[CustomStationCode],VS.[PlaceOfArrival],VS.[EntryPoint],
	VS.[LoadingPort], VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,VS.PSACode,VS.PSAName,VS.PSAAddress,
	Vsl.VesselName ,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(LP.PortName,'') As LoadingPortName,
	ISNULL(DiP.PortName,'') As DischargePortName,
	ISNULL(DeP.PortName,'') As DestinationPortName,
	ISNULL(Ter.MerchantName,'') As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(nvarchar(20),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE  Vs.[BranchID] = @BranchID  
	       AND VS.[VoyageNoInWard] LIKE '%' +  @VoyageNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [VesselSchedule] Record Into [VesselSchedule] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_VesselScheduleInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleInsert] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleInsert] 
    @BranchID BIGINT,
	@VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNoInWard nvarchar(20),
	@VoyageNoOutWard nvarchar(20),
	@AgentCode nvarchar(10),
	@JobType smallint,
	@CustomStationCode nvarchar(50),
	@PlaceOfArrival nvarchar(50),
	@EntryPoint nvarchar(50),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @DestinationPort nvarchar(10),
    @Terminal nvarchar(10),
    @ETA datetime,
    @ETD datetime,
    @ActualETA datetime,
    @ActualETD datetime,
    @ImpAvaliableDate datetime,
    @ExpAvailableDate datetime,
    @ClosingDate datetime,
    @ClosingDateRF datetime,
    @YardCutOffDate datetime,
    @YardCutOffDateRF datetime,
    @ImportStorageStartDate datetime,
    @IsRevised bit,
    @ShipCallNo nvarchar(25),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@PSACode nvarchar(50),
	@PSAName nvarchar(50),
	@PSAAddress nvarchar(255)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[VesselSchedule] (
			BranchID,[VesselID], [VoyageNoInWard],[VoyageNoOutWard], [AgentCode],[JobType],[CustomStationCode],PlaceOfArrival,EntryPoint,
			[LoadingPort], [DischargePort], [DestinationPort], [Terminal], [ETA], [ETD], [ActualETA], 
			[ActualETD], [ImpAvaliableDate], [ExpAvailableDate], [ClosingDate], [ClosingDateRF], [YardCutOffDate], [YardCutOffDateRF], 
			[ImportStorageStartDate], [IsRevised], [ShipCallNo], [CreatedBy], [CreatedOn],PSACode,PSAName,PSAAddress)
	SELECT	@BranchID,@VesselID, @VoyageNoInWard,@VoyageNoOutWard,@AgentCode,@JobType,@CustomStationCode,@PlaceOfArrival,@EntryPoint,
			@LoadingPort, @DischargePort, @DestinationPort, @Terminal, @ETA, @ETD, @ActualETA, 
			@ActualETD, @ImpAvaliableDate, @ExpAvailableDate, @ClosingDate, @ClosingDateRF, @YardCutOffDate, @YardCutOffDateRF, 
			@ImportStorageStartDate, @IsRevised, @ShipCallNo, @CreatedBy, GETUTCDATE(),@PSACode,@PSAName,@PSAAddress
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [VesselSchedule] Record Into [VesselSchedule] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_VesselScheduleUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleUpdate] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleUpdate] 
    @BranchID BIGINT,
	@VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNoInWard nvarchar(20),
	@VoyageNoOutWard nvarchar(20),
	@AgentCode nvarchar(10),
	@JobType smallint,
	@CustomStationCode nvarchar(50),
	@PlaceOfArrival nvarchar(50),
	@EntryPoint nvarchar(50),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @DestinationPort nvarchar(10),
    @Terminal nvarchar(10),
    @ETA datetime,
    @ETD datetime,
    @ActualETA datetime,
    @ActualETD datetime,
    @ImpAvaliableDate datetime,
    @ExpAvailableDate datetime,
    @ClosingDate datetime,
    @ClosingDateRF datetime,
    @YardCutOffDate datetime,
    @YardCutOffDateRF datetime,
    @ImportStorageStartDate datetime,
    @IsRevised bit,
    @ShipCallNo nvarchar(25),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@PSACode nvarchar(50),
	@PSAName nvarchar(50),
	@PSAAddress nvarchar(255)
AS 
 
	
BEGIN

	UPDATE [Operation].[VesselSchedule]
	SET     [VesselID] = @VesselID, [VoyageNoInWard] = @VoyageNoInWard,[VoyageNoOutWard]=@VoyageNoOutWard,[AgentCode]=@AgentCode,[JobType]=@JobType, 
			[CustomStationCode] = @CustomStationCode,PlaceOfArrival= @PlaceOfArrival,EntryPoint=@EntryPoint,
			[LoadingPort] = @LoadingPort, [DischargePort] = @DischargePort, 
			[DestinationPort] = @DestinationPort, [Terminal] = @Terminal, [ETA] = @ETA, [ETD] = @ETD, [ActualETA] = @ActualETA, 
			[ActualETD] = @ActualETD, [ImpAvaliableDate] = @ImpAvaliableDate, [ExpAvailableDate] = @ExpAvailableDate, 
			[ClosingDate] = @ClosingDate, [ClosingDateRF] = @ClosingDateRF, [YardCutOffDate] = @YardCutOffDate, [YardCutOffDateRF] = @YardCutOffDateRF, 
			[ImportStorageStartDate] = @ImportStorageStartDate, [IsRevised] = @IsRevised, [ShipCallNo] = @ShipCallNo, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),PSACode=@PSACode,PSAName=@PSAName,PSAAddress=@PSAAddress
	WHERE  
	 BranchID = @BranchID
	AND [VesselScheduleID] = @VesselScheduleID
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [VesselSchedule] Record Into [VesselSchedule] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_VesselScheduleSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSave] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSave] 
    @BranchID BIGINT,
	@VesselScheduleID bigint,
    @VesselID nvarchar(20),
    @VoyageNoInWard nvarchar(20),
	@VoyageNoOutWard nvarchar(20),
	@AgentCode nvarchar(10),
	@JobType smallint,
	@CustomStationCode nvarchar(50),
	@PlaceOfArrival nvarchar(50),
	@EntryPoint nvarchar(50),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @DestinationPort nvarchar(10),
    @Terminal nvarchar(10),
    @ETA datetime,
    @ETD datetime,
    @ActualETA datetime,
    @ActualETD datetime,
    @ImpAvaliableDate datetime,
    @ExpAvailableDate datetime,
    @ClosingDate datetime,
    @ClosingDateRF datetime,
    @YardCutOffDate datetime,
    @YardCutOffDateRF datetime,
    @ImportStorageStartDate datetime,
    @IsRevised bit,
    @ShipCallNo nvarchar(25),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@PSACode nvarchar(50),
	@PSAName nvarchar(50),
	@PSAAddress nvarchar(255)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[VesselSchedule] 
		WHERE 	[VesselScheduleID] = @VesselScheduleID)>0
	BEGIN
	    Exec [Operation].[usp_VesselScheduleUpdate] 
			@BranchID,@VesselScheduleID, @VesselID, @VoyageNoInWard,@VoyageNoOutWard,@AgentCode,@JobType,@CustomStationCode,@PlaceOfArrival,@EntryPoint, 
			@LoadingPort, @DischargePort, @DestinationPort, @Terminal, @ETA, @ETD, @ActualETA, 
			@ActualETD, @ImpAvaliableDate, @ExpAvailableDate, @ClosingDate, @ClosingDateRF, @YardCutOffDate, @YardCutOffDateRF, 
			@ImportStorageStartDate, @IsRevised, @ShipCallNo, @CreatedBy, @ModifiedBy,@PSACode,@PSAName,@PSAAddress


	END
	ELSE
	BEGIN


	    Exec [Operation].[usp_VesselScheduleInsert] 
			@BranchID,@VesselScheduleID, @VesselID, @VoyageNoInWard,@VoyageNoOutWard,@AgentCode,@JobType,@CustomStationCode,@PlaceOfArrival,@EntryPoint, 
			@LoadingPort, @DischargePort, @DestinationPort, @Terminal, @ETA, @ETD, @ActualETA, 
			@ActualETD, @ImpAvaliableDate, @ExpAvailableDate, @ClosingDate, @ClosingDateRF, @YardCutOffDate, @YardCutOffDateRF, 
			@ImportStorageStartDate, @IsRevised, @ShipCallNo, @CreatedBy, @ModifiedBy,@PSACode,@PSAName,@PSAAddress

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[VesselScheduleSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [VesselSchedule] Record  based on [VesselSchedule]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_VesselScheduleDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleDelete] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleDelete] 
    @BranchID BIGINT,
	@VesselScheduleID bigint
AS 

	
BEGIN

 
	DELETE
	FROM   [Operation].[VesselSchedule]
	WHERE  
	BranchID = @BranchID
	AND [VesselScheduleID] = @VesselScheduleID
 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleDelete]
-- ========================================================================================================================================

GO
 
