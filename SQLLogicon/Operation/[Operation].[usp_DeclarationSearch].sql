
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationHeader] Record based on [DeclarationHeader] table

-- Exec [Operation].[usp_DeclarationSearch] 1001001,'DLH','ASDF','','','',NULL,NULL,9101

-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSearch] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSearch]
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
	@ManifestNo nvarchar(50),
	@OceanBLNo nvarchar(50),
	@HouseBLNo nvarchar(50),
	@VoyageNo nvarchar(20),
	@DateFrom datetime =NULL,
	@DateTo DateTime =NULL,
	@DeclarationType smallint=0

AS 

BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''DeclarationType''),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''TransportMode''),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ShipmentType''),
			TransactionTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory = ''TransactionTypeK1'')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter], Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,
						ISNULL(LP.PortName,'''') As LoadingPortName,
						ISNULL(DP.PortName,'''') As DischargePortName,
						ISNULL(TP.PortName,'''') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'''') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'''') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'''') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As LocalTraderName
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = DT.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = DT.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	WHERE   Hd.[BranchID] = ' +  Convert(varchar(20),@BranchID)  

	if (@DeclarationType > 0) set @sql = @sql + ' AND  Hd.[DeclarationType]  =' + Convert(varchar(10),@DeclarationType)
	if (len(rtrim(@DeclarationNo)) > 0) set @sql = @sql + ' AND  Hd.[DeclarationNo] LIKE ''%' + @DeclarationNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And Sh.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@OceanBLNo)) > 0) set @sql = @sql + ' And Sh.OceanBLNo LIKE ''%' + @OceanBLNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And Sh.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''

	if @DeclarationType = 9101
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoInWard LIKE ''%' + @VoyageNo  + '%'''
	else if @DeclarationType = 9102
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoOutWard LIKE ''%' + @VoyageNo  + '%'''

	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ImportDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Hd.ImportDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ImportDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Hd.ImportDate)'

	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSearch]
-- ========================================================================================================================================


GO




IF OBJECT_ID('[Operation].[usp_DeclarationK2Search]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationK2Search] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationK2Search] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
	@ManifestNo nvarchar(50),
	@OceanBLNo nvarchar(50),
	@HouseBLNo nvarchar(50),
	@VoyageNo nvarchar(20),
	@DateFrom datetime =NULL,
	@DateTo DateTime =NULL,
	@DeclarationType smallint=0

AS 

BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''DeclarationType''),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''TransportMode''),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ShipmentType''),
			TransactionTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory = ''TransactionTypeK1'')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ExportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter], Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,
						ISNULL(LP.PortName,'''') As LoadingPortName,
						ISNULL(DP.PortName,'''') As DischargePortName,
						ISNULL(TP.PortName,'''') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'''') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'''') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'''') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As LocalTraderName
	FROM	[Operation].[DeclarationHeaderK2] Hd
	Left Outer Join Operation.DeclarationShipmentK2 Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = DT.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = DT.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	WHERE   Hd.[BranchID] = ' +  Convert(varchar(20),@BranchID)  

	if (@DeclarationType > 0) set @sql = @sql + ' AND  Hd.[DeclarationType]  =' + Convert(varchar(10),@DeclarationType)
	if (len(rtrim(@DeclarationNo)) > 0) set @sql = @sql + ' AND  Hd.[DeclarationNo] LIKE ''%' + @DeclarationNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And Sh.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@OceanBLNo)) > 0) set @sql = @sql + ' And Sh.OceanBLNo LIKE ''%' + @OceanBLNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And Sh.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''

	if @DeclarationType = 9101
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoInWard LIKE ''%' + @VoyageNo  + '%'''
	else if @DeclarationType = 9102
	 if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Vs.VoyageNoOutWard LIKE ''%' + @VoyageNo  + '%'''

	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ExportDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Hd.ExportDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ExportDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Hd.ExportDate)'

	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationK2Search]
-- ========================================================================================================================================


GO

