/****** Object:  Table [Operation].[DeclarationShipmentK2]    Script Date: 30-01-2017 18:33:06 ******/
DROP TABLE [Operation].[DeclarationShipmentK2]
GO
/****** Object:  Table [Operation].[DeclarationShipmentK2]    Script Date: 22-01-2017 15:11:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationShipmentK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[ManifestNo] [nvarchar](50) NOT NULL,
	[VesselScheduleID] [bigint] NOT NULL,
	[SCNNo] [nvarchar](50) NOT NULL,
	[VesselID] [nvarchar](20) NOT NULL,
	[VesselName] [nvarchar](100) NOT NULL,
	[VoyageNo] [nvarchar](10) NOT NULL,
	[LoadingPort] [nvarchar](10) NOT NULL,
	[DischargePort] [nvarchar](10) NOT NULL,
	[TranshipmentPort] [nvarchar](10) NOT NULL,
	[PortOperator] [bigint] NOT NULL,
	[OceanBLNo] [nvarchar](100) NOT NULL,
	[HouseBLNo] [nvarchar](100) NOT NULL,
	[ETADate] [datetime] NULL,
	[WarehouseNo] [nvarchar](20) NOT NULL,
	[WagonNo] [nvarchar](35) NULL,
	[VehicleNo1] [nvarchar](35) NULL,
	[VehicleNo2] [nvarchar](35) NULL,
	[FlightNo] [nvarchar](35) NULL,
	[ARNNo] [nvarchar](35) NULL,
	[MasterAWBNo] [nvarchar](35) NULL,
	[HouseAWBNo] [nvarchar](35) NULL,
	[AIRCOLoadNo] [nvarchar](35) NULL,
	[JKNo] [nvarchar](35) NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[PlaceOfExport] [nvarchar](100) NULL,
	[OriginCountry] [nvarchar](3) NULL,
	[DestinationCountry] [nvarchar](3) NULL,



 CONSTRAINT [PK_DeclarationShipmentK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[ManifestNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_VesselScheduleID]  DEFAULT ((0)) FOR [VesselScheduleID]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_SCNNo]  DEFAULT ('') FOR [SCNNo]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_VesselID]  DEFAULT ('') FOR [VesselID]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_VesselName]  DEFAULT ('') FOR [VesselName]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_VoyageNo]  DEFAULT ('') FOR [VoyageNo]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_LoadingPort]  DEFAULT ('') FOR [LoadingPort]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_DischargePort]  DEFAULT ('') FOR [DischargePort]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_TranshipmentPort]  DEFAULT ('') FOR [TranshipmentPort]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_PortOperator]  DEFAULT ((0)) FOR [PortOperator]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_OceanBLNo]  DEFAULT ('') FOR [OceanBLNo]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_HouseBLNo]  DEFAULT ('') FOR [HouseBLNo]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationShipmentK2] ADD  CONSTRAINT [DF_DeclarationShipmentK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


