

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationDocuments] Record based on [DeclarationDocuments] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsSelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsSelect] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], 
			[DocDate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationDocuments]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  
			AND [ItemNo] = @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationDocuments] Records from [DeclarationDocuments] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], 
			[DocDate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Operation].[DeclarationDocuments]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsList] 
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationDocuments] Record Into [DeclarationDocuments] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationDocuments] (
			[BranchID], [DeclarationNo], [ItemNo], [SupportingDocumentType], [CustomStationCode], [ReferenceNo], [DocDateType], [DocDate], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationDocuments] Record Into [DeclarationDocuments] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationDocuments]
	SET    [SupportingDocumentType] = @SupportingDocumentType, [CustomStationCode] = @CustomStationCode, [ReferenceNo] = @ReferenceNo, [DocDateType] = @DocDateType, 
			[DocDate] = @DocDate, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationDocuments] Record Into [DeclarationDocuments] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsSave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SupportingDocumentType smallint,
    @CustomStationCode nvarchar(10),
    @ReferenceNo nvarchar(50),
    @DocDateType nvarchar(20),
    @DocDate datetime,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationDocuments] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationDocumentsUpdate] 
		@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationDocumentsInsert] 
		@BranchID, @DeclarationNo, @ItemNo, @SupportingDocumentType, @CustomStationCode, @ReferenceNo, @DocDateType, @DocDate, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationDocumentsSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationDocumentsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationDocuments] Record  based on [DeclarationDocuments]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationDocumentsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationDocumentsDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationDocumentsDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50) 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationDocuments]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	      
	 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationDocumentsDelete]
-- ========================================================================================================================================

GO
 