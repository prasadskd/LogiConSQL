

/****** Object:  Table [Operation].[DeclarationDocumentsK2]    Script Date: 22-01-2017 15:27:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationDocumentsK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[SupportingDocumentType] [smallint] NOT NULL,
	[CustomStationCode] [nvarchar](10) NOT NULL,
	[ReferenceNo] [nvarchar](50) NOT NULL,
	[DocDateType] [nvarchar](20) NOT NULL,
	[DocDate] [datetime] NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DeclarationDocumentsK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC,
	[ItemNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_OGABranchCode]  DEFAULT ((0)) FOR [SupportingDocumentType]
GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_CustomStationCode]  DEFAULT ('') FOR [CustomStationCode]
GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_ReferenceNo]  DEFAULT ('') FOR [ReferenceNo]
GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_DocDateType]  DEFAULT ('') FOR [DocDateType]
GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationDocumentsK2] ADD  CONSTRAINT [DF_DeclarationDocumentsK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


