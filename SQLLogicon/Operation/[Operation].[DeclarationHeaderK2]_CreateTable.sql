/****** Object:  Table [Operation].[DeclarationHeaderK2]    Script Date: 01/22/2017 12:55:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationHeaderK2](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[DeclarationDate] [datetime] NOT NULL,
	[DeclarationType] [smallint] NOT NULL,
	[OpenDate] [datetime] NOT NULL,
	[ExportDate] [datetime] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[TransportMode] [smallint] NOT NULL,
	[ShipmentType] [smallint] NOT NULL,
	[TransactionType] [smallint] NOT NULL,
	[Importer] [bigint] NOT NULL,
	[Exporter] [nvarchar](50) NOT NULL,
	[ExporterAddress1] [nvarchar](50) NULL,
	[ExporterAddress2] [nvarchar](50) NULL,
	[ExporterCity] [nvarchar](50) NULL,
	[ExporterState] [nvarchar](50) NULL,
	[ExporterCountry] [nvarchar](2) NULL,
	[ExporterOrganizationType] [smallint] NULL,
	[ExporterTelNo] [nvarchar](20) NULL,
	[SMKCode] [smallint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](60) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsApproved] [bit] NOT NULL,
	[ApprovedBy] [nvarchar](60) NULL,
	[ApprovedOn] [datetime] NULL,
	[CustomStationCode] [nvarchar](10) NULL,
	[ShippingAgent] [bigint] NULL,
	[DeclarantID] [nvarchar](35) NULL,
	[DeclarantName] [nvarchar](35) NULL,
	[DeclarantDesignation] [nvarchar](35) NULL,
	[DeclarantNRIC] [nvarchar](35) NULL,
	[DeclarantAddress1] [nvarchar](35) NULL,
	[DeclarantAddress2] [nvarchar](35) NULL,
	[DeclarantAddressCity] [nvarchar](35) NULL,
	[DeclarantAddressState] [nvarchar](35) NULL,
	[DeclarantAddressCountry] [nvarchar](2) NULL,
	[DeclarantAddressPostCode] [nvarchar](10) NULL,
 CONSTRAINT [PK_DeclarationHeaderK2] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_DeclarationDate]  DEFAULT (getutcdate()) FOR [DeclarationDate]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_DeclarationType]  DEFAULT ((0)) FOR [DeclarationType]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_OpenDate]  DEFAULT (getutcdate()) FOR [OpenDate]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_ExportDate]  DEFAULT (getutcdate()) FOR [ExportDate]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_OrderNo]  DEFAULT ('') FOR [OrderNo]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_TransportMode]  DEFAULT ((0)) FOR [TransportMode]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_ShipmentType]  DEFAULT ((0)) FOR [ShipmentType]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_TransactionType]  DEFAULT ((0)) FOR [TransactionType]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_LocalTrader]  DEFAULT ((0)) FOR [Importer]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_OverseasTrader]  DEFAULT ('') FOR [Exporter]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_SMKCode]  DEFAULT ((0)) FOR [SMKCode]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [Operation].[DeclarationHeaderK2] ADD  CONSTRAINT [DF_DeclarationHeaderK2_IsApproved]  DEFAULT ((0)) FOR [IsApproved]
GO


