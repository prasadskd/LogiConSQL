 

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [DeclarationItem] Record based on [DeclarationItem] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationItemSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemSelect] 
END
GO
CREATE PROC [Operation].[usp_DeclarationItemSelect] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 
 

BEGIN

	SELECT	Dt.[BranchID], Dt.[DeclarationNo], Dt.[ItemNo], Dt.[OrderNo], Dt.[ContainerKey], Dt.[CargoKey], Dt.[ProductCode], Dt.[ProductDescription], Dt.[HSCode], 
			Dt.[OriginCountryCode], Dt.[StatisticalQty], Dt.[StatisticalUOM], Dt.[DeclaredQty], Dt.[DeclaredUOM], Dt.[ItemAmount], Dt.[ItemDescription1], Dt.[ItemDescription2], 
			Dt.[ImportDutyMethod], Dt.[ImportDutyRate], Dt.[ImportDutyRateAmount], Dt.[ImportDutySpecificRate], Dt.[ImportDutySpecificRateAmount], Dt.[ImportGST], 
			Dt.[ImportGSTAmount], Dt.[ImportExciseMethod], Dt.[ImportExciseRate], Dt.[ImportExciseRateAmount], Dt.[ImportExciseSpecificRate], Dt.[ImportExciseSpecificAmount], 
			Dt.[SalesTaxMethod], Dt.[SalesTaxTariffCode], Dt.[SalesTaxPercentage], Dt.[SalesTaxExcemption], Dt.[SalesTaxSpecific], Dt.[SalesTaxSpecificExcemption], 
			Dt.[AntiDumpingMethod], Dt.[AntiDumpingTariffCode], Dt.[AntiDumpingPercentage], Dt.[AntiDumpingExcemption], Dt.[AntiDumpingSpecific], Dt.[AntiDumpingSpecificExcemption], 
			Dt.[AlcoholMethod], Dt.[ProofOfSpirit], Dt.[VehicleType], Dt.[VehicleBrand], Dt.[VehicleModel], Dt.[VehicleEngineNo], Dt.[VehicleChassisNo], Dt.[VehicleCC], Dt.[VehicleYear], 
			Dt.[UnitLocalAmount], Dt.[TotalUnitLocalAmount], Dt.[TotalDutyPayable], Dt.[TotalGSTPayable], Dt.[ECCNNo], Dt.[ItemWeight], Dt.[UnitCurrency], Dt.[UnitCurrencyExchangeRate], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			Dt.FOBValue,Dt.CIFValue,Dt.EXWValue,Dt.CNFValue,Dt.CNIValue,Dt.FreightValue,Dt.InsuranceValue,Dt.CIFCValue,
			Dt.ImportDutyExemptionRatePercent,
			Dt.ImportDutyExemptionRate,
			Dt.ImportDutyExemptionRateAmount,
			Dt.ImportDutyExemptionAmount,
			Dt.ImportDutySpecificExemptionRatePercent,
			Dt.ImportDutySpecificExemptionRate,
			Dt.ImportDutySpecificExemptionRateAmount,
			Dt.ImportDutySpecificExemptionAmount,
			Dt.ImportGSTExemptionPercent,
			Dt.ImportGSTExemptionRate,
			Dt.ImportGSTExemptionRateAmount,
			Dt.ImportGSTExemptionAmount,
			Dt.ImportExciseExemptionRatePercent,
			Dt.ImportExciseExemptionRate,
			Dt.ImportExciseExemptionRateAmount,
			Dt.ImportExciseExemptionAmount,
			Dt.ImportExciseExemptionSpecificRatePercent,
			Dt.ImportExciseExemptionSpecificRate,
			Dt.ImportExciseExemptionSpecificRateAmount,
			Dt.ImportExciseExemptionSpecificAmount,
			Hs.OldPDK, Hs.OldAHTN
	FROM	[Operation].[DeclarationItem] Dt
	Left Outer Join [Master].[HSCode] Hs ON 
		Dt.HSCode = Hs.TariffCode
	WHERE	[BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  
	       AND [OrderNo] = @OrderNo 
	       AND [ContainerKey] = @ContainerKey 
	       AND [CargoKey] = @CargoKey 

END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [DeclarationItem] Records from [DeclarationItem] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationItemList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemList] 
END
GO
CREATE PROC [Operation].[usp_DeclarationItemList] 
@BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
 
BEGIN
	SELECT	Dt.[BranchID], Dt.[DeclarationNo], Dt.[ItemNo], Dt.[OrderNo], Dt.[ContainerKey], Dt.[CargoKey], Dt.[ProductCode], Dt.[ProductDescription], Dt.[HSCode], 
			Dt.[OriginCountryCode], Dt.[StatisticalQty], Dt.[StatisticalUOM], Dt.[DeclaredQty], Dt.[DeclaredUOM], Dt.[ItemAmount], Dt.[ItemDescription1], Dt.[ItemDescription2], 
			Dt.[ImportDutyMethod], Dt.[ImportDutyRate], Dt.[ImportDutyRateAmount], Dt.[ImportDutySpecificRate], Dt.[ImportDutySpecificRateAmount], Dt.[ImportGST], 
			Dt.[ImportGSTAmount], Dt.[ImportExciseMethod], Dt.[ImportExciseRate], Dt.[ImportExciseRateAmount], Dt.[ImportExciseSpecificRate], Dt.[ImportExciseSpecificAmount], 
			Dt.[SalesTaxMethod], Dt.[SalesTaxTariffCode], Dt.[SalesTaxPercentage], Dt.[SalesTaxExcemption], Dt.[SalesTaxSpecific], Dt.[SalesTaxSpecificExcemption], 
			Dt.[AntiDumpingMethod], Dt.[AntiDumpingTariffCode], Dt.[AntiDumpingPercentage], Dt.[AntiDumpingExcemption], Dt.[AntiDumpingSpecific], Dt.[AntiDumpingSpecificExcemption], 
			Dt.[AlcoholMethod], Dt.[ProofOfSpirit], Dt.[VehicleType], Dt.[VehicleBrand], Dt.[VehicleModel], Dt.[VehicleEngineNo], Dt.[VehicleChassisNo], Dt.[VehicleCC], Dt.[VehicleYear], 
			Dt.[UnitLocalAmount], Dt.[TotalUnitLocalAmount], Dt.[TotalDutyPayable], Dt.[TotalGSTPayable], Dt.[ECCNNo], Dt.[ItemWeight], Dt.[UnitCurrency], Dt.[UnitCurrencyExchangeRate], 
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			Dt.FOBValue,Dt.CIFValue,Dt.EXWValue,Dt.CNFValue,Dt.CNIValue,Dt.FreightValue,Dt.InsuranceValue,Dt.CIFCValue,
			Dt.ImportDutyExemptionRatePercent,
			Dt.ImportDutyExemptionRate,
			Dt.ImportDutyExemptionRateAmount,
			Dt.ImportDutyExemptionAmount,
			Dt.ImportDutySpecificExemptionRatePercent,
			Dt.ImportDutySpecificExemptionRate,
			Dt.ImportDutySpecificExemptionRateAmount,
			Dt.ImportDutySpecificExemptionAmount,
			Dt.ImportGSTExemptionPercent,
			Dt.ImportGSTExemptionRate,
			Dt.ImportGSTExemptionRateAmount,
			Dt.ImportGSTExemptionAmount,
			Dt.ImportExciseExemptionRatePercent,
			Dt.ImportExciseExemptionRate,
			Dt.ImportExciseExemptionRateAmount,
			Dt.ImportExciseExemptionAmount,
			Dt.ImportExciseExemptionSpecificRatePercent,
			Dt.ImportExciseExemptionSpecificRate,
			Dt.ImportExciseExemptionSpecificRateAmount,
			Dt.ImportExciseExemptionSpecificAmount,
			Hs.OldPDK, Hs.OldAHTN
	FROM	[Operation].[DeclarationItem] Dt
	Left Outer Join [Master].[HSCode] Hs ON 
		Dt.HSCode = Hs.TariffCode
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo   

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemList] 
-- ========================================================================================================================================

GO





-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [DeclarationItem] Record Into [DeclarationItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemInsert] 
END
GO
CREATE PROC [Operation].[usp_DeclarationItemInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ProductCode nvarchar(20),
    @ProductDescription nvarchar(100),
    @HSCode nvarchar(20),
    @OriginCountryCode varchar(2),
    @StatisticalQty decimal(18, 7),
    @StatisticalUOM nvarchar(20),
    @DeclaredQty decimal(18, 7),
    @DeclaredUOM nvarchar(20),
    @ItemAmount decimal(18, 7),
    @ItemDescription1 nvarchar(50) = NULL,
    @ItemDescription2 nvarchar(50) = NULL,
    @ImportDutyMethod smallint = NULL,
    @ImportDutyRate decimal(18, 3) = NULL,
    @ImportDutyRateAmount decimal(18, 7) = NULL,
    @ImportDutySpecificRate decimal(18, 3) = NULL,
    @ImportDutySpecificRateAmount decimal(18, 7) = NULL,
    @ImportGST decimal(18, 7) = NULL,
    @ImportGSTAmount decimal(18, 7) = NULL,
    @ImportExciseMethod smallint = NULL,
    @ImportExciseRate decimal(18, 7) = NULL,
    @ImportExciseRateAmount decimal(18, 7) = NULL,
    @ImportExciseSpecificRate decimal(18, 7) = NULL,
    @ImportExciseSpecificAmount decimal(18, 7) = NULL,
    @SalesTaxMethod nvarchar(20) = NULL,
    @SalesTaxTariffCode smallint = NULL,
    @SalesTaxPercentage decimal(18, 7) = NULL,
    @SalesTaxExcemption decimal(18, 7) = NULL,
    @SalesTaxSpecific decimal(18, 7) = NULL,
    @SalesTaxSpecificExcemption decimal(18, 7) = NULL,
    @AntiDumpingMethod nvarchar(20) = NULL,
    @AntiDumpingTariffCode smallint = NULL,
    @AntiDumpingPercentage decimal(18, 7) = NULL,
    @AntiDumpingExcemption decimal(18, 7) = NULL,
    @AntiDumpingSpecific decimal(18, 7) = NULL,
    @AntiDumpingSpecificExcemption decimal(18, 7) = NULL,
    @AlcoholMethod decimal(18, 7) = NULL,
    @ProofOfSpirit decimal(18, 7) = NULL,
    @VehicleType smallint = NULL,
    @VehicleBrand nvarchar(50) = NULL,
    @VehicleModel nvarchar(50) = NULL,
    @VehicleEngineNo nvarchar(50) = NULL,
    @VehicleChassisNo nvarchar(50) = NULL,
    @VehicleCC nvarchar(50) = NULL,
    @VehicleYear smallint = NULL,
    @UnitLocalAmount decimal(18, 7) = NULL,
    @TotalUnitLocalAmount decimal(18, 7) = NULL,
    @TotalDutyPayable decimal(18, 7) = NULL,
    @TotalGSTPayable decimal(18, 7) = NULL,
    @ECCNNo nvarchar(5) = NULL,
    @ItemWeight decimal(18, 7) = NULL,
    @UnitCurrency nvarchar(3) = NULL,
    @UnitCurrencyExchangeRate decimal(18, 3) = NULL,
    @CreatedBy  nvarchar(60),
    @ModifiedBy nvarchar(60),
	@FOBValue decimal(18,7), 
	@CIFValue decimal(18,7),
	@EXWValue decimal(18,7),
	@CNFValue decimal(18,7),
	@CNIValue decimal(18,7),
	@FreightValue decimal(18,7),
	@InsuranceValue decimal(18,7),
	@CIFCValue  decimal(18,7),

	@ImportDutyExemptionRatePercent decimal(18, 7),
	@ImportDutyExemptionRate decimal(18, 7),
	@ImportDutyExemptionRateAmount decimal(18, 7),
	@ImportDutyExemptionAmount decimal(18, 7),

	@ImportDutySpecificExemptionRatePercent decimal(18, 7),
	@ImportDutySpecificExemptionRate decimal(18, 7),
	@ImportDutySpecificExemptionRateAmount decimal(18, 7),
	@ImportDutySpecificExemptionAmount decimal(18, 7),

	@ImportGSTExemptionPercent decimal(18, 7),
	@ImportGSTExemptionRate decimal(18, 7),
	@ImportGSTExemptionRateAmount decimal(18, 7),
	@ImportGSTExemptionAmount decimal(18, 7),

	@ImportExciseExemptionRatePercent decimal(18, 7),
	@ImportExciseExemptionRate decimal(18, 7),
	@ImportExciseExemptionRateAmount decimal(18, 7),
	@ImportExciseExemptionAmount decimal(18, 7),

	@ImportExciseExemptionSpecificRatePercent decimal(18, 7),
	@ImportExciseExemptionSpecificRate decimal(18, 7),
	@ImportExciseExemptionSpecificRateAmount decimal(18, 7),
	@ImportExciseExemptionSpecificAmount decimal(18, 7) 
AS 
  

BEGIN
	
	INSERT INTO [Operation].[DeclarationItem] (
			[BranchID], [DeclarationNo], [ItemNo], [OrderNo], [ContainerKey], [CargoKey], [ProductCode], [ProductDescription], [HSCode], [OriginCountryCode], 
			[StatisticalQty], [StatisticalUOM], [DeclaredQty], [DeclaredUOM], [ItemAmount], [ItemDescription1], [ItemDescription2], [ImportDutyMethod], 
			[ImportDutyRate], [ImportDutyRateAmount], [ImportDutySpecificRate], [ImportDutySpecificRateAmount], [ImportGST], [ImportGSTAmount], [ImportExciseMethod], 
			[ImportExciseRate], [ImportExciseRateAmount], [ImportExciseSpecificRate], [ImportExciseSpecificAmount], [SalesTaxMethod], [SalesTaxTariffCode], 
			[SalesTaxPercentage], [SalesTaxExcemption], [SalesTaxSpecific], [SalesTaxSpecificExcemption], [AntiDumpingMethod], [AntiDumpingTariffCode], 
			[AntiDumpingPercentage], [AntiDumpingExcemption], [AntiDumpingSpecific], [AntiDumpingSpecificExcemption], [AlcoholMethod], [ProofOfSpirit], 
			[VehicleType], [VehicleBrand], [VehicleModel], [VehicleEngineNo], [VehicleChassisNo], [VehicleCC], [VehicleYear], [UnitLocalAmount], [TotalUnitLocalAmount], 
			[TotalDutyPayable], [TotalGSTPayable], [ECCNNo], [ItemWeight], [UnitCurrency], [UnitCurrencyExchangeRate], [CreatedBy], [CreatedOn],
			FOBValue,CIFValue,EXWValue,CNFValue,CNIValue,FreightValue,InsuranceValue,CIFCValue,
			ImportDutyExemptionRatePercent,
			ImportDutyExemptionRate,
			ImportDutyExemptionRateAmount,
			ImportDutyExemptionAmount,
			ImportDutySpecificExemptionRatePercent,
			ImportDutySpecificExemptionRate,
			ImportDutySpecificExemptionRateAmount,
			ImportDutySpecificExemptionAmount,
			ImportGSTExemptionPercent,
			ImportGSTExemptionRate,
			ImportGSTExemptionRateAmount,
			ImportGSTExemptionAmount,
			ImportExciseExemptionRatePercent,
			ImportExciseExemptionRate,
			ImportExciseExemptionRateAmount,
			ImportExciseExemptionAmount,
			ImportExciseExemptionSpecificRatePercent,
			ImportExciseExemptionSpecificRate,
			ImportExciseExemptionSpecificRateAmount,
			ImportExciseExemptionSpecificAmount)
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey, @ProductCode, @ProductDescription, @HSCode, @OriginCountryCode, 
			@StatisticalQty, @StatisticalUOM, @DeclaredQty, @DeclaredUOM, @ItemAmount, @ItemDescription1, @ItemDescription2, @ImportDutyMethod, 
			@ImportDutyRate, @ImportDutyRateAmount, @ImportDutySpecificRate, @ImportDutySpecificRateAmount, @ImportGST, @ImportGSTAmount, @ImportExciseMethod, 
			@ImportExciseRate, @ImportExciseRateAmount, @ImportExciseSpecificRate, @ImportExciseSpecificAmount, @SalesTaxMethod, @SalesTaxTariffCode, 
			@SalesTaxPercentage, @SalesTaxExcemption, @SalesTaxSpecific, @SalesTaxSpecificExcemption, @AntiDumpingMethod, @AntiDumpingTariffCode, 
			@AntiDumpingPercentage, @AntiDumpingExcemption, @AntiDumpingSpecific, @AntiDumpingSpecificExcemption, @AlcoholMethod, @ProofOfSpirit, 
			@VehicleType, @VehicleBrand, @VehicleModel, @VehicleEngineNo, @VehicleChassisNo, @VehicleCC, @VehicleYear, @UnitLocalAmount, @TotalUnitLocalAmount, 
			@TotalDutyPayable, @TotalGSTPayable, @ECCNNo, @ItemWeight, @UnitCurrency, @UnitCurrencyExchangeRate, @CreatedBy, GETUTCDATE(),
			@FOBValue,@CIFValue,@EXWValue,@CNFValue,@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,
			@ImportDutyExemptionRatePercent,
			@ImportDutyExemptionRate,
			@ImportDutyExemptionRateAmount,
			@ImportDutyExemptionAmount,
			@ImportDutySpecificExemptionRatePercent,
			@ImportDutySpecificExemptionRate,
			@ImportDutySpecificExemptionRateAmount,
			@ImportDutySpecificExemptionAmount,
			@ImportGSTExemptionPercent,
			@ImportGSTExemptionRate,
			@ImportGSTExemptionRateAmount,
			@ImportGSTExemptionAmount,
			@ImportExciseExemptionRatePercent,
			@ImportExciseExemptionRate,
			@ImportExciseExemptionRateAmount,
			@ImportExciseExemptionAmount,
			@ImportExciseExemptionSpecificRatePercent,
			@ImportExciseExemptionSpecificRate,
			@ImportExciseExemptionSpecificRateAmount,
			@ImportExciseExemptionSpecificAmount

	
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [DeclarationItem] Record Into [DeclarationItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemUpdate] 
END
GO
CREATE PROC [Operation].[usp_DeclarationItemUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ProductCode nvarchar(20),
    @ProductDescription nvarchar(100),
    @HSCode nvarchar(20),
    @OriginCountryCode varchar(2),
    @StatisticalQty decimal(18, 7),
    @StatisticalUOM nvarchar(20),
    @DeclaredQty decimal(18, 7),
    @DeclaredUOM nvarchar(20),
    @ItemAmount decimal(18, 7),
    @ItemDescription1 nvarchar(50) = NULL,
    @ItemDescription2 nvarchar(50) = NULL,
    @ImportDutyMethod smallint = NULL,
    @ImportDutyRate decimal(18, 3) = NULL,
    @ImportDutyRateAmount decimal(18, 7) = NULL,
    @ImportDutySpecificRate decimal(18, 3) = NULL,
    @ImportDutySpecificRateAmount decimal(18, 7) = NULL,
    @ImportGST decimal(18, 7) = NULL,
    @ImportGSTAmount decimal(18, 7) = NULL,
    @ImportExciseMethod smallint = NULL,
    @ImportExciseRate decimal(18, 7) = NULL,
    @ImportExciseRateAmount decimal(18, 7) = NULL,
    @ImportExciseSpecificRate decimal(18, 7) = NULL,
    @ImportExciseSpecificAmount decimal(18, 7) = NULL,
    @SalesTaxMethod nvarchar(20) = NULL,
    @SalesTaxTariffCode smallint = NULL,
    @SalesTaxPercentage decimal(18, 7) = NULL,
    @SalesTaxExcemption decimal(18, 7) = NULL,
    @SalesTaxSpecific decimal(18, 7) = NULL,
    @SalesTaxSpecificExcemption decimal(18, 7) = NULL,
    @AntiDumpingMethod nvarchar(20) = NULL,
    @AntiDumpingTariffCode smallint = NULL,
    @AntiDumpingPercentage decimal(18, 7) = NULL,
    @AntiDumpingExcemption decimal(18, 7) = NULL,
    @AntiDumpingSpecific decimal(18, 7) = NULL,
    @AntiDumpingSpecificExcemption decimal(18, 7) = NULL,
    @AlcoholMethod decimal(18, 7) = NULL,
    @ProofOfSpirit decimal(18, 7) = NULL,
    @VehicleType smallint = NULL,
    @VehicleBrand nvarchar(50) = NULL,
    @VehicleModel nvarchar(50) = NULL,
    @VehicleEngineNo nvarchar(50) = NULL,
    @VehicleChassisNo nvarchar(50) = NULL,
    @VehicleCC nvarchar(50) = NULL,
    @VehicleYear smallint = NULL,
    @UnitLocalAmount decimal(18, 7) = NULL,
    @TotalUnitLocalAmount decimal(18, 7) = NULL,
    @TotalDutyPayable decimal(18, 7) = NULL,
    @TotalGSTPayable decimal(18, 7) = NULL,
    @ECCNNo nvarchar(5) = NULL,
    @ItemWeight decimal(18, 7) = NULL,
    @UnitCurrency nvarchar(3) = NULL,
    @UnitCurrencyExchangeRate decimal(18, 3) = NULL,
    @CreatedBy  nvarchar(60),
    @ModifiedBy nvarchar(60),
	@FOBValue decimal(18,7), 
	@CIFValue decimal(18,7),
	@EXWValue decimal(18,7),
	@CNFValue decimal(18,7),
	@CNIValue decimal(18,7),
	@FreightValue decimal(18,7),
	@InsuranceValue decimal(18,7),
	@CIFCValue  decimal(18,7),
	@ImportDutyExemptionRatePercent decimal(18, 7) = NULL,
	@ImportDutyExemptionRate decimal(18, 7) = NULL,
	@ImportDutyExemptionRateAmount decimal(18, 7) = NULL,
	@ImportDutyExemptionAmount decimal(18, 7) = NULL,

	@ImportDutySpecificExemptionRatePercent decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionRate decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionRateAmount decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionAmount decimal(18, 7) = NULL,

	@ImportGSTExemptionPercent decimal(18, 7) = NULL,
	@ImportGSTExemptionRate decimal(18, 7) = NULL,
	@ImportGSTExemptionRateAmount decimal(18, 7) = NULL,
	@ImportGSTExemptionAmount decimal(18, 7) = NULL,

	@ImportExciseExemptionRatePercent decimal(18, 7) = NULL,
	@ImportExciseExemptionRate decimal(18, 7) = NULL,
	@ImportExciseExemptionRateAmount decimal(18, 7) = NULL,
	@ImportExciseExemptionAmount decimal(18, 7) = NULL,

	@ImportExciseExemptionSpecificRatePercent decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificRate decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificRateAmount decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificAmount decimal(18, 7) = NULL
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationItem]
	SET   [ProductCode] = @ProductCode, [ProductDescription] = @ProductDescription, [HSCode] = @HSCode, [OriginCountryCode] = @OriginCountryCode, 
	[StatisticalQty] = @StatisticalQty, [StatisticalUOM] = @StatisticalUOM, [DeclaredQty] = @DeclaredQty, [DeclaredUOM] = @DeclaredUOM, [ItemAmount] = @ItemAmount, 
	[ItemDescription1] = @ItemDescription1, [ItemDescription2] = @ItemDescription2, [ImportDutyMethod] = @ImportDutyMethod, [ImportDutyRate] = @ImportDutyRate, 
	[ImportDutyRateAmount] = @ImportDutyRateAmount, [ImportDutySpecificRate] = @ImportDutySpecificRate, [ImportDutySpecificRateAmount] = @ImportDutySpecificRateAmount, 
	[ImportGST] = @ImportGST, [ImportGSTAmount] = @ImportGSTAmount, [ImportExciseMethod] = @ImportExciseMethod, [ImportExciseRate] = @ImportExciseRate, 
	[ImportExciseRateAmount] = @ImportExciseRateAmount, [ImportExciseSpecificRate] = @ImportExciseSpecificRate, [ImportExciseSpecificAmount] = @ImportExciseSpecificAmount, 
	[SalesTaxMethod] = @SalesTaxMethod, [SalesTaxTariffCode] = @SalesTaxTariffCode, [SalesTaxPercentage] = @SalesTaxPercentage, [SalesTaxExcemption] = @SalesTaxExcemption, 
	[SalesTaxSpecific] = @SalesTaxSpecific, [SalesTaxSpecificExcemption] = @SalesTaxSpecificExcemption, [AntiDumpingMethod] = @AntiDumpingMethod, 
	[AntiDumpingTariffCode] = @AntiDumpingTariffCode, [AntiDumpingPercentage] = @AntiDumpingPercentage, [AntiDumpingExcemption] = @AntiDumpingExcemption, 
	[AntiDumpingSpecific] = @AntiDumpingSpecific, [AntiDumpingSpecificExcemption] = @AntiDumpingSpecificExcemption, [AlcoholMethod] = @AlcoholMethod, 
	[ProofOfSpirit] = @ProofOfSpirit, [VehicleType] = @VehicleType, [VehicleBrand] = @VehicleBrand, [VehicleModel] = @VehicleModel, [VehicleEngineNo] = @VehicleEngineNo, 
	[VehicleChassisNo] = @VehicleChassisNo, [VehicleCC] = @VehicleCC, [VehicleYear] = @VehicleYear, [UnitLocalAmount] = @UnitLocalAmount, 
	[TotalUnitLocalAmount] = @TotalUnitLocalAmount, [TotalDutyPayable] = @TotalDutyPayable, [TotalGSTPayable] = @TotalGSTPayable, [ECCNNo] = @ECCNNo, 
	[ItemWeight] = @ItemWeight, [UnitCurrency] = @UnitCurrency, [UnitCurrencyExchangeRate] = @UnitCurrencyExchangeRate, 
	[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),

	FOBValue=@FOBValue,CIFValue=@CIFValue,EXWValue=@EXWValue,CNFValue=@CNFValue,CNIValue=@CNIValue,FreightValue=@FreightValue,InsuranceValue=@InsuranceValue,CIFCValue=@CIFCValue,
	ImportDutyExemptionRatePercent = @ImportDutyExemptionRatePercent,
	ImportDutyExemptionRate = @ImportDutyExemptionRate,
	ImportDutyExemptionRateAmount = @ImportDutyExemptionRateAmount,
	ImportDutyExemptionAmount = @ImportDutyExemptionAmount,
	ImportDutySpecificExemptionRatePercent = @ImportDutySpecificExemptionRatePercent,
	ImportDutySpecificExemptionRate = @ImportDutySpecificExemptionRate,
	ImportDutySpecificExemptionRateAmount = @ImportDutySpecificExemptionRateAmount,
	ImportDutySpecificExemptionAmount = @ImportDutySpecificExemptionAmount,
	ImportGSTExemptionPercent = @ImportGSTExemptionPercent,
	ImportGSTExemptionRate = @ImportGSTExemptionRate,
	ImportGSTExemptionRateAmount = @ImportGSTExemptionRateAmount,
	ImportGSTExemptionAmount = @ImportGSTExemptionAmount,
	ImportExciseExemptionRatePercent = @ImportExciseExemptionRatePercent,
	ImportExciseExemptionRate = @ImportExciseExemptionRate,
	ImportExciseExemptionRateAmount = @ImportExciseExemptionRateAmount,
	ImportExciseExemptionAmount = @ImportExciseExemptionAmount,
	ImportExciseExemptionSpecificRatePercent = @ImportExciseExemptionSpecificRatePercent,
	ImportExciseExemptionSpecificRate = @ImportExciseExemptionSpecificRate,
	ImportExciseExemptionSpecificRateAmount = @ImportExciseExemptionSpecificRateAmount,
	ImportExciseExemptionSpecificAmount = @ImportExciseExemptionSpecificAmount
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [CargoKey] = @CargoKey
	

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [DeclarationItem] Record Into [DeclarationItem] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemSave] 
END
GO
CREATE PROC [Operation].[usp_DeclarationItemSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ProductCode nvarchar(20),
    @ProductDescription nvarchar(100),
    @HSCode nvarchar(20),
    @OriginCountryCode varchar(2),
    @StatisticalQty decimal(18, 7),
    @StatisticalUOM nvarchar(20),
    @DeclaredQty decimal(18, 7),
    @DeclaredUOM nvarchar(20),
    @ItemAmount decimal(18, 7),
    @ItemDescription1 nvarchar(50) = NULL,
    @ItemDescription2 nvarchar(50) = NULL,
    @ImportDutyMethod smallint = NULL,
    @ImportDutyRate decimal(18, 3) = NULL,
    @ImportDutyRateAmount decimal(18, 7) = NULL,
    @ImportDutySpecificRate decimal(18, 3) = NULL,
    @ImportDutySpecificRateAmount decimal(18, 7) = NULL,
    @ImportGST decimal(18, 7) = NULL,
    @ImportGSTAmount decimal(18, 7) = NULL,
    @ImportExciseMethod smallint = NULL,
    @ImportExciseRate decimal(18, 7) = NULL,
    @ImportExciseRateAmount decimal(18, 7) = NULL,
    @ImportExciseSpecificRate decimal(18, 7) = NULL,
    @ImportExciseSpecificAmount decimal(18, 7) = NULL,
    @SalesTaxMethod nvarchar(20) = NULL,
    @SalesTaxTariffCode smallint = NULL,
    @SalesTaxPercentage decimal(18, 7) = NULL,
    @SalesTaxExcemption decimal(18, 7) = NULL,
    @SalesTaxSpecific decimal(18, 7) = NULL,
    @SalesTaxSpecificExcemption decimal(18, 7) = NULL,
    @AntiDumpingMethod nvarchar(20) = NULL,
    @AntiDumpingTariffCode smallint = NULL,
    @AntiDumpingPercentage decimal(18, 7) = NULL,
    @AntiDumpingExcemption decimal(18, 7) = NULL,
    @AntiDumpingSpecific decimal(18, 7) = NULL,
    @AntiDumpingSpecificExcemption decimal(18, 7) = NULL,
    @AlcoholMethod decimal(18, 7) = NULL,
    @ProofOfSpirit decimal(18, 7) = NULL,
    @VehicleType smallint = NULL,
    @VehicleBrand nvarchar(50) = NULL,
    @VehicleModel nvarchar(50) = NULL,
    @VehicleEngineNo nvarchar(50) = NULL,
    @VehicleChassisNo nvarchar(50) = NULL,
    @VehicleCC nvarchar(50) = NULL,
    @VehicleYear smallint = NULL,
    @UnitLocalAmount decimal(18, 7) = NULL,
    @TotalUnitLocalAmount decimal(18, 7) = NULL,
    @TotalDutyPayable decimal(18, 7) = NULL,
    @TotalGSTPayable decimal(18, 7) = NULL,
    @ECCNNo nvarchar(5) = NULL,
    @ItemWeight decimal(18, 7) = NULL,
    @UnitCurrency nvarchar(3) = NULL,
    @UnitCurrencyExchangeRate decimal(18, 3) = NULL,
    @CreatedBy  nvarchar(60),
    @ModifiedBy nvarchar(60),
	@FOBValue decimal(18,7), 
	@CIFValue decimal(18,7),
	@EXWValue decimal(18,7),
	@CNFValue decimal(18,7),
	@CNIValue decimal(18,7),
	@FreightValue decimal(18,7),
	@InsuranceValue decimal(18,7),
	@CIFCValue  decimal(18,7),
	@ImportDutyExemptionRatePercent decimal(18, 7) = NULL,
	@ImportDutyExemptionRate decimal(18, 7) = NULL,
	@ImportDutyExemptionRateAmount decimal(18, 7) = NULL,
	@ImportDutyExemptionAmount decimal(18, 7) = NULL,

	@ImportDutySpecificExemptionRatePercent decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionRate decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionRateAmount decimal(18, 7) = NULL,
	@ImportDutySpecificExemptionAmount decimal(18, 7) = NULL,

	@ImportGSTExemptionPercent decimal(18, 7) = NULL,
	@ImportGSTExemptionRate decimal(18, 7) = NULL,
	@ImportGSTExemptionRateAmount decimal(18, 7) = NULL,
	@ImportGSTExemptionAmount decimal(18, 7) = NULL,

	@ImportExciseExemptionRatePercent decimal(18, 7) = NULL,
	@ImportExciseExemptionRate decimal(18, 7) = NULL,
	@ImportExciseExemptionRateAmount decimal(18, 7) = NULL,
	@ImportExciseExemptionAmount decimal(18, 7) = NULL,

	@ImportExciseExemptionSpecificRatePercent decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificRate decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificRateAmount decimal(18, 7) = NULL,
	@ImportExciseExemptionSpecificAmount decimal(18, 7) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Operation].[DeclarationItem] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [CargoKey] = @CargoKey)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationItemUpdate] 
		@BranchID, @DeclarationNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey, @ProductCode, @ProductDescription, @HSCode, @OriginCountryCode,
		@StatisticalQty, @StatisticalUOM, @DeclaredQty, @DeclaredUOM, @ItemAmount, @ItemDescription1, @ItemDescription2, @ImportDutyMethod, 
		@ImportDutyRate, @ImportDutyRateAmount, @ImportDutySpecificRate, @ImportDutySpecificRateAmount, @ImportGST, @ImportGSTAmount, 
		@ImportExciseMethod, @ImportExciseRate, @ImportExciseRateAmount, @ImportExciseSpecificRate, @ImportExciseSpecificAmount, 
		@SalesTaxMethod, @SalesTaxTariffCode, @SalesTaxPercentage, @SalesTaxExcemption, @SalesTaxSpecific, @SalesTaxSpecificExcemption, 
		@AntiDumpingMethod, @AntiDumpingTariffCode, @AntiDumpingPercentage, @AntiDumpingExcemption, @AntiDumpingSpecific, @AntiDumpingSpecificExcemption, 
		@AlcoholMethod, @ProofOfSpirit, @VehicleType, @VehicleBrand, @VehicleModel, @VehicleEngineNo, @VehicleChassisNo, @VehicleCC, @VehicleYear, 
		@UnitLocalAmount, @TotalUnitLocalAmount, @TotalDutyPayable, @TotalGSTPayable, @ECCNNo, @ItemWeight, @UnitCurrency, @UnitCurrencyExchangeRate, 
		@CreatedBy, @ModifiedBy,@FOBValue,@CIFValue,@EXWValue,@CNFValue,@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,
		@ImportDutyExemptionRatePercent,
		@ImportDutyExemptionRate,
		@ImportDutyExemptionRateAmount,
		@ImportDutyExemptionAmount,
		@ImportDutySpecificExemptionRatePercent,
		@ImportDutySpecificExemptionRate,
		@ImportDutySpecificExemptionRateAmount,
		@ImportDutySpecificExemptionAmount,
		@ImportGSTExemptionPercent,
		@ImportGSTExemptionRate,
		@ImportGSTExemptionRateAmount,
		@ImportGSTExemptionAmount,
		@ImportExciseExemptionRatePercent,
		@ImportExciseExemptionRate,
		@ImportExciseExemptionRateAmount,
		@ImportExciseExemptionAmount,
		@ImportExciseExemptionSpecificRatePercent,
		@ImportExciseExemptionSpecificRate,
		@ImportExciseExemptionSpecificRateAmount,
		@ImportExciseExemptionSpecificAmount



	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationItemInsert] 
		@BranchID, @DeclarationNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey, @ProductCode, @ProductDescription, @HSCode, @OriginCountryCode,
		@StatisticalQty, @StatisticalUOM, @DeclaredQty, @DeclaredUOM, @ItemAmount, @ItemDescription1, @ItemDescription2, @ImportDutyMethod, 
		@ImportDutyRate, @ImportDutyRateAmount, @ImportDutySpecificRate, @ImportDutySpecificRateAmount, @ImportGST, @ImportGSTAmount, 
		@ImportExciseMethod, @ImportExciseRate, @ImportExciseRateAmount, @ImportExciseSpecificRate, @ImportExciseSpecificAmount, 
		@SalesTaxMethod, @SalesTaxTariffCode, @SalesTaxPercentage, @SalesTaxExcemption, @SalesTaxSpecific, @SalesTaxSpecificExcemption, 
		@AntiDumpingMethod, @AntiDumpingTariffCode, @AntiDumpingPercentage, @AntiDumpingExcemption, @AntiDumpingSpecific, @AntiDumpingSpecificExcemption, 
		@AlcoholMethod, @ProofOfSpirit, @VehicleType, @VehicleBrand, @VehicleModel, @VehicleEngineNo, @VehicleChassisNo, @VehicleCC, @VehicleYear, 
		@UnitLocalAmount, @TotalUnitLocalAmount, @TotalDutyPayable, @TotalGSTPayable, @ECCNNo, @ItemWeight, @UnitCurrency, @UnitCurrencyExchangeRate, 
		@CreatedBy, @ModifiedBy,@FOBValue,@CIFValue,@EXWValue,@CNFValue,@CNIValue,@FreightValue,@InsuranceValue,@CIFCValue,
		@ImportDutyExemptionRatePercent,
		@ImportDutyExemptionRate,
		@ImportDutyExemptionRateAmount,
		@ImportDutyExemptionAmount,
		@ImportDutySpecificExemptionRatePercent,
		@ImportDutySpecificExemptionRate,
		@ImportDutySpecificExemptionRateAmount,
		@ImportDutySpecificExemptionAmount,
		@ImportGSTExemptionPercent,
		@ImportGSTExemptionRate,
		@ImportGSTExemptionRateAmount,
		@ImportGSTExemptionAmount,
		@ImportExciseExemptionRatePercent,
		@ImportExciseExemptionRate,
		@ImportExciseExemptionRateAmount,
		@ImportExciseExemptionAmount,
		@ImportExciseExemptionSpecificRatePercent,
		@ImportExciseExemptionSpecificRate,
		@ImportExciseExemptionSpecificRateAmount,
		@ImportExciseExemptionSpecificAmount
 


	END
	


END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationItemSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationItemDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [DeclarationItem] Record  based on [DeclarationItem]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationItemDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationItemDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationItemDelete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationItem]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       --AND [ItemNo] = @ItemNo
	       --AND [OrderNo] = @OrderNo
	       --AND [ContainerKey] = @ContainerKey
	      -- AND [CargoKey] = @CargoKey
	 
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationItemDelete]
-- ========================================================================================================================================


GO 
