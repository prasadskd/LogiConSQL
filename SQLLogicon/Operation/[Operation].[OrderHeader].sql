

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderSelect] 
END 
GO

CREATE PROC [Operation].[usp_OrderHeaderSelect] 
    @BranchID BIGINT,
    @OrderNo VARCHAR(70)
AS 
 

BEGIN

	;with	JobTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
			ShipmentTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='OrderShipmentType'),
			ServiceTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='ServiceType'),
			TransportTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='TransportType'),
			StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType'),
			ModuleTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module'),
			IncoTermsLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='IncoTerms'),
			CargoClassDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'CargoClass')
	SELECT	OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[ModuleID], OrdHd.[JobType], 
			OrdHd.[ShipmentType], OrdHd.[ServiceType], OrdHd.[TransportType], OrdHd.[IncoTerm], OrdHd.[OrderCategory], OrdHd.[ShipperCode], 
			OrdHd.[ConsigneeCode], OrdHd.[PrincipalCode], OrdHd.[CustomerCode], OrdHd.[VendorCode], OrdHd.[CoLoaderCode], 
			OrdHd.[ShippingAgent], OrdHd.[ForeignAgent], OrdHd.[FwdAgent], OrdHd.[PortOperator], OrdHd.[VesselScheduleID], 
			OrdHd.[VesselID], OrdHd.[Voyageno], OrdHd.[FeederVoyageNo],OrdHd.[CustomerRef], OrdHd.[OceanBLNo], OrdHd.[HouseBLNo], 
			OrdHd.[ShippingLinerRefNo], OrdHd.[ShippingNote], OrdHd.[KANo],OrdHd.[ManifestNo], OrdHd.[SCNNo], OrdHd.[PlaceOfReceipt], OrdHd.[PortOfLoading], 
			OrdHd.[TranshipmentPort], OrdHd.[PortOfDischarge], OrdHd.[PlaceOfDischarge], OrdHd.[DropOffCode], OrdHd.[ClosingDate], 
			OrdHd.[ClosingDateReefer], OrdHd.[ETA], OrdHd.[ETD], OrdHd.[YardCutOffTime], OrdHd.[YardCutOffTimeReefer],OrdHd.[OrderStatus],
			OrdHd.[WebOrderStatus],OrdHd.[FreightStatus],OrdHd.[FwdStatus],OrdHd.[TransportStatus],OrdHd.[HaulageStatus],OrdHd.[WarehouseStatus],
			OrdHd.[CFSStatus],OrdHd.[DepotStatus],OrdHd.[BillingStatus],OrdHd.[IsMasterJob],OrdHd.[IsBillable],OrdHd.[IsCancel], 
			OrdHd.[IsPrinted], OrdHd.[IsEDI],OrdHd.[EDIDateTime], OrdHd.[CancelBy], OrdHd.[CancelOn], OrdHd.[PrintedBy], 
			OrdHd.[PrintedOn], OrdHd.[CreatedBy], OrdHd.[CreatedOn], OrdHd.[ModifiedBy], OrdHd.[ModifiedOn] ,OrdHd.[Remarks],
			OrdHd.CargoCurrencyCode,OrdHd.CargoExchangeRate,OrdHd.CargoBaseAmount,OrdHd.CargoLocalAmount,OrdHd.PrimaryYard,
			OrdHd.FwdAgentCompanyID,OrdHd.FwdAgentBranchID,OrdHd.FwdAgentRemarks, OrdHd.FwdAgentModifiedBy,OrdHd.FwdAgentModifiedOn,
			OrdHd.NotifyParty,OrdHd.NotifyPartyAddressID,OrdHd.CustomerAddressID,OrdHd.ShipperAddressID,OrdHd.ConsigneeAddressID,OrdHd.ShippingAgentAddressID,OrdHd.FwdAgentAddressID,
			OrdHd.InvoiceNo,OrdHd.InvoiceAmount,OrdHd.InvoiceCurrency,OrdHd.LocalAmount,OrdHd.ExchangeRate,OrdHd.PONo,OrdHd.COLoaderBLNo,OrdHd.ContractNo,
			OrdHd.SalesOrderNo,OrdHd.SalesOrderDate,OrdHd.SalesTerm,OrdHd.PaymentTerm,OrdHd.VehicleNo1,OrdHd.VehicleNo2,OrdHd.WagonNo,
			OrdHd.FlightNo,OrdHd.ARNNo,OrdHd.MasterAWBNo,OrdHd.HouseAWBNo,OrdHd.AIRCOLoadNo,OrdHd.JKNo,OrdHd.InvoiceDate,Convert(varchar(100),OrdHd.MessageID) As MessageID,
			Vsl.CallSignNo, --OC.ItemDescription,
			OrdHd.InsuranceTokenNo,OrdHd.ExtraCargoDescription,OrdHd.StowageCode,
			OrdHd.OverseasAgentCode,OrdHd.OverseasAgentAddressID,OrdHd.FregihtForwarderCode,OrdHd.FreightForwarderAddressID,
			ISNULL(Mdl.LookupDescription ,'') As ModuleDescription ,
			ISNULL(Jb.LookupDescription,'') As JobTypeDescription ,
			ISNULL(Sp.LookupDescription,'') As ShipmentTypeDescription ,
			ISNULL(Svr.LookupDescription,'') As ServiceTypeDescription,
			ISNULL(Tpt.LookupDescription,'') As TransportTypeDescription, 
			ISNULL(Jc.Description,'') As JobCategoryDescription, 
			ISNULL(Shpr.MerchantName,'') As ShipperName, 
			ISNULL(Cons.MerchantName,'') As ConsigneeName, 
			ISNULL(Pr.MerchantName,'') As PrincipalName, 
			ISNULL(Cus.MerchantName,'') As CustomerName, 
			ISNULL(Ntp.MerchantName,'') As NotifyPartyName,
			ISNULL(Vndr.MerchantName,'') As VendorName, 
			ISNULL(CoLdr.MerchantName,'') As CoLoaderName, 
			ISNULL(SL.MerchantName,'') As ShippingAgentName, 
			ISNULL(FA.MerchantName,'') As ForeignAgentName, 
			ISNULL(osa.MerchantName,'') As OverseasAgentName, 
			ISNULL(ff.MerchantName,'') As FreightForwarderName, 
			ISNULL(Fwd.MerchantName,'') As ForwardingAgentName, 
			ISNULL(Opr.OperatorName,'') As PortOperatorName, 
			ISNULL(Drp.MerchantName,'') As DropOffName, 
			ISNULL(Vsl.VesselName,'') As VesselName, 
			ISNULL(OrdHd.PlaceOfReceipt + ' - ' + PoR.PortName,'') As PlaceOfReceiptName,     
			ISNULL(OrdHd.PortOfLoading + ' - ' + PoL.PortName,'') As PortOfLoadingName, 
			ISNULL(OrdHd.TranshipmentPort + ' - ' + TransPort.PortName,'') As TranshipmentPortName, 
			ISNULL(OrdHd.PortOfDischarge + ' - ' + PoD.PortName,'') As PortOfDischargeName, 
			ISNULL(OrdHd.PlaceOfDischarge + ' - ' + PloD.PortName,'') As PlaceOfDischargeName, 
			ISNULL(OrdSt.LookupDescription,'') As OrderStatusDescription, 
			ISNULL(OrdStweb.LookupDescription,'') As WebOrderStatusDescription, 
			ISNULL(FrgSt.LookupDescription,'') As FreightStatusDescription, 
			ISNULL(FwdSt.LookupDescription,'') As FwdStatusDescription, 
			ISNULL(TrnSt.LookupDescription,'') As TransportStatusDescription, 
			ISNULL(HlgSt.LookupDescription,'') As HaulageStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(CfsSt.LookupDescription,'') As CFSStatusDescription, 
			ISNULL(DptSt.LookupDescription,'') As DepotStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(BillSt.LookupDescription,'') As BillingStatusDescription,
			ISNULL(Inco.LookupDescription,'') As IncoTermsDescription,
			ISNULL(Cur.Description,'') As CargoCurrencyDescription,

			ISNULL(CAdd.Address1,'') As CustomerAddress1,
			ISNULL(CAdd.Address2,'') As CustomerAddress2,
			ISNULL(CAdd.Address3,'') As CustomerAddress3,
			ISNULL(CAdd.Address4,'') As CustomerAddress4, 
			ISNULL(CAdd.City,'') As CustomerCity,
			ISNULL(CAdd.CountryCode,'') As CustomerCountryCode,
			ISNULL(CAdd.ZipCode,'') As CustomerZipCode,
			ISNULL(CAdd.State,'') As CustomerState,
			ISNULL(CAdd.TelNo,'') As CustomerTelNo,
			ISNULL(Cus.RegNo,'') As CustomerROCNo,

			ISNULL(SAdd.Address1,'') As ShipperAddress1,
			ISNULL(SAdd.Address2,'') As ShipperAddress2,
			ISNULL(SAdd.Address3,'') As ShipperAddress3,
			ISNULL(SAdd.Address4,'') As ShipperAddress4, 
			ISNULL(SAdd.City,'') As ShipperCity,
			ISNULL(SAdd.CountryCode,'') As ShipperCountryCode,
			ISNULL(SAdd.ZipCode,'') As ShipperZipCode,
			ISNULL(SAdd.State,'') As ShipperState,
			ISNULL(CAdd.TelNo,'') As ShipperTelNo,
			ISNULL(Shpr.RegNo,'') As ShipperROCNo,

			ISNULL(CnAdd.Address1,'') As ConsigneeAddress1,
			ISNULL(CnAdd.Address2,'') As ConsigneeAddress2,
			ISNULL(CnAdd.Address3,'') As ConsigneeAddress3,
			ISNULL(CnAdd.Address4,'') As ConsigneeAddress4, 
			ISNULL(CnAdd.City,'') As ConsigneeCity,
			ISNULL(CnAdd.CountryCode,'') As ConsigneeCountryCode,
			ISNULL(CnAdd.ZipCode,'') As ConsigneeZipCode,
			ISNULL(CnAdd.State,'') As ConsigneeState,
			ISNULL(CAdd.TelNo,'') As ConsigneeTelNo,
			ISNULL(Cons.RegNo,'') As ConsigneeROCNo,

			ISNULL(NAdd.Address1,'') As NotifyPartyAddress1,
			ISNULL(NAdd.Address2,'') As NotifyPartyAddress2,
			ISNULL(NAdd.Address3,'') As NotifyPartyAddress3,
			ISNULL(NAdd.Address4,'') As NotifyPartyAddress4, 
			ISNULL(NAdd.City,'') As NotifyPartyCity,
			ISNULL(NAdd.CountryCode,'') As NotifyPartyCountryCode,
			ISNULL(NAdd.ZipCode,'') As NotifyPartyZipCode,
			ISNULL(NAdd.State,'') As NotifyPartyState,
			ISNULL(NAdd.TelNo,'') As NotifyPartyTelNo,
			ISNULL(Ntp.RegNo,'') As NotifyPartyROCNo,

			ISNULL(FaAdd.Address1,'') As FwdAgentAddress1,
			ISNULL(FaAdd.Address2,'') As FwdAgentAddress2,
			ISNULL(FaAdd.Address3,'') As FwdAgentAddress3,
			ISNULL(FaAdd.Address4,'') As FwdAgentAddress4, 
			ISNULL(FaAdd.City,'') As FwdAgentCity,
			ISNULL(FaAdd.CountryCode,'') As FwdAgentCountryCode,
			ISNULL(FaAdd.ZipCode,'') As FwdAgentZipCode,
			ISNULL(FaAdd.State,'') As FwdAgentState,
			ISNULL(FaAdd.TelNo,'') As FwdAgentTelNo,
			ISNULL(Fwd.RegNo,'') As FwdAgentROCNo,

			ISNULL(SaAdd.Address1,'') As ShippingAgentAddress1,
			ISNULL(SaAdd.Address2,'') As ShippingAgentAddress2,
			ISNULL(SaAdd.Address3,'') As ShippingAgentAddress3,
			ISNULL(SaAdd.Address4,'') As ShippingAgentAddress4, 
			ISNULL(SaAdd.City,'') As ShippingAgentCity,
			ISNULL(SaAdd.CountryCode,'') As ShippingAgentCountryCode,
			ISNULL(SaAdd.ZipCode,'') As ShippingAgentZipCode,
			ISNULL(SaAdd.State,'') As ShippingAgentState,
			ISNULL(SaAdd.TelNo,'') As ShippingAgentTelNo,
			ISNULL(SL.RegNo,'') As ShippingAgentROCNo,

			ISNULL(osaAdd.Address1,'') As OverseasAgentAddress1,
			ISNULL(osaAdd.Address2,'') As OverseasAgentAddress2,
			ISNULL(osaAdd.Address3,'') As OverseasAgentAddress3,
			ISNULL(osaAdd.Address4,'') As OverseasAgentAddress4, 
			ISNULL(osaAdd.City,'') As OverseasAgentCity,
			ISNULL(osaAdd.CountryCode,'') As OverseasAgentCountryCode,
			ISNULL(osaAdd.ZipCode,'') As OverseasAgentZipCode,
			ISNULL(osaAdd.State,'') As OverseasAgentState,
			ISNULL(osaAdd.TelNo,'') As OverseasAgentTelNo,
			ISNULL(osa.RegNo,'') As OverseasAgentROCNo,

			ISNULL(ffAdd.Address1,'') As FreightForwarderAddress1,
			ISNULL(ffAdd.Address2,'') As FreightForwarderAddress2,
			ISNULL(ffAdd.Address3,'') As FreightForwarderAddress3,
			ISNULL(ffAdd.Address4,'') As FreightForwarderAddress4, 
			ISNULL(ffAdd.City,'') As FreightForwarderCity,
			ISNULL(ffAdd.CountryCode,'') As FreightForwarderCountryCode,
			ISNULL(ffAdd.ZipCode,'') As FreightForwarderZipCode,
			ISNULL(ffAdd.State,'') As OFreightForwarderState,
			ISNULL(ffAdd.TelNo,'') As FreightForwarderTelNo,
			ISNULL(ff.RegNo,'') As FreightForwarderROCNo,

			OrdHd.CargoClass,OrdHd.CargoDescription,
			ISNULL(CR.LookupDescription,'') As CargoClassDescription,ISNULL(CR.LookupCode,'') As CargoClassCode,IsPartial
	FROM	[Operation].[OrderHeader] OrdHd
	Left Outer Join ModuleTypeLookup Mdl ON 
		OrdHd.ModuleID = Mdl.LookupID
	Left Outer Join JobTypeLookup Jb ON 
		OrdHd.JobType = Jb.LookupID
	Left Outer Join ShipmentTypeLookup Sp On
		OrdHd.ShipmentType = Sp.LookupID
	Left Outer Join ServiceTypeLookup Svr ON 
		OrdHd.ServiceType = Svr.LookupID 
	Left Outer Join TransportTypeLookup Tpt ON 
		OrdHd.TransportType = Tpt.LookupID
	Left Outer Join Master.JobCategory Jc ON 
		OrdHd.OrderCategory = jc.Code
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		And Cons.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Merchant Pr ON 
		OrdHd.PrincipalCode = Pr.MerchantCode 
		And Pr.IsPrincipal = CAST(1 as bit)
	Left Outer Join Master.Merchant Cus ON 
		OrdHd.CustomerCode = Cus.MerchantCode 
	Left Outer Join Master.Merchant Vndr ON 
		OrdHd.VendorCode = Vndr.MerchantCode 
		And Vndr.IsVendor = CAST(1 as bit)
	Left Outer Join Master.Merchant CoLdr ON 
		OrdHd.CoLoaderCode = CoLdr.MerchantCode 
	Left Outer Join Master.Merchant SL ON 
		OrdHd.ShippingAgent = SL.MerchantCode 
		--And SL.IsLiner= CAST(1 as bit)
	Left Outer Join Master.Merchant FA ON 
		OrdHd.ForeignAgent = FA.MerchantCode 
		And FA.IsLiner= CAST(1 as bit)
	Left Outer Join Master.Merchant Fwd ON 
		OrdHd.FwdAgent = Fwd.MerchantCode 
		And Fwd.IsForwarder= CAST(1 as bit)
	Left Outer Join Master.Operator Opr ON 
		OrdHd.PortOperator = Opr.OperatorID 
		 
	Left Outer Join Master.Merchant Drp ON
		OrdHd.DropOffCode = Drp.MerchantCode
		And Drp.IsYard = Cast(1 as bit)
	Left Outer Join Master.Merchant Ntp ON
		OrdHd.NotifyParty = Ntp.MerchantCode
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	Left Outer Join Master.Port PoR On 
		OrdHd.PlaceOfReceipt = PoR.PortCode 
	Left Outer Join Master.Port PoL ON
		OrdHd.PortOfLoading = PoL.PortCode 
	Left Outer Join Master.Port TransPort ON
		OrdHd.TranshipmentPort = Transport.PortCode
	Left Outer Join master.Port PoD ON
		OrdHd.PortOfDischarge = PoD.PortCode
	Left Outer Join master.Port PloD ON 
		OrdHd.PlaceOfDischarge = PloD.PortCode
	Left Outer Join StateTypeLookup OrdSt ON 
		OrdHd.OrderStatus = OrdSt.LookupID
	Left Outer Join StateTypeLookup OrdStweb ON 
		OrdHd.WebOrderStatus = OrdStweb.LookupID
	Left Outer Join StateTypeLookup FrgSt ON 
		OrdHd.FreightStatus = FrgSt.LookupID
	Left Outer Join StateTypeLookup FwdSt ON 
		OrdHd.FwdStatus = FwdSt.LookupID
	Left Outer Join StateTypeLookup TrnSt ON 
		OrdHd.TransportStatus = TrnSt.LookupID
	Left Outer Join StateTypeLookup HlgSt ON 
		OrdHd.HaulageStatus = HlgSt.LookupID
	Left Outer Join StateTypeLookup WhSt ON 
		OrdHd.WarehouseStatus = WhSt.LookupID
	Left Outer Join StateTypeLookup CfsSt ON 
			OrdHd.CFSStatus = CfsSt.LookupID
	Left Outer Join StateTypeLookup DptSt ON 
			OrdHd.DepotStatus = DptSt.LookupID
	Left Outer Join StateTypeLookup BillSt ON 
			OrdHd.BillingStatus = BillSt.LookupID
	Left Outer Join IncoTermsLookup Inco ON 
		OrdHd.IncoTerm = Inco.LookupID
	Left Outer Join Master.Currency Cur ON 
		OrdHd.CargoCurrencyCode = Cur.CurrencyCode
	Left Outer Join Master.Address CAdd On 
		OrdHd.CustomerCode = CAdd.LinkId
		And CAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address SAdd On 
		OrdHd.ShipperCode = SAdd.LinkId
		And SAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address CnAdd On 
		OrdHd.ConsigneeCode = CnAdd.LinkId
		And CAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address NAdd On 
		OrdHd.NotifyParty = NAdd.LinkId
		And NAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address FaAdd On 
		OrdHd.FwdAgent = FaAdd.LinkId
		And FaAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address SaAdd On 
		OrdHd.ShippingAgent = SaAdd.LinkId
		And SaAdd.IsActive = Cast(1 as bit)
	--Left Outer Join Operation.OrderCargo OC On
	--    OC.BranchId=OrdHd.[BranchID]
	Left Outer Join CargoClassDescription CR ON
		OrdHd.[CargoClass]=CR.LookupID
	Left Outer Join Master.Merchant Osa ON
		OrdHd.OverseasAgentCode = Osa.MerchantCode
	Left Outer Join Master.Merchant ff ON
		OrdHd.FregihtForwarderCode = ff.MerchantCode
	Left Outer Join Master.Address osaAdd On 
		OrdHd.OverseasAgentCode = osaAdd.LinkId
		And osaAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address ffAdd On 
		OrdHd.FregihtForwarderCode = ffAdd.LinkId
		And ffAdd.IsActive = Cast(1 as bit)
	WHERE	OrdHd.[BranchID] = @BranchID  
			AND OrdHd.[OrderNo] = @OrderNo 
		  And OrdHd.[IsCancel] = CAST(0 as bit)
			 

END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderSelect]
-- ========================================================================================================================================




GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderHeader] Records from [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderList] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderList] 
    @BranchID BIGINT

AS 
BEGIN
 

	;with	JobTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
			ShipmentTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='OrderShipmentType'),
			ServiceTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='ServiceType'),
			TransportTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='TransportType'),
			StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType'),
			ModuleTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module'),
			IncoTermsLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='IncoTerms'),
			CargoClassDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'CargoClass')
	SELECT	OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[ModuleID], OrdHd.[JobType], 
			OrdHd.[ShipmentType], OrdHd.[ServiceType], OrdHd.[TransportType], OrdHd.[IncoTerm], OrdHd.[OrderCategory], OrdHd.[ShipperCode], 
			OrdHd.[ConsigneeCode], OrdHd.[PrincipalCode], OrdHd.[CustomerCode], OrdHd.[VendorCode], OrdHd.[CoLoaderCode], 
			OrdHd.[ShippingAgent], OrdHd.[ForeignAgent], OrdHd.[FwdAgent], OrdHd.[PortOperator], OrdHd.[VesselScheduleID], 
			OrdHd.[VesselID], OrdHd.[Voyageno], OrdHd.[FeederVoyageNo],OrdHd.[CustomerRef], OrdHd.[OceanBLNo], OrdHd.[HouseBLNo], 
			OrdHd.[ShippingLinerRefNo], OrdHd.[ShippingNote], OrdHd.[KANo],OrdHd.[ManifestNo], OrdHd.[SCNNo], OrdHd.[PlaceOfReceipt], OrdHd.[PortOfLoading], 
			OrdHd.[TranshipmentPort], OrdHd.[PortOfDischarge], OrdHd.[PlaceOfDischarge], OrdHd.[DropOffCode], OrdHd.[ClosingDate], 
			OrdHd.[ClosingDateReefer], OrdHd.[ETA], OrdHd.[ETD], OrdHd.[YardCutOffTime], OrdHd.[YardCutOffTimeReefer],OrdHd.[OrderStatus],
			OrdHd.[WebOrderStatus],OrdHd.[FreightStatus],OrdHd.[FwdStatus],OrdHd.[TransportStatus],OrdHd.[HaulageStatus],OrdHd.[WarehouseStatus],
			OrdHd.[CFSStatus],OrdHd.[DepotStatus],OrdHd.[BillingStatus],OrdHd.[IsMasterJob],OrdHd.[IsBillable],OrdHd.[IsCancel], 
			OrdHd.[IsPrinted], OrdHd.[IsEDI],OrdHd.[EDIDateTime], OrdHd.[CancelBy], OrdHd.[CancelOn], OrdHd.[PrintedBy], 
			OrdHd.[PrintedOn], OrdHd.[CreatedBy], OrdHd.[CreatedOn], OrdHd.[ModifiedBy], OrdHd.[ModifiedOn] ,OrdHd.[Remarks],
			OrdHd.CargoCurrencyCode,OrdHd.CargoExchangeRate,OrdHd.CargoBaseAmount,OrdHd.CargoLocalAmount,OrdHd.PrimaryYard,
			OrdHd.FwdAgentCompanyID,OrdHd.FwdAgentBranchID,OrdHd.FwdAgentRemarks, OrdHd.FwdAgentModifiedBy,OrdHd.FwdAgentModifiedOn,
			OrdHd.NotifyParty,OrdHd.NotifyPartyAddressID,OrdHd.CustomerAddressID,OrdHd.ShipperAddressID,OrdHd.ConsigneeAddressID,OrdHd.ShippingAgentAddressID,OrdHd.FwdAgentAddressID,
			OrdHd.InvoiceNo,OrdHd.InvoiceAmount,OrdHd.InvoiceCurrency,OrdHd.LocalAmount,OrdHd.ExchangeRate,OrdHd.PONo,OrdHd.COLoaderBLNo,OrdHd.ContractNo,
			OrdHd.SalesOrderNo,OrdHd.SalesOrderDate,OrdHd.SalesTerm,OrdHd.PaymentTerm,OrdHd.VehicleNo1,OrdHd.VehicleNo2,OrdHd.WagonNo,
			OrdHd.FlightNo,OrdHd.ARNNo,OrdHd.MasterAWBNo,OrdHd.HouseAWBNo,OrdHd.AIRCOLoadNo,OrdHd.JKNo,OrdHd.InvoiceDate,Convert(varchar(100),OrdHd.MessageID) As MessageID,
			Vsl.CallSignNo, --OC.ItemDescription,
			OrdHd.InsuranceTokenNo,OrdHd.ExtraCargoDescription,OrdHd.StowageCode,
			OrdHd.OverseasAgentCode,OrdHd.OverseasAgentAddressID,OrdHd.FregihtForwarderCode,OrdHd.FreightForwarderAddressID,
			ISNULL(Mdl.LookupDescription ,'') As ModuleDescription ,
			ISNULL(Jb.LookupDescription,'') As JobTypeDescription ,
			ISNULL(Sp.LookupDescription,'') As ShipmentTypeDescription ,
			ISNULL(Svr.LookupDescription,'') As ServiceTypeDescription,
			ISNULL(Tpt.LookupDescription,'') As TransportTypeDescription, 
			ISNULL(Jc.Description,'') As JobCategoryDescription, 
			ISNULL(Shpr.MerchantName,'') As ShipperName, 
			ISNULL(Cons.MerchantName,'') As ConsigneeName, 
			ISNULL(Pr.MerchantName,'') As PrincipalName, 
			ISNULL(Cus.MerchantName,'') As CustomerName, 
			ISNULL(Ntp.MerchantName,'') As NotifyPartyName,
			ISNULL(Vndr.MerchantName,'') As VendorName, 
			ISNULL(CoLdr.MerchantName,'') As CoLoaderName, 
			ISNULL(SL.MerchantName,'') As ShippingAgentName, 
			ISNULL(FA.MerchantName,'') As ForeignAgentName, 
			ISNULL(osa.MerchantName,'') As OverseasAgentName, 
			ISNULL(ff.MerchantName,'') As FreightForwarderName, 
			ISNULL(Fwd.MerchantName,'') As ForwardingAgentName, 
			ISNULL(Opr.OperatorName,'') As PortOperatorName, 
			ISNULL(Drp.MerchantName,'') As DropOffName, 
			ISNULL(Vsl.VesselName,'') As VesselName, 
			ISNULL(OrdHd.PlaceOfReceipt + ' - ' + PoR.PortName,'') As PlaceOfReceiptName,     
			ISNULL(OrdHd.PortOfLoading + ' - ' + PoL.PortName,'') As PortOfLoadingName, 
			ISNULL(OrdHd.TranshipmentPort + ' - ' + TransPort.PortName,'') As TranshipmentPortName, 
			ISNULL(OrdHd.PortOfDischarge + ' - ' + PoD.PortName,'') As PortOfDischargeName, 
			ISNULL(OrdHd.PlaceOfDischarge + ' - ' + PloD.PortName,'') As PlaceOfDischargeName, 
			ISNULL(OrdSt.LookupDescription,'') As OrderStatusDescription, 
			ISNULL(OrdStweb.LookupDescription,'') As WebOrderStatusDescription, 
			ISNULL(FrgSt.LookupDescription,'') As FreightStatusDescription, 
			ISNULL(FwdSt.LookupDescription,'') As FwdStatusDescription, 
			ISNULL(TrnSt.LookupDescription,'') As TransportStatusDescription, 
			ISNULL(HlgSt.LookupDescription,'') As HaulageStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(CfsSt.LookupDescription,'') As CFSStatusDescription, 
			ISNULL(DptSt.LookupDescription,'') As DepotStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(BillSt.LookupDescription,'') As BillingStatusDescription,
			ISNULL(Inco.LookupDescription,'') As IncoTermsDescription,
			ISNULL(Cur.Description,'') As CargoCurrencyDescription,

			ISNULL(CAdd.Address1,'') As CustomerAddress1,
			ISNULL(CAdd.Address2,'') As CustomerAddress2,
			ISNULL(CAdd.Address3,'') As CustomerAddress3,
			ISNULL(CAdd.Address4,'') As CustomerAddress4, 
			ISNULL(CAdd.City,'') As CustomerCity,
			ISNULL(CAdd.CountryCode,'') As CustomerCountryCode,
			ISNULL(CAdd.ZipCode,'') As CustomerZipCode,
			ISNULL(CAdd.State,'') As CustomerState,
			ISNULL(CAdd.TelNo,'') As CustomerTelNo,
			ISNULL(Cus.RegNo,'') As CustomerROCNo,

			ISNULL(SAdd.Address1,'') As ShipperAddress1,
			ISNULL(SAdd.Address2,'') As ShipperAddress2,
			ISNULL(SAdd.Address3,'') As ShipperAddress3,
			ISNULL(SAdd.Address4,'') As ShipperAddress4, 
			ISNULL(SAdd.City,'') As ShipperCity,
			ISNULL(SAdd.CountryCode,'') As ShipperCountryCode,
			ISNULL(SAdd.ZipCode,'') As ShipperZipCode,
			ISNULL(SAdd.State,'') As ShipperState,
			ISNULL(CAdd.TelNo,'') As ShipperTelNo,
			ISNULL(Shpr.RegNo,'') As ShipperROCNo,

			ISNULL(CnAdd.Address1,'') As ConsigneeAddress1,
			ISNULL(CnAdd.Address2,'') As ConsigneeAddress2,
			ISNULL(CnAdd.Address3,'') As ConsigneeAddress3,
			ISNULL(CnAdd.Address4,'') As ConsigneeAddress4, 
			ISNULL(CnAdd.City,'') As ConsigneeCity,
			ISNULL(CnAdd.CountryCode,'') As ConsigneeCountryCode,
			ISNULL(CnAdd.ZipCode,'') As ConsigneeZipCode,
			ISNULL(CnAdd.State,'') As ConsigneeState,
			ISNULL(CAdd.TelNo,'') As ConsigneeTelNo,
			ISNULL(Cons.RegNo,'') As ConsigneeROCNo,

			ISNULL(NAdd.Address1,'') As NotifyPartyAddress1,
			ISNULL(NAdd.Address2,'') As NotifyPartyAddress2,
			ISNULL(NAdd.Address3,'') As NotifyPartyAddress3,
			ISNULL(NAdd.Address4,'') As NotifyPartyAddress4, 
			ISNULL(NAdd.City,'') As NotifyPartyCity,
			ISNULL(NAdd.CountryCode,'') As NotifyPartyCountryCode,
			ISNULL(NAdd.ZipCode,'') As NotifyPartyZipCode,
			ISNULL(NAdd.State,'') As NotifyPartyState,
			ISNULL(NAdd.TelNo,'') As NotifyPartyTelNo,
			ISNULL(Ntp.RegNo,'') As NotifyPartyROCNo,

			ISNULL(FaAdd.Address1,'') As FwdAgentAddress1,
			ISNULL(FaAdd.Address2,'') As FwdAgentAddress2,
			ISNULL(FaAdd.Address3,'') As FwdAgentAddress3,
			ISNULL(FaAdd.Address4,'') As FwdAgentAddress4, 
			ISNULL(FaAdd.City,'') As FwdAgentCity,
			ISNULL(FaAdd.CountryCode,'') As FwdAgentCountryCode,
			ISNULL(FaAdd.ZipCode,'') As FwdAgentZipCode,
			ISNULL(FaAdd.State,'') As FwdAgentState,
			ISNULL(FaAdd.TelNo,'') As FwdAgentTelNo,
			ISNULL(Fwd.RegNo,'') As FwdAgentROCNo,

			ISNULL(SaAdd.Address1,'') As ShippingAgentAddress1,
			ISNULL(SaAdd.Address2,'') As ShippingAgentAddress2,
			ISNULL(SaAdd.Address3,'') As ShippingAgentAddress3,
			ISNULL(SaAdd.Address4,'') As ShippingAgentAddress4, 
			ISNULL(SaAdd.City,'') As ShippingAgentCity,
			ISNULL(SaAdd.CountryCode,'') As ShippingAgentCountryCode,
			ISNULL(SaAdd.ZipCode,'') As ShippingAgentZipCode,
			ISNULL(SaAdd.State,'') As ShippingAgentState,
			ISNULL(SaAdd.TelNo,'') As ShippingAgentTelNo,
			ISNULL(SL.RegNo,'') As ShippingAgentROCNo,

			ISNULL(osaAdd.Address1,'') As OverseasAgentAddress1,
			ISNULL(osaAdd.Address2,'') As OverseasAgentAddress2,
			ISNULL(osaAdd.Address3,'') As OverseasAgentAddress3,
			ISNULL(osaAdd.Address4,'') As OverseasAgentAddress4, 
			ISNULL(osaAdd.City,'') As OverseasAgentCity,
			ISNULL(osaAdd.CountryCode,'') As OverseasAgentCountryCode,
			ISNULL(osaAdd.ZipCode,'') As OverseasAgentZipCode,
			ISNULL(osaAdd.State,'') As OverseasAgentState,
			ISNULL(osaAdd.TelNo,'') As OverseasAgentTelNo,
			ISNULL(osa.RegNo,'') As OverseasAgentROCNo,

			ISNULL(ffAdd.Address1,'') As FreightForwarderAddress1,
			ISNULL(ffAdd.Address2,'') As FreightForwarderAddress2,
			ISNULL(ffAdd.Address3,'') As FreightForwarderAddress3,
			ISNULL(ffAdd.Address4,'') As FreightForwarderAddress4, 
			ISNULL(ffAdd.City,'') As FreightForwarderCity,
			ISNULL(ffAdd.CountryCode,'') As FreightForwarderCountryCode,
			ISNULL(ffAdd.ZipCode,'') As FreightForwarderZipCode,
			ISNULL(ffAdd.State,'') As OFreightForwarderState,
			ISNULL(ffAdd.TelNo,'') As FreightForwarderTelNo,
			ISNULL(ff.RegNo,'') As FreightForwarderROCNo,

			OrdHd.CargoClass,OrdHd.CargoDescription,
			ISNULL(CR.LookupDescription,'') As CargoClassDescription,ISNULL(CR.LookupCode,'') As CargoClassCode,IsPartial
	FROM	[Operation].[OrderHeader] OrdHd
	Left Outer Join ModuleTypeLookup Mdl ON 
		OrdHd.ModuleID = Mdl.LookupID
	Left Outer Join JobTypeLookup Jb ON 
		OrdHd.JobType = Jb.LookupID
	Left Outer Join ShipmentTypeLookup Sp On
		OrdHd.ShipmentType = Sp.LookupID
	Left Outer Join ServiceTypeLookup Svr ON 
		OrdHd.ServiceType = Svr.LookupID 
	Left Outer Join TransportTypeLookup Tpt ON 
		OrdHd.TransportType = Tpt.LookupID
	Left Outer Join Master.JobCategory Jc ON 
		OrdHd.OrderCategory = jc.Code
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		And Cons.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Merchant Pr ON 
		OrdHd.PrincipalCode = Pr.MerchantCode 
		And Pr.IsPrincipal = CAST(1 as bit)
	Left Outer Join Master.Merchant Cus ON 
		OrdHd.CustomerCode = Cus.MerchantCode 
	Left Outer Join Master.Merchant Vndr ON 
		OrdHd.VendorCode = Vndr.MerchantCode 
		And Vndr.IsVendor = CAST(1 as bit)
	Left Outer Join Master.Merchant CoLdr ON 
		OrdHd.CoLoaderCode = CoLdr.MerchantCode 
	Left Outer Join Master.Merchant SL ON 
		OrdHd.ShippingAgent = SL.MerchantCode 
		--And SL.IsLiner= CAST(1 as bit)
	Left Outer Join Master.Merchant FA ON 
		OrdHd.ForeignAgent = FA.MerchantCode 
		And FA.IsLiner= CAST(1 as bit)
	Left Outer Join Master.Merchant Fwd ON 
		OrdHd.FwdAgent = Fwd.MerchantCode 
		And Fwd.IsForwarder= CAST(1 as bit)
	Left Outer Join Master.Operator Opr ON 
		OrdHd.PortOperator = Opr.OperatorID
	Left Outer Join Master.Merchant Drp ON
		OrdHd.DropOffCode = Drp.MerchantCode
		And Drp.IsYard = Cast(1 as bit)
	Left Outer Join Master.Merchant Ntp ON
		OrdHd.NotifyParty = Ntp.MerchantCode
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	Left Outer Join Master.Port PoR On 
		OrdHd.PlaceOfReceipt = PoR.PortCode 
	Left Outer Join Master.Port PoL ON
		OrdHd.PortOfLoading = PoL.PortCode 
	Left Outer Join Master.Port TransPort ON
		OrdHd.TranshipmentPort = Transport.PortCode
	Left Outer Join master.Port PoD ON
		OrdHd.PortOfDischarge = PoD.PortCode
	Left Outer Join master.Port PloD ON 
		OrdHd.PlaceOfDischarge = PloD.PortCode
	Left Outer Join StateTypeLookup OrdSt ON 
		OrdHd.OrderStatus = OrdSt.LookupID
	Left Outer Join StateTypeLookup OrdStweb ON 
		OrdHd.WebOrderStatus = OrdStweb.LookupID
	Left Outer Join StateTypeLookup FrgSt ON 
		OrdHd.FreightStatus = FrgSt.LookupID
	Left Outer Join StateTypeLookup FwdSt ON 
		OrdHd.FwdStatus = FwdSt.LookupID
	Left Outer Join StateTypeLookup TrnSt ON 
		OrdHd.TransportStatus = TrnSt.LookupID
	Left Outer Join StateTypeLookup HlgSt ON 
		OrdHd.HaulageStatus = HlgSt.LookupID
	Left Outer Join StateTypeLookup WhSt ON 
		OrdHd.WarehouseStatus = WhSt.LookupID
	Left Outer Join StateTypeLookup CfsSt ON 
			OrdHd.CFSStatus = CfsSt.LookupID
	Left Outer Join StateTypeLookup DptSt ON 
			OrdHd.DepotStatus = DptSt.LookupID
	Left Outer Join StateTypeLookup BillSt ON 
			OrdHd.BillingStatus = BillSt.LookupID
	Left Outer Join IncoTermsLookup Inco ON 
		OrdHd.IncoTerm = Inco.LookupID
	Left Outer Join Master.Currency Cur ON 
		OrdHd.CargoCurrencyCode = Cur.CurrencyCode
	Left Outer Join Master.Address CAdd On 
		OrdHd.CustomerCode = CAdd.LinkId
		And CAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address SAdd On 
		OrdHd.ShipperCode = SAdd.LinkId
		And SAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address CnAdd On 
		OrdHd.ConsigneeCode = CnAdd.LinkId
		And CAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address NAdd On 
		OrdHd.NotifyParty = NAdd.LinkId
		And NAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address FaAdd On 
		OrdHd.FwdAgent = FaAdd.LinkId
		And FaAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address SaAdd On 
		OrdHd.ShippingAgent = SaAdd.LinkId
		And SaAdd.IsActive = Cast(1 as bit)
	--Left Outer Join Operation.OrderCargo OC On
	--    OC.BranchId=OrdHd.[BranchID]
	Left Outer Join CargoClassDescription CR ON
		OrdHd.[CargoClass]=CR.LookupID
	Left Outer Join Master.Merchant Osa ON
		OrdHd.OverseasAgentCode = Osa.MerchantCode
	Left Outer Join Master.Merchant ff ON
		OrdHd.FregihtForwarderCode = ff.MerchantCode
	Left Outer Join Master.Address osaAdd On 
		OrdHd.OverseasAgentCode = osaAdd.LinkId
		And osaAdd.IsActive = Cast(1 as bit)
	Left Outer Join Master.Address ffAdd On 
		OrdHd.FregihtForwarderCode = ffAdd.LinkId
		And ffAdd.IsActive = Cast(1 as bit)
	WHERE	OrdHd.[BranchID] = @BranchID 
		  And OrdHd.[IsCancel] = CAST(0 as bit) 
	 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderList] 
-- ========================================================================================================================================
GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderInsert] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
    @OrderDate datetime,
    @MasterJobNo nvarchar(70),
    @ModuleID smallint,
    @JobType smallint,
    @ShipmentType smallint,
    @ServiceType smallint,
    @TransportType smallint,
    @IncoTerm smallint,
    @OrderCategory nvarchar(50),
    @ShipperCode bigint,
    @ConsigneeCode bigint,
    @PrincipalCode bigint,
    @CustomerCode bigint,
    @VendorCode bigint,
    @CoLoaderCode bigint,
    @ShippingAgent bigint,
    @ForeignAgent bigint,
    @FwdAgent bigint,
    @PortOperator bigint,
    @VesselScheduleID int,
    @VesselID nvarchar(20),
    @Voyageno nvarchar(10),
    @FeederVoyageNo nvarchar(10),
    @CustomerRef nvarchar(100),
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ShippingLinerRefNo nvarchar(100),
    @ShippingNote nvarchar(100),
    @KANo nvarchar(100),
	@ManifestNo nvarchar(100),
    @SCNNo nvarchar(50),
    @PlaceOfReceipt nvarchar(10),
    @PortOfLoading nvarchar(10),
    @TranshipmentPort nvarchar(10),
    @PortOfDischarge nvarchar(10),
    @PlaceOfDischarge nvarchar(10),
    @DropOffCode nvarchar(10),
    @ClosingDate datetime,
    @ClosingDateReefer datetime,
    @ETA datetime,
    @ETD datetime,
    @YardCutOffTime datetime,
    @YardCutOffTimeReefer datetime,
    @IsMasterJob bit,
	@Remarks nvarchar(100),
	@CargoCurrencyCode nvarchar(3),
	@CargoExchangeRate float,
	@CargoBaseAmount numeric(18,4),
	@CargoLocalAmount numeric(18,4),
	@PrimaryYard nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NotifyParty bigint,
	@NotifyPartyAddressID INT,
	@CustomerAddressID INT,
	@ShipperAddressID INT,
	@ConsigneeAddressID INT,
	@InvoiceNo nvarchar(35),
	@InvoiceAmount decimal(18,4),
	@InvoiceCurrency nvarchar(3),
	@LocalAmount decimal(18,4),
	@ExchangeRate decimal(18,4),
	@PONo nvarchar(35),
	@COLoaderBLNo nvarchar(35),
	@ContractNo nvarchar(35),
	@SalesOrderNo nvarchar(35),
	@SalesOrderDate DateTime,
	@SalesTerm nvarchar(100),
	@PaymentTerm nvarchar(100),
	@VehicleNo1 nvarchar(35),
	@VehicleNo2 nvarchar(35),
	@WagonNo nvarchar(35),
	@FlightNo nvarchar(35),
	@ARNNo nvarchar(35),
	@MasterAWBNo nvarchar(35),
	@HouseAWBNo nvarchar(35),
	@AIRCOLoadNo nvarchar(35),
	@JKNo nvarchar(35),
	@InvoiceDate datetime, 
	@ShippingAgentAddressID INT,
	@FwdAgentAddressID INT,
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@StowageCode smallint,
	@ExtraCargoDescription nvarchar(300),
	@IsPartial bit =0,
	@Flag bit,
	@OverseasAgentCode bigint,
	@OverseasAgentAddressID int,
	@FregihtForwarderCode bigint,
	@FreightForwarderAddressID int,
	@WebOrderStatus smallint,
	@NewDocumentNo varchar(50) OUTPUT
    
AS 
  

BEGIN
 
Declare @OrderStatus smallint =0

Declare @ActivityParty smallint=0;

		 

		Select @OrderStatus = LookupID 
		From Config.Lookup 
		Where LookupCategory='StateType' And LookupCode='CREATED'

		 

		Declare @FwdAgentCompanyID bigInt=0;
		/*TODO : Replace the below code with the correct mapping (Agent Address from Branch ) */
		--Select @FwdAgentCompanyID = 
		--ISNull(C.CompanyCode,0)
		--From Master.Company C
		--Left Outer Join Master.Branch Br ON 
		--	C.CompanyCode = Br.CompanyCode
		--Where BranchID = @FwdAgent

		Declare @vMessageID uniqueidentifier 
		Select @vMessageID = NewID();
 
		if(@WebOrderStatus = 0)
		Begin 
			Select @WebOrderStatus = LookupID
			From Config.Lookup 
			Where LookupCategory='WebOrderStatus' And LookupCode='WEB-ORIGINAL'
		End
	
	INSERT INTO [Operation].[OrderHeader] (
			[BranchID], [OrderNo], [OrderDate], [MasterJobNo], [ModuleID], [JobType], [ShipmentType], [ServiceType], [TransportType], 
			[IncoTerm], [OrderCategory], [ShipperCode], [ConsigneeCode], [PrincipalCode], [CustomerCode], [VendorCode], [CoLoaderCode], 
			[ShippingAgent], [ForeignAgent], [FwdAgent], [PortOperator], [VesselScheduleID], [VesselID], [Voyageno], [FeederVoyageNo], 
			[CustomerRef], [OceanBLNo], [HouseBLNo], [ShippingLinerRefNo], [ShippingNote], [KANo],ManifestNo, [SCNNo], [PlaceOfReceipt], [PortOfLoading], 
			[TranshipmentPort], [PortOfDischarge], [PlaceOfDischarge], [DropOffCode], [ClosingDate], [ClosingDateReefer], [ETA], [ETD], 
			[YardCutOffTime], [YardCutOffTimeReefer],[OrderStatus], [IsMasterJob],Remarks, 
			[CargoCurrencyCode],[CargoExchangeRate],[CargoBaseAmount],[CargoLocalAmount],PrimaryYard,[CreatedBy], [CreatedOn] ,
			[WebOrderStatus],[FwdAgentCompanyID],
			NotifyParty,NotifyPartyAddressID,CustomerAddressID,ShipperAddressID,ConsigneeAddressID,
			InvoiceNo,InvoiceAmount,InvoiceCurrency,LocalAmount,ExchangeRate,PONo,COLoaderBLNo,ContractNo,
			SalesOrderNo,SalesOrderDate,SalesTerm,PaymentTerm,VehicleNo1,VehicleNo2,WagonNo,
			FlightNo,ARNNo,MasterAWBNo,HouseAWBNo,AIRCOLoadNo,JKNo,InvoiceDate,ShippingAgentAddressID,FwdAgentAddressID,CargoClass,CargoDescription,
			MessageID,StowageCode,ExtraCargoDescription,IsPartial,OverseasAgentCode,OverseasAgentAddressID,FregihtForwarderCode,FreightForwarderAddressID)
	SELECT	@BranchID, @OrderNo, @OrderDate, @MasterJobNo, @ModuleID, @JobType, @ShipmentType, @ServiceType, @TransportType, 
			@IncoTerm, @OrderCategory, @ShipperCode, @ConsigneeCode, @PrincipalCode, @CustomerCode, @VendorCode, @CoLoaderCode, 
			@ShippingAgent, @ForeignAgent, @FwdAgent, @PortOperator, @VesselScheduleID, @VesselID, @Voyageno, @FeederVoyageNo, 
			@CustomerRef, @OceanBLNo, @HouseBLNo, @ShippingLinerRefNo, @ShippingNote, @KANo,@ManifestNo, @SCNNo, @PlaceOfReceipt, @PortOfLoading, 
			@TranshipmentPort, @PortOfDischarge, @PlaceOfDischarge, @DropOffCode, @ClosingDate, @ClosingDateReefer, @ETA, @ETD, 
			@YardCutOffTime, @YardCutOffTimeReefer,@OrderStatus, @IsMasterJob,@Remarks, 
			@CargoCurrencyCode,@CargoExchangeRate,@CargoBaseAmount,@CargoLocalAmount,@PrimaryYard,@CreatedBy, GETUTCDATE() ,
			@WebOrderStatus,@FwdAgentCompanyID,
			@NotifyParty,@NotifyPartyAddressID,@CustomerAddressID,@ShipperAddressID,@ConsigneeAddressID,
			@InvoiceNo,@InvoiceAmount,@InvoiceCurrency,@LocalAmount,@ExchangeRate,@PONo,@COLoaderBLNo,@ContractNo,
			@SalesOrderNo,@SalesOrderDate,@SalesTerm,@PaymentTerm,@VehicleNo1,@VehicleNo2,@WagonNo,
			@FlightNo,@ARNNo,@MasterAWBNo,@HouseAWBNo,@AIRCOLoadNo,@JKNo,@InvoiceDate,@ShippingAgentAddressID,@FwdAgentAddressID,@CargoClass,@CargoDescription,
			@vMessageID,@StowageCode,@ExtraCargoDescription,@IsPartial,@OverseasAgentCode,@OverseasAgentAddressID,@FregihtForwarderCode,@FreightForwarderAddressID

			 


		Select @ActivityParty= LookupID From Config.Lookup 
		Where LookupCategory='OrderActivityParty' And LookupCode='OA-TR'
	
		Insert Into Operation.OrderActivity
		Select @BranchID,@OrderNo,'ORDER CREATED',GETUTCDATE(),@ActivityParty,@CreatedBy,GETUTCDATE()

		Select @NewDocumentNo = @OrderNo

		if(@Flag = 1 AND @IsPartial = 0)
		Begin
		 Select @vMessageID = NewID();
		 Insert into EDI.InsuranceHeader(BranchID, OrderNo,CreateDateTime,MessageFunction, EDIDateTime, FileName,MessageID)
		 Values(@BranchID, @NewDocumentNo,GETUTCDATE(),'02', null, null,@vMessageID)
		End
 	
               
END

go



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderHeader] Record Into [OrderHeader] Table.

-- ===================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderUpdate] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
    @OrderDate datetime,
    @MasterJobNo nvarchar(70),
    @ModuleID smallint,
    @JobType smallint,
    @ShipmentType smallint,
    @ServiceType smallint,
    @TransportType smallint,
    @IncoTerm smallint,
    @OrderCategory nvarchar(50),
    @ShipperCode bigint,
    @ConsigneeCode bigint,
    @PrincipalCode bigint,
    @CustomerCode bigint,
    @VendorCode bigint,
    @CoLoaderCode bigint,
    @ShippingAgent bigint,
    @ForeignAgent bigint,
    @FwdAgent bigint,
    @PortOperator bigint,
    @VesselScheduleID int,
    @VesselID nvarchar(20),
    @Voyageno nvarchar(10),
    @FeederVoyageNo nvarchar(10),
    @CustomerRef nvarchar(100),
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ShippingLinerRefNo nvarchar(100),
    @ShippingNote nvarchar(100),
    @KANo nvarchar(100),
	@ManifestNo nvarchar(100),
    @SCNNo nvarchar(50),
    @PlaceOfReceipt nvarchar(10),
    @PortOfLoading nvarchar(10),
    @TranshipmentPort nvarchar(10),
    @PortOfDischarge nvarchar(10),
    @PlaceOfDischarge nvarchar(10),
    @DropOffCode nvarchar(10),
    @ClosingDate datetime,
    @ClosingDateReefer datetime,
    @ETA datetime,
    @ETD datetime,
    @YardCutOffTime datetime,
    @YardCutOffTimeReefer datetime,
    @IsMasterJob bit,
	@Remarks nvarchar(100),
	@CargoCurrencyCode nvarchar(3),
	@CargoExchangeRate float,
	@CargoBaseAmount numeric(18,4),
	@CargoLocalAmount numeric(18,4),
	@PrimaryYard nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NotifyParty bigint,
	@NotifyPartyAddressID INT,
	@CustomerAddressID INT,
	@ShipperAddressID INT,
	@ConsigneeAddressID INT,
	@InvoiceNo nvarchar(35),
	@InvoiceAmount decimal(18,4),
	@InvoiceCurrency nvarchar(3),
	@LocalAmount decimal(18,4),
	@ExchangeRate decimal(18,4),
	@PONo nvarchar(35),
	@COLoaderBLNo nvarchar(35),
	@ContractNo nvarchar(35),
	@SalesOrderNo nvarchar(35),
	@SalesOrderDate DateTime,
	@SalesTerm nvarchar(100),
	@PaymentTerm nvarchar(100),
	@VehicleNo1 nvarchar(35),
	@VehicleNo2 nvarchar(35),
	@WagonNo nvarchar(35),
	@FlightNo nvarchar(35),
	@ARNNo nvarchar(35),
	@MasterAWBNo nvarchar(35),
	@HouseAWBNo nvarchar(35),
	@AIRCOLoadNo nvarchar(35),
	@JKNo nvarchar(35),
	@InvoiceDate datetime, 
	@ShippingAgentAddressID INT,
	@FwdAgentAddressID INT,
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@StowageCode smallint,
	@ExtraCargoDescription nvarchar(300),
	@IsPartial bit =0,
	@Flag bit,
	@OverseasAgentCode bigint,
	@OverseasAgentAddressID int,
	@FregihtForwarderCode bigint,
	@FreightForwarderAddressID int,
	@NewDocumentNo varchar(50) OUTPUT
AS 
 
	
BEGIN

Declare @ActivityParty smallint=0; 
		Declare @FwdAgentCompanyID bigInt;
		Select @FwdAgentCompanyID = Convert(bigint,ISNULL(EDIMappingCode,0))
		From Master.Merchant
		Where MerchantCode = @FwdAgent

		/*TODO : Replace the below code with the correct mapping (Agent Address from Branch ) */
		Select @FwdAgentCompanyID = 0
		--ISNull(C.CompanyCode,0)
		--From Master.Company C
		--Left Outer Join Master.Branch Br ON 
		--	C.CompanyCode = Br.CompanyCode
		--Where BranchID = @FwdAgent

	UPDATE	[Operation].[OrderHeader]
	SET		[OrderDate] = @OrderDate, [MasterJobNo] = @MasterJobNo, [ModuleID] = @ModuleID, [JobType] = @JobType, 
			[ShipmentType] = @ShipmentType, [ServiceType] = @ServiceType, [TransportType] = @TransportType, [IncoTerm] = @IncoTerm, 
			[OrderCategory] = @OrderCategory, [ShipperCode] = @ShipperCode, [ConsigneeCode] = @ConsigneeCode, [PrincipalCode] = @PrincipalCode, 
			[CustomerCode] = @CustomerCode, [VendorCode] = @VendorCode, [CoLoaderCode] = @CoLoaderCode, [ShippingAgent] = @ShippingAgent, 
			[ForeignAgent] = @ForeignAgent, [FwdAgent] = @FwdAgent, [PortOperator] = @PortOperator, [VesselScheduleID] = @VesselScheduleID, 
			[VesselID] = @VesselID, [Voyageno] = @Voyageno, [FeederVoyageNo] = @FeederVoyageNo, [CustomerRef] = @CustomerRef, 
			[OceanBLNo] = @OceanBLNo, [HouseBLNo] = @HouseBLNo, [ShippingLinerRefNo] = @ShippingLinerRefNo, [ShippingNote] = @ShippingNote, 
			[KANo] = @KANo,ManifestNo=@ManifestNo, [SCNNo] = @SCNNo, [PlaceOfReceipt] = @PlaceOfReceipt, [PortOfLoading] = @PortOfLoading, 
			[TranshipmentPort] = @TranshipmentPort, [PortOfDischarge] = @PortOfDischarge, [PlaceOfDischarge] = @PlaceOfDischarge, 
			[DropOffCode] = @DropOffCode, [ClosingDate] = @ClosingDate, [ClosingDateReefer] = @ClosingDateReefer, [ETA] = @ETA, [ETD] = @ETD, 
			[YardCutOffTime] = @YardCutOffTime, [YardCutOffTimeReefer] = @YardCutOffTimeReefer, [IsMasterJob] = @IsMasterJob, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),Remarks  =@Remarks ,
			[CargoCurrencyCode]=@CargoCurrencyCode,[CargoExchangeRate]=@CargoExchangeRate,[CargoBaseAmount]=@CargoBaseAmount,
			[CargoLocalAmount]=@CargoLocalAmount,[PrimaryYard]=@PrimaryYard,FwdAgentCompanyID =@FwdAgentCompanyID,
			NotifyParty=@NotifyParty,NotifyPartyAddressID=@NotifyPartyAddressID,CustomerAddressID=@CustomerAddressID,
			ShipperAddressID=@ShipperAddressID,ConsigneeAddressID=@ConsigneeAddressID,
			InvoiceNo=@InvoiceNo,InvoiceAmount=@InvoiceAmount,InvoiceCurrency=@InvoiceCurrency,LocalAmount=@LocalAmount,
			ExchangeRate=@ExchangeRate,PONo=@PONo,COLoaderBLNo=@COLoaderBLNo,ContractNo=@ContractNo,
			SalesOrderNo=@SalesOrderNo,SalesOrderDate=@SalesOrderDate,SalesTerm=@SalesTerm,PaymentTerm=@PaymentTerm,
			VehicleNo1=@VehicleNo1,VehicleNo2=@VehicleNo2,WagonNo=@WagonNo,
			FlightNo=@FlightNo,ARNNo=@ARNNo,MasterAWBNo=@MasterAWBNo,HouseAWBNo=@HouseAWBNo,AIRCOLoadNo=@AIRCOLoadNo,JKNo=@JKNo,InvoiceDate=@InvoiceDate,
			ShippingAgentAddressID=@ShippingAgentAddressID,FwdAgentAddressID=@FwdAgentAddressID,CargoClass=@CargoClass,CargoDescription=@CargoDescription,
			StowageCode = @StowageCode, ExtraCargoDescription = @ExtraCargoDescription,IsPartial = @IsPartial,
			OverseasAgentCode=@OverseasAgentCode,OverseasAgentAddressID=@OverseasAgentAddressID,FregihtForwarderCode=@FregihtForwarderCode,
			FreightForwarderAddressID=@FreightForwarderAddressID
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo

		Select @ActivityParty= LookupID From Config.Lookup 
		Where LookupCategory='OrderActivityParty' And LookupCode='OA-FA'
	
		Insert Into Operation.OrderActivity
		Select @BranchID,@OrderNo,'ORDER MODIFIED',GETUTCDATE(),@ActivityParty,@CreatedBy,GETUTCDATE()
	
	/*
	if(@Flag=1)
	Begin
		Declare @vMessageID uniqueIdentifier;
		Select @vMessageID = NewID();
		Insert into EDI.InsuranceHeader(BranchID, OrderNo,CreateDateTime,MessageFunction, EDIDateTime, FileName,MessageID)
		Values(@BranchID, @OrderNo,GETUTCDATE(),'05', null, null,@vMessageID)
	END
	*/
	if(Select Count(0) From EDI.InsuranceHeader Where BranchID = @BranchID and OrderNo = @OrderNo) > 0
	 Begin
	  Declare @vMessageID uniqueIdentifier;
	  Declare @vUUID nvarchar(100)='';

		Select TOP(1) @vUUID = ISNULL(UUID,'') From EDI.InsuranceHeader Where BranchID = @BranchID and OrderNo = @OrderNo
		And LEN(UUID)>0

		Select @vMessageID = NewID();
		Insert into EDI.InsuranceHeader(BranchID, OrderNo,CreateDateTime,MessageFunction, EDIDateTime, FileName,MessageID,UUID)
		Values(@BranchID, @OrderNo,GETUTCDATE(),'05', null, null,@vMessageID,@vUUID)
	 End
	Else
	 Begin
	  Select @vMessageID = NewID();
		 Insert into EDI.InsuranceHeader(BranchID, OrderNo,CreateDateTime,MessageFunction, EDIDateTime, FileName,MessageID)
		 Values(@BranchID, @OrderNo,GETUTCDATE(),'02', null, null,@vMessageID)
	 End
	Select @NewDocumentNo = @OrderNo
	 
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderHeader] Record Into [OrderHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderSave] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
    @OrderDate datetime,
    @MasterJobNo nvarchar(70),
    @ModuleID smallint,
    @JobType smallint,
    @ShipmentType smallint,
    @ServiceType smallint,
    @TransportType smallint,
    @IncoTerm smallint,
    @OrderCategory nvarchar(50),
    @ShipperCode bigint,
    @ConsigneeCode bigint,
    @PrincipalCode bigint,
    @CustomerCode bigint,
    @VendorCode bigint,
    @CoLoaderCode bigint,
    @ShippingAgent bigint,
    @ForeignAgent bigint,
    @FwdAgent bigint,
    @PortOperator bigint,
    @VesselScheduleID int,
    @VesselID nvarchar(20),
    @Voyageno nvarchar(10),
    @FeederVoyageNo nvarchar(10),
    @CustomerRef nvarchar(100),
    @OceanBLNo nvarchar(100),
    @HouseBLNo nvarchar(100),
    @ShippingLinerRefNo nvarchar(100),
    @ShippingNote nvarchar(100),
    @KANo nvarchar(100),
	@ManifestNo nvarchar(100),
    @SCNNo nvarchar(50),
    @PlaceOfReceipt nvarchar(10),
    @PortOfLoading nvarchar(10),
    @TranshipmentPort nvarchar(10),
    @PortOfDischarge nvarchar(10),
    @PlaceOfDischarge nvarchar(10),
    @DropOffCode nvarchar(10),
    @ClosingDate datetime,
    @ClosingDateReefer datetime,
    @ETA datetime,
    @ETD datetime,
    @YardCutOffTime datetime,
    @YardCutOffTimeReefer datetime,
    @IsMasterJob bit,
	@Remarks nvarchar(100),
	@CargoCurrencyCode nvarchar(3),
	@CargoExchangeRate float,
	@CargoBaseAmount numeric(18,4),
	@CargoLocalAmount numeric(18,4),
	@PrimaryYard nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@NotifyParty bigint,
	@NotifyPartyAddressID INT,
	@CustomerAddressID INT,
	@ShipperAddressID INT,
	@ConsigneeAddressID INT,
	@InvoiceNo nvarchar(35),
	@InvoiceAmount decimal(18,4),
	@InvoiceCurrency nvarchar(3),
	@LocalAmount decimal(18,4),
	@ExchangeRate decimal(18,4),
	@PONo nvarchar(35),
	@COLoaderBLNo nvarchar(35),
	@ContractNo nvarchar(35),
	@SalesOrderNo nvarchar(35),
	@SalesOrderDate DateTime,
	@SalesTerm nvarchar(100),
	@PaymentTerm nvarchar(100),
	@VehicleNo1 nvarchar(35),
	@VehicleNo2 nvarchar(35),
	@WagonNo nvarchar(35),
	@FlightNo nvarchar(35),
	@ARNNo nvarchar(35),
	@MasterAWBNo nvarchar(35),
	@HouseAWBNo nvarchar(35),
	@AIRCOLoadNo nvarchar(35),
	@JKNo nvarchar(35),
	@InvoiceDate datetime, 
	@ShippingAgentAddressID INT,
	@FwdAgentAddressID INT,
	@CargoClass smallint,
	@CargoDescription nvarchar(150),
	@StowageCode smallint,
	@ExtraCargoDescription nvarchar(300),
	@IsPartial bit =0,
	@Flag bit,
	@OverseasAgentCode bigint,
	@OverseasAgentAddressID int,
	@FregihtForwarderCode bigint,
	@FreightForwarderAddressID int,	
	@WebOrderStatus smallint,
	@NewDocumentNo varchar(50) OUTPUT
AS 
 

BEGIN 
	IF (SELECT COUNT(0) FROM [Operation].[OrderHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo)>0
	BEGIN
		
		print 'update'

	    Exec [Operation].[usp_OrderHeaderUpdate] 
			@BranchID, @OrderNo, @OrderDate, @MasterJobNo, @ModuleID, @JobType, @ShipmentType, @ServiceType, @TransportType, @IncoTerm, 
			@OrderCategory, @ShipperCode, @ConsigneeCode, @PrincipalCode, @CustomerCode, @VendorCode, @CoLoaderCode, @ShippingAgent, 
			@ForeignAgent, @FwdAgent, @PortOperator, @VesselScheduleID, @VesselID, @Voyageno, @FeederVoyageNo, @CustomerRef, @OceanBLNo, 
			@HouseBLNo, @ShippingLinerRefNo, @ShippingNote, @KANo,@ManifestNo, @SCNNo, @PlaceOfReceipt, @PortOfLoading, @TranshipmentPort, 
			@PortOfDischarge, @PlaceOfDischarge, @DropOffCode, @ClosingDate, @ClosingDateReefer, @ETA, @ETD, @YardCutOffTime, @YardCutOffTimeReefer, 
			@IsMasterJob,@Remarks,@CargoCurrencyCode,@CargoExchangeRate,@CargoBaseAmount,@CargoLocalAmount,@PrimaryYard, @CreatedBy, 
			@ModifiedBy ,@NotifyParty,@NotifyPartyAddressID,@CustomerAddressID,@ShipperAddressID,@ConsigneeAddressID,
			@InvoiceNo,@InvoiceAmount,@InvoiceCurrency,@LocalAmount,@ExchangeRate,@PONo,@COLoaderBLNo,@ContractNo,@SalesOrderNo,@SalesOrderDate,@SalesTerm,
			@PaymentTerm,@VehicleNo1,@VehicleNo2,@WagonNo,@FlightNo,@ARNNo,@MasterAWBNo,@HouseAWBNo,@AIRCOLoadNo,@JKNo,@InvoiceDate,
			@ShippingAgentAddressID,@FwdAgentAddressID,@CargoClass,@CargoDescription,@StowageCode,@ExtraCargoDescription,@IsPartial,
			@Flag,@OverseasAgentCode,@OverseasAgentAddressID,@FregihtForwarderCode,@FreightForwarderAddressID,
			@NewDocumentNo = @NewDocumentNo OUTPUT


	END
	ELSE
	BEGIN

		print 'Insert'

		Declare @Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@BookingTypeDesc = LookupDescription,@OrderNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@JobType
		
		print @BookingTypeDesc
		
		IF @BookingTypeDesc='Import' 
			SET @DocID ='Opr\Order(Import)'
		ELSE IF @BookingTypeDesc='Export' 
			SET @DocID ='Opr\Order(Export)'
		ELSE IF @BookingTypeDesc='Pos-In' 
			SET @DocID ='Opr\Order(Pos-In)'
		ELSE IF @BookingTypeDesc='Pos-Out' 
			SET @DocID ='Opr\Order(Pos-Out)'
		ELSE IF @BookingTypeDesc='Local' 
			SET @DocID ='Opr\Order(Local)'
		ELSE IF @BookingTypeDesc='Repo' 
			SET @DocID ='Opr\Order(Repo)'
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1001001, 'Opr\Order(Import)', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		--select @BranchID, @DocID, @Dt ,@CreatedBy
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @OrderNo OUTPUT

		 


	    Exec [Operation].[usp_OrderHeaderInsert] 
			@BranchID, @OrderNo, @OrderDate, @MasterJobNo, @ModuleID, @JobType, @ShipmentType, @ServiceType, @TransportType, @IncoTerm, 
			@OrderCategory, @ShipperCode, @ConsigneeCode, @PrincipalCode, @CustomerCode, @VendorCode, @CoLoaderCode, @ShippingAgent, 
			@ForeignAgent, @FwdAgent, @PortOperator, @VesselScheduleID, @VesselID, @Voyageno, @FeederVoyageNo, @CustomerRef, @OceanBLNo, 
			@HouseBLNo, @ShippingLinerRefNo, @ShippingNote, @KANo,@ManifestNo, @SCNNo, @PlaceOfReceipt, @PortOfLoading, @TranshipmentPort, 
			@PortOfDischarge, @PlaceOfDischarge, @DropOffCode, @ClosingDate, @ClosingDateReefer, @ETA, @ETD, @YardCutOffTime, @YardCutOffTimeReefer, 
			@IsMasterJob,@Remarks,@CargoCurrencyCode,@CargoExchangeRate,@CargoBaseAmount,@CargoLocalAmount,@PrimaryYard, @CreatedBy, @ModifiedBy ,
			@NotifyParty,@NotifyPartyAddressID,@CustomerAddressID,@ShipperAddressID,@ConsigneeAddressID,
			@InvoiceNo,@InvoiceAmount,@InvoiceCurrency,@LocalAmount,@ExchangeRate,@PONo,@COLoaderBLNo,@ContractNo,
			@SalesOrderNo,@SalesOrderDate,@SalesTerm,@PaymentTerm,@VehicleNo1,@VehicleNo2,@WagonNo,
			@FlightNo,@ARNNo,@MasterAWBNo,@HouseAWBNo,@AIRCOLoadNo,@JKNo,@InvoiceDate,@ShippingAgentAddressID,@FwdAgentAddressID,@CargoClass,@CargoDescription,
			@StowageCode,@ExtraCargoDescription,@IsPartial,@Flag,@OverseasAgentCode,@OverseasAgentAddressID,@FregihtForwarderCode,@FreightForwarderAddressID,
			@WebOrderStatus,
			@NewDocumentNo = @NewDocumentNo OUTPUT

	END
	 
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderHeaderSave]
-- ========================================================================================================================================


GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderHeader] Record  based on [OrderHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderDelete] 
    @BranchID BIGINT,
    @OrderNo varchar(70),
	@CancelBy nvarchar(60)
AS 

	
BEGIN 
	
Declare @OrderStatus smallint=0;

	Select @OrderStatus = LookupID From Config.Lookup 
	Where LookupCategory='StateType' And LookupCode='CANCELLED'

	UPDATE	[Operation].[OrderHeader]
	SET	IsCancel = CAST(1 as bit),CancelBy=@CancelBy,CancelOn = GETUTCDATE(),OrderStatus=@OrderStatus
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo

		Declare @vMessageID uniqueIdentifier;
		Select @vMessageID = NewID();
 		Insert into EDI.InsuranceHeader(BranchID, OrderNo,CreateDateTime,MessageFunction, EDIDateTime, FileName,MessageID)
		Values(@BranchID, @OrderNo,GETUTCDATE(),'03', null, null,@vMessageID)

SET NOCOUNT OFF;

END


-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderDelete]
-- ========================================================================================================================================


GO


IF OBJECT_ID('[Operation].[usp_OrderInquirySearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderInquirySearch] 
END 
GO
CREATE Procedure [Operation].[usp_OrderInquirySearch]
@branchID bigint,
@declarationNo varchar(30),
@manifest varchar(20),
@oceanBLNo varchar(20),
@houseBLNo varchar(20),
@voyageNo varchar(20),
@dateFrom datetime,
@dateTo datetime
As
Begin
  declare @sql nvarchar(max);
  declare @maxSearchRecord int;
  select @maxSearchRecord = 500;

  Set @sql = 'Select OrdHdr.[BranchID], OrdHdr.[OrderNo], MasVes.[VesselName], OrdHdr.[VoyageNo], OrdHdr.[HouseBLNo], OrdHdr.[ManifestNo], MasMer.[MerchantName] as ShipperName, MasMer2.[MerchantName] as ConsigneeName, OrdHdr.[OrderDate]  
  From Operation.OrderHeader OrdHdr
  Left Outer Join Operation.OrderContainer OrdCon
  On OrdHdr.OrderNo = OrdCon.OrderNo
  Left Outer Join Master.Vessel MasVes
  On OrdHdr.VesselID = MasVes.VesselID
  Left Outer Join Master.Merchant MasMer
  On OrdHdr.ShipperCode = MasMer.MerchantCode
  Left Outer Join Master.Merchant MasMer2
  On OrdHdr.ConsigneeCode = MasMer2.MerchantCode';
  
  Set @sql += ' WHERE OrdHdr.[BranchID] = ' +  Convert(varchar(20),@branchID)+' AND OrdHdr.[IsCancel] = CAST(0 as bit)'

  IF (LEN(RTRIM(@declarationNo)) > 0)
	Set @sql += ' And OrdHdr.OrderNo like ''%' + @declarationNo + '%''';

  IF (LEN(RTRIM(@manifest)) > 0)
	Set @sql += ' And OrdHdr.ManifestNo like ''%' + @manifest + '%''';

  IF (LEN(RTRIM(@oceanBLNo)) > 0)
	Set @sql += ' And OrdHdr.OceanBLNo like ''%' + @oceanBLNo + '%''';

  IF (LEN(RTRIM(@houseBLNo)) > 0)
	Set @sql += ' And OrdHdr.HouseBLNo like ''%' + @houseBLNo + '%''';

  IF (LEN(RTRIM(@voyageNo)) > 0)
	Set @sql += ' And OrdHdr.VoyageNo like ''%' + @voyageNo + '%''';
  
  IF (ISNULL(@dateFrom,'') > 0) 
    set @sql = @sql + ' And Convert(Char(10),OrdHdr.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@dateFrom,120) + ''',OrdHdr.OrderDate)';

  IF (ISNULL(@dateTo,'') > 0) 
    set @sql = @sql + ' And Convert(Char(10),OrdHdr.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@dateTo,120) + ''',OrdHdr.OrderDate)';

  print @sql;
  exec sp_executesql @sql , N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;
  
   
End
GO



IF OBJECT_ID('[Operation].[usp_UpdateDeclarationNoInOrderEntry]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_UpdateDeclarationNoInOrderEntry] 
END 
GO
Create Procedure [Operation].[usp_UpdateDeclarationNoInOrderEntry]
@BranchID bigint,
@OrderNo varchar(20),
@DeclarationNo varchar(20)

As
Begin
 Update Operation.OrderContainer Set DeclarationNo = @DeclarationNo Where BranchID = @BranchID and OrderNo = @OrderNo
End
 
 GO
 

-- ========================================================================================================================================
-- START											 [Operation].[usp_DashBoardData]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Get Data for dashboard widgets

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DashBoardData]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DashBoardData] 
END 
GO
CREATE PROC [Operation].[usp_DashBoardData] 
	 
AS 
BEGIN
	Declare 
	@Subscribercount int,
	@OrderHeadercount int,
	@K1Declarationcount int,
	@K2Declarationcount int,
	@InvoiceAmount decimal(18,4)
	

	set @Subscribercount = (select count(0) AS Subscribercount from master.company where companycode <> 1000 AND (YEAR(CreatedOn)=YEAR(getdate()) AND MONTH(CreatedOn)=MONTH(getdate()))) 
	set @OrderHeadercount = (select count(0) AS OrderHeadercount from operation.orderheader where iscancel = CAST(0 as bit) AND (YEAR(CreatedOn)=YEAR(getdate()) AND MONTH(CreatedOn)=MONTH(getdate())))
	set @K1Declarationcount = (select count(0) AS K1Declarationcount from operation.Declarationheader where IsActive = CAST(1 as bit) AND (YEAR(CreatedOn)=YEAR(getdate()) AND MONTH(CreatedOn)=MONTH(getdate())))
	set @K2Declarationcount= (select count(0) AS K2Declarationcount from operation.DeclarationheaderK2 where IsActive = CAST(1 as bit) AND (YEAR(CreatedOn)=YEAR(getdate()) AND MONTH(CreatedOn)=MONTH(getdate())))
 
 SELECT @Subscribercount Subscribercount, @OrderHeadercount OrderHeadercount, @K1Declarationcount K1Declarationcount, @K2Declarationcount K2Declarationcount, @InvoiceAmount InvoiceAmount

END


-- ========================================================================================================================================
-- END  											 [Operation].[usp_DashBoardData]
-- ========================================================================================================================================

GO
 

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationHeaderSelectByOrderNo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Get Data for dashboard widgets

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationHeaderSelectByOrderNo]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationHeaderSelectByOrderNo] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationHeaderSelectByOrderNo]
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 

BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			CargoClassDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'CargoClass')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo], Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], 
						Hd.[ExporterState], Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.[CreatedBy], Hd.[CreatedOn],Hd.[ModifiedBy], Hd.[ModifiedOn],Hd.CustomStationCode,Hd.ShippingAgent,Hd.DeclarantID,
						Hd.DeclarantName,Hd.DeclarantNRIC,Hd.DeclarantDesignation,HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],

						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,CRH.RegistrationDate,
						ISNULL(Sh.LoadingPort + ' - ' + LP.PortName,'') As LoadingPortName,
						ISNULL(sh.DischargePort + ' - ' + DP.PortName,'') As DischargePortName,
						ISNULL(sh.TranshipmentPort + ' - ' + TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As ImporterName,
						SA.MerchantName As ShippingAgentName,
						TR.LookupCode As TransactionTypeCode,
						Ex.MerchantName As ExporterName,
						Hd.CargoClass,Hd.CargoDescription,
						ISNULL(CR.LookupDescription,'') As CargoClassDescription,ISNULL(CR.LookupCode,'') As CargoClassCode,
						CustomerReferenceNo,ExtraCargoDescription,Hd.MarksAndNos,Hd.DeclarationShipmentType
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	Left OUter JOin Master.Merchant Ex ON 
		Hd.Exporter = Convert(varchar(10),Ex.MerchantCode)
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = SA.MerchantCode
	Left Outer Join EDI.CustomResponseHeader CRH ON
	    Hd.[BranchID]=CRH.BranchID and 
		Hd.[DeclarationNo]=CRH.DeclarationNo
	Left Outer Join CargoClassDescription CR ON
		Hd.[CargoClass]=CR.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[OrderNo] = @OrderNo  
END


-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationHeaderSelectByOrderNo]
-- ========================================================================================================================================
GO


 



-- ========================================================================================================================================
-- START											[EDI].[usp_GetUUIDFromInsuranceHeader]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Get UUID from Insurance Header

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_GetUUIDFromInsuranceHeader]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GetUUIDFromInsuranceHeader]
END 
GO
CREATE PROC [EDI].[usp_GetUUIDFromInsuranceHeader] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
As
Begin

 Select [BranchId],[InsuranceReferenceNo],[OrderNo],[UUID] from[EDI].[InsuranceHeader]
   where orderno = @OrderNo AND BranchID =@BranchID order by [InsuranceReferenceNo] desc

END


-- ========================================================================================================================================
-- END  											 [EDI].[usp_GetUUIDFromInsuranceHeader]
-- ========================================================================================================================================


GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderEntryRecordCount]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	12-Apr-2017
-- Description:	Get Order header record count

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderEntryRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderEntryRecordCount]
END 
GO
CREATE Procedure [Operation].[usp_OrderEntryRecordCount]
	@BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@HouseBLNo nvarchar(50),
	@ManifestNo nvarchar(100),
	@ShipperName nvarchar(100),
	@ConsigneeName nvarchar(100),
	@DateFrom datetime=NULL,
	@DateTo datetime = NULL

As
Begin
 
 declare @sql nvarchar(max);

	set @sql = N';
	SELECT  Count(0)
	FROM	[Operation].[OrderHeader] OrdHd
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		And Shpr.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	WHERE	OrdHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND OrdHd.[IsCancel] = CAST(0 as bit)' 
			 
			 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  OrdHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And OrdHd.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And OrdHd.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And Vsl.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And OrdHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@ShipperName)) > 0) set @sql = @sql + ' And Shpr.MerchantName LIKE ''%' + @ShipperName  + '%'''
	if (len(rtrim(@ConsigneeName)) > 0) set @sql = @sql + ' And Cons.MerchantName LIKE ''%' + @ConsigneeName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',OrdHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',OrdHd.OrderDate)'

 Exec(@Sql) 

End

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderEntryRecordCount]
-- ========================================================================================================================================


GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderEntryDataTableList]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	12-Apr-2017
-- Description:	Get Order header record list

-- ========================================================================================================================================



IF OBJECT_ID('[Operation].[usp_OrderEntryDataTableList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderEntryDataTableList]
END 
GO
CREATE PROC [Operation].[usp_OrderEntryDataTableList]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@HouseBLNo nvarchar(50),
	@ManifestNo nvarchar(100),
	@ShipperName nvarchar(100),
	@ConsigneeName nvarchar(100),
	@DateFrom datetime=NULL,
	@DateTo datetime = NULL,

	@limit smallint,
	@offset smallint,
	@sortColumn varchar(50),
	@sortType varchar(50)

AS 
 

BEGIN

	declare @sql nvarchar(max);
	declare @sqlCompany nvarchar(max)


Declare @CompanyCode bigint

Select @CompanyCode=CompanyCode From Master.Branch Where BranchID = @BranchID
 


set @sqlCompany = N';SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant Cns ON 
	OrdHd.ConsigneeCode = Cns.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID
where Cns.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant Shp ON 
OrdHd.ShipperCode = Shp.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID

where Shp.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant Fwd ON 
OrdHd.FwdAgent = Fwd.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID

where Fwd.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant c ON 
OrdHd.CustomerCode = c.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID

where c.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant sa ON 
OrdHd.ShippingAgent = sa.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID

where sa.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant np ON 
OrdHd.NotifyParty = np.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID
where np.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant oa ON 
OrdHd.OverseasAgentCode = oa.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID

where oa.SubscriberCompanyCode = ' + @CompanyCode + 
' UNION  ' + 
' SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
	OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
	ISNULL(Cons.MerchantName,'''') As ConsigneeName,
	ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
left Outer Join Master.Merchant FF ON 
	OrdHd.FregihtForwarderCode = FF.MerchantCode
Left Outer Join Master.Merchant Shpr ON 
	OrdHd.ShipperCode = Shpr.MerchantCode 
	And Shpr.IsShipper = CAST(1 as bit)
Left Outer Join Master.Merchant Cons ON 
	OrdHd.ConsigneeCode = Cons.MerchantCode 
	And Shpr.IsConsignee = CAST(1 as bit)
Left Outer Join Master.Vessel Vsl ON 
	OrdHd.VesselID = Vsl.VesselID
where FF.SubscriberCompanyCode = ' + @CompanyCode 

	set @sql = @sqlCompany + ' UNION ' +  N';
	SELECT OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
			OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
			ISNULL(Cons.MerchantName,'''') As ConsigneeName,
			ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		And Shpr.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	WHERE	OrdHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND OrdHd.[IsCancel] = CAST(0 as bit)' 
			 
			 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  OrdHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And OrdHd.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And OrdHd.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And Vsl.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And OrdHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@ShipperName)) > 0) set @sql = @sql + ' And Shpr.MerchantName LIKE ''%' + @ShipperName  + '%'''
	if (len(rtrim(@ConsigneeName)) > 0) set @sql = @sql + ' And Cons.MerchantName LIKE ''%' + @ConsigneeName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',OrdHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',OrdHd.OrderDate)'

	Set @Sql += ' Order By ' + @sortColumn + ' ' + @sortType + ' OFFSET ' + Convert(varchar(5), @offset) + ' ROWS Fetch Next ' + Convert(varchar(5), @limit) + ' Rows Only';

 Exec(@Sql) 

END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderEntryDataTableList]
-- ========================================================================================================================================

GO
