Create PROC [Operation].[usp_GetK1EdiResponse]
@BRANCHID BIGINT,
@DeclarationNo varchar(20) 
  AS
     BEGIN
		Select Top 1 [MessageType],[EDIDateTime],[FileName],[ErrorMessage] from [EDI].[K1Declaration]
			Where BranchID=@BRANCHID and DeclarationNo=@DeclarationNo and EDIDateTime is not null order by EDIDateTime Desc

	 END


