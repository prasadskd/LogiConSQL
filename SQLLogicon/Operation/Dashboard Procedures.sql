
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCountByDateForDashboard] 
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	27-Feb-2017
-- Description:	Used for Main dashboard, get the count of orders on current day.
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCountByDateForDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCountByDateForDashboard]  
END 
GO
CREATE PROC [Operation].[usp_OrderCountByDateForDashboard] 
	@BranchID bigint	 
As
BEGIN
	 
	Select Count(0)  as TotalOrders
	From Operation.OrderHeader OrdHd 
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),GetUtcDate(),120)
	 
END

go


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCountByDateForDashboard] 
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	27-Feb-2017
-- Description:	Used for Main dashboard, get the count of orders on current day.
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_PendingOrdersForPortBookingDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_PendingOrdersForPortBookingDashboard]  
END 
GO
CREATE PROC [Operation].[usp_PendingOrdersForPortBookingDashboard] 
	@BranchID bigint	 
As
BEGIN
	 
	Select Count(OrdHd.OrderNo)  as TotalOrders
	From Operation.OrderHeader OrdHd 
	Left Outer Join Port.BookingHeader BkHd ON 
		Ordhd.BranchID = BkHd.RelationBranchID
		And OrdHd.HouseBLNo = BkHd.BLNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),GETUTCDATE(),120)
	And BkHd.OrderNo IS NULL 
	 
END

go



/* count of bookings in order entry but not in port */
/* pending billing */
 

 
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCountByDateForDashboard] 
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	27-Feb-2017
-- Description:	Used for Main dashboard, get the count of orders on current day.
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard]  
END 
GO
CREATE PROC [Operation].[usp_OrderToDeclarationTurnaoundtimeDashboard] 
	@BranchID bigint	 
As
BEGIN
	 

	Declare @totalOrders smallint=0,
			@totalTime smallint=0,
			@totalTAT smallint=0;


	Select @totalOrders = ISNULL(Count(OrdHd.OrderNo),0)
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeader K1 ON
		OrdHd.BranchID = K1.BranchID
		And OrdHd.OrderNo = K1.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalOrders = @totalOrders +  ISNULL(Count(OrdHd.OrderNo),0)   
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeaderK2 K2 ON
		OrdHd.BranchID = K2.BranchID
		And OrdHd.OrderNo = K2.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	 
	Select @totalTime = ISNULL(SUM(DateDiff("M",Ordhd.OrderDate,K1.DeclarationDate)),0)
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeader K1 ON
		OrdHd.BranchID = K1.BranchID
		And OrdHd.OrderNo = K1.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalTime = @totalTime + ISNULL(SUM(DateDiff("M",Ordhd.OrderDate,K2.DeclarationDate)),0)   
	From Operation.OrderHeader OrdHd 
	Inner Join Operation.DeclarationHeaderK2 K2 ON
		OrdHd.BranchID = K2.BranchID
		And OrdHd.OrderNo = K2.OrderNo
	Where OrdHd.BranchID = @BranchID
	And Convert(varchar(10),OrdHd.OrderDate,120) = Convert(varchar(10),getutcdate(),120)

	Select @totalTAT = (@totalTime / @totalOrders) 



	Select @totalTAT;


END

go



IF OBJECT_ID('[Operation].[usp_GetSubscribersListCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_GetSubscribersListCount]  
END 
GO
CREATE Proc [Operation].[usp_GetSubscribersListCount] 
@CompanyCode BigInt
As
Begin

Declare @UserCount int=(Select COUNT(*) from [Security].[User] where CompanyCode=@CompanyCode)
Declare @K2SubscribersCount int=(Select COUNT(*) from [Operation].[DeclarationHeaderK2] where BranchID in 
                                            (Select BranchID from Master.Branch where CompanyCode=@CompanyCode))

Declare @K1SubscribersCount int=(Select COUNT(*) from [Operation].[DeclarationHeader] where BranchID in 
                             (Select BranchID from Master.Branch where CompanyCode=@CompanyCode))
Declare @OrdersCount int=(Select COUNT(*) from [Operation].[OrderHeader] where BranchID in
                                           (Select BranchID from Master.Branch where CompanyCode=@CompanyCode))

Select @UserCount as UserCount,@K2SubscribersCount+@K1SubscribersCount as DeclarationCount,@OrdersCount as OrderCount
End