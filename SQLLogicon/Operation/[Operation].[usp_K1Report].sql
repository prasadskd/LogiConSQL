-- ========================================================================================================================================
-- START											 [Reports].[usp_K1ReportA4]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	01-Mar-2017
-- Description:	Inserts the [DeclarationHeader] Record Into [DeclarationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Reports].[usp_K1ReportA4]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_K1ReportA4] 
END 
GO
CREATE PROC [Reports].[usp_K1ReportA4] --1007001,'D11704000007'
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	Declare @ForwardingAgentCode bigint
	set @ForwardingAgentCode = (Select M.MerchantCode From Master.Branch B
								INNER JOIN Master.Merchant M ON 
								B.CompanyCode = M.CompanyCode 
								And B.BranchName = M.MerchantName
								Where 
								B.branchid =@BranchID)
	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			IncoTermDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'IncoTerms')


	SELECT				--General Information
						Hd.[BranchID], Hd.[DeclarationNo], Hd.CustomerReferenceNo,Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ImportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType],	Hd.[CargoDescription],
						ISNULL(Hd.CustomStationCode,'') CustomStationCode, CSC.StationDescription,CSMB.Line1,CSMB.Line2,CSMB.Line3,Hd.[MarksAndNos] MarksNos,
						Br.BranchName,Br.RegNo,Br.GSTNo,
	
						--Consignor Information
						Hd.[Exporter],
						ET.MerchantName As ExporterName,												
						ISNULL(ET.RegNo,'') As ExporterROCNo,ET.TaxID As ExporterGSTNo,ETAdd.Address1 As ExporterAddress1,ETAdd.Address2 As ExporterAddress2, 
						ETAdd.Address3 as ExporterAddress3,ETAdd.City As ExporterCity, ETAdd.State As ExporterState,
						ETAdd.CountryCode As ExporterCountryCode,ETAdd.ZipCode As ExporterZipCode,ECtry.CountryName ExporterCountryName,

						--Consignee Information
						Hd.[Importer], 
						ISNULL(LT.RegNo,'') As ImporterROCNo,LT.TaxID As ImporterGSTNo,LT.MerchantName As ImporterName,
						LTAdd.Address1 As ImporterAddress1,LTAdd.Address2 As ImporterAddress2, 
						LTAdd.Address3 as ImporterAddress3,LTAdd.City As ImporterCity, LTAdd.State As ImporterState,
						LTAdd.CountryCode As ImporterCountryCode,LTAdd.ZipCode As ImporterZipCode,ICtry.CountryName ImporterCountryName,

						--Agent Information
						Hd.ShippingAgent,
						SA.MerchantName As ShippingAgentName,
						ISNULL(SA.RegNo,'') As ShippingAgentROCNo,SA.TaxID As ShippingAgentGSTNo,SAAdd.Address1 As ShippingAgentAddress1,SAAdd.Address2 As ShippingAgentAddress2, 
						SAAdd.Address3 as ShippingAgentAddress3,SAAdd.City As ShippingAgentCity, SAAdd.State As ShippingAgentState,
						SAAdd.CountryCode As ShippingAgentCountryCode,SAAdd.ZipCode As ShippingAgentZipCode,ACtry.CountryName ShippingAgentCountryName,

						--Container and Marks & Nos Information
						DCnt.[ContainerNo], DCnt.[Size], DCnt.[Type], DCnt.[ContainerStatus], DCnt.[EQDStatus], '' SealNo,
						

						-- Port & Vessel Informaton
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,Sh.PlaceOfImport,
						Sh.HouseBLNo, Sh.OceanBLNo,Sh.ManifestNo,
						ISNULL(LP.PortName,'') As LoadingPortName,
						ISNULL(POI.PortName,'') As PlaceOfImportName,
						ISNULL(DP.PortName,'') As DischargePortName,
						ISNULL(TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						ISNULL(ITD.LookupDescription,'') As IncoTermDescription,

						--Declarant Information	
						Hd.DeclarantID,
						Hd.DeclarantName,ISNULL(Hd.DeclarantNRIC,'') DeclarantNRIC,Hd.DeclarantDesignation,
						--HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, 
						--Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,

						--Document Information
						DDoc.SupportingDocumentType,DDoc.ReferenceNo,DDoc.DocDateType,DDoc.DocDate,

						--Item Information
						DItm.ItemNo,DItm.ItemDescription1,
						DItm.ProductCode,DItm.ProductDescription,DItm.HSCode,HS.Description As HSCodeDescription,DItm.OriginCountryCode,
						DItm.StatisticalQty, DItm.StatisticalUOM,DItm.DeclaredQty, DItm.DeclaredUOM,DItm.ItemAmount,
						DItm.ImportDutyRate,DItm.ImportDutyRateAmount,DItm.ImportDutySpecificRate,DItm.ImportDutySpecificRateAmount,
						DItm.ImportGST,DItm.ImportGSTAmount,DItm.ImportExciseRate,DItm.ImportExciseRateAmount,
						DItm.UnitLocalAmount,DItm.TotalUnitLocalAmount,DItm.TotalDutyPayable,DItm.TotalGSTPayable,
						DItm.ItemWeight,DItm.UnitCurrency,DItm.UnitCurrencyExchangeRate,
						DItm.FOBValue,DItm.CIFValue,DItm.EXWValue,DItm.CNFValue,DItm.CNIValue,DItm.FreightValue,DItm.InsuranceValue,DItm.CIFCValue,
						DItm.VehicleBrand,DItm.VehicleModel,DItm.VehicleEngineNo,DItm.VehicleChassisNo,DItm.VehicleCC,DItm.VehicleYear,
						
						--Invoice INformation
						Inv.[InvoiceNo], Inv.[InvoiceDate], Inv.[InvoiceValue], Inv.[InvoiceCurrencyCode], Inv.[LocalCurrencyCode], Inv.[CurrencyExRate],
						Inv.[IncoTerm], Inv.[PayCountry], Inv.[PortAmountPercent], Inv.[PortAmountCurrencyCode], Inv.[PortAmountExRate], Inv.[PortAmountValue], Inv.[FreightAmountPercent], 
						Inv.[IsFreightCurrency], Inv.[FreightAmountCurrencyCode], Inv.[FreightAmountExRate], Inv.[FreightAmountValue], Inv.[FreightIncoTerm], 
						Inv.[InsuranceAmountPercent], Inv.[IsInsuranceCurrency], Inv.[InsuranceAmountCurrencyCode], Inv.[InsuranceAmountExRate], 
						Inv.[InsuranceAmountValue], Inv.[InsuranceIncoTerm], Inv.[OthersAmountPercent], Inv.[OthersAmountCurrencyCode], Inv.[OthersAmountExRate], 
						Inv.[OthersAmountValue], Inv.[PackageQty], Inv.[PackingTypeCode], PMD.LookupDescription PackingTypeDesc,
						Inv.[PackingMaterialCode], Inv.[GrossWeight], Inv.[UOMWeight], Inv.[GrossVolume], Inv.[UOMVolume], Inv.[Status], 
						ISNULL(Inv.PayCountry,'') PayCountry,Cntry.CountryName,

						--Responses Information
						CRH.RegistrationDate, CRD.Code As CustomStatus, CRD.[Description] As CustomDescription

	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.DeclarationInvoice Inv ON 
		Hd.BranchId = Inv.BranchID
		And Hd.DeclarationNo = Inv.DeclarationNo
	Left Outer Join IncoTermDescription ITD ON 
		Inv.IncoTerm = ITD.LookupID
	Left Outer Join Operation.DeclarationContainer DCnt ON 
		Hd.BranchId = DCnt.BranchID
		And Hd.DeclarationNo = DCnt.DeclarationNo		
	Left Outer Join Operation.declarationDocuments DDoc ON 
		Hd.BranchId = DDoc.BranchID
		And Hd.DeclarationNo = DDoc.DeclarationNo
		AND DDoc.SupportingDocumentType = 25813
	Left Outer Join Operation.DeclarationItem DItm ON 
		DCnt.BranchId = DItm.BranchID
		And DCnt.DeclarationNo = DItm.DeclarationNo	
		And DCnt.ContainerKey = DItm.containerKey
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = Convert(varchar(10),LT.MerchantCode)
	Left OUter JOin Master.Merchant ET ON 
		Hd.Exporter = Convert(varchar(10),ET.MerchantCode)
	Left Outer Join Master.Address LTAdd ON 
		Convert(varchar(10),LT.MerchantCode) = LTAdd.LinkID
	Left OUter JOin Master.Merchant SA ON 
		@ForwardingAgentCode = SA.MerchantCode
	Left Outer Join Master.Address SAAdd ON 
		Convert(varchar(10),SA.MerchantCode) = SAAdd.LinkID
		AND SAAdd.AddressType='Merchant'
	Left Outer Join Master.Address ETAdd ON 
		Hd.Exporter = ETAdd.LinkID
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfImport = POI.PortCode
	Left Outer Join Master.Port sLP ON 
		Sh.LoadingPort = sLP.PortCode
	Left Outer Join Master.Port sDP ON 
			Sh.DischargePort = sDP.PortCode
	Left Outer Join Master.Port sTP ON 
			Sh.TranshipmentPort = sTP.PortCode
	Left Outer Join Master.Merchant PO ON 
			Sh.PortOperator = PO.MerchantCode
	LEFT OUTER JOIN [Master].Branch Br ON 
		Hd.BranchID = Br.BranchID
	LEFT OUTER JOIN [Master].[CustomStationCode] CSC ON 
		Hd.CustomStationCode = CSC.Code
	LEFT OUTER JOIN [Master].[Country] Cntry ON 
		Inv.PayCountry = Cntry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ECtry ON 
		ETAdd.CountryCode = ECtry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ICtry ON 
		LTAdd.CountryCode = ICtry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ACtry ON 
		SAAdd.CountryCode = ACtry.CountryCode
	LEFT OUTER JOIN [Master].[HSCode] HS ON 
		DItm.HSCode = HS.TariffCode
	LEFT OUTER JOIN [Master].[CustomStationMailBox] CSMB ON 
		Hd.CustomStationCode=CSMB.CustomStationCode
		AND Hd.DeclarationType=CSMB.DeclarationType
	LEFT OUTER JOIN config.lookup PMD ON
	Inv.[PackingTypeCode]=PMD.LookupID
	LEFT OUTER JOIN [EDI].[CustomResponseHeader] CRH ON
		Hd.BranchID = CRH.BRANCHID 
		AND Hd.DeclarationNo = CRH.DeclarationNo
	LEFT OUTER JOIN [EDI].[CustomResponseDetail] CRD ON
		CRH.BranchID = CRD.BRANCHID 
		AND CRH.DeclarationNo = CRD.DeclarationNo
		AND CRH.ResponseReferenceNo = CRD.ResponseReferenceNo
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo 



END




-- ========================================================================================================================================
-- END  										[Reports].[usp_K1ReportA4] 
-- ========================================================================================================================================

GO

GO

-- ========================================================================================================================================
-- START											 [Reports].[usp_K2ReportA4]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	01-Mar-2017
-- Description:	Get the [DeclarationHeader] Record Info

-- ========================================================================================================================================

IF OBJECT_ID('[Reports].[usp_K2ReportA4]') IS NOT NULL
BEGIN 
    DROP PROC [Reports].[usp_K2ReportA4] 
END 
GO
CREATE PROC [Reports].[usp_K2ReportA4] --1007001,'D11704000007'
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	Declare @ForwardingAgentCode bigint
	set @ForwardingAgentCode = (Select M.MerchantCode From Master.Branch B
								INNER JOIN Master.Merchant M ON 
								B.CompanyCode = M.CompanyCode 
								And B.BranchName = M.MerchantName
								Where 
								B.branchid =@BranchID)
	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1'),
			IncoTermDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'IncoTerms')


	SELECT				--General Information
						Hd.[BranchID], Hd.[DeclarationNo], Hd.CustomerReferenceNo,Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						Hd.[ExportDate], Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType],	Hd.[CargoDescription],
						ISNULL(Hd.CustomStationCode,'') CustomStationCode, CSC.StationDescription,CSMB.Line1,CSMB.Line2,CSMB.Line3,Hd.[MarksAndNos] MarksNos,
						Br.BranchName,Br.RegNo,Br.GSTNo,
	
						--Consignor Information
						Hd.[Exporter],
						ET.MerchantName As ExporterName,												
						ISNULL(ET.RegNo,'') As ExporterROCNo,ET.TaxID As ExporterGSTNo,ETAdd.Address1 As ExporterAddress1,ETAdd.Address2 As ExporterAddress2, 
						ETAdd.Address3 as ExporterAddress3,ETAdd.City As ExporterCity, ETAdd.State As ExporterState,
						ETAdd.CountryCode As ExporterCountryCode,ETAdd.ZipCode As ExporterZipCode,ECtry.CountryName ExporterCountryName,

						--Consignee Information
						Hd.[Importer], 
						ISNULL(LT.RegNo,'') As ImporterROCNo,LT.TaxID As ImporterGSTNo,LT.MerchantName As ImporterName,
						LTAdd.Address1 As ImporterAddress1,LTAdd.Address2 As ImporterAddress2, 
						LTAdd.Address3 as ImporterAddress3,LTAdd.City As ImporterCity, LTAdd.State As ImporterState,
						LTAdd.CountryCode As ImporterCountryCode,LTAdd.ZipCode As ImporterZipCode,ICtry.CountryName ImporterCountryName,

						--Agent Information
						Hd.ShippingAgent,
						FA.MerchantName As ForwardingAgentName,
						ISNULL(FA.RegNo,'') As ForwardingAgentROCNo,FA.TaxID As ForwardingAgentGSTNo,FAAdd.Address1 As ForwardingAgentAddress1,FAAdd.Address2 As ForwardingAgentAddress2, 
						FAAdd.Address3 as ForwardingAgentAddress3,FAAdd.City As ForwardingAgentCity, FAAdd.State As ForwardingAgentState,
						FAAdd.CountryCode As ForwardingAgentCountryCode,FAAdd.ZipCode As ForwardingAgentZipCode,ACtry.CountryName ForwardingAgentCountryName,

						--Container and Marks & Nos Information
						DCnt.[ContainerNo], DCnt.[Size], DCnt.[Type], DCnt.[ContainerStatus], DCnt.[EQDStatus], '' SealNo,
						

						-- Port & Vessel Informaton
						Sh.VesselID,sh.VesselName,sh.VoyageNo,Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,Sh.PlaceOfExport,
						Sh.HouseBLNo, Sh.OceanBLNo,Sh.ManifestNo,
						ISNULL(LP.PortName,'') As LoadingPortName,
						ISNULL(POI.PortName,'') As PlaceOfExportName,
						ISNULL(DP.PortName,'') As DischargePortName,
						ISNULL(TP.PortName,'') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'') As TransactionTypeDescription,
						ISNULL(ITD.LookupDescription,'') As IncoTermDescription,

						--Declarant Information	
						Hd.DeclarantID,
						Hd.DeclarantName,ISNULL(Hd.DeclarantNRIC,'') DeclarantNRIC,Hd.DeclarantDesignation,
						--HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, 
						--Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,

						--Document Information
						DDoc.SupportingDocumentType,DDoc.ReferenceNo,DDoc.DocDateType,DDoc.DocDate,

						--Item Information
						DItm.ItemNo,DItm.ItemDescription1,
						DItm.ProductCode,DItm.ProductDescription,DItm.HSCode,HS.Description As HSCodeDescription,DItm.OriginCountryCode,
						DItm.StatisticalQty, DItm.StatisticalUOM,DItm.DeclaredQty, DItm.DeclaredUOM,DItm.ItemAmount,
						DItm.ExportDutyRate,DItm.ExportDutyRateAmount,DItm.ExportDutySpecificRate,DItm.ExportDutySpecificRateAmount,
						--DItm.ImportGST,DItm.ImportGSTAmount,DItm.ImportExciseRate,DItm.ImportExciseRateAmount,
						DItm.UnitLocalAmount,DItm.TotalUnitLocalAmount,DItm.TotalDutyPayable,--DItm.TotalGSTPayable,
						DItm.ItemWeight,DItm.UnitCurrency,DItm.UnitCurrencyExchangeRate,
						DItm.FOBValue,DItm.CIFValue,DItm.EXWValue,DItm.CNFValue,DItm.CNIValue,DItm.FreightValue,DItm.InsuranceValue,DItm.CIFCValue,
						DItm.VehicleBrand,DItm.VehicleModel,DItm.VehicleEngineNo,DItm.VehicleChassisNo,DItm.VehicleCC,DItm.VehicleYear,
						
						--Invoice INformation
						Inv.[InvoiceNo], Inv.[InvoiceDate], Inv.[InvoiceValue], Inv.[InvoiceCurrencyCode], Inv.[LocalCurrencyCode], Inv.[CurrencyExRate],
						Inv.[IncoTerm], Inv.[PayCountry], Inv.[PortAmountPercent], Inv.[PortAmountCurrencyCode], Inv.[PortAmountExRate], Inv.[PortAmountValue], Inv.[FreightAmountPercent], 
						Inv.[IsFreightCurrency], Inv.[FreightAmountCurrencyCode], Inv.[FreightAmountExRate], Inv.[FreightAmountValue], Inv.[FreightIncoTerm], 
						Inv.[InsuranceAmountPercent], Inv.[IsInsuranceCurrency], Inv.[InsuranceAmountCurrencyCode], Inv.[InsuranceAmountExRate], 
						Inv.[InsuranceAmountValue], Inv.[InsuranceIncoTerm], Inv.[OthersAmountPercent], Inv.[OthersAmountCurrencyCode], Inv.[OthersAmountExRate], 
						Inv.[OthersAmountValue], Inv.[PackageQty], Inv.[PackingTypeCode], PMD.LookupDescription PackingTypeDesc,
						Inv.[PackingMaterialCode], Inv.[GrossWeight], Inv.[UOMWeight], Inv.[GrossVolume], Inv.[UOMVolume], Inv.[Status], 
						ISNULL(Inv.PayCountry,'') PayCountry,Cntry.CountryName,ISNULL(Inv.GoodsReceiveCountry,'') GoodsReceiveCountry,GRCntry.CountryName GoodsReceiveCountryName,ISNULL(Inv.DestinationCountry,'') DestinationCountry,DCntry.CountryName DestinationCountryName,



						ISNULL(Inv.AmountReceivedCurrencyCode,'') AmountReceivedCurrencyCode,Inv.AmountReceivedLocalValue,Inv.AmountReceivedExchangeRate,

						
						--Responses Information
						CRH.RegistrationDate, CRD.Code As CustomStatus, CRD.[Description] As CustomDescription

	FROM	[Operation].[DeclarationHeaderK2] Hd
	Left Outer Join Operation.DeclarationShipmentK2 Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.DeclarationInvoiceK2 Inv ON 
		Hd.BranchId = Inv.BranchID
		And Hd.DeclarationNo = Inv.DeclarationNo
	Left Outer Join IncoTermDescription ITD ON 
		Inv.IncoTerm = ITD.LookupID
	Left Outer Join Operation.DeclarationContainerK2 DCnt ON 
		Hd.BranchId = DCnt.BranchID
		And Hd.DeclarationNo = DCnt.DeclarationNo		
	Left Outer Join Operation.declarationDocumentsK2 DDoc ON 
		Hd.BranchId = DDoc.BranchID
		And Hd.DeclarationNo = DDoc.DeclarationNo
		AND DDoc.SupportingDocumentType = 25819
	Left Outer Join Operation.DeclarationItemK2 DItm ON 
		DCnt.BranchId = DItm.BranchID
		And DCnt.DeclarationNo = DItm.DeclarationNo	
		And DCnt.ContainerKey = DItm.containerKey
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = Convert(varchar(10),LT.MerchantCode)
	Left OUter JOin Master.Merchant ET ON 
		Hd.Exporter = Convert(varchar(10),ET.MerchantCode)
	Left Outer Join Master.Address LTAdd ON 
		Convert(varchar(10),LT.MerchantCode) = LTAdd.LinkID
	Left OUter JOin Master.Merchant FA ON 
		@ForwardingAgentCode = FA.MerchantCode
	Left Outer Join Master.Address FAAdd ON 
		Convert(varchar(10),FA.MerchantCode) = FAAdd.LinkID
		AND FAAdd.AddressType='Merchant'
	Left Outer Join Master.Address ETAdd ON 
		Hd.Exporter = ETAdd.LinkID
	Left Outer Join Master.Port POI ON 
		Sh.PlaceOfExport = POI.PortCode
	Left Outer Join Master.Port sLP ON 
		Sh.LoadingPort = sLP.PortCode
	Left Outer Join Master.Port sDP ON 
			Sh.DischargePort = sDP.PortCode
	Left Outer Join Master.Port sTP ON 
			Sh.TranshipmentPort = sTP.PortCode
	Left Outer Join Master.Merchant PO ON 
			Sh.PortOperator = PO.MerchantCode
	LEFT OUTER JOIN [Master].Branch Br ON 
		Hd.BranchID = Br.BranchID
	LEFT OUTER JOIN [Master].[CustomStationCode] CSC ON 
		Hd.CustomStationCode = CSC.Code
	LEFT OUTER JOIN [Master].[Country] Cntry ON 
		Inv.PayCountry = Cntry.CountryCode
	LEFT OUTER JOIN [Master].[Country] GRCntry ON 
		Inv.GoodsReceiveCountry = GRCntry.CountryCode
	LEFT OUTER JOIN [Master].[Country] DCntry ON 
		Inv.DestinationCountry = DCntry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ECtry ON 
		ETAdd.CountryCode = ECtry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ICtry ON 
		LTAdd.CountryCode = ICtry.CountryCode
	LEFT OUTER JOIN [Master].[Country] ACtry ON 
		FAAdd.CountryCode = ACtry.CountryCode
	LEFT OUTER JOIN [Master].[HSCode] HS ON 
		DItm.HSCode = HS.TariffCode
	LEFT OUTER JOIN [Master].[CustomStationMailBox] CSMB ON 
		Hd.CustomStationCode=CSMB.CustomStationCode
		AND Hd.DeclarationType=CSMB.DeclarationType
	LEFT OUTER JOIN config.lookup PMD ON
	Inv.[PackingTypeCode]=PMD.LookupID
	LEFT OUTER JOIN [EDI].[CustomResponseHeader] CRH ON
		Hd.BranchID = CRH.BRANCHID 
		AND Hd.DeclarationNo = CRH.DeclarationNo
	LEFT OUTER JOIN [EDI].[CustomResponseDetail] CRD ON
		CRH.BranchID = CRD.BRANCHID 
		AND CRH.DeclarationNo = CRD.DeclarationNo
		AND CRH.ResponseReferenceNo = CRD.ResponseReferenceNo
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo 



END


	

-- ========================================================================================================================================
-- END  										[Reports].[usp_K2ReportA4] 
-- ========================================================================================================================================

GO




