
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationExcemptionK2] Record based on [DeclarationExcemptionK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionK2Select] 
    @BranchID BIGINT,
    @Declarationno NVARCHAR(50)
AS 

BEGIN

	SELECT DE.[BranchID], DE.[Declarationno], DE.[GeneralExcemptionType], DE.[GeneralExcemptionRefNo], 
	Cc.[ClaimantID], Cc.[Name] As ClaimantName, Cc.[Company] As ClaimantCompany, Cc.[NRIC] As ClaimantNRIC, Cc.[Designation] As ClaimantDesignation, Cc.[Status] ClaimantStatus,  
	DE.[SalesTaxExcemptionTyp], DE.[CustomStationCode], DE.[SalesTaxReferenceNo], DE.[SalesTaxRegistratonDate], 
	DE.[SpecialTreatmentType], DE.[SpecialTreatmentReferenceNo], DE.[CreatedBy], DE.[CreatedOn], DE.[ModifiedBy], DE.[ModifiedOn] 
	FROM   [Operation].[DeclarationExcemptionK2] DE	
	Left Outer Join Master.CustomClaimant Cc ON 
		DE.BranchID = Cc.BranchID 
		And DE.ClaimantID = Cc.ClaimantID
		WHERE  DE.[BranchID] = @BranchID  
	       AND DE.[Declarationno] = @Declarationno   
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationExcemptionK2] Records from [DeclarationExcemptionK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionK2List] 
    @BranchID BIGINT,
    @Declarationno NVARCHAR(50)

AS 
BEGIN

	SELECT DE.[BranchID], DE.[Declarationno], DE.[GeneralExcemptionType], DE.[GeneralExcemptionRefNo], 
	Cc.[ClaimantID], Cc.[Name] As ClaimantName, Cc.[Company] As ClaimantCompany, Cc.[NRIC] As ClaimantNRIC, Cc.[Designation] As ClaimantDesignation, Cc.[Status] ClaimantStatus,  
	DE.[SalesTaxExcemptionTyp], DE.[CustomStationCode], DE.[SalesTaxReferenceNo], DE.[SalesTaxRegistratonDate], 
	DE.[SpecialTreatmentType], DE.[SpecialTreatmentReferenceNo], DE.[CreatedBy], DE.[CreatedOn], DE.[ModifiedBy], DE.[ModifiedOn] 
	FROM   [Operation].[DeclarationExcemptionK2] DE	
	Left Outer Join Master.CustomClaimant Cc ON 
		DE.BranchID = Cc.BranchID 
		And DE.ClaimantID = Cc.ClaimantID
		WHERE  DE.[BranchID] = @BranchID  
	       AND DE.[Declarationno] = @Declarationno  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionK2List] 
-- ========================================================================================================================================




GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationExcemptionK2] Record Into [DeclarationExcemptionK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionK2Insert] 
    @BranchID bigint,
    @Declarationno nvarchar(50),
    @GeneralExcemptionType smallint,
    @GeneralExcemptionRefNo nvarchar(50),
    @ClaimantID nvarchar(35),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @ClaimantStatus nvarchar(20),
    @SalesTaxExcemptionTyp smallint,
    @CustomStationCode nvarchar(10),
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistratonDate datetime,
    @SpecialTreatmentType smallint,
    @SpecialTreatmentReferenceNo nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationExcemptionK2] (
			[BranchID], [Declarationno], [GeneralExcemptionType], [GeneralExcemptionRefNo], [ClaimantID], [ClaimantName], 
			[ClaimantCompany], [ClaimantNRIC], [ClaimantDesignation], [ClaimantStatus], [SalesTaxExcemptionTyp], [CustomStationCode], [SalesTaxReferenceNo], 
			[SalesTaxRegistratonDate], [SpecialTreatmentType], [SpecialTreatmentReferenceNo], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @Declarationno, @GeneralExcemptionType, @GeneralExcemptionRefNo, @ClaimantID, @ClaimantName, 
			@ClaimantCompany, @ClaimantNRIC, @ClaimantDesignation, @ClaimantStatus, @SalesTaxExcemptionTyp, @CustomStationCode, @SalesTaxReferenceNo, 
			@SalesTaxRegistratonDate, @SpecialTreatmentType, @SpecialTreatmentReferenceNo, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationExcemptionK2] Record Into [DeclarationExcemptionK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionK2Update] 
    @BranchID bigint,
    @Declarationno nvarchar(50),
    @GeneralExcemptionType smallint,
    @GeneralExcemptionRefNo nvarchar(50),
    @ClaimantID nvarchar(35),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @ClaimantStatus nvarchar(20),
    @SalesTaxExcemptionTyp smallint,
    @CustomStationCode nvarchar(10),
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistratonDate datetime,
    @SpecialTreatmentType smallint,
    @SpecialTreatmentReferenceNo nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationExcemptionK2]
	SET    [GeneralExcemptionType] = @GeneralExcemptionType, [GeneralExcemptionRefNo] = @GeneralExcemptionRefNo, [ClaimantID] = @ClaimantID, 
	[ClaimantName] = @ClaimantName, [ClaimantCompany] = @ClaimantCompany, [ClaimantNRIC] = @ClaimantNRIC, [ClaimantDesignation] = @ClaimantDesignation, 
	[ClaimantStatus] = @ClaimantStatus, [SalesTaxExcemptionTyp] = @SalesTaxExcemptionTyp, [CustomStationCode] = @CustomStationCode, [SalesTaxReferenceNo] = @SalesTaxReferenceNo, 
	[SalesTaxRegistratonDate] = @SalesTaxRegistratonDate, [SpecialTreatmentType] = @SpecialTreatmentType, 
	[SpecialTreatmentReferenceNo] = @SpecialTreatmentReferenceNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [Declarationno] = @Declarationno
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionK2Update]
-- ========================================================================================================================================


GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationExcemptionK2] Record Into [DeclarationExcemptionK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2Save] 
END 
GO

CREATE PROC [Operation].[usp_DeclarationExcemptionK2Save] 
    @BranchID bigint,
    @Declarationno nvarchar(50),
    @GeneralExcemptionType smallint,
    @GeneralExcemptionRefNo nvarchar(50),
	@ClaimantID  nvarchar(35),
    @ClaimantStatus nvarchar(20),
    @ClaimantName nvarchar(50),
    @ClaimantCompany nvarchar(50),
    @ClaimantNRIC nvarchar(20),
    @ClaimantDesignation nvarchar(50),
    @SalesTaxExcemptionTyp smallint,
    @CustomStationCode nvarchar(10),
    @SalesTaxReferenceNo nvarchar(50),
    @SalesTaxRegistratonDate datetime,
    @SpecialTreatmentType smallint,
    @SpecialTreatmentReferenceNo nvarchar(50),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationExcemptionK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [Declarationno] = @Declarationno)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationExcemptionK2Update] 
		@BranchID, @Declarationno, @GeneralExcemptionType, @GeneralExcemptionRefNo, @ClaimantID, @ClaimantName, @ClaimantCompany, 
		@ClaimantNRIC, @ClaimantDesignation, @ClaimantStatus, @SalesTaxExcemptionTyp, @CustomStationCode, @SalesTaxReferenceNo, @SalesTaxRegistratonDate, 
		@SpecialTreatmentType, @SpecialTreatmentReferenceNo, @CreatedBy,  @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationExcemptionK2Insert] 
		@BranchID, @Declarationno, @GeneralExcemptionType, @GeneralExcemptionRefNo, @ClaimantID, @ClaimantName, @ClaimantCompany, 
		@ClaimantNRIC, @ClaimantDesignation, @ClaimantStatus, @SalesTaxExcemptionTyp, @CustomStationCode, @SalesTaxReferenceNo, @SalesTaxRegistratonDate, 
		@SpecialTreatmentType, @SpecialTreatmentReferenceNo, @CreatedBy,  @ModifiedBy 

	END
	
END
-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationExcemptionK2Save]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationExcemptionK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationExcemptionK2] Record  based on [DeclarationExcemptionK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationExcemptionK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationExcemptionK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationExcemptionK2Delete] 
    @BranchID bigint,
    @Declarationno nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationExcemptionK2]
	WHERE  [BranchID] = @BranchID
	       AND [Declarationno] = @Declarationno
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationExcemptionK2Delete]
-- ========================================================================================================================================

GO
 