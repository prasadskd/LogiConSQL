
-- ========================================================================================================================================
-- START											[Operation].[usp_DeclarationCountByDate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationCountByDate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationCountByDate]
END 
GO
CREATE PROC  [Operation].[usp_DeclarationCountByDate]
  @BranchID bigint,
  @DateFrom datetime,
  @DateTo datetime
As
 Begin
 Declare @K1Count varchar(100);
 Declare @K2Count Varchar(100);
  Set @K1Count=(Select COUNT(*)  from [Operation].[DeclarationItem] where BranchID=@BranchID
   And Convert(varchar(10),CreatedOn,120) >= @DateFrom
 And Convert(varchar(10),CreatedOn,120) <= @DateTo )
   Set @K2Count=(Select COUNT(*) from [Operation].[DeclarationItemK2] where BranchID=@BranchID And 
   Convert(varchar(10),CreatedOn,120) >= @DateFrom
 And Convert(varchar(10),CreatedOn,120) <= @DateTo )
  Select @K1Count as K1Count,@K2Count as K2Count
End