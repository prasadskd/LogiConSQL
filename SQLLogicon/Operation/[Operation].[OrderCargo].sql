
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderCargo] Record based on [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCargoSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoSelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoSelect] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
	@ContainerKey nvarchar(50),
    @CargoKey NVARCHAR(50)
AS 

BEGIN

	SELECT	OrdCg.[BranchID], OrdCg.[OrderNo], OrdCg.ContainerKey, OrdCg.[CargoKey], OrdCg.[ShipperConsigneeCode], OrdCg.[ShipperConsigneeName], OrdCg.[ProductCode], OrdCg.[ProductDescription], 
			OrdCg.[StockRoom], OrdCg.[ConsignmentNo], OrdCg.[BatchNo], OrdCg.[PONo], OrdCg.[HouseBLNo], OrdCg.[InvoiceNo], OrdCg.[StyleNo], OrdCg.[Colour], OrdCg.[TallyInNo], OrdCg.[IMOCode], 
			OrdCg.[UNNo], OrdCg.[Qty], OrdCg.[UOM], OrdCg.[BaseQty], OrdCg.[BaseUOM], OrdCg.[GrossVolume], OrdCg.[GrossWeight], OrdCg.[ExchangeRate], 
			OrdCg.[PageNo], OrdCg.[CargoLine], OrdCg.[MarksNumbers], OrdCg.[Remarks], OrdCg.[WHCode], OrdCg.[PalletCount], OrdCg.[LoadQty], OrdCg.[OrderQty], OrdCg.[Len], 
			OrdCg.[Width], OrdCg.[Height], OrdCg.[CommodityCode], OrdCg.[Packs], OrdCg.[CargoClass], Ordcg.HSCode,OrdCg.CountryCode,Ordcg.DeclarationNo,
			OrdCg.[ItemDescription],OrdCg.[ForeignCurrencyCode],OrdCg.[ForeignPrice],OrdCg.[LocalCurrencyCode],OrdCg.[ExchangeRate],
			OrdCg.[LocalPrice],OrdCg.[IsGST],OrdCg.[GSTCurrencyCode],OrdCg.[PackageCount],OrdCg.[PackageType],OrdCg.[SpecialCargoHandling],
			OrdCg.[CreatedBy], OrdCg.[CreatedOn], OrdCg.[ModifiedBy], OrdCg.[ModifiedOn],0 As Selected ,
			OrdCnt.ContainerNo,
			ISNULL(Cmd.Description ,'') As CommodityDescription,
			ISNULL(Imo.Description,'') As IMOCodeDescription,
			ISNULL(Uom.UOMDescription,'') As UOMDescription,
			ISNULL(BUom.UOMDescription,'') As BaseUOMDescription,
			ISNULL(Hs.Description,'') As HSCodeDescription
	FROM	[Operation].[OrderCargo] OrdCg
	Left Outer Join Operation.OrderContainer OrdCnt On 
		OrdCg.ContainerKey = OrdCnt.ContainerKey
	Left Outer Join Master.Commodity Cmd On 
		OrdCg.CommodityCode = Cmd.CommodityCode
	Left Outer Join Master.IMOCode Imo ON 
		Convert(nvarchar(20),OrdCg.IMOCode) = Imo.Code
	Left Outer Join Master.UOM Uom ON 
		OrdCg.UOM = Uom.UOMCode
	Left Outer Join Master.UOM BUom ON 
		OrdCg.BaseUOM = BUom.UOMCode
	Left Outer Join Master.HSCode Hs ON
		OrdCg.HSCode = Hs.TariffCode	
	WHERE  OrdCg.[BranchID] = @BranchID  
	       AND OrdCg.[OrderNo] = @OrderNo 
		   And OrdCg.[ContainerKey]= @ContainerKey
	       AND OrdCg.[CargoKey] = @CargoKey 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderCargo] Records from [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCargoList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoList]
END 
GO
CREATE PROC [Operation].[usp_OrderCargoList] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT	OrdCg.[BranchID], OrdCg.[OrderNo], OrdCg.ContainerKey, OrdCg.[CargoKey], OrdCg.[ShipperConsigneeCode], OrdCg.[ShipperConsigneeName], OrdCg.[ProductCode], OrdCg.[ProductDescription], 
			OrdCg.[StockRoom], OrdCg.[ConsignmentNo], OrdCg.[BatchNo], OrdCg.[PONo], OrdCg.[HouseBLNo], OrdCg.[InvoiceNo], OrdCg.[StyleNo], OrdCg.[Colour], OrdCg.[TallyInNo], OrdCg.[IMOCode], 
			OrdCg.[UNNo], OrdCg.[Qty], OrdCg.[UOM], OrdCg.[BaseQty], OrdCg.[BaseUOM], OrdCg.[GrossVolume], OrdCg.[GrossWeight], OrdCg.[ExchangeRate], 
			OrdCg.[PageNo], OrdCg.[CargoLine], OrdCg.[MarksNumbers], OrdCg.[Remarks], OrdCg.[WHCode], OrdCg.[PalletCount], OrdCg.[LoadQty], OrdCg.[OrderQty], OrdCg.[Len], 
			OrdCg.[Width], OrdCg.[Height], OrdCg.[CommodityCode], OrdCg.[Packs], OrdCg.[CargoClass], Ordcg.HSCode,OrdCg.CountryCode,Ordcg.DeclarationNo,
			OrdCg.[ItemDescription],OrdCg.[ForeignCurrencyCode],OrdCg.[ForeignPrice],OrdCg.[LocalCurrencyCode],OrdCg.[ExchangeRate],
			OrdCg.[LocalPrice],OrdCg.[IsGST],OrdCg.[GSTCurrencyCode],OrdCg.[PackageCount],OrdCg.[PackageType],OrdCg.[SpecialCargoHandling],
			OrdCg.[CreatedBy], OrdCg.[CreatedOn], OrdCg.[ModifiedBy], OrdCg.[ModifiedOn],0 As Selected ,
			OrdCnt.ContainerNo,
			ISNULL(Cmd.Description ,'') As CommodityDescription,
			ISNULL(Imo.Description,'') As IMOCodeDescription,
			ISNULL(Uom.UOMDescription,'') As UOMDescription,
			ISNULL(BUom.UOMDescription,'') As BaseUOMDescription,
			ISNULL(Hs.Description,'') As HSCodeDescription
	FROM	[Operation].[OrderCargo] OrdCg
	Left Outer Join Operation.OrderContainer OrdCnt On 
		OrdCg.ContainerKey = OrdCnt.ContainerKey
		AND OrdCnt.BranchID=@BranchID
	Left Outer Join Master.Commodity Cmd On 
		OrdCg.CommodityCode = Cmd.CommodityCode
	Left Outer Join Master.IMOCode Imo ON 
		Convert(nvarchar(20),OrdCg.IMOCode) = Imo.Code
	Left Outer Join Master.UOM Uom ON 
		OrdCg.UOM = Uom.UOMCode
	Left Outer Join Master.UOM BUom ON 
		OrdCg.BaseUOM = BUom.UOMCode
	Left Outer Join Master.HSCode Hs ON
		OrdCg.HSCode = Hs.TariffCode
	WHERE  OrdCg.[BranchID] = @BranchID  
	       AND OrdCg.[OrderNo] = @OrderNo
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderCargo] Records from [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCargoPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoPageView] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoPageView] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT	OrdCg.[BranchID], OrdCg.[OrderNo], OrdCg.ContainerKey, OrdCg.[CargoKey], OrdCg.[ShipperConsigneeCode], OrdCg.[ShipperConsigneeName], OrdCg.[ProductCode], OrdCg.[ProductDescription], 
			OrdCg.[StockRoom], OrdCg.[ConsignmentNo], OrdCg.[BatchNo], OrdCg.[PONo], OrdCg.[HouseBLNo], OrdCg.[InvoiceNo], OrdCg.[StyleNo], OrdCg.[Colour], OrdCg.[TallyInNo], OrdCg.[IMOCode], 
			OrdCg.[UNNo], OrdCg.[Qty], OrdCg.[UOM], OrdCg.[BaseQty], OrdCg.[BaseUOM], OrdCg.[GrossVolume], OrdCg.[GrossWeight], OrdCg.[ExchangeRate], 
			OrdCg.[PageNo], OrdCg.[CargoLine], OrdCg.[MarksNumbers], OrdCg.[Remarks], OrdCg.[WHCode], OrdCg.[PalletCount], OrdCg.[LoadQty], OrdCg.[OrderQty], OrdCg.[Len], 
			OrdCg.[Width], OrdCg.[Height], OrdCg.[CommodityCode], OrdCg.[Packs], OrdCg.[CargoClass], Ordcg.HSCode,OrdCg.CountryCode,Ordcg.DeclarationNo,
			OrdCg.[ItemDescription],OrdCg.[ForeignCurrencyCode],OrdCg.[ForeignPrice],OrdCg.[LocalCurrencyCode],OrdCg.[ExchangeRate],
			OrdCg.[LocalPrice],OrdCg.[IsGST],OrdCg.[GSTCurrencyCode],OrdCg.[PackageCount],OrdCg.[PackageType],OrdCg.[SpecialCargoHandling],
			OrdCg.[CreatedBy], OrdCg.[CreatedOn], OrdCg.[ModifiedBy], OrdCg.[ModifiedOn],0 As Selected ,
			OrdCnt.ContainerNo,
			ISNULL(Cmd.Description ,'') As CommodityDescription,
			ISNULL(Imo.Description,'') As IMOCodeDescription,
			ISNULL(Uom.UOMDescription,'') As UOMDescription,
			ISNULL(BUom.UOMDescription,'') As BaseUOMDescription,
			ISNULL(Hs.Description,'') As HSCodeDescription
	FROM	[Operation].[OrderCargo] OrdCg
	Left Outer Join Operation.OrderContainer OrdCnt On 
		OrdCg.ContainerKey = OrdCnt.ContainerKey
	Left Outer Join Master.Commodity Cmd On 
		OrdCg.CommodityCode = Cmd.CommodityCode
	Left Outer Join Master.IMOCode Imo ON 
		Convert(nvarchar(20),OrdCg.IMOCode) = Imo.Code
	Left Outer Join Master.UOM Uom ON 
		OrdCg.UOM = Uom.UOMCode
	Left Outer Join Master.UOM BUom ON 
		OrdCg.BaseUOM = BUom.UOMCode
	Left Outer Join Master.HSCode Hs ON
		OrdCg.HSCode = Hs.TariffCode
	ORDER BY OrdCg.[CargoKey]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCargoRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoRecordCount] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[OrderCargo]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderCargo] Record based on [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderCargoAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoAutoCompleteSearch] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
    @CargoKey NVARCHAR(50)
AS 

BEGIN

	SELECT	OrdCg.[BranchID], OrdCg.[OrderNo], OrdCg.ContainerKey, OrdCg.[CargoKey], OrdCg.[ShipperConsigneeCode], OrdCg.[ShipperConsigneeName], OrdCg.[ProductCode], OrdCg.[ProductDescription], 
			OrdCg.[StockRoom], OrdCg.[ConsignmentNo], OrdCg.[BatchNo], OrdCg.[PONo], OrdCg.[HouseBLNo], OrdCg.[InvoiceNo], OrdCg.[StyleNo], OrdCg.[Colour], OrdCg.[TallyInNo], OrdCg.[IMOCode], 
			OrdCg.[UNNo], OrdCg.[Qty], OrdCg.[UOM], OrdCg.[BaseQty], OrdCg.[BaseUOM], OrdCg.[GrossVolume], OrdCg.[GrossWeight], OrdCg.[ExchangeRate], 
			OrdCg.[PageNo], OrdCg.[CargoLine], OrdCg.[MarksNumbers], OrdCg.[Remarks], OrdCg.[WHCode], OrdCg.[PalletCount], OrdCg.[LoadQty], OrdCg.[OrderQty], OrdCg.[Len], 
			OrdCg.[Width], OrdCg.[Height], OrdCg.[CommodityCode], OrdCg.[Packs], OrdCg.[CargoClass], Ordcg.HSCode,OrdCg.CountryCode,Ordcg.DeclarationNo,
			OrdCg.[ItemDescription],OrdCg.[ForeignCurrencyCode],OrdCg.[ForeignPrice],OrdCg.[LocalCurrencyCode],OrdCg.[ExchangeRate],
			OrdCg.[LocalPrice],OrdCg.[IsGST],OrdCg.[GSTCurrencyCode],OrdCg.[PackageCount],OrdCg.[PackageType],OrdCg.[SpecialCargoHandling],
			OrdCg.[CreatedBy], OrdCg.[CreatedOn], OrdCg.[ModifiedBy], OrdCg.[ModifiedOn],0 As Selected ,
			OrdCnt.ContainerNo,
			ISNULL(Cmd.Description ,'') As CommodityDescription,
			ISNULL(Imo.Description,'') As IMOCodeDescription,
			ISNULL(Uom.UOMDescription,'') As UOMDescription,
			ISNULL(BUom.UOMDescription,'') As BaseUOMDescription,
			ISNULL(Hs.Description,'') As HSCodeDescription
	FROM	[Operation].[OrderCargo] OrdCg
	Left Outer Join Operation.OrderContainer OrdCnt On 
		OrdCg.ContainerKey = OrdCnt.ContainerKey
	Left Outer Join Master.Commodity Cmd On 
		OrdCg.CommodityCode = Cmd.CommodityCode
	Left Outer Join Master.IMOCode Imo ON 
		Convert(nvarchar(20),OrdCg.IMOCode) = Imo.Code
	Left Outer Join Master.UOM Uom ON 
		OrdCg.UOM = Uom.UOMCode
	Left Outer Join Master.UOM BUom ON 
		OrdCg.BaseUOM = BUom.UOMCode
	Left Outer Join Master.HSCode Hs ON
		OrdCg.HSCode = Hs.TariffCode
	WHERE  OrdCg.[BranchID] = @BranchID  
	       AND OrdCg.[OrderNo] LIKE '%' +  @OrderNo  + '%' 
		   AND [CargoKey] LIKE '%' +  @CargoKey  + '%'
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderCargo] Record Into [OrderCargo] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderCargoInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoInsert] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
	@ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ShipperConsigneeCode nvarchar(10),
    @ShipperConsigneeName nvarchar(50),
    @ProductDescription nvarchar(100),
    @StockRoom nvarchar(10),
    @Qty float,
    @UOM nvarchar(20),
    @GrossVolume float,
    @GrossWeight float,
    @CargoLine nvarchar(10),
    @MarksNumbers nvarchar(100),
    @Remarks nvarchar(100),
    @PalletCount smallint,
	@Len smallint,
	@Width smallint,
	@Height smallint,
	@HSCode nvarchar(20),
	@CountryCode varchar(2),
	@DeclarationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@ItemDescription nvarchar(50),
    @ForeignCurrencyCode nvarchar(3),
    @ForeignPrice decimal(18, 4),
    @LocalCurrencyCode nvarchar(3),
    @ExchangeRate decimal(18, 4),
    @LocalPrice decimal(18, 4),
    @IsGST bit,
    @GSTCurrencyCode nvarchar(3),
    @PackageCount int,
    @PackageType nvarchar(10),
    @SpecialCargoHandling nvarchar(100),
	@IMOCode nvarchar(20),
	@UNNo nvarchar(10)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[OrderCargo] (
			[BranchID], [OrderNo],[ContainerKey], [CargoKey], [ShipperConsigneeCode], [ShipperConsigneeName], [ProductDescription], [StockRoom], 
			[Qty], [UOM], 
			[GrossVolume], [GrossWeight], 
			[CargoLine], [MarksNumbers], 
			[Remarks], [PalletCount], 
			[Len], [Width], [Height], 
			[HSCode],[CountryCode],DeclarationNo,[CreatedBy], [CreatedOn],
			[ItemDescription], [ForeignCurrencyCode], 
			[ForeignPrice], [LocalCurrencyCode], [ExchangeRate], [LocalPrice], [IsGST], [GSTCurrencyCode], [PackageCount], [PackageType], 
			[SpecialCargoHandling],[IMOCode],[UNNo])
	SELECT 	@BranchID, @OrderNo,@ContainerKey, @CargoKey, @ShipperConsigneeCode, @ShipperConsigneeName, @ProductDescription, 
			@StockRoom, @Qty, @UOM, @GrossVolume, @GrossWeight, @CargoLine, 
			@MarksNumbers, @Remarks, @PalletCount,@Len,@Width,@Height, @HSCode,@CountryCode,@DeclarationNo, @CreatedBy, GETUTCDATE(),
			@ItemDescription, @ForeignCurrencyCode, @ForeignPrice, @LocalCurrencyCode, @ExchangeRate, @LocalPrice, @IsGST, @GSTCurrencyCode, @PackageCount, @PackageType, 
			@SpecialCargoHandling,@IMOCode,@UNNo
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderCargo] Record Into [OrderCargo] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderCargoUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoUpdate] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
	@ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ShipperConsigneeCode nvarchar(10),
    @ShipperConsigneeName nvarchar(50),
    @ProductDescription nvarchar(100),
    @StockRoom nvarchar(10),
    @Qty float,
    @UOM nvarchar(20),
    @GrossVolume float,
    @GrossWeight float,
    @CargoLine nvarchar(10),
    @MarksNumbers nvarchar(100),
    @Remarks nvarchar(100),
    @PalletCount smallint,
	@Len smallint,
	@Width smallint,
	@Height smallint,
	@HSCode nvarchar(20),
	@CountryCode varchar(2),
	@DeclarationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@ItemDescription nvarchar(50),
    @ForeignCurrencyCode nvarchar(3),
    @ForeignPrice decimal(18, 4),
    @LocalCurrencyCode nvarchar(3),
    @ExchangeRate decimal(18, 4),
    @LocalPrice decimal(18, 4),
    @IsGST bit,
    @GSTCurrencyCode nvarchar(3),
    @PackageCount int,
    @PackageType nvarchar(10),
    @SpecialCargoHandling nvarchar(100),
	@IMOCode nvarchar(20),
	@UNNo nvarchar(10)
AS 
 
	
BEGIN

	UPDATE	[Operation].[OrderCargo]
	SET		[ShipperConsigneeCode] = @ShipperConsigneeCode, [ShipperConsigneeName] = @ShipperConsigneeName,  
			[ProductDescription] = @ProductDescription, [StockRoom] = @StockRoom, [Qty] = @Qty, [UOM] = @UOM, 
			[GrossVolume] = @GrossVolume, [GrossWeight] = @GrossWeight,
			[CargoLine] = @CargoLine, 
			[MarksNumbers] = @MarksNumbers, [Remarks] = @Remarks,  [PalletCount] = @PalletCount,
			[Len]=@Len,[Width]=@Width,[Height]=@Height, 
			 HSCode=@HSCode,CountryCode=@CountryCode,DeclarationNo= @DeclarationNo,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),
			[ItemDescription] = @ItemDescription, [ForeignCurrencyCode] = @ForeignCurrencyCode, [ForeignPrice] = @ForeignPrice, 
			 [LocalCurrencyCode] = @LocalCurrencyCode, [ExchangeRate] = @ExchangeRate, [LocalPrice] = @LocalPrice, [IsGST] = @IsGST, 
			 [GSTCurrencyCode] = @GSTCurrencyCode, [PackageCount] = @PackageCount, [PackageType] = @PackageType, 
			 [SpecialCargoHandling] = @SpecialCargoHandling,IMOCode = @IMOCode,UNNo = @UNNo
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			And [ContainerKey] = @ContainerKey
			AND [CargoKey] = @CargoKey
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderCargo] Record Into [OrderCargo] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderCargoSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoSave] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
	@ContainerKey nvarchar(50),
    @CargoKey nvarchar(50),
    @ShipperConsigneeCode nvarchar(10),
    @ShipperConsigneeName nvarchar(50),
    @ProductDescription nvarchar(100),
    @StockRoom nvarchar(10),
    @Qty float,
    @UOM nvarchar(20),
    @GrossVolume float,
    @GrossWeight float,
    @CargoLine nvarchar(10),
    @MarksNumbers nvarchar(100),
    @Remarks nvarchar(100),
    @PalletCount smallint,
	@Len smallint,
	@Width smallint,
	@Height smallint,
	@HSCode nvarchar(20),
	@CountryCode varchar(2),
	@DeclarationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@ItemDescription nvarchar(50),
    @ForeignCurrencyCode nvarchar(3),
    @ForeignPrice decimal(18, 4),
    @LocalCurrencyCode nvarchar(3),
    @ExchangeRate decimal(18, 4),
    @LocalPrice decimal(18, 4),
    @IsGST bit,
    @GSTCurrencyCode nvarchar(3),
    @PackageCount int,
    @PackageType nvarchar(10),
    @SpecialCargoHandling nvarchar(100),
	@IMOCode nvarchar(20),
	@UNNo nvarchar(10)


AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[OrderCargo] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [CargoKey] = @CargoKey)>0
	BEGIN
	    Exec [Operation].[usp_OrderCargoUpdate] 
				@BranchID, @OrderNo,@ContainerKey, @CargoKey, @ShipperConsigneeCode, @ShipperConsigneeName, @ProductDescription, 
				@StockRoom, @Qty, @UOM, @GrossVolume, @GrossWeight, @CargoLine, 
				@MarksNumbers, @Remarks, @PalletCount,@Len,@Width,@Height, @HSCode,@CountryCode,@DeclarationNo, @CreatedBy,@ModifiedBy,
				@ItemDescription, @ForeignCurrencyCode, @ForeignPrice, @LocalCurrencyCode, @ExchangeRate, @LocalPrice, @IsGST, @GSTCurrencyCode, 
				@PackageCount, @PackageType, @SpecialCargoHandling,@IMOCode,@UNNo



	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderCargoInsert] 
				@BranchID, @OrderNo,@ContainerKey, @CargoKey, @ShipperConsigneeCode, @ShipperConsigneeName, @ProductDescription, 
				@StockRoom, @Qty, @UOM, @GrossVolume, @GrossWeight, @CargoLine, 
				@MarksNumbers, @Remarks, @PalletCount,@Len,@Width,@Height, @HSCode,@CountryCode,@DeclarationNo, @CreatedBy,@ModifiedBy,
				@ItemDescription, @ForeignCurrencyCode, @ForeignPrice, @LocalCurrencyCode, @ExchangeRate, @LocalPrice, @IsGST, @GSTCurrencyCode, 
				@PackageCount, @PackageType, @SpecialCargoHandling,@IMOCode,@UNNo
	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderCargoSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderCargo] Record  based on [OrderCargo]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderCargoDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoDelete] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
	@ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[OrderCargo]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
		   And [ContainerKey]=@ContainerKey
	       AND [CargoKey] = @CargoKey
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoDelete]
-- ========================================================================================================================================

GO

 
 
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderCargoDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderCargo] Record  based on [OrderCargo]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderCargoDeleteAll]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderCargoDeleteAll] 
END 
GO
CREATE PROC [Operation].[usp_OrderCargoDeleteAll] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50) 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[OrderCargo]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	        
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoDeleteAll]
-- ========================================================================================================================================

GO

 
