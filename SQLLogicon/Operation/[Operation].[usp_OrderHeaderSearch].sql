

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table

-- Exec [Operation].[usp_OrderHeaderSearch] 1001001,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-01','2017-01-12'
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderHeaderSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderSearch]
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderSearch]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@HouseBLNo nvarchar(50),
	@ManifestNo nvarchar(100),
	@ShipperName nvarchar(100),
	@CustomerName nvarchar(100),
	@DateFrom datetime=null,
	@DateTo datetime = NULL

AS 
 

BEGIN
	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
	declare @sql2 nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';
	SELECT Top(100)	OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[Voyageno], OrdHd.[HouseBLNo],
			OrdHd.[ManifestNo], ISNULL(Shpr.MerchantName,'''') As ShipperName, 
			ISNULL(Cons.MerchantName,'''') As ConsigneeName,
			ISNULL(Vsl.VesselName,'''') As VesselName
	FROM	[Operation].[OrderHeader] OrdHd
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		And Shpr.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	WHERE	OrdHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND OrdHd.[IsCancel] = CAST(0 as bit)' 
			 
			 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  OrdHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And OrdHd.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	if (len(rtrim(@HouseBLNo)) > 0) set @sql = @sql + ' And OrdHd.HouseBLNo LIKE ''%' + @HouseBLNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And Vsl.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And OrdHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@ShipperName)) > 0) set @sql = @sql + ' And Shpr.MerchantName LIKE ''%' + @ShipperName  + '%'''
	if (len(rtrim(@CustomerName)) > 0) set @sql = @sql + ' And Cons.MerchantName LIKE ''%' + @CustomerName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',OrdHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),OrdHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',OrdHd.OrderDate)'

	print @sql;

	exec sp_executesql @sql , N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;

END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderHeaderSearch]
-- ========================================================================================================================================

GO

 