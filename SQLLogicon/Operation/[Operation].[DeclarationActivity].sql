
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivitySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationActivity] Record based on [DeclarationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationActivitySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivitySelect] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivitySelect] 
    @ActivityID BIGINT,
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	SELECT [ActivityID], [BranchID], [DeclarationNo], [ActivityDate], [AcitivtyMessage], [ActivityBy], [EDIMessageType], [EDIResponse], [CreatedBy], [CreatedOn] 
	FROM   [Operation].[DeclarationActivity]
	WHERE  [ActivityID] = @ActivityID  
	       AND [BranchID] = @BranchID 
	       AND [DeclarationNo] = @DeclarationNo 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivitySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationActivity] Records from [DeclarationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationActivityList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityList] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityList] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)

AS 
BEGIN

	SELECT [ActivityID], [BranchID], [DeclarationNo], [ActivityDate], [AcitivtyMessage], [ActivityBy], [EDIMessageType], [EDIResponse], [CreatedBy], [CreatedOn] 
	FROM   [Operation].[DeclarationActivity]
	WHERE  [BranchID] = @BranchID 
	    AND [DeclarationNo] = @DeclarationNo 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationActivity] Records from [DeclarationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationActivityPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityPageView] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityPageView] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	SELECT [ActivityID], [BranchID], [DeclarationNo], [ActivityDate], [AcitivtyMessage], [ActivityBy], [EDIMessageType], [EDIResponse], [CreatedBy], [CreatedOn] 
	FROM   [Operation].[DeclarationActivity]
	WHERE  [BranchID] = @BranchID 
	    AND [DeclarationNo] = @DeclarationNo 
	ORDER BY [ActivityDate]
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [DeclarationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationActivityRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityRecordCount] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[DeclarationActivity]
	WHERE  [BranchID] = @BranchID 
	    AND [DeclarationNo] = @DeclarationNo 
	

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationActivity] Record based on [DeclarationActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationActivityAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityAutoCompleteSearch] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50)
AS 

BEGIN

	SELECT [ActivityID], [BranchID], [DeclarationNo], [ActivityDate], [AcitivtyMessage], [ActivityBy], [EDIMessageType], [EDIResponse], [CreatedBy], [CreatedOn] 
	FROM   [Operation].[DeclarationActivity]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] LIKE '%' +  @DeclarationNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationActivity] Record Into [DeclarationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationActivityInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityInsert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @AcitivtyMessage nvarchar(200),
    @ActivityBy nvarchar(60),
    @EDIMessageType nvarchar(50),
    @EDIResponse nvarchar(50),
    @CreatedBy nvarchar(60) 
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationActivity] (
			[BranchID], [DeclarationNo], [ActivityDate], [AcitivtyMessage], [ActivityBy], [EDIMessageType], [EDIResponse], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, GETUTCDATE(), @AcitivtyMessage, @ActivityBy, @EDIMessageType, @EDIResponse, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationActivity] Record Into [DeclarationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationActivityUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityUpdate] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @AcitivtyMessage nvarchar(200),
    @ActivityBy nvarchar(60),
    @EDIMessageType nvarchar(50),
    @EDIResponse nvarchar(50),
    @CreatedBy nvarchar(60) 
AS 
 
	
BEGIN

	UPDATE [Operation].[DeclarationActivity]
	SET    [AcitivtyMessage] = @AcitivtyMessage, [ActivityBy] = @ActivityBy, [EDIMessageType] = @EDIMessageType, [EDIResponse] = @EDIResponse, [CreatedBy] = @CreatedBy, [CreatedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivitySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationActivity] Record Into [DeclarationActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationActivitySave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivitySave] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivitySave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @AcitivtyMessage nvarchar(200),
    @ActivityBy nvarchar(60),
    @EDIMessageType nvarchar(50),
    @EDIResponse nvarchar(50),
    @CreatedBy nvarchar(60) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationActivity] 
		WHERE 	[ActivityID] = SCOPE_IDENTITY()
	       AND [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationActivityUpdate] 
		@BranchID, @DeclarationNo, @AcitivtyMessage, @ActivityBy, @EDIMessageType, @EDIResponse, @CreatedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationActivityInsert] 
		@BranchID, @DeclarationNo, @AcitivtyMessage, @ActivityBy, @EDIMessageType, @EDIResponse, @CreatedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationActivitySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationActivityDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationActivity] Record  based on [DeclarationActivity]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationActivityDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationActivityDelete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationActivityDelete] 
    @ActivityID bigint,
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Operation].[DeclarationActivity]
	WHERE  [ActivityID] = @ActivityID
	       AND [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationActivityDelete]
-- ========================================================================================================================================

GO
 