
-- ========================================================================================================================================
-- START											 [Operation].[usp_GenerateOrderHeaderFromDeclaration]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	24-Feb-2017
-- Description:	generate an Order Entry from the K2 Document Declaration No.
/*
Sample Script
Declare @NewOrderNo nvarchar(35)=''
Exec [Operation].[usp_GeneratePortBookingHeaderFromOrderHeader]  1007001,'IHQ170200014',@NewOrderNo = @NewOrderNo OUTPUT


	Select ContainerNo, Size, [Type], ContainerGrade, Temprature, TemperatureMode,  NUllif(Vent,0),VentMode, SealNo, SealNo2,VGM ,Remarks
	From Operation.OrderContainer 
	Where BranchID = 1007001 and OrderNo = 'EHQ170100002'

*/
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_GeneratePortBookingHeaderFromOrderHeader]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_GeneratePortBookingHeaderFromOrderHeader]
END 
GO
CREATE PROC  [Operation].[usp_GeneratePortBookingHeaderFromOrderHeader]
    @BranchID BIGINT,
    @ExistOrderNo NVARCHAR(50),
	@NewOrderNo nvarchar(35) OUTPUT
AS
BEGIN
 
	Declare 
    @OrderNo nvarchar(50),
    @BookingNo nvarchar(50) = NULL,
    @BLNo nvarchar(50) = NULL,
    @OrderDate datetime = NULL,
    @BookingType smallint,
    @TransportType smallint,
	@OrderType nvarchar(15),
    @CustomerCode bigint,
    @CustomerName nvarchar(50) = NULL,
    @ForwarderCode bigint = NULL,
    @ForwarderName nvarchar(50) = NULL,
    @ShippingAgent bigint = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @OwnerCode bigint = NULL,
    @LoadingPort nvarchar(10) = NULL,
    @DischargePort nvarchar(10) = NULL,
    @DestinationPort nvarchar(10) = NULL,
    @VesselScheduleID int,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @Wharf bigint = NULL,
    @IsAllowLateGate bit = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PortCutOffDry datetime = NULL,
    @PortCutOffReefer datetime = NULL,
    @FlightNo nvarchar(35) = NULL,
    @ARNNo nvarchar(35) = NULL,
    @VehicleNo1 nvarchar(35) = NULL,
    @VehicleNo2 nvarchar(35) = NULL,
    @WagonNo nvarchar(35) = NULL,
    @JKNo nvarchar(35) = NULL,
	@TotalQty nvarchar(20) = NULL,
	@UOM nvarchar(20) = NULL,
	@TotalVolume nvarchar(20) = NULL,
	@TotalWeight nvarchar(20) = NULL,
	@MarksNos nvarchar(200) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @RelationBranchID bigint = NULL;
	


     Declare @Dt as Datetime;
	 Set @Dt = GETUTCDATE();


	/*
	Constructing the Order Header
	1. get the declaration header details
	2. get the declaration shipmentdetails
	3. get the declaration Invoie Details

	*/


	/* Declaration Header Information */
	Select @BookingType=JobType,@TransportType = TransportType, @ShippingAgent=ShippingAgent, @ForwarderCode=FwdAgent, @CustomerCode=CustomerCode,@OrderType=OrderCategory,
	@LoadingPort=PortOfLoading, @DischargePort=PortOfDischarge,@VesselScheduleID=isnull(VesselScheduleID,''),
	@VesselID = VesselID,@VesselName = '', @Voyageno = VoyageNo,@ShipCallNo=SCNNo,@ETA = ETA,
	@FlightNo = FlightNo, @ARNNo = @ARNNo, @VehicleNo1 = VehicleNo1, @VehicleNo2 = VehicleNo2,@JKNo = JKNo,@WagonNo=WagonNo,
	@CreatedBy=CreatedBy, @ModifiedBy=ModifiedBy
	From Operation.OrderHeader 
	Where OrderNo = @ExistOrderNo and BranchID = @BranchID;


	--Declare @OrderShipmentType smallint =0

	--Select @OrderShipmentType = LookupID
	--From Config.Lookup Where LookupCategory ='ShipmentType' And LookupCode ='FCL'


	--Select @OrderCategory = Code
	--From Master.JobCategory 
	--Where	JobType= @JobType And ShipmentType = @OrderShipmentType

	 /* Execute to Create Booking Header*/
	 Exec [Port].[usp_BookingHeaderSave] @BranchID, @OrderNo, '','', @Dt, @BookingType, @TransportType, @OrderType, @CustomerCode,
	 '', @ForwarderCode, '', @ShippingAgent, '', null, @LoadingPort, @DischargePort, '', @VesselScheduleID, @VesselID, @VesselName, @VoyageNo,
	 @ShipCallNo, null, null, @Dt, @Dt, @Dt,@Dt, @FlightNo, @ARNNo, @VehicleNo1, @VehicleNo2, @WagonNo, @JKNo, null, null,null, null,null, null,null, null,
	 @CreatedBy, @ModifiedBy, @NewOrderNo= @NewOrderNo OUTPUT

	 print 'New order No : ' + @NewOrderNo 


	 /*Dump the K1 Declaration Container Information into a table variable */

	Declare @BookingContainer table (id smallint primary key identity(1,1),ContainerNo nvarchar(50),Size nvarchar(10), [Type] nvarchar(10), ContainerGrade smallint,
										Temprature decimal(18, 2), TemperatureMode smallint, Vent decimal(18, 2),VentMode smallint,
										SealNo nvarchar(20), SealNo2 nvarchar(20),VGM decimal(18, 4),Remarks nvarchar(MAX))


	/*Booking Container Variables */

	Declare    
    @ContainerKey nvarchar(50),
    @ContainerNo nvarchar(50),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ContainerGrade smallint = NULL,
    @EFIndicator smallint = NULL,
    @IMOCode nvarchar(20) = NULL,
    @UNNo nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TempratureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @CargoCategory smallint = NULL,
    @PickupDate datetime = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @VGM decimal(18, 4) = NULL,
    @StowageCell nvarchar(20) = NULL,
    @PUDOMode smallint = NULL,
    @Remarks nvarchar(MAX) = NULL,
	@Commodity nvarchar(MAX) = NULL,
	@SpecialInstructions nvarchar(MAX) = NULL
     
	 print '111'

	Insert into @BookingContainer(ContainerNo, Size, [Type],ContainerGrade, Temprature, TemperatureMode, Vent,VentMode, SealNo, SealNo2,VGM ,Remarks) 
	Select ContainerNo, Size, [Type], ContainerGrade, NULLIF(Temprature,0), TemperatureMode, NULLIF(Vent,0) As Vent,VentMode, SealNo, SealNo2,VGM ,Remarks
	From Operation.OrderContainer 
	Where BranchID = @BranchID and OrderNo = @ExistOrderNo

	print '222'
	Declare @vContainerKey varchar(50),
			@vContainerNo nvarchar(50),
			@vSize nvarchar(10),
			@vType nvarchar(10),
			@vContainerGrade smallint = NULL,
			@vTemperature decimal(18, 2) = NULL,
			@vTempratureType smallint = NULL,
			@vVent decimal(18, 2) = NULL,
			@vVentType smallint = NULL,
			@vSealNo1 nvarchar(20) = NULL,
			@vSealNo2 nvarchar(20) = NULL,
			@vVGM decimal(18, 4) = NULL,
			@vRemarks nvarchar(MAX) = NULL,
			@vCounter smallint=0,
			@vRecordCount smallint =0
			

	Select @vRecordCount = COUNT(0),@vCounter=1 From @BookingContainer

	print '333'
	 While @vCounter <= @vRecordCount
	 Begin
	    
		Select @vContainerNo = ContainerNo,@vSize=Size, @vType = [Type],@vContainerGrade=ContainerGrade, @vTemperature=Temprature, @vTempratureType=TemperatureMode, @vVent=Vent,@vVentType=VentMode, @vSealNo1=SealNo, @vSealNo2=SealNo2,@vVGM=VGM ,@vRemarks=Remarks
		From @BookingContainer
		Where id = @vCounter;

		Set @vContainerKey = @NewOrderNo + '-' + RIGHT('000' + Convert(varchar(3),@vCounter), 3)

		print '444' + @vContainerKey
	     
		
	Exec [Port].[usp_BookingContainerSave] 
			@BranchID, @NewOrderNo, @vContainerKey, @vContainerNo, @vSize, @vType, @vContainerGrade,null,null,null,@vTemperature, @vTempratureType,
			@vVent, @vVentType,null, @dt,@vSealNo1,@vSealNo2,@vVGM,null,null,@vRemarks,null,null,@CreatedBy, @ModifiedBy

		 Set @vCounter = @vCounter + 1
	   
	 End

	Return 1;
END