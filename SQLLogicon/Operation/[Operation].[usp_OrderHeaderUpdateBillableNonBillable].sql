

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderHeaderUpdateBillableNonBillable]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Update Billable Status in the [OrderHeader]
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderHeaderUpdateBillableNonBillable]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderHeaderUpdateBillableNonBillable] 
END 
GO
CREATE PROC [Operation].[usp_OrderHeaderUpdateBillableNonBillable] 
    @BranchID BIGINT,
    @OrderNo VARCHAR(50),
	@IsBillable bit,
	@ModifiedBy nvarchar(50)
AS 
 

BEGIN

	Update Operation.OrderHeader
	Set IsBillable = @IsBillable, ModifiedBy=@ModifiedBy,ModifiedOn = GETUTCDATE()
	Where
		BranchID = @BranchID
		And OrderNo = @OrderNo


END