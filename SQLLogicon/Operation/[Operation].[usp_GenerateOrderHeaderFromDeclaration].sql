
-- ========================================================================================================================================
-- START											 [Operation].[usp_GenerateOrderHeaderFromDeclaration]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	03-Feb-2017
-- Description:	generate an Order Entry from the K1 Document Declaration No.

-- Modification History :
-- Author:		Sharma
-- Create date: 	10-Feb-2017
-- Description:	Corrections made to the Order Container and Order Cargo stored procedure calls.

-- Modification History :
-- Author:		Sharma
-- Create date: 	19-May-2017
-- Description:	Added new additional parameters for the OrderEntry Save sp.




/*
Sample Script
Declare @NewOrderNo nvarchar(35)=''
Exec [Operation].[usp_GenerateOrderHeaderFromDeclaration]  1007001,'DLHQ170100001',@NewOrderNo = @NewOrderNo OUTPUT
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_GenerateOrderHeaderFromDeclaration]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_GenerateOrderHeaderFromDeclaration]
END 
GO
CREATE PROC  [Operation].[usp_GenerateOrderHeaderFromDeclaration] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
	@NewOrderNo nvarchar(35) OUTPUT
AS
BEGIN
 
	Declare 
	@OrderNo varchar(70)='',
    @OrderDate datetime,
    @MasterJobNo nvarchar(70)='',
    @ModuleID smallint,
    @JobType smallint,
    @ShipmentType smallint,
    @ServiceType smallint,
    @TransportType smallint,
    @IncoTerm smallint,
    @OrderCategory nvarchar(50)='',
    @ShipperCode bigint,
    @ConsigneeCode bigint,
    @PrincipalCode bigint,
    @CustomerCode bigint,
    @VendorCode bigint,
    @CoLoaderCode bigint,
    @ShippingAgent bigint,
    @ForeignAgent bigint,
    @FwdAgent bigint,
    @PortOperator bigint,
    @VesselScheduleID int,
    @VesselID nvarchar(20)='',
    @Voyageno nvarchar(10)='',
    @FeederVoyageNo nvarchar(10)='',
    @CustomerRef nvarchar(100)='',
    @OceanBLNo nvarchar(100)='',
    @HouseBLNo nvarchar(100)='',
    @ShippingLinerRefNo nvarchar(100)='',
    @ShippingNote nvarchar(100)='',
    @KANo nvarchar(100)='',
	@ManifestNo nvarchar(100)='',
    @SCNNo nvarchar(50)='',
    @PlaceOfReceipt nvarchar(10)='',
    @PortOfLoading nvarchar(10)='',
    @TranshipmentPort nvarchar(10)='',
    @PortOfDischarge nvarchar(10)='',
    @PlaceOfDischarge nvarchar(10)='',
    @DropOffCode nvarchar(10)='',
    @ClosingDate datetime,
    @ClosingDateReefer datetime,
    @ETA datetime,
    @ETD datetime,
    @YardCutOffTime datetime,
    @YardCutOffTimeReefer datetime,
    @IsMasterJob bit,
	@Remarks nvarchar(100)='',
	@CargoCurrencyCode nvarchar(3)='',
	@CargoExchangeRate float,
	@CargoBaseAmount numeric(18,4),
	@CargoLocalAmount numeric(18,4),
	@PrimaryYard nvarchar(50)='',
    @CreatedBy nvarchar(60)='',
    @ModifiedBy nvarchar(60)='',
	@NotifyParty bigint=0,
	@NotifyPartyAddressID INT=0,
	@CustomerAddressID INT=0,
	@ShipperAddressID INT=0,
	@ConsigneeAddressID INT=0,
	@InvoiceNo nvarchar(35)='',
	@InvoiceAmount decimal(18,4)=0,
	@InvoiceCurrency nvarchar(3)='',
	@LocalAmount decimal(18,4)=0,
	@ExchangeRate decimal(18,4)=0,
	@PONo nvarchar(35)='',
	@COLoaderBLNo nvarchar(35)='',
	@ContractNo nvarchar(35)='',
	@SalesOrderNo nvarchar(35)='',
	@SalesOrderDate DateTime,
	@SalesTerm nvarchar(100)='',
	@PaymentTerm nvarchar(100)='',
	@VehicleNo1 nvarchar(35)='',
	@VehicleNo2 nvarchar(35)='',
	@WagonNo nvarchar(35)='',
	@FlightNo nvarchar(35)='',
	@ARNNo nvarchar(35)='',
	@MasterAWBNo nvarchar(35)='',
	@HouseAWBNo nvarchar(35)='',
	@AIRCOLoadNo nvarchar(35)='',
	@JKNo nvarchar(35)='',
	@InvoiceDate datetime,
	@ShippingAgentAddressID INT=0,
	@FwdAgentAddressID INT=0,
	@VesselName nvarchar(35)='',
	@CargoClass smallint=NULL,
	@CargoDescription nvarchar(150)=NULL,
	@MessageID UniqueIdentifier,
	@StowageCode smallint=NULL,
	@ExtraCargoDescription nvarchar(300) = NULL,
	@IsPartial bit =1,
	@Flag bit =0,
	@OverseasAgentCode bigint=0,
	@OverseasAgentAddressID int=0,
	@FregihtForwarderCode bigint=0,
	@FreightForwarderAddressID int=0,
	@WebOrderStatus smallint = 0


	Select @WebOrderStatus = ISNULL(LookupID,0)
	From Config.Lookup 
	Where LookupCategory='WebOrderStatus' And LookupCode='WEB-D1'





     Declare @Dt as Datetime;
	 Set @Dt = GETUTCDATE();

	 Set @OrderNo = (Select ISNULL(OrderNo, '') as OrderNo From Operation.DeclarationHeader Where BranchID = @BranchID And DeclarationNo = @DeclarationNo)


	/*
	Constructing the Order Header
	1. get the declaration header details
	2. get the declaration shipmentdetails
	3. get the declaration Invoie Details

	*/


	/* Declaration Header Information */
	Select @ShipmentType = ShipmentType, @TransportType = TransportMode, @ShippingAgent = ShippingAgent,
	@ShipperCode = Exporter, @ConsigneeCode = Importer,
	@CargoClass=CargoClass,@CargoDescription=CargoDescription,@ExtraCargoDescription= ExtraCargoDescription, @CustomerRef = CustomerReferenceNo
	From Operation.DeclarationHeader 
	Where DeclarationNo = @DeclarationNo and BranchID = @BranchID;

		if((Select count(0) From Master.Branch B INNER JOIN Master.Merchant M ON 
					B.CompanyCode = M.CompanyCode 
					And B.BranchName = M.MerchantName
					Where 
					B.branchid =@BranchID)>0)
			Begin
				set @FwdAgent = (Select ISNULL(M.MerchantCode,0) From Master.Branch B
					INNER JOIN Master.Merchant M ON 
					B.CompanyCode = M.CompanyCode 
					And B.BranchName = M.MerchantName
					Where 
					B.branchid =@BranchID)
					if((Select count(0) From Master.Address Where LinkID = @FwdAgent)>0)
						Begin
							/* Get The Shipping Agent Address From the Master Data */
							Select @FwdAgentAddressID = ISNULL(AddressID,0)
							From Master.Address Where LinkID = @FwdAgent
						End
					else
						Begin
							set @FwdAgentAddressID = 0
						end
			END
		ELSE
			BEGIN
				set @FwdAgent =0;
				
				/* Get The Shipping Agent Address From the Master Data */
				set @FwdAgentAddressID = 0
			END

	/* Get The Shipper Address From the Master Data */
	Select @ShipperAddressID = ISNULL(AddressID,0)
	From Master.Address Where LinkID = @ShipperCode

	/* Get The Consignee Address From the Master Data */
	Select @ConsigneeAddressID = ISNULL(AddressID,0)
	From Master.Address Where LinkID = @ConsigneeCode

	/* Get The Shipping Agent Address From the Master Data */
	Select @ShippingAgentAddressID = ISNULL(AddressID,0)
	From Master.Address Where LinkID = @ShippingAgent



	/* Declaration Shipment Information */
	Select @ManifestNo = ManifestNo, @VesselScheduleID = isnull(VesselScheduleID,''), @VesselID = VesselID,
	@VesselName = VesselName, @Voyageno = VoyageNo, @OceanBLNo = OceanBLNo, @HouseBLNo = HouseBLNo,
	@SCNNo = SCNNo, @PortOfLoading = LoadingPort, @TranshipmentPort = TranshipmentPort,
	@PortOfDischarge = DischargePort, @ETA = ETADate, @CreatedBy = CreatedBy, @ModifiedBy = ModifiedBy,
	@VehicleNo1 = VehicleNo1, @VehicleNo2 = VehicleNo2, @FlightNo = FlightNo, @ARNNo = @ARNNo, @MasterAWBNo = MasterAWBNo,
	@HouseAWBNo = HouseAWBNo, @AIRCOLoadNo = AIRCOLoadNo, @JKNo = JKNo, @PortOperator = PortOperator,
	@PlaceOfReceipt = PlaceOfImport
	From Operation.DeclarationShipment 
	Where DeclarationNo = @DeclarationNo and BranchID = @BranchID;

	/* Declaration Invoice Information */
	Select @InvoiceNo = InvoiceNo, @InvoiceAmount = InvoiceValue, @InvoiceCurrency = InvoiceCurrencyCode,
	@LocalAmount = (InvoiceValue * CurrencyExRate), @ExchangeRate = CurrencyExRate, @IncoTerm = IncoTerm,
	@InvoiceDate = InvoiceDate
	From Operation.DeclarationInvoice 
	Where DeclarationNo = @DeclarationNo and BranchID = @BranchID;
	 

	 /*Get the Lookup Values against the Declaration */
	 /*K1 = IMPORT */

	 Select @JobType = ISNULL(LookupID,0)
	 From Config.Lookup
	 Where LookupCategory ='JobType' And LookupCode='IMPORT'


	Declare @OrderShipmentType smallint =0

	Select @OrderShipmentType = LookupID
	From Config.Lookup Where LookupCategory ='ShipmentType' And LookupCode ='FCL'


	/*Get the Login Branch's CompanyCode. The Login CompanyCode is the Frowarding Agent for the K Forms.*/

	--Set @FwdAgent = @BranchID


	Select @OrderCategory = Code
	From Master.JobCategory 
	Where	JobType= @JobType And ShipmentType = @OrderShipmentType

	Set @ModifiedBy = @CreatedBy;
	 /* Execute to ALTER Order Header*/
	 Exec Operation.usp_OrderHeaderSave @BranchID, @OrderNo, @Dt, @MasterJobNo, 0, @JobType, @ShipmentType, 0,
	 @TransportType, @IncoTerm, @OrderCategory, @ShipperCode, @ConsigneeCode, 0, @ConsigneeCode, 0, 0, @ShippingAgent, 0, @FwdAgent, 
	 @PortOperator, @VesselScheduleID,
	 @VesselID, @Voyageno, '',@CustomerRef,@OceanBLNo,@HouseBLNo, '', '','', @ManifestNo, @SCNNo, @PlaceOfReceipt,
	 @PortOfLoading, @TranshipmentPort, @PortOfDischarge, @PortOfDischarge, @DropOffCode, 
	 @Dt, @Dt, @ETA, @Dt, @Dt, @Dt, 0, '', @InvoiceCurrency,@ExchangeRate,@InvoiceAmount,@LocalAmount,'',
	 @CreatedBy, @ModifiedBy,@NotifyParty,@NotifyPartyAddressID,@CustomerAddressID,@ShipperAddressID,
	 @ConsigneeAddressID, @InvoiceNo, @InvoiceAmount, @InvoiceCurrency, @LocalAmount, 
	 @ExchangeRate,@PONo, @COLoaderBLNo,@ContractNo,@SalesOrderNo,@Dt,@SalesTerm,@PaymentTerm,@VehicleNo1, 
	 @VehicleNo2, @WagonNo, @FlightNo, @ARNNo, @MasterAWBNo, @HouseAWBNo, @AIRCOLoadNo, @JKNo, 
	 @InvoiceDate,@ShippingAgentAddressID,@FwdAgentAddressID,@CargoClass,@CargoDescription,@StowageCode,@ExtraCargoDescription,
	 @IsPartial,@Flag,@OverseasAgentCode,@OverseasAgentAddressID,@FregihtForwarderCode,@FreightForwarderAddressID,@WebOrderStatus,
	 @NewDocumentNo= @NewOrderNo OUTPUT

	


	 print 'New order No : ' + @NewOrderNo 


	 /*Dump the K1 Declaration Container Information into a table variable */

	Declare @orderContainer table (id smallint primary key identity(1,1),ContainerNo nvarchar(15),Size nvarchar(2), [Type] nvarchar(2), IsSOC bit)

	/*Order Container Variables */

	Declare    
    @SealNo nvarchar(20)='',
    @SealNo2 nvarchar(20)='',
    @ContainerGrade smallint=0,
    @Temprature varchar(10)='',
    @TemperatureMode smallint=0,
    @Vent varchar(10)='',
    @VentMode smallint=0,
    @GrossWeight decimal(18,7)=0,
    @CargoWeight decimal(18,7)=0,
	@VGM decimal(18,7)=0,
	@VGMType smallint=0,
    @CargoType smallint=0,
    @TrailerType smallint=0,
    @CargoHandling nvarchar(20)='',
	@Volume nvarchar(10)='',
	@ContainerRef nvarchar(50)='',
	@WeighingDate datetime,
	@WeighingPlace nvarchar(50)='',
	@CertificateNo nvarchar(50)='',
	@VerificationCountry nvarchar(2)='',
	@SOLASMethod smallint=0,
	@RequiredDate datetime,
	@OriginCountry nvarchar(2)='' ,
	@MasterJobBranchID bigint=NULL,
	@MasterJobNoContainer varchar(50)=NULL,
	@MasterContainerKey varchar(50)=NULL,
	@ContainerState smallint=NULL

     

	Insert into @orderContainer(ContainerNo, Size, Type, IsSOC) 
	Select ContainerNo, Size, Type, IsSOC 
	From Operation.DeclarationContainer 
	Where BranchID = @BranchID and DeclarationNo = @DeclarationNo

	Declare @vContainerKey varchar(20),
			@vContainerNo nvarchar(20),
			@vSize nvarchar(2),
			@vType nvarchar(2),
			@vCounter smallint=0,
			@vRecordCount smallint =0,
			@vIsSOCType bit =0
			

	Select @vRecordCount = COUNT(0),@vCounter=1 From @orderContainer


	 While @vCounter <= @vRecordCount
	 Begin
	    
		Select @vContainerNo = ContainerNo,@vSize=Size, @vType = [Type], @vIsSOCType = IsSOC
		From @orderContainer
		Where id = @vCounter;

		Set @vContainerKey = @NewOrderNo + '-' + RIGHT('000' + Convert(varchar(3),@vCounter), 3)

		print @vContainerKey
	     
		Exec [Operation].[usp_OrderContainerSave] 
			@BranchID, @NewOrderNo, @vContainerKey, @vContainerNo, @vSize, @vType, @SealNo,@SealNo2, @ContainerGrade, @Temprature, @TemperatureMode, 
			@Vent, @VentMode, @GrossWeight, @CargoWeight,@VGM,@VGMType, @CargoType, @TrailerType, @CargoHandling,@Volume,@Remarks,
			@ContainerRef, @DeclarationNo,@WeighingDate,@WeighingPlace,@CertificateNo,@VerificationCountry,@SOLASMethod,
			@dt,@OriginCountry,	@CreatedBy, @ModifiedBy,@MasterJobBranchID,@MasterJobNoContainer,@MasterContainerKey,@ContainerState, @vIsSOCType

		 Set @vCounter = @vCounter + 1
	   
	 End

	
	
	/* Order Cargo Variables */			


		Declare 
		@CargoKey nvarchar(50)='',
		@ShipperConsigneeCode nvarchar(10)='',
		@ShipperConsigneeName nvarchar(50)='',
		@ProductDescription nvarchar(100)='',
		@StockRoom nvarchar(10)='',
		@Qty float=0,
		@UOM nvarchar(20)='',
		@GrossVolume float=0,
		@CargoLine nvarchar(10)='',
		@MarksNumbers nvarchar(100)='',
		@PalletCount smallint=0,
		@Len smallint=0,
		@Width smallint=0,
		@Height smallint=0,
		@HSCode nvarchar(20)='',
		@CountryCode varchar(2)='',
		@ItemDescription nvarchar(50)='',
		@ForeignCurrencyCode nvarchar(3)='',
		@ForeignPrice decimal(18, 4),
		@LocalCurrencyCode nvarchar(3)='',
		@LocalPrice decimal(18, 4),
		@IsGST bit=1,
		@GSTCurrencyCode nvarchar(3)='',
		@PackageCount int=0,
		@PackageType nvarchar(10)='',
		@SpecialCargoHandling nvarchar(100)='',
		@IMOCode nvarchar(20)='',
		@UNNo nvarchar(10)=''


	

		Declare @orderCargo table (ID smallint primary key identity(1,1),ProductDescription nvarchar(100),DeclaredQty decimal(18,7),
									declaredUOM nvarchar(20), ItemDescription nvarchar(50))

	
		
		Insert into @orderCargo(ProductDescription,DeclaredQty,declaredUOM,ItemDescription)
		Select ProductCode,DeclaredQty,DeclaredUOM, ItemDescription1
		From Operation.DeclarationItem 
		Where BranchID = @BranchID 
		And DeclarationNo = @DeclarationNo 

		Select @vRecordCount = COUNT(0),@vCounter = 1 From @orderCargo


	 While @vCounter <= @vRecordCount
	 Begin
	  
		Select @ProductDescription = ProductDescription,@Qty = Convert(float,DeclaredQty), @UOM = declaredUOM, @ItemDescription=ItemDescription
		From @orderCargo
		Where ID = @vCounter
	  

	   Exec [Operation].[usp_OrderCargoSave] 
				@BranchID, @NewOrderNo,'', @CargoKey, @ShipperConsigneeCode, @ShipperConsigneeName, @ProductDescription, 
				@StockRoom, @Qty, @UOM, @GrossVolume, @GrossWeight, @CargoLine, 
				@MarksNumbers, @Remarks, @PalletCount,@Len,@Width,@Height, @HSCode,@CountryCode,@DeclarationNo, @CreatedBy,@ModifiedBy,
				@ItemDescription, @InvoiceCurrency, @InvoiceAmount, '', @ExchangeRate, @LocalAmount, @IsGST, '', 
				@PackageCount, @PackageType, @SpecialCargoHandling,@IMOCode,@UNNo


		Set @vCounter = @vCounter + 1

	   
	 End


	Return 1;
END

