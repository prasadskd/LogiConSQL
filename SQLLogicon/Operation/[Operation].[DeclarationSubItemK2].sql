
-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2Select]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationSubItemK2] Record based on [DeclarationSubItemK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2Select]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2Select] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2Select] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT,
    @SubItemNo SMALLINT
AS 

BEGIN

	SELECT	[BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], 
			[POilUnitPrice], [POilTotalValue], [POilRate], [POilAmount], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationSubItemK2]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  
			AND [ItemNo] = @ItemNo  
			AND [SubItemNo] = @SubItemNo  
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemK2Select]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2List]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationSubItemK2] Records from [DeclarationSubItemK2] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2List]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2List] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2List] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
    @ItemNo SMALLINT

AS 
BEGIN

	SELECT	[BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], 
			[POilUnitPrice], [POilTotalValue], [POilRate], [POilAmount], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Operation].[DeclarationSubItemK2]
	WHERE	[BranchID] = @BranchID  
			AND [DeclarationNo] = @DeclarationNo  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemK2List] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2Insert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationSubItemK2] Record Into [DeclarationSubItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2Insert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2Insert] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2Insert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @POilUnitPrice decimal(18, 7),
    @POilTotalValue decimal(18, 7),
    @POilRate decimal(18, 7),
    @POilAmount decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Operation].[DeclarationSubItemK2] (
			[BranchID], [DeclarationNo], [ItemNo], [SubItemNo], [ItemType], [ItemCode], [PType], [PMeterFigure], [PTankFigure], 
			[POilUnitPrice], [POilTotalValue], [POilRate], [POilAmount], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, 
			@POilUnitPrice, @POilTotalValue, @POilRate, @POilAmount, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemK2Insert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2Update]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationSubItemK2] Record Into [DeclarationSubItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2Update]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2Update] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2Update] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @POilUnitPrice decimal(18, 7),
    @POilTotalValue decimal(18, 7),
    @POilRate decimal(18, 7),
    @POilAmount decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE	[Operation].[DeclarationSubItemK2]
	SET		[ItemType] = @ItemType, [ItemCode] = @ItemCode, [PType] = @PType, [PMeterFigure] = @PMeterFigure, [PTankFigure] = @PTankFigure, 
			[POilUnitPrice] = @POilUnitPrice, [POilTotalValue] = @POilTotalValue, [POilRate] = @POilRate, [POilAmount] = @POilAmount, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [SubItemNo] = @SubItemNo
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemK2Update]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2Save]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationSubItemK2] Record Into [DeclarationSubItemK2] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2Save]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2Save] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2Save] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint,
    @ItemType nvarchar(20),
    @ItemCode nvarchar(20),
    @PType nvarchar(20),
    @PMeterFigure decimal(18, 7),
    @PTankFigure decimal(18, 7),
    @POilUnitPrice decimal(18, 7),
    @POilTotalValue decimal(18, 7),
    @POilRate decimal(18, 7),
    @POilAmount decimal(18, 7),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[DeclarationSubItemK2] 
		WHERE 	[BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [SubItemNo] = @SubItemNo)>0
	BEGIN
	    Exec [Operation].[usp_DeclarationSubItemK2Update] 
		@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, @POilUnitPrice, 
		@POilTotalValue, @POilRate, @POilAmount, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_DeclarationSubItemK2Insert] 
		@BranchID, @DeclarationNo, @ItemNo, @SubItemNo, @ItemType, @ItemCode, @PType, @PMeterFigure, @PTankFigure, @POilUnitPrice, 
		@POilTotalValue, @POilRate, @POilAmount, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[DeclarationSubItemK2Save]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_DeclarationSubItemK2Delete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationSubItemK2] Record  based on [DeclarationSubItemK2]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_DeclarationSubItemK2Delete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_DeclarationSubItemK2Delete] 
END 
GO
CREATE PROC [Operation].[usp_DeclarationSubItemK2Delete] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @ItemNo smallint,
    @SubItemNo smallint
AS 

	
BEGIN

	DELETE
	FROM   [Operation].[DeclarationSubItemK2]
	WHERE  [BranchID] = @BranchID
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	       AND [SubItemNo] = @SubItemNo


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSubItemK2Delete]
-- ========================================================================================================================================

GO
