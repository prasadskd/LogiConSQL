
-- ========================================================================================================================================
-- START											 [Operation].[usp_VesselScheduleSearchByVesselID]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [VesselSchedule] Record based on [VesselSchedule] table

/*

Exec [Operation].[usp_VesselScheduleSearchByVesselID]  1001001,'Q92'
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_VesselScheduleSearchByVesselID]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_VesselScheduleSearchByVesselID] 
END 
GO
CREATE PROC [Operation].[usp_VesselScheduleSearchByVesselID] 
	@BranchID bigint,
	@VesselID nvarchar(20)=NULL
AS 

BEGIN

	;with
	JobTypeLookup As (select LookupID,LookupDescription From config.Lookup Where LookupCategory ='JobType')
	SELECT TOP (10) VS.[VesselScheduleID], VS.[VesselID], VS.[VoyageNoInWard],VS.[VoyageNoOutWard],vs.[AgentCode],VS.[JobType], VS.[LoadingPort], 
	VS.[DischargePort], VS.[DestinationPort], VS.[Terminal], 
	VS.[ETA],VS.[ETA] As ETATime, VS.[ETD],VS.[ETD] As ETDTime , VS.[ActualETA], VS.[ActualETA] As ActualETATime, 
	VS.[ActualETD],VS.[ActualETD] As ActualETDTime , VS.[ImpAvaliableDate], VS.[ExpAvailableDate], VS.[ClosingDate], VS.[ClosingDate] As ClosingTime, 
	VS.[ClosingDateRF], VS.[ClosingDateRF] As ClosingTimeRF,
	VS.[YardCutOffDate], VS.[YardCutOffDateRF], 
	VS.[YardCutOffDate] As YardCutOffTime, VS.[YardCutOffDateRF] As YardCutOffTimeRF,
	VS.[ImportStorageStartDate], VS.[IsRevised], VS.[ShipCallNo], VS.[CreatedBy], VS.[CreatedOn], 
	VS.[ModifiedBy], VS.[ModifiedOn] ,	Vsl.VesselName ,
	ISNULL(Jb.LookupDescription,'') As JobTypeDescription,
	COALESCE(Agnt.MerchantName,VS.AgentCode) As AgentName,
	COALESCE(LP.PortName,Vs.LoadingPort) As LoadingPortName,
	COALESCE(DiP.PortName,VS.DischargePort) As DischargePortName,
	COALESCE(DeP.PortName,VS.DestinationPort) As DestinationPortName,
	COALESCE(Ter.MerchantName,VS.Terminal) As TerminalName,
	Vsl.CallSignNo As CallSignNo
	FROM   [Operation].[VesselSchedule] VS
	Left Outer Join master.Vessel Vsl On 
		Vs.VesselID = Vsl.VesselID
	Left Outer Join JobTypeLookup Jb ON 
		VS.JobType = jb.LookupID
	Left Outer Join Master.Merchant Agnt On
		Vs.AgentCode = Convert(varchar(20),Agnt.MerchantCode)
		And Agnt.IsLiner = CAST(1 as bit)
	Left Outer Join Master.Port LP On 
		VS.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DiP On 
		VS.DischargePort = DiP.PortCode
	Left Outer Join Master.Port DeP On 
		VS.DestinationPort = DeP.PortCode
	Left Outer Join Master.Merchant Ter ON
		Vs.Terminal = Convert(varchar(10),Ter.MerchantCode)
		And Ter.IsTerminal = CAST(1 as bit)
	WHERE 
	VS.BranchID = @BranchID
	And Vs.VesselID = ISNULL(@VesselID,Vs.VesselID)
	Order By VS.[VesselScheduleID] Desc
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_VesselScheduleSearchByVesselID]
-- ========================================================================================================================================

GO
