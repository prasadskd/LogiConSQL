
-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderContainer] Record based on [OrderContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderContainerSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerSelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerSelect] 
    @BranchID BIGINT,
    @OrderNo VARCHAR(50),
    @ContainerKey VARCHAR(50)
AS 

SET NOCOUNT ON; 

BEGIN

	;with 
	TempratureLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TempratureType'),
	ContainerGradeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='ContainerGradeType'),
	VentLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='VentilationType'),
	CargoTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='CargoType'),
	TrailerTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TrailerType'),
	SpecialHandlingTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='SpecialHandlingType')
	SELECT	Dt.[BranchID], Dt.[OrderNo], Dt.[ContainerKey], Dt.[ContainerNo], Dt.[Size], Dt.[Type], Dt.[SealNo], Dt.[SealNo2], Dt.[ContainerGrade], 
			Dt.[Temprature], Dt.[TemperatureMode],Dt.[Vent], Dt.[VentMode], Dt.[GrossWeight], Dt.[CargoWeight], Dt.[VGM],Dt.[VGMType],
			Dt.[CargoType], Dt.[TrailerType], Dt.[CargoHandling],Dt.[Volume],Dt.[Remarks],
			Dt.[ContainerRef], Dt.[HaulageStatus], Dt.[CFSStatus], Dt.[DepotStatus], Dt.[FreightStatus], 
			Dt.[PickUpDropOffCode], Dt.[WagonNo], Dt.[JobNo], Dt.[SubJobNo], 
			Dt.[EmptyInDate], Dt.[EmptyStartDate], Dt.[FullInDate], Dt.[FullInStartDate], Dt.[FullOutDate], Dt.[DeclarationNo],
			Dt.[WeighingDate],Dt.[WeighingPlace],Dt.[CertificateNo],Dt.[VerificationCountry],Dt.[SOLASMethod],
			Dt.[RequiredDate],Dt.[OriginCountry],
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,0 As Selected,
			ISNULL(T.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(V.LookupDescription,'') As VentModeDescription,
			ISNULL(CG.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(CT.LookupDescription,'') As CargoTypeDescription,
			ISNULL(TT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(ST.LookupDescription,'') As SpecialHandlingTypeDescription,Dt.IsSOC 
	FROM	[Operation].[OrderContainer] Dt
	Left Outer Join TempratureLookup T ON 
		Dt.TemperatureMode = T.LookupID
	Left Outer Join VentLookup V ON 
		Dt.VentMode = V.LookupID
	Left Outer Join ContainerGradeLookup CG ON 
		Dt.ContainerGrade= CG.LookupID
	Left Outer Join CargoTypeLookup CT ON 
		Dt.CargoType = CT.LookupID
	Left Outer Join TrailerTypeLookup TT ON 
		Dt.TrailerType = TT.LookupID
	Left Outer Join SpecialHandlingTypeLookup ST ON 
		Dt.TrailerType = ST.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo  
			AND [ContainerKey] = @ContainerKey  

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerSelect]
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderContainer] Records from [OrderContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderContainerList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerList] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerList] 
    @BranchID BIGINT,
    @OrderNo VARCHAR(50)
AS 
BEGIN

SET NOCOUNT ON;

	;with 
	TempratureLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TempratureType'),
	ContainerGradeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='ContainerGradeType'),
	VentLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='VentilationType'),
	CargoTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='CargoType'),
	TrailerTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TrailerType'),
	SpecialHandlingTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='SpecialHandlingType')
	SELECT	Dt.[BranchID], Dt.[OrderNo], Dt.[ContainerKey], Dt.[ContainerNo], Dt.[Size], Dt.[Type], Dt.[SealNo], Dt.[SealNo2], Dt.[ContainerGrade], 
			Dt.[Temprature], Dt.[TemperatureMode],Dt.[Vent], Dt.[VentMode], Dt.[GrossWeight], Dt.[CargoWeight], Dt.[VGM],Dt.[VGMType],
			Dt.[CargoType], Dt.[TrailerType], Dt.[CargoHandling],Dt.[Volume],Dt.[Remarks],
			Dt.[ContainerRef], Dt.[HaulageStatus], Dt.[CFSStatus], Dt.[DepotStatus], Dt.[FreightStatus], 
			Dt.[PickUpDropOffCode], Dt.[WagonNo], Dt.[JobNo], Dt.[SubJobNo], 
			Dt.[EmptyInDate], Dt.[EmptyStartDate], Dt.[FullInDate], Dt.[FullInStartDate], Dt.[FullOutDate], Dt.[DeclarationNo],
			Dt.[WeighingDate],Dt.[WeighingPlace],Dt.[CertificateNo],Dt.[VerificationCountry],Dt.[SOLASMethod],
			Dt.[RequiredDate],Dt.[OriginCountry],MasterJobBranchID,MasterJobNo,MasterContainerKey,ContainerState,
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,0 As Selected,
			ISNULL(T.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(V.LookupDescription,'') As VentModeDescription,
			ISNULL(CG.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(CT.LookupDescription,'') As CargoTypeDescription,
			ISNULL(TT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(ST.LookupDescription,'') As SpecialHandlingTypeDescription,Dt.IsSOC 

	FROM	[Operation].[OrderContainer] Dt
	Left Outer Join TempratureLookup T ON 
		Dt.TemperatureMode = T.LookupID
	Left Outer Join VentLookup V ON 
		Dt.VentMode = V.LookupID
	Left Outer Join ContainerGradeLookup CG ON 
		Dt.ContainerGrade= CG.LookupID
	Left Outer Join CargoTypeLookup CT ON 
		Dt.CargoType = CT.LookupID
	Left Outer Join TrailerTypeLookup TT ON 
		Dt.TrailerType = TT.LookupID
	Left Outer Join SpecialHandlingTypeLookup ST ON 
		Dt.TrailerType = ST.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderCantainer] Records from [OrderCantainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderContainerPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerPageView] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerPageView] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;with 
	TempratureLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TempratureType'),
	ContainerGradeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='ContainerGradeType'),
	VentLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='VentilationType'),
	CargoTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='CargoType'),
	TrailerTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TrailerType'),
	SpecialHandlingTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='SpecialHandlingType')
	SELECT	Dt.[BranchID], Dt.[OrderNo], Dt.[ContainerKey], Dt.[ContainerNo], Dt.[Size], Dt.[Type], Dt.[SealNo], Dt.[SealNo2], Dt.[ContainerGrade], 
			Dt.[Temprature], Dt.[TemperatureMode],Dt.[Vent], Dt.[VentMode], Dt.[GrossWeight], Dt.[CargoWeight], Dt.[VGM],Dt.[VGMType],
			Dt.[CargoType], Dt.[TrailerType], Dt.[CargoHandling],Dt.[Volume],Dt.[Remarks],
			Dt.[ContainerRef], Dt.[HaulageStatus], Dt.[CFSStatus], Dt.[DepotStatus], Dt.[FreightStatus], 
			Dt.[PickUpDropOffCode], Dt.[WagonNo], Dt.[JobNo], Dt.[SubJobNo], 
			Dt.[EmptyInDate], Dt.[EmptyStartDate], Dt.[FullInDate], Dt.[FullInStartDate], Dt.[FullOutDate], Dt.[DeclarationNo],
			Dt.[WeighingDate],Dt.[WeighingPlace],Dt.[CertificateNo],Dt.[VerificationCountry],Dt.[SOLASMethod],
			Dt.[RequiredDate],Dt.[OriginCountry],MasterJobBranchID,MasterJobNo,MasterContainerKey,ContainerState,
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,0 As Selected,
			ISNULL(T.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(V.LookupDescription,'') As VentModeDescription,
			ISNULL(CG.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(CT.LookupDescription,'') As CargoTypeDescription,
			ISNULL(TT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(ST.LookupDescription,'') As SpecialHandlingTypeDescription,Dt.IsSOC 

	FROM	[Operation].[OrderContainer] Dt
	Left Outer Join TempratureLookup T ON 
		Dt.TemperatureMode = T.LookupID
	Left Outer Join VentLookup V ON 
		Dt.VentMode = V.LookupID
	Left Outer Join ContainerGradeLookup CG ON 
		Dt.ContainerGrade= CG.LookupID
	Left Outer Join CargoTypeLookup CT ON 
		Dt.CargoType = CT.LookupID
	Left Outer Join TrailerTypeLookup TT ON 
		Dt.TrailerType = TT.LookupID
	Left Outer Join SpecialHandlingTypeLookup ST ON 
		Dt.TrailerType = ST.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo
	ORDER BY [ContainerKey]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderContainerRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerRecordCount] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[OrderContainer]
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderCargo] Record based on [OrderCargo] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderContainerAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerAutoCompleteSearch] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50) 
AS 

BEGIN

	;with 
	TempratureLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TempratureType'),
	ContainerGradeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='ContainerGradeType'),
	VentLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='VentilationType'),
	CargoTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='CargoType'),
	TrailerTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='TrailerType'),
	SpecialHandlingTypeLookup As (select Lookupid,LookupDescription From config.Lookup Where LookupCategory='SpecialHandlingType')
	SELECT	Dt.[BranchID], Dt.[OrderNo], Dt.[ContainerKey], Dt.[ContainerNo], Dt.[Size], Dt.[Type], Dt.[SealNo], Dt.[SealNo2], Dt.[ContainerGrade], 
			Dt.[Temprature], Dt.[TemperatureMode],Dt.[Vent], Dt.[VentMode], Dt.[GrossWeight], Dt.[CargoWeight], Dt.[VGM],Dt.[VGMType],
			Dt.[CargoType], Dt.[TrailerType], Dt.[CargoHandling],Dt.[Volume],Dt.[Remarks],
			Dt.[ContainerRef], Dt.[HaulageStatus], Dt.[CFSStatus], Dt.[DepotStatus], Dt.[FreightStatus], 
			Dt.[PickUpDropOffCode], Dt.[WagonNo], Dt.[JobNo], Dt.[SubJobNo], 
			Dt.[EmptyInDate], Dt.[EmptyStartDate], Dt.[FullInDate], Dt.[FullInStartDate], Dt.[FullOutDate], Dt.[DeclarationNo],
			Dt.[WeighingDate],Dt.[WeighingPlace],Dt.[CertificateNo],Dt.[VerificationCountry],Dt.[SOLASMethod],
			Dt.[RequiredDate],Dt.[OriginCountry],MasterJobBranchID,MasterJobNo,MasterContainerKey,ContainerState,
			Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn] ,0 As Selected,
			ISNULL(T.LookupDescription,'') As TemperatureModeDescription,
			ISNULL(V.LookupDescription,'') As VentModeDescription,
			ISNULL(CG.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(CT.LookupDescription,'') As CargoTypeDescription,
			ISNULL(TT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(ST.LookupDescription,'') As SpecialHandlingTypeDescription,Dt.IsSOC 

	FROM	[Operation].[OrderContainer] Dt
	Left Outer Join TempratureLookup T ON 
		Dt.TemperatureMode = T.LookupID
	Left Outer Join VentLookup V ON 
		Dt.VentMode = V.LookupID
	Left Outer Join ContainerGradeLookup CG ON 
		Dt.ContainerGrade= CG.LookupID
	Left Outer Join CargoTypeLookup CT ON 
		Dt.CargoType = CT.LookupID
	Left Outer Join TrailerTypeLookup TT ON 
		Dt.TrailerType = TT.LookupID
	Left Outer Join SpecialHandlingTypeLookup ST ON 
		Dt.TrailerType = ST.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] LIKE '%' +  @OrderNo  + '%'
			 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderCargoAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderContainer] Record Into [OrderContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderContainerInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerInsert] 
    @BranchID BIGINT,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @SealNo nvarchar(20),
    @SealNo2 nvarchar(20),
    @ContainerGrade smallint,
    @Temprature varchar(10),
    @TemperatureMode smallint,
    @Vent varchar(10),
    @VentMode smallint,
    @GrossWeight varchar(10),
    @CargoWeight varchar(10),
	@VGM decimal(18,7),
	@VGMType smallint,
    @CargoType smallint,
    @TrailerType smallint,
    @CargoHandling nvarchar(20),
	@Volume nvarchar(10),
	@Remarks nvarchar(100),
	@ContainerRef nvarchar(50),
	@DeclarationNo nvarchar(50),
	@WeighingDate datetime,
	@WeighingPlace nvarchar(50),
	@CertificateNo nvarchar(50),
	@VerificationCountry nvarchar(2),
	@SOLASMethod smallint,
	@RequiredDate datetime,
	@OriginCountry nvarchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo varchar(50),
	@MasterContainerKey varchar(50),
	@ContainerState smallint,
	@IsSOC bit
AS 
  

BEGIN
 
	
	INSERT INTO [Operation].[OrderContainer] (
			[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [SealNo],[SealNo2], [ContainerGrade], [Temprature], [TemperatureMode], 
			[Vent], [VentMode], [GrossWeight], [CargoWeight], [VGM], [VGMType], [CargoType], [TrailerType],[CargoHandling],[Volume],[Remarks],
			[ContainerRef],[DeclarationNo],[WeighingDate],[WeighingPlace],[CertificateNo],[VerificationCountry],[SOLASMethod],
			[RequiredDate],[OriginCountry],
			[CreatedBy], [CreatedOn],MasterJobBranchID,MasterJobNo,MasterContainerKey,ContainerState,IsSOC)
	SELECT	@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @SealNo,@SealNo2, @ContainerGrade, @Temprature, @TemperatureMode, 
				@Vent, @VentMode, @GrossWeight, @CargoWeight,@VGM,@VGMType, @CargoType, @TrailerType, @CargoHandling,@Volume,@Remarks, 
				@ContainerRef, @DeclarationNo,@WeighingDate,@WeighingPlace,@CertificateNo,@VerificationCountry,@SOLASMethod,
				@RequiredDate,@OriginCountry,
				@CreatedBy, GETUTCDATE(),@MasterJobBranchID,@MasterJobNo,@MasterContainerKey,@ContainerState,@IsSOC
			 	
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderContainer] Record Into [OrderContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderContainerUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerUpdate] 
    @BranchID BIGINT,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @SealNo nvarchar(20),
    @SealNo2 nvarchar(20),
    @ContainerGrade smallint,
    @Temprature varchar(10),
    @TemperatureMode smallint,
    @Vent varchar(10),
    @VentMode smallint,
    @GrossWeight varchar(10),
    @CargoWeight varchar(10),
	@VGM decimal(18,7),
	@VGMType smallint,
    @CargoType smallint,
    @TrailerType smallint,
    @CargoHandling nvarchar(20),
	@Volume nvarchar(10),
	@Remarks nvarchar(100),
	@ContainerRef nvarchar(50),
	@DeclarationNo nvarchar(50),
	@WeighingDate datetime,
	@WeighingPlace nvarchar(50),
	@CertificateNo nvarchar(50),
	@VerificationCountry nvarchar(2),
	@SOLASMethod smallint,
	@RequiredDate datetime,
	@OriginCountry nvarchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo varchar(50),
	@MasterContainerKey varchar(50),
	@ContainerState smallint,
	@IsSOC bit
AS 
 
	
BEGIN

 

	UPDATE	[Operation].[OrderContainer]
	SET		[ContainerNo] = @ContainerNo, [Size] = @Size, [Type] = @Type, [SealNo] = @SealNo,[SealNo2] = @SealNo2, [ContainerGrade] = @ContainerGrade, 
			[Temprature] = @Temprature, [TemperatureMode] = @TemperatureMode, [Vent] = @Vent, [VentMode] = @VentMode, 
			[GrossWeight] = @GrossWeight, [CargoWeight] = @CargoWeight, VGM=@VGM, [VGMType]=@VGMType, [CargoType] = @CargoType, 
			[TrailerType] = @TrailerType, [CargoHandling]=@CargoHandling,[Volume]=@Volume,[Remarks]=@Remarks,
			[ContainerRef]=@ContainerRef, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),DeclarationNo= @DeclarationNo,
			[WeighingDate] =@WeighingDate,[WeighingPlace]=@WeighingPlace,[CertificateNo]=@CertificateNo,[VerificationCountry]=@VerificationCountry,
			[SOLASMethod]=@SOLASMethod,RequiredDate=@RequiredDate,OriginCountry=@OriginCountry,
			MasterJobBranchID=@MasterJobBranchID,MasterJobNo=@MasterJobNo,MasterContainerKey=@MasterContainerKey,
			ContainerState=@ContainerState,IsSOC = @IsSOC
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
	 
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderContainer] Record Into [OrderContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderContainerSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerSave] 
    @BranchID BIGINT,
    @OrderNo varchar(50),
    @ContainerKey varchar(50),
    @ContainerNo varchar(15),
    @Size varchar(2),
    @Type varchar(2),
    @SealNo nvarchar(20),
    @SealNo2 nvarchar(20),
    @ContainerGrade smallint,
    @Temprature varchar(10),
    @TemperatureMode smallint,
    @Vent varchar(10),
    @VentMode smallint,
    @GrossWeight varchar(10),
    @CargoWeight varchar(10),
	@VGM decimal(18,7),
    @VGMType smallint,
    @CargoType smallint,
    @TrailerType smallint,
    @CargoHandling nvarchar(20),
	@Volume nvarchar(10),
	@Remarks nvarchar(100),
	@ContainerRef nvarchar(50),
	@DeclarationNo nvarchar(50),
	@WeighingDate datetime,
	@WeighingPlace nvarchar(50),
	@CertificateNo nvarchar(50),
	@VerificationCountry nvarchar(2),
	@SOLASMethod smallint,
	@RequiredDate datetime,
	@OriginCountry nvarchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo varchar(50),
	@MasterContainerKey varchar(50),
	@ContainerState smallint,
	@IsSOC bit

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Operation].[OrderContainer] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Operation].[usp_OrderContainerUpdate] 
				@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @SealNo,@SealNo2, @ContainerGrade, @Temprature, @TemperatureMode, 
				@Vent, @VentMode, @GrossWeight, @CargoWeight,@VGM,@VGMType, @CargoType, @TrailerType, @CargoHandling,@Volume,@Remarks,
				@ContainerRef, @DeclarationNo,@WeighingDate,@WeighingPlace,@CertificateNo,@VerificationCountry,@SOLASMethod,
				@RequiredDate,@OriginCountry,@CreatedBy, @ModifiedBy,@MasterJobBranchID,@MasterJobNo,@MasterContainerKey,@ContainerState,@IsSOC


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderContainerInsert] 
				@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @SealNo,@SealNo2, @ContainerGrade, @Temprature, @TemperatureMode, 
				@Vent, @VentMode, @GrossWeight, @CargoWeight,@VGM,@VGMType, @CargoType, @TrailerType, @CargoHandling,@Volume,@Remarks,
				@ContainerRef, @DeclarationNo,@WeighingDate,@WeighingPlace,@CertificateNo,@VerificationCountry,@SOLASMethod,
				@RequiredDate,@OriginCountry,@CreatedBy, @ModifiedBy,@MasterJobBranchID,@MasterJobNo,@MasterContainerKey,@ContainerState,@IsSOC

	END
	 
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderContainerSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderContainer] Record  based on [OrderContainer]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderContainerDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerDelete] 
    @BranchID BIGINT,
    @OrderNo varchar(50),
    @ContainerKey varchar(50)
AS 

	
BEGIN 
	 
	DELETE
	FROM   [Operation].[OrderContainer]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
 
  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerDelete]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderContainerDeleteAll]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes All [OrderContainer] Record  based on [OrderContainer]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderContainerDeleteAll]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderContainerDeleteAll] 
END 
GO
CREATE PROC [Operation].[usp_OrderContainerDeleteAll] 
    @BranchID BIGINT,
    @OrderNo varchar(50) 
AS 

	
BEGIN
 
	 
	DELETE
	FROM   [Operation].[OrderContainer]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       
 
  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderContainerDeleteAll]
-- ========================================================================================================================================

GO


