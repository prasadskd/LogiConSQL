

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderTruckMovement] Record based on [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementSelect] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementSelect] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
    @ContainerKey NVARCHAR(50),
    @MovementCode nvarchar(50)
AS 

BEGIN

	;With StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType')
	SELECT [BranchID], [OrderNo], [ContainerKey],[MovementCode],  [MovementSeqNo], [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI], [EDIDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,0 As Selected,
			TransactionNo,MasterJobBranchID,MasterJobNo,MasterItemNo,ContractorCode,AssignedDate,
			ISNULL(MvmtSt.LookupDescription,'') As MovementStatusDescription
	FROM   [Operation].[OrderTruckMovement] OrdTM
	Left Outer Join StateTypeLookup MvmtSt ON 
		OrdTM.MovementStatus = MvmtSt.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
			AND [ContainerKey] = @ContainerKey  
			AND [MovementSeqNo] = @MovementCode 
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderTruckMovement] Records from [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementList] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementList] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50),
    @ContainerKey NVARCHAR(50)

AS 
BEGIN

	;With StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType')
	SELECT [BranchID], [OrderNo], [ContainerKey],[MovementCode],  [MovementSeqNo], [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI], [EDIDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,0 As Selected,
			TransactionNo,MasterJobBranchID,MasterJobNo,MasterItemNo,ContractorCode,AssignedDate,
			ISNULL(MvmtSt.LookupDescription,'') As MovementStatusDescription
	FROM   [Operation].[OrderTruckMovement] OrdTM
	Left Outer Join StateTypeLookup MvmtSt ON 
		OrdTM.MovementStatus = MvmtSt.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
			AND [ContainerKey] = @ContainerKey  

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementListByOrderNo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderTruckMovement] Records from [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementListByOrderNo]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementListByOrderNo] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementListByOrderNo] 
	@BranchID BIGINT,
    @OrderNo NVARCHAR(50) 

AS 
BEGIN

	;With StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType')
	SELECT [BranchID], [OrderNo], [ContainerKey],[MovementCode],  [MovementSeqNo], [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI], [EDIDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,0 As Selected,
			TransactionNo,MasterJobBranchID,MasterJobNo,MasterItemNo,ContractorCode,AssignedDate,
			ISNULL(MvmtSt.LookupDescription,'') As MovementStatusDescription
	FROM   [Operation].[OrderTruckMovement] OrdTM
	Left Outer Join StateTypeLookup MvmtSt ON 
		OrdTM.MovementStatus = MvmtSt.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
			 

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementListByOrderNo] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OrderTruckMovement] Records from [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementPageView] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementPageView] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50) ,
	@fetchrows bigint
AS 
BEGIN

	;With StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType')
	SELECT [BranchID], [OrderNo], [ContainerKey],[MovementCode],  [MovementSeqNo], [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI], [EDIDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,0 As Selected,
			TransactionNo,MasterJobBranchID,MasterJobNo,MasterItemNo,ContractorCode,AssignedDate,
			ISNULL(MvmtSt.LookupDescription,'') As MovementStatusDescription
	FROM   [Operation].[OrderTruckMovement] OrdTM
	Left Outer Join StateTypeLookup MvmtSt ON 
		OrdTM.MovementStatus = MvmtSt.LookupID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
	ORDER BY [ContainerKey]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementRecordCount] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementRecordCount] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50) 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Operation].[OrderTruckMovement]
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo 
	

END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderTruckMovement] Record based on [OrderTruckMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Operation].[usp_OrderTruckMovementAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementAutoCompleteSearch] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementAutoCompleteSearch] 
    @BranchID BIGINT,
    @OrderNo NVARCHAR(50),
    @ContainerKey NVARCHAR(50) 
AS 

BEGIN

	;With StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType')
	SELECT [BranchID], [OrderNo], [ContainerKey],[MovementCode],  [MovementSeqNo], [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI], [EDIDateTime], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,0 As Selected,
			TransactionNo,MasterJobBranchID,MasterJobNo,MasterItemNo,ContractorCode,
			ISNULL(MvmtSt.LookupDescription,'') As MovementStatusDescription
	FROM   [Operation].[OrderTruckMovement] OrdTM
	Left Outer Join StateTypeLookup MvmtSt ON 
		OrdTM.MovementStatus = MvmtSt.LookupID
	WHERE  [BranchID] = @BranchID  
	       AND [OrderNo] LIKE '%' + @OrderNo  + '%'
		   AND [ContainerKey]  LIKE '%' +  @ContainerKey   + '%'
END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OrderTruckMovement] Record Into [OrderTruckMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTruckMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementInsert] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementInsert] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @MovementSeqNo smallint,
    @FromLocationCode nvarchar(10),
    @FromLocationAddressID int,
    @ToLocationCode nvarchar(10),
    @ToLocationAddressID int,
    @RequiredDate datetime,
	@PlannedBy nvarchar(60),
    @PlannedDate datetime,
    @StartDate datetime,
    @EndDate datetime,
    @TruckID nvarchar(20),
    @TrailerID nvarchar(20),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @Remarks nvarchar(50),
    @MovementStatus smallint,
    @Size nvarchar(2),
    @Type nvarchar(2),
    @IsDropOff bit,
    @IsRequireEDI bit,
	@TransactionNo nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo nvarchar(50),
	@MasterItemNo nvarchar(50),
	@ContractorCode bigint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Operation].[OrderTruckMovement] (
			[BranchID], [OrderNo], [ContainerKey],[MovementCode], [MovementSeqNo],  [FromLocationCode], [FromLocationAddressID], [ToLocationCode], 
			[ToLocationAddressID], [RequiredDate],[PlannedBy], [PlannedDate], [StartDate], [EndDate], [TruckID], [TrailerID], [DriverID1], [DriverID2], 
			[Remarks], [MovementStatus], [Size], [Type], [IsDropOff], [IsRequireEDI],[TransactionNo],[MasterJobBranchID],
			[MasterJobNo],[MasterItemNo],[ContractorCode],[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey,@MovementCode, @MovementSeqNo,  @FromLocationCode, @FromLocationAddressID, @ToLocationCode, 
			@ToLocationAddressID, @RequiredDate,@PlannedBy, @PlannedDate, @StartDate, @EndDate, @TruckID, @TrailerID, @DriverID1, @DriverID2, 
			@Remarks, @MovementStatus, @Size, @Type, @IsDropOff, @IsRequireEDI,@TransactionNo,@MasterJobBranchID,@MasterJobNo,@MasterItemNo,
			@ContractorCode,@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OrderTruckMovement] Record Into [OrderTruckMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTruckMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementUpdate] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementUpdate] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @MovementSeqNo smallint,
    @FromLocationCode nvarchar(10),
    @FromLocationAddressID int,
    @ToLocationCode nvarchar(10),
    @ToLocationAddressID int,
    @RequiredDate datetime,
	@PlannedBy nvarchar(60),
    @PlannedDate datetime,
    @StartDate datetime,
    @EndDate datetime,
    @TruckID nvarchar(20),
    @TrailerID nvarchar(20),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @Remarks nvarchar(50),
    @MovementStatus smallint,
    @Size nvarchar(2),
    @Type nvarchar(2),
    @IsDropOff bit,
    @IsRequireEDI bit,
	@TransactionNo nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo nvarchar(50),
	@MasterItemNo nvarchar(50),
	@ContractorCode bigint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE [Operation].[OrderTruckMovement]
	SET    [MovementCode] = @MovementCode, [FromLocationCode] = @FromLocationCode, [FromLocationAddressID] = @FromLocationAddressID, 
			[ToLocationCode] = @ToLocationCode, [ToLocationAddressID] = @ToLocationAddressID, [RequiredDate] = @RequiredDate, PlannedBy = @PlannedBy,
			[PlannedDate] = @PlannedDate, [StartDate] = @StartDate, [EndDate] = @EndDate, [TruckID] = @TruckID, [TrailerID] = @TrailerID, 
			[DriverID1] = @DriverID1, [DriverID2] = @DriverID2, [Remarks] = @Remarks, [MovementStatus] = @MovementStatus, 
			[Size] = @Size, [Type] = @Type, [IsDropOff] = @IsDropOff, [IsRequireEDI] = @IsRequireEDI, 
			TransactionNo = @TransactionNo,MasterJobBranchID = @MasterJobBranchID,MasterJobNo = @MasterJobNo,
			MasterItemNo = @MasterItemNo,ContractorCode =@ContractorCode,[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()

	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode
	
END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OrderTruckMovement] Record Into [OrderTruckMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTruckMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementSave] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementSave] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @MovementSeqNo smallint,
    @FromLocationCode nvarchar(10),
    @FromLocationAddressID int,
    @ToLocationCode nvarchar(10),
    @ToLocationAddressID int,
    @RequiredDate datetime,
	@PlannedBy nvarchar(60),
    @PlannedDate datetime,
    @StartDate datetime,
    @EndDate datetime,
    @TruckID nvarchar(20),
    @TrailerID nvarchar(20),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @Remarks nvarchar(50),
    @MovementStatus smallint,
    @Size nvarchar(2),
    @Type nvarchar(2),
    @IsDropOff bit,
    @IsRequireEDI bit,
	@TransactionNo nvarchar(50),
	@MasterJobBranchID bigint,
	@MasterJobNo nvarchar(50),
	@MasterItemNo nvarchar(50),
	@ContractorCode bigint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Operation].[OrderTruckMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode)>0
	BEGIN
	    Exec [Operation].[usp_OrderTruckMovementUpdate] 
				@BranchID, @OrderNo, @ContainerKey,@MovementCode, @MovementSeqNo,  @FromLocationCode, @FromLocationAddressID, @ToLocationCode, 
				@ToLocationAddressID, @RequiredDate,@PlannedBy, @PlannedDate, @StartDate, @EndDate, @TruckID, @TrailerID, @DriverID1, @DriverID2, 
				@Remarks, @MovementStatus, @Size, @Type, @IsDropOff, @IsRequireEDI, 
				@TransactionNo,@MasterJobBranchID,@MasterJobNo,@MasterItemNo,@ContractorCode,@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Operation].[usp_OrderTruckMovementInsert] 
				@BranchID, @OrderNo, @ContainerKey,@MovementCode, @MovementSeqNo,  @FromLocationCode, @FromLocationAddressID, @ToLocationCode, 
				@ToLocationAddressID, @RequiredDate,@PlannedBy, @PlannedDate, @StartDate, @EndDate, @TruckID, @TrailerID, @DriverID1, @DriverID2, 
				@Remarks, @MovementStatus, @Size, @Type, @IsDropOff, @IsRequireEDI, 
				@TransactionNo,@MasterJobBranchID,@MasterJobNo,@MasterItemNo,@ContractorCode,@CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Operation].usp_[OrderTruckMovementSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderTruckMovement] Record  based on [OrderTruckMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTruckMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementDelete] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementDelete] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50)
AS 

	
BEGIN

	Declare @MovementStatus smallint=0;

	Select @MovementStatus = LookupID From Config.Lookup 
	Where LookupCategory='StateType' And LookupCode='CANCELLED'
	 
	Update [Operation].[OrderTruckMovement]
	Set MovementStatus = @MovementStatus
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementDelete]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Operation].[usp_OrderTruckMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OrderTruckMovement] Record  based on [OrderTruckMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Operation].[usp_OrderTruckMovementDeleteAll]') IS NOT NULL
BEGIN 
    DROP PROC [Operation].[usp_OrderTruckMovementDeleteAll] 
END 
GO
CREATE PROC [Operation].[usp_OrderTruckMovementDeleteAll] 
    @BranchID BIGINT,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50) 
AS 

	
BEGIN

	DELETE [Operation].[OrderTruckMovement]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	
	 


END

-- ========================================================================================================================================
-- END  											 [Operation].[usp_OrderTruckMovementDeleteAll]
-- ========================================================================================================================================

GO

  