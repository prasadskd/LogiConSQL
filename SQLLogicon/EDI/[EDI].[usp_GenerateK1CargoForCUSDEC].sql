IF OBJECT_ID('[EDI].[usp_GenerateK1CargoForCUSDEC]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GenerateK1CargoForCUSDEC] 
END 
GO
CREATE PROC [EDI].[usp_GenerateK1CargoForCUSDEC] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN
 Select DecItem.DeclaredUOM, OrdCar.GrossWeight, DecItem.DeclaredUOM, DecCon.ContainerNo, DecCon.EQDStatus, '' as SMKRefNo, DecShip.OceanBLNo as MasterBLNo, DecShip.HouseBLNo, OrdCar.PackageType, OrdCar.ItemDescription,'' SMKDeclarationRefNo,
 DecShip.VesselID, DecShip.VesselName, DecShip.VoyageNo, DecInv.InvoiceNo, DecInv.InvoiceDate, DecHdr.ImportDate, OrdHdr.FwdAgent, Mer.MerchantName as AgentName, DecHdr.DeclarantDesignation, DecHdr.DeclarantName, DecHdr.DeclarantNRIC, DecHdr.Importer, Mer2.MerchantName as ImporterName,
 '' as ConsignmentNoteIdentifier, DecItem.ItemNo, DecItem.ItemDescription1, DecItem.OriginCountryCode, DecItem.DeclaredQty, DecItem.StatisticalQty, DecInv.FOBAmount, DecInv.InsuranceAmount, DecInv.LocalCurrencyCode, DecItem.ImportDutyAmount
 From 
 Operation.DeclarationItem as DecItem
 Left Outer Join Operation.DeclarationContainer as DecCon
	On DecCon.DeclarationNo = DecItem.DeclarationNo
 Left Outer Join Operation.DeclarationShipment as DecShip
	On DecShip.DeclarationNo = DecCon.DeclarationNo
 Left Outer Join Operation.OrderCargo as OrdCar
	On DecCon.OrderNo = OrdCar.OrderNo
 Left Outer Join Operation.DeclarationInvoice as DecInv
	On DecCon.DeclarationNo = DecInv.DeclarationNo
 Left Outer Join Operation.DeclarationHeader as DecHdr
	On DecCon.DeclarationNo = DecHdr.DeclarationNo
 Left Outer Join Operation.OrderHeader as OrdHdr
	On DecCon.OrderNo = OrdHdr.OrderNo
 Left Outer Join Master.Merchant as Mer
	On OrdHdr.FwdAgent = Mer.MerchantCode
 Left Outer Join Master.Merchant as Mer2
	On DecHdr.Importer = Mer2.MerchantCode
 Where DecItem.BranchID = @BranchID
	And Decitem.DeclarationNo = @DeclarationNo
END