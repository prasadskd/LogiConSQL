
-- ========================================================================================================================================
-- START											 [Operation].[usp_GenerateCodeco]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the Pending Movements for CODECO
/*

 Exec [EDI].[usp_GenerateCodecoForPort]  '2017-02-10','2017-02-25', 0,'IN'
 
 exec [EDI].[usp_GenerateCodecoForPort] @DateFrom='2017-02-24 21:02:41.563',@DateTo='2017-02-24 21:02:41.563',@IsCurrentDate=1,@EdiType=N'IN'

*/

-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_GenerateCodecoForPort]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GenerateCodecoForPort]
END 
GO
CREATE PROC [EDI].[usp_GenerateCodecoForPort]
    @DateFrom datetime =null,
    @DateTo datetime = null,
    @IsCurrentDate bit = 0,
    @EdiType varchar(10)
    
AS 
 
BEGIN



If @IsCurrentDate=0 
Begin

;WITH ContainerGradeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerGrade'),
	ContainerHeightDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerHeight'),
	TempratureModeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='TempratureMode'),
	VentModeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='VentMode'),
	MaterialDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerMaterial'),
	MovementIndicatorDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='MovementIndicator'),
	EFIndicatorDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='E/FIndicator'),
	BookingTypeDescription As
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='JobType') 

Select CMv.DocumentNo,Cmv.OrderNo,CMv.ContainerKey,CMv.ContainerNo,CMv.Size,CMv.Type,CMv.EFIndicator,CMv.MovementType,cmv.MovementCode,
		Convert(varchar(12),CONVERT(VARCHAR(8),CMv.MovementDate,112) + REPLACE(CONVERT(VARCHAR(8),CMv.MovementDate,108),':','')) As MovementDate ,
		'TRUCK' As MovementBy,TkHd.TruckNo,CMv.AgentCode,CMv.CustomerCode,cmv.ContainerStatus,
		BkHd.VesselID,Convert(Varchar(35),LTRIM(RTRIM(ISNULL(Vsl.VesselName,'')))) As VesselName,TkHd.HaulierCode,CMv.Status,BkHd.VoyageNo,BkHd.BookingNo,
		BkHd.BLNo, CMv.SealNo1,
		CMv.SealNo2,  CMv.Remarks,BkHd.LoadingPort,BkHd.DischargePort,BkHd.DestinationPort,bkdt.IMOCode,BkDt.UNNo,
		BkDt.Remarks, CMv.Temperature ,CMv.TemperatureType,0 As MasWeight, 
		ISNULL(Cmv.MGW,0) As VGM, 
		ISNULL(CGD.LookupCode,'') As ContainerGradeDescription,'' As ContainerStatusDescription,
		ISNULL(TMD.LookupCode,'') As TempratureModeDescription,
		ISNULl(CHD.LookupCode,'') As ContainerHeightDescription,
		ISNULL(VMD.LookupCode,'') As VentModeDescription,
		ISNULL(MD.LookupCode,'') As MaterialDescription,
		ISNULL(MID.LookupCode,'') As MovementIndicatorDescription,
		ISNULL(EFD.LookupCode,'') As EFIndicatorDescription,
		ISNULL(Agnt.MerchantName,'') As AgentName,
		ISNULL(Cus.MerchantName,'') As CustomerName,
		ISNULL(BKT.LookupCode,'') As BookingTypeDescription,
		ISNULL(Cmv.Size,'') + ISNULL(Cmv.Type,'') As TypeSize,
		convert(varchar(14),CONVERT(VARCHAR(10),CMv.MovementDate,112) + REPLACE(CONVERT(VARCHAR(10),CMv.MovementDate,108),':','')) As MovementDateTime
From	Port.ContainerMovement CMv WITH (NOLOCK) 
		LEFT OUTER JOIN Port.BookingContainer BkDt WITH (NOLOCK) ON
			CMv.OrderNo = BkDt.OrderNo
			And CMv.ContainerKey = bkdt.ContainerKey
			And CMv.ContainerNo = BkDt.ContainerNo
		LEFT OUTER JOIN Port.TruckMovementDetail TkDt WITH (NOLOCK) ON
			CMv.TruckTransactionNo = TkDt.TransactionNo
		LEFT OUTER JOIN Port.TruckMovementHeader TkHd WITH (NOLOCK) ON
			TkDt.TransactionNo = TkHd.TransactionNo 
		LEFT OUTER JOIN Port.BookingHeader BkHd WITH (NOLOCK) ON
			BkDt.OrderNo = BkHd.OrderNo
		LEFT OUTER JOIN Master.Vessel Vsl WITH (NOLOCK) ON
			Vsl.VesselID = BkHd.VesselID
		LEFT OUTER JOIN ContainerGradeDescription CGD ON
				CMv.ContainerGrade = CGD.LookupID
		LEFT OUTER JOIN TempratureModeDescription TMD ON
				CMv.TemperatureType = TMD.LookupID
		LEFT OUTER JOIN ContainerHeightDescription CHD ON
				CMv.Height = CHD.LookupID
		LEFT OUTER JOIN VentModeDescription VMD ON
				CMv.VentType = VMD.LookupID
		LEFT OUTER JOIN MaterialDescription MD ON
				CMv.Material = MD.LookupID
		LEFT OUTER JOIN MovementIndicatorDescription MID ON
				CMv.MovementType = MID.LookupID
		LEFT OUTER JOIN EFIndicatorDescription EFD ON
				CMv.EFIndicator = EFD.LookupID
		LEFT OUTER JOIN [Master].[Merchant] Agnt ON
				CMv.AgentCode = Agnt.MerchantCode
		LEFT OUTER JOIN [Master].[Merchant] Cus ON
				CMv.CustomerCode = Cus.MerchantCode		
		LEFT OUTER JOIN BookingTypeDescription BKT ON 
				BkHd.BookingType = BKT.LookupID	
		Where Cmv.Status = CAST(1 as bit)
				And CONVERT(Char(10),Cmv.MovementDate,120) >= CONVERT(Char(10),@DateFrom,120) 
				And CONVERT(Char(10),Cmv.MovementDate,120)< = CONVERT(Char(10),@DateTo,120) 
				And ISNULL(MID.LookupCode,'') =
				Case When @EdiType = 'IN' Then 'IN'
				Else 'OUT'
				End			 

									
End

Else
Begin

;WITH ContainerGradeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerGrade'),
	ContainerHeightDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerHeight'),
	TempratureModeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='TempratureMode'),
	VentModeDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='VentMode'),
	MaterialDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='ContainerMaterial'),
	MovementIndicatorDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='MovementIndicator'),
	EFIndicatorDescription AS
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='E/FIndicator'),
	BookingTypeDescription As
	(	SELECT LookupID,LookUpCode FROM [Config].[Lookup]
		WHERE LookupCategory='JobType') 

Select CMv.DocumentNo,Cmv.OrderNo,CMv.ContainerKey,CMv.ContainerNo,CMv.Size,CMv.Type,CMv.EFIndicator,CMv.MovementType,cmv.MovementCode,
		Convert(varchar(12),CONVERT(VARCHAR(8),CMv.MovementDate,112) + REPLACE(CONVERT(VARCHAR(8),CMv.MovementDate,108),':','')) As MovementDate ,
		'TRUCK' As MovementBy,TkHd.TruckNo,CMv.AgentCode,CMv.CustomerCode,cmv.ContainerStatus,
		BkHd.VesselID,Convert(Varchar(35),LTRIM(RTRIM(ISNULL(Vsl.VesselName,'')))) As VesselName,TkHd.HaulierCode,CMv.Status,BkHd.VoyageNo,BkHd.BookingNo,
		BkHd.BLNo, CMv.SealNo1,
		CMv.SealNo2,  CMv.Remarks,BkHd.LoadingPort,BkHd.DischargePort,BkHd.DestinationPort,bkdt.IMOCode,BkDt.UNNo,
		BkDt.Remarks, CMv.Temperature ,CMv.TemperatureType,0 As MasWeight, 
		ISNULL(Cmv.MGW,0) As VGM, 
		ISNULL(CGD.LookupCode,'') As ContainerGradeDescription,'' As ContainerStatusDescription,
		ISNULL(TMD.LookupCode,'') As TempratureModeDescription,
		ISNULl(CHD.LookupCode,'') As ContainerHeightDescription,
		ISNULL(VMD.LookupCode,'') As VentModeDescription,
		ISNULL(MD.LookupCode,'') As MaterialDescription,
		ISNULL(MID.LookupCode,'') As MovementIndicatorDescription,
		ISNULL(EFD.LookupCode,'') As EFIndicatorDescription,
		ISNULL(Agnt.MerchantName,'') As AgentName,
		ISNULL(Cus.MerchantName,'') As CustomerName,
		ISNULL(BKT.LookupCode,'') As BookingTypeDescription,
		
		ISNULL(Cmv.Size,'') + ISNULL(Cmv.Type,'') As TypeSize,
		convert(varchar(14),CONVERT(VARCHAR(10),CMv.MovementDate,112) + REPLACE(CONVERT(VARCHAR(10),CMv.MovementDate,108),':','')) As MovementDateTime
From	Port.ContainerMovement CMv WITH (NOLOCK) 
		LEFT OUTER JOIN Port.BookingContainer BkDt WITH (NOLOCK) ON
			CMv.OrderNo = BkDt.OrderNo
			And CMv.ContainerKey = bkdt.ContainerKey
			And CMv.ContainerNo = BkDt.ContainerNo
		LEFT OUTER JOIN Port.TruckMovementDetail TkDt WITH (NOLOCK) ON
			CMv.TruckTransactionNo = TkDt.TransactionNo
		LEFT OUTER JOIN Port.TruckMovementHeader TkHd WITH (NOLOCK) ON
			TkDt.TransactionNo = TkHd.TransactionNo 
		LEFT OUTER JOIN Port.BookingHeader BkHd WITH (NOLOCK) ON
			BkDt.OrderNo = BkHd.OrderNo
		LEFT OUTER JOIN Master.Vessel Vsl WITH (NOLOCK) ON
			Vsl.VesselID = BkHd.VesselID
		LEFT OUTER JOIN ContainerGradeDescription CGD ON
				CMv.ContainerGrade = CGD.LookupID
		LEFT OUTER JOIN TempratureModeDescription TMD ON
				CMv.TemperatureType = TMD.LookupID
		LEFT OUTER JOIN ContainerHeightDescription CHD ON
				CMv.Height = CHD.LookupID
		LEFT OUTER JOIN VentModeDescription VMD ON
				CMv.VentType = VMD.LookupID
		LEFT OUTER JOIN MaterialDescription MD ON
				CMv.Material = MD.LookupID
		LEFT OUTER JOIN MovementIndicatorDescription MID ON
				CMv.MovementType = MID.LookupID
		LEFT OUTER JOIN EFIndicatorDescription EFD ON
				CMv.EFIndicator = EFD.LookupID
		LEFT OUTER JOIN [Master].[Merchant] Agnt ON
				CMv.AgentCode = Agnt.MerchantCode
		LEFT OUTER JOIN [Master].[Merchant] Cus ON
				CMv.CustomerCode = Cus.MerchantCode	
		LEFT OUTER JOIN BookingTypeDescription BKT ON 
				BkHd.BookingType = BKT.LookupID	
		Where Cmv.Status = CAST(1 as bit)
			 And Cmv.EdiDateTime IS NULL
			 And ISNULL(MID.LookupCode,'') =
				Case When @EdiType = 'IN' Then 'IN'
				Else 'OUT'
				End			 
				
End									
			
END
			 
		
