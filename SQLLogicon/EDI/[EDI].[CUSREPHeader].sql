
-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CUSREPHeader] Record based on [CUSREPHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderSelect] 
    @BranchID BIGINT,
    @ReferenceNo BIGINT
AS 

BEGIN

	SELECT	[BranchID], [ReferenceNo], [FileName], [ReferenceText], [DocumentType], [MessageType], [SenderCode], [ReceiverCode], [PreparationDateTime], 
			[POP], [POT], [PlaceOfArrival], [ETA], [ETD], [PlaceOfRegistration], [DischargePort], [DestinationPort], [AgentCode], [AgentContact], [AgentTel], 
			[AgentFax], [PSACode], [PSAName], [PSAAddress], [VesselName], [VesselCode], [VoyageNo], [BerthNumber], [ShipCallNumber], [GrossRegisteredTonnage], 
			[NettRegisteredTonnage], [OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [ShipClass], [FlagShip], [YearBuilt], [CountryCode], 
			[LoadingRemarks], [DischargeRemarks], [ChangeRemarks], [CreatedBy], [CreatedOn], [IsProcessed], [Remarks] 
	FROM	[EDI].[CUSREPHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [ReferenceNo] = @ReferenceNo  
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CUSREPHeader] Records from [CUSREPHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderList] 
    @BranchID BIGINT

AS 
BEGIN

	SELECT	[BranchID], [ReferenceNo], [FileName], [ReferenceText], [DocumentType], [MessageType], [SenderCode], [ReceiverCode], [PreparationDateTime], 
			[POP], [POT], [PlaceOfArrival], [ETA], [ETD], [PlaceOfRegistration], [DischargePort], [DestinationPort], [AgentCode], [AgentContact], [AgentTel], 
			[AgentFax], [PSACode], [PSAName], [PSAAddress], [VesselName], [VesselCode], [VoyageNo], [BerthNumber], [ShipCallNumber], [GrossRegisteredTonnage], 
			[NettRegisteredTonnage], [OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [ShipClass], [FlagShip], [YearBuilt], [CountryCode], 
			[LoadingRemarks], [DischargeRemarks], [ChangeRemarks], [CreatedBy], [CreatedOn], [IsProcessed], [Remarks] 
	FROM	[EDI].[CUSREPHeader]
	WHERE  [BranchID] = @BranchID  


END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CUSREPHeader] Records from [CUSREPHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderPageView] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderPageView] 
    @BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	SELECT	[BranchID], [ReferenceNo], [FileName], [ReferenceText], [DocumentType], [MessageType], [SenderCode], [ReceiverCode], [PreparationDateTime], 
			[POP], [POT], [PlaceOfArrival], [ETA], [ETD], [PlaceOfRegistration], [DischargePort], [DestinationPort], [AgentCode], [AgentContact], [AgentTel], 
			[AgentFax], [PSACode], [PSAName], [PSAAddress], [VesselName], [VesselCode], [VoyageNo], [BerthNumber], [ShipCallNumber], [GrossRegisteredTonnage], 
			[NettRegisteredTonnage], [OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [ShipClass], [FlagShip], [YearBuilt], [CountryCode], 
			[LoadingRemarks], [DischargeRemarks], [ChangeRemarks], [CreatedBy], [CreatedOn], [IsProcessed], [Remarks] 
	FROM	[EDI].[CUSREPHeader]
	WHERE  [BranchID] = @BranchID  
	ORDER BY [ReferenceNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CUSREPHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderRecordCount] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderRecordCount] 
    @BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [EDI].[CUSREPHeader]
	WHERE  [BranchID] = @BranchID  
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CUSREPHeader] Record based on [CUSREPHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderAutoCompleteSearch] 
    @BranchID BIGINT,
    @FileName nvarchar(10)
AS 

BEGIN

	SELECT	[BranchID], [ReferenceNo], [FileName], [ReferenceText], [DocumentType], [MessageType], [SenderCode], [ReceiverCode], [PreparationDateTime], 
			[POP], [POT], [PlaceOfArrival], [ETA], [ETD], [PlaceOfRegistration], [DischargePort], [DestinationPort], [AgentCode], [AgentContact], [AgentTel], 
			[AgentFax], [PSACode], [PSAName], [PSAAddress], [VesselName], [VesselCode], [VoyageNo], [BerthNumber], [ShipCallNumber], [GrossRegisteredTonnage], 
			[NettRegisteredTonnage], [OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [ShipClass], [FlagShip], [YearBuilt], [CountryCode], 
			[LoadingRemarks], [DischargeRemarks], [ChangeRemarks], [CreatedBy], [CreatedOn], [IsProcessed], [Remarks] 
	FROM   [EDI].[CUSREPHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [FileName] Like '%' +  @FileName  + '%'
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CUSREPHeader] Record Into [CUSREPHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSREPHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderInsert] 
	@BranchID bigint,
    @ReferenceNo bigint,
    @FileName nvarchar(MAX),
    @ReferenceText nvarchar(50) = NULL,
    @DocumentType smallint,
    @MessageType tinyint,
    @SenderCode nvarchar(50) = NULL,
    @ReceiverCode nvarchar(50) = NULL,
    @PreparationDateTime datetime = NULL,
    @POP nvarchar(50) = NULL,
    @POT nvarchar(50) = NULL,
    @PlaceOfArrival nvarchar(50) = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PlaceOfRegistration nvarchar(50) = NULL,
    @DischargePort nvarchar(50) = NULL,
    @DestinationPort nvarchar(50) = NULL,
    @AgentCode nvarchar(50) = NULL,
    @AgentContact nvarchar(100) = NULL,
    @AgentTel nvarchar(50) = NULL,
    @AgentFax nvarchar(50) = NULL,
    @PSACode nvarchar(50) = NULL,
    @PSAName nvarchar(100) = NULL,
    @PSAAddress nvarchar(255) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VesselCode nvarchar(50) = NULL,
    @VoyageNo nvarchar(50) = NULL,
    @BerthNumber nvarchar(50) = NULL,
    @ShipCallNumber nvarchar(50) = NULL,
    @GrossRegisteredTonnage decimal(18, 0) = NULL,
    @NettRegisteredTonnage decimal(18, 0) = NULL,
    @OverallLength decimal(18, 0) = NULL,
    @DeadWeight decimal(18, 0) = NULL,
    @StandardDraft decimal(18, 0) = NULL,
    @VesselCapacity decimal(18, 0) = NULL,
    @ShipClass nvarchar(50) = NULL,
    @FlagShip nvarchar(3) = NULL,
    @YearBuilt smallint = NULL,
    @CountryCode nvarchar(50) = NULL,
    @LoadingRemarks nvarchar(50) = NULL,
    @DischargeRemarks nvarchar(50) = NULL,
    @ChangeRemarks nvarchar(50) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @IsProcessed bit,
    @Remarks nvarchar(MAX) = NULL,
	@ATA datetime=null,
	@ATD datetime=null
AS 
  

BEGIN

	
	INSERT INTO [EDI].[CUSREPHeader] (
			[BranchID], [ReferenceNo], [FileName], [ReferenceText], [DocumentType], [MessageType], [SenderCode], [ReceiverCode], [PreparationDateTime], 
			[POP], [POT], [PlaceOfArrival], [ETA], [ETD], [PlaceOfRegistration], [DischargePort], [DestinationPort], [AgentCode], [AgentContact], [AgentTel], [AgentFax], 
			[PSACode], [PSAName], [PSAAddress], [VesselName], [VesselCode], [VoyageNo], [BerthNumber], [ShipCallNumber], [GrossRegisteredTonnage], [NettRegisteredTonnage], 
			[OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [ShipClass], [FlagShip], [YearBuilt], [CountryCode], [LoadingRemarks], [DischargeRemarks], 
			[ChangeRemarks], [CreatedBy], [CreatedOn], [IsProcessed], [Remarks],[ATA],[ATD])
	SELECT	@BranchID, @ReferenceNo, @FileName, @ReferenceText, @DocumentType, @MessageType, @SenderCode, @ReceiverCode, @PreparationDateTime, 
			@POP, @POT, @PlaceOfArrival, @ETA, @ETD, @PlaceOfRegistration, @DischargePort, @DestinationPort, @AgentCode, @AgentContact, @AgentTel, @AgentFax, 
			@PSACode, @PSAName, @PSAAddress, @VesselName, @VesselCode, @VoyageNo, @BerthNumber, @ShipCallNumber, @GrossRegisteredTonnage, @NettRegisteredTonnage, 
			@OverallLength, @DeadWeight, @StandardDraft, @VesselCapacity, @ShipClass, @FlagShip, @YearBuilt, @CountryCode, @LoadingRemarks, @DischargeRemarks, 
			@ChangeRemarks, @CreatedBy, GETUTCDATE(), @IsProcessed, @Remarks,@ATA,@ATD

               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CUSREPHeader] Record Into [CUSREPHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSREPHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderUpdate] 
	@BranchID bigint,
    @FileName nvarchar(MAX),
    @ReferenceText nvarchar(50) = NULL,
    @DocumentType smallint,
    @MessageType tinyint,
    @SenderCode nvarchar(50) = NULL,
    @ReceiverCode nvarchar(50) = NULL,
    @PreparationDateTime datetime = NULL,
    @POP nvarchar(50) = NULL,
    @POT nvarchar(50) = NULL,
    @PlaceOfArrival nvarchar(50) = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PlaceOfRegistration nvarchar(50) = NULL,
    @DischargePort nvarchar(50) = NULL,
    @DestinationPort nvarchar(50) = NULL,
    @AgentCode nvarchar(50) = NULL,
    @AgentContact nvarchar(100) = NULL,
    @AgentTel nvarchar(50) = NULL,
    @AgentFax nvarchar(50) = NULL,
    @PSACode nvarchar(50) = NULL,
    @PSAName nvarchar(100) = NULL,
    @PSAAddress nvarchar(255) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VesselCode nvarchar(50) = NULL,
    @VoyageNo nvarchar(50) = NULL,
    @BerthNumber nvarchar(50) = NULL,
    @ShipCallNumber nvarchar(50) = NULL,
    @GrossRegisteredTonnage decimal(18, 0) = NULL,
    @NettRegisteredTonnage decimal(18, 0) = NULL,
    @OverallLength decimal(18, 0) = NULL,
    @DeadWeight decimal(18, 0) = NULL,
    @StandardDraft decimal(18, 0) = NULL,
    @VesselCapacity decimal(18, 0) = NULL,
    @ShipClass nvarchar(50) = NULL,
    @FlagShip nvarchar(3) = NULL,
    @YearBuilt smallint = NULL,
    @CountryCode nvarchar(50) = NULL,
    @LoadingRemarks nvarchar(50) = NULL,
    @DischargeRemarks nvarchar(50) = NULL,
    @ChangeRemarks nvarchar(50) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @IsProcessed bit,
    @Remarks nvarchar(MAX) = NULL,
	@ATA datetime=null,
	@ATD datetime=null
AS 
 
	
BEGIN

	UPDATE	[EDI].[CUSREPHeader]
	SET		[ReferenceText] = @ReferenceText, [DocumentType] = @DocumentType, [MessageType] = @MessageType, [SenderCode] = @SenderCode, [ReceiverCode] = @ReceiverCode, 
			[PreparationDateTime] = @PreparationDateTime, [POP] = @POP, [POT] = @POT, [PlaceOfArrival] = @PlaceOfArrival, [ETA] = @ETA, [ETD] = @ETD, 
			[PlaceOfRegistration] = @PlaceOfRegistration, [DischargePort] = @DischargePort, [DestinationPort] = @DestinationPort, [AgentCode] = @AgentCode, 
			[AgentContact] = @AgentContact, [AgentTel] = @AgentTel, [AgentFax] = @AgentFax, [PSACode] = @PSACode, [PSAName] = @PSAName, 
			[PSAAddress] = @PSAAddress, [VesselName] = @VesselName, [VesselCode] = @VesselCode, [VoyageNo] = @VoyageNo, [BerthNumber] = @BerthNumber, 
			[ShipCallNumber] = @ShipCallNumber, [GrossRegisteredTonnage] = @GrossRegisteredTonnage, [NettRegisteredTonnage] = @NettRegisteredTonnage, 
			[OverallLength] = @OverallLength, [DeadWeight] = @DeadWeight, [StandardDraft] = @StandardDraft, [VesselCapacity] = @VesselCapacity, 
			[ShipClass] = @ShipClass, [FlagShip] = @FlagShip, [YearBuilt] = @YearBuilt, [CountryCode] = @CountryCode, [LoadingRemarks] = @LoadingRemarks, 
			[DischargeRemarks] = @DischargeRemarks, [ChangeRemarks] = @ChangeRemarks, [Remarks] = @Remarks,[ATA]=@ATA,[ATD]=@ATD
	WHERE	[BranchID] = @BranchID
			AND [FileName] = @FileName
	
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CUSREPHeader] Record Into [CUSREPHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSREPHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderSave]
    @BranchID bigint,
	@ReferenceNo bigint,
    @FileName nvarchar(MAX),
	@ReferenceText nvarchar(50),
    @DocumentType smallint,
    @MessageType tinyint,
    @SenderCode nvarchar(50),
    @ReceiverCode nvarchar(50),
    @PreparationDateTime datetime,
    @POP nvarchar(50),
    @POT nvarchar(50),
    @PlaceOfArrival nvarchar(50),
    @ETA datetime,
    @ETD datetime,
    @PlaceOfRegistration nvarchar(50),
    @DischargePort nvarchar(50),
    @DestinationPort nvarchar(50),
    @AgentCode nvarchar(50),
    @AgentContact nvarchar(100),
    @AgentTel nvarchar(50),
    @AgentFax nvarchar(50),
    @PSACode nvarchar(50),
    @PSAName nvarchar(100),
    @PSAAddress nvarchar(255),
    @VesselName nvarchar(100),
    @VesselCode nvarchar(50),
    @VoyageNo nvarchar(50),
	@BerthNumber nvarchar(50),
	@ShipCallNumber nvarchar(50),
	@GrossRegisteredTonnage decimal(18,0),
	@NettRegisteredTonnage decimal(18,0),
	@OverallLength decimal(18,0),
	@DeadWeight decimal(18,0),
	@StandardDraft decimal(18,0),
	@VesselCapacity decimal(18,0),
	@ShipClass nvarchar(50),
	@FlagShip nvarchar(50),
	@YearBuilt smallint,
	@CountryCode nvarchar(50),
	@LoadingRemarks nvarchar(50),
	@DischargeRemarks nvarchar(50),
	@ChangeRemarks nvarchar(50),
    @CreatedBy nvarchar(60),
    @IsProcessed bit,
    @Remarks nvarchar(MAX),
	@ATA datetime=null,
	@ATD datetime=null

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [EDI].[CUSREPHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSREPHeaderUpdate] 
			@BranchID,@ReferenceNo, @FileName,@ReferenceText, @DocumentType, @MessageType, @SenderCode, @ReceiverCode, @PreparationDateTime, @POP, @POT, 
			@PlaceOfArrival, @ETA, @ETD, @PlaceOfRegistration, @DischargePort, @DestinationPort, @AgentCode, @AgentContact, @AgentTel, @AgentFax, 
			@PSACode, @PSAName, @PSAAddress, @VesselName, @VesselCode, @VoyageNo, @BerthNumber,	@ShipCallNumber, @GrossRegisteredTonnage, 
			@NettRegisteredTonnage,	@OverallLength,	@DeadWeight, @StandardDraft, @VesselCapacity, @ShipClass, @FlagShip, @YearBuilt, @CountryCode,
			@LoadingRemarks,	@DischargeRemarks,	@ChangeRemarks, @CreatedBy, @IsProcessed, @Remarks,@ATA,@ATD


	END
	ELSE
	BEGIN

		

		Select @ReferenceNo = [EDI].[udf_GenerateCusRepReferenceNo](@BranchID,GetUTCDate())
		 

	    Exec [EDI].[usp_CUSREPHeaderInsert] 
			@BranchID,@ReferenceNo, @FileName,@ReferenceText, @DocumentType, @MessageType, @SenderCode, @ReceiverCode, @PreparationDateTime, @POP, @POT, 
			@PlaceOfArrival, @ETA, @ETD, @PlaceOfRegistration, @DischargePort, @DestinationPort, @AgentCode, @AgentContact, @AgentTel, @AgentFax, 
			@PSACode, @PSAName, @PSAAddress, @VesselName, @VesselCode, @VoyageNo, 	@BerthNumber,	@ShipCallNumber,	@GrossRegisteredTonnage,
			@NettRegisteredTonnage,	@OverallLength,	@DeadWeight, @StandardDraft, @VesselCapacity, @ShipClass, @FlagShip, @YearBuilt,	
			@CountryCode, @LoadingRemarks, @DischargeRemarks, @ChangeRemarks, @CreatedBy,  @IsProcessed, @Remarks,@ATA,@ATD


	END

	Exec [EDI].[usp_CreateVesselScheduleFromCUSREP] @ReferenceNo
	
END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSREPHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CUSREPHeader] Record  based on [CUSREPHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSREPHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSREPHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSREPHeaderDelete] 
    @BranchID bigint,
    @ReferenceNo bigint
AS 

	
BEGIN

	UPDATE	[EDI].[CUSREPHeader]
	SET	IsProcessed = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo

	 


END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSREPHeaderDelete]
-- ========================================================================================================================================

GO

