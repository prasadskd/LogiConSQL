
-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationHeader] Record based on [DeclarationHeader] table

-- Exec [EDI].[usp_CUSDECHeaderSearch] 1001001,'DLH','ASDF',NULL,NULL,9101

-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderSearch]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderSearch] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderSearch] 
    @BranchID BIGINT,
    @DeclarationNo NVARCHAR(50),
	@ManifestNo nvarchar(50),
	@DateFrom datetime =NULL,
	@DateTo DateTime =NULL,
	@DeclarationType smallint=0

AS 

BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N'With ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ShipmentType'')
				SELECT	
				CHd.[BranchID], CHd.[ReferenceNo], CHd.[DeclarationNo], CHd.[FileName], CHd.[DocumentType], CHd.[MessageType], CHd.[CreatedOn], CHd.[IsUploaded],
				Hd.ImportDate,Hd.ShipmentType,Hd.DeclarationDate,Hd.IsApproved,
				ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription
				FROM	
				[EDI].CUSDECHeader CHd
				Left Outer Join [EDI].[CUSDECDetail] CDt On
					CHd.BranchID = CDt.BranchID
					And Chd.ReferenceNo = CDt.ReferenceNo
				Left Outer Join [Operation].[DeclarationHeader] Hd ON
					CHd.BranchID = hd.BranchID
					And Chd.DeclarationNo = Hd.DeclarationNo
				Left Outer Join ShipmentTypeDescription ST ON 
					Hd.ShipmentType = ST.LookupID
				Left Outer Join Operation.DeclarationShipment Sh ON
					Hd.BranchID = Sh.BranchID
					And Hd.DeclarationNo = Sh.DeclarationNo
				WHERE   CHd.[BranchID] = ' +  Convert(varchar(20),@BranchID)  

	if (@DeclarationType > 0) set @sql = @sql + ' AND  Hd.[DeclarationType]  =' + Convert(varchar(10),@DeclarationType)
	if (len(rtrim(@DeclarationNo)) > 0) set @sql = @sql + ' AND  Hd.[DeclarationNo] LIKE ''%' + @DeclarationNo + '%'''
	if (len(rtrim(@ManifestNo)) > 0) set @sql = @sql + ' And Sh.ManifestNo LIKE ''%' + @ManifestNo  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ImportDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Hd.ImportDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ImportDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Hd.ImportDate)'

	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderSearch]
-- ========================================================================================================================================

GO

