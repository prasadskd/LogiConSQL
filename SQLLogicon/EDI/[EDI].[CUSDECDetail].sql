
-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CUSDECDetail] Record based on [CUSDECDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailSelect] 
    @BranchID BIGINT,
    @ReferenceNo BIGINT,
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [ReferenceNo], [ItemNo], [OrderNo], [ContainerKey], [CargoKey] 
	FROM   [EDI].[CUSDECDetail]
	WHERE  [BranchID] = @BranchID  
	       AND [ReferenceNo] = @ReferenceNo  
	       AND [ItemNo] = @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CUSDECDetail] Records from [CUSDECDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailList] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailList] 
    @BranchID BIGINT,
    @ReferenceNo BIGINT

AS 
BEGIN

	SELECT [BranchID], [ReferenceNo], [ItemNo], [OrderNo], [ContainerKey], [CargoKey] 
	FROM   [EDI].[CUSDECDetail]
	WHERE  [BranchID] = @BranchID  
	       AND [ReferenceNo] = @ReferenceNo  

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECDetailList] 
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CUSDECDetail] Record Into [CUSDECDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailInsert] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [EDI].[CUSDECDetail] ([BranchID], [ReferenceNo], [ItemNo], [OrderNo], [ContainerKey], [CargoKey])
	SELECT @BranchID, @ReferenceNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CUSDECDetail] Record Into [CUSDECDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailUpdate] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [EDI].[CUSDECDetail]
	SET    [OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [CargoKey] = @CargoKey
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECDetailUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CUSDECDetail] Record Into [CUSDECDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailSave] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @ItemNo smallint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @CargoKey nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [EDI].[CUSDECDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSDECDetailUpdate] 
		@BranchID, @ReferenceNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSDECDetailInsert] 
		@BranchID, @ReferenceNo, @ItemNo, @OrderNo, @ContainerKey, @CargoKey


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSDECDetailSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CUSDECDetail] Record  based on [CUSDECDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECDetailDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECDetailDelete] 
    @BranchID bigint,
    @ReferenceNo bigint 
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CUSDECDetail]
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	       

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECDetailDelete]
-- ========================================================================================================================================

GO
 