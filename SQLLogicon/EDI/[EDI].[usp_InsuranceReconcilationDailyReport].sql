IF OBJECT_ID('[EDI].[usp_InsuranceReconcilationDailyReport]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceReconcilationDailyReport] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceReconcilationDailyReport]  
 
AS 

BEGIN

	--SELECT  (Convert(varchar(100),[MessageID]) + '|' +  [MessageFunction] + '|' + 	Convert(varchar(10),[CreateDateTime],112) + '|' + 
	--    Replace(SUBSTRING(convert(varchar(4), [CreateDateTime],108),1,5),':','') + '|' + Convert(Varchar(10),GetUTCDate(),112) + '|' + 
	--    Replace(SUBSTRING(convert(varchar(4), GetUTCDate(),108),1,5),':','')  + '|' +  [OrderNo] ) As ReconData 
 --         FROM [EDI].[InsuranceHeader] 
 --         where Convert(Char(10),EDIDateTime,120)=Convert(Char(10),GetUtcDate(),120)
		  
	SELECT  Convert(varchar(100),[MessageID]) As MessageID,  [MessageFunction] , 	Convert(varchar(10),[CreateDateTime],112)  As CreateDate, 
	    Replace(SUBSTRING(convert(varchar(4), [CreateDateTime],108),1,5),':','') As CreateTime, Convert(Varchar(10),GetUTCDate(),112)  PrepareDate, 
	    Replace(SUBSTRING(convert(varchar(4), GetUTCDate(),108),1,5),':','') PrepareTime  ,  [OrderNo] 
          FROM [EDI].[InsuranceHeader] 
          where Convert(Char(10),EDIDateTime,120)=Convert(Char(10),GetUtcDate(),120)
		  

END

  