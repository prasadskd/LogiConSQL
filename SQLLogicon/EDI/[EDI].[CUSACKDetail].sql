USE [LogiCon];
GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSACKDetail] Record based on [CUSACKDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSACKDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailSelect] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @ItemNo nchar(10)
AS 
 

BEGIN

	SELECT [BranchID], ReferenceNo, [ItemNo], [PackageID] 
	FROM   [EDI].[CUSACKDetail]
	WHERE  [BranchID] = @BranchID  
	       AND ReferenceNo = @ReferenceNo 
	       AND [ItemNo] = @ItemNo 

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CUSACKDetail] Records from [CUSACKDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSACKDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailList] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailList] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier

AS 
 
BEGIN
	SELECT [BranchID], ReferenceNo, [ItemNo], [PackageID] 
	FROM   [EDI].[CUSACKDetail]
	WHERE  [BranchID] = @BranchID  
	       AND ReferenceNo = @ReferenceNo 

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKDetailList] 
-- ========================================================================================================================================

GO

 

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CUSACKDetail] Record Into [CUSACKDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailInsert] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @ItemNo nchar(10),
    @PackageID nvarchar(250) = NULL
AS 
  

BEGIN
	
	INSERT INTO [EDI].[CUSACKDetail] ([BranchID], ReferenceNo, [ItemNo], [PackageID])
	SELECT @BranchID, @ReferenceNo, @ItemNo, @PackageID
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKDetailInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CUSACKDetail] Record Into [CUSACKDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailUpdate] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @ItemNo nchar(10),
    @PackageID nvarchar(250) = NULL
AS 
 
	
BEGIN

	UPDATE [EDI].[CUSACKDetail]
	SET    [PackageID] = @PackageID
	WHERE  [BranchID] = @BranchID
	       AND ReferenceNo = @ReferenceNo
	       AND [ItemNo] = @ItemNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKDetailUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CUSACKDetail] Record Into [CUSACKDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailSave] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @ItemNo nchar(10),
    @PackageID nvarchar(250) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [EDI].[CUSACKDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND ReferenceNo = @ReferenceNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSACKDetailUpdate] 
		@BranchID, @ReferenceNo, @ItemNo, @PackageID


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSACKDetailInsert] 
		@BranchID, @ReferenceNo, @ItemNo, @PackageID


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSACKDetailSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CUSACKDetail] Record  based on [CUSACKDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKDetailDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKDetailDelete] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier 
AS 

	
BEGIN

	DELETE	[EDI].[CUSACKDetail]
	 
	WHERE 	[BranchID] = @BranchID
	       AND ReferenceNo = @ReferenceNo
 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKDetailDelete]
-- ========================================================================================================================================

GO 
