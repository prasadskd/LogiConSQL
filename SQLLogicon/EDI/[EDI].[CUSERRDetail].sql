

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSERRDetail] Record based on [CUSERRDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSERRDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailSelect] 
    @BranchID bigint,
    @Referenceno uniqueidentifier,
    @ItemNo smallint
AS 
 

BEGIN

	SELECT [BranchID], [Referenceno], [ItemNo], [MessageNo], [ErrorCode], [CustomeIndicator], [ResponseAgency], [TaxType],[TaxAmount] 
	FROM   [EDI].[CUSERRDetail]
	WHERE   [BranchID] = @BranchID 
	       AND [Referenceno] = @Referenceno 
	       AND [ItemNo] = @ItemNo 

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CUSERRDetail] Records from [CUSERRDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSERRDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailList] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailList] 
    @BranchID bigint,
    @Referenceno uniqueidentifier

AS 
 
BEGIN
	SELECT [BranchID], [Referenceno], [ItemNo], [MessageNo], [ErrorCode], [CustomeIndicator], [ResponseAgency], [TaxType],[TaxAmount] 
	FROM   [EDI].[CUSERRDetail]
	WHERE   [BranchID] = @BranchID 
	       AND [Referenceno] = @Referenceno 

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRDetailList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CUSERRDetail] Record Into [CUSERRDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailInsert] 
    @BranchID bigint,
    @Referenceno uniqueidentifier,
    @ItemNo smallint,
    @MessageNo nvarchar(50) = NULL,
    @ErrorCode nvarchar(10) = NULL,
    @CustomeIndicator nvarchar(10) = NULL,
    @ResponseAgency nvarchar(10) = NULL,
    @TaxType nvarchar(10) = NULL,
    @TaxAmount nvarchar(10) = NULL
AS 
  

BEGIN
	
	INSERT INTO [EDI].[CUSERRDetail] (
			[BranchID], [Referenceno], [ItemNo], [MessageNo], [ErrorCode], [CustomeIndicator], [ResponseAgency], [TaxType], [TaxAmount])
	SELECT	@BranchID, @Referenceno, @ItemNo, @MessageNo, @ErrorCode, @CustomeIndicator, @ResponseAgency, @TaxType, @TaxAmount
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRDetailInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CUSERRDetail] Record Into [CUSERRDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailUpdate] 
    @BranchID bigint,
    @Referenceno uniqueidentifier,
    @ItemNo smallint,
    @MessageNo nvarchar(50) = NULL,
    @ErrorCode nvarchar(10) = NULL,
    @CustomeIndicator nvarchar(10) = NULL,
    @ResponseAgency nvarchar(10) = NULL,
    @TaxType nvarchar(10) = NULL,
    @TaxAmount nvarchar(10) = NULL
AS 
 
	
BEGIN

	UPDATE	[EDI].[CUSERRDetail]
	SET		[BranchID] = @BranchID, [Referenceno] = @Referenceno, [ItemNo] = @ItemNo, [MessageNo] = @MessageNo, 
			[ErrorCode] = @ErrorCode, [CustomeIndicator] = @CustomeIndicator, [ResponseAgency] = @ResponseAgency, [TaxType] = @TaxType, [TaxAmount] = @TaxAmount
	WHERE	[BranchID] = @BranchID
			AND [Referenceno] = @Referenceno
			AND [ItemNo] = @ItemNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRDetailUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CUSERRDetail] Record Into [CUSERRDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailSave] 
    @BranchID bigint,
    @Referenceno uniqueidentifier,
    @ItemNo smallint,
    @MessageNo nvarchar(50) = NULL,
    @ErrorCode nvarchar(10) = NULL,
    @CustomeIndicator nvarchar(10) = NULL,
    @ResponseAgency nvarchar(10) = NULL,
    @TaxType nvarchar(10) = NULL,
    @TaxAmount nvarchar(10) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [EDI].[CUSERRDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [Referenceno] = @Referenceno
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSERRDetailUpdate] 
			@BranchID, @Referenceno, @ItemNo, @MessageNo, @ErrorCode, @CustomeIndicator, @ResponseAgency, @TaxType, @TaxAmount


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSERRDetailInsert] 
			@BranchID, @Referenceno, @ItemNo, @MessageNo, @ErrorCode, @CustomeIndicator, @ResponseAgency, @TaxType, @TaxAmount


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSERRDetailSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CUSERRDetail] Record  based on [CUSERRDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRDetailDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRDetailDelete] 
    @BranchID bigint,
    @Referenceno uniqueidentifier 
     
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CUSERRDetail]
	WHERE  [BranchID] = @BranchID
	       AND [Referenceno] = @Referenceno
 END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRDetailDelete]
-- ========================================================================================================================================

GO 
