
-- ========================================================================================================================================
-- START											 [EDI].[usp_CreateVesselScheduleFromCUSREP]
-- ========================================================================================================================================
-- Author:			Sharma
-- Create date: 	22-Feb-2017
-- Description:		Generates Vessel Master and Vessel SChedule from the CUSREPHeader Table (EDI ONLY)
/*
Exec [EDI].[usp_CreateVesselScheduleFromCUSREP] 2017022510000000


select * from Edi.CUSREPHeader

*/

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CreateVesselScheduleFromCUSREP]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CreateVesselScheduleFromCUSREP] 
END 
GO
CREATE PROC [EDI].[usp_CreateVesselScheduleFromCUSREP]
	@ReferenceNo bigint
AS 
BEGIN


-- Vessel Master variables
Declare
	@BranchID bigint,    
	@VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VesselType smallint,
    @PlaceOfRegistration nvarchar(100),
    @CountryCode varchar(2),
    @FlagShip varchar(2),
    @CallSignNo nvarchar(20),
    @LloydID nvarchar(20),
    @VesselClass smallint,
    @OwnerCode nvarchar(20),
    @OwnerAddress1 nvarchar(100),
    @OwnerAddress2 nvarchar(100),
    @OwnerAddress3 nvarchar(100),
    @PSACode nvarchar(20),
    @GrossRegisteredTonnage float,
    @NetRegisteredTonnage float,
    @OverallLength float,
    @DeadWeight float,
    @StandardDraft float,
    @VesselCapacity float,
    @Beam smallint,
    @DisplacementWeight float,
	@YearBuilt smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@defaultVesselType smallint


-- Vessel Schedule variables

Declare 
    @VoyageNoInWard nvarchar(20),
	@VesselScheduleID bigint,
	@VoyageNoOutWard nvarchar(20),
	@AgentCode nvarchar(10),
	@JobType smallint,
	@CustomStationCode nvarchar(50),
	@PlaceOfArrival nvarchar(50),
	@EntryPoint nvarchar(50),
    @LoadingPort nvarchar(10),
    @DischargePort nvarchar(10),
    @DestinationPort nvarchar(10),
    @Terminal nvarchar(10),
    @ETA datetime,
    @ETD datetime,
    @ActualETA datetime,
    @ActualETD datetime,
    @ImpAvaliableDate datetime,
    @ExpAvailableDate datetime,
    @ClosingDate datetime,
    @ClosingDateRF datetime,
    @YardCutOffDate datetime,
    @YardCutOffDateRF datetime,
    @ImportStorageStartDate datetime,
    @IsRevised bit,
    @ShipCallNo nvarchar(25),
	@PSAName nvarchar(50),
	@PSAAddress nvarchar(255)

	Select @defaultVesselType = LookupID From Config.Lookup Where LookupCategory ='VesselType' And LookupCode ='VT-27'

	--select * from config.Lookup where LookupID=9000

	Select @BranchID = BranchID, @VesselID = VesselCode, @VesselName = VesselName, @VesselType = @defaultVesselType, @PlaceOfRegistration = PlaceOfRegistration, @CountryCode = CountryCode, 
			@FlagShip = FlagShip, @CallSignNo = ShipCallNumber, @LloydID = '', @VesselClass = 0, @OwnerCode = '', 
			@OwnerAddress1 = '', @OwnerAddress2 = '', @OwnerAddress3 = '', @PSACode = PSACode, 
			@GrossRegisteredTonnage = GrossRegisteredTonnage, @NetRegisteredTonnage = NettRegisteredTonnage, @OverallLength = OverallLength, 
			@DeadWeight = DeadWeight, @StandardDraft = StandardDraft, @VesselCapacity = VesselCapacity, @Beam = 0, 
			@DisplacementWeight = 0, @CreatedBy = 'EDI',@ModifiedBy = 'EDI',  @YearBuilt =YearBuilt,
			@VoyageNoInWard = VoyageNo, @VoyageNoOutward = VoyageNo,@AgentCode = AgentCode,@JobType=101,@CustomStationCode='',@EntryPoint='',
			@PlaceOfArrival = PlaceOfArrival, @LoadingPort = PlaceOfArrival,@DischargePort = DischargePort,@DestinationPort = DestinationPort,@Terminal='',
			@ETA=ETA,@ETD=ETD,@ActualETA = ATA, @ActualETD = ATD,@ImpAvaliableDate=NULL,@ExpAvailableDate=NULL,@ClosingDate=NULL,
			@ClosingDateRF=NULL,@YardCutOffDate=NULL,@YardCutOffDateRF=NULL,@ImportStorageStartDate=ETA,@IsRevised = Cast(0 as bit),
			@PSAAddress = PSAAddress,@ShipCallNo = ShipCallNumber
	From Edi.CUSREPHeader
	Where ReferenceNo = @ReferenceNo	




	Exec [Master].[usp_VesselSave] 
			@VesselID, @VesselName, @VesselType, @PlaceOfRegistration, @CountryCode, @FlagShip, @CallSignNo, @LloydID, @VesselClass, @OwnerCode, 
			@OwnerAddress1, @OwnerAddress2, @OwnerAddress3, @PSACode, @GrossRegisteredTonnage, @NetRegisteredTonnage, @OverallLength, @DeadWeight, 
			@StandardDraft, @VesselCapacity, @Beam, @DisplacementWeight,@YearBuilt, @CreatedBy, @ModifiedBy 

	Select @VesselScheduleID =0;

	Select @VesselScheduleID = ISNULL(VesselScheduleID,0)
	From Operation.VesselSchedule
	Where BranchID = @BranchID
		And VesselID = @VesselID
		And (VoyageNoInWard = @VoyageNoInWard  OR VoyageNoOutWard = @VoyageNoOutWard)

	--select @VesselScheduleID

	Exec [Operation].[usp_VesselScheduleSave]
			@BranchID,@VesselScheduleID,@VesselID, @VoyageNoInWard,@VoyageNoOutWard,@AgentCode,@JobType,@CustomStationCode,@PlaceOfArrival,@EntryPoint, 
			@LoadingPort, @DischargePort, @DestinationPort, @Terminal, @ETA, @ETD, @ActualETA, 
			@ActualETD, @ImpAvaliableDate, @ExpAvailableDate, @ClosingDate, @ClosingDateRF, @YardCutOffDate, @YardCutOffDateRF, 
			@ImportStorageStartDate, @IsRevised, @ShipCallNo, @CreatedBy, @ModifiedBy,@PSACode,@PSAName,@PSAAddress

	
END

	

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CreateVesselScheduleFromCUSREP]
-- ========================================================================================================================================


GO