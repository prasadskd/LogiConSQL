--alter table [EDI].[K2Declaration]
--add ErrorMessage nvarchar(MAX) NULL 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [K2Declaration] Record based on [K2Declaration] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_K2DeclarationSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationSelect] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationSelect] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN

	SELECT [BranchID], [ReferenceNo], [DeclarationNo],[MessageType],[CreateDate], [EDIDateTime], [FileName],ErrorMessage 
	FROM   [EDI].[K2Declaration]
	WHERE   [BranchID] = @BranchID  
	       AND [ReferenceNo] = @ReferenceNo 
	       AND [DeclarationNo] = @DeclarationNo 

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [K2Declaration] Records from [K2Declaration] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_K2DeclarationList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationList] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationList] 
	 
AS 
 
BEGIN
	SELECT [BranchID], [ReferenceNo], [DeclarationNo],[MessageType],[CreateDate], [EDIDateTime], [FileName],ErrorMessage
	FROM   [EDI].[K2Declaration]
	WHERE   EDIDateTime IS NULL

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [K2Declaration] Record Into [K2Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K2DeclarationInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationInsert] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL
AS 
  

BEGIN
	
	INSERT INTO [EDI].[K2Declaration] ([BranchID], [DeclarationNo], [EDIDateTime], [FileName])
	SELECT @BranchID, @DeclarationNo, @EDIDateTime, @FileName
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [K2Declaration] Record Into [K2Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K2DeclarationUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationUpdate] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationUpdate] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL
AS 
 
	
BEGIN

	UPDATE	[EDI].[K2Declaration]
	SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [EDIDateTime] = @EDIDateTime, 
			[FileName] = @FileName,MessageType=@MessageType
	WHERE	[BranchID] = @BranchID
			AND [ReferenceNo] = @ReferenceNo
			AND [DeclarationNo] = @DeclarationNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [K2Declaration] Record Into [K2Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K2DeclarationSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationSave] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL
AS 
 

BEGIN

	Declare @refNo bigint = SCOPE_IDENTITY()

	IF (SELECT COUNT(0) FROM [EDI].[K2Declaration] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @refNo
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [EDI].[usp_K2DeclarationUpdate] 
		@BranchID, @refNo,@DeclarationNo,@MessageType,@CreateDate, @EDIDateTime, @FileName


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_K2DeclarationInsert] 
		@BranchID, @DeclarationNo,@MessageType,@CreateDate, @EDIDateTime, @FileName


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[K2DeclarationSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [K2Declaration] Record  based on [K2Declaration]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K2DeclarationDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationDelete] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationDelete] 
    @BranchID bigint,
    @ReferenceNo bigint 
AS 

	
BEGIN
 
	DELETE
	FROM   [EDI].[K2Declaration]
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	        
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationDelete]
-- ========================================================================================================================================

GO 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K2DeclarationUpdateEDIDateTime]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K2DeclarationUpdateEDIDateTime]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K2DeclarationUpdateEDIDateTime] 
END 
GO
CREATE PROC [EDI].[usp_K2DeclarationUpdateEDIDateTime] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @FileName nvarchar(100),
	@ErrorMessage nvarchar(max)
AS 
 
	
BEGIN

	IF LEN(@ErrorMessage) = 0
	Begin
	UPDATE [EDI].[K2Declaration]
	SET    [EDIDateTime] = GetUTCDate(), [FileName] = @FileName
	WHERE  [BranchID] = @BranchID
	       AND ReferenceNo = @ReferenceNo
	END
	Else
	Begin
	UPDATE [EDI].[K2Declaration]
	SET    [EDIDateTime] = GetUTCDate(), ErrorMessage = @ErrorMessage
	WHERE  [BranchID] = @BranchID
	       AND ReferenceNo = @ReferenceNo

	ENd

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K2DeclarationUpdateEDIDateTime]
-- ========================================================================================================================================

GO
