

-- ========================================================================================================================================
-- START											 [EDI].[usp_GetOrderCargoItemsForInsurance]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_GetOrderCargoItemsForInsurance]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GetOrderCargoItemsForInsurance]
END 
GO
CREATE PROC [EDI].[usp_GetOrderCargoItemsForInsurance]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70)
AS 
BEGIN
	Select 
	[BranchID],[OrderNo],[ContainerKey],[CargoKey],[ShipperConsigneeCode],[ShipperConsigneeName],[ProductCode],[ProductDescription],
	[StockRoom],[ConsignmentNo],[BatchNo],[PONo],[HouseBLNo],[InvoiceNo],[StyleNo],[Colour],[TallyInNo],
	[IMOCode],[UNNo],[Qty],[UOM],[BaseQty],[BaseUOM],[GrossVolume],[GrossWeight],[PageNo],[CargoLine],
	[MarksNumbers],[Remarks],[WHCode],[PalletCount],[LoadQty],[OrderQty],[Len],[Width],[Height],[CommodityCode],
	[Packs],[CargoClass],[HSCode],[CountryCode],[DeclarationNo],[CreatedBy],[CreatedOn],[ModifiedBy],
	[ModifiedOn],[ItemDescription],[ForeignCurrencyCode],[ForeignPrice],[LocalCurrencyCode],[ExchangeRate],[LocalPrice],
	[IsGST],[GSTCurrencyCode],[PackageCount],[PackageType],[SpecialCargoHandling]  
	FROM [Operation].[OrderCargo]
	where BranchID=@BranchID 
	And OrderNo=@OrderNo

END