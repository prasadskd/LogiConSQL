 

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSDECHeader] Record based on [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderSelect] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN

	SELECT [BranchID], [ReferenceNo], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime], [MovementType] 
	FROM   [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID   
	       AND [ReferenceNo] = @ReferenceNo  
	       AND [DeclarationNo] = @DeclarationNo  

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CUSDECHeader] Records from [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderList] 
	    @BranchID bigint

AS 
 
BEGIN
	SELECT [BranchID], [ReferenceNo], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime], [MovementType] 
	FROM   [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID   

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CUSDECHeader] PageView Records from [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderPageView]
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderPageView] 
    @BranchID bigint,
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [ReferenceNo], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime], [MovementType] 
	FROM   [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID   
	ORDER BY  [DeclarationNo]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderRecordCount] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderRecordCount] 
    @BranchID bigint

AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID   
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CUSDECHeader] auto-complete search based on [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSDECHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderAutoCompleteSearch]
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderAutoCompleteSearch]
        @BranchID bigint,
    @DeclarationNo nvarchar(50)
     
AS 

BEGIN
	SELECT [BranchID], [ReferenceNo], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime], [MovementType] 
	FROM   [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID   
 	     AND [DeclarationNo] LIKE '%' +  @DeclarationNo  + '%' 
END
-- ========================================================================================================================================
-- END  											[EDI].[usp_CUSDECHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CUSDECHeader] Record Into [CUSDECHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @FileName nvarchar(100) = NULL,
    @DocumentType smallint = NULL,
    @MessageType tinyint = NULL,
    @MovementType nvarchar(5)
AS 
  

BEGIN
	
	INSERT INTO [EDI].[CUSDECHeader] (
			[BranchID], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime], [MovementType])
	SELECT	@BranchID, @DeclarationNo, @FileName, @DocumentType, @MessageType, GETUTCDATE(), Cast(0 as bit), NULL, @MovementType
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CUSDECHeader] Record Into [CUSDECHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderUpdate] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @FileName nvarchar(100) = NULL,
    @DocumentType smallint = NULL,
    @MessageType tinyint = NULL,
    @MovementType nvarchar(5)
AS 
 
	
BEGIN

	UPDATE [EDI].[CUSDECHeader]
	SET     [DocumentType] = @DocumentType, [MessageType] = @MessageType,  [MovementType] = @MovementType
	WHERE  [BranchID] = @BranchID
	       
	       AND [DeclarationNo] = @DeclarationNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CUSDECHeader] Record Into [CUSDECHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
    @FileName nvarchar(100) = NULL,
    @DocumentType smallint = NULL,
    @MessageType tinyint = NULL,
    @MovementType nvarchar(5)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [EDI].[CUSDECHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = SCOPE_IDENTITY()
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSDECHeaderUpdate] 
		@BranchID, @DeclarationNo, @FileName, @DocumentType, @MessageType,  @MovementType


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSDECHeaderInsert] 
		@BranchID, @DeclarationNo, @FileName, @DocumentType, @MessageType,  @MovementType


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSDECHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSDECHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CUSDECHeader] Record  based on [CUSDECHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSDECHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSDECHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSDECHeaderDelete] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CUSDECHeader]
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSDECHeaderDelete]
-- ========================================================================================================================================

GO 