
-- ========================================================================================================================================
-- START											 [EDI].[udf_GenerateCusRepReferenceNo]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	18-Dec-2016
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[udf_GenerateCusRepReferenceNo]') IS NOT NULL
BEGIN 
    DROP FUNCTION [EDI].[udf_GenerateCusRepReferenceNo]
END 
GO
Create   function [EDI].[udf_GenerateCusRepReferenceNo]
(
	@BranchID bigInt,
	@EdiDate datetime
)
returns bigint
begin
	declare @ediReferenceNumber bigint
	declare @counter int;

	set @counter = 0;

	while (@counter < 100)
	begin
		select @ediReferenceNumber = 
			cast(
				(convert(varchar, year(@EdiDate)) + 
				case 
					when month(@EdiDate) < 10 then '0'
					else ''
				end + 
				convert(varchar, month(@EdiDate)) + 
				case 
					when day(@EdiDate) < 10 then '0'
					else ''	
				end + 
				convert(varchar, day(@EdiDate)) + 
				convert(varchar, @BranchID) + 
				case 
					when (@counter = 0) or (@counter % 10 > 0) then '0' 
					else '' 
				end  + 
				convert(varchar, @counter))
			as bigint);

		if not exists (
			select 1 from EDI.CUSREPHeader 
			where 
			BranchID = @BranchID
			And ReferenceNo = @ediReferenceNumber
		)
		begin
			break; -- exit for
		end

		set @counter = @counter + 1;
	end 

	return @ediReferenceNumber;
end;
-- ========================================================================================================================================
-- END  											 [EDI].[udf_GenerateCusRepReferenceNo]
-- ========================================================================================================================================

GO