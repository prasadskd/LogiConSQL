

-- ========================================================================================================================================
-- START											 [EDI].[usp_DumpK1DetailsForCUSDEC]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the Declaration Reference To the EDI Watcher Table. 
-- Should be implemented at the K1 Document Page , when the User clicks the "Generate K1 Document "

-- [EDI].[usp_DumpK1DetailsForCUSDEC] 1007001,'DLHQ170100002'

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_DumpK1DetailsForCUSDEC]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_DumpK1DetailsForCUSDEC]
END 
GO
CREATE PROC [EDI].[usp_DumpK1DetailsForCUSDEC] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
     
AS 
  

BEGIN
	
	Declare @MessageType smallint,
			@DocumentType smallint,
			@MovementType nvarchar(3)

	Select @MessageType = 9, @DocumentType = 929,@MovementType = 'IN';

	/*
		TODO : Load the Above 2 variables from the Config.Lookup with EDIMessageCategoryType, and DocumentCategoryType. (Sharma : 2017-02-26)
	*/

	INSERT INTO [EDI].[CUSDECHeader] ([BranchID], [DeclarationNo], [FileName], [DocumentType], [MessageType], [CreatedOn], [IsUploaded], [EDIDateTime],[MovementType])
	SELECT BranchID,DeclarationNo,'',@DocumentType,@MessageType,GETUTCDATE(),Cast(0 as bit),NULL,@MovementType
	From Operation.DeclarationHeader
	Where BranchID = @BranchID
	And DeclarationNo = @DeclarationNo
	And IsActive = Cast(1 as bit) 

	
               
END

-- ========================================================================================================================================
-- END  											[EDI].[usp_DumpK1DetailsForCUSDEC]
-- ========================================================================================================================================

GO
