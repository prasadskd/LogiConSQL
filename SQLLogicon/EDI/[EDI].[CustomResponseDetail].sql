
-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomResponseDetail] Record based on [CustomResponseDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CustomResponseDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailSelect] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailSelect] 
    @BranchID BIGINT,
    @ResponseReferenceNo NVARCHAR(35),
    @DeclarationNo VARCHAR(50),
    @ItemNo SMALLINT
AS 

BEGIN

	SELECT [BranchID], [ResponseReferenceNo], [DeclarationNo], [ItemNo], [Code], [Description], [ItemLine] 
	FROM   [EDI].[CustomResponseDetail]
	WHERE  [BranchID] = @BranchID  
	       AND [ResponseReferenceNo] = @ResponseReferenceNo  
	       AND [DeclarationNo] = @DeclarationNo  
	       AND [ItemNo] = @ItemNo  
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomResponseDetail] Records from [CustomResponseDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CustomResponseDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailList] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailList] 
    @BranchID BIGINT,
    @ResponseReferenceNo NVARCHAR(35),
    @DeclarationNo VARCHAR(50) 
AS 

BEGIN

	SELECT [BranchID], [ResponseReferenceNo], [DeclarationNo], [ItemNo], [Code], [Description], [ItemLine] 
	FROM   [EDI].[CustomResponseDetail]
	WHERE  [BranchID] = @BranchID  
	       AND [ResponseReferenceNo] = @ResponseReferenceNo  
	       AND [DeclarationNo] = @DeclarationNo  
	      

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseDetailList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CustomResponseDetail] Record Into [CustomResponseDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailInsert] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailInsert] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @ItemNo smallint,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @ItemLine nvarchar(100)
AS 
  

BEGIN

	
	INSERT INTO [EDI].[CustomResponseDetail] ([BranchID], [ResponseReferenceNo], [DeclarationNo], [ItemNo], [Code], [Description], [ItemLine])
	SELECT @BranchID, @ResponseReferenceNo, @DeclarationNo, @ItemNo, @Code, @Description, @ItemLine
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CustomResponseDetail] Record Into [CustomResponseDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailUpdate] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @ItemNo smallint,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @ItemLine nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [EDI].[CustomResponseDetail]
	SET    [BranchID] = @BranchID, [ResponseReferenceNo] = @ResponseReferenceNo, [DeclarationNo] = @DeclarationNo, [ItemNo] = @ItemNo, [Code] = @Code, [Description] = @Description, [ItemLine] = @ItemLine
	WHERE  [BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseDetailUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CustomResponseDetail] Record Into [CustomResponseDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailSave] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailSave] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @ItemNo smallint,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @ItemLine nvarchar(100)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [EDI].[CustomResponseDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [EDI].[usp_CustomResponseDetailUpdate] 
		@BranchID, @ResponseReferenceNo, @DeclarationNo, @ItemNo, @Code, @Description, @ItemLine


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CustomResponseDetailInsert] 
		@BranchID, @ResponseReferenceNo, @DeclarationNo, @ItemNo, @Code, @Description, @ItemLine


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CustomResponseDetailSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CustomResponseDetail] Record  based on [CustomResponseDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseDetailDelete] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseDetailDelete] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @ItemNo smallint
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CustomResponseDetail]
	WHERE  [BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	       AND [ItemNo] = @ItemNo
	 


END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseDetailDelete]
-- ========================================================================================================================================

GO 