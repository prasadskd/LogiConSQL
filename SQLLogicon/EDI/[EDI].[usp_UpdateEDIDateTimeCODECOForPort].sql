IF OBJECT_ID('[EDI].[usp_UpdateEDIDateTimeCODECOForPort]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_UpdateEDIDateTimeCODECOForPort]
END 
GO
CREATE  PROC [EDI].[usp_UpdateEDIDateTimeCODECOForPort] 
    @DocumentNo varchar(30)
As
Begin
	UPDATE Port.ContainerMovement 
	SET EDIDateTime = GETDATE()
	Where DocumentNo = @DocumentNo

End