

-- ========================================================================================================================================
-- START											 [EDI].[usp_GenerateOrderForInsurance]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OrderHeader] Record based on [OrderHeader] table

-- Exec [EDI].[usp_GetOrderHeaderForInsurance] 1009001,	10012
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_GetOrderHeaderForInsurance]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GetOrderHeaderForInsurance]
END 
GO
CREATE PROC [EDI].[usp_GetOrderHeaderForInsurance]  
    @BranchID BIGINT,
    @InsuranceReferenceNo bigint
AS 
 

BEGIN

	
	;with	JobTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
			ShipmentTypeLookup As (Select Lookupid,LookupDescription,MappingCode From Config.Lookup Where LookupCategory='ShipmentType'),
			ServiceTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='ServiceType'),
			TransportTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='TransportType'),
			StateTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='StateType'),
			ModuleTypeLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module'),
			IncoTermsLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='IncoTerms'),
			TransportModeLookup   As(Select * From Config.Lookup Where LookupCategory = 'TransportMode')
	SELECT	  OrdHd.[BranchID], OrdHd.[OrderNo], OrdHd.[OrderDate], OrdHd.[MasterJobNo], OrdHd.[ModuleID],  
			OrdHd.[ShipmentType], OrdHd.[ServiceType], OrdHd.[IncoTerm], OrdHd.[OrderCategory], OrdHd.[ShipperCode], 
			OrdHd.[ConsigneeCode], OrdHd.[PrincipalCode], OrdHd.[CustomerCode], OrdHd.[VendorCode], OrdHd.[CoLoaderCode], 
			OrdHd.[ShippingAgent], OrdHd.[ForeignAgent], OrdHd.[FwdAgent], OrdHd.[PortOperator], OrdHd.[VesselScheduleID], 
			OrdHd.[VesselID], OrdHd.[Voyageno], OrdHd.[FeederVoyageNo],OrdHd.[CustomerRef], OrdHd.[OceanBLNo], OrdHd.[HouseBLNo], 
			OrdHd.[ShippingLinerRefNo], OrdHd.[ShippingNote], OrdHd.[KANo],OrdHd.[ManifestNo], OrdHd.[SCNNo], OrdHd.[PlaceOfReceipt], OrdHd.[PortOfLoading], 
			OrdHd.[TranshipmentPort], OrdHd.[PortOfDischarge], OrdHd.[PlaceOfDischarge], OrdHd.[DropOffCode], OrdHd.[ClosingDate], 
			OrdHd.[ClosingDateReefer], OrdHd.[ETA], OrdHd.[ETD], OrdHd.[YardCutOffTime], OrdHd.[YardCutOffTimeReefer],OrdHd.[OrderStatus],
			OrdHd.[WebOrderStatus],OrdHd.[FreightStatus],OrdHd.[FwdStatus],OrdHd.[TransportStatus],OrdHd.[HaulageStatus],OrdHd.[WarehouseStatus],
			OrdHd.[CFSStatus],OrdHd.[DepotStatus],OrdHd.[BillingStatus],OrdHd.[IsMasterJob],OrdHd.[IsBillable],OrdHd.[IsCancel], 
			OrdHd.[IsPrinted], OrdHd.[IsEDI],OrdHd.[EDIDateTime], OrdHd.[CancelBy], OrdHd.[CancelOn], OrdHd.[PrintedBy], 
			OrdHd.[PrintedOn], OrdHd.[CreatedBy], OrdHd.[CreatedOn], OrdHd.[ModifiedBy], OrdHd.[ModifiedOn] ,OrdHd.[Remarks],
			OrdHd.CargoCurrencyCode,OrdHd.CargoExchangeRate,OrdHd.CargoBaseAmount,OrdHd.CargoLocalAmount,OrdHd.PrimaryYard,
			OrdHd.FwdAgentCompanyID,OrdHd.FwdAgentBranchID,OrdHd.FwdAgentRemarks, OrdHd.FwdAgentModifiedBy,OrdHd.FwdAgentModifiedOn,
			OrdHd.NotifyParty,OrdHd.NotifyPartyAddressID,OrdHd.CustomerAddressID,OrdHd.ShipperAddressID,OrdHd.ConsigneeAddressID,OrdHd.ShippingAgentAddressID,OrdHd.FwdAgentAddressID,
			OrdHd.InvoiceNo,OrdHd.InvoiceAmount,OrdHd.InvoiceCurrency,OrdHd.LocalAmount,OrdHd.ExchangeRate,OrdHd.PONo,OrdHd.COLoaderBLNo,OrdHd.ContractNo,
			OrdHd.SalesOrderNo,OrdHd.SalesOrderDate,OrdHd.SalesTerm,OrdHd.PaymentTerm,OrdHd.VehicleNo1,OrdHd.VehicleNo2,OrdHd.WagonNo,
			OrdHd.FlightNo,OrdHd.ARNNo,OrdHd.MasterAWBNo,OrdHd.HouseAWBNo,OrdHd.AIRCOLoadNo,OrdHd.JKNo,OrdHd.InvoiceDate ,
			Vsl.CallSignNo,
			ISNULL(Mdl.LookupDescription ,'') As ModuleDescription ,
			ISNULL(Jb.LookupDescription,'') As JobTypeDescription ,
			ISNULL(Sp.LookupDescription,'') As ShipmentTypeDescription ,
			ISNULL(Sp.MappingCode,'') As MappingCode,
			ISNULL(Svr.LookupDescription,'') As ServiceTypeDescription,
			ISNULL(Tpt.LookupDescription,'') As TransportTypeDescription, 
			ISNULL(Jc.Description,'') As JobCategoryDescription, 
			ISNULL(Shpr.MerchantName,'') As ShipperName, 
			ISNULL(Cons.MerchantName,'') As ConsigneeName, 
			ISNULL(Pr.MerchantName,'') As PrincipalName, 
			ISNULL(Cus.MerchantName,'') As CustomerName, 
			ISNULL(Ntp.MerchantName,'') As NotifyPartyName,
			ISNULL(Vndr.MerchantName,'') As VendorName, 
			ISNULL(CoLdr.MerchantName,'') As CoLoaderName, 
			ISNULL(SL.MerchantName,'') As ShippingAgentName, 
			ISNULL(FA.MerchantName,'') As ForeignAgentName, 
			ISNULL(Fwd.MerchantName,'') As ForwardingAgentName, 
			ISNULL(Opr.MerchantName,'') As PortOperatorName, 
			ISNULL(Drp.MerchantName,'') As DropOffName, 
			ISNULL(Vsl.VesselName,'') As VesselName, 
			ISNULL(PoR.PortName,'') As PlaceOfReceiptName, 
			ISNULL(PoR.PortName,'') As PlaceOfReceiptName, 
			ISNULL(PoL.PortName,'') As PortOfLoadingName, 
			ISNULL(TransPort.PortName,'') As TranshipmentPortName, 
			ISNULL(PoD.PortName,'') As PortOfDischargeName, 
			ISNULL(PloD.PortName,'') As PlaceOfDischargeName, 
			ISNULL(OrdSt.LookupDescription,'') As OrderStatusDescription, 
			ISNULL(OrdStweb.LookupDescription,'') As WebOrderStatusDescription, 
			ISNULL(FrgSt.LookupDescription,'') As FreightStatusDescription, 
			ISNULL(FwdSt.LookupDescription,'') As FwdStatusDescription, 
			ISNULL(TrnSt.LookupDescription,'') As TransportStatusDescription, 
			ISNULL(HlgSt.LookupDescription,'') As HaulageStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(CfsSt.LookupDescription,'') As CFSStatusDescription, 
			ISNULL(DptSt.LookupDescription,'') As DepotStatusDescription, 
			ISNULL(WhSt.LookupDescription,'') As WarehouseStatusDescription, 
			ISNULL(BillSt.LookupDescription,'') As BillingStatusDescription,
			ISNULL(Inco.LookupDescription,'') As IncoTermsDescription,
			ISNULL(Cur.Description,'') As CargoCurrencyDescription,

			ISNULL(CAdd.Address1,'') As CustomerAddress1,
			ISNULL(CAdd.Address2,'') As CustomerAddress2,
			ISNULL(CAdd.Address3,'') As CustomerAddress3,
			ISNULL(CAdd.Address4,'') As CustomerAddress4, 
			ISNULL(CAdd.City,'') As CustomerCity,
			ISNULL(CAdd.CountryCode,'') As CustomerCountryCode,
			ISNULL(CAdd.ZipCode,'') As CustomerZipCode,
			ISNULL(CAdd.State,'') As CustomerState,
			ISNULL(CAdd.TelNo,'') As CustomerTelNo,
			ISNULL(Cus.RegNo,'') As CustomerROCNo,

			ISNULL(SAdd.Address1,'') As ShipperAddress1,
			ISNULL(SAdd.Address2,'') As ShipperAddress2,
			ISNULL(SAdd.Address3,'') As ShipperAddress3,
			ISNULL(SAdd.Address4,'') As ShipperAddress4, 
			ISNULL(SAdd.City,'') As ShipperCity,
			ISNULL(SAdd.CountryCode,'') As ShipperCountryCode,
			ISNULL(SAdd.ZipCode,'') As ShipperZipCode,
			ISNULL(SAdd.State,'') As ShipperState,
			ISNULL(CAdd.TelNo,'') As ShipperTelNo,
			ISNULL(Shpr.TaxID,'') As ShipperGSTnumber,
			ISNULL(Shpr.RegNo,'') As ShipperROCNo,
			ISNULL(Shpr.CreatedOn,'') As ShipperGSTRegDate,

			ISNULL(CnAdd.Address1,'') As ConsigneeAddress1,
			ISNULL(CnAdd.Address2,'') As ConsigneeAddress2,
			ISNULL(CnAdd.Address3,'') As ConsigneeAddress3,
			ISNULL(CnAdd.Address4,'') As ConsigneeAddress4, 
			ISNULL(CnAdd.City,'') As ConsigneeCity,
			ISNULL(CnAdd.CountryCode,'') As ConsigneeCountryCode,
			ISNULL(CnAdd.ZipCode,'') As ConsigneeZipCode,
			ISNULL(CnAdd.State,'') As ConsigneeState,
			ISNULL(CAdd.TelNo,'') As ConsigneeTelNo,
			ISNULL(Cons.RegNo,'') As ConsigneeROCNo,
			ISNULL(Cons.TaxID,'') As ConsigneeGSTnumber,
			ISNULL(Cons.CreatedOn,'') As ConsigneeGSTRegDate,
			

			ISNULL(NAdd.Address1,'') As NotifyPartyAddress1,
			ISNULL(NAdd.Address2,'') As NotifyPartyAddress2,
			ISNULL(NAdd.Address3,'') As NotifyPartyAddress3,
			ISNULL(NAdd.Address4,'') As NotifyPartyAddress4, 
			ISNULL(NAdd.City,'') As NotifyPartyCity,
			ISNULL(NAdd.CountryCode,'') As NotifyPartyCountryCode,
			ISNULL(NAdd.ZipCode,'') As NotifyPartyZipCode,
			ISNULL(NAdd.State,'') As NotifyPartyState,
			ISNULL(NAdd.TelNo,'') As NotifyPartyTelNo,
			ISNULL(Ntp.RegNo,'') As NotifyPartyROCNo,

			ISNULL(FaAdd.Address1,'') As FwdAgentAddress1,
			ISNULL(FaAdd.Address2,'') As FwdAgentAddress2,
			ISNULL(FaAdd.Address3,'') As FwdAgentAddress3,
			ISNULL(FaAdd.Address4,'') As FwdAgentAddress4, 
			ISNULL(FaAdd.City,'') As FwdAgentCity,
			ISNULL(FaAdd.CountryCode,'') As FwdAgentCountryCode,
			ISNULL(FaAdd.ZipCode,'') As FwdAgentZipCode,
			ISNULL(FaAdd.State,'') As FwdAgentState,
			ISNULL(FaAdd.TelNo,'') As FwdAgentTelNo,
			ISNULL(Fwd.RegNo,'') As FwdAgentROCNo,
			ISNULL(Fwd.TaxID,'') As FwdAgentGSTnumber,
			ISNULL(Fwd.CreatedOn,'') As FwdAgentGSTRegDate,

			ISNULL(SaAdd.Address1,'') As ShippingAgentAddress1,
			ISNULL(SaAdd.Address2,'') As ShippingAgentAddress2,
			ISNULL(SaAdd.Address3,'') As ShippingAgentAddress3,
			ISNULL(SaAdd.Address4,'') As ShippingAgentAddress4, 
			ISNULL(SaAdd.City,'') As ShippingAgentCity,
			ISNULL(SaAdd.CountryCode,'') As ShippingAgentCountryCode,
			ISNULL(SaAdd.ZipCode,'') As ShippingAgentZipCode,
			ISNULL(SaAdd.State,'') As ShippingAgentState,
			ISNULL(SaAdd.TelNo,'') As ShippingAgentTelNo,
			ISNULL(SL.RegNo,'') As ShippingAgentROCNo,
			ISNULL(Usr.EmailID,'') As  CreatedByEmail,
			ISNULL(Usr.ContactNo,'') As  CreatedByTelNo,
			ISNULL(Usr.UserName,'') As  CreatedByName,
			ISNULL(Trm.LookupCode,'') As TransportMode,
			ISNULL(OC.PackageType,'') As PackageType,
			IH.InsuranceReferenceNo,IH.MessageFunction,
			Case 
			When Jb.LookupDescription ='IMPORT' Then 'I'
			When Jb.LookupDescription ='EXPORT' Then 'E'
			Else 'T'
			END As JobType,
			Convert(nvarchar(50),IH.MessageID) As MessageID,
			Case 
			When Trm.LookupCode ='SEA' Then '1'
			When Trm.LookupCode ='AIR' Then '4'
			When Trm.LookupCode ='RAIL' Then '2'
			When Trm.LookupCode ='ROAD' Then '3'
			When Trm.LookupCode ='FIXTRANSPORT' Then '7'
			Else '999' End As TransportType, OrdHd.InvoiceCurrency,
			ISNULL(CmpAdd.CountryCode,'999') As UserCountryCode,
			ISNULL(Br.RegNo,'') As UserROCNo	
	FROM	
	[EDI].[InsuranceHeader] IH 
	Left Outer Join 
	[Operation].[OrderHeader] OrdHd On		
		IH.BranchID = OrdHd.BranchID 
		And IH.OrderNo = OrdHd.OrderNo
	Left Outer Join Operation.OrderCargo OC ON
			OrdHd.[BranchID]=OC.BranchID 
			And OrdHd.OrderNo = OC.OrderNo
	Left Outer Join ModuleTypeLookup Mdl ON 
		OrdHd.ModuleID = Mdl.LookupID
	Left Outer Join JobTypeLookup Jb ON 
		OrdHd.JobType = Jb.LookupID
	Left Outer Join ShipmentTypeLookup Sp On
		OrdHd.ShipmentType = Sp.LookupID
	Left Outer Join ServiceTypeLookup Svr ON 
		OrdHd.ServiceType = Svr.LookupID 
	Left Outer Join TransportTypeLookup Tpt ON 
		OrdHd.TransportType = Tpt.LookupID
	Left Outer Join Master.JobCategory Jc ON 
		OrdHd.OrderCategory = jc.Code
	Left Outer Join Master.Merchant Shpr ON 
		OrdHd.ShipperCode = Shpr.MerchantCode 
		--And Shpr.IsShipper = CAST(1 as bit)
	Left Outer Join Master.Merchant Cons ON 
		OrdHd.ConsigneeCode = Cons.MerchantCode 
		--And Cons.IsConsignee = CAST(1 as bit)
	Left Outer Join Master.Merchant Pr ON 
		OrdHd.PrincipalCode = Pr.MerchantCode 
		--And Pr.IsPrincipal = CAST(1 as bit)
	Left Outer Join Master.Merchant Cus ON 
		OrdHd.CustomerCode = Cus.MerchantCode 
	Left Outer Join Master.Merchant Vndr ON 
		OrdHd.VendorCode = Vndr.MerchantCode 
		And Shpr.IsVendor = CAST(1 as bit)
	Left Outer Join Master.Merchant CoLdr ON 
		OrdHd.CoLoaderCode = CoLdr.MerchantCode 
	Left Outer Join Master.Merchant SL ON 
		OrdHd.ShippingAgent = SL.MerchantCode 
	Left Outer Join Master.Merchant FA ON 
		OrdHd.ForeignAgent = FA.MerchantCode 
		And FA.IsLiner= CAST(1 as bit)
	Left Outer Join Master.Merchant Fwd ON 
		OrdHd.FwdAgent = Fwd.MerchantCode 
		And Fwd.IsForwarder= CAST(1 as bit)
	Left Outer Join Master.Merchant Opr ON 
		OrdHd.PortOperator = Opr.MerchantCode 
		And Opr.IsTerminal= CAST(1 as bit)
	Left Outer Join Master.Merchant Drp ON
		OrdHd.DropOffCode = Drp.MerchantCode
		And Drp.IsYard = Cast(1 as bit)
	Left Outer Join Master.Merchant Ntp ON
		OrdHd.NotifyParty = Ntp.MerchantCode
	Left Outer Join Master.Vessel Vsl ON 
		OrdHd.VesselID = Vsl.VesselID
	Left Outer Join Master.Port PoR On 
		OrdHd.PlaceOfReceipt = PoR.PortCode
	Left Outer Join Master.Port PoL ON
		OrdHd.PortOfLoading = PoL.PortCode 
	Left Outer Join Master.Port TransPort ON
		OrdHd.TranshipmentPort = Transport.PortCode
	Left Outer Join master.Port PoD ON
		OrdHd.PortOfDischarge = PoD.PortCode
	Left Outer Join master.Port PloD ON 
		OrdHd.PlaceOfDischarge = PloD.PortCode
	Left Outer Join StateTypeLookup OrdSt ON 
		OrdHd.OrderStatus = OrdSt.LookupID
	Left Outer Join StateTypeLookup OrdStweb ON 
		OrdHd.WebOrderStatus = OrdStweb.LookupID
	Left Outer Join StateTypeLookup FrgSt ON 
		OrdHd.FreightStatus = FrgSt.LookupID
	Left Outer Join StateTypeLookup FwdSt ON 
		OrdHd.FwdStatus = FwdSt.LookupID
	Left Outer Join StateTypeLookup TrnSt ON 
		OrdHd.TransportStatus = TrnSt.LookupID
	Left Outer Join StateTypeLookup HlgSt ON 
		OrdHd.HaulageStatus = HlgSt.LookupID
	Left Outer Join StateTypeLookup WhSt ON 
		OrdHd.WarehouseStatus = WhSt.LookupID
	Left Outer Join StateTypeLookup CfsSt ON 
			OrdHd.CFSStatus = CfsSt.LookupID
	Left Outer Join StateTypeLookup DptSt ON 
			OrdHd.DepotStatus = DptSt.LookupID
	Left Outer Join StateTypeLookup BillSt ON 
			OrdHd.BillingStatus = BillSt.LookupID
	Left Outer Join IncoTermsLookup Inco ON 
		OrdHd.IncoTerm = Inco.LookupID
	Left Outer Join Master.Currency Cur ON 
		OrdHd.CargoCurrencyCode = Cur.CurrencyCode
	Left Outer Join Master.Address CAdd On 
		OrdHd.CustomerCode = CAdd.LinkId
		And CAdd.AddressType='Merchant'
	Left Outer Join Master.Address SAdd On 
		OrdHd.ShipperCode = SAdd.LinkId
		And SAdd.AddressType='Merchant'
	Left Outer Join Master.Address CnAdd On 
		OrdHd.ConsigneeCode = CnAdd.LinkId
		And CnAdd.AddressType='Merchant'
	Left Outer Join Master.Address NAdd On 
		OrdHd.NotifyParty = NAdd.LinkId
		And NAdd.AddressType='Merchant'
	Left Outer Join Master.Address FaAdd On 
		OrdHd.FwdAgent = FaAdd.LinkId
		And FaAdd.AddressType='Merchant'
	Left Outer Join Master.Address SaAdd On 
		OrdHd.ShippingAgent = SaAdd.LinkId
		And SaAdd.AddressType='Merchant'
	Left Outer Join [Security].[User] Usr ON 
		OrdHd.CreatedBy = Usr.UserID
	Left Outer Join TransportModeLookup Trm ON
		OrdHd.TransportType=Trm.LookupID
	--Left Outer Join Master.Address BrAdd ON 
	--	Convert(nvarchar(50),OrdHd.BranchID) = Convert(nvarchar(50),BrAdd.LinkID)
	--	And BrAdd.AddressType ='Branch'
	Left Outer Join Master.Branch Br On 
		OrdHd.BranchID = Br.BranchID 
	Left Outer Join Master.Company Cmp ON 
		Br.CompanyCode = Cmp.CompanyCode 
	Left Outer Join Master.Address CmpAdd ON 
		Convert(nvarchar(50),Cmp.CompanyCode) = Convert(nvarchar(50),CmpAdd.LinkID)
		And CmpAdd.AddressType ='Company'
	
	WHERE	IH.[BranchID] = @BranchID  
			AND IH.[InsuranceReferenceNo] = @InsuranceReferenceNo
			AND IH.EDIDateTime IS NULL 
	Order By IH.CreateDateTime
			 
			 

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_GenerateOrderForInsurance]
-- ========================================================================================================================================

GO
