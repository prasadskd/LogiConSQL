

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSERRHeader] Record based on [CUSERRHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSERRHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderSelect] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier
AS 
 

BEGIN

	SELECT	[BranchID], [ReferenceNo], [EDIType], [FileName], [DocumentDate], [SenderID], [ReceiverID], 
			[MessageRefernceNo], [MessageType], [MessageFunction], [ProcessIndicator], [StatusCode], [CustomsName], 
			[CustomsAddress], [DeclarantName], [DeclarantAddress], [AgentName], [AgentAddress], [ShippingAgentName], 
			[ShippingAgentAddress], [CustomsStationCode], [PlaceOfDischarge], [DateType], [PresentationDate], 
			[GISIndicator1], [GISIndicator2], [ResponseCode], [ResponseOGACode], [PackageType], [PackageCode], 
			[PackageCount], [MarksAndNumbers], [OrderType], [RegistrationDate], [DeclarationNo], [DeclarationRefNo], 
			[VesselID], [VoyageNo], [FlightNo], [PTJCode], [WareHouseLicenceNo], [FileStatus], [IsProcessed], 
			[ProcessRemarks], [OrderNo] 
	FROM	[EDI].[CUSERRHeader]
	WHERE	[BranchID] = @BranchID  
			AND [ReferenceNo] = @ReferenceNo  
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CUSERRHeader] Records from [CUSERRHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSERRHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderList] 
    @BranchID bigint
AS 
 
BEGIN
	SELECT	[BranchID], [ReferenceNo], [EDIType], [FileName], [DocumentDate], [SenderID], [ReceiverID], 
			[MessageRefernceNo], [MessageType], [MessageFunction], [ProcessIndicator], [StatusCode], [CustomsName], 
			[CustomsAddress], [DeclarantName], [DeclarantAddress], [AgentName], [AgentAddress], [ShippingAgentName], 
			[ShippingAgentAddress], [CustomsStationCode], [PlaceOfDischarge], [DateType], [PresentationDate], 
			[GISIndicator1], [GISIndicator2], [ResponseCode], [ResponseOGACode], [PackageType], [PackageCode], 
			[PackageCount], [MarksAndNumbers], [OrderType], [RegistrationDate], [DeclarationNo], [DeclarationRefNo], 
			[VesselID], [VoyageNo], [FlightNo], [PTJCode], [WareHouseLicenceNo], [FileStatus], [IsProcessed], 
			[ProcessRemarks], [OrderNo] 
	FROM	[EDI].[CUSERRHeader]
	WHERE	[BranchID] = @BranchID  
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRHeaderList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CUSERRHeader] Record Into [CUSERRHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderInsert] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @EDIType smallint,
    @FileName nvarchar(100),
    @DocumentDate datetime,
    @SenderID nvarchar(35) = NULL,
    @ReceiverID nvarchar(35) = NULL,
    @MessageRefernceNo nvarchar(20) = NULL,
    @MessageType smallint = NULL,
    @MessageFunction tinyint = NULL,
    @ProcessIndicator nvarchar(3) = NULL,
    @StatusCode nvarchar(3) = NULL,
    @CustomsName nvarchar(50) = NULL,
    @CustomsAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @ShippingAgentAddress nvarchar(250) = NULL,
    @CustomsStationCode nvarchar(50) = NULL,
    @PlaceOfDischarge nvarchar(50) = NULL,
    @DateType smallint = NULL,
    @PresentationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @ResponseCode nvarchar(50) = NULL,
    @ResponseOGACode nvarchar(50) = NULL,
    @PackageType nvarchar(10) = NULL,
    @PackageCode nvarchar(10) = NULL,
    @PackageCount nvarchar(10) = NULL,
    @MarksAndNumbers nvarchar(200) = NULL,
    @OrderType smallint = NULL,
    @RegistrationDate datetime = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @DeclarationRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(20) = NULL,
    @VoyageNo nvarchar(10) = NULL,
    @FlightNo nvarchar(10) = NULL,
    @PTJCode nvarchar(10) = NULL,
    @WareHouseLicenceNo nvarchar(10) = NULL,
    @FileStatus bit = NULL,
    @IsProcessed bit = NULL,
    @ProcessRemarks nvarchar(100) = NULL,
    @OrderNo nvarchar(50) = NULL,
	@NewRegistrationNo uniqueIdentifier OUTPUT
AS 
  

BEGIN
	
	INSERT INTO [EDI].[CUSERRHeader] (
			[BranchID], [ReferenceNo], [EDIType], [FileName], [DocumentDate], [SenderID], [ReceiverID], [MessageRefernceNo], 
			[MessageType], [MessageFunction], [ProcessIndicator], [StatusCode], [CustomsName], [CustomsAddress], [DeclarantName], 
			[DeclarantAddress], [AgentName], [AgentAddress], [ShippingAgentName], [ShippingAgentAddress], [CustomsStationCode], 
			[PlaceOfDischarge], [DateType], [PresentationDate], [GISIndicator1], [GISIndicator2], [ResponseCode], [ResponseOGACode], 
			[PackageType], [PackageCode], [PackageCount], [MarksAndNumbers], [OrderType], [RegistrationDate], [DeclarationNo], 
			[DeclarationRefNo], [VesselID], [VoyageNo], [FlightNo], [PTJCode], [WareHouseLicenceNo], [FileStatus], [IsProcessed], 
			[ProcessRemarks], [OrderNo])
	SELECT	@BranchID, @ReferenceNo, @EDIType, @FileName, @DocumentDate, @SenderID, @ReceiverID, @MessageRefernceNo, 
			@MessageType, @MessageFunction, @ProcessIndicator, @StatusCode, @CustomsName, @CustomsAddress, @DeclarantName, 
			@DeclarantAddress, @AgentName, @AgentAddress, @ShippingAgentName, @ShippingAgentAddress, @CustomsStationCode, 
			@PlaceOfDischarge, @DateType, @PresentationDate, @GISIndicator1, @GISIndicator2, @ResponseCode, @ResponseOGACode, 
			@PackageType, @PackageCode, @PackageCount, @MarksAndNumbers, @OrderType, @RegistrationDate, @DeclarationNo, 
			@DeclarationRefNo, @VesselID, @VoyageNo, @FlightNo, @PTJCode, @WareHouseLicenceNo, @FileStatus, @IsProcessed, 
			@ProcessRemarks, @OrderNo

	Select @NewRegistrationNo = @ReferenceNo
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CUSERRHeader] Record Into [CUSERRHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderUpdate] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @EDIType smallint,
    @FileName nvarchar(100),
    @DocumentDate datetime,
    @SenderID nvarchar(35) = NULL,
    @ReceiverID nvarchar(35) = NULL,
    @MessageRefernceNo nvarchar(20) = NULL,
    @MessageType smallint = NULL,
    @MessageFunction tinyint = NULL,
    @ProcessIndicator nvarchar(3) = NULL,
    @StatusCode nvarchar(3) = NULL,
    @CustomsName nvarchar(50) = NULL,
    @CustomsAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @ShippingAgentAddress nvarchar(250) = NULL,
    @CustomsStationCode nvarchar(50) = NULL,
    @PlaceOfDischarge nvarchar(50) = NULL,
    @DateType smallint = NULL,
    @PresentationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @ResponseCode nvarchar(50) = NULL,
    @ResponseOGACode nvarchar(50) = NULL,
    @PackageType nvarchar(10) = NULL,
    @PackageCode nvarchar(10) = NULL,
    @PackageCount nvarchar(10) = NULL,
    @MarksAndNumbers nvarchar(200) = NULL,
    @OrderType smallint = NULL,
    @RegistrationDate datetime = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @DeclarationRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(20) = NULL,
    @VoyageNo nvarchar(10) = NULL,
    @FlightNo nvarchar(10) = NULL,
    @PTJCode nvarchar(10) = NULL,
    @WareHouseLicenceNo nvarchar(10) = NULL,
    @FileStatus bit = NULL,
    @IsProcessed bit = NULL,
    @ProcessRemarks nvarchar(100) = NULL,
    @OrderNo nvarchar(50) = NULL,
	@NewRegistrationNo uniqueIdentifier OUTPUT
AS 
 
	
BEGIN

	UPDATE	[EDI].[CUSERRHeader]
	SET		[BranchID] = @BranchID, [ReferenceNo] = @ReferenceNo, [EDIType] = @EDIType, [FileName] = @FileName, 
			[DocumentDate] = @DocumentDate, [SenderID] = @SenderID, [ReceiverID] = @ReceiverID, [MessageRefernceNo] = @MessageRefernceNo, 
			[MessageType] = @MessageType, [MessageFunction] = @MessageFunction, [ProcessIndicator] = @ProcessIndicator, 
			[StatusCode] = @StatusCode, [CustomsName] = @CustomsName, [CustomsAddress] = @CustomsAddress, 
			[DeclarantName] = @DeclarantName, [DeclarantAddress] = @DeclarantAddress, [AgentName] = @AgentName, 
			[AgentAddress] = @AgentAddress, [ShippingAgentName] = @ShippingAgentName, [ShippingAgentAddress] = @ShippingAgentAddress, 
			[CustomsStationCode] = @CustomsStationCode, [PlaceOfDischarge] = @PlaceOfDischarge, [DateType] = @DateType, 
			[PresentationDate] = @PresentationDate, [GISIndicator1] = @GISIndicator1, [GISIndicator2] = @GISIndicator2, 
			[ResponseCode] = @ResponseCode, [ResponseOGACode] = @ResponseOGACode, [PackageType] = @PackageType, 
			[PackageCode] = @PackageCode, [PackageCount] = @PackageCount, [MarksAndNumbers] = @MarksAndNumbers, 
			[OrderType] = @OrderType, [RegistrationDate] = @RegistrationDate, [DeclarationNo] = @DeclarationNo, 
			[DeclarationRefNo] = @DeclarationRefNo, [VesselID] = @VesselID, [VoyageNo] = @VoyageNo, 
			[FlightNo] = @FlightNo, [PTJCode] = @PTJCode, [WareHouseLicenceNo] = @WareHouseLicenceNo, 
			[FileStatus] = @FileStatus, [IsProcessed] = @IsProcessed, [ProcessRemarks] = @ProcessRemarks, 
			[OrderNo] = @OrderNo
	WHERE	[BranchID] = @BranchID
			AND [ReferenceNo] = @ReferenceNo
	
		Select @NewRegistrationNo = @ReferenceNo

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CUSERRHeader] Record Into [CUSERRHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderSave] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier,
    @EDIType smallint,
    @FileName nvarchar(100),
    @DocumentDate datetime,
    @SenderID nvarchar(35) = NULL,
    @ReceiverID nvarchar(35) = NULL,
    @MessageRefernceNo nvarchar(20) = NULL,
    @MessageType smallint = NULL,
    @MessageFunction tinyint = NULL,
    @ProcessIndicator nvarchar(3) = NULL,
    @StatusCode nvarchar(3) = NULL,
    @CustomsName nvarchar(50) = NULL,
    @CustomsAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @ShippingAgentAddress nvarchar(250) = NULL,
    @CustomsStationCode nvarchar(50) = NULL,
    @PlaceOfDischarge nvarchar(50) = NULL,
    @DateType smallint = NULL,
    @PresentationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @ResponseCode nvarchar(50) = NULL,
    @ResponseOGACode nvarchar(50) = NULL,
    @PackageType nvarchar(10) = NULL,
    @PackageCode nvarchar(10) = NULL,
    @PackageCount nvarchar(10) = NULL,
    @MarksAndNumbers nvarchar(200) = NULL,
    @OrderType smallint = NULL,
    @RegistrationDate datetime = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @DeclarationRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(20) = NULL,
    @VoyageNo nvarchar(10) = NULL,
    @FlightNo nvarchar(10) = NULL,
    @PTJCode nvarchar(10) = NULL,
    @WareHouseLicenceNo nvarchar(10) = NULL,
    @FileStatus bit = NULL,
    @IsProcessed bit = NULL,
    @ProcessRemarks nvarchar(100) = NULL,
    @OrderNo nvarchar(50) = NULL,
	@NewRegistrationNo uniqueIdentifier OUTPUT
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [EDI].[CUSERRHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSERRHeaderUpdate] 
			@BranchID, @ReferenceNo, @EDIType, @FileName, @DocumentDate, @SenderID, @ReceiverID, @MessageRefernceNo, 
			@MessageType, @MessageFunction, @ProcessIndicator, @StatusCode, @CustomsName, @CustomsAddress, 
			@DeclarantName, @DeclarantAddress, @AgentName, @AgentAddress, @ShippingAgentName, @ShippingAgentAddress, 
			@CustomsStationCode, @PlaceOfDischarge, @DateType, @PresentationDate, @GISIndicator1, @GISIndicator2, 
			@ResponseCode, @ResponseOGACode, @PackageType, @PackageCode, @PackageCount, @MarksAndNumbers, @OrderType, 
			@RegistrationDate, @DeclarationNo, @DeclarationRefNo, @VesselID, @VoyageNo, @FlightNo, @PTJCode, 
			@WareHouseLicenceNo, @FileStatus, @IsProcessed, @ProcessRemarks, @OrderNo , @NewRegistrationNo= @NewRegistrationNo OUTPUT


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSERRHeaderInsert] 
			@BranchID, @ReferenceNo, @EDIType, @FileName, @DocumentDate, @SenderID, @ReceiverID, @MessageRefernceNo, 
			@MessageType, @MessageFunction, @ProcessIndicator, @StatusCode, @CustomsName, @CustomsAddress, 
			@DeclarantName, @DeclarantAddress, @AgentName, @AgentAddress, @ShippingAgentName, @ShippingAgentAddress, 
			@CustomsStationCode, @PlaceOfDischarge, @DateType, @PresentationDate, @GISIndicator1, @GISIndicator2, 
			@ResponseCode, @ResponseOGACode, @PackageType, @PackageCode, @PackageCount, @MarksAndNumbers, @OrderType, 
			@RegistrationDate, @DeclarationNo, @DeclarationRefNo, @VesselID, @VoyageNo, @FlightNo, @PTJCode, 
			@WareHouseLicenceNo, @FileStatus, @IsProcessed, @ProcessRemarks, @OrderNo , @NewRegistrationNo= @NewRegistrationNo OUTPUT


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSERRHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSERRHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CUSERRHeader] Record  based on [CUSERRHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSERRHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSERRHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSERRHeaderDelete] 
    @BranchID bigint,
    @ReferenceNo uniqueidentifier
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CUSERRHeader]
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSERRHeaderDelete]
-- ========================================================================================================================================

GO 
