 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [K1Declaration] Record based on [K1Declaration] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_K1DeclarationSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationSelect] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationSelect] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN

	SELECT [BranchID], [ReferenceNo], [DeclarationNo],[MessageType],[CreateDate], [EDIDateTime], [FileName],[ErrorMessage] 
	FROM   [EDI].[K1Declaration]
	WHERE   [BranchID] = @BranchID  
	       AND [ReferenceNo] = @ReferenceNo 
	       AND [DeclarationNo] = @DeclarationNo 

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [K1Declaration] Records from [K1Declaration] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_K1DeclarationList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationList] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationList] 
	 
AS 
 
BEGIN
	SELECT [BranchID], [ReferenceNo], [DeclarationNo],[MessageType],[CreateDate], [EDIDateTime], [FileName] ,[ErrorMessage]
	FROM   [EDI].[K1Declaration]
	WHERE   EDIDateTime IS NULL

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [K1Declaration] Record Into [K1Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K1DeclarationInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationInsert] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationInsert] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL 
AS 
  

BEGIN
	
	INSERT INTO [EDI].[K1Declaration] ([BranchID], [DeclarationNo], [EDIDateTime], [FileName])
	SELECT @BranchID, @DeclarationNo, @EDIDateTime, @FileName
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [K1Declaration] Record Into [K1Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K1DeclarationUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationUpdate] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationUpdate] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL 
AS 
 
	
BEGIN

	UPDATE	[EDI].[K1Declaration]
	SET		[BranchID] = @BranchID, [DeclarationNo] = @DeclarationNo, [EDIDateTime] = @EDIDateTime, 
			[FileName] = @FileName,MessageType=@MessageType
	WHERE	[BranchID] = @BranchID
			AND [ReferenceNo] = @ReferenceNo
			AND [DeclarationNo] = @DeclarationNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [K1Declaration] Record Into [K1Declaration] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K1DeclarationSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationSave] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationSave] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50),
	@MessageType nvarchar(3),
	@CreateDate datetime = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(100) = NULL 
AS 
 

BEGIN

	Declare @refNo bigint = SCOPE_IDENTITY()

	IF (SELECT COUNT(0) FROM [EDI].[K1Declaration] 
		WHERE 	[BranchID] = @BranchID
	       AND [ReferenceNo] = @refNo
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [EDI].[usp_K1DeclarationUpdate] 
		@BranchID, @refNo,@DeclarationNo,@MessageType,@CreateDate, @EDIDateTime, @FileName 


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_K1DeclarationInsert] 
		@BranchID, @DeclarationNo,@MessageType,@CreateDate, @EDIDateTime, @FileName 


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[K1DeclarationSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [K1Declaration] Record  based on [K1Declaration]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K1DeclarationDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationDelete] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationDelete] 
    @BranchID bigint,
    @ReferenceNo bigint 
AS 

	
BEGIN
 
	DELETE
	FROM   [EDI].[K1Declaration]
	WHERE  [BranchID] = @BranchID
	       AND [ReferenceNo] = @ReferenceNo
	        
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationDelete]
-- ========================================================================================================================================

GO 

-- ========================================================================================================================================
-- START											 [EDI].[usp_K1DeclarationUpdateEDIDateTime]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_K1DeclarationUpdateEDIDateTime]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_K1DeclarationUpdateEDIDateTime] 
END 
GO
CREATE PROC [EDI].[usp_K1DeclarationUpdateEDIDateTime] 
    @BranchID bigint,
    @ReferenceNo bigint,
    @FileName nvarchar(100),
	@ErrorMessage nvarchar(max)
AS 
 
	
BEGIN
	IF LEN(@ErrorMessage)=0
	Begin
		UPDATE [EDI].[K1Declaration]
		SET    [EDIDateTime] = GetUTCDate(), [FileName] = @FileName
		WHERE  [BranchID] = @BranchID
			   AND ReferenceNo = @ReferenceNo
	End
	Else
	Begin
		UPDATE [EDI].[K1Declaration]
		SET    [EDIDateTime] = GetUTCDate(),[FileName] = @FileName,[ErrorMessage] = @ErrorMessage
		WHERE  [BranchID] = @BranchID
			   AND ReferenceNo = @ReferenceNo

	End

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_K1DeclarationUpdateEDIDateTime]
-- ========================================================================================================================================

GO


--ALTER TABLE [EDI].[K1Declaration]
--ADD [ErrorMessage] NVARCHAR(MAX) NULL
--go
