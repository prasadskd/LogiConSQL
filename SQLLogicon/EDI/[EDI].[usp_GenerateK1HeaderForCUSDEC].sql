
-- ========================================================================================================================================
-- START											 [EDI].[usp_GenerateK1HeaderForCUSDEC]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSDECHeader] Record based on [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_GenerateK1HeaderForCUSDEC]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GenerateK1HeaderForCUSDEC] 
END 
GO
CREATE PROC [EDI].[usp_GenerateK1HeaderForCUSDEC] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN

	;with	DeclarationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='DeclarationType'),
			TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
			ShipmentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ShipmentType'),
			TransactionTypeDescription As (Select LookupID,LookupCode,LookupDescription From Config.Lookup Where LookupCategory = 'TransactionTypeK1')
	SELECT	Hd.[BranchID], Hd.[DeclarationNo],EdiHd.MessageType As DocumentType,'K1D' As DocumentCode,LEFT(Hd.CustomStationCode,3) As CustomStationCode,LEFT(Hd.ShippingAgent,3) As 
	ShippingAgentCode,	Right(Convert(char(8),Getdate(),112),6) +  Replace(convert(char(5),getdate(),114),':','') As PreparationDateTime,
	'1' As TerminalCode,Left(Hd.DeclarationNo,10) As UserJobNo,EdiHd.MessageType, Inv.PayCountry ,
	Sh.LoadingPort,sh.DischargePort,sh.TranshipmentPort,Hd.[ImportDate],Sh.ETADate,
	Hd.[DeclarationDate], Hd.[DeclarationType], Hd.[OpenDate], 
						 Hd.[OrderNo], Hd.[TransportMode], Hd.[ShipmentType], Hd.[TransactionType], 
						Hd.[Importer], Hd.[Exporter],Hd.[ExporterAddress1],Hd.[ExporterAddress2],  Hd.[ExporterCity], 
						Hd.[ExporterState], Hd.[ExporterCountry], Hd.[ExporterOrganizationType], Hd.[ExporterTelNo], 
						Hd.[SMKCode], Hd.[CreatedBy], Hd.[CreatedOn],Hd.[ModifiedBy], Hd.[ModifiedOn],Hd.CustomStationCode,Hd.ShippingAgent,Hd.DeclarantID,
						Hd.DeclarantName,Hd.DeclarantNRIC,Hd.DeclarantDesignation,HD.DeclarantAddress1,Hd.DeclarantAddress2, Hd.DeclarantAddressCity, Hd.DeclarantAddressCountry, Hd.DeclarantAddressPostCode,Hd.DeclarantAddressState,
						Hd.[IsActive], Hd.[IsApproved],Hd.[ApprovedBy],Hd.[ApprovedOn],
						Sh.VesselID,sh.VesselName,sh.VoyageNo,
						ISNULL(LP.PortName,'''') As LoadingPortName,
						ISNULL(DP.PortName,'''') As DischargePortName,
						ISNULL(TP.PortName,'''') As TranshipmentPortName,
						ISNULL(DT.LookupDescription,'''') As DeclarationTypeDescription,
						ISNULL(TM.LookupDescription,'''') As TransportModeDescription,
						ISNULL(ST.LookupDescription,'''') As ShipmentTypeDescription,
						ISNULL(TR.LookupDescription,'''') As TransactionTypeDescription,
						Sh.ManifestNo,Sh.OceanBLNo,Sh.HouseBLNo,
						LT.MerchantName As ImporterName,
						SA.MerchantName As ShippingAgentName,
						TR.LookupCode As TransactionTypeCode 
	FROM	[Operation].[DeclarationHeader] Hd
	Left Outer Join Operation.DeclarationShipment Sh ON
		Hd.BranchID = Sh.BranchID
		And Hd.DeclarationNo = Sh.DeclarationNo
	Left Outer Join Operation.DeclarationInvoice Inv ON 
		Hd.BranchID = Inv.BranchID
		And Hd.DeclarationNo = Inv.DeclarationNo
	Left Outer Join Operation.VesselSchedule Vs ON 
		Sh.VesselScheduleID = Vs.VesselScheduleID
	Left Outer Join Master.Port LP ON
		Sh.LoadingPort = LP.PortCode
	Left Outer Join Master.Port DP ON
		Sh.DischargePort = DP.PortCode
	Left Outer Join Master.Port TP ON
		Sh.TranshipmentPort = TP.PortCode
	Left Outer Join DeclarationTypeDescription DT ON 
		Hd.DeclarationType = DT.LookupID
	Left Outer Join TransportModeDescription TM ON 
		Hd.TransportMode = TM.LookupID
	Left Outer Join ShipmentTypeDescription ST ON 
		Hd.ShipmentType = ST.LookupID
	Left Outer Join TransactionTypeDescription TR ON 
		Hd.TransactionType = TR.LookupID
	Left OUter JOin Master.Merchant LT ON 
		Hd.Importer = LT.MerchantCode
	Left OUter JOin Master.Merchant SA ON 
		Hd.ShippingAgent = SA.MerchantCode
	Left Outer Join [EDI].[K1Declaration] EdiHd On 
		Hd.BranchID = EdiHd.BranchID
		And Hd.DeclarationNo = EdiHd.DeclarationNo
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[DeclarationNo] = @DeclarationNo
			And EdiHd.EdiDateTime IS NULL  

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_GenerateK1HeaderForCUSDEC]
-- ========================================================================================================================================

GO