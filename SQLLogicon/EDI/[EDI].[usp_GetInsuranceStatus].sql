
-- ========================================================================================================================================
-- START											[EDI].[usp_GetInsuranceStatus]
-- ========================================================================================================================================
-- Author:			Sharma
-- Create date: 	03-May-2017
-- Description:		Gets the current EDI status of the order.
/*
Exec [EDI].[usp_GetInsuranceStatus] 'EHQ170500002'

*/

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_GetInsuranceStatus]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GetInsuranceStatus] 
END 
GO
CREATE PROC [EDI].[usp_GetInsuranceStatus]
@OrderNo nvarchar(50)
AS
BEGIN

--Select @OrderNo ='EHQ170500002'

Select Hd.InsuranceReferenceNo,Hd.OrderNo,Hd.MessageFunction,Hd.EDIDateTime,Hd.UUID,
Hd.FileName,St.StatusCode,St.ActivityCode,St.InsuranceTokenNo,Rs.ErrorMsg
From EDI.InsuranceHeader Hd
Left Outer Join Edi.InsuranceStatus St On 
Hd.InsuranceReferenceNo = St.InsuranceReferenceNo
Left Outer Join EDI.InsuranceResponse Rs On 
Hd.InsuranceReferenceNo = Rs.InsuranceRefNo
Where Hd.OrderNo =@OrderNo

END