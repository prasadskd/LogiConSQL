
-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSREPSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationHeader] Record based on [DeclarationHeader] table

-- Exec [EDI].[usp_CUSREPSearch] 1001001,NULL,NULL,NULL,NULL,NULL,NULL

-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSREPSearch]') IS NOT NULL
BEGIN 
    DROP PROC  [EDI].[usp_CUSREPSearch]
END 
GO
CREATE PROC  [EDI].[usp_CUSREPSearch] 
    @BranchID BIGINT,
    @VesselName NVARCHAR(100),
	@VoyageNo nvarchar(50),
	@PlaceOfArrival nvarchar(50), 
	@DischargePort nvarchar(50),
	@ETA datetime =NULL,
	@ETD DateTime =NULL
AS 

BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N'SELECT	[BranchID], [FileName],[VesselName],[VoyageNo],[ETA], [ETD], [DischargePort], [PlaceOfArrival]
	FROM	[EDI].[CUSREPHeader] Hd
	WHERE   Hd.[BranchID] = ' +  Convert(varchar(20),@BranchID)  

	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' AND  Hd.[VesselName] LIKE ''%' + @VesselName + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And Hd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@PlaceOfArrival)) > 0) set @sql = @sql + ' And Hd.[PlaceOfArrival] LIKE ''%' + @PlaceOfArrival  + '%'''
	if (len(rtrim(@DischargePort)) > 0) set @sql = @sql + ' And Hd.@DischargePort LIKE ''%' + @DischargePort  + '%'''

	if (ISNULL(@ETA,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ETA,120) >= ISNULL(''' + Convert(Char(10),@ETA,120) + ''',Hd.ETA)'
	if (ISNULL(@ETD,'') > 0) set @sql = @sql + ' And Convert(Char(10),Hd.ETD,120) <= ISNULL(''' + Convert(Char(10),@ETD,120) + ''',Hd.ETD)'

	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END
-- ========================================================================================================================================
-- END  											 [Operation].[usp_DeclarationSearch]
-- ========================================================================================================================================

GO

