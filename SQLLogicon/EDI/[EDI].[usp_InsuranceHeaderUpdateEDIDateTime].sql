
-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderUpdateEDIDateTime]
-- ========================================================================================================================================
-- Author:		Maruthi
-- Create date: 	01-Apr-2017
-- Description:	Either INSERT or UPDATE the [OrderHeader] Record Into [OrderHeader] Table.


/*
Modification History:

Modified By	:	sharma
Modified On	:	19-Apr-2017
Description :	Added a new parameter InsuranceTokenNo to store the Incoming Response number from the   Insurance EDI partner.

*/

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderUpdateEDIDateTime]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderUpdateEDIDateTime]
END 
GO

CREATE PROC [EDI].[usp_InsuranceHeaderUpdateEDIDateTime] 
    @BranchID bigint,
    @InsuranceReferenceNo bigint,
    @FileName nvarchar(50),
	@UUID nvarchar(100),
	@InsuranceTokenNo nvarchar(255)
AS 
 
	
BEGIN

	UPDATE [EDI].[InsuranceHeader]
	SET    [EDIDateTime] = GetUTCDate(), [FileName] = @FileName,UUID = @UUID,InsuranceTokenNo = @InsuranceTokenNo
	WHERE  [BranchID] = @BranchID
	       AND [InsuranceReferenceNo] = @InsuranceReferenceNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderUpdateEDIDateTime]
-- ========================================================================================================================================


