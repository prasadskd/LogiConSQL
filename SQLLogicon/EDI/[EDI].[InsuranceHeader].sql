 
-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [InsuranceHeader] Record based on [InsuranceHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_InsuranceHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderSelect] 
    @BranchID bigint,
    @InsuranceReferenceNo bigint
AS 
 

BEGIN

	SELECT	[BranchID], [InsuranceReferenceNo], [OrderNo],[CreateDateTime],[MessageFunction], 
			[EDIDateTime], [FileName],MessageID,UUID 
	FROM	[EDI].[InsuranceHeader]
	WHERE	[BranchID] = @BranchID  
			AND [InsuranceReferenceNo] = @InsuranceReferenceNo   

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [InsuranceHeader] Records from [InsuranceHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_InsuranceHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderList] 
    

AS 
 
BEGIN
	SELECT	[BranchID], [InsuranceReferenceNo], [OrderNo],[CreateDateTime],[MessageFunction], 
			[EDIDateTime], [FileName],MessageID,UUID
	FROM	[EDI].[InsuranceHeader]
	WHERE   EDIDateTime IS NULL

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderInsert] 
    @BranchID bigint,
    @OrderNo nvarchar(50) = NULL,
	@CreateDateTime datetime = NULL,
	@MessageFunction nvarchar(3) = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(50) = NULL
AS 
  

BEGIN
	
	INSERT INTO [EDI].[InsuranceHeader] ([BranchID], [OrderNo], [EDIDateTime], [FileName])
	SELECT @BranchID, @OrderNo, @EDIDateTime, @FileName 
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderUpdate] 
    @BranchID bigint,
    @InsuranceReferenceNo bigint,
    @OrderNo nvarchar(50) = NULL,
	@CreateDateTime datetime = NULL,
	@MessageFunction nvarchar(3) = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(50) = NULL
AS 
 
	
BEGIN

	UPDATE [EDI].[InsuranceHeader]
	SET    [BranchID] = @BranchID, [OrderNo] = @OrderNo, [EDIDateTime] = @EDIDateTime, [FileName] = @FileName
	WHERE  [BranchID] = @BranchID
	       AND [InsuranceReferenceNo] = @InsuranceReferenceNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderSave] 
    @BranchID bigint,
    @OrderNo nvarchar(50) = NULL,
	@CreateDateTime datetime = NULL,
	@MessageFunction nvarchar(3) = NULL,
    @EDIDateTime datetime = NULL,
    @FileName nvarchar(50) = NULL
AS 
 

BEGIN

Declare @ReferenceNo bigint = SCOPE_IDENTITY();

	IF (SELECT COUNT(0) FROM [EDI].[InsuranceHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [InsuranceReferenceNo] = @ReferenceNo)>0
	BEGIN
	    Exec [EDI].[usp_InsuranceHeaderUpdate] 
		@BranchID,@ReferenceNo, @OrderNo,@CreateDateTime,@MessageFunction, @EDIDateTime, @FileName


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_InsuranceHeaderInsert] 
		@BranchID,@OrderNo,@CreateDateTime,@MessageFunction, @EDIDateTime, @FileName



	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[InsuranceHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [InsuranceHeader] Record  based on [InsuranceHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderDelete] 
    @BranchID bigint,
    @InsuranceReferenceNo bigint
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[InsuranceHeader]
	WHERE  [BranchID] = @BranchID
	       AND [InsuranceReferenceNo] = @InsuranceReferenceNo
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderDelete]
-- ========================================================================================================================================

GO 



-- ========================================================================================================================================
-- START											 [EDI].[usp_InsuranceHeaderUpdateEDIDateTime]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [InsuranceHeader] Record Into [InsuranceHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_InsuranceHeaderUpdateEDIDateTime]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_InsuranceHeaderUpdateEDIDateTime] 
END 
GO
CREATE PROC [EDI].[usp_InsuranceHeaderUpdateEDIDateTime] 
    @BranchID bigint,
    @InsuranceReferenceNo bigint,
    @FileName nvarchar(50),
	@UUID nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [EDI].[InsuranceHeader]
	SET    [EDIDateTime] = GetUTCDate(), [FileName] = @FileName,UUID = @UUID
	WHERE  [BranchID] = @BranchID
	       AND [InsuranceReferenceNo] = @InsuranceReferenceNo
	

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_InsuranceHeaderUpdateEDIDateTime]
-- ========================================================================================================================================

GO
