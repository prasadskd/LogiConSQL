USE [LogiCon];
GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSACKHeader] Record based on [CUSACKHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSACKHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderSelect] 
    @BranchId bigint,
    @ReferenceNo uniqueidentifier
AS 
 

BEGIN

	SELECT	[BranchId], [ReferenceNo], [FileName], [DocumentDate], [SenderID], [RecieverID], [MessageType], [MessageName], 
			[MessageFunction], [AgentCode], [ProcessIndicator], [StatusCode], [CustomName], [CustomAddress], [DeclarantName], 
			[DeclarantAddress], [AgentName], [AgentAddress], [CustomStationCode], [PortOfDischarge], [DeclarationDate], 
			[GISIndicator1], [GISIndicator2], [GISTransactionType], [GISStatus], [NoOfPackages], [PackageID], [TaxType], 
			[TaxAmount], [DeclarationNo], [RegistrationDate], [ReceiptNo], [ReceiptDate], [DeclarantRefNo], [VesselID], 
			[VoyageNo], [FlightNo], [PTJCode], [ConsignmentNote1], [ConsignmentNote2], [ConsignmentNote3], [ContainerStatus], 
			[ContainerNo] 
	FROM	[EDI].[CUSACKHeader]
	WHERE  [BranchId] = @BranchId   
	       AND [ReferenceNo] = @ReferenceNo  

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CUSACKHeader] Records from [CUSACKHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CUSACKHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderList] 
    @BranchId bigint
AS 
 
BEGIN
	SELECT	[BranchId], [ReferenceNo], [FileName], [DocumentDate], [SenderID], [RecieverID], [MessageType], [MessageName], 
			[MessageFunction], [AgentCode], [ProcessIndicator], [StatusCode], [CustomName], [CustomAddress], [DeclarantName], 
			[DeclarantAddress], [AgentName], [AgentAddress], [CustomStationCode], [PortOfDischarge], [DeclarationDate], 
			[GISIndicator1], [GISIndicator2], [GISTransactionType], [GISStatus], [NoOfPackages], [PackageID], [TaxType], 
			[TaxAmount], [DeclarationNo], [RegistrationDate], [ReceiptNo], [ReceiptDate], [DeclarantRefNo], [VesselID], 
			[VoyageNo], [FlightNo], [PTJCode], [ConsignmentNote1], [ConsignmentNote2], [ConsignmentNote3], [ContainerStatus], 
			[ContainerNo] 
	FROM   [EDI].[CUSACKHeader]
		WHERE  [BranchId] = @BranchId   

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKHeaderList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CUSACKHeader] Record Into [CUSACKHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderInsert] 
    @BranchId bigint,
    @ReferenceNo uniqueidentifier,
    @FileName nvarchar(50) = NULL,
    @DocumentDate datetime = NULL,
    @SenderID nvarchar(50) = NULL,
    @RecieverID nvarchar(50) = NULL,
    @MessageType nvarchar(10) = NULL,
    @MessageName nvarchar(50) = NULL,
    @MessageFunction tinyint = NULL,
    @AgentCode nvarchar(50) = NULL,
    @ProcessIndicator nvarchar(10) = NULL,
    @StatusCode nvarchar(10) = NULL,
    @CustomName nvarchar(50) = NULL,
    @CustomAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @CustomStationCode nvarchar(50) = NULL,
    @PortOfDischarge nvarchar(50) = NULL,
    @DeclarationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @GISTransactionType nvarchar(5) = NULL,
    @GISStatus tinyint = NULL,
    @NoOfPackages nvarchar(50) = NULL,
    @PackageID nvarchar(50) = NULL,
    @TaxType nvarchar(50) = NULL,
    @TaxAmount nvarchar(50) = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @RegistrationDate datetime = NULL,
    @ReceiptNo nvarchar(50) = NULL,
    @ReceiptDate datetime = NULL,
    @DeclarantRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(50) = NULL,
    @VoyageNo nvarchar(50) = NULL,
    @FlightNo nvarchar(50) = NULL,
    @PTJCode nvarchar(50) = NULL,
    @ConsignmentNote1 nvarchar(50) = NULL,
    @ConsignmentNote2 nvarchar(50) = NULL,
    @ConsignmentNote3 nvarchar(50) = NULL,
    @ContainerStatus tinyint = NULL,
    @ContainerNo nvarchar(20) = NULL,
	@NewReferenceNo uniqueidentifier OUTPUT
AS 
  

BEGIN
	
	INSERT INTO [EDI].[CUSACKHeader] (
			[BranchId], [ReferenceNo], [FileName], [DocumentDate], [SenderID], [RecieverID], [MessageType], [MessageName], 
			[MessageFunction], [AgentCode], [ProcessIndicator], [StatusCode], [CustomName], [CustomAddress], [DeclarantName], 
			[DeclarantAddress], [AgentName], [AgentAddress], [CustomStationCode], [PortOfDischarge], [DeclarationDate], 
			[GISIndicator1], [GISIndicator2], [GISTransactionType], [GISStatus], [NoOfPackages], [PackageID], [TaxType], 
			[TaxAmount], [DeclarationNo], [RegistrationDate], [ReceiptNo], [ReceiptDate], [DeclarantRefNo], [VesselID], 
			[VoyageNo], [FlightNo], [PTJCode], [ConsignmentNote1], [ConsignmentNote2], [ConsignmentNote3], [ContainerStatus], 
			[ContainerNo])
	SELECT	@BranchId, @ReferenceNo, @FileName, @DocumentDate, @SenderID, @RecieverID, @MessageType, @MessageName, 
			@MessageFunction, @AgentCode, @ProcessIndicator, @StatusCode, @CustomName, @CustomAddress, @DeclarantName, 
			@DeclarantAddress, @AgentName, @AgentAddress, @CustomStationCode, @PortOfDischarge, @DeclarationDate, 
			@GISIndicator1, @GISIndicator2, @GISTransactionType, @GISStatus, @NoOfPackages, @PackageID, @TaxType, @TaxAmount, 
			@DeclarationNo, @RegistrationDate, @ReceiptNo, @ReceiptDate, @DeclarantRefNo, @VesselID, 
			@VoyageNo, @FlightNo, @PTJCode, @ConsignmentNote1, @ConsignmentNote2, @ConsignmentNote3, @ContainerStatus, 
			@ContainerNo

	Select @NewReferenceNo = @ReferenceNo
	
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CUSACKHeader] Record Into [CUSACKHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderUpdate] 
    @BranchId bigint,
    @ReferenceNo uniqueidentifier,
    @FileName nvarchar(50) = NULL,
    @DocumentDate datetime = NULL,
    @SenderID nvarchar(50) = NULL,
    @RecieverID nvarchar(50) = NULL,
    @MessageType nvarchar(10) = NULL,
    @MessageName nvarchar(50) = NULL,
    @MessageFunction tinyint = NULL,
    @AgentCode nvarchar(50) = NULL,
    @ProcessIndicator nvarchar(10) = NULL,
    @StatusCode nvarchar(10) = NULL,
    @CustomName nvarchar(50) = NULL,
    @CustomAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @CustomStationCode nvarchar(50) = NULL,
    @PortOfDischarge nvarchar(50) = NULL,
    @DeclarationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @GISTransactionType nvarchar(5) = NULL,
    @GISStatus tinyint = NULL,
    @NoOfPackages nvarchar(50) = NULL,
    @PackageID nvarchar(50) = NULL,
    @TaxType nvarchar(50) = NULL,
    @TaxAmount nvarchar(50) = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @RegistrationDate datetime = NULL,
    @ReceiptNo nvarchar(50) = NULL,
    @ReceiptDate datetime = NULL,
    @DeclarantRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(50) = NULL,
    @VoyageNo nvarchar(50) = NULL,
    @FlightNo nvarchar(50) = NULL,
    @PTJCode nvarchar(50) = NULL,
    @ConsignmentNote1 nvarchar(50) = NULL,
    @ConsignmentNote2 nvarchar(50) = NULL,
    @ConsignmentNote3 nvarchar(50) = NULL,
    @ContainerStatus tinyint = NULL,
    @ContainerNo nvarchar(20) = NULL,
	@NewReferenceNo uniqueidentifier OUTPUT
AS 
 
	
BEGIN

	UPDATE [EDI].[CUSACKHeader]
	SET    [BranchId] = @BranchId, [ReferenceNo] = @ReferenceNo, [FileName] = @FileName, [DocumentDate] = @DocumentDate, 
			[SenderID] = @SenderID, [RecieverID] = @RecieverID, [MessageType] = @MessageType, [MessageName] = @MessageName, 
			[MessageFunction] = @MessageFunction, [AgentCode] = @AgentCode, [ProcessIndicator] = @ProcessIndicator, 
			[StatusCode] = @StatusCode, [CustomName] = @CustomName, [CustomAddress] = @CustomAddress, 
			[DeclarantName] = @DeclarantName, [DeclarantAddress] = @DeclarantAddress, [AgentName] = @AgentName, 
			[AgentAddress] = @AgentAddress, [CustomStationCode] = @CustomStationCode, [PortOfDischarge] = @PortOfDischarge, 
			[DeclarationDate] = @DeclarationDate, [GISIndicator1] = @GISIndicator1, [GISIndicator2] = @GISIndicator2, 
			[GISTransactionType] = @GISTransactionType, [GISStatus] = @GISStatus, [NoOfPackages] = @NoOfPackages, 
			[PackageID] = @PackageID, [TaxType] = @TaxType, [TaxAmount] = @TaxAmount, [DeclarationNo] = @DeclarationNo, 
			[RegistrationDate] = @RegistrationDate, [ReceiptNo] = @ReceiptNo, [ReceiptDate] = @ReceiptDate, 
			[DeclarantRefNo] = @DeclarantRefNo, [VesselID] = @VesselID, [VoyageNo] = @VoyageNo, [FlightNo] = @FlightNo, 
			[PTJCode] = @PTJCode, [ConsignmentNote1] = @ConsignmentNote1, [ConsignmentNote2] = @ConsignmentNote2, 
			[ConsignmentNote3] = @ConsignmentNote3, [ContainerStatus] = @ContainerStatus, [ContainerNo] = @ContainerNo
	WHERE  [BranchId] = @BranchId
	       AND [ReferenceNo] = @ReferenceNo
	
		Select @NewReferenceNo = @ReferenceNo

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKHeaderUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CUSACKHeader] Record Into [CUSACKHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderSave] 
    @BranchId bigint,
    @ReferenceNo uniqueidentifier,
    @FileName nvarchar(50) = NULL,
    @DocumentDate datetime = NULL,
    @SenderID nvarchar(50) = NULL,
    @RecieverID nvarchar(50) = NULL,
    @MessageType nvarchar(10) = NULL,
    @MessageName nvarchar(50) = NULL,
    @MessageFunction tinyint = NULL,
    @AgentCode nvarchar(50) = NULL,
    @ProcessIndicator nvarchar(10) = NULL,
    @StatusCode nvarchar(10) = NULL,
    @CustomName nvarchar(50) = NULL,
    @CustomAddress nvarchar(250) = NULL,
    @DeclarantName nvarchar(50) = NULL,
    @DeclarantAddress nvarchar(250) = NULL,
    @AgentName nvarchar(50) = NULL,
    @AgentAddress nvarchar(250) = NULL,
    @CustomStationCode nvarchar(50) = NULL,
    @PortOfDischarge nvarchar(50) = NULL,
    @DeclarationDate datetime = NULL,
    @GISIndicator1 tinyint = NULL,
    @GISIndicator2 tinyint = NULL,
    @GISTransactionType nvarchar(5) = NULL,
    @GISStatus tinyint = NULL,
    @NoOfPackages nvarchar(50) = NULL,
    @PackageID nvarchar(50) = NULL,
    @TaxType nvarchar(50) = NULL,
    @TaxAmount nvarchar(50) = NULL,
    @DeclarationNo nvarchar(50) = NULL,
    @RegistrationDate datetime = NULL,
    @ReceiptNo nvarchar(50) = NULL,
    @ReceiptDate datetime = NULL,
    @DeclarantRefNo nvarchar(50) = NULL,
    @VesselID nvarchar(50) = NULL,
    @VoyageNo nvarchar(50) = NULL,
    @FlightNo nvarchar(50) = NULL,
    @PTJCode nvarchar(50) = NULL,
    @ConsignmentNote1 nvarchar(50) = NULL,
    @ConsignmentNote2 nvarchar(50) = NULL,
    @ConsignmentNote3 nvarchar(50) = NULL,
    @ContainerStatus tinyint = NULL,
    @ContainerNo nvarchar(20) = NULL,
	@NewReferenceNo uniqueidentifier OUTPUT
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [EDI].[CUSACKHeader] 
		WHERE 	[BranchId] = @BranchId
	       AND [ReferenceNo] = @ReferenceNo)>0
	BEGIN
	    Exec [EDI].[usp_CUSACKHeaderUpdate] 
		@BranchId, @ReferenceNo, @FileName, @DocumentDate, @SenderID, @RecieverID, @MessageType, @MessageName, @MessageFunction, 
		@AgentCode, @ProcessIndicator, @StatusCode, @CustomName, @CustomAddress, @DeclarantName, @DeclarantAddress, @AgentName, 
		@AgentAddress, @CustomStationCode, @PortOfDischarge, @DeclarationDate, @GISIndicator1, @GISIndicator2, @GISTransactionType, 
		@GISStatus, @NoOfPackages, @PackageID, @TaxType, @TaxAmount, @DeclarationNo, @RegistrationDate, @ReceiptNo, @ReceiptDate, 
		@DeclarantRefNo, @VesselID, @VoyageNo, @FlightNo, @PTJCode, @ConsignmentNote1, @ConsignmentNote2, @ConsignmentNote3, 
		@ContainerStatus, @ContainerNo,@NewReferenceNo = @NewReferenceNo OUTPUT


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CUSACKHeaderInsert] 
		@BranchId, @ReferenceNo, @FileName, @DocumentDate, @SenderID, @RecieverID, @MessageType, @MessageName, @MessageFunction, 
		@AgentCode, @ProcessIndicator, @StatusCode, @CustomName, @CustomAddress, @DeclarantName, @DeclarantAddress, @AgentName, 
		@AgentAddress, @CustomStationCode, @PortOfDischarge, @DeclarationDate, @GISIndicator1, @GISIndicator2, @GISTransactionType, 
		@GISStatus, @NoOfPackages, @PackageID, @TaxType, @TaxAmount, @DeclarationNo, @RegistrationDate, @ReceiptNo, @ReceiptDate, 
		@DeclarantRefNo, @VesselID, @VoyageNo, @FlightNo, @PTJCode, @ConsignmentNote1, @ConsignmentNote2, @ConsignmentNote3, 
		@ContainerStatus, @ContainerNo,@NewReferenceNo = @NewReferenceNo OUTPUT


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CUSACKHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CUSACKHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CUSACKHeader] Record  based on [CUSACKHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CUSACKHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CUSACKHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_CUSACKHeaderDelete] 
    @BranchId nvarchar(50),
    @ReferenceNo uniqueidentifier
AS 

	
BEGIN

	 
	DELETE
	FROM   [EDI].[CUSACKHeader]
	WHERE  [BranchId] = @BranchId
	       AND [ReferenceNo] = @ReferenceNo
	 
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CUSACKHeaderDelete]
-- ========================================================================================================================================

GO 
