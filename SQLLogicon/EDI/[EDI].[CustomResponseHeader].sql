
-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomResponseHeader] Record based on [CustomResponseHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderSelect] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderSelect] 
    @BranchID BIGINT,
    @ResponseReferenceNo NVARCHAR(35),
    @DeclarationNo VARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [ResponseReferenceNo], [DeclarationNo], [RegistrationNo], [RegistrationDate], [Remark], [Code], [SNRF], [Status], [OfficerID], [OfficeName] 
	FROM   [EDI].[CustomResponseHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [ResponseReferenceNo] = @ResponseReferenceNo  
	       AND [DeclarationNo] = @DeclarationNo  
END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomResponseHeader] Records from [CustomResponseHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderList] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderList] 
    @BranchID BIGINT,
	@DeclarationNo VARCHAR(50)
AS 

BEGIN

	SELECT [BranchID], [ResponseReferenceNo], [DeclarationNo], [RegistrationNo], [RegistrationDate], [Remark], [Code], [SNRF], [Status], [OfficerID], [OfficeName] 
	FROM   [EDI].[CustomResponseHeader]
	WHERE  [BranchID] = @BranchID  
	       AND [DeclarationNo] = @DeclarationNo  
	        

END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseHeaderList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CustomResponseHeader] Record Into [CustomResponseHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderInsert] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderInsert] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @RegistrationNo nvarchar(50),
    @RegistrationDate datetime,
    @Remark nvarchar(255),
    @Code nvarchar(50),
    @SNRF nvarchar(50),
    @Status nvarchar(50),
    @OfficerID nvarchar(50),
    @OfficeName nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [EDI].[CustomResponseHeader] ([BranchID], [ResponseReferenceNo], [DeclarationNo], [RegistrationNo], [RegistrationDate], [Remark], [Code], [SNRF], [Status], [OfficerID], [OfficeName])
	SELECT @BranchID, @ResponseReferenceNo, @DeclarationNo, @RegistrationNo, @RegistrationDate, @Remark, @Code, @SNRF, @Status, @OfficerID, @OfficeName
               
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CustomResponseHeader] Record Into [CustomResponseHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderUpdate] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderUpdate] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @RegistrationNo nvarchar(50),
    @RegistrationDate datetime,
    @Remark nvarchar(255),
    @Code nvarchar(50),
    @SNRF nvarchar(50),
    @Status nvarchar(50),
    @OfficerID nvarchar(50),
    @OfficeName nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [EDI].[CustomResponseHeader]
	SET    [BranchID] = @BranchID, [ResponseReferenceNo] = @ResponseReferenceNo, [DeclarationNo] = @DeclarationNo, [RegistrationNo] = @RegistrationNo, [RegistrationDate] = @RegistrationDate, [Remark] = @Remark, [Code] = @Code, [SNRF] = @SNRF, [Status] = @Status, [OfficerID] = @OfficerID, [OfficeName] = @OfficeName
	WHERE  [BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	
END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CustomResponseHeader] Record Into [CustomResponseHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderSave] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderSave] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50),
    @RegistrationNo nvarchar(50),
    @RegistrationDate datetime,
    @Remark nvarchar(255),
    @Code nvarchar(50),
    @SNRF nvarchar(50),
    @Status nvarchar(50),
    @OfficerID nvarchar(50),
    @OfficeName nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [EDI].[CustomResponseHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo)>0
	BEGIN
	    Exec [EDI].[usp_CustomResponseHeaderUpdate] 
		@BranchID, @ResponseReferenceNo, @DeclarationNo, @RegistrationNo, @RegistrationDate, @Remark, @Code, @SNRF, @Status, @OfficerID, @OfficeName


	END
	ELSE
	BEGIN
	    Exec [EDI].[usp_CustomResponseHeaderInsert] 
		@BranchID, @ResponseReferenceNo, @DeclarationNo, @RegistrationNo, @RegistrationDate, @Remark, @Code, @SNRF, @Status, @OfficerID, @OfficeName


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [EDI].usp_[CustomResponseHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [EDI].[usp_CustomResponseHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CustomResponseHeader] Record  based on [CustomResponseHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[EDI].[usp_CustomResponseHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_CustomResponseHeaderDelete] 
END 
GO
CREATE PROC [EDI].[usp_CustomResponseHeaderDelete] 
    @BranchID bigint,
    @ResponseReferenceNo nvarchar(35),
    @DeclarationNo varchar(50)
AS 

	
BEGIN

 

	 
	DELETE
	FROM   [EDI].[CustomResponseHeader]
	WHERE  [BranchID] = @BranchID
	       AND [ResponseReferenceNo] = @ResponseReferenceNo
	       AND [DeclarationNo] = @DeclarationNo
	 


END

-- ========================================================================================================================================
-- END  											 [EDI].[usp_CustomResponseHeaderDelete]
-- ========================================================================================================================================

GO
 