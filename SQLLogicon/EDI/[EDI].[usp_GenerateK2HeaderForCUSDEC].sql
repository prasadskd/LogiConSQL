
-- ========================================================================================================================================
-- START											 [EDI].[usp_GenerateK2HeaderForCUSDEC]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CUSDECHeader] Record based on [CUSDECHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[EDI].[usp_GenerateK2HeaderForCUSDEC]') IS NOT NULL
BEGIN 
    DROP PROC [EDI].[usp_GenerateK2HeaderForCUSDEC] 
END 
GO
CREATE PROC [EDI].[usp_GenerateK2HeaderForCUSDEC] 
    @BranchID bigint,
    @DeclarationNo nvarchar(50)
AS 
 

BEGIN

;With DeclarationShipmentTypeIndicator As (Select LookupID,LookupCode From Config.Lookup where LookupCategory ='DeclarationShipmentType'),
TransactionTypeK2 As (Select LookupID,LookupCode From Config.Lookup where LookupCategory ='TransactionTypeK2')
SELECT Hd.BranchID,Hd.DeclarationNo,Hd.CustomStationCode,Sh.PortOperator,Inv.PayCountry,Sh.LoadingPort,Sh.DischargePort,Sh.TranshipmentPort,
Convert(Char(4),DATEADD(HOUR,8,GETUTCDATE()),120 ) As ProcessingDate, CONVERT(VARCHAR(10),Hd.DeclarationDate,112) As DeclarationDate,
ISNULL(DST.LookupCode,'') As DeclarationShipmentType
FROM Operation.DeclarationHeaderK2 Hd 
INNER JOIN Operation.DeclarationInvoiceK2 Inv ON 
	Hd.BranchID = Inv.BranchID 
	AND Hd.DeclarationNo = Inv.DeclarationNo 
INNER JOIN Operation.DeclarationShipmentK2 Sh ON 
Hd.BranchID = Sh.BranchID 
AND Hd.DeclarationNo = Sh.DeclarationNo
Left Outer Join DeclarationShipmentTypeIndicator DST ON 
	Hd.DeclarationShipmentType = DST.LookupID
Left Outer Join TransactionTypeK2 K2T ON 
	Hd.TransactionType = K2T.LookupID

	--WHERE	Hd.[BranchID] = @BranchID  
	--		AND Hd.[DeclarationNo] = @DeclarationNo
	--		And EdiHd.EdiDateTime IS NULL  

END
-- ========================================================================================================================================
-- END  											 [EDI].[usp_GenerateK2HeaderForCUSDEC]
-- ========================================================================================================================================

GO


 