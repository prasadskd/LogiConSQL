

-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PostalCodes] Record based on [PostalCodes] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PostalCodesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesSelect] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesSelect] 
    @PostalCode NVARCHAR(20),
    @StateID INT
AS 

BEGIN

	
	SELECT	Pc.[PostalCode], Pc.[StateID], Pc.[PlaceName], Pc.[Latitude], Pc.[Longitude], Pc.[IsActive], 
			Pc.[CreatedBy], Pc.[CreatedOn], Pc.[ModifiedBy], Pc.[ModifiedOn] ,
			ISNULL(Cs.StateName,'') As StateName,
			ISNULL(C.CountryName,'') As CountryName
	FROM	[Master].[PostalCodes] Pc
	Left Outer Join [Master].[CountryStates] Cs On 
		Pc.StateID = Cs.StateID
	Left Outer Join [Master].[Country] C ON 
		Cs.CountryCode = C.CountryCode
	WHERE  Pc.[PostalCode] = @PostalCode  
	       AND Pc.[StateID] = @StateID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PostalCodes] Records from [PostalCodes] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PostalCodesList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesList] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesList] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT	Pc.[PostalCode], Pc.[StateID], Pc.[PlaceName], Pc.[Latitude], Pc.[Longitude], Pc.[IsActive], 
			Pc.[CreatedBy], Pc.[CreatedOn], Pc.[ModifiedBy], Pc.[ModifiedOn] ,
			ISNULL(Cs.StateName,'') As StateName,
			ISNULL(C.CountryName,'') As CountryName
	FROM	[Master].[PostalCodes] Pc
	Left Outer Join [Master].[CountryStates] Cs On 
		Pc.StateID = Cs.StateID
	Left Outer Join [Master].[Country] C ON 
		Cs.CountryCode = C.CountryCode
	Where C.CountryCode = @CountryCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PostalCodes] Records from [PostalCodes] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PostalCodesPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesPageView] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesPageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT	Pc.[PostalCode], Pc.[StateID], Pc.[PlaceName], Pc.[Latitude], Pc.[Longitude], Pc.[IsActive], 
			Pc.[CreatedBy], Pc.[CreatedOn], Pc.[ModifiedBy], Pc.[ModifiedOn] ,
			ISNULL(Cs.StateName,'') As StateName,
			ISNULL(C.CountryName,'') As CountryName
	FROM	[Master].[PostalCodes] Pc
	Left Outer Join [Master].[CountryStates] Cs On 
		Pc.StateID = Cs.StateID
	Left Outer Join [Master].[Country] C ON 
		Cs.CountryCode = C.CountryCode
	Where C.CountryCode = @CountryCode
	ORDER BY Pc.[PostalCode] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [PostalCodes] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PostalCodesRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesRecordCount] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesRecordCount] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT	Count(0)
	FROM	[Master].[PostalCodes] Pc
	Left Outer Join [Master].[CountryStates] Cs On 
		Pc.StateID = Cs.StateID
	Left Outer Join [Master].[Country] C ON 
		Cs.CountryCode = C.CountryCode
	Where C.CountryCode = @CountryCode

	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PostalCodes] Record based on [PostalCodes] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PostalCodesAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesAutoCompleteSearch] 
    @PostalCode NVARCHAR(20),
    @StateID INT
AS 

BEGIN

	SELECT	Pc.[PostalCode], Pc.[StateID], Pc.[PlaceName], Pc.[Latitude], Pc.[Longitude], Pc.[IsActive], 
			Pc.[CreatedBy], Pc.[CreatedOn], Pc.[ModifiedBy], Pc.[ModifiedOn] ,
			ISNULL(Cs.StateName,'') As StateName,
			ISNULL(C.CountryName,'') As CountryName
	FROM	[Master].[PostalCodes] Pc
	Left Outer Join [Master].[CountryStates] Cs On 
		Pc.StateID = Cs.StateID
	Left Outer Join [Master].[Country] C ON 
		Cs.CountryCode = C.CountryCode
	WHERE  Pc.[PostalCode] Like '%' +  @PostalCode + '%'
	       
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [PostalCodes] Record Into [PostalCodes] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PostalCodesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesInsert] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesInsert] 
    @PostalCode nvarchar(20),
    @StateID int,
    @PlaceName nvarchar(100),
    @Latitude decimal(18, 10),
    @Longitude decimal(18, 10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
  

BEGIN

	
	INSERT INTO [Master].[PostalCodes] (
			[PostalCode], [StateID], [PlaceName], [Latitude], [Longitude], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@PostalCode, @StateID, @PlaceName, @Latitude, @Longitude, Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [PostalCodes] Record Into [PostalCodes] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PostalCodesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesUpdate] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesUpdate] 
    @PostalCode nvarchar(20),
    @StateID int,
    @PlaceName nvarchar(100),
    @Latitude decimal(18, 10),
    @Longitude decimal(18, 10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 
	
BEGIN

	UPDATE [Master].[PostalCodes]
	SET    [PlaceName] = @PlaceName, [Latitude] = @Latitude, [Longitude] = @Longitude, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [PostalCode] = @PostalCode
	       AND [StateID] = @StateID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [PostalCodes] Record Into [PostalCodes] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PostalCodesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesSave] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesSave] 
    @PostalCode nvarchar(20),
    @StateID int,
    @PlaceName nvarchar(100),
    @Latitude decimal(18, 10),
    @Longitude decimal(18, 10),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[PostalCodes] 
		WHERE 	[PostalCode] = @PostalCode
	       AND [StateID] = @StateID)>0
	BEGIN
	    Exec [Master].[usp_PostalCodesUpdate] 
		@PostalCode, @StateID, @PlaceName, @Latitude, @Longitude, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_PostalCodesInsert] 
		@PostalCode, @StateID, @PlaceName, @Latitude, @Longitude, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[PostalCodesSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PostalCodesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [PostalCodes] Record  based on [PostalCodes]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PostalCodesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PostalCodesDelete] 
END 
GO
CREATE PROC [Master].[usp_PostalCodesDelete] 
    @PostalCode nvarchar(20),
    @StateID int,
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[PostalCodes]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy= @ModifiedBy, ModifiedOn = GETUTCDATE()
	WHERE 	[PostalCode] = @PostalCode
	       AND [StateID] = @StateID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PostalCodesDelete]
-- ========================================================================================================================================

GO
 