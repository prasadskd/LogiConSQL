USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AddressSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Address] Record based on [Address] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AddressSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressSelect] 
END 
GO
CREATE PROC [Master].[usp_AddressSelect] 
    @LinkId nvarchar(50)
AS 
 

BEGIN

	SELECT	[AddressId], [LinkId], [SeqNo], [AddressType], [Address1], [Address2], [Address3], [Address4], [City], [State], 
			[CountryCode], [ZipCode], [TelNo], [FaxNo], [MobileNo], [Email], [WebSite], [HeadOfficeCode], [AreaCode], [ZoneCode], 
			[IsBilling], [IsPickup], [IsOperation], [IsActive], [Distance], [TravelTime], [RequireEDI], [EdiDateTime], 
			[EDIMapping], [DeliveryWindow], [WaitingTime], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[Address]
	WHERE	LinkId= @LinkId  

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_AddressList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Address] Records from [Address] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AddressList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressList] 
END 
GO
CREATE PROC [Master].[usp_AddressList] 

AS 
 
BEGIN
	SELECT	[AddressId], [LinkId], [SeqNo], [AddressType], [Address1], [Address2], [Address3], [Address4], [City], [State], 
			[CountryCode], [ZipCode], [TelNo], [FaxNo], [MobileNo], [Email], [WebSite], [HeadOfficeCode], [AreaCode], [ZoneCode], 
			[IsBilling], [IsPickup], [IsOperation], [IsActive], [Distance], [TravelTime], [RequireEDI], [EdiDateTime], 
			[EDIMapping], [DeliveryWindow], [WaitingTime], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[Address]
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AddressListByID]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Address] Records from [Address] table based on LinkID and AddressType
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AddressListByID]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressListByID] 
END 
GO
CREATE PROC [Master].[usp_AddressListByID] 
	@LinkId nvarchar(50),
	@AddressType varchar(50)
AS 
 
BEGIN
	SELECT	[AddressId], [LinkId], [SeqNo], [AddressType], [Address1], [Address2], [Address3], [Address4], [City], [State], 
			[CountryCode], [ZipCode], [TelNo], [FaxNo], [MobileNo], [Email], [WebSite], [HeadOfficeCode], [AreaCode], [ZoneCode], 
			[IsBilling], [IsPickup], [IsOperation], [IsActive], [Distance], [TravelTime], [RequireEDI], [EdiDateTime], 
			[EDIMapping], [DeliveryWindow], [WaitingTime], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[Address]
	Where	[LinkId] = @LinkId
			And AddressType = @AddressType
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_AddressInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Address] Record Into [Address] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AddressInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressInsert] 
END 
GO
CREATE PROC [Master].[usp_AddressInsert] 
	@AddressId int,
    @LinkId nvarchar(50),
    @SeqNo smallint,
    @AddressType varchar(50),
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @Address4 nvarchar(255),
    @City nvarchar(50),
    @State nvarchar(50),
    @CountryCode varchar(2),
    @ZipCode varchar(10),
    @TelNo nvarchar(50),
    @FaxNo nvarchar(50),
    @MobileNo nvarchar(50),
    @Email nvarchar(100),
    @WebSite nvarchar(50),
    @HeadOfficeCode nvarchar(50),
    @AreaCode nvarchar(10),
    @ZoneCode nvarchar(10),
    @IsBilling bit,
    @IsPickup bit,
    @IsOperation bit,
    @IsActive bit,
    @Distance numeric(9, 2),
    @TravelTime numeric(9, 2),
    @RequireEDI bit,
    @EdiDateTime datetime,
    @EDIMapping nvarchar(50),
    @DeliveryWindow varchar(50),
    @WaitingTime numeric(9, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN
	
	INSERT INTO [Master].[Address] (
			[LinkId], [SeqNo], [AddressType], [Address1], [Address2], [Address3], [Address4], [City], [State], [CountryCode], [ZipCode], 
			[TelNo], [FaxNo], [MobileNo], [Email], [WebSite], [HeadOfficeCode], [AreaCode], [ZoneCode], [IsBilling], [IsPickup], 
			[IsOperation], [IsActive], [Distance], [TravelTime], [RequireEDI], [EdiDateTime], [EDIMapping], [DeliveryWindow], 
			[WaitingTime], [CreatedBy], [CreatedOn])
	SELECT	@LinkId, @SeqNo, @AddressType, @Address1, @Address2, @Address3, @Address4, @City, @State, @CountryCode, @ZipCode, 
			@TelNo, @FaxNo, @MobileNo, @Email, @WebSite, @HeadOfficeCode, @AreaCode, @ZoneCode, @IsBilling, @IsPickup, 
			@IsOperation, @IsActive, @Distance, @TravelTime, @RequireEDI, @EdiDateTime, @EDIMapping, @DeliveryWindow, 
			@WaitingTime, @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_AddressUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Address] Record Into [Address] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AddressUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressUpdate] 
END 
GO
CREATE PROC [Master].[usp_AddressUpdate] 
    @AddressId int,
    @LinkId nvarchar(50),
    @SeqNo smallint,
    @AddressType varchar(50),
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @Address4 nvarchar(255),
    @City nvarchar(50),
    @State nvarchar(50),
    @CountryCode varchar(2),
    @ZipCode varchar(10),
    @TelNo nvarchar(50),
    @FaxNo nvarchar(50),
    @MobileNo nvarchar(50),
    @Email nvarchar(100),
    @WebSite nvarchar(50),
    @HeadOfficeCode nvarchar(50),
    @AreaCode nvarchar(10),
    @ZoneCode nvarchar(10),
    @IsBilling bit,
    @IsPickup bit,
    @IsOperation bit,
    @IsActive bit,
    @Distance numeric(9, 2),
    @TravelTime numeric(9, 2),
    @RequireEDI bit,
    @EdiDateTime datetime,
    @EDIMapping nvarchar(50),
    @DeliveryWindow varchar(50),
    @WaitingTime numeric(9, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Address]
	SET		[LinkId] = @LinkId, [SeqNo] = @SeqNo, [AddressType] = @AddressType, [Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, 
			[Address4] = @Address4, [City] = @City, [State] = @State, [CountryCode] = @CountryCode, [ZipCode] = @ZipCode, [TelNo] = @TelNo, 
			[FaxNo] = @FaxNo, [MobileNo] = @MobileNo, [Email] = @Email, [WebSite] = @WebSite, [HeadOfficeCode] = @HeadOfficeCode, [AreaCode] = @AreaCode, 
			[ZoneCode] = @ZoneCode, [IsBilling] = @IsBilling, [IsPickup] = @IsPickup, [IsOperation] = @IsOperation, [IsActive] = @IsActive, 
			[Distance] = @Distance, [TravelTime] = @TravelTime, [RequireEDI] = @RequireEDI, [EdiDateTime] = @EdiDateTime, [EDIMapping] = @EDIMapping, 
			[DeliveryWindow] = @DeliveryWindow, [WaitingTime] = @WaitingTime, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[AddressId] = @AddressId
			
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AddressSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Address] Record Into [Address] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AddressSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressSave] 
END 
GO
CREATE PROC [Master].[usp_AddressSave] 
	@AddressId int,
    @LinkId nvarchar(50),
    @SeqNo smallint,
    @AddressType varchar(50),
    @Address1 nvarchar(255),
    @Address2 nvarchar(255),
    @Address3 nvarchar(255),
    @Address4 nvarchar(255),
    @City nvarchar(50),
    @State nvarchar(50),
    @CountryCode varchar(2),
    @ZipCode varchar(10),
    @TelNo nvarchar(50),
    @FaxNo nvarchar(50),
    @MobileNo nvarchar(50),
    @Email nvarchar(100),
    @WebSite nvarchar(50),
    @HeadOfficeCode nvarchar(50),
    @AreaCode nvarchar(10),
    @ZoneCode nvarchar(10),
    @IsBilling bit,
    @IsPickup bit,
    @IsOperation bit,
    @IsActive bit,
    @Distance numeric(9, 2),
    @TravelTime numeric(9, 2),
    @RequireEDI bit,
    @EdiDateTime datetime,
    @EDIMapping nvarchar(50),
    @DeliveryWindow varchar(50),
    @WaitingTime numeric(9, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[Address] 
		WHERE 	LinkId = @LinkId And AddressType=@AddressType)>0
	BEGIN
	    Exec [Master].[usp_AddressUpdate] 
			@AddressId,@LinkId, @SeqNo, @AddressType, @Address1, @Address2, @Address3, @Address4, @City, @State, @CountryCode, @ZipCode, @TelNo, @FaxNo, 
			@MobileNo, @Email, @WebSite, @HeadOfficeCode, @AreaCode, @ZoneCode, @IsBilling, @IsPickup, @IsOperation, @IsActive, @Distance, 
			@TravelTime, @RequireEDI, @EdiDateTime, @EDIMapping, @DeliveryWindow, @WaitingTime, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_AddressInsert] 
			@AddressId,@LinkId, @SeqNo, @AddressType, @Address1, @Address2, @Address3, @Address4, @City, @State, @CountryCode, @ZipCode, @TelNo, @FaxNo, 
			@MobileNo, @Email, @WebSite, @HeadOfficeCode, @AreaCode, @ZoneCode, @IsBilling, @IsPickup, @IsOperation, @IsActive, @Distance, 
			@TravelTime, @RequireEDI, @EdiDateTime, @EDIMapping, @DeliveryWindow, @WaitingTime, @CreatedBy, @ModifiedBy 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[AddressSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AddressDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Address] Record  based on [Address]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AddressDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AddressDelete] 
END 
GO
CREATE PROC [Master].[usp_AddressDelete] 
    @AddressId int
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[Address]
	WHERE  [AddressId] = @AddressId
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AddressDelete]
-- ========================================================================================================================================

GO
 
