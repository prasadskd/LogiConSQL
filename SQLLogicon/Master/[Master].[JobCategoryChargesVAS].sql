


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryChargesVAS] Record based on [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASSelect] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASSelect] 
    @BranchID BIGINT,
    @ChargeCode NVARCHAR(20),
    @JobCategoryCode NVARCHAR(15),
    @PaymentTo SMALLINT,
    @PaymentTerm SMALLINT
AS 

BEGIN

	;With PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[PaymentTo], Jc.[PaymentTerm], Jc.[IsLoadAtGate], 
			Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn] ,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			Jb.Description As JobCategoryDescription,
			Chg.ChargeDescription As ChargeCodeDescription
	FROM   [Master].[JobCategoryChargesVAS] JC 
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Left Outer Join Master.ChargeMaster Chg On 
		Jc.ChargeCode = Chg.ChargeCode
	Left Outer Join Master.JobCategory Jb On
		Jc.JobCategoryCode = Jb.Code
	WHERE  Jc.[BranchID] = @BranchID  
	       AND Jc.[ChargeCode] = @ChargeCode  
	       AND Jc.[JobCategoryCode] = @JobCategoryCode  
	       AND Jc.[PaymentTo] = @PaymentTo  
	       AND Jc.[PaymentTerm] = @PaymentTerm  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryChargesVAS] Records from [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASList] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASList] 

AS 
BEGIN

	;With PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[PaymentTo], Jc.[PaymentTerm], Jc.[IsLoadAtGate], 
			Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn] ,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			Jb.Description As JobCategoryDescription,
			Chg.ChargeDescription As ChargeCodeDescription
	FROM   [Master].[JobCategoryChargesVAS] JC 
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Left Outer Join Master.ChargeMaster Chg On 
		Jc.ChargeCode = Chg.ChargeCode
	Left Outer Join Master.JobCategory Jb On
		Jc.JobCategoryCode = Jb.Code


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASPageList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryChargesVAS] Records from [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASPageView] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASPageView] 
	@fetchrows bigint
AS 
BEGIN

	;With PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[PaymentTo], Jc.[PaymentTerm], Jc.[IsLoadAtGate], 
			Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn] ,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			Jb.Description As JobCategoryDescription,
			Chg.ChargeDescription As ChargeCodeDescription
	FROM   [Master].[JobCategoryChargesVAS] JC 
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Left Outer Join Master.ChargeMaster Chg On 
		Jc.ChargeCode = Chg.ChargeCode
	Left Outer Join Master.JobCategory Jb On
		Jc.JobCategoryCode = Jb.Code
	ORDER BY [ChargeCode]
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASRecordCount] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[JobCategoryChargesVAS]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobCategoryChargesVAS] Record Into [JobCategoryChargesVAS] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASInsert] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASInsert] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsLoadAtGate bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[JobCategoryChargesVAS] (
			[BranchID], [ChargeCode], [JobCategoryCode], [PaymentTo], [PaymentTerm], [IsLoadAtGate], 
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ChargeCode, @JobCategoryCode, @PaymentTo, @PaymentTerm, @IsLoadAtGate, 
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobCategoryChargesVAS] Record Into [JobCategoryChargesVAS] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASUpdate] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsLoadAtGate bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[JobCategoryChargesVAS]
	SET    [IsLoadAtGate] = @IsLoadAtGate, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode
	       AND [JobCategoryCode] = @JobCategoryCode
	       AND [PaymentTo] = @PaymentTo
	       AND [PaymentTerm] = @PaymentTerm
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobCategoryChargesVAS] Record Into [JobCategoryChargesVAS] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASSave] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASSave] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsLoadAtGate bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[JobCategoryChargesVAS] 
		WHERE 	[BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode
	       AND [JobCategoryCode] = @JobCategoryCode
	       AND [PaymentTo] = @PaymentTo
	       AND [PaymentTerm] = @PaymentTerm)>0
	BEGIN
	    Exec [Master].[usp_JobCategoryChargesVASUpdate] 
		@BranchID, @ChargeCode, @JobCategoryCode, @PaymentTo, @PaymentTerm, @IsLoadAtGate, @CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobCategoryChargesVASInsert] 
		@BranchID, @ChargeCode, @JobCategoryCode, @PaymentTo, @PaymentTerm, @IsLoadAtGate, @CreatedBy, @ModifiedBy


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobCategoryChargesVASSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobCategoryChargesVAS] Record  based on [JobCategoryChargesVAS]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASDelete] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASDelete] 
    @BranchID BIGINT,
    @JobCategoryCode nvarchar(15)
     
AS 

	
BEGIN

SET NOCOUNT ON;	
	 
	DELETE
	FROM   [Master].[JobCategoryChargesVAS]
	WHERE  [BranchID] = @BranchID
	       AND [JobCategoryCode] = @JobCategoryCode
	       
SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesVASDelete]
-- ========================================================================================================================================

GO
 
