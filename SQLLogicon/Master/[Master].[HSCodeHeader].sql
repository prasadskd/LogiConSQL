 


-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [HSCodeHeader] Record based on [HSCodeHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderSelect] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderSelect] 
    @HeadingCode varchar(10)
AS 
 

BEGIN

	SELECT [HeadingCode], [HeadingDescription] 
	FROM   [Master].[HSCodeHeader]
	WHERE  ([HeadingCode] = @HeadingCode OR @HeadingCode IS NULL) 

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [HSCodeHeader] Records from [HSCodeHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderList] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderList] 

AS 
 
BEGIN
	SELECT [HeadingCode], [HeadingDescription] 
	FROM   [Master].[HSCodeHeader]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [HSCodeHeader] Record Into [HSCodeHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderInsert] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderInsert] 
    @HeadingCode varchar(10),
    @HeadingDescription nvarchar(MAX) = NULL
AS 
  

BEGIN
	
	INSERT INTO [Master].[HSCodeHeader] ([HeadingCode], [HeadingDescription])
	SELECT @HeadingCode, @HeadingDescription
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [HSCodeHeader] Record Into [HSCodeHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderUpdate] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderUpdate] 
    @HeadingCode varchar(10),
    @HeadingDescription nvarchar(MAX) = NULL
AS 
 
	
BEGIN

	UPDATE [Master].[HSCodeHeader]
	SET    [HeadingCode] = @HeadingCode, [HeadingDescription] = @HeadingDescription
	WHERE  [HeadingCode] = @HeadingCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [HSCodeHeader] Record Into [HSCodeHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderSave] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderSave] 
    @HeadingCode varchar(10),
    @HeadingDescription nvarchar(MAX) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[HSCodeHeader] 
		WHERE 	[HeadingCode] = @HeadingCode)>0
	BEGIN
	    Exec [Master].[usp_HSCodeHeaderUpdate] 
		@HeadingCode, @HeadingDescription


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_HSCodeHeaderInsert] 
		@HeadingCode, @HeadingDescription


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[HSCodeHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [HSCodeHeader] Record  based on [HSCodeHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderDelete] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderDelete] 
    @HeadingCode varchar(10)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[HSCodeHeader]
	WHERE  [HeadingCode] = @HeadingCode
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderDelete]
-- ========================================================================================================================================

GO 