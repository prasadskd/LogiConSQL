 


-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CurrencyRate] Record based on [CurrencyRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CurrencyRateSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateSelect] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateSelect] 
    @BranchID BIGINT,
    @CurrencyCode NVARCHAR(3),
    @ExpiryDate DATETIME
AS 

BEGIN

	SELECT [BranchID], [CurrencyCode], [ExpiryDate], [ExchangeRate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[CurrencyRate]
	WHERE  [BranchID] = @BranchID  
	       AND [CurrencyCode] = @CurrencyCode  
	       AND [ExpiryDate] = @ExpiryDate  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyRateSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CurrencyRate] Records from [CurrencyRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CurrencyRateList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateList] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateList] 
	@BranchID BIGINT,
	@CurrencyCode NVARCHAR(3)
AS 
BEGIN

	SELECT [BranchID], [CurrencyCode], [ExpiryDate], [ExchangeRate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[CurrencyRate]
	WHERE  [BranchID] = @BranchID  
	       AND [CurrencyCode] = @CurrencyCode 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyRateList] 
-- ========================================================================================================================================

GO

--========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CurrencyRate] Record Into [CurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyRateInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateInsert] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateInsert] 
    @BranchID BIGINT,
    @CurrencyCode nvarchar(3),
    @ExpiryDate datetime,
    @ExchangeRate numeric(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[CurrencyRate] ([BranchID], [CurrencyCode], [ExpiryDate], [ExchangeRate], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @CurrencyCode, @ExpiryDate, @ExchangeRate, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyRateInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CurrencyRate] Record Into [CurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyRateUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateUpdate] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateUpdate] 
    @BranchID BIGINT,
    @CurrencyCode nvarchar(3),
    @ExpiryDate datetime,
    @ExchangeRate numeric(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[CurrencyRate]
	SET		[BranchID] = @BranchID, [CurrencyCode] = @CurrencyCode, [ExpiryDate] = @ExpiryDate, [ExchangeRate] = @ExchangeRate, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [CurrencyCode] = @CurrencyCode
			AND [ExpiryDate] = @ExpiryDate
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyRateUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CurrencyRate] Record Into [CurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyRateSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateSave] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateSave] 
    @BranchID BIGINT,
    @CurrencyCode nvarchar(3),
    @ExpiryDate datetime,
    @ExchangeRate numeric(18, 4),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CurrencyRate] 
		WHERE 	[BranchID] = @BranchID
	       AND [CurrencyCode] = @CurrencyCode
	       AND [ExpiryDate] = @ExpiryDate)>0
	BEGIN
	    Exec [Master].[usp_CurrencyRateUpdate] 
		@BranchID, @CurrencyCode, @ExpiryDate, @ExchangeRate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CurrencyRateInsert] 
		@BranchID, @CurrencyCode, @ExpiryDate, @ExchangeRate, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CurrencyRateSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyRateDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CurrencyRate] Record  based on [CurrencyRate]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyRateDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyRateDelete] 
END 
GO
CREATE PROC [Master].[usp_CurrencyRateDelete] 
    @BranchID BIGINT,
    @CurrencyCode nvarchar(3),
    @ExpiryDate datetime
AS 

	
BEGIN

 
	DELETE
	FROM   [Master].[CurrencyRate]
	WHERE  [BranchID] = @BranchID
	       AND [CurrencyCode] = @CurrencyCode
	       AND [ExpiryDate] = @ExpiryDate
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyRateDelete]
-- ========================================================================================================================================

GO
 
