
-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerStatus] Record based on [ContainerStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ContainerStatusSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusSelect] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusSelect] 
    @BranchID BIGINT,
    @Code NVARCHAR(10)
AS 

BEGIN

	SELECT [BranchID], [Code], [Description], [IsHold], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[ContainerStatus]
	WHERE  ([BranchID] = @BranchID  OR BranchID = [Utility].[udf_GetRootBranchCode]())
	       AND [Code] = @Code  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerStatus] Records from [ContainerStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ContainerStatusList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusList] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusList] 
	--@BranchID BIGINT
AS 
BEGIN

	SELECT [BranchID], [Code], [Description], [IsHold], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[ContainerStatus]
	/*WHERE  ([BranchID] = @BranchID  OR BranchID = [Utility].[udf_GetRootBranchCode]())
	And Status = Cast (1 as bit)*/

END


-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerStatus] Records from [ContainerStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ContainerStatusPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusPageView] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusPageView] 
	@BranchID  smallint,
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [Code], [Description], [IsHold], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[ContainerStatus]
	Where ([BranchID] = @BranchID  OR BranchID = [Utility].[udf_GetRootBranchCode]())
	ORDER BY [Code]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ContainerStatusRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusRecordCount] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusRecordCount] 
	@BranchID  smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[ContainerStatus]
	Where ([BranchID] = @BranchID  OR BranchID = [Utility].[udf_GetRootBranchCode]()) 
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerStatus] Record based on [ContainerStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ContainerStatusAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusAutoCompleteSearch] 
    @BranchID BIGINT,
    @Code NVARCHAR(10)
AS 

BEGIN

	SELECT [BranchID], [Code], [Description], [IsHold], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[ContainerStatus]
	WHERE  ([BranchID] = @BranchID  OR BranchID = [Utility].[udf_GetRootBranchCode]())
		   And Status = Cast (1 as bit)
	       AND [Code] LIKE '%' +  @Code  + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ContainerStatus] Record Into [ContainerStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ContainerStatusInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusInsert] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusInsert] 
    @BranchID BIGINT,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsHold bit,
    @CreatedBy nvarchar(50),
	@ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[ContainerStatus] (
			[BranchID], [Code], [Description], [IsHold], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @Code, @Description, @IsHold, CAST(1 AS BIT), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ContainerStatus] Record Into [ContainerStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ContainerStatusUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusUpdate] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusUpdate] 
    @BranchID BIGINT,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsHold bit,
    @CreatedBy nvarchar(50),
	@ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[ContainerStatus]
	SET    [Description] = @Description, [IsHold] = @IsHold, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [Code] = @Code
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ContainerStatus] Record Into [ContainerStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ContainerStatusSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusSave] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusSave] 
    @BranchID BIGINT,
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsHold bit,
    @CreatedBy nvarchar(50),
	@ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[ContainerStatus] 
		WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_ContainerStatusUpdate] 
		@BranchID, @Code, @Description, @IsHold, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_ContainerStatusInsert] 
		@BranchID, @Code, @Description, @IsHold, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[ContainerStatusSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ContainerStatusDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ContainerStatus] Record  based on [ContainerStatus]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ContainerStatusDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ContainerStatusDelete] 
END 
GO
CREATE PROC [Master].[usp_ContainerStatusDelete] 
    @BranchID BIGINT,
    @Code nvarchar(10)
AS 

	
BEGIN

	UPDATE	[Master].[ContainerStatus]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ContainerStatusDelete]
-- ========================================================================================================================================

GO
