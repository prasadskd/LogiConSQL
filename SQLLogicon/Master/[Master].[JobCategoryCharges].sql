

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryCharges] Record based on [JobCategoryCharges] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesSelect] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesSelect] 
    @BranchID BIGINT,
    @ChargeCode NVARCHAR(20),
    @JobCategoryCode NVARCHAR(15),
    @MovementCode NVARCHAR(50),
	@Module smallint,
    @PaymentTo SMALLINT,
    @PaymentTerm SMALLINT
AS 

BEGIN

	;With ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='Module'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[MovementCode],Jc.Module, Jc.[PaymentTo], Jc.[PaymentTerm], 
			Jc.[IsCargoCharge], Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn],
			ISNULL(Ml.LookupDescription ,'') As ModuleDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription
	FROM	[Master].[JobCategoryCharges] JC 
	Left Outer Join ModuleLookup Ml ON 
		JC.Module = Ml.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	WHERE	Jc.[BranchID] = @BranchID  
			AND Jc.[ChargeCode] = @ChargeCode  
			AND Jc.[JobCategoryCode] = @JobCategoryCode  
			AND Jc.[MovementCode] = @MovementCode  
			AND Jc.[PaymentTo] = @PaymentTo  
			AND Jc.[PaymentTerm] = @PaymentTerm  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryCharges] Records from [JobCategoryCharges] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesList] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesList] 

AS 
BEGIN

	;With ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='Module'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[MovementCode],Jc.Module, Jc.[PaymentTo], Jc.[PaymentTerm], 
			Jc.[IsCargoCharge], Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn],
			ISNULL(Ml.LookupDescription ,'') As ModuleDescription,
			ISNULL(Pt.LookupDescription ,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription ,'') As PaymentTermDescription
	FROM	[Master].[JobCategoryCharges] JC 
	Left Outer Join ModuleLookup Ml ON 
		JC.Module = Ml.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesPageList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryCharges] Records from [JobCategoryCharges] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesPageView] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;With ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='Module'),
	PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[MovementCode],Jc.Module, Jc.[PaymentTo], Jc.[PaymentTerm], 
			Jc.[IsCargoCharge], Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn],
			ISNULL(Ml.LookupDescription ,'') As ModuleDescription,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription
	FROM	[Master].[JobCategoryCharges] JC 
	Left Outer Join ModuleLookup Ml ON 
		JC.Module = Ml.LookupID
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Where BranchID=@BranchID
	ORDER BY [ChargeCode]
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [JobCategoryCharges] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesRecordCount] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesRecordCount] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[JobCategoryCharges]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobCategoryCharges] Record Into [JobCategoryCharges] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesInsert] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesInsert] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsCargoCharge bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[JobCategoryCharges] (
			[BranchID], [ChargeCode], [JobCategoryCode], [MovementCode],Module, [PaymentTo], [PaymentTerm], [IsCargoCharge], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @ChargeCode, @JobCategoryCode, @MovementCode,@Module, @PaymentTo, @PaymentTerm, @IsCargoCharge, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobCategoryCharges] Record Into [JobCategoryCharges] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesUpdate] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsCargoCharge bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[JobCategoryCharges]
	SET    [IsCargoCharge] = @IsCargoCharge, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode
	       AND [JobCategoryCode] = @JobCategoryCode
	       AND [MovementCode] = @MovementCode
		   AND Module = @Module
	       AND [PaymentTo] = @PaymentTo
	       AND [PaymentTerm] = @PaymentTerm
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobCategoryCharges] Record Into [JobCategoryCharges] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesSave] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesSave] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @JobCategoryCode nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @PaymentTo smallint,
    @PaymentTerm smallint,
    @IsCargoCharge bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[JobCategoryCharges] 
		WHERE 	[BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode
	       AND [JobCategoryCode] = @JobCategoryCode
	       AND [MovementCode] = @MovementCode
		   And Module = @Module
	       AND [PaymentTo] = @PaymentTo
	       AND [PaymentTerm] = @PaymentTerm)>0
	BEGIN
	    Exec [Master].[usp_JobCategoryChargesUpdate] 
			@BranchID, @ChargeCode, @JobCategoryCode, @MovementCode,@Module, @PaymentTo, @PaymentTerm, @IsCargoCharge, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobCategoryChargesInsert] 
			@BranchID, @ChargeCode, @JobCategoryCode, @MovementCode,@Module, @PaymentTo, @PaymentTerm, @IsCargoCharge, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobCategoryChargesSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobCategoryCharges] Record  based on [JobCategoryCharges]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryChargesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesDelete] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesDelete] 
    @BranchID BIGINT,
    @JobCategoryCode nvarchar(15)

AS 

	
BEGIN

SET NOCOUNT ON;	

	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[JobCategoryCharges]
	WHERE  [BranchID] = @BranchID
	       AND [JobCategoryCode] = @JobCategoryCode
	       
SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryChargesDelete]
-- ========================================================================================================================================

GO

