 

-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TariffDetail] Record based on [TariffDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailSelect] 
END 
GO
CREATE PROCedure [Master].[usp_TariffDetailSelect] 
    @QuotationNo NVARCHAR(50),
    @QuotationType SMALLINT,
    @Module SMALLINT,    
    @SlabFrom INT,
    @SlabTo INT
AS 

BEGIN

	SELECT [QuotationNo], [QuotationType], [Module], [SlabFrom], [SlabTo], [TariffMode], [Transactions], [SellingPrice], [Percentage], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[TariffDetail]
	WHERE  [QuotationNo] = @QuotationNo  
	       AND [QuotationType] = @QuotationType  
	       AND [Module] = @Module  	       
	       AND [SlabFrom] = @SlabFrom  
	       AND [SlabTo] = @SlabTo  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TariffDetail] Records from [TariffDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailList] 
END 
GO
CREATE PROC [Master].[usp_TariffDetailList] 
    @QuotationNo NVARCHAR(50)
AS 
BEGIN
	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType'),
	TariffModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TariffDiscountType'),
	TransDiscountTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransDiscountType')
	SELECT [QuotationNo], [QuotationType], [Module], [SlabFrom], [SlabTo], [TariffMode], [Transactions], [SellingPrice], [Percentage], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(MD.LookupDescription, '') as ModuleDescription,
	ISNULL(TD.LookupDescription, '') as TariffModeDescription,
	ISNULL(TT.LookupDescription, '') as TransactionsDescription
	FROM   [Master].[TariffDetail] as Traiff
	Left Outer Join ModuleDescription MD On
		Traiff.Module = MD.LookupID
	Left Outer Join TariffModeDescription TD On
		Traiff.TariffMode = TD.LookupID
	Left Outer Join TransDiscountTypeDescription TT On
		Traiff.TariffMode = TT.LookupID	
	WHERE  [QuotationNo] = @QuotationNo  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffDetailList] 
-- ========================================================================================================================================

GO

 

-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [TariffDetail] Record Into [TariffDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailInsert] 
END 
GO
CREATE PROC [Master].[usp_TariffDetailInsert] 
    @QuotationNo nvarchar(50),
    @QuotationType smallint,
    @Module smallint,    
    @SlabFrom int,
    @SlabTo int,
    @TariffMode smallint,
    @Transactions smallint,
    @SellingPrice decimal(18, 2),
    @Percentage decimal(18, 2),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
  

BEGIN

	
	INSERT INTO [Master].[TariffDetail] (
			[QuotationNo], [QuotationType], [Module], [SlabFrom], [SlabTo], [TariffMode], [Transactions], [SellingPrice], [Percentage], 
			[CreatedBy], [CreatedOn])
	SELECT	@QuotationNo, @QuotationType, @Module, @SlabFrom, @SlabTo, @TariffMode, @Transactions, @SellingPrice, @Percentage, 
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [TariffDetail] Record Into [TariffDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailUpdate] 
END 
GO
CREATE PROC [Master].[usp_TariffDetailUpdate] 
    @QuotationNo nvarchar(50),
    @QuotationType smallint,
    @Module smallint,    
    @SlabFrom int,
    @SlabTo int,
    @TariffMode smallint,
	@Transactions smallint,
    @SellingPrice decimal(18, 2),
    @Percentage decimal(18, 2),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 
	
BEGIN

	UPDATE [Master].[TariffDetail]
	SET    [TariffMode] = @TariffMode, 
	[SellingPrice] = @SellingPrice, 
	[Percentage] = @Percentage, 
	[Transactions] = @Transactions,
	[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [QuotationNo] = @QuotationNo
	       AND [QuotationType] = @QuotationType
	       AND [Module] = @Module	       
	       AND [SlabFrom] = @SlabFrom
	       AND [SlabTo] = @SlabTo
	
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffDetailUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [TariffDetail] Record Into [TariffDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailSave] 
END 
GO
CREATE PROC [Master].[usp_TariffDetailSave] 
    @QuotationNo nvarchar(50),
    @QuotationType smallint,
    @Module smallint,    
    @SlabFrom int,
    @SlabTo int,
    @TariffMode smallint,
    @Transactions smallint,
    @SellingPrice decimal(18, 2),
    @Percentage decimal(18, 2),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[TariffDetail] 
		WHERE 	[QuotationNo] = @QuotationNo
	       AND [QuotationType] = @QuotationType
	       AND [Module] = @Module	       
	       AND [SlabFrom] = @SlabFrom
	       AND [SlabTo] = @SlabTo)>0
	BEGIN
	    Exec [Master].[usp_TariffDetailUpdate] 
		@QuotationNo, @QuotationType, @Module, @SlabFrom, @SlabTo, @TariffMode, @Transactions, @SellingPrice, 
		@Percentage, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_TariffDetailInsert] 
		@QuotationNo, @QuotationType, @Module, @SlabFrom, @SlabTo, @TariffMode, @Transactions, @SellingPrice, 
		@Percentage, @CreatedBy, @ModifiedBy 


	END
	
END
	

-- ========================================================================================================================================
-- END  											 [Master].usp_[TariffDetailSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TariffDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [TariffDetail] Record  based on [TariffDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffDetailDelete] 
END 
GO
CREATE PROC [Master].[usp_TariffDetailDelete] 
    @QuotationNo nvarchar(50)
     
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[TariffDetail]
	WHERE  [QuotationNo] = @QuotationNo
	        
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffDetailDelete]
-- ========================================================================================================================================



GO
 
