
-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DriverIncentive] Record based on [DriverIncentive] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveSelect] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveSelect] 
    @BranchID BIGINT,
    @QuotationNo NVARCHAR(50)
AS 

BEGIN

	;with FreightModeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Di.[BranchID], Di.[QuotationNo], Di.[Remarks], Di.[FreightMode], Di.[IsApproved], Di.[ApprovedBy], Di.[ApprovedOn], 
			Di.[IsCancel], Di.[CancelledBy], Di.[CancelledOn], Di.[CreatedBy], Di.[CreatedOn], Di.[ModifiedBy], Di.[ModifiedOn], 
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription
	FROM	[Master].[DriverIncentive] Di
	Left Outer Join FreightModeLookup Fm On 
		Di.FreightMode = Fm.LookupID
	WHERE  Di.[BranchID] = @BranchID  
	       AND Di.[QuotationNo] = @QuotationNo  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DriverIncentive] Records from [DriverIncentive] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveList] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveList] 
	@BranchID BIGINT
AS 
BEGIN

	;with FreightModeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Di.[BranchID], Di.[QuotationNo], Di.[Remarks], Di.[FreightMode], Di.[IsApproved], Di.[ApprovedBy], Di.[ApprovedOn], 
			Di.[IsCancel], Di.[CancelledBy], Di.[CancelledOn], Di.[CreatedBy], Di.[CreatedOn], Di.[ModifiedBy], Di.[ModifiedOn], 
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription
	FROM	[Master].[DriverIncentive] Di
	Left Outer Join FreightModeLookup Fm On 
		Di.FreightMode = Fm.LookupID
	WHERE  Di.[BranchID] = @BranchID 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentivePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DriverIncentive] Records from [DriverIncentive] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentivePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentivePageView] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentivePageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;with FreightModeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Di.[BranchID], Di.[QuotationNo], Di.[Remarks], Di.[FreightMode], Di.[IsApproved], Di.[ApprovedBy], Di.[ApprovedOn], 
			Di.[IsCancel], Di.[CancelledBy], Di.[CancelledOn], Di.[CreatedBy], Di.[CreatedOn], Di.[ModifiedBy], Di.[ModifiedOn], 
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription
	FROM	[Master].[DriverIncentive] Di
	Left Outer Join FreightModeLookup Fm On 
		Di.FreightMode = Fm.LookupID
	WHERE  Di.[BranchID] = @BranchID 
	ORDER BY [QuotationNo]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentivePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [DriverIncentive] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveRecordCount] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveRecordCount] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[DriverIncentive]
	WHERE   [BranchID] = @BranchID 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DriverIncentive] Record based on [DriverIncentive] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveAutoCompleteSearch] 
    @BranchID BIGINT,
    @QuotationNo NVARCHAR(50)
AS 

BEGIN

	;with FreightModeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Di.[BranchID], Di.[QuotationNo], Di.[Remarks], Di.[FreightMode], Di.[IsApproved], Di.[ApprovedBy], Di.[ApprovedOn], 
			Di.[IsCancel], Di.[CancelledBy], Di.[CancelledOn], Di.[CreatedBy], Di.[CreatedOn], Di.[ModifiedBy], Di.[ModifiedOn], 
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription
	FROM	[Master].[DriverIncentive] Di
	Left Outer Join FreightModeLookup Fm On 
		Di.FreightMode = Fm.LookupID
	WHERE  Di.[BranchID] = @BranchID 
			And Di.[IsCancel]= CAST(0 as bit)	 
	       AND Di.[QuotationNo] LIKE '%' +  @QuotationNo + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DriverIncentive] Record Into [DriverIncentive] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveInsert] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveInsert] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @Remarks nvarchar(100),
    @FreightMode smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[DriverIncentive] (
			[BranchID], [QuotationNo], [Remarks], [FreightMode], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @QuotationNo, @Remarks, @FreightMode, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DriverIncentive] Record Into [DriverIncentive] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveUpdate] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveUpdate] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @Remarks nvarchar(100),
    @FreightMode smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[DriverIncentive]
	SET    [Remarks] = @Remarks, [FreightMode] = @FreightMode,[ModifiedBy] = @ModifiedBy, [ModifiedOn] = getutcdate()
	WHERE  [BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DriverIncentive] Record Into [DriverIncentive] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveSave] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveSave] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @Remarks nvarchar(100),
    @FreightMode smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[DriverIncentive] 
		WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo)>0
	BEGIN
	    Exec [Master].[usp_DriverIncentiveUpdate] 
			@BranchID, @QuotationNo, @Remarks, @FreightMode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_DriverIncentiveInsert] 
			@BranchID, @QuotationNo, @Remarks, @FreightMode, @CreatedBy, @ModifiedBy


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[DriverIncentiveSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DriverIncentive] Record  based on [DriverIncentive]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDelete] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDelete] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
	@CancelledBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[DriverIncentive]
	SET	[IsCancel] = CAST(0 as bit),CancelledBy = @CancelledBy, CancelledOn = GetUtcDate()
	WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDelete]
-- ========================================================================================================================================

GO

 

-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DriverIncentive] Record  based on [DriverIncentive]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveApproveDisApprove]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveApproveDisApprove] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveApproveDisApprove] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
	@IsApproved bit,
	@ApprovedBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[DriverIncentive]
	SET	[IsApproved] = @IsApproved,ApprovedBy = @ApprovedBy,ApprovedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveApproveDisApprove]
-- ========================================================================================================================================

GO

 