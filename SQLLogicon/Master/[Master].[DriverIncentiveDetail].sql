-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DriverIncentiveDetail] Record based on [DriverIncentiveDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailSelect] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailSelect] 
    @BranchID BIGINT,
    @QuotationNo NVARCHAR(50) 
AS 

BEGIN

	  

	;WITH BookingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='JobType'),
	ShipmentTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='ShipmentType'),
	CargoTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='CargoType'),
	BillingUnitTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='BillingUnitType'),
	IncentiveTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='IncentiveType'),
	TransportTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TransportType'),
	TrailerTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TrailerType'),
	HandlingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageHandlingType'),
	MovementTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageMovementType')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemID], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[PickupFrom], Dt.[DeliveryTo], 
			Dt.[Size], Dt.[Type], Dt.[CargoType], Dt.TransportType,Dt.TrailerType,Dt.HandlingType,Dt.MovementCode,Dt.MovementType,
			Dt.[CustomerCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[IncentiveType], Dt.[Percentage], Dt.[FixRate], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jt.LookupDescription,'') As JobTypeDescription,
			ISNULL(St.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Ct.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Bt.LookupDescription,'') As BillingUnitTypeDescription,
			ISNULL(It.LookupDescription,'') As IncentiveTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Tl.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Ht.LookupDescription,'') As HandlingTypeDescription,
			ISNULL(Mt.LookupDescription,'') As MovementTypeDescription,
			Cg.ChargeDescription,
			Jc.Description As JobCategoryDescription,
			Cu.MerchantName as CustomerName, 0 As IsSelected
	FROM	[Master].[DriverIncentiveDetail] Dt
	Left Outer Join BookingTypeDescription Jt ON 
		Dt.JobType = Jt.LookupID
	Left Outer Join ShipmentTypeDescription St On
		Dt.ShipmentType = St.LookupID
	Left Outer Join CargoTypeDescription Ct On 
		Dt.CargoType = Ct.LookupID
	Left Outer Join BillingUnitTypeDescription Bt On 
		Dt.BillingUnit = Bt.LookupID
	Left Outer Join IncentiveTypeDescription It On
		Dt.IncentiveType = It.LookupID
	Left Outer Join TransportTypeDescription Tr On
		Dt.TransportType = Tr.LookupID
	Left Outer Join TrailerTypeDescription Tl On
		Dt.TrailerType = Tl.LookupID
	Left Outer Join HandlingTypeDescription Ht On
		Dt.HandlingType = Ht.LookupID
	Left Outer Join MovementTypeDescription Mt On
		Dt.MovementType = Mt.LookupID
	Left Outer Join Master.ChargeMaster Cg ON 
		Dt.ChargeCode = Cg.ChargeCode
	Left Outer Join Master.JobCategory Jc On 
		Dt.JobCategory = Jc.Code
	Left Outer Join Master.Merchant Cu On 
		Dt.CustomerCode = Cu.MerchantCode
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[QuotationNo] = @QuotationNo  
	      
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DriverIncentiveDetail] Records from [DriverIncentiveDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailList] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailList] 
	@BranchID BIGINT,
    @QuotationNo NVARCHAR(50)
AS 
BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='JobType'),
	ShipmentTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='ShipmentType'),
	CargoTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='CargoType'),
	BillingUnitTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='BillingUnitType'),
	IncentiveTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='IncentiveType'),
	TransportTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TransportType'),
	TrailerTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TrailerType'),
	HandlingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageHandlingType'),
	MovementTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageMovementType')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemID], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[PickupFrom], Dt.[DeliveryTo], 
			Dt.[Size], Dt.[Type], Dt.[CargoType], Dt.TransportType,Dt.TrailerType,Dt.HandlingType,Dt.MovementCode,Dt.MovementType,
			Dt.[CustomerCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[IncentiveType], Dt.[Percentage], Dt.[FixRate], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jt.LookupDescription,'') As JobTypeDescription,
			ISNULL(St.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Ct.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Bt.LookupDescription,'') As BillingUnitTypeDescription,
			ISNULL(It.LookupDescription,'') As IncentiveTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Tl.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Ht.LookupDescription,'') As HandlingTypeDescription,
			ISNULL(Mt.LookupDescription,'') As MovementTypeDescription,
			Cg.ChargeDescription,
			Jc.Description As JobCategoryDescription,
			Cu.MerchantName as CustomerName, 0 As IsSelected
	FROM	[Master].[DriverIncentiveDetail] Dt
	Left Outer Join BookingTypeDescription Jt ON 
		Dt.JobType = Jt.LookupID
	Left Outer Join ShipmentTypeDescription St On
		Dt.ShipmentType = St.LookupID
	Left Outer Join CargoTypeDescription Ct On 
		Dt.CargoType = Ct.LookupID
	Left Outer Join BillingUnitTypeDescription Bt On 
		Dt.BillingUnit = Bt.LookupID
	Left Outer Join IncentiveTypeDescription It On
		Dt.IncentiveType = It.LookupID
	Left Outer Join TransportTypeDescription Tr On
		Dt.TransportType = Tr.LookupID
	Left Outer Join TrailerTypeDescription Tl On
		Dt.TrailerType = Tl.LookupID
	Left Outer Join HandlingTypeDescription Ht On
		Dt.HandlingType = Ht.LookupID
	Left Outer Join MovementTypeDescription Mt On
		Dt.MovementType = Mt.LookupID
	Left Outer Join Master.ChargeMaster Cg ON 
		Dt.ChargeCode = Cg.ChargeCode
	Left Outer Join Master.JobCategory Jc On 
		Dt.JobCategory = Jc.Code
	Left Outer Join Master.Merchant Cu On 
		Dt.CustomerCode = Cu.MerchantCode
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[QuotationNo] = @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DriverIncentiveDetail] Records from [DriverIncentiveDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailPageView] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailPageView] 
	@BranchID BIGINT,
    @QuotationNo NVARCHAR(50),
	@fetchrows bigint
AS 
BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='JobType'),
	ShipmentTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='ShipmentType'),
	CargoTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='CargoType'),
	BillingUnitTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='BillingUnitType'),
	IncentiveTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='IncentiveType'),
	TransportTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TransportType'),
	TrailerTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TrailerType'),
	HandlingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageHandlingType'),
	MovementTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageMovementType')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemID], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[PickupFrom], Dt.[DeliveryTo], 
			Dt.[Size], Dt.[Type], Dt.[CargoType], Dt.TransportType,Dt.TrailerType,Dt.HandlingType,Dt.MovementCode,Dt.MovementType,
			Dt.[CustomerCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[IncentiveType], Dt.[Percentage], Dt.[FixRate], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jt.LookupDescription,'') As JobTypeDescription,
			ISNULL(St.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Ct.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Bt.LookupDescription,'') As BillingUnitTypeDescription,
			ISNULL(It.LookupDescription,'') As IncentiveTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Tl.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Ht.LookupDescription,'') As HandlingTypeDescription,
			ISNULL(Mt.LookupDescription,'') As MovementTypeDescription,
			Cg.ChargeDescription,
			Jc.Description As JobCategoryDescription,
			Cu.MerchantName as CustomerName, 0 As IsSelected
	FROM	[Master].[DriverIncentiveDetail] Dt
	Left Outer Join BookingTypeDescription Jt ON 
		Dt.JobType = Jt.LookupID
	Left Outer Join ShipmentTypeDescription St On
		Dt.ShipmentType = St.LookupID
	Left Outer Join CargoTypeDescription Ct On 
		Dt.CargoType = Ct.LookupID
	Left Outer Join BillingUnitTypeDescription Bt On 
		Dt.BillingUnit = Bt.LookupID
	Left Outer Join IncentiveTypeDescription It On
		Dt.IncentiveType = It.LookupID
	Left Outer Join TransportTypeDescription Tr On
		Dt.TransportType = Tr.LookupID
	Left Outer Join TrailerTypeDescription Tl On
		Dt.TrailerType = Tl.LookupID
	Left Outer Join HandlingTypeDescription Ht On
		Dt.HandlingType = Ht.LookupID
	Left Outer Join MovementTypeDescription Mt On
		Dt.MovementType = Mt.LookupID
	Left Outer Join Master.ChargeMaster Cg ON 
		Dt.ChargeCode = Cg.ChargeCode
	Left Outer Join Master.JobCategory Jc On 
		Dt.JobCategory = Jc.Code
	Left Outer Join Master.Merchant Cu On 
		Dt.CustomerCode = Cu.MerchantCode
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[QuotationNo] = @QuotationNo
	ORDER BY [QuotationNo]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [DriverIncentiveDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailRecordCount] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailRecordCount] 
	@BranchID BIGINT,
    @QuotationNo NVARCHAR(50) 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[DriverIncentiveDetail] Dt
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[QuotationNo] =  @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DriverIncentiveDetail] Record based on [DriverIncentiveDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailAutoCompleteSearch] 
    @BranchID BIGINT,
    @QuotationNo NVARCHAR(50) 
AS 

BEGIN

	;WITH BookingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='JobType'),
	ShipmentTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='ShipmentType'),
	CargoTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='CargoType'),
	BillingUnitTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='BillingUnitType'),
	IncentiveTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='IncentiveType'),
	TransportTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TransportType'),
	TrailerTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='TrailerType'),
	HandlingTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageHandlingType'),
	MovementTypeDescription As (Select LookupID,LookUpDescription FROM [Config].[Lookup]WHERE [LookupCategory] ='HaulageMovementType')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemID], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[PickupFrom], Dt.[DeliveryTo], 
			Dt.[Size], Dt.[Type], Dt.[CargoType], Dt.TransportType,Dt.TrailerType,Dt.HandlingType,Dt.MovementCode,Dt.MovementType,
			Dt.[CustomerCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[IncentiveType], Dt.[Percentage], Dt.[FixRate], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jt.LookupDescription,'') As JobTypeDescription,
			ISNULL(St.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Ct.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Bt.LookupDescription,'') As BillingUnitTypeDescription,
			ISNULL(It.LookupDescription,'') As IncentiveTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Tl.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Ht.LookupDescription,'') As HandlingTypeDescription,
			ISNULL(Mt.LookupDescription,'') As MovementTypeDescription,
			Cg.ChargeDescription,
			Jc.Description As JobCategoryDescription,
			Cu.MerchantName as CustomerName, 0 As IsSelected
	FROM	[Master].[DriverIncentiveDetail] Dt
	Left Outer Join BookingTypeDescription Jt ON 
		Dt.JobType = Jt.LookupID
	Left Outer Join ShipmentTypeDescription St On
		Dt.ShipmentType = St.LookupID
	Left Outer Join CargoTypeDescription Ct On 
		Dt.CargoType = Ct.LookupID
	Left Outer Join BillingUnitTypeDescription Bt On 
		Dt.BillingUnit = Bt.LookupID
	Left Outer Join IncentiveTypeDescription It On
		Dt.IncentiveType = It.LookupID
	Left Outer Join TransportTypeDescription Tr On
		Dt.TransportType = Tr.LookupID
	Left Outer Join TrailerTypeDescription Tl On
		Dt.TrailerType = Tl.LookupID
	Left Outer Join HandlingTypeDescription Ht On
		Dt.HandlingType = Ht.LookupID
	Left Outer Join MovementTypeDescription Mt On
		Dt.MovementType = Mt.LookupID
	Left Outer Join Master.ChargeMaster Cg ON 
		Dt.ChargeCode = Cg.ChargeCode
	Left Outer Join Master.JobCategory Jc On 
		Dt.JobCategory = Jc.Code
	Left Outer Join Master.Merchant Cu On 
		Dt.CustomerCode = Cu.MerchantCode
	WHERE  Dt.[BranchID] = @BranchID  
	       AND Dt.[QuotationNo] LIKE '%' +  @QuotationNo  + '%'
	       
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DriverIncentiveDetail] Record Into [DriverIncentiveDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailInsert] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailInsert] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @ItemID int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @PickupFrom nvarchar(10),
    @DeliveryTo nvarchar(10),
    @Size varchar(10),
    @Type varchar(10),
    @CargoType smallint,
	@TransportType smallint,
	@TrailerType smallint,
	@HandlingType smallint,
	@MovementCode nvarchar(50),
	@MovementType smallint,
    @CustomerCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @IncentiveType smallint,
    @Percentage decimal(18, 2),
    @FixRate decimal(18, 2),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[DriverIncentiveDetail] (
			[BranchID], [QuotationNo], [ItemID], [JobCategory], [JobType], [ShipmentType], [ChargeCode], [PickupFrom], [DeliveryTo], 
			[Size], [Type], [CargoType],TransportType,TrailerType,HandlingType,MovementCode,MovementType, [CustomerCode], [BillingUnit], [CurrencyCode], [IncentiveType], [Percentage], [FixRate], 
			[Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @QuotationNo, @ItemID, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @PickupFrom, @DeliveryTo, 
			@Size, @Type, @CargoType,@TransportType,@TrailerType,@HandlingType,@MovementCode,@MovementType, @CustomerCode, @BillingUnit, @CurrencyCode, @IncentiveType, @Percentage, @FixRate, 
			@Remarks, @CreatedBy, getUtcDate()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DriverIncentiveDetail] Record Into [DriverIncentiveDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailUpdate] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailUpdate] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @ItemID int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @PickupFrom nvarchar(10),
    @DeliveryTo nvarchar(10),
    @Size varchar(10),
    @Type varchar(10),
    @CargoType smallint,
	@TransportType smallint,
	@TrailerType smallint,
	@HandlingType smallint,
	@MovementCode nvarchar(50),
	@MovementType smallint,
    @CustomerCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @IncentiveType smallint,
    @Percentage decimal(18, 2),
    @FixRate decimal(18, 2),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[DriverIncentiveDetail]
	SET		[JobCategory] = @JobCategory, [JobType] = @JobType, [ShipmentType] = @ShipmentType, [ChargeCode] = @ChargeCode, 
			[PickupFrom] = @PickupFrom, [DeliveryTo] = @DeliveryTo, [Size] = @Size, [Type] = @Type, [CargoType] = @CargoType, 
			TransportType=@TransportType,TrailerType=@TrailerType,HandlingType=@HandlingType,MovementCode=@MovementCode,MovementType=@MovementType,
			[CustomerCode] = @CustomerCode, [BillingUnit] = @BillingUnit, [CurrencyCode] = @CurrencyCode, [IncentiveType] = @IncentiveType, 
			[Percentage] = @Percentage, [FixRate] = @FixRate, [Remarks] = @Remarks, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GetUtcDate()
	WHERE	[BranchID] = @BranchID
			AND [QuotationNo] = @QuotationNo
			AND [ItemID] = @ItemID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DriverIncentiveDetail] Record Into [DriverIncentiveDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailSave] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailSave] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50),
    @ItemID int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @PickupFrom nvarchar(10),
    @DeliveryTo nvarchar(10),
    @Size varchar(10),
    @Type varchar(10),
    @CargoType smallint,
	@TransportType smallint,
	@TrailerType smallint,
	@HandlingType smallint,
	@MovementCode nvarchar(50),
	@MovementType smallint,
    @CustomerCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @IncentiveType smallint,
    @Percentage decimal(18, 2),
    @FixRate decimal(18, 2),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[DriverIncentiveDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	       AND [ItemID] = @ItemID)>0
	BEGIN
	    Exec [Master].[usp_DriverIncentiveDetailUpdate] 
				@BranchID, @QuotationNo, @ItemID, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @PickupFrom, @DeliveryTo, @Size, 
				@Type, @CargoType, @TransportType,@TrailerType,@HandlingType,@MovementCode,@MovementType, @CustomerCode, @BillingUnit, @CurrencyCode, @IncentiveType, @Percentage, @FixRate, @Remarks, 
				@CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_DriverIncentiveDetailInsert] 
				@BranchID, @QuotationNo, @ItemID, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @PickupFrom, @DeliveryTo, @Size, 
				@Type, @CargoType,@TransportType,@TrailerType,@HandlingType,@MovementCode,@MovementType, @CustomerCode, @BillingUnit, @CurrencyCode, @IncentiveType, @Percentage, @FixRate, @Remarks, 
				@CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[DriverIncentiveDetailSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverIncentiveDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DriverIncentiveDetail] Record  based on [DriverIncentiveDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverIncentiveDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverIncentiveDetailDelete] 
END 
GO
CREATE PROC [Master].[usp_DriverIncentiveDetailDelete] 
    @BranchID BIGINT,
    @QuotationNo nvarchar(50) 
     
AS 

	
BEGIN

	Delete	[Master].[DriverIncentiveDetail]
	WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	      
	Return 1
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverIncentiveDetailDelete]
-- ========================================================================================================================================

GO

 
