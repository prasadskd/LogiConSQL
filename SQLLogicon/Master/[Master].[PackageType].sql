-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PackageType] Record based on [PackageType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PackageTypeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeSelect] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeSelect] 
    @PackageTypeCode NVARCHAR(10)
AS 

BEGIN

	SELECT [PackageTypeCode], [PackageDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[PackageType]
	WHERE  [PackageTypeCode] = @PackageTypeCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PackageType] Records from [PackageType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PackageTypeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeList] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeList] 

AS 
BEGIN

	SELECT [PackageTypeCode], [PackageDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[PackageType]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PackageType] Records from [PackageType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PackageTypePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypePageView] 
END 
GO
CREATE PROC [Master].[usp_PackageTypePageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [PackageTypeCode], [PackageDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[PackageType]
	ORDER BY [PackageDescription]
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [PackageType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PackageTypeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[PackageType]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PackageType] Record based on [PackageType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PackageTypeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeAutoCompleteSearch] 
    @PackageDescription NVARCHAR(100)
AS 

BEGIN

	SELECT [PackageTypeCode], [PackageDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[PackageType]
	WHERE  [PackageDescription] Like '%' + @PackageDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [PackageType] Record Into [PackageType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PackageTypeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeInsert] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeInsert] 
    @PackageTypeCode nvarchar(10),
    @PackageDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[PackageType] (
			[PackageTypeCode], [PackageDescription], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@PackageTypeCode, @PackageDescription, Cast(1 as bit), @CreatedBy, GetUTCDate()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [PackageType] Record Into [PackageType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PackageTypeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeUpdate] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeUpdate] 
    @PackageTypeCode nvarchar(10),
    @PackageDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[PackageType]
	SET    [PackageDescription] = @PackageDescription, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GetUTCDate()
	WHERE  [PackageTypeCode] = @PackageTypeCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [PackageType] Record Into [PackageType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PackageTypeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeSave] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeSave] 
    @PackageTypeCode nvarchar(10),
    @PackageDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[PackageType] 
		WHERE 	[PackageTypeCode] = @PackageTypeCode)>0
	BEGIN
	    Exec [Master].[usp_PackageTypeUpdate] 
		@PackageTypeCode, @PackageDescription, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_PackageTypeInsert] 
		@PackageTypeCode, @PackageDescription, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[PackageTypeSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PackageTypeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [PackageType] Record  based on [PackageType]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PackageTypeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PackageTypeDelete] 
END 
GO
CREATE PROC [Master].[usp_PackageTypeDelete] 
    @PackageTypeCode nvarchar(10),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[PackageType]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy, ModifiedOn = GetUTCDate()
	WHERE 	[PackageTypeCode] = @PackageTypeCode

	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PackageTypeDelete]
-- ========================================================================================================================================

GO
 