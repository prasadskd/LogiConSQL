


-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CountryStates] Record based on [CountryStates] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryStatesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesSelect] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesSelect] 
    @StateID INT,
    @StateCode NVARCHAR(100),
    @CountryCode VARCHAR(2)
AS 

BEGIN

	SELECT CS.[StateID], CS.[StateCode], CS.[CountryCode], CS.[StateName], CS.[IsActive], CS.[CreatedBy], CS.[CreatedOn], CS.[ModifiedBy], CS.[ModifiedOn] ,
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CountryStates] CS
	Left Outer Join [Master].[Country] C On 
	Cs.CountryCode = C.CountryCode
	WHERE  CS.[StateID] = @StateID  
	       AND CS.[StateCode] = @StateCode  
	       AND CS.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CountryStates] Records from [CountryStates] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryStatesList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesList] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesList] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT CS.[StateID], CS.[StateCode], CS.[CountryCode], CS.[StateName], CS.[IsActive], CS.[CreatedBy], CS.[CreatedOn], CS.[ModifiedBy], CS.[ModifiedOn] ,
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CountryStates] CS
	Left Outer Join [Master].[Country] C On 
	Cs.CountryCode = C.CountryCode
	Where Cs.CountryCode = @CountryCode


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CountryStates] Records from [CountryStates] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryStatesPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesPageView] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesPageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT CS.[StateID], CS.[StateCode], CS.[CountryCode], CS.[StateName], CS.[IsActive], CS.[CreatedBy], CS.[CreatedOn], CS.[ModifiedBy], CS.[ModifiedOn] ,
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CountryStates] CS
	Left Outer Join [Master].[Country] C On 
	Cs.CountryCode = C.CountryCode
	Where Cs.CountryCode = @CountryCode
	ORDER BY  Cs.StateName
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CountryStates] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryStatesRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesRecordCount] 
	 @CountryCode VARCHAR(2)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[CountryStates]
	Where CountryCode = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CountryStates] Record based on [CountryStates] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryStatesAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesAutoCompleteSearch] 
    @StateName NVARCHAR(100),
    @CountryCode VARCHAR(2)
AS 

BEGIN

	SELECT CS.[StateID], CS.[StateCode], CS.[CountryCode], CS.[StateName], CS.[IsActive], CS.[CreatedBy], CS.[CreatedOn], CS.[ModifiedBy], CS.[ModifiedOn] ,
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CountryStates] CS
	Left Outer Join [Master].[Country] C On 
	Cs.CountryCode = C.CountryCode
	WHERE  [StateCode] Like '%' +  @StateName   + '%'
	       AND CS.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CountryStates] Record Into [CountryStates] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryStatesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesInsert] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesInsert]
	@StateID int,
    @StateCode nvarchar(100),
    @CountryCode varchar(2),
    @StateName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[CountryStates] (
			[StateCode], [CountryCode], [StateName], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@StateCode, @CountryCode, @StateName, Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CountryStates] Record Into [CountryStates] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryStatesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesUpdate] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesUpdate] 
    @StateID int,
	@StateCode nvarchar(100),
    @CountryCode varchar(2),
    @StateName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[CountryStates]
	SET    [StateName] = @StateName, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [StateID] = @StateID
	       AND [StateCode] = @StateCode
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CountryStates] Record Into [CountryStates] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryStatesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesSave] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesSave] 
    @StateID int,
	@StateCode nvarchar(100),
    @CountryCode varchar(2),
    @StateName nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CountryStates] 
		WHERE 	[StateID] = @StateID
	       AND [StateCode] = @StateCode
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_CountryStatesUpdate] 
		@StateID,@StateCode, @CountryCode, @StateName,  @CreatedBy,  @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CountryStatesInsert] 
		@StateID,@StateCode, @CountryCode, @StateName,  @CreatedBy,  @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CountryStatesSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CountryStatesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CountryStates] Record  based on [CountryStates]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryStatesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryStatesDelete] 
END 
GO
CREATE PROC [Master].[usp_CountryStatesDelete] 
    @StateID int,
    @StateCode nvarchar(100),
    @CountryCode varchar(2),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[CountryStates]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy,ModifiedOn = GETUTCDATE()
	WHERE 	[StateID] = @StateID
	       AND [StateCode] = @StateCode
	       AND [CountryCode] = @CountryCode

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[CountryStates]
	WHERE  [StateID] = @StateID
	       AND [StateCode] = @StateCode
	       AND [CountryCode] = @CountryCode
	*/


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryStatesDelete]
-- ========================================================================================================================================

GO

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

