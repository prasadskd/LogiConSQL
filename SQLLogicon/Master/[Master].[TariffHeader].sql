 


-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TariffHeader] Record based on [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderSelect] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderSelect] 
    @QuotationNo NVARCHAR(50)
     
AS 

BEGIN

	SELECT [QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[TariffHeader]
	WHERE  [QuotationNo] = @QuotationNo  
	       
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TariffHeader] Records from [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderList] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderList] 
     
AS 
BEGIN

	SELECT TOP(100) [QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[TariffHeader]
	--WHERE  [QuotationNo] = @QuotationNo  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TariffHeader] Records from [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderListByTariffMode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderListByTariffMode] 
END 
GO
CREATE  PROC [Master].[usp_TariffHeaderListByTariffMode]
@TariffMode int
As
Begin


 Select [QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] From Master.TariffHeader Where QuotationNo in (
 Select distinct(QuotationNo) From Master.TariffDetail Where QuotationType = @TariffMode)

End
-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TariffHeader] Records from [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderPageView] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderPageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[TariffHeader]
	ORDER BY [QuotationNo]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderRecordCount] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[TariffHeader]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TariffHeader] Record based on [TariffHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TariffHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderAutoCompleteSearch] 
    @QuotationNo NVARCHAR(50) 
     
AS 

BEGIN

	SELECT [QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[TariffHeader]
	WHERE  [QuotationNo] LIKE '%' +  @QuotationNo  + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [TariffHeader] Record Into [TariffHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderInsert] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderInsert] 
    @QuotationNo nvarchar(50),
    @EffectiveDate datetime,
    @ExpiryDate datetime,
    @CreatedBy nvarchar(50), 
    @ModifiedBy nvarchar(50), 
	@NewQuotationNo nvarchar(50) OUTPUT
AS 
  

BEGIN

	
	INSERT INTO [Master].[TariffHeader] (
			[QuotationNo], [EffectiveDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@QuotationNo, @EffectiveDate, @ExpiryDate, Cast(1 as bit), @CreatedBy, getutcdate()
               
	Select @NewQuotationNo = @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [TariffHeader] Record Into [TariffHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderUpdate] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderUpdate] 
    @QuotationNo nvarchar(50),
    @EffectiveDate datetime,
    @ExpiryDate datetime,
    @CreatedBy nvarchar(50), 
    @ModifiedBy nvarchar(50) ,
	@NewQuotationNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE [Master].[TariffHeader]
	SET     [EffectiveDate] = @EffectiveDate, [ExpiryDate] = @ExpiryDate,  [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [QuotationNo] = @QuotationNo
	        
	Select @NewQuotationNo = @QuotationNo
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [TariffHeader] Record Into [TariffHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderSave] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderSave] 
    @QuotationNo nvarchar(50),
    @Association smallint,
    @EffectiveDate datetime,
    @ExpiryDate datetime,
    @CreatedBy nvarchar(50), 
    @ModifiedBy nvarchar(50), 
    @NewQuotationNo nvarchar(50) OUTPUT
AS 
 

BEGIN
    IF @QuotationNo = 'STANDARD_1000001'
      Begin
		IF (SELECT COUNT(0) FROM [Master].[TariffHeader] WHERE [QuotationNo] = @QuotationNo) > 0
		 Exec [Master].[usp_TariffHeaderUpdate] 
		 @QuotationNo, @Association, @EffectiveDate, @ExpiryDate, @CreatedBy, @ModifiedBy,@NewQuotationNo = @NewQuotationNo OUTPUT
		Else
		 Exec [Master].[usp_TariffHeaderInsert] 
		   @QuotationNo, @Association, @EffectiveDate, @ExpiryDate, @CreatedBy,  @ModifiedBy, @NewQuotationNo = @NewQuotationNo OUTPUT  
      End
 Else 
    
	IF (Select Count(0) From [Master].[TariffHeader] Where Association = @Association and (EffectiveDate <= @EffectiveDate and ExpiryDate >= @ExpiryDate)) > 0
	   RaisError('A QUOTATION ALREADY EXISTS WITH THIS DATE RANGE',16,1)
	Else
		Begin
		 IF (SELECT COUNT(0) FROM [Master].[TariffHeader] WHERE  [QuotationNo] = @QuotationNo AND [EffectiveDate] = @EffectiveDate)>0
			   Exec [Master].[usp_TariffHeaderUpdate] 
				@QuotationNo, @Association, @EffectiveDate, @ExpiryDate, @CreatedBy, @ModifiedBy ,@NewQuotationNo = @NewQuotationNo OUTPUT
			ELSE
			  BEGIN
  
			  Declare @Dt as datetime = getutcdate();
			  Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Master\Tariff', @Dt ,'system', @NewQuotationNo OUTPUT
     
			  Exec [Master].[usp_TariffHeaderInsert] 
			  @NewQuotationNo, @Association, @EffectiveDate, @ExpiryDate, @CreatedBy,  @ModifiedBy,@NewQuotationNo = @NewQuotationNo OUTPUT  
		   END
		End
    
 END



	

-- ========================================================================================================================================
-- END  											 [Master].usp_[TariffHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TariffHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [TariffHeader] Record  based on [TariffHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TariffHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TariffHeaderDelete] 
END 
GO
CREATE PROC [Master].[usp_TariffHeaderDelete] 
    @QuotationNo nvarchar(50),
	@ModifiedBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[TariffHeader]
	SET	[IsActive] = CAST(0 as bit),ModifiedOn = GetUtcDate(),ModifiedBy = @ModifiedBy
	WHERE 	[QuotationNo] = @QuotationNo

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TariffHeaderDelete]
-- ========================================================================================================================================

GO 