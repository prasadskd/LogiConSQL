
-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivitySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyActivity] Record based on [CompanyActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyActivitySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivitySelect] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivitySelect] 
    @CompanyCode INT,
    @IsSuspended BIT,
    @ActivityDate DATETIME
AS 

BEGIN

	SELECT [CompanyCode], [IsSuspended], [ActivityDate],[Remarks], [CreatedBy], [CreatedOn] 
	FROM   [Master].[CompanyActivity]
	WHERE  [CompanyCode] = @CompanyCode  
	       AND [IsSuspended] = @IsSuspended  
	       AND [ActivityDate] = @ActivityDate 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivitySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyActivity] Records from [CompanyActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyActivityList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityList] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityList] 
    @CompanyCode INT
AS 
BEGIN

	SELECT [CompanyCode], [IsSuspended], [ActivityDate],[Remarks], [CreatedBy], [CreatedOn] 
	FROM   [Master].[CompanyActivity]
	WHERE  [CompanyCode] = @CompanyCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyActivity] Records from [CompanyActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyActivityPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityPageView] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityPageView] 
    @CompanyCode INT,
	@fetchrows bigint
AS 
BEGIN

	SELECT [CompanyCode], [IsSuspended], [ActivityDate],[Remarks], [CreatedBy], [CreatedOn] 
	FROM   [Master].[CompanyActivity]
	WHERE  [CompanyCode] = @CompanyCode  
	ORDER BY [ActivityDate] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanyActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyActivityRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityRecordCount] 
    @CompanyCode INT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[CompanyActivity]
	WHERE  [CompanyCode] = @CompanyCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyActivity] Record based on [CompanyActivity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyActivityAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityAutoCompleteSearch] 
    @ActivityDate DATETIME
AS 

BEGIN

	SELECT [CompanyCode], [IsSuspended], [ActivityDate],[Remarks], [CreatedBy], [CreatedOn] 
	FROM   [Master].[CompanyActivity]
	WHERE  [ActivityDate] = @ActivityDate  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanyActivity] Record Into [CompanyActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyActivityInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityInsert] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityInsert] 
    @CompanyCode int,
    @IsSuspended bit,
    @ActivityDate datetime,
	@Remarks nvarchar(255),
    @CreatedBy nvarchar(60) 
AS 
  

BEGIN

	
	INSERT INTO [Master].[CompanyActivity] ([CompanyCode], [IsSuspended], [ActivityDate], [CreatedBy], [CreatedOn])
	SELECT @CompanyCode, @IsSuspended, @ActivityDate, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanyActivity] Record Into [CompanyActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyActivityUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityUpdate] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityUpdate] 
    @CompanyCode int,
    @IsSuspended bit,
    @ActivityDate datetime,
	@Remarks nvarchar(255),
    @CreatedBy nvarchar(60) 
AS 
 
	
BEGIN

	UPDATE	[Master].[CompanyActivity]
	SET		[CompanyCode] = @CompanyCode, [IsSuspended] = @IsSuspended, [ActivityDate] = @ActivityDate, 
			[Remarks]= @Remarks,[CreatedBy] = @CreatedBy, [CreatedOn] = GETUTCDATE()
	WHERE	[CompanyCode] = @CompanyCode
			AND [IsSuspended] = @IsSuspended
			AND [ActivityDate] = @ActivityDate
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivitySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanyActivity] Record Into [CompanyActivity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyActivitySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivitySave] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivitySave] 
    @CompanyCode int,
    @IsSuspended bit,
    @ActivityDate datetime,
	@Remarks nvarchar(255),
    @CreatedBy nvarchar(60) 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CompanyActivity] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [IsSuspended] = @IsSuspended
	       AND [ActivityDate] = @ActivityDate)>0
	BEGIN
	    Exec [Master].[usp_CompanyActivityUpdate] 
		@CompanyCode, @IsSuspended, @ActivityDate,@Remarks, @CreatedBy


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CompanyActivityInsert] 
		@CompanyCode, @IsSuspended, @ActivityDate,@Remarks, @CreatedBy


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CompanyActivitySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyActivityDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanyActivity] Record  based on [CompanyActivity]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyActivityDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyActivityDelete] 
END 
GO
CREATE PROC [Master].[usp_CompanyActivityDelete] 
    @CompanyCode int,
    @IsSuspended bit,
    @ActivityDate datetime
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[CompanyActivity]
	WHERE  [CompanyCode] = @CompanyCode
	       AND [IsSuspended] = @IsSuspended
	       AND [ActivityDate] = @ActivityDate
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyActivityDelete]
-- ========================================================================================================================================

GO
 