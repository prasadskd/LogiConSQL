

-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Trailer] Record based on [Trailer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TrailerSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerSelect] 
END 
GO
CREATE PROC [Master].[usp_TrailerSelect] 
    @BranchID BIGINT,
    @TrailerID NVARCHAR(20)
AS 

BEGIN

	;With TrailerTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerType'),
	TrailerStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerStatusType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TrailerID], Tr.[TrailerType], Tr.[TrailerSize], Tr.[FreightMode], Tr.[RegistrationNo], Tr.[ChassisNo], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[TrailerStatus], Tr.[IsActive], Tr.[VendorCode], Tr.[BaseLocation], Tr.[CurrentLocation], Tr.[IsOwn], 
			Tr.[IsDGEquipped], Tr.[ManufactureDate], Tr.[NextServiceDate], Tr.[Compartment], Tr.[Axle], Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(TS.LookupDescription,'') As TrailerStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName 
	FROM	[Master].[Trailer] Tr 
	Left Outer Join TrailerTypeLookup DT ON 
		Tr.TrailerType = DT.LookupID
	Left Outer Join TrailerStatusTypeLookup TS ON
		Tr.TrailerStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE  Tr.[BranchID] = @BranchID  
	       AND Tr.[TrailerID] = @TrailerID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Trailer] Records from [Trailer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TrailerList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerList] 
END 
GO
CREATE PROC [Master].[usp_TrailerList] 
	@BranchID BIGINT 
AS 
BEGIN

	;With TrailerTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerType'),
	TrailerStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerStatusType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TrailerID], Tr.[TrailerType], Tr.[TrailerSize], Tr.[FreightMode], Tr.[RegistrationNo], Tr.[ChassisNo], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[TrailerStatus], Tr.[IsActive], Tr.[VendorCode], Tr.[BaseLocation], Tr.[CurrentLocation], Tr.[IsOwn], 
			Tr.[IsDGEquipped], Tr.[ManufactureDate], Tr.[NextServiceDate], Tr.[Compartment], Tr.[Axle], Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(TS.LookupDescription,'') As TrailerStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName 
	FROM	[Master].[Trailer] Tr 
	Left Outer Join TrailerTypeLookup DT ON 
		Tr.TrailerType = DT.LookupID
	Left Outer Join TrailerStatusTypeLookup TS ON
		Tr.TrailerStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE  Tr.[BranchID] = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Trailer] Records from [Trailer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TrailerPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerPageView] 
END 
GO
CREATE PROC [Master].[usp_TrailerPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;With TrailerTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerType'),
	TrailerStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerStatusType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TrailerID], Tr.[TrailerType], Tr.[TrailerSize], Tr.[FreightMode], Tr.[RegistrationNo], Tr.[ChassisNo], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[TrailerStatus], Tr.[IsActive], Tr.[VendorCode], Tr.[BaseLocation], Tr.[CurrentLocation], Tr.[IsOwn], 
			Tr.[IsDGEquipped], Tr.[ManufactureDate], Tr.[NextServiceDate], Tr.[Compartment], Tr.[Axle], Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(TS.LookupDescription,'') As TrailerStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName 
	FROM	[Master].[Trailer] Tr 
	Left Outer Join TrailerTypeLookup DT ON 
		Tr.TrailerType = DT.LookupID
	Left Outer Join TrailerStatusTypeLookup TS ON
		Tr.TrailerStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE  Tr.[BranchID] = @BranchID
	ORDER BY [TrailerID]   
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Trailer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TrailerRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerRecordCount] 
END 
GO
CREATE PROC [Master].[usp_TrailerRecordCount] 
	@BranchID BIGINT 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Trailer]
	WHERE BRANCHID=@BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Trailer] Record based on [Trailer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TrailerAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_TrailerAutoCompleteSearch] 
    @BranchID BIGINT,
    @TrailerID NVARCHAR(20)
AS 

BEGIN

	;With TrailerTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerType'),
	TrailerStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TrailerStatusType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TrailerID], Tr.[TrailerType], Tr.[TrailerSize], Tr.[FreightMode], Tr.[RegistrationNo], Tr.[ChassisNo], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[TrailerStatus], Tr.[IsActive], Tr.[VendorCode], Tr.[BaseLocation], Tr.[CurrentLocation], Tr.[IsOwn], 
			Tr.[IsDGEquipped], Tr.[ManufactureDate], Tr.[NextServiceDate], Tr.[Compartment], Tr.[Axle], Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(TS.LookupDescription,'') As TrailerStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName 
	FROM	[Master].[Trailer] Tr 
	Left Outer Join TrailerTypeLookup DT ON 
		Tr.TrailerType = DT.LookupID
	Left Outer Join TrailerStatusTypeLookup TS ON
		Tr.TrailerStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE  Tr.[BranchID] = @BranchID
	       AND Tr.[TrailerID] LIKE '%' + @TrailerID + '%'
		   And Tr.[IsActive]=Cast(1 as bit)
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Trailer] Record Into [Trailer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TrailerInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerInsert] 
END 
GO
CREATE PROC [Master].[usp_TrailerInsert] 
    @BranchID BIGINT,
    @TrailerID nvarchar(20),
    @TrailerType smallint,
    @TrailerSize varchar(2),
    @FreightMode smallint,
    @RegistrationNo nvarchar(20),
    @ChassisNo nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @TrailerStatus smallint,
    @IsActive bit,
    @VendorCode nvarchar(20),
    @BaseLocation nvarchar(20),
    @CurrentLocation nvarchar(20),
    @IsOwn bit,
    @IsDGEquipped bit,
    @ManufactureDate datetime,
    @NextServiceDate datetime,
    @Compartment int,
    @Axle int,
    @CreatedBy nvarchar(50),
    @ModifedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[Trailer] (
			[BranchID], [TrailerID], [TrailerType], [TrailerSize], [FreightMode], [RegistrationNo], [ChassisNo], [InsuranceExpiry], 
			[RoadTaxExpiry], [InspectionExpiry], [TrailerStatus], [IsActive], [VendorCode], [BaseLocation], [CurrentLocation], [IsOwn], [IsDGEquipped], 
			[ManufactureDate], [NextServiceDate], [Compartment], [Axle], [CreatedBy], [CreatedOn])
	SELECT 	@BranchID, @TrailerID, @TrailerType, @TrailerSize, @FreightMode, @RegistrationNo, @ChassisNo, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @TrailerStatus, @IsActive, @VendorCode, @BaseLocation, @CurrentLocation, @IsOwn, @IsDGEquipped, 
			@ManufactureDate, @NextServiceDate, @Compartment, @Axle, @CreatedBy, GETUTCDATE()     
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Trailer] Record Into [Trailer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TrailerUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerUpdate] 
END 
GO
CREATE PROC [Master].[usp_TrailerUpdate] 
    @BranchID BIGINT,
    @TrailerID nvarchar(20),
    @TrailerType smallint,
    @TrailerSize varchar(2),
    @FreightMode smallint,
    @RegistrationNo nvarchar(20),
    @ChassisNo nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @TrailerStatus smallint,
    @IsActive bit,
    @VendorCode nvarchar(20),
    @BaseLocation nvarchar(20),
    @CurrentLocation nvarchar(20),
    @IsOwn bit,
    @IsDGEquipped bit,
    @ManufactureDate datetime,
    @NextServiceDate datetime,
    @Compartment int,
    @Axle int,
    @CreatedBy nvarchar(50),
    @ModifedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[Trailer]
	SET		[TrailerType] = @TrailerType, [TrailerSize] = @TrailerSize, [FreightMode] = @FreightMode, [RegistrationNo] = @RegistrationNo, 
			[ChassisNo] = @ChassisNo, [InsuranceExpiry] = @InsuranceExpiry, [RoadTaxExpiry] = @RoadTaxExpiry, [InspectionExpiry] = @InspectionExpiry, 
			[TrailerStatus] = @TrailerStatus, [IsActive] = @IsActive, [VendorCode] = @VendorCode, [BaseLocation] = @BaseLocation, 
			[CurrentLocation] = @CurrentLocation, [IsOwn] = @IsOwn, [IsDGEquipped] = @IsDGEquipped, [ManufactureDate] = @ManufactureDate, 
			[NextServiceDate] = @NextServiceDate, [Compartment] = @Compartment, [Axle] = @Axle, [ModifedBy] = @ModifedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [TrailerID] = @TrailerID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Trailer] Record Into [Trailer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TrailerSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerSave] 
END 
GO
CREATE PROC [Master].[usp_TrailerSave] 
    @BranchID BIGINT,
    @TrailerID nvarchar(20),
    @TrailerType smallint,
    @TrailerSize varchar(2),
    @FreightMode smallint,
    @RegistrationNo nvarchar(20),
    @ChassisNo nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @TrailerStatus smallint,
    @IsActive bit,
    @VendorCode nvarchar(20),
    @BaseLocation nvarchar(20),
    @CurrentLocation nvarchar(20),
    @IsOwn bit,
    @IsDGEquipped bit,
    @ManufactureDate datetime,
    @NextServiceDate datetime,
    @Compartment int,
    @Axle int,
    @CreatedBy nvarchar(50),
    @ModifedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Trailer] 
		WHERE 	[BranchID] = @BranchID
	       AND [TrailerID] = @TrailerID)>0
	BEGIN
	    Exec [Master].[usp_TrailerUpdate] 
			@BranchID, @TrailerID, @TrailerType, @TrailerSize, @FreightMode, @RegistrationNo, @ChassisNo, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @TrailerStatus, @IsActive, @VendorCode, @BaseLocation, @CurrentLocation, @IsOwn, @IsDGEquipped, 
			@ManufactureDate, @NextServiceDate, @Compartment, @Axle, @CreatedBy, @ModifedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_TrailerInsert] 
			@BranchID, @TrailerID, @TrailerType, @TrailerSize, @FreightMode, @RegistrationNo, @ChassisNo, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @TrailerStatus, @IsActive, @VendorCode, @BaseLocation, @CurrentLocation, @IsOwn, @IsDGEquipped, 
			@ManufactureDate, @NextServiceDate, @Compartment, @Axle, @CreatedBy, @ModifedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[TrailerSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TrailerDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Trailer] Record  based on [Trailer]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TrailerDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TrailerDelete] 
END 
GO
CREATE PROC [Master].[usp_TrailerDelete] 
    @BranchID BIGINT,
    @TrailerID nvarchar(20)
AS 

	
BEGIN

	UPDATE	[Master].[Trailer]
	SET	isactive = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [TrailerID] = @TrailerID

	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TrailerDelete]
-- ========================================================================================================================================

GO

 
