


-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [SubPermitCode] Record based on [SubPermitCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_SubPermitCodeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeSelect] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeSelect] 
    @SubPermitCode NVARCHAR(10),
    @PermitCode NVARCHAR(10),
    @OGACode SMALLINT
AS 

BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT	Spc.[SubPermitCode] As Code, Spc.[PermitCode], Spc.[OGACode], Spc.[SubPermitDescription], Spc.[IsActive], 
			Spc.[CreatedBy], Spc.[CreatedOn], Spc.[ModifiedBy], Spc.[ModifiedOn],
			ISNULL(PC.PermitDescription,'') As PermitDescription,
			ISNULL(Oga.LookupDescription,'') As OGADescription
	FROM   [Master].[SubPermitCode] Spc
	Left Outer Join [Master].[PermitCategory] PC ON 
		Spc.PermitCode = PC.PermitCode
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	WHERE  Spc.[SubPermitCode] = @SubPermitCode  
	       AND Spc.[PermitCode] = @PermitCode  
	       AND Spc.[OGACode] = @OGACode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [SubPermitCode] Records from [SubPermitCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_SubPermitCodeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeList] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeList] 
    @PermitCode NVARCHAR(10)

AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT	Spc.[SubPermitCode] As Code, Spc.[PermitCode], Spc.[OGACode], Spc.[SubPermitDescription], Spc.[IsActive], 
			Spc.[CreatedBy], Spc.[CreatedOn], Spc.[ModifiedBy], Spc.[ModifiedOn],
			ISNULL(PC.PermitDescription,'') As PermitDescription,
			ISNULL(Oga.LookupDescription,'') As OGADescription
	FROM   [Master].[SubPermitCode] Spc
	Left Outer Join [Master].[PermitCategory] PC ON 
		Spc.PermitCode = PC.PermitCode
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Where SPc.PermitCode = @PermitCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [SubPermitCode] Records from [SubPermitCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_SubPermitCodePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodePageView] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodePageView]
	@PermitCode NVARCHAR(10),  
	@fetchrows bigint
AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT	Spc.[SubPermitCode] As Code, Spc.[PermitCode], Spc.[OGACode], Spc.[SubPermitDescription], Spc.[IsActive], 
			Spc.[CreatedBy], Spc.[CreatedOn], Spc.[ModifiedBy], Spc.[ModifiedOn],
			ISNULL(PC.PermitDescription,'') As PermitDescription,
			ISNULL(Oga.LookupDescription,'') As OGADescription
	FROM   [Master].[SubPermitCode] Spc
	Left Outer Join [Master].[PermitCategory] PC ON 
		Spc.PermitCode = PC.PermitCode
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Where Spc.PermitCode = @PermitCode
	ORDER BY [PermitCode] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [SubPermitCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_SubPermitCodeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeRecordCount] 
	@PermitCode NVARCHAR(10)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[SubPermitCode]
	Where PermitCode = @PermitCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [SubPermitCode] Record based on [SubPermitCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_SubPermitCodeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeAutoCompleteSearch] 
    @SubPermitDescription NVARCHAR(100),
    @PermitCode NVARCHAR(10)
AS 

BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT	Spc.[SubPermitCode] As Code, Spc.[PermitCode], Spc.[OGACode], Spc.[SubPermitDescription], Spc.[IsActive], 
			Spc.[CreatedBy], Spc.[CreatedOn], Spc.[ModifiedBy], Spc.[ModifiedOn],
			ISNULL(PC.PermitDescription,'') As PermitDescription,
			ISNULL(Oga.LookupDescription,'') As OGADescription
	FROM   [Master].[SubPermitCode] Spc
	Left Outer Join [Master].[PermitCategory] PC ON 
		Spc.PermitCode = PC.PermitCode
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	WHERE  Spc.[PermitCode] = @PermitCode 
			And Spc.SubPermitDescription like '%' + @SubPermitDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [SubPermitCode] Record Into [SubPermitCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_SubPermitCodeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeInsert] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeInsert] 
    @SubPermitCode nvarchar(10),
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @SubPermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[SubPermitCode] (
			[SubPermitCode], [PermitCode], [OGACode], [SubPermitDescription], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@SubPermitCode, @PermitCode, @OGACode, @SubPermitDescription, Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [SubPermitCode] Record Into [SubPermitCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_SubPermitCodeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeUpdate] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeUpdate] 
    @SubPermitCode nvarchar(10),
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @SubPermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[SubPermitCode]
	SET    [SubPermitDescription] = @SubPermitDescription, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [SubPermitCode] = @SubPermitCode
	       AND [PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [SubPermitCode] Record Into [SubPermitCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_SubPermitCodeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeSave] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeSave] 
    @SubPermitCode nvarchar(10),
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @SubPermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
    
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[SubPermitCode] 
		WHERE 	[SubPermitCode] = @SubPermitCode
	       AND [PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode)>0
	BEGIN
	    Exec [Master].[usp_SubPermitCodeUpdate] 
		@SubPermitCode, @PermitCode, @OGACode, @SubPermitDescription, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_SubPermitCodeInsert] 
		@SubPermitCode, @PermitCode, @OGACode, @SubPermitDescription, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[SubPermitCodeSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_SubPermitCodeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [SubPermitCode] Record  based on [SubPermitCode]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_SubPermitCodeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubPermitCodeDelete] 
END 
GO
CREATE PROC [Master].[usp_SubPermitCodeDelete] 
    @SubPermitCode nvarchar(10),
    @PermitCode nvarchar(10),
    @OGACode smallint,
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[SubPermitCode]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy,ModifiedOn = GETUTCDATE()
	WHERE 	[SubPermitCode] = @SubPermitCode
	       AND [PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[SubPermitCode]
	WHERE  [SubPermitCode] = @SubPermitCode
	       AND [PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode
	*/


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_SubPermitCodeDelete]
-- ========================================================================================================================================

GO

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

