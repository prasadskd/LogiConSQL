
-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateByCurrencyCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CustomCurrencyRate] Record based on [CustomCurrencyRate] table

-- Exec [Master].[usp_CustomCurrencyRateByCurrencyCode] 'MY','USD','2017-04-12'
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomCurrencyRateByCurrencyCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateByCurrencyCode] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateByCurrencyCode] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
	@CurrentDate datetime
AS 
 

BEGIN

	SELECT TOP(1) [CountryCode], [CurrencyCode], [StartDate], [EndDate], [ImportRate], [ExportRate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[CustomCurrencyRate]
	WHERE  [CountryCode] = @CountryCode   
	       AND [CurrencyCode] = @CurrencyCode  
	       AND  Convert(Char(10),@CurrentDate,120) Between Convert(Char(10),[StartDate],120)  And Convert(Char(10),[EndDate],120)
	Order By CreatedOn Desc 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateByCurrencyCode]
-- ========================================================================================================================================

GO

 