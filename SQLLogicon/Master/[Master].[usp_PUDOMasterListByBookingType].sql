-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterListByBookingType]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PUDOMaster] Record based on [BookingType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterListByBookingType]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterListByBookingType] 
END 
GO
CREATE PROC  [Master].[usp_PUDOMasterListByBookingType] 
@bookingType as smallint
AS 

BEGIN

 SELECT [BookingType], [OrderType], [PUDOMode], [MovementCode] 
 FROM   [Master].[PUDOMaster]
 Where [BookingType] = @bookingType

END
-- ========================================================================================================================================
-- END              [usp_PUDOMasterListByBookingType] 
-- ========================================================================================================================================