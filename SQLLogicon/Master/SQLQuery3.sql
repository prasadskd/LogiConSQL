-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSelectByCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Merchant] Record based on [Merchant] table

IF OBJECT_ID('[Master].[usp_MerchantSelectByCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantSelectByCode] 
END 
GO
CREATE PROC [Master].[usp_MerchantSelectByCode] 
    @MerchantCode BigInt 
AS 

SET NOCOUNT ON; 

BEGIN

 
	SELECT	[MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
			[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
			[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
			[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
			[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,BusinessType,IsFreightForwarder,IsOverseasAgent
	FROM	[Master].[Merchant]
	WHERE  [MerchantCode] = @MerchantCode  
		    

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantSelectByCode]
-- ========================================================================================================================================


 