

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanySubscription] Record based on [CompanySubscription] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionSelect] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionSelect] 
    @CompanyCode INT,
    @ModuleID SMALLINT
AS 

BEGIN

	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module')
	SELECT [CompanyCode], [ModuleID], [IsActive], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn],
	ISNULL(Md.LookupDescription,'') As ModuleDescription
	FROM   [Master].[CompanySubscription] Cs
	Left Outer Join ModuleDescription Md ON 
		Cs.ModuleID = Md.LookupID
	WHERE  [CompanyCode] = @CompanyCode  
	       AND [ModuleID] = @ModuleID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanySubscription] Records from [CompanySubscription] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionList] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionList] 
    @CompanyCode INT

AS 
BEGIN

	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module')
	SELECT [CompanyCode], [ModuleID], [IsActive], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn],
	ISNULL(Md.LookupDescription,'') As ModuleDescription
	FROM   [Master].[CompanySubscription] Cs
	Left Outer Join ModuleDescription Md ON 
		Cs.ModuleID = Md.LookupID
	Where CompanyCode = @CompanyCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanySubscription] Records from [CompanySubscription] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionPageView] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionPageView] 
    @CompanyCode INT,
	@fetchrows bigint
AS 
BEGIN

	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module')
	SELECT [CompanyCode], [ModuleID], [IsActive], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn],
	ISNULL(Md.LookupDescription,'') As ModuleDescription
	FROM   [Master].[CompanySubscription] Cs
	Left Outer Join ModuleDescription Md ON 
		Cs.ModuleID = Md.LookupID
	Where CompanyCode = @CompanyCode
	ORDER BY  [ModuleID] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanySubscription] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionRecordCount] 
    @CompanyCode INT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[CompanySubscription]
	Where CompanyCode = @CompanyCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanySubscription] Record based on [CompanySubscription] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionAutoCompleteSearch] 
    @CompanyCode INT 
AS 

BEGIN

	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module')
	SELECT [CompanyCode], [ModuleID], [IsActive], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn],
	ISNULL(Md.LookupDescription,'') As ModuleDescription
	FROM   [Master].[CompanySubscription] Cs
	Left Outer Join ModuleDescription Md ON 
		Cs.ModuleID = Md.LookupID
	WHERE  [CompanyCode] = @CompanyCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanySubscription] Record Into [CompanySubscription] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionInsert] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionInsert] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsActive bit,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[CompanySubscription] ([CompanyCode], [ModuleID], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @CompanyCode, @ModuleID, @IsActive, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanySubscription] Record Into [CompanySubscription] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionUpdate] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionUpdate] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsActive bit,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)

AS 
 
	
BEGIN

	UPDATE [Master].[CompanySubscription]
	SET    [IsActive] = @IsActive, [ModifedBy] = @ModifedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [CompanyCode] = @CompanyCode
	       AND [ModuleID] = @ModuleID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanySubscription] Record Into [CompanySubscription] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionSave] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionSave] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsActive bit,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CompanySubscription] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [ModuleID] = @ModuleID)>0
	BEGIN
	    Exec [Master].[usp_CompanySubscriptionUpdate] 
		@CompanyCode, @ModuleID, @IsActive, @CreatedBy, @ModifedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CompanySubscriptionInsert] 
		@CompanyCode, @ModuleID, @IsActive, @CreatedBy, @ModifedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CompanySubscriptionSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanySubscription] Record  based on [CompanySubscription]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionDelete] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionDelete] 
    @CompanyCode int 
AS 

	
BEGIN

	SET NOCOUNT ON;

	DELETE [Master].[CompanySubscription]
	WHERE 	[CompanyCode] = @CompanyCode
	      
	Select (1);
 
	SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionDelete]
-- ========================================================================================================================================

GO

 
