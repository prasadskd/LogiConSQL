

-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [IMOCode] Record based on [IMOCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_IMOCodeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeSelect] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeSelect] 
    @Code NVARCHAR(20)
AS 

BEGIN

	SELECT [Code], [Description], [IsDangerous], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifedOn] 
	FROM   [Master].[IMOCode]
	WHERE  [Code] = @Code 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [IMOCode] Records from [IMOCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_IMOCodeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeList] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeList] 

AS 
BEGIN

	SELECT [Code], [Description], [IsDangerous], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifedOn] 
	FROM   [Master].[IMOCode]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [IMOCode] Records from [IMOCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_IMOCodePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodePageView] 
END 
GO
CREATE PROC [Master].[usp_IMOCodePageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [Code], [Description], [IsDangerous], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifedOn] 
	FROM   [Master].[IMOCode]
	ORDER BY [Code]  
	OFFSET  @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [IMOCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_IMOCodeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[IMOCode]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [IMOCode] Record based on [IMOCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_IMOCodeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeAutoCompleteSearch] 
    @Code NVARCHAR(20)
AS 

BEGIN

	SELECT [Code], [Description], [IsDangerous], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifedOn] 
	FROM   [Master].[IMOCode]
	WHERE  [Code] LIKE '%' +  @Code + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [IMOCode] Record Into [IMOCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_IMOCodeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeInsert] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeInsert] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @IsDangerous bit,
    @ISOCode nvarchar(50),
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[IMOCode] ([Code], [Description], [IsDangerous], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn])
	SELECT @Code, @Description, @IsDangerous, @ISOCode, @MappingCode, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [IMOCode] Record Into [IMOCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_IMOCodeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeUpdate] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeUpdate] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @IsDangerous bit,
    @ISOCode nvarchar(50),
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[IMOCode]
	SET    [Description] = @Description, [IsDangerous] = @IsDangerous, [ISOCode] = @ISOCode, [MappingCode] = @MappingCode, 
			[ModifiedBy] = @ModifiedBy, [ModifedOn] = GETUTCDATE()
	WHERE  [Code] = @Code
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [IMOCode] Record Into [IMOCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_IMOCodeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeSave] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeSave] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @IsDangerous bit,
    @ISOCode nvarchar(50),
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[IMOCode] 
		WHERE 	[Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_IMOCodeUpdate] 
		@Code, @Description, @IsDangerous, @ISOCode, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_IMOCodeInsert] 
		@Code, @Description, @IsDangerous, @ISOCode, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[IMOCodeSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_IMOCodeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [IMOCode] Record  based on [IMOCode]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_IMOCodeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_IMOCodeDelete] 
END 
GO
CREATE PROC [Master].[usp_IMOCodeDelete] 
    @Code nvarchar(20)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[IMOCode]
	WHERE  [Code] = @Code
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_IMOCodeDelete]
-- ========================================================================================================================================

GO
 