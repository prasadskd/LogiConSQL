-- ========================================================================================================================================
-- START            [Master].[usp_CompanySelect]
-- ========================================================================================================================================
-- Author:  Vijay
-- Create date:  01-Jun-2016
-- Description: Select the [Company] Record based on [Company] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySelect] 
END 
GO
CREATE PROC [Master].[usp_CompanySelect] 
    @CompanyCode int
AS 
 

BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType],
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks,Rc.AgentCode, Rc.EDIMailBoxNo
	 FROM   [Master].[Company] Rc
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID
	 WHERE  [CompanyCode] = @CompanyCode 

END
-- ========================================================================================================================================
-- END              [Master].[usp_CompanySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START            [Master].[usp_CompanyList]
-- ========================================================================================================================================
-- Author:  Vijay
-- Create date:  01-Jun-2016
-- Description: Select all the [Company] Records from [Company] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyList] 
END 
GO
CREATE PROC [Master].[usp_CompanyList] 

AS 
 
BEGIN
	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType],
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks,Rc.AgentCode, Rc.EDIMailBoxNo
	 FROM   [Master].[Company] Rc
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID

END

-- ========================================================================================================================================
-- END              [Master].[usp_CompanyList] 
-- ========================================================================================================================================
GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyPageView] 
END 
GO
CREATE PROC [Master].[usp_CompanyPageView] 
	@fetchrows bigint
AS 
BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType], 
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks,Rc.AgentCode, Rc.EDIMailBoxNo
	 FROM   [Master].[Company] Rc
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID
	ORDER BY Rc.[CompanyName]
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyPageView] 
-- ========================================================================================================================================

GO

 


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Company] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CompanyRecordCount] 
	 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Company]  
	 
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Company] Records from [Company] table

-- Exec [Master].[usp_CompanyAutoCompleteSearch] 'what'
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CompanyAutoCompleteSearch] 
	 
	@CompanyName nvarchar(100)
AS 
BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType], 
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks,Rc.AgentCode, Rc.EDIMailBoxNo
	 FROM   [Master].[Company] Rc
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID
	WHERE  Rc.[CompanyName] Like '%' + @CompanyName  + '%'
		    
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_Company] 
-- ========================================================================================================================================

GO




--========================================================================================================================================
-- START            [Master].[usp_CompanyInsert]
-- ========================================================================================================================================
-- Author:  Vijay
-- Create date:  01-Jun-2016
-- Description: Inserts the [Company] Record Into [Company] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyInsert] 
END 
GO
CREATE PROC [Master].[usp_CompanyInsert] 
    @CompanyCode Int,
	@CompanyName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@RegistrationType smallint,
    @Logo nvarchar(100),
    @TaxId nvarchar(50),
    @IsActive bit,
	@RegistrationReferenceNo INT,
	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewCompanyCode int OUTPUT
AS 
  

BEGIN
 
 INSERT INTO [Master].[Company] (
			[CompanyCode],[CompanyName], [RegNo],GSTNo,RegistrationType, [Logo], [TaxId], [IsActive], [CreatedBy], [CreatedOn],RegistrationReferenceNo,AgentCode,EDIMailBoxNo)
 SELECT		@CompanyCode,@CompanyName, @RegNo,@GSTNo,@RegistrationType, @Logo, @TaxId, @IsActive, @CreatedBy, GETUTCDATE(),@RegistrationReferenceNo,@AgentCode,@EDIMailBoxNo
 
 Select @NewCompanyCode = @CompanyCode
               
END

-- ========================================================================================================================================
-- END              [Master].[usp_CompanyInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyUpdate]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	01-Jun-2016
-- Description:	updates the [Company] Record Into [Company] Table.

/*
Modification History 
Modified By		:	sharma
Modified ON		:	2017-04-22
Description		:	Added a new Update Function for branch to update the relevant information from Company to Branch (HQ ONLY)

*/

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyUpdate] 
END 
GO
CREATE PROC [Master].[usp_CompanyUpdate] 
    @CompanyCode Int,
	@CompanyName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@RegistrationType smallint,
    @Logo nvarchar(100),
    @TaxId nvarchar(50),
    @IsActive bit,
	@RegistrationReferenceNo INT,
 	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewCompanyCode int OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Master].[Company]
	SET		[CompanyCode] = @CompanyCode, [CompanyName] = @CompanyName, [RegNo] = @RegNo, GSTNo=@GSTNo, RegistrationType = @RegistrationType,
			[Logo] = @Logo, [TaxId] = @TaxId, [IsActive] = @IsActive, 
			AgentCode= @AgentCode,EDIMailBoxNo=@EDIMailBoxNo,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] =  GETUTCDATE(),RegistrationReferenceNo= @RegistrationReferenceNo
	WHERE  [CompanyCode] = @CompanyCode
	
	Declare @BranchID bigint=0,
			@NewBranchID bigint=0;

	
	Select @BranchID = BranchID
	From Master.Branch
	Where CompanyCode = @CompanyCode 
	And BranchCode ='HQ'

	Exec [Master].[usp_BranchUpdate] @BranchID,@CompanyCode,'HQ',@CompanyName,@RegNo,@GSTNo,@TaxID,@AgentCode,@EDIMailBoxNo,
			@CreatedBy,@ModifiedBy,@NewBranchID =@NewBranchID OUTPUT


	Select @NewCompanyCode = @CompanyCode
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyUpdate]
-- ========================================================================================================================================

GO



GO
-- ========================================================================================================================================
-- START            [Master].[usp_CompanySave]
-- ========================================================================================================================================
-- Author:  Vijay
-- Create date:  01-Jun-2016
-- Description: Either INSERT or UPDATE the [Company] Record Into [Company] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySave] 
END 
GO
CREATE PROC [Master].[usp_CompanySave] 
    @CompanyCode Int,
	@CompanyName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@RegistrationType smallint,
    @Logo nvarchar(100),
    @TaxId nvarchar(50),
    @IsActive bit,
	@RegistrationReferenceNo INT,
    @CreatedBy nvarchar(50),
	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @ModifiedBy nvarchar(50),
	@NewCompanyCode Int OUTPUT
AS 
 

BEGIN

 IF (SELECT COUNT(0) FROM [Master].[Company] 
  WHERE  [CompanyCode] = @CompanyCode)>0
 BEGIN
     Exec [Master].[usp_CompanyUpdate] 
		@CompanyCode, @CompanyName, @RegNo,@GSTNo,@RegistrationType, @Logo, @TaxId, @IsActive,@RegistrationReferenceNo,@AgentCode,@EDIMailBoxNo, @CreatedBy, @ModifiedBy,@NewCompanyCode = @NewCompanyCode OUTPUT


 END
 ELSE
 BEGIN

	Select @NewCompanyCode = ISNULL(Max(CompanyCode),999)+1 
	From Master.Company 

     Exec [Master].[usp_CompanyInsert] 
		@NewCompanyCode,@CompanyName, @RegNo,@GSTNo,@RegistrationType, @Logo, @TaxId, @IsActive,@RegistrationReferenceNo,@AgentCode,@EDIMailBoxNo, @CreatedBy, @ModifiedBy,@NewCompanyCode = @NewCompanyCode OUTPUT
 END
 
	


END

 

-- ========================================================================================================================================
-- END              [Master].usp_[CompanySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyDelete]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Company] Record  based on [Company]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyDelete] 
END 
GO
CREATE PROC [Master].[usp_CompanyDelete] 
    @CompanyCode Int
AS 

	
BEGIN

	UPDATE	[Master].[Company]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[CompanyCode] = @CompanyCode

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[Company]
	WHERE  [CompanyCode] = @CompanyCode
	*/
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyDelete]
-- ========================================================================================================================================

GO


IF OBJECT_ID('[Master].[usp_SubscriberInquiry]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_SubscriberInquiry] 
END 
GO
Create Proc [Master].[usp_SubscriberInquiry] 
 @CompanyCode Varchar(100),
 @FromData DateTime,
 @ToData DateTime,
 @ModuleID Varchar(50)

AS
BEGIN
Select MS.CompanyCode,MS.CompanyName,MS.RegNo,MS.GSTNo From [Master].[Company] as MS 
LEFT OUTER JOIN  [Master].[CompanySubscription] CS ON MS.CompanyCode=CS.CompanyCode
WHERE MS.CompanyCode=@CompanyCode AND MS.CreatedOn>= @FromData and MS.CreatedOn <= @ToData and CS.ModuleID=@ModuleID
END

go


IF OBJECT_ID('[Master].[usp_GetCompanyBillingStatus2]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GetCompanyBillingStatus2] 
END 
GO
CREATE PROCedure [Master].[usp_GetCompanyBillingStatus2](@CompanyID int)
As
Begin
	/*
	Select B.LookupID as ModuleID, B.LookupDescription as ModuleDescription, A.IsBillable as isSelected From (Select ModuleID, IsBillable From Master.CompanySubscriptionBillingStatus Where CompanyCode = @CompanyID
	Union 
	Select LookupID as ModuleID, 0 as IsActive From Config.Lookup Where LookupCategory = 'RegistrationType' and LookupID != 4053
	and LookupID not in (Select ModuleID From Master.CompanySubscriptionBillingStatus Where CompanyCode = @CompanyID)) as A
	inner join Config.Lookup as B On A.ModuleID = B.LookupID*/
	Select Tbl.ModuleID, Tbl.ModuleDescription, Tbl.isSelected, TDetail.Percentage as Rate From (Select B.LookupID as ModuleID, B.LookupDescription as ModuleDescription, A.IsBillable as isSelected From 
		(Select ModuleID, IsBillable From Master.CompanySubscriptionBillingStatus Where CompanyCode = @CompanyID
	Union 
	Select LookupID as ModuleID, 0 as IsActive From Config.Lookup Where LookupCategory = 'RegistrationType' and LookupID != 4053
	and LookupID not in (Select ModuleID From Master.CompanySubscriptionBillingStatus Where CompanyCode = @CompanyID)) as A
	inner join Config.Lookup as B On A.ModuleID = B.LookupID) as Tbl
	Left Outer Join Master.TariffDetail TDetail
	On Tbl.ModuleID = TDetail.Module
	Where TDetail.QuotationNo = 'STANDARD_1000001'
End



GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyUpdateAgentcodeAndMailBox]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	18-Apr-2017
-- Description:	updates the [Company] Record Into [Company] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyUpdateAgentcodeAndMailBox]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyUpdateAgentcodeAndMailBox] 
END 
GO
CREATE PROC [Master].[usp_CompanyUpdateAgentcodeAndMailBox] 
    @CompanyCode Int,
	@AgentCode nvarchar(20),
    @EDIMailBoxNo nvarchar(30)
AS 
 
	
BEGIN

	UPDATE	[Master].[Company]
	SET		AgentCode = @AgentCode, EDIMailBoxNo = @EDIMailBoxNo
	WHERE  [CompanyCode] = @CompanyCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyUpdateAgentcodeAndMailBox] 
-- ========================================================================================================================================

GO



GO

-- ========================================================================================================================================
-- START											 [Master].[usp_GetForwardingAgent]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	18-Apr-2017
-- Description:	Get forwarding agent branchid

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_GetForwardingAgent]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GetForwardingAgent] 
END 
GO
CREATE Proc [Master].[usp_GetForwardingAgent] 
 @MerchantCode bigint

AS
BEGIN
	if((Select count(0) From Master.Branch A
			INNER JOIN Master.Merchant M ON 
			A.CompanyCode = M.CompanyCode 
			And A.BranchName = M.MerchantName
			Where 
			M.MerchantCode =@MerchantCode)>0)
		Begin
			Select A.BranchID AS ForwardingAgentCount From Master.Branch A
			INNER JOIN Master.Merchant M ON 
			A.CompanyCode = M.CompanyCode 
			And A.BranchName = M.MerchantName
			Where 
			M.MerchantCode =@MerchantCode
		end
	else
		Begin
			Select 0 AS ForwardingAgentCount
		End
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyUpdateAgentcodeAndMailBox] 
-- ========================================================================================================================================

go
