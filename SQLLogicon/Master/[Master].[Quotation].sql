

-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Quotation] Record based on [Quotation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationSelect] 
END 
GO

CREATE PROC [Master].[usp_QuotationSelect]
    @BranchID BIGINT, 
    @QuotationNo VARCHAR(50)
AS 

SET NOCOUNT ON; 

BEGIN

	;With  
	QuotationTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='QuotationType')
	SELECT Hd.[BranchID], Hd.[QuotationNo],Hd.[QuotationDate], Hd.[AgentCode], Hd.[CustomerCode], Hd.[ForwarderCode], Hd.[EffectiveDate], Hd.[ExpiryDate], 
	Hd.[CargoStorageMethod], Hd.[CargoCalcPeriod], Hd.[IsBillingWarehouse], 
	Hd.[CargoFPImport], Hd.[CargoFPExport], Hd.[CargoFPLocal], Hd.[CargoFPImportDG], Hd.[CargoFPExportDG], Hd.[CargoFPLocalDG], 
	Hd.[ContainerFPImport], Hd.[ContainerFPExport], Hd.[ContainerFPLocal], Hd.[QuotationType],
	Hd.[ContainerFPImportDG], Hd.[ContainerFPExportDG], Hd.[ContainerFPLocalDG], 
	Hd.[ContainerFPImportRF], Hd.[ContainerFPExportRF], Hd.[ContainerFPLocalRF],
	Hd.[ContainerFPEmpty], Hd.[ContainerFPEmptyRF],
	Hd.[TrailerDetentionFreeDays], Hd.[IsActive], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[IsApproved], Hd.[ApprovedBy], 
	Hd.[ApprovedOn], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn],
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(Cst.MerchantName,'') As CustomerName,
	ISNULL(Fwd.MerchantName,'') As ForwarderName,
	ISNULL(QT.LookupDescription,'') As QuotationTypeDescription
	FROM   [Master].[Quotation] Hd
	Left Outer join Master.Merchant Agnt ON 
		hd.AgentCode = Agnt.MerchantCode
	Left Outer join Master.Merchant Cst ON 
			hd.CustomerCode = Cst.MerchantCode
	Left Outer join Master.Merchant Fwd ON 
		hd.[ForwarderCode] = Fwd.MerchantCode
	Left Outer Join QuotationTypeLookup QT ON 
		Hd.QuotationType = QT.LookupID
	WHERE  Hd.[BranchID] = @BranchID  
	       AND Hd.[QuotationNo] = @QuotationNo  

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Quotation] Records from [Quotation] table
-- [Master].[usp_QuotationList] 10
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationList] 
END 
GO
CREATE PROC [Master].[usp_QuotationList] 
	@BranchID smallint
AS 
BEGIN

SET NOCOUNT ON;

	;With  
	QuotationTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='QuotationType')
	SELECT Hd.[BranchID], Hd.[QuotationNo],Hd.[QuotationDate], Hd.[AgentCode], Hd.[CustomerCode], Hd.[ForwarderCode], Hd.[EffectiveDate], Hd.[ExpiryDate], 
	Hd.[CargoStorageMethod], Hd.[CargoCalcPeriod], Hd.[IsBillingWarehouse], 
	Hd.[CargoFPImport], Hd.[CargoFPExport], Hd.[CargoFPLocal], Hd.[CargoFPImportDG], Hd.[CargoFPExportDG], Hd.[CargoFPLocalDG], 
	Hd.[ContainerFPImport], Hd.[ContainerFPExport], Hd.[ContainerFPLocal], 
	Hd.[ContainerFPImportDG], Hd.[ContainerFPExportDG], Hd.[ContainerFPLocalDG], 
	Hd.[ContainerFPImportRF], Hd.[ContainerFPExportRF], Hd.[ContainerFPLocalRF],
	Hd.[ContainerFPEmpty], Hd.[ContainerFPEmptyRF],
	Hd.[TrailerDetentionFreeDays], Hd.[IsActive], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[IsApproved], Hd.[ApprovedBy], 
	Hd.[ApprovedOn], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn],
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(Cst.MerchantName,'') As CustomerName,
	ISNULL(Fwd.MerchantName,'') As ForwarderName,
	ISNULL(QT.LookupDescription,'') As QuotationTypeDescription
	FROM   [Master].[Quotation] Hd
	Left Outer join Master.Merchant Agnt ON 
		hd.AgentCode = Agnt.MerchantCode
	Left Outer join Master.Merchant Cst ON 
			hd.CustomerCode = Cst.MerchantCode
	Left Outer join Master.Merchant Fwd ON 
		hd.[ForwarderCode] = Fwd.MerchantCode
	Left Outer Join QuotationTypeLookup QT ON 
		Hd.QuotationType = QT.LookupID
	Where hd.BranchID = @BranchID

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Quotation] Records from [Quotation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationPageView] 
END 
GO
CREATE PROC [Master].[usp_QuotationPageView] 
	@fetchrows bigint
AS 
BEGIN

	;With  
	QuotationTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='QuotationType')
	SELECT Hd.[BranchID], Hd.[QuotationNo],Hd.[QuotationDate], Hd.[AgentCode], Hd.[CustomerCode], Hd.[ForwarderCode], Hd.[EffectiveDate], Hd.[ExpiryDate], 
	Hd.[CargoStorageMethod], Hd.[CargoCalcPeriod], Hd.[IsBillingWarehouse], 
	Hd.[CargoFPImport], Hd.[CargoFPExport], Hd.[CargoFPLocal], Hd.[CargoFPImportDG], Hd.[CargoFPExportDG], Hd.[CargoFPLocalDG], 
	Hd.[ContainerFPImport], Hd.[ContainerFPExport], Hd.[ContainerFPLocal], 
	Hd.[ContainerFPImportDG], Hd.[ContainerFPExportDG], Hd.[ContainerFPLocalDG], 
	Hd.[ContainerFPImportRF], Hd.[ContainerFPExportRF], Hd.[ContainerFPLocalRF],
	Hd.[ContainerFPEmpty], Hd.[ContainerFPEmptyRF],
	Hd.[TrailerDetentionFreeDays], Hd.[IsActive], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[IsApproved], Hd.[ApprovedBy], 
	Hd.[ApprovedOn], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn],
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(Cst.MerchantName,'') As CustomerName,
	ISNULL(Fwd.MerchantName,'') As ForwarderName,
	ISNULL(QT.LookupDescription,'') As QuotationTypeDescription
	FROM   [Master].[Quotation] Hd
	Left Outer join Master.Merchant Agnt ON 
		hd.AgentCode = Agnt.MerchantCode
	Left Outer join Master.Merchant Cst ON 
			hd.CustomerCode = Cst.MerchantCode
	Left Outer join Master.Merchant Fwd ON 
		hd.[ForwarderCode] = Fwd.MerchantCode
	Left Outer Join QuotationTypeLookup QT ON 
		Hd.QuotationType = QT.LookupID
	ORDER BY Hd.[QuotationNo]
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Quotation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationRecordCount] 
END 
GO
CREATE PROC [Master].[usp_QuotationRecordCount] 
	@BranchID smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Quotation]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Quotation] Records from [Quotation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_QuotationAutoCompleteSearch] 
	@BranchID smallint,
	@QuotationNo varchar(50)
AS 
BEGIN

	;With  
	QuotationTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='QuotationType')
	SELECT Hd.[BranchID], Hd.[QuotationNo],Hd.[QuotationDate], Hd.[AgentCode], Hd.[CustomerCode], Hd.[ForwarderCode], Hd.[EffectiveDate], Hd.[ExpiryDate], 
	Hd.[CargoStorageMethod], Hd.[CargoCalcPeriod], Hd.[IsBillingWarehouse], 
	Hd.[CargoFPImport], Hd.[CargoFPExport], Hd.[CargoFPLocal], Hd.[CargoFPImportDG], Hd.[CargoFPExportDG], Hd.[CargoFPLocalDG], 
	Hd.[ContainerFPImport], Hd.[ContainerFPExport], Hd.[ContainerFPLocal], 
	Hd.[ContainerFPImportDG], Hd.[ContainerFPExportDG], Hd.[ContainerFPLocalDG], 
	Hd.[ContainerFPImportRF], Hd.[ContainerFPExportRF], Hd.[ContainerFPLocalRF],
	Hd.[ContainerFPEmpty], Hd.[ContainerFPEmptyRF],
	Hd.[TrailerDetentionFreeDays], Hd.[IsActive], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[IsApproved], Hd.[ApprovedBy], 
	Hd.[ApprovedOn], Hd.[IsCancel], Hd.[CancelBy], Hd.[CancelOn],
	ISNULL(Agnt.MerchantName,'') As AgentName,
	ISNULL(Cst.MerchantName,'') As CustomerName,
	ISNULL(Fwd.MerchantName,'') As ForwarderName,
	ISNULL(QT.LookupDescription,'') As QuotationTypeDescription
	FROM   [Master].[Quotation] Hd
	Left Outer join Master.Merchant Agnt ON 
		hd.AgentCode = Agnt.MerchantCode
	Left Outer join Master.Merchant Cst ON 
			hd.CustomerCode = Cst.MerchantCode
	Left Outer join Master.Merchant Fwd ON 
		hd.[ForwarderCode] = Fwd.MerchantCode
	Left Outer Join QuotationTypeLookup QT ON 
		Hd.QuotationType = QT.LookupID
	WHERE Hd.BranchID = @BranchID 
	And Hd.QuotationNo LIKE '%' + @QuotationNo + '%'
	And Hd.[IsCancel]= Cast(0 as bit)

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationAutoCompleteSearch] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Quotation] Record Into [Quotation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationInsert] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailInsert] 
    @BranchID bigint,
    @QuotationNo varchar(50),
    @ItemId int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @ServiceType smallint,
    @IncoTerms nvarchar(10),
    @TransportType smallint,
    @TrailerType smallint,
    @SpecialHandling smallint,
    @CargoHandling smallint,
	@CargoType smallint,
    @Size varchar(10),
    @Type varchar(10),
    @PickupCode nvarchar(10),
    @DeliveryCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @SellRate numeric(18, 2),
    @EstCost numeric(18, 2),
    @MinQty numeric(18, 2),
    @MinAmt numeric(18, 2),
    @MaxAmnt numeric(18, 2),
    @PaymentTerm smallint,
    @BilledTo smallint,
    @IsSlabRate bit,
    @IsFlatRate bit,
    @IsRoundTrip bit,
    @SlabFrom int,
    @SlabTo int,
    @Commodity nvarchar(10),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN
 
	
	INSERT INTO [Master].[QuotationDetail] (
			[BranchID], [QuotationNo], [ItemId], [JobCategory], [JobType], [ShipmentType], [ChargeCode], [ServiceType], [IncoTerms], [TransportType], 
			[TrailerType], [SpecialHandling],[CargoHandling], [CargoType],  [Size], [Type], [PickupCode], [DeliveryCode], [BillingUnit], [CurrencyCode], [SellRate], 
			[EstCost], [MinQty], [MinAmt], [MaxAmnt], PaymentTerm, BilledTo, [IsSlabRate], [IsFlatRate], [IsRoundTrip], [SlabFrom], [SlabTo], 
			[Commodity], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @QuotationNo, @ItemId, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @ServiceType, @IncoTerms, @TransportType, 
			@TrailerType, @SpecialHandling, @CargoHandling,@CargoType, @Size, @Type, @PickupCode, @DeliveryCode, @BillingUnit, @CurrencyCode, @SellRate, 
			@EstCost, @MinQty, @MinAmt, @MaxAmnt, @PaymentTerm, @BilledTo, @IsSlabRate, @IsFlatRate, @IsRoundTrip, @SlabFrom, @SlabTo, 
			@Commodity, @Remarks, @CreatedBy, GETUTCDATE()
 	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Quotation] Record Into [Quotation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationUpdate] 
END 
GO
CREATE PROC [Master].[usp_QuotationUpdate] 
    @BranchID bigint,
    @QuotationNo varchar(50),
    @QuotationDate datetime,
	@QuotationType smallint,
	@AgentCode nvarchar(10),
    @CustomerCode nvarchar(10),
	@ForwarderCode nvarchar(10),
    @EffectiveDate datetime,
    @ExpiryDate datetime,
    @CargoStorageMethod smallint,
    @CargoCalcPeriod smallint,
    @IsBillingWarehouse bit,
    @CargoFPImport smallint,
    @CargoFPExport smallint,
    @CargoFPLocal smallint,
    @CargoFPImportDG smallint,
    @CargoFPExportDG smallint,
    @CargoFPLocalDG smallint,
    @ContainerFPImport smallint,
    @ContainerFPExport smallint,
    @ContainerFPLocal smallint,
    @ContainerFPImportDG smallint,
    @ContainerFPExportDG smallint,
    @ContainerFPLocalDG smallint,
	@ContainerFPImportRF smallint,
    @ContainerFPExportRF smallint,
    @ContainerFPLocalRF smallint,
    @ContainerFPEmpty smallint,
    @ContainerFPEmptyRF smallint,
    @TrailerDetentionFreeDays int,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewQuotationNo nvarchar(50) OUTPUT

AS 
 
	
BEGIN

SET NOCOUNT ON;

	UPDATE	[Master].[Quotation]
	SET		[EffectiveDate] = @EffectiveDate, [ExpiryDate] = @ExpiryDate, [CargoStorageMethod] = @CargoStorageMethod, 
			[AgentCode]=@AgentCode,[CustomerCode]=@CustomerCode,[ForwarderCode]=@ForwarderCode,[QuotationDate]=@QuotationDate,
			[CargoCalcPeriod] = @CargoCalcPeriod, [IsBillingWarehouse] = @IsBillingWarehouse, [CargoFPImport] = @CargoFPImport, [CargoFPExport] = @CargoFPExport, 
			[CargoFPLocal] = @CargoFPLocal, [CargoFPImportDG] = @CargoFPImportDG, [CargoFPExportDG] = @CargoFPExportDG, [CargoFPLocalDG] = @CargoFPLocalDG, 
			[ContainerFPImport] = @ContainerFPImport, [ContainerFPExport] = @ContainerFPExport, [ContainerFPLocal] = @ContainerFPLocal, 
			[ContainerFPImportDG] = @ContainerFPImportDG, [ContainerFPExportDG] = @ContainerFPExportDG, [ContainerFPLocalDG] = @ContainerFPLocalDG, 
			[ContainerFPImportRF] = @ContainerFPImportRF, [ContainerFPExportRF] = @ContainerFPExportRF, [ContainerFPLocalRF] = @ContainerFPLocalRF,
			[ContainerFPEmpty] = @ContainerFPEmpty , [ContainerFPEmptyRF] = @ContainerFPEmptyRF,
			[TrailerDetentionFreeDays] = @TrailerDetentionFreeDays, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [QuotationNo] = @QuotationNo

	select @NewQuotationNo = @QuotationNo   
	 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Quotation] Record Into [Quotation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationSave] 
END 
GO
CREATE [Master].[usp_QuotationSave] 
    @BranchID bigint,
    @QuotationNo varchar(50),
    @QuotationDate datetime,
	@QuotationType smallint,
	@AgentCode nvarchar(10),
    @CustomerCode nvarchar(10),
	@ForwarderCode nvarchar(10),
    @EffectiveDate datetime,
    @ExpiryDate datetime,
    @CargoStorageMethod smallint,
    @CargoCalcPeriod smallint,
    @IsBillingWarehouse bit,
    @CargoFPImport smallint,
    @CargoFPExport smallint,
    @CargoFPLocal smallint,
    @CargoFPImportDG smallint,
    @CargoFPExportDG smallint,
    @CargoFPLocalDG smallint,
    @ContainerFPImport smallint,
    @ContainerFPExport smallint,
    @ContainerFPLocal smallint,
    @ContainerFPImportDG smallint,
    @ContainerFPExportDG smallint,
    @ContainerFPLocalDG smallint,
	@ContainerFPImportRF smallint,
    @ContainerFPExportRF smallint,
    @ContainerFPLocalRF smallint,
    @ContainerFPEmpty smallint,
    @ContainerFPEmptyRF smallint,
    @TrailerDetentionFreeDays int,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewQuotationNo nvarchar(50) OUTPUT

AS 
 

BEGIN 
	Declare @Dt datetime,@DocID nvarchar(50), @vQuotationType smallint ;

	IF @QuotationNo = 'STANDARD_'+ Convert(varchar(15),@BranchID)
		Begin
			IF (SELECT COUNT(0) FROM [Master].[Quotation] WHERE [BranchID] = @BranchID AND [QuotationNo] = @QuotationNo) > 0
					Exec [Master].[usp_QuotationUpdate] 
						@BranchID, @QuotationNo,@QuotationDate,@QuotationType, @AgentCode, @CustomerCode,@ForwarderCode, @EffectiveDate, @ExpiryDate, 
						@CargoStorageMethod, @CargoCalcPeriod, @IsBillingWarehouse, 
						@CargoFPImport, @CargoFPExport, @CargoFPLocal, @CargoFPImportDG, @CargoFPExportDG, @CargoFPLocalDG, @ContainerFPImport, @ContainerFPExport, 
						@ContainerFPLocal, @ContainerFPImportDG, @ContainerFPExportDG, @ContainerFPLocalDG, 
						@ContainerFPImportRF, @ContainerFPExportRF,@ContainerFPLocalRF, @ContainerFPEmpty,@ContainerFPEmptyRF,
						@TrailerDetentionFreeDays, @CreatedBy, @ModifiedBy , @NewQuotationNo = @NewQuotationNo OUTPUT
			ELSE
				/*
				Select @vQuotationType = ISNULL(LookupID,0)
				From Config.Lookup Where LookupCategory ='QuotationType' And LookupCode ='STANDARD'

				If @QuotationType <> @vQuotationType
				Begin
					Select @QuotationNo='',@Dt=GETUTCDATE(),@DocID='Mst\Quotation'

					Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID , @Dt ,@CreatedBy, @QuotationNo OUTPUT
				ENd 
				*/
				Exec [Master].[usp_QuotationInsert] 
					@BranchID, @QuotationNo,@QuotationDate,@QuotationType, @AgentCode, @CustomerCode,@ForwarderCode, @EffectiveDate, @ExpiryDate, 
					@CargoStorageMethod, @CargoCalcPeriod, @IsBillingWarehouse, 
					@CargoFPImport, @CargoFPExport, @CargoFPLocal, @CargoFPImportDG, @CargoFPExportDG, @CargoFPLocalDG, @ContainerFPImport, @ContainerFPExport, 
					@ContainerFPLocal, @ContainerFPImportDG, @ContainerFPExportDG, @ContainerFPLocalDG, 
					@ContainerFPImportRF, @ContainerFPExportRF,@ContainerFPLocalRF, @ContainerFPEmpty,@ContainerFPEmptyRF,
					@TrailerDetentionFreeDays, @CreatedBy, @ModifiedBy , @NewQuotationNo = @NewQuotationNo OUTPUT
		END
	  ELSE


		   BEGIN
			IF (SELECT COUNT(0) FROM [Master].[Quotation] 
				WHERE 	[BranchID] = @BranchID
				   AND [QuotationNo] = @QuotationNo)>0
				Exec [Master].[usp_QuotationUpdate] 
					@BranchID, @QuotationNo,@QuotationDate,@QuotationType, @AgentCode, @CustomerCode,@ForwarderCode, @EffectiveDate, @ExpiryDate, 
					@CargoStorageMethod, @CargoCalcPeriod, @IsBillingWarehouse, 
					@CargoFPImport, @CargoFPExport, @CargoFPLocal, @CargoFPImportDG, @CargoFPExportDG, @CargoFPLocalDG, @ContainerFPImport, @ContainerFPExport, 
					@ContainerFPLocal, @ContainerFPImportDG, @ContainerFPExportDG, @ContainerFPLocalDG, 
					@ContainerFPImportRF, @ContainerFPExportRF,@ContainerFPLocalRF, @ContainerFPEmpty,@ContainerFPEmptyRF,
					@TrailerDetentionFreeDays, @CreatedBy, @ModifiedBy , @NewQuotationNo = @NewQuotationNo OUTPUT

	       ELSE
				BEGIN

				IF(Select COUNT(0) FROM [Master].[Quotation] Where  QuotationType=@QuotationType AND (EffectiveDate <= @EffectiveDate and ExpiryDate >= @ExpiryDate)) > 0
			  RaisError('A QUOTATION ALREADY EXISTS WITH THIS DATE RANGE',16,1)

					Select @vQuotationType = ISNULL(LookupID,0) From Config.Lookup Where LookupCategory ='QuotationType' And LookupCode ='STANDARD'

				If @QuotationType <> @vQuotationType
				Begin
					Select @QuotationNo='',@Dt=GETUTCDATE(),@DocID='Mst\Quotation'

					Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID , @Dt ,@CreatedBy, @QuotationNo OUTPUT
				END

				Exec [Master].[usp_QuotationInsert] 
					@BranchID, @QuotationNo,@QuotationDate,@QuotationType, @AgentCode, @CustomerCode,@ForwarderCode, @EffectiveDate, @ExpiryDate, 
					@CargoStorageMethod, @CargoCalcPeriod, @IsBillingWarehouse, 
					@CargoFPImport, @CargoFPExport, @CargoFPLocal, @CargoFPImportDG, @CargoFPExportDG, @CargoFPLocalDG, @ContainerFPImport, @ContainerFPExport, 
					@ContainerFPLocal, @ContainerFPImportDG, @ContainerFPExportDG, @ContainerFPLocalDG, 
					@ContainerFPImportRF, @ContainerFPExportRF,@ContainerFPLocalRF, @ContainerFPEmpty,@ContainerFPEmptyRF,
					@TrailerDetentionFreeDays, @CreatedBy, @ModifiedBy , @NewQuotationNo = @NewQuotationNo OUTPUT

			END
	 END
END


-- ========================================================================================================================================
-- END  											 [Master].usp_[QuotationSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Quotation] Record  based on [Quotation]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDelete] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailDelete] 
    @BranchID bigint,
    @QuotationNo varchar(50) 
AS 

	
BEGIN
SET NOCOUNT ON;
	 
	DELETE
	FROM   [Master].[QuotationDetail]
	WHERE  [BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	       
	 

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailDelete]
-- ========================================================================================================================================

GO
 