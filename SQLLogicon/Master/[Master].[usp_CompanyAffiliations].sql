-- ========================================================================================================================================
-- START											[Master].[usp_CompanyAffiliations]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	16-Apr-2017
-- Description:	Get The List of Company Affiliations

/*
Exec [Master].[usp_CompanyAffiliations] 1009 
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyAffiliations]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyAffiliations] 
END 
GO
CREATE PROC [Master].[usp_CompanyAffiliations]
    @CompanyCode INT 
AS 

BEGIN

Select Hd.AssociationID,Hd.AssociationName,Dt.MerchantCode,Dt.JoinDate,Dt.CompanyName,Dt.ROCNumber,Dt.Email
From [Master].[AssociationHeader] Hd
Inner Join [Master].[AssociationDetail] Dt ON 
Hd.AssociationID = Dt.AssociationID
Where 
Dt.MerchantCode = @CompanyCode

END
 