

-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobRules] Record based on [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesSelect] 
END 
GO
CREATE PROC [Master].[usp_JobRulesSelect] 
    @RuleType SMALLINT,
    @RuleDescription NVARCHAR(100),
    @Module SMALLINT
AS 

BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType'),
	ModuleLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module')
	SELECT [RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(Jc.LookupDescription,'') As JobRuleDescription,
	ISNULL(Ml.LookupDescription,'') As ModuleDescription
	FROM   [Master].[JobRules] Jr
	Left Outer Join JobCategoryRulesLookup Jc On 
		Jr.RuleType = Jc.LookupID
	Left Outer Join ModuleLookup Ml ON 
		Jr.Module = Ml.LookupID
	WHERE  Jr.[RuleType] = @RuleType  
	       AND Jr.[RuleDescription] = @RuleDescription  
	       AND Jr.[Module] = @Module   
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobRules] Records from [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesList] 
END 
GO
CREATE PROC [Master].[usp_JobRulesList] 

AS 
BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType'),
	ModuleLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module')
	SELECT [RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(Jc.LookupDescription,'') As JobRuleDescription,
	ISNULL(Ml.LookupDescription,'') As ModuleDescription
	FROM   [Master].[JobRules] Jr
	Left Outer Join JobCategoryRulesLookup Jc On 
		Jr.RuleType = Jc.LookupID
	Left Outer Join ModuleLookup Ml ON 
		Jr.Module = Ml.LookupID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesListByRuleType]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobRules] Records from [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesListByRuleType]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesListByRuleType] 
END 
GO
CREATE PROC [Master].[usp_JobRulesListByRuleType] 
	@RuleType Smallint
AS 
BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType'),
	ModuleLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module')
	SELECT [RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(Jc.LookupDescription,'') As JobRuleDescription,
	ISNULL(Ml.LookupDescription,'') As ModuleDescription
	FROM   [Master].[JobRules] Jr
	Left Outer Join JobCategoryRulesLookup Jc On 
		Jr.RuleType = Jc.LookupID
	Left Outer Join ModuleLookup Ml ON 
		Jr.Module = Ml.LookupID
	WHERE  Jr.[RuleType] = @RuleType
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesListByRuleType] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobRules] Records from [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesPageView] 
END 
GO
CREATE PROC [Master].[usp_JobRulesPageView] 
	@fetchrows bigint
AS 
BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType'),
	ModuleLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module')
	SELECT [RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(Jc.LookupDescription,'') As JobRuleDescription,
	ISNULL(Ml.LookupDescription,'') As ModuleDescription
	FROM   [Master].[JobRules] Jr
	Left Outer Join JobCategoryRulesLookup Jc On 
		Jr.RuleType = Jc.LookupID
	Left Outer Join ModuleLookup Ml ON 
		Jr.Module = Ml.LookupID
	ORDER BY [RuleDescription]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesRecordCount] 
END 
GO
CREATE PROC [Master].[usp_JobRulesRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[JobRules]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobRules] Record based on [JobRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobRulesAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_JobRulesAutoCompleteSearch] 
    @RuleType SMALLINT,
    @RuleDescription NVARCHAR(100)
AS 

BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType'),
	ModuleLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='Module')
	SELECT [RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],
	ISNULL(Jc.LookupDescription,'') As JobRuleDescription,
	ISNULL(Ml.LookupDescription,'') As ModuleDescription
	FROM   [Master].[JobRules] Jr
	Left Outer Join JobCategoryRulesLookup Jc On 
		Jr.RuleType = Jc.LookupID
	Left Outer Join ModuleLookup Ml ON 
		Jr.Module = Ml.LookupID
	WHERE  [RuleType] = @RuleType  
	       AND [RuleDescription] LIKE '%' +  @RuleDescription + '%'
	        
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobRules] Record Into [JobRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobRulesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesInsert] 
END 
GO
CREATE PROC [Master].[usp_JobRulesInsert] 
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @Module smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[JobRules] ([RuleType], [RuleDescription], [Module], [CreatedBy], [CreatedOn])
	SELECT @RuleType, @RuleDescription, @Module, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobRules] Record Into [JobRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobRulesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobRulesUpdate] 
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @Module smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[JobRules]
	SET    [RuleType] = @RuleType, [RuleDescription] = @RuleDescription, [Module] = @Module, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [RuleType] = @RuleType
	       AND [RuleDescription] = @RuleDescription
	       AND [Module] = @Module
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobRules] Record Into [JobRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobRulesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesSave] 
END 
GO
CREATE PROC [Master].[usp_JobRulesSave] 
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @Module smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[JobRules] 
		WHERE 	[RuleType] = @RuleType
	       AND [RuleDescription] = @RuleDescription
	       AND [Module] = @Module)>0
	BEGIN
	    Exec [Master].[usp_JobRulesUpdate] 
		@RuleType, @RuleDescription, @Module, @CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobRulesInsert] 
		@RuleType, @RuleDescription, @Module, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobRulesSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobRulesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobRules] Record  based on [JobRules]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobRulesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobRulesDelete] 
END 
GO
CREATE PROC [Master].[usp_JobRulesDelete] 
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @Module smallint
AS 

	
BEGIN
 
	DELETE
	FROM   [Master].[JobRules]
	WHERE  [RuleType] = @RuleType
	       AND [RuleDescription] = @RuleDescription
	       AND [Module] = @Module
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobRulesDelete]
-- ========================================================================================================================================

GO
 