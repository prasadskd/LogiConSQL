

-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [EquipmentSizeType] Record based on [EquipmentSizeType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeSelect] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeSelect] 
    @Code NVARCHAR(20)
AS 

BEGIN

	;With EquipmentSizeTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EquipmentType') 
	SELECT	[Code],[ISOCode],  [Description], [Size], [Type], [TareWeight],[NettWeight],[MaxGrossWeight], [Volume], [TEU], [EquipmentType], [IsReefer], [MappingCode], 
			[Height], [Length], [Width], [MaxPallet], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,
			ISNULL(EL.LookupDescription,'') As EquipmentTypeDescription 
	FROM	[Master].[EquipmentSizeType] E
	Left Outer Join EquipmentSizeTypeLookup EL ON 
		E.EquipmentType = EL.LookupID
	WHERE  [Code] = @Code  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [EquipmentSizeType] Records from [EquipmentSizeType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeList] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeList] 

AS 
BEGIN

	;With EquipmentSizeTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EquipmentType') 
	SELECT	[Code],[ISOCode],  [Description], [Size], [Type], [TareWeight],[NettWeight],[MaxGrossWeight], [Volume], [TEU], [EquipmentType], [IsReefer], [MappingCode], 
			[Height], [Length], [Width], [MaxPallet], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,
			ISNULL(EL.LookupDescription,'') As EquipmentTypeDescription 
	FROM	[Master].[EquipmentSizeType] E
	Left Outer Join EquipmentSizeTypeLookup EL ON 
		E.EquipmentType = EL.LookupID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypePageList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [EquipmentSizeType] Records from [EquipmentSizeType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_EquipmentSizeTypePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypePageView] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypePageView] 
	@fetchrows bigint
AS 
BEGIN

	;With EquipmentSizeTypeLookup As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='EquipmentType') 
	SELECT	[Code],[ISOCode],  [Description], [Size], [Type], [TareWeight],[NettWeight],[MaxGrossWeight], [Volume], [TEU], [EquipmentType], [IsReefer], [MappingCode], 
			[Height], [Length], [Width], [MaxPallet], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,
			ISNULL(EL.LookupDescription,'') As EquipmentTypeDescription 
	FROM	[Master].[EquipmentSizeType] E
	Left Outer Join EquipmentSizeTypeLookup EL ON 
		E.EquipmentType = EL.LookupID
	ORDER BY E.Code
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypePageList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [EquipmentSizeType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeRecordCount] 
	 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[EquipmentSizeType]
 
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeRecordCount] 
-- ========================================================================================================================================

GO



--========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [EquipmentSizeType] Record Into [EquipmentSizeType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeInsert] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeInsert] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @TareWeight nvarchar(20),
    @NettWeight nvarchar(20),
	@MaxGrossWeight nvarchar(20),
    @Volume nvarchar(20),
    @TEU int,
    @EquipmentType smallint,
    @IsReefer bit,
    @ISOCode nvarchar(20),
    @MappingCode nvarchar(20),
    @Height nvarchar(20),
    @Length nvarchar(20),
    @Width nvarchar(20),
    @MaxPallet nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[EquipmentSizeType] (
			[Code], [Description], [Size], [Type], [TareWeight],NettWeight,MaxGrossWeight, [Volume], [TEU], [EquipmentType], [IsReefer], [ISOCode], [MappingCode], 
			[Height], [Length], [Width], [MaxPallet], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@Code, @Description, @Size, @Type, @TareWeight,@NettWeight,@MaxGrossWeight, @Volume, @TEU, @EquipmentType, @IsReefer, @ISOCode, @MappingCode, 
			@Height, @Length, @Width, @MaxPallet, Cast(1 As Bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [EquipmentSizeType] Record Into [EquipmentSizeType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeUpdate] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeUpdate] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @TareWeight nvarchar(20),
    @NettWeight nvarchar(20),
	@MaxGrossWeight nvarchar(20),
    @Volume nvarchar(20),
    @TEU int,
    @EquipmentType smallint,
    @IsReefer bit,
    @ISOCode nvarchar(20),
    @MappingCode nvarchar(20),
    @Height nvarchar(20),
    @Length nvarchar(20),
    @Width nvarchar(20),
    @MaxPallet nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[EquipmentSizeType]
	SET		[Description] = @Description, [Size] = @Size, [Type] = @Type, [TareWeight] = @TareWeight,NettWeight = @NettWeight,MaxGrossWeight= @MaxGrossWeight, [Volume] = @Volume, [TEU] = @TEU, 
			[EquipmentType] = @EquipmentType, [IsReefer] = @IsReefer, [ISOCode] = @ISOCode, [MappingCode] = @MappingCode, 
			[Height] = @Height, [Length] = @Length, [Width] = @Width, [MaxPallet] = @MaxPallet, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[Code] = @Code
	And ISOCode = @ISOCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [EquipmentSizeType] Record Into [EquipmentSizeType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeSave] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeSave] 
    @Code nvarchar(20),
    @Description nvarchar(100),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @TareWeight nvarchar(20),
    @NettWeight nvarchar(20),
	@MaxGrossWeight nvarchar(20),
    @Volume nvarchar(20),
    @TEU int,
    @EquipmentType smallint,
    @IsReefer bit,
    @ISOCode nvarchar(20),
    @MappingCode nvarchar(20),
    @Height nvarchar(20),
    @Length nvarchar(20),
    @Width nvarchar(20),
    @MaxPallet nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[EquipmentSizeType] 
		WHERE 	[Code] = @Code
		And ISOCode = @ISOCode)>0
	BEGIN
	    Exec [Master].[usp_EquipmentSizeTypeUpdate] 
			@Code, @Description, @Size, @Type, @TareWeight,@NettWeight,@MaxGrossWeight, @Volume, @TEU, @EquipmentType, @IsReefer, @ISOCode, @MappingCode, 
			@Height, @Length, @Width, @MaxPallet, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_EquipmentSizeTypeInsert] 
			@Code, @Description, @Size, @Type, @TareWeight,@NettWeight,@MaxGrossWeight, @Volume, @TEU, @EquipmentType, @IsReefer, @ISOCode, @MappingCode, 
			@Height, @Length, @Width, @MaxPallet, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[EquipmentSizeTypeSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_EquipmentSizeTypeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [EquipmentSizeType] Record  based on [EquipmentSizeType]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_EquipmentSizeTypeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_EquipmentSizeTypeDelete] 
END 
GO
CREATE PROC [Master].[usp_EquipmentSizeTypeDelete] 
    @Code nvarchar(20),
	@ISOCode nvarchar(20)
AS 

	
BEGIN

	UPDATE	[Master].[EquipmentSizeType]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[Code] = @Code
	And ISOCode = @ISOCode
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_EquipmentSizeTypeDelete]
-- ========================================================================================================================================

GO

 