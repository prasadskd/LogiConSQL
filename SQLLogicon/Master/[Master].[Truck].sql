
-- ========================================================================================================================================
-- START											 [Master].[usp_TruckSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Truck] Record based on [Truck] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TruckSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckSelect] 
END 
GO
CREATE PROC [Master].[usp_TruckSelect] 
    @BranchID BIGINT,
    @TruckID NVARCHAR(20)
AS 

BEGIN

	;With TruckStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckStatusType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TruckID], Tr.[RegistrationNo], Tr.[TruckType], Tr.[Model], Tr.[EngineNo], Tr.[TruckStatus], Tr.[IsOwn], Tr.[IsActive], Tr.[IsDGEquipped], 
			Tr.[IsRequireTrailer], Tr.[FreightMode], Tr.[TruckSize], Tr.[VendorCode], Tr.[DriverID1], Tr.[DriverID2], Tr.[TrailerID1], Tr.[TrailerID2], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[BaseLocation], Tr.[Latitude], Tr.[Longitude], Tr.[Status], 
			Tr.[LastOdoMeter],Tr.[CurrentArea],Tr.[NextServiceKM],Tr.[NextServiceDate],Tr.[ManufactureDate],
			Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifiedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TruckTypeDescription,
			ISNULL(TS.LookupDescription,'') As TruckStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName  
	FROM	[Master].[Truck] Tr 
	Left Outer Join TruckTypeLookup DT ON 
		Tr.TruckType = DT.LookupID
	Left Outer Join TruckStatusTypeLookup TS ON
		Tr.TruckStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE	Tr.[BranchID] = @BranchID
	       AND Tr.[TruckID] = '' 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_TruckList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Truck] Records from [Truck] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TruckList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckList] 
END 
GO
CREATE PROC [Master].[usp_TruckList] 
    @BranchID BIGINT 

AS 
BEGIN

	;With TruckStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckStatusType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TruckID], Tr.[RegistrationNo], Tr.[TruckType], Tr.[Model], Tr.[EngineNo], Tr.[TruckStatus], Tr.[IsOwn], Tr.[IsActive], Tr.[IsDGEquipped], 
			Tr.[IsRequireTrailer], Tr.[FreightMode], Tr.[TruckSize], Tr.[VendorCode], Tr.[DriverID1], Tr.[DriverID2], Tr.[TrailerID1], Tr.[TrailerID2], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[BaseLocation], Tr.[Latitude], Tr.[Longitude], Tr.[Status], 
			Tr.[LastOdoMeter],Tr.[CurrentArea],Tr.[NextServiceKM],Tr.[NextServiceDate],Tr.[ManufactureDate],
			Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifiedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TruckTypeDescription,
			ISNULL(TS.LookupDescription,'') As TruckStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName  
	FROM	[Master].[Truck] Tr 
	Left Outer Join TruckTypeLookup DT ON 
		Tr.TruckType = DT.LookupID
	Left Outer Join TruckStatusTypeLookup TS ON
		Tr.TruckStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE	Tr.[BranchID] = @BranchID 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TruckPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Truck] Records from [Truck] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TruckPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckPageView] 
END 
GO
CREATE PROC [Master].[usp_TruckPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;With TruckStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckStatusType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TruckID], Tr.[RegistrationNo], Tr.[TruckType], Tr.[Model], Tr.[EngineNo], Tr.[TruckStatus], Tr.[IsOwn], Tr.[IsActive], Tr.[IsDGEquipped], 
			Tr.[IsRequireTrailer], Tr.[FreightMode], Tr.[TruckSize], Tr.[VendorCode], Tr.[DriverID1], Tr.[DriverID2], Tr.[TrailerID1], Tr.[TrailerID2], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[BaseLocation], Tr.[Latitude], Tr.[Longitude], Tr.[Status], 
			Tr.[LastOdoMeter],Tr.[CurrentArea],Tr.[NextServiceKM],Tr.[NextServiceDate],Tr.[ManufactureDate],
			Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifiedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TruckTypeDescription,
			ISNULL(TS.LookupDescription,'') As TruckStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName  
	FROM	[Master].[Truck] Tr 
	Left Outer Join TruckTypeLookup DT ON 
		Tr.TruckType = DT.LookupID
	Left Outer Join TruckStatusTypeLookup TS ON
		Tr.TruckStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE	Tr.[BranchID] = @BranchID 
	ORDER BY Tr.[TruckID]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TruckRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Truck] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TruckRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckRecordCount] 
END 
GO
CREATE PROC [Master].[usp_TruckRecordCount] 
    @BranchID BIGINT 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Truck]
	where BranchID=@BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TruckAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Truck] Record based on [Truck] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TruckAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_TruckAutoCompleteSearch] 
    @BranchID BIGINT,
    @TruckID NVARCHAR(20)
AS 

BEGIN

	;With TruckStatusTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckStatusType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Tr.[BranchID], Tr.[TruckID], Tr.[RegistrationNo], Tr.[TruckType], Tr.[Model], Tr.[EngineNo], Tr.[TruckStatus], Tr.[IsOwn], Tr.[IsActive], Tr.[IsDGEquipped], 
			Tr.[IsRequireTrailer], Tr.[FreightMode], Tr.[TruckSize], Tr.[VendorCode], Tr.[DriverID1], Tr.[DriverID2], Tr.[TrailerID1], Tr.[TrailerID2], Tr.[InsuranceExpiry], 
			Tr.[RoadTaxExpiry], Tr.[InspectionExpiry], Tr.[BaseLocation], Tr.[Latitude], Tr.[Longitude], Tr.[Status], 
			Tr.[LastOdoMeter],Tr.[CurrentArea],Tr.[NextServiceKM],Tr.[NextServiceDate],Tr.[ManufactureDate],
			Tr.[CreatedBy], Tr.[CreatedOn], Tr.[ModifiedBy], Tr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As TruckTypeDescription,
			ISNULL(TS.LookupDescription,'') As TruckStatusDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName  
	FROM	[Master].[Truck] Tr 
	Left Outer Join TruckTypeLookup DT ON 
		Tr.TruckType = DT.LookupID
	Left Outer Join TruckStatusTypeLookup TS ON
		Tr.TruckStatus = TS.LookupID
	Left Outer Join FreightModeLookup FM ON
		Tr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Tr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE	Tr.[BranchID] = @BranchID  
	       AND Tr.[TruckID] Like '%' +  @TruckID  + '%'
		   And Tr.[Status] = Cast(1 as bit)
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TruckInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Truck] Record Into [Truck] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TruckInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckInsert] 
END 
GO
CREATE PROC [Master].[usp_TruckInsert] 
    @BranchID BIGINT,
    @TruckID nvarchar(20),
    @RegistrationNo nvarchar(20),
    @TruckType smallint,
    @Model nvarchar(50),
    @EngineNo nvarchar(30),
    @TruckStatus smallint,
    @IsOwn bit,
    @IsActive bit,
    @IsDGEquipped bit,
    @IsRequireTrailer bit,
    @FreightMode smallint,
    @TruckSize nvarchar(20),
    @VendorCode nvarchar(10),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @TrailerID1 nvarchar(20),
    @TrailerID2 nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @BaseLocation nvarchar(50),
    @Latitude decimal(18, 6),
    @Longitude decimal(18, 6),
	@LastOdoMeter nvarchar(20),
	@CurrentArea nvarchar(50),
	@NextServiceKM nvarchar(10),
	@NextServiceDate datetime,
	@ManufactureDate datetime,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[Truck] (
			[BranchID], [TruckID], [RegistrationNo], [TruckType], [Model], [EngineNo], [TruckStatus], [IsOwn], [IsActive], [IsDGEquipped], 
			[IsRequireTrailer], [FreightMode], [TruckSize], [VendorCode], [DriverID1], [DriverID2], [TrailerID1], [TrailerID2], [InsuranceExpiry], 
			[RoadTaxExpiry], [InspectionExpiry], [BaseLocation], [Latitude], [Longitude], [Status], 
			[LastOdoMeter],[CurrentArea],[NextServiceKM],[NextServiceDate],[ManufactureDate],
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @TruckID, @RegistrationNo, @TruckType, @Model, @EngineNo, @TruckStatus, @IsOwn, @IsActive, @IsDGEquipped, 
			@IsRequireTrailer, @FreightMode, @TruckSize, @VendorCode, @DriverID1, @DriverID2, @TrailerID1, @TrailerID2, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @BaseLocation, @Latitude, @Longitude, CAST(1 AS BIT), 
			@LastOdoMeter,@CurrentArea,@NextServiceKM,@NextServiceDate,@ManufactureDate,
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TruckUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Truck] Record Into [Truck] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TruckUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckUpdate] 
END 
GO
CREATE PROC [Master].[usp_TruckUpdate] 
    @BranchID BIGINT,
    @TruckID nvarchar(20),
    @RegistrationNo nvarchar(20),
    @TruckType smallint,
    @Model nvarchar(50),
    @EngineNo nvarchar(30),
    @TruckStatus smallint,
    @IsOwn bit,
    @IsActive bit,
    @IsDGEquipped bit,
    @IsRequireTrailer bit,
    @FreightMode smallint,
    @TruckSize nvarchar(20),
    @VendorCode nvarchar(10),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @TrailerID1 nvarchar(20),
    @TrailerID2 nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @BaseLocation nvarchar(50),
    @Latitude decimal(18, 6),
    @Longitude decimal(18, 6),
	@LastOdoMeter nvarchar(20),
	@CurrentArea nvarchar(50),
	@NextServiceKM nvarchar(10),
	@NextServiceDate datetime,
	@ManufactureDate datetime,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[Truck]
	SET		[RegistrationNo] = @RegistrationNo, [TruckType] = @TruckType, [Model] = @Model, [EngineNo] = @EngineNo, [TruckStatus] = @TruckStatus, 
			[IsOwn] = @IsOwn, [IsActive] = @IsActive, [IsDGEquipped] = @IsDGEquipped, [IsRequireTrailer] = @IsRequireTrailer, 
			[FreightMode] = @FreightMode, [TruckSize] = @TruckSize, [VendorCode] = @VendorCode, [DriverID1] = @DriverID1, 
			[DriverID2] = @DriverID2, [TrailerID1] = @TrailerID1, [TrailerID2] = @TrailerID2, [InsuranceExpiry] = @InsuranceExpiry, 
			[RoadTaxExpiry] = @RoadTaxExpiry, [InspectionExpiry] = @InspectionExpiry, [BaseLocation] = @BaseLocation, 
			[Latitude] = @Latitude, [Longitude] = @Longitude, 
			LastOdoMeter=@LastOdoMeter,CurrentArea=@CurrentArea,NextServiceKM=@NextServiceKM,
			NextServiceDate=@NextServiceDate,ManufactureDate=@ManufactureDate,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [TruckID] = @TruckID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TruckSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Truck] Record Into [Truck] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TruckSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckSave] 
END 
GO
CREATE PROC [Master].[usp_TruckSave] 
    @BranchID BIGINT,
    @TruckID nvarchar(20),
    @RegistrationNo nvarchar(20),
    @TruckType smallint,
    @Model nvarchar(50),
    @EngineNo nvarchar(30),
    @TruckStatus smallint,
    @IsOwn bit,
    @IsActive bit,
    @IsDGEquipped bit,
    @IsRequireTrailer bit,
    @FreightMode smallint,
    @TruckSize nvarchar(20),
    @VendorCode nvarchar(10),
    @DriverID1 nvarchar(20),
    @DriverID2 nvarchar(20),
    @TrailerID1 nvarchar(20),
    @TrailerID2 nvarchar(20),
    @InsuranceExpiry datetime,
    @RoadTaxExpiry datetime,
    @InspectionExpiry datetime,
    @BaseLocation nvarchar(50),
    @Latitude decimal(18, 6),
    @Longitude decimal(18, 6),
	@LastOdoMeter nvarchar(20),
	@CurrentArea nvarchar(50),
	@NextServiceKM nvarchar(10),
	@NextServiceDate datetime,
	@ManufactureDate datetime,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Truck] 
		WHERE 	[BranchID] = @BranchID
	       AND [TruckID] = @TruckID)>0
	BEGIN
	    Exec [Master].[usp_TruckUpdate] 
			@BranchID, @TruckID, @RegistrationNo, @TruckType, @Model, @EngineNo, @TruckStatus, @IsOwn, @IsActive, @IsDGEquipped, 
			@IsRequireTrailer, @FreightMode, @TruckSize, @VendorCode, @DriverID1, @DriverID2, @TrailerID1, @TrailerID2, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @BaseLocation, @Latitude, @Longitude,
			@LastOdoMeter,@CurrentArea,@NextServiceKM,@NextServiceDate,@ManufactureDate,
			@CreatedBy,@ModifiedBy

	END
	ELSE
	BEGIN
	    Exec [Master].[usp_TruckInsert] 
			@BranchID, @TruckID, @RegistrationNo, @TruckType, @Model, @EngineNo, @TruckStatus, @IsOwn, @IsActive, @IsDGEquipped, 
			@IsRequireTrailer, @FreightMode, @TruckSize, @VendorCode, @DriverID1, @DriverID2, @TrailerID1, @TrailerID2, @InsuranceExpiry, 
			@RoadTaxExpiry, @InspectionExpiry, @BaseLocation, @Latitude, @Longitude,
			@LastOdoMeter,@CurrentArea,@NextServiceKM,@NextServiceDate,@ManufactureDate,
			@CreatedBy,@ModifiedBy

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[TruckSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_TruckDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Truck] Record  based on [Truck]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TruckDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TruckDelete] 
END 
GO
CREATE PROC [Master].[usp_TruckDelete] 
    @BranchID BIGINT,
    @TruckID nvarchar(20)
AS 

	
BEGIN

	UPDATE	[Master].[Truck]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [TruckID] = @TruckID

	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TruckDelete]
-- ========================================================================================================================================

GO

 