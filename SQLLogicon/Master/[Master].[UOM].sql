 

-- ========================================================================================================================================
-- START											 [Master].[usp_UOMSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [UOM] Record based on [UOM] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_UOMSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMSelect] 
END 
GO
CREATE PROC [Master].[usp_UOMSelect] 
    @UOMCode NVARCHAR(20)
AS 

BEGIN

	SELECT [UOMCode], [UOMDescription], [MappingCode], [ISOCode] 
	FROM   [Master].[UOM]
	WHERE  [UOMCode] = @UOMCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_UOMSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_UOMList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [UOM] Records from [UOM] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_UOMList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMList] 
END 
GO
CREATE PROC [Master].[usp_UOMList] 

AS 
BEGIN

	SELECT [UOMCode], [UOMDescription], [MappingCode], [ISOCode] 
	FROM   [Master].[UOM]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_UOMList] 
-- ========================================================================================================================================

GO

--========================================================================================================================================
-- START											 [Master].[usp_UOMInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [UOM] Record Into [UOM] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_UOMInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMInsert] 
END 
GO
CREATE PROC [Master].[usp_UOMInsert] 
    @UOMCode nvarchar(20),
    @UOMDescription nvarchar(100),
    @MappingCode nvarchar(20),
    @ISOCode nvarchar(20)
AS 
  

BEGIN

	
	INSERT INTO [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode])
	SELECT @UOMCode, @UOMDescription, @MappingCode, @ISOCode
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_UOMInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_UOMUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [UOM] Record Into [UOM] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_UOMUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMUpdate] 
END 
GO
CREATE PROC [Master].[usp_UOMUpdate] 
    @UOMCode nvarchar(20),
    @UOMDescription nvarchar(100),
    @MappingCode nvarchar(20),
    @ISOCode nvarchar(20)
AS 
 
	
BEGIN

	UPDATE [Master].[UOM]
	SET    [UOMDescription] = @UOMDescription, [MappingCode] = @MappingCode, [ISOCode] = @ISOCode
	WHERE  [UOMCode] = @UOMCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_UOMUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_UOMSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [UOM] Record Into [UOM] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_UOMSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMSave] 
END 
GO
CREATE PROC [Master].[usp_UOMSave] 
    @UOMCode nvarchar(20),
    @UOMDescription nvarchar(100),
    @MappingCode nvarchar(20),
    @ISOCode nvarchar(20)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[UOM] 
		WHERE 	[UOMCode] = @UOMCode)>0
	BEGIN
	    Exec [Master].[usp_UOMUpdate] 
		@UOMCode, @UOMDescription, @MappingCode, @ISOCode


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_UOMInsert] 
		@UOMCode, @UOMDescription, @MappingCode, @ISOCode


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[UOMSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_UOMDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [UOM] Record  based on [UOM]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_UOMDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_UOMDelete] 
END 
GO
CREATE PROC [Master].[usp_UOMDelete] 
    @UOMCode nvarchar(20)
AS 

	
BEGIN
 
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[UOM]
	WHERE  [UOMCode] = @UOMCode
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_UOMDelete]
-- ========================================================================================================================================

GO
 