-- ========================================================================================================================================
-- START											[Master].[usp_GetCompanySubscriptions]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	16-Feb-2017
-- Description:	Get The List of Company Subscriptions

/*
Exec [Master].[usp_GetCompanySubscriptions] 1009
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_GetCompanySubscriptions]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GetCompanySubscriptions]
END 
GO
CREATE PROC [Master].[usp_GetCompanySubscriptions]
	@CompanyID int 
As
Begin
	Select B.LookupID as ModuleID, B.LookupDescription as ModuleDescription, A.IsActive as isSelected From 
	(Select ModuleID, IsActive From Master.CompanySubscription Where CompanyCode = @CompanyID
	Union 
	Select LookupID as ModuleID, 0 as IsActive From Config.Lookup Where LookupCategory = 'RegistrationType' and LookupID != 4053
	and LookupID not in (Select ModuleID From Master.CompanySubscription Where CompanyCode = @CompanyID)
	) as A
	inner join Config.Lookup as B On A.ModuleID = B.LookupID
End

 
 