


-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [EmailSettings] Record based on [EmailSettings] table
-- ========================================================================================================================================


IF OBJECT_ID('[Config].[usp_EmailSettingsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsSelect] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsSelect] 
    @id int
AS 
 

BEGIN

	SELECT [id], [Host], [Port], [Username], [Password], [Transport], [Smtp], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Config].[EmailSettings]
	WHERE  [id] = @id 

END
-- ========================================================================================================================================
-- END  											 [Config].[usp_EmailSettingsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [EmailSettings] Records from [EmailSettings] table
-- ========================================================================================================================================


IF OBJECT_ID('[Config].[usp_EmailSettingsList]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsList] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsList] 

AS 
 
BEGIN
	SELECT [id], [Host], [Port], [Username], [Password], [Transport], [Smtp], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Config].[EmailSettings]

END

-- ========================================================================================================================================
-- END  											 [Config].[usp_EmailSettingsList] 
-- ========================================================================================================================================

GO

 
-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [EmailSettings] Record Into [EmailSettings] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Config].[usp_EmailSettingsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsInsert] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsInsert] 
    @Host nvarchar(100),
    @Port smallint,
    @Username nvarchar(100),
    @Password nvarchar(100),
    @Transport nvarchar(50),
    @Smtp bit,
    @CreatedBy nvarchar(100),
    @ModifiedBy nvarchar(100)
AS 
  

BEGIN
	
	INSERT INTO [Config].[EmailSettings] ([Host], [Port], [Username], [Password], [Transport], [Smtp], [CreatedBy], [CreatedOn] )
	SELECT @Host, @Port, @Username, @Password, @Transport, @Smtp, @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Config].[usp_EmailSettingsInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [EmailSettings] Record Into [EmailSettings] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Config].[usp_EmailSettingsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsUpdate] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsUpdate] 
    @id int,
    @Host nvarchar(100),
    @Port smallint,
    @Username nvarchar(100),
    @Password nvarchar(100),
    @Transport nvarchar(50),
    @Smtp bit,
    @CreatedBy nvarchar(100),
    @ModifiedBy nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [Config].[EmailSettings]
	SET    [Port] = @Port, [Username] = @Username, [Password] = @Password, [Transport] = @Transport, [Smtp] = @Smtp, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [id] = @id
	

END

-- ========================================================================================================================================
-- END  											 [Config].[usp_EmailSettingsUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [EmailSettings] Record Into [EmailSettings] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Config].[usp_EmailSettingsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsSave] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsSave]
    @id int,
    @Host nvarchar(100),
    @Port smallint,
    @Username nvarchar(100),
    @Password nvarchar(100),
    @Transport nvarchar(50),
    @Smtp bit,
    @CreatedBy nvarchar(100),
    @ModifiedBy nvarchar(100)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Config].[EmailSettings] 
		WHERE 	[id] = @id)>0
	BEGIN
	    Exec [Config].[usp_EmailSettingsUpdate] 
		@id, @Host, @Port, @Username, @Password, @Transport, @Smtp, @CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Config].[usp_EmailSettingsInsert] 
		@Host, @Port, @Username, @Password, @Transport, @Smtp, @CreatedBy, @ModifiedBy


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Config].usp_[EmailSettingsSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Config].[usp_EmailSettingsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [EmailSettings] Record  based on [EmailSettings]

-- ========================================================================================================================================

IF OBJECT_ID('[Config].[usp_EmailSettingsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_EmailSettingsDelete] 
END 
GO
CREATE PROC [Config].[usp_EmailSettingsDelete] 
    @id int
AS 

	
BEGIN

	DELETE	[Config].[EmailSettings]
	 
	WHERE 	[id] = @id

	 
END

-- ========================================================================================================================================
-- END  											 [Config].[usp_EmailSettingsDelete]
-- ========================================================================================================================================

GO 
