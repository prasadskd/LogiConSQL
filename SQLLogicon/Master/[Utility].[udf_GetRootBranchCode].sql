IF OBJECT_ID('[Utility].[udf_GetRootBranchCode]') IS NOT NULL
BEGIN 
    DROP FUNCTION [Utility].[udf_GetRootBranchCode]
END 
GO

/* Select [Utility].[udf_GetRootBranchCode]() */
CREATE FUNCTION [Utility].[udf_GetRootBranchCode]()
Returns BIGINT
As
Begin

Declare @result BIGINT = 0;

	;With CompanyType As (Select LookupId From Config.Lookup Where LookupCategory ='RegistrationType' And LookupCode ='REG-OW')
	Select @result = ISNULL(B.BranchID,0)
	From Master.Company C 
	Inner Join CompanyType L ON 
		C.RegistrationType = L.LookupId
	Inner Join Master.Branch B On 
		C.CompanyCode = B.CompanyCode
	where B.BranchCode = 'HQ'
	

	Return @result

End
 
   