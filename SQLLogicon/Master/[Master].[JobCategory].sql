


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategorySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategory] Record based on [JobCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategorySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategorySelect] 
END 
GO
CREATE PROC [Master].[usp_JobCategorySelect] 
    @BranchID BIGINT,
    @Code NVARCHAR(15)
AS 
 

BEGIN

	;With JobType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='JobType'),
	ShipmentType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderShipmentType'),
	FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='FreightMode'),
	BillingRuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='BillingRuleType'),
	TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode')
	SELECT	Jd.[BranchID], Jd.[Code], Jd.[Description], Jd.[JobType], Jd.[ShipmentType], Jd.[FreightMode], Jd.[BillingRule], Jd.[ChargePercent], Jd.[MinLeadTime], 
			Jd.[IsFreight], Jd.[IsForwarding], Jd.[IsHaulage], Jd.[IsTransport], Jd.[IsWarehouse], Jd.[IsCFS], Jd.[IsDepot], Jd.[IsRail], Jd.[IsActive], 
			Jd.[CreatedBy], Jd.[CreatedOn], Jd.[ModifiedBy], Jd.[ModifiedOn],Jd.[IsTrader], Jd.[IsMystats], Jd.[IsDeclaration], Jd.[IsAgent], Jd.[IsManifest], Jd.[IsPort],
			Jd.TransportMode,Jd.IsBilling,Jd.IsSales,
			ISNULL(M.LookupDescription,'') As JobTypeDescription,
			ISNULL(Pt.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Tm.LookupDescription,'') As FreightModeDescription,
			ISNULL(Fm.LookupDescription,'') As BillingRuleTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportModeDescription
	FROM	[Master].[JobCategory] Jd
	Left Outer Join JobType M On 
		M.LookupID = Jd.JobType
	Left Outer Join ShipmentType Pt On 
		Pt.LookupID = Jd.ShipmentType
	Left Outer Join BillingRuleType Tm On 
		Tm.LookupID = Jd.BillingRule
	Left Outer Join FreightMode Fm On 
		Fm.LookupID = Jd.FreightMode
	Left Outer Join TransportMode Tr ON 
		Jd.TransportMode = Tr.LookupID
	WHERE   (Jd.[BranchID] = @BranchID  OR Jd.BranchID = [Utility].[udf_GetRootBranchCode]())
	        AND Jd.[Code] = @Code  

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategorySelect]
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategory] Records from [JobCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryList] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryList] 

AS 
 
BEGIN
	;With JobType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='JobType'),
	ShipmentType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OrderShipmentType'),
	FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='FreightMode'),
	BillingRuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='BillingRuleType'),
	TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode')
	SELECT	Jd.[BranchID], Jd.[Code], Jd.[Description], Jd.[JobType], Jd.[ShipmentType], Jd.[FreightMode], Jd.[BillingRule], Jd.[ChargePercent], Jd.[MinLeadTime], 
			Jd.[IsFreight], Jd.[IsForwarding], Jd.[IsHaulage], Jd.[IsTransport], Jd.[IsWarehouse], Jd.[IsCFS], Jd.[IsDepot], Jd.[IsRail], Jd.[IsActive], 
			Jd.[CreatedBy], Jd.[CreatedOn], Jd.[ModifiedBy], Jd.[ModifiedOn],Jd.[IsTrader], Jd.[IsMystats], Jd.[IsDeclaration], Jd.[IsAgent], Jd.[IsManifest], Jd.[IsPort],
			Jd.TransportMode,Jd.IsBilling,Jd.IsSales,
			ISNULL(M.LookupDescription,'') As JobTypeDescription,
			ISNULL(Pt.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Tm.LookupDescription,'') As FreightModeDescription,
			ISNULL(Fm.LookupDescription,'') As BillingRuleTypeDescription,
			ISNULL(Tr.LookupDescription,'') As TransportModeDescription
	FROM	[Master].[JobCategory] Jd
	Left Outer Join JobType M On 
		M.LookupID = Jd.JobType
	Left Outer Join ShipmentType Pt On 
		Pt.LookupID = Jd.ShipmentType
	Left Outer Join BillingRuleType Tm On 
		Tm.LookupID = Jd.BillingRule
	Left Outer Join FreightMode Fm On 
		Fm.LookupID = Jd.FreightMode
	Left Outer Join TransportMode Tr ON 
		Jd.TransportMode = Tr.LookupID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryList] 
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobCategory] Record Into [JobCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryInsert] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryInsert] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @Description nvarchar(100),
    @JobType smallint,
    @ShipmentType smallint,
    @FreightMode smallint,
    @BillingRule smallint,
    @ChargePercent smallint,
    @MinLeadTime smallint,
    @IsFreight bit,
    @IsForwarding bit,
    @IsHaulage bit,
    @IsTransport bit,
    @IsWarehouse bit,
    @IsCFS bit,
    @IsDepot bit,
    @IsRail bit,
	@IsTrader bit,
	@IsMystats bit,
	@IsDeclaration bit,
	@IsAgent bit,
	@IsManifest bit,
	@IsPort bit,
	@TransportMode smallint,
	@IsBilling bit,
	@IsSales bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN
	
	INSERT INTO [Master].[JobCategory] (
			[BranchID], [Code], [Description], [JobType], [ShipmentType], [FreightMode], [BillingRule], [ChargePercent], [MinLeadTime], [IsFreight], 
			[IsForwarding], [IsHaulage], [IsTransport], [IsWarehouse], [IsCFS], [IsDepot], [IsRail], [IsTrader], [IsMystats], [IsDeclaration], [IsAgent], 
			[IsManifest], [IsPort],TransportMode,IsBilling,IsSales, [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @Code, @Description, @JobType, @ShipmentType, @FreightMode, @BillingRule, @ChargePercent, @MinLeadTime, @IsFreight, 
			@IsForwarding, @IsHaulage, @IsTransport, @IsWarehouse, @IsCFS, @IsDepot, @IsRail,@IsTrader, @IsMystats, @IsDeclaration, @IsAgent, 
			@IsManifest, @IsPort, @TransportMode,@IsBilling,@IsSales,@CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobCategory] Record Into [JobCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryUpdate] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @Description nvarchar(100),
    @JobType smallint,
    @ShipmentType smallint,
    @FreightMode smallint,
    @BillingRule smallint,
    @ChargePercent smallint,
    @MinLeadTime smallint,
    @IsFreight bit,
    @IsForwarding bit,
    @IsHaulage bit,
    @IsTransport bit,
    @IsWarehouse bit,
    @IsCFS bit,
    @IsDepot bit,
    @IsRail bit,
	@IsTrader bit,
	@IsMystats bit,
	@IsDeclaration bit,
	@IsAgent bit,
	@IsManifest bit,
	@IsPort bit,
	@TransportMode smallint,
	@IsBilling bit,
	@IsSales bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[JobCategory]
	SET		[Description] = @Description, [JobType] = @JobType, [ShipmentType] = @ShipmentType, [FreightMode] = @FreightMode, 
			[BillingRule] = @BillingRule, [ChargePercent] = @ChargePercent, [MinLeadTime] = @MinLeadTime, [IsFreight] = @IsFreight, 
			[IsForwarding] = @IsForwarding, [IsHaulage] = @IsHaulage, [IsTransport] = @IsTransport, [IsWarehouse] = @IsWarehouse, 
			[IsCFS] = @IsCFS, [IsDepot] = @IsDepot, [IsRail] = @IsRail, [IsTrader]=@IsTrader, [IsMystats]=@IsMystats, 
			[IsDeclaration]=@IsDeclaration, [IsAgent]=@IsAgent, [IsManifest]=@IsManifest, [IsPort]=@IsPort,  
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),IsBilling= @IsBilling,IsSales=@IsSales,
			[TransportMode]=@TransportMode
	WHERE	[BranchID] = @BranchID
			AND [Code] = @Code
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategorySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobCategory] Record Into [JobCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategorySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategorySave] 
END 
GO
CREATE PROC [Master].[usp_JobCategorySave] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @Description nvarchar(100),
    @JobType smallint,
    @ShipmentType smallint,
    @FreightMode smallint,
    @BillingRule smallint,
    @ChargePercent smallint,
    @MinLeadTime smallint,
    @IsFreight bit,
    @IsForwarding bit,
    @IsHaulage bit,
    @IsTransport bit,
    @IsWarehouse bit,
    @IsCFS bit,
    @IsDepot bit,
    @IsRail bit,
	@IsTrader bit,
	@IsMystats bit,
	@IsDeclaration bit,
	@IsAgent bit,
	@IsManifest bit,
	@IsPort bit,
	@TransportMode smallint,
	@IsBilling bit,
	@IsSales bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[JobCategory] 
		WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_JobCategoryUpdate] 
				@BranchID, @Code, @Description, @JobType, @ShipmentType, @FreightMode, @BillingRule, @ChargePercent, @MinLeadTime, @IsFreight, 
				@IsForwarding, @IsHaulage, @IsTransport, @IsWarehouse, @IsCFS, @IsDepot, @IsRail,@IsTrader, @IsMystats, @IsDeclaration, @IsAgent, 
				@IsManifest, @IsPort,@TransportMode ,@IsBilling,@IsSales,@CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobCategoryInsert] 
				@BranchID, @Code, @Description, @JobType, @ShipmentType, @FreightMode, @BillingRule, @ChargePercent, @MinLeadTime, @IsFreight, 
				@IsForwarding, @IsHaulage, @IsTransport, @IsWarehouse, @IsCFS, @IsDepot, @IsRail,@IsTrader, @IsMystats, @IsDeclaration, @IsAgent, 
				@IsManifest, @IsPort,@TransportMode ,@IsBilling,@IsSales,@CreatedBy, @ModifiedBy

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobCategorySave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobCategory] Record  based on [JobCategory]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryDelete] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryDelete] 
    @BranchID BIGINT,
    @Code nvarchar(15)
AS 

	
BEGIN

	UPDATE	[Master].[JobCategory]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code

	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryDelete]
-- ========================================================================================================================================

GO

 
