-- ========================================================================================================================================
-- START											[Master].[usp_CompanyCurrentMonthTariff]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	16-Apr-2017
-- Description:	Get The List of Company CurrentUsage Tariff Metrics for current month

/*
Exec [Master].[usp_CompanyCurrentMonthTariff] 1009 
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyCurrentMonthTariff]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyCurrentMonthTariff]
END 
GO
CREATE PROC [Master].[usp_CompanyCurrentMonthTariff]
    @CompanyCode INT 
AS 

BEGIN

Declare @UsersCount smallint=0,
		@UserTariffMode smallint=0,
		 
		@Percentage decimal(18,4),
		@ModuleTariffMode smallint=0,
		@StandardQuotationNo nvarchar(50) ='',
		@TariffQuotationNo nvarchar(50)='',
		@DeclarationModule smallint,
		@IsDeclarationSubscribed bit =0
		 


Declare @CurrentTariff Table
	(TariffType nvarchar(100),TariffDescription nvarchar(100),TariffRate decimal(18,2),Rebate decimal(18,2),DiscountPercent decimal(18,2),
		DiscountRate decimal(18,2),QuotationNo nvarchar(50))

--Select @CompanyCode =1009;

Select @UserTariffMode = LookupID From Config.Lookup Where LookupCategory ='TariffDiscountType' And  LookupDescription ='Users'
Select @ModuleTariffMode = LookupID From Config.Lookup Where LookupCategory ='TariffDiscountType' And  LookupDescription ='Modules'
Select @DeclarationModule = LookupID From Config.Lookup Where LookupCategory ='RegistrationType' And LookupDescription='DECLARATION'


Select TOP(1) @StandardQuotationNo = QuotationNo 
From Master.StandardTariffView
Order By EffectiveDate Desc

Select @UsersCount = ISNULL(Count(0),0) 
From [Security].[User] Where CompanyCode = @CompanyCode

Select @IsDeclarationSubscribed = COUNT(0)
From [Master].[CompanySubscriptionBillingStatus]
Where CompanyCode = @CompanyCode
And ModuleID = @DeclarationModule


;With CompanyAssociations As (Select Hd.AssociationID From [Master].[AssociationHeader] Hd
Inner Join [Master].[AssociationDetail] Dt ON 
Hd.AssociationID = Dt.AssociationID
Where 
Dt.MerchantCode = @CompanyCode)
Select  TOP(1) @TariffQuotationNo = ISNULL(QuotationNo,''),@Percentage=Percentage 
From Master.TariffView Tv
Left Outer Join CompanyAssociations Ca On 
Tv.Association = Ca.AssociationID
Where 
@UsersCount Between Tv.SlabFrom  And Tv.SlabTo
And Tv.TariffMode = @UserTariffMode
And Convert(char(10),Tv.EffectiveDate,120) <= Convert(Char(10),GetUTCDate(),120)
And Convert(Char(10),Tv.ExpiryDate,120) >= Convert(Char(10),GetUTCDate(),120)
Order By Percentage Desc  


 
If @TariffQuotationNo=''
	Set @TariffQuotationNo = @StandardQuotationNo

If @TariffQuotationNo <> @StandardQuotationNo
Begin
	Insert Into @CurrentTariff
	Select 'Users', 'Users : ' + Convert(varchar(10),Tv.SlabFrom) + '-' + Convert(varchar(10),Tv.SlabTo),Stv.Percentage,0.00,Tv.Percentage,
	(Stv.Percentage - ((Stv.Percentage*Tv.Percentage)/100)),@TariffQuotationNo
	From Master.StandardTariffView Stv
	Left Outer Join Master.Tariffview Tv ON 
	Stv.TariffMode = Tv.TariffMode
	And Stv.SlabFrom = Tv.SlabFrom
	And Stv.SlabTo = Tv.SlabTo
	Where 
	Tv.quotationNo = @TariffQuotationNo
	And Stv.QuotationNo =@StandardQuotationNo
	And Tv.TariffMode = @UserTariffMode
	And @UsersCount Between Tv.SlabFrom  And Tv.SlabTo

	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	Insert Into @CurrentTariff
	Select  'Modules' ,ISNULL(Dt.LookupDescription,''),Stv.Percentage,0.00,Tv.Percentage,
	(Stv.Percentage - ((Stv.Percentage*Tv.Percentage)/100)),
	@TariffQuotationNo 
	From Master.Tariffview Tv 
	INNER Join Master.StandardTariffView Stv ON 
		Stv.TariffMode = Tv.TariffMode
		And Stv.Module = Tv.Module
	Left Outer join ModuleDescription Dt ON 
	Stv.Module = Dt.LookupID
	Where
	Tv.Module > 0
	And Tv.TariffMode = @ModuleTariffMode
	And Stv.QuotationNo = @StandardQuotationNo
	And Tv.QuotationNo = @TariffQuotationNo

	If @IsDeclarationSubScribed = 1
	Begin
	 

	If(Select COUNT(0) From Master.TariffView Where QuotationNo =@TariffQuotationNo
		And Module = @DeclarationModule)>0
		Begin
			;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
			Insert Into @CurrentTariff
			Select  'Transactions' ,ISNULL(Dt.LookupDescription,''),Stv.Percentage,0.00,Tv.Percentage,
			(Stv.Percentage - ((Stv.Percentage*Tv.Percentage)/100)),
			@TariffQuotationNo 
			From Master.Tariffview Tv 
			Left Outer Join Master.StandardTariffView Stv ON 
				Stv.TariffMode = Tv.TariffMode
				And Stv.Module = Tv.Module
			Left Outer join ModuleDescription Dt ON 
			Stv.Module = Dt.LookupID
			Where
			Tv.Module > 0
			And Tv.TariffMode = @ModuleTariffMode
			And Stv.QuotationNo = @StandardQuotationNo
			And Tv.QuotationNo = @TariffQuotationNo
			And Tv.Module = @DeclarationModule
		End
	Else
		with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
		Insert Into @CurrentTariff
		Select  'Transactions',ISNULL(Dt.LookupDescription,''),Std.Percentage,0.00,0.00,Std.Percentage,@StandardQuotationNo
		From Master.StandardTariffView Std 
		Left Outer join ModuleDescription Dt ON 
		Std.Module = Dt.LookupID
		Where
		Std.Module > 0
		And Std.TariffMode = @ModuleTariffMode
		And Std.QuotationNo = @StandardQuotationNo
		And Std.Module = @DeclarationModule		

	End


End
Else
Begin
	Insert Into @CurrentTariff
	Select 'Users','Users : ' + Convert(varchar(10), SlabFrom) + '-' + Convert(varchar(10),SlabTo), Percentage,0.00,0.00,Stv.Percentage ,@TariffQuotationNo 
	From Master.StandardTariffView Stv
	Where 
	Stv.QuotationNo =@StandardQuotationNo
	And Stv.TariffMode = @UserTariffMode
	And @UsersCount Between Stv.SlabFrom  And Stv.SlabTo



	;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
	Insert Into @CurrentTariff
	Select  'Modules',ISNULL(Dt.LookupDescription,''),Std.Percentage,0.00,0.00,Std.Percentage,@StandardQuotationNo
	From Master.StandardTariffView Std 
	Left Outer join ModuleDescription Dt ON 
	Std.Module = Dt.LookupID
	Where
	Std.Module > 0
	And Std.TariffMode = @ModuleTariffMode
	And Std.QuotationNo = @StandardQuotationNo

	If @IsDeclarationSubScribed = 1
	Begin
		;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
		Insert Into @CurrentTariff
		Select  'Transactions',ISNULL(Dt.LookupDescription,''),Std.Percentage,0.00,0.00,Std.Percentage,@StandardQuotationNo
		From Master.StandardTariffView Std 
		Left Outer join ModuleDescription Dt ON 
		Std.Module = Dt.LookupID
		Where
		Std.Module > 0
		And Std.TariffMode = @ModuleTariffMode
		And Std.QuotationNo = @StandardQuotationNo
		And Std.Module = @DeclarationModule


	End

End

 
  
Select * From @CurrentTariff

END