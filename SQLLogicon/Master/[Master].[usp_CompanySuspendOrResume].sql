

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySuspendOrResume]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	01-Jun-2016
-- Description:	Suspend or Resume Company!

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySuspendOrResume]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySuspendOrResume] 
END 
GO
CREATE PROC [Master].[usp_CompanySuspendOrResume] 
    @CompanyCode Int,
	@IsSuspended bit,
	@SuspensionRemarks nvarchar(255), 
	@SuspendedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[Company]
	SET	IsSuspended = @IsSuspended,SuspensionRemarks = @SuspensionRemarks
	WHERE 	[CompanyCode] = @CompanyCode

	Insert Into Master.CompanyActivity
	Select @CompanyCode,@IsSuspended,GetUTCDate(),@SuspensionRemarks,@SuspendedBy,GetUTCDate()

	Return 1;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySuspendOrResume]
-- ========================================================================================================================================

GO

