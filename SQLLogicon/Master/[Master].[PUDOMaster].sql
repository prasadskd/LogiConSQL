 


-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PUDOMaster] Record based on [PUDOMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterSelect] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterSelect] 
	@BranchID smallint,
    @JobType SMALLINT,
    @OrderType nVARCHAR(15),
    @PUDOMode SMALLINT,
    @MovementCode nVARCHAR(50)
AS 

BEGIN

 

	;With 
	JobTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	PUDOModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='PUDOMode')
	SELECT PM.BranchID,PM.[JobType], [OrderType], [PUDOMode], [MovementCode] ,
	ISNULL(Bk.LookupDescription,'') As JobTypeDescription,
	ISNULL(PUDO.LookupDescription,'') As PUDOModeDescription,
	ISNULL(JC.Description,'') As OrderTypeDescription
	FROM   [Master].[PUDOMaster] PM
	Left Outer Join JobTypeLookup Bk ON 
		PM.JobType = Bk.LookupID
	Left Outer Join PUDOModeLookup PUDO ON 
		PM.PUDOMode = PUDO.LookupID
	Left Outer Join Master.JobCategory JC ON 
		PM.OrderType = JC.Code
	WHERE  PM.BranchID= @BranchID And PM.[JobType] = @JobType  
	       AND [OrderType] = @OrderType  
	       AND [PUDOMode] = @PUDOMode  
	       AND [MovementCode] = @MovementCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PUDOMaster] Records from [PUDOMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterList] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterList] 
	@BranchID smallint
AS 
BEGIN

	;With 
	JobTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	PUDOModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='PUDOMode')
	SELECT PM.BranchID,PM.[JobType], [OrderType], [PUDOMode], [MovementCode] ,
	ISNULL(Bk.LookupDescription,'') As JobTypeDescription,
	ISNULL(PUDO.LookupDescription,'') As PUDOModeDescription,
	ISNULL(JC.Description,'') As OrderTypeDescription
	FROM   [Master].[PUDOMaster] PM
	Left Outer Join JobTypeLookup Bk ON 
		PM.JobType = Bk.LookupID
	Left Outer Join PUDOModeLookup PUDO ON 
		PM.PUDOMode = PUDO.LookupID
	Left Outer Join Master.JobCategory JC ON 
		PM.OrderType = JC.Code
	where PM.BranchID= @BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PUDOMaster] Records from [PUDOMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterPageView] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterPageView] 
	@BranchID smallint,
	@fetchrows bigint
AS 
BEGIN

		;With 
	JobTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	PUDOModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='PUDOMode')
	SELECT PM.BranchID,PM.[JobType], [OrderType], [PUDOMode], [MovementCode] ,
	ISNULL(Bk.LookupDescription,'') As JobTypeDescription,
	ISNULL(PUDO.LookupDescription,'') As PUDOModeDescription,
	ISNULL(JC.Description,'') As OrderTypeDescription
	FROM   [Master].[PUDOMaster] PM
	Left Outer Join JobTypeLookup Bk ON 
		PM.JobType = Bk.LookupID
	Left Outer Join PUDOModeLookup PUDO ON 
		PM.PUDOMode = PUDO.LookupID
	Left Outer Join Master.JobCategory JC ON 
		PM.OrderType = JC.Code
	Where PM.BranchID= @BranchID
	ORDER BY [PUDOMode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [PUDOMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterRecordCount] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterRecordCount] 
	@BranchID smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[PUDOMaster]
	Where BranchID= @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PUDOMaster] Record based on [PUDOMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PUDOMasterAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterAutoCompleteSearch] 
    @BranchID smallint,
    @PUDOMode SMALLINT 
AS 

BEGIN

	;With 
	JobTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	PUDOModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='PUDOMode')
	SELECT PM.BranchID,PM.[JobType], [OrderType], [PUDOMode], [MovementCode] ,
	ISNULL(Bk.LookupDescription,'') As JobTypeDescription,
	ISNULL(PUDO.LookupDescription,'') As PUDOModeDescription,
	ISNULL(JC.Description,'') As OrderTypeDescription
	FROM   [Master].[PUDOMaster] PM
	Left Outer Join JobTypeLookup Bk ON 
		PM.JobType = Bk.LookupID
	Left Outer Join PUDOModeLookup PUDO ON 
		PM.PUDOMode = PUDO.LookupID
	Left Outer Join Master.JobCategory JC ON 
		PM.OrderType = JC.Code
	WHERE PM.BranchID= @BranchID And [PUDOMode] =  @PUDOMode
	        
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [PUDOMaster] Record Into [PUDOMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PUDOMasterInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterInsert] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterInsert] 
    @BranchID smallint,
	@JobType smallint,
    @OrderType nvarchar(15),
    @PUDOMode smallint,
    @MovementCode nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[PUDOMaster] (BranchID, [JobType], [OrderType], [PUDOMode], [MovementCode])
	SELECT @BranchID,@JobType, @OrderType, @PUDOMode, @MovementCode
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [PUDOMaster] Record Into [PUDOMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PUDOMasterUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterUpdate] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterUpdate] 
    @BranchID smallint,
	@JobType smallint,
    @OrderType varchar(15),
    @PUDOMode smallint,
    @MovementCode varchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[PUDOMaster]
	SET    [JobType] = @JobType, [OrderType] = @OrderType, [PUDOMode] = @PUDOMode, [MovementCode] = @MovementCode
	WHERE  BranchID= @BranchID AND [JobType] = @JobType
	       AND [OrderType] = @OrderType
	       AND [PUDOMode] = @PUDOMode
	       AND [MovementCode] = @MovementCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [PUDOMaster] Record Into [PUDOMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PUDOMasterSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterSave] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterSave] 
    @BranchID smallint,
	@JobType smallint,
    @OrderType varchar(15),
    @PUDOMode smallint,
    @MovementCode varchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[PUDOMaster] 
		WHERE 	BranchID= @BranchID
		   AND  [JobType] = @JobType
	       AND [OrderType] = @OrderType
	       AND [PUDOMode] = @PUDOMode
	       AND [MovementCode] = @MovementCode)>0
	BEGIN
	    Exec [Master].[usp_PUDOMasterUpdate] 
		@BranchID,@JobType, @OrderType, @PUDOMode, @MovementCode


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_PUDOMasterInsert] 
		@BranchID,@JobType, @OrderType, @PUDOMode, @MovementCode


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[PUDOMasterSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PUDOMasterDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [PUDOMaster] Record  based on [PUDOMaster]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PUDOMasterDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PUDOMasterDelete] 
END 
GO
CREATE PROC [Master].[usp_PUDOMasterDelete] 
    @BranchID smallint,
	@JobType smallint,
    @OrderType varchar(15),
    @PUDOMode smallint,
    @MovementCode varchar(50)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[PUDOMaster]
	WHERE  BranchID = @BranchID
		   AND [JobType] = @JobType
	       AND [OrderType] = @OrderType
	       AND [PUDOMode] = @PUDOMode
	       AND [MovementCode] = @MovementCode
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PUDOMasterDelete]
-- ========================================================================================================================================

GO
 