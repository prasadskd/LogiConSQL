
-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantListByCompany]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Merchant] Records from [Merchant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantListByCompany]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantListByCompany] 
END 
GO
CREATE PROC [Master].[usp_MerchantListByCompany] 
@CompanyCode bigint
As
BEGIN
 	
	Declare @DNeXCompanyID bigint = 1000;


	SELECT	[MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
			[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
			[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
			[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
			[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],BusinessType,
			IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal
	FROM	[Master].[Merchant]
	WHERE 
	CompanyCode = @CompanyCode
	And IsActive = CAST(1 as bit)
	UNION ALL
	SELECT	[MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
			[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
			[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
			[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
			[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],BusinessType,
			IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal
	FROM	[Master].[Merchant]
	WHERE 
	CompanyCode = @DNeXCompanyID
	And IsActive = CAST(1 as bit)
	And IsVerified = Cast(1 as bit)
	And CompanyCode NOT IN (@CompanyCode)	 

END
GO