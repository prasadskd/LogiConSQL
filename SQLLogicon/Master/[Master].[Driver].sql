
-- ========================================================================================================================================
-- START											 [Master].[usp_DriverSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Driver] Record based on [Driver] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverSelect] 
END 
GO
CREATE PROC [Master].[usp_DriverSelect] 
    @BranchID BIGINT,
    @DriverID NVARCHAR(20)
AS 

BEGIN

	;With DriverTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='DriverType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Dr.[BranchID], Dr.[DriverID], Dr.[DriverName], Dr.[DriverType], Dr.[IsDriver], Dr.[ICNo], Dr.[LicenseNo], Dr.[LicenseClass], Dr.[LicenseExpiry], 
			Dr.[TruckID], Dr.[TruckType], Dr.[FreightMode], Dr.[JoinDate], Dr.[ResignDate], Dr.[PassportNo], Dr.[PassportExpiry], Dr.[VendorCode], Dr.[IsActive], 
			Dr.[Status], Dr.[QuotationNo], Dr.[CreatedBy], Dr.[CreatedOn], Dr.[ModifiedBy], Dr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As DriverTypeDescription,
			ISNULL(TK.LookupDescription,'') As TruckTypeDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName
	FROM	[Master].[Driver] Dr
	Left Outer Join DriverTypeLookup DT ON 
		Dr.DriverType = DT.LookupID
	Left Outer Join TruckTypeLookup TK ON
		Dr.TruckType = TK.LookupID
	Left Outer Join FreightModeLookup FM ON
		Dr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Dr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE	Dr.[BranchID] = @BranchID  
			AND Dr.[DriverID] = @DriverID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_DriverList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Driver] Records from [Driver] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverList] 
END 
GO
CREATE PROC [Master].[usp_DriverList] 
	@BranchID BIGINT 
AS 
BEGIN

	;With DriverTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='DriverType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Dr.[BranchID], Dr.[DriverID], Dr.[DriverName], Dr.[DriverType], Dr.[IsDriver], Dr.[ICNo], Dr.[LicenseNo], Dr.[LicenseClass], Dr.[LicenseExpiry], 
			Dr.[TruckID], Dr.[TruckType], Dr.[FreightMode], Dr.[JoinDate], Dr.[ResignDate], Dr.[PassportNo], Dr.[PassportExpiry], Dr.[VendorCode], Dr.[IsActive], 
			Dr.[Status], Dr.[QuotationNo], Dr.[CreatedBy], Dr.[CreatedOn], Dr.[ModifiedBy], Dr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As DriverTypeDescription,
			ISNULL(TK.LookupDescription,'') As TruckTypeDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName
	FROM	[Master].[Driver] Dr
	Left Outer Join DriverTypeLookup DT ON 
		Dr.DriverType = DT.LookupID
	Left Outer Join TruckTypeLookup TK ON
		Dr.TruckType = TK.LookupID
	Left Outer Join FreightModeLookup FM ON
		Dr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Dr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE Dr.[BranchID] = @BranchID 
	And Dr.[IsActive] = Cast(1 as bit)
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Driver] Records from [Driver] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverPageView] 
END 
GO
CREATE PROC [Master].[usp_DriverPageView] 
	@BranchID BIGINT,
    @fetchrows bigint
AS 
BEGIN

	;With DriverTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='DriverType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Dr.[BranchID], Dr.[DriverID], Dr.[DriverName], Dr.[DriverType], Dr.[IsDriver], Dr.[ICNo], Dr.[LicenseNo], Dr.[LicenseClass], Dr.[LicenseExpiry], 
			Dr.[TruckID], Dr.[TruckType], Dr.[FreightMode], Dr.[JoinDate], Dr.[ResignDate], Dr.[PassportNo], Dr.[PassportExpiry], Dr.[VendorCode], Dr.[IsActive], 
			Dr.[Status], Dr.[QuotationNo], Dr.[CreatedBy], Dr.[CreatedOn], Dr.[ModifiedBy], Dr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As DriverTypeDescription,
			ISNULL(TK.LookupDescription,'') As TruckTypeDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName
	FROM	[Master].[Driver] Dr
	Left Outer Join DriverTypeLookup DT ON 
		Dr.DriverType = DT.LookupID
	Left Outer Join TruckTypeLookup TK ON
		Dr.TruckType = TK.LookupID
	Left Outer Join FreightModeLookup FM ON
		Dr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Dr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	where Dr.[BranchID] = @BranchID 
	ORDER BY  [DriverID] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Driver] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverRecordCount] 
END 
GO
CREATE PROC [Master].[usp_DriverRecordCount] 
	@BranchID BIGINT 
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Driver]
	WHERE [BranchID] = @BranchID 
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Driver] Record based on [Driver] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DriverAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_DriverAutoCompleteSearch] 
    @BranchID BIGINT,
    @DriverID NVARCHAR(20)
AS 

BEGIN

	;With DriverTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='DriverType'),
	TruckTypeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TruckType'),
	FreightModeLookup As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='FreightMode')
	SELECT	Dr.[BranchID], Dr.[DriverID], Dr.[DriverName], Dr.[DriverType], Dr.[IsDriver], Dr.[ICNo], Dr.[LicenseNo], Dr.[LicenseClass], Dr.[LicenseExpiry], 
			Dr.[TruckID], Dr.[TruckType], Dr.[FreightMode], Dr.[JoinDate], Dr.[ResignDate], Dr.[PassportNo], Dr.[PassportExpiry], Dr.[VendorCode], Dr.[IsActive], 
			Dr.[Status], Dr.[QuotationNo], Dr.[CreatedBy], Dr.[CreatedOn], Dr.[ModifiedBy], Dr.[ModifiedOn],
			ISNULL(DT.LookupDescription,'') As DriverTypeDescription,
			ISNULL(TK.LookupDescription,'') As TruckTypeDescription,
			ISNULL(FM.LookupDescription,'') As FreightModeDescription,
			ISNULL(VN.MerchantName,'') As VendorName
	FROM	[Master].[Driver] Dr
	Left Outer Join DriverTypeLookup DT ON 
		Dr.DriverType = DT.LookupID
	Left Outer Join TruckTypeLookup TK ON
		Dr.TruckType = TK.LookupID
	Left Outer Join FreightModeLookup FM ON
		Dr.FreightMode = FM.LookupID
	Left Outer Join Master.Merchant VN ON
		Dr.VendorCode = VN.MerchantCode
		And VN.IsVendor = CAST(1 as bit)
	WHERE  Dr.[BranchID] = @BranchID 
	       And Dr.[IsActive] = Cast(1 as bit)
		   AND Dr.[DriverID] LIKE '%' +  @DriverID + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_DriverInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Driver] Record Into [Driver] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverInsert] 
END 
GO
CREATE PROC [Master].[usp_DriverInsert] 
    @BranchID BIGINT,
    @DriverID nvarchar(20),
    @DriverName nvarchar(50),
    @DriverType smallint,
    @IsDriver bit,
    @ICNo nvarchar(20),
    @LicenseNo nvarchar(20),
    @LicenseClass smallint,
    @LicenseExpiry datetime,
    @TruckID nvarchar(20),
    @TruckType smallint,
    @FreightMode smallint,
    @JoinDate datetime,
    @ResignDate datetime,
    @PassportNo nvarchar(20),
    @PassportExpiry datetime,
    @VendorCode nvarchar(10),
    @IsActive bit,
    @QuotationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[Driver] (
			[BranchID], [DriverID], [DriverName], [DriverType], [IsDriver], [ICNo], [LicenseNo], [LicenseClass], [LicenseExpiry], 
			[TruckID], [TruckType], [FreightMode], [JoinDate], [ResignDate], [PassportNo], [PassportExpiry], [VendorCode], 
			[IsActive], [Status], [QuotationNo], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DriverID, @DriverName, @DriverType, @IsDriver, @ICNo, @LicenseNo, @LicenseClass, @LicenseExpiry, 
			@TruckID, @TruckType, @FreightMode, @JoinDate, @ResignDate, @PassportNo, @PassportExpiry, @VendorCode, 
			@IsActive, CAST(1 AS BIT), @QuotationNo, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Driver] Record Into [Driver] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverUpdate] 
END 
GO
CREATE PROC [Master].[usp_DriverUpdate] 
    @BranchID BIGINT,
    @DriverID nvarchar(20),
    @DriverName nvarchar(50),
    @DriverType smallint,
    @IsDriver bit,
    @ICNo nvarchar(20),
    @LicenseNo nvarchar(20),
    @LicenseClass smallint,
    @LicenseExpiry datetime,
    @TruckID nvarchar(20),
    @TruckType smallint,
    @FreightMode smallint,
    @JoinDate datetime,
    @ResignDate datetime,
    @PassportNo nvarchar(20),
    @PassportExpiry datetime,
    @VendorCode nvarchar(10),
    @IsActive bit,
    @QuotationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[Driver]
	SET		[DriverName] = @DriverName, [DriverType] = @DriverType, [IsDriver] = @IsDriver, [ICNo] = @ICNo, [LicenseNo] = @LicenseNo, 
			[LicenseClass] = @LicenseClass, [LicenseExpiry] = @LicenseExpiry, [TruckID] = @TruckID, [TruckType] = @TruckType, 
			[FreightMode] = @FreightMode, [JoinDate] = @JoinDate, [ResignDate] = @ResignDate, [PassportNo] = @PassportNo, 
			[PassportExpiry] = @PassportExpiry, [VendorCode] = @VendorCode, [IsActive] = @IsActive, 
			[QuotationNo] = @QuotationNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [DriverID] = @DriverID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DriverSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Driver] Record Into [Driver] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverSave] 
END 
GO
CREATE PROC [Master].[usp_DriverSave] 
    @BranchID BIGINT,
    @DriverID nvarchar(20),
    @DriverName nvarchar(50),
    @DriverType smallint,
    @IsDriver bit,
    @ICNo nvarchar(20),
    @LicenseNo nvarchar(20),
    @LicenseClass smallint,
    @LicenseExpiry datetime,
    @TruckID nvarchar(20),
    @TruckType smallint,
    @FreightMode smallint,
    @JoinDate datetime,
    @ResignDate datetime,
    @PassportNo nvarchar(20),
    @PassportExpiry datetime,
    @VendorCode nvarchar(10),
    @IsActive bit,
    @QuotationNo nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Driver] 
		WHERE 	[BranchID] = @BranchID
	       AND [DriverID] = @DriverID)>0
	BEGIN
	    Exec [Master].[usp_DriverUpdate] 
			@BranchID, @DriverID, @DriverName, @DriverType, @IsDriver, @ICNo, @LicenseNo, @LicenseClass, @LicenseExpiry, 
			@TruckID, @TruckType, @FreightMode, @JoinDate, @ResignDate, @PassportNo, @PassportExpiry, @VendorCode, @IsActive, 
			@QuotationNo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_DriverInsert] 
			@BranchID, @DriverID, @DriverName, @DriverType, @IsDriver, @ICNo, @LicenseNo, @LicenseClass, @LicenseExpiry, 
			@TruckID, @TruckType, @FreightMode, @JoinDate, @ResignDate, @PassportNo, @PassportExpiry, @VendorCode, @IsActive, 
			@QuotationNo, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[DriverSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DriverDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Driver] Record  based on [Driver]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DriverDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DriverDelete] 
END 
GO
CREATE PROC [Master].[usp_DriverDelete] 
    @BranchID BIGINT,
    @DriverID nvarchar(20)
AS 

	
BEGIN

	UPDATE	[Master].[Driver]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [DriverID] = @DriverID

	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DriverDelete]
-- ========================================================================================================================================

GO


