 

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CustomDeclarant] Record based on [CustomDeclarant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDeclarantSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantSelect] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantSelect] 
    @BranchID bigint,
    @ID nvarchar(50)
AS 
 

BEGIN

	SELECT [BranchID], [ID], [Name], [Designation], [NRIC],[IsActive] 
	FROM   [Master].[CustomDeclarant]
	WHERE  [BranchID] = @BranchID  
			AND [ID] = @ID   

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CustomDeclarant] Records from [CustomDeclarant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDeclarantList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantList] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantList] 
    @BranchID bigint

AS 
 
BEGIN
	SELECT [BranchID], [ID], [Name], [Designation], [NRIC],[IsActive] 
	FROM   [Master].[CustomDeclarant]
	WHERE  [BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomDeclarant] PageView Records from [CustomDeclarant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDeclarantPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantPageView]
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantPageView]
	@BranchID bigint, 
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [ID], [Name], [Designation], [NRIC],[IsActive] 
	FROM   [Master].[CustomDeclarant]
	Where BranchID = @BranchID
	ORDER BY  [ID]           
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CustomDeclarant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDeclarantRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantRecordCount] 
	@BranchID bigint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Master].[CustomDeclarant]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomDeclarant] auto-complete search based on [CustomDeclarant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDeclarantAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantAutoCompleteSearch]
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantAutoCompleteSearch]
    @BranchID bigint,
    @ID nvarchar(50)
     
AS 

BEGIN
	SELECT [BranchID], [ID], [Name], [Designation], [NRIC],[IsActive] 
	FROM   [Master].[CustomDeclarant]
	WHERE  [BranchID] = @BranchID  
	       AND [ID] LIKE '%' + @ID + '%'
END
-- ========================================================================================================================================
-- END  											[Master].[usp_CustomDeclarantAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CustomDeclarant] Record Into [CustomDeclarant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDeclarantInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantInsert] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantInsert] 
    @BranchID bigint,
    @ID nvarchar(50),
    @Name nvarchar(100) = NULL,
    @Designation nvarchar(50) = NULL,
    @NRIC nvarchar(20) = NULL
AS 
  

BEGIN
	
	INSERT INTO [Master].[CustomDeclarant] ([BranchID], [ID], [Name], [Designation], [NRIC])
	SELECT @BranchID, @ID, @Name, @Designation, @NRIC
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CustomDeclarant] Record Into [CustomDeclarant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDeclarantUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantUpdate] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantUpdate] 
    @BranchID bigint,
    @ID nvarchar(50),
    @Name nvarchar(100) = NULL,
    @Designation nvarchar(50) = NULL,
    @NRIC nvarchar(20) = NULL
AS 
 
	
BEGIN

	UPDATE [Master].[CustomDeclarant]
	SET    [BranchID] = @BranchID, [ID] = @ID, [Name] = @Name, [Designation] = @Designation, [NRIC] = @NRIC
	WHERE  [BranchID] = @BranchID
	       AND [ID] = @ID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CustomDeclarant] Record Into [CustomDeclarant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDeclarantSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantSave] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantSave] 
    @BranchID bigint,
    @ID nvarchar(50),
    @Name nvarchar(100) = NULL,
    @Designation nvarchar(50) = NULL,
    @NRIC nvarchar(20) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[CustomDeclarant] 
		WHERE 	[BranchID] = @BranchID
	       AND [ID] = @ID)>0
	BEGIN
	    Exec [Master].[usp_CustomDeclarantUpdate] 
		@BranchID, @ID, @Name, @Designation, @NRIC


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CustomDeclarantInsert] 
		@BranchID, @ID, @Name, @Designation, @NRIC


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CustomDeclarantSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDeclarantDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CustomDeclarant] Record  based on [CustomDeclarant]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDeclarantDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDeclarantDelete] 
END 
GO
CREATE PROC [Master].[usp_CustomDeclarantDelete] 
    @BranchID bigint,
    @ID nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[CustomDeclarant]
	SET	[IsActive] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [ID] = @ID

	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDeclarantDelete]
-- ========================================================================================================================================

GO 
