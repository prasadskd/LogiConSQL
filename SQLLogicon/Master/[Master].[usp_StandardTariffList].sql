-- ========================================================================================================================================
-- START											[Master].[usp_StandardTariffList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	16-Apr-2017
-- Description:	Get The List of Company Affiliations

/*
Exec [Master].[usp_StandardTariffList] 1009 
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StandardTariffList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StandardTariffList] 
END 
GO
CREATE Procedure [Master].[usp_StandardTariffList]
@CompanyID int
As
Begin
 if @CompanyID is not null
	Select RegistrationType as ModuleID, B.LookupDescription as ModuleDescription, Cast(1 as bit) isSelected, 
	C.Percentage as Rate
	From Root.CompanyRegistrationType A
	inner Join Config.Lookup B On A.RegistrationType = B.LookupID
	inner Join [Master].[TariffDetail] C On A.RegistrationType = C.Module 
	Where A.CompanyID = @CompanyID and C.QuotationNo = 'STANDARD_1000001' and C.TariffMode = 26150
	Union 
	Select B.LookupID as ModuleID, B.LookupDescription as ModuleDescription, Cast(0 as bit) isSelected, Percentage as Rate From [Master].[TariffDetail] A
	inner Join Config.Lookup B
	On A.Module = B.LookUpID
	Where A.QuotationNo = 'STANDARD_1000001' and A.TariffMode = 26150
	and B.LookupCategory = 'RegistrationType' and B.LookupID not in 
	(Select RegistrationType From Root.CompanyRegistrationType Where CompanyID = @CompanyID)
 Else 
    Select B.LookupID as ModuleID, B.LookupDescription as ModuleDescription, Cast(0 as bit) isSelected, Percentage as Rate From [Master].[TariffDetail] A
	inner Join Config.Lookup B
	On A.Module = B.LookUpID
	Where A.QuotationNo = 'STANDARD_1000001' and A.TariffMode = 26150
	and B.LookupCategory = 'RegistrationType'
End