
-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Merchant] Record based on [Merchant] table


/*
Modification History 

Modified by	:	Maruthi
Modified ON :	06-04-2017
Modification:	added the ISNULL checks for the RegNo & TaxID in the whereclause , (required for the CUSDEC K1,K2 EDI )

*/

/*
Sample script

Declare 
	@MerchantCode BigInt,
	@RegNo nvarchar(50),
	@TaxID nvarchar(50)

Select @MerchantCode=1153,@RegNo =NULL,@TaxID=NULL

Exec [Master].[usp_MerchantSelect] @MerchantCode,@RegNo ,@TaxID 

    

*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantSelect] 
END 
GO

CREATE PROC [Master].[usp_MerchantSelect] 
    @MerchantCode BigInt,
	@RegNo nvarchar(50),
	@TaxID nvarchar(50)
AS 

SET NOCOUNT ON; 

BEGIN
	if(@RegNo='') 
	set @RegNo = null
	if(@TaxID='') 
	set @TaxID = null
	SELECT	[MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
			[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
			[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
			[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
			[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,BusinessType,
			IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
	FROM	[Master].[Merchant]
	WHERE  [MerchantCode] = @MerchantCode  
		   And RegNo = ISNULL(@RegNo,RegNo)
		   And TaxID = ISNULL(@TaxID,TaxID)
		   And IsActive = CAST(1 as bit)

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantSelect]
-- ========================================================================================================================================
GO

-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Merchant] Records from [Merchant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantList] 
END 
GO
CREATE PROC [Master].[usp_MerchantList] 

AS 
BEGIN

SET NOCOUNT ON;

	
	SELECT	[MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
			[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
			[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
			[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
			[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
			[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],BusinessType,
			IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
	FROM	[Master].[Merchant]
	WHERE IsActive = CAST(1 as bit)

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantListByPaging]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Merchant] Records from [Merchant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantListByPaging]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantListByPaging] 
END 
GO
CREATE PROC  [Master].[usp_MerchantListByPaging]
(@offset int, @merchantType varchar(100), @CompanyCode bigint)
as
Begin
	if(@merchantType = 'billingCustomer')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsBillingCustomer = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
	End
	else if(@merchantType = 'shipper')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsShipper = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'consignee')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsConsignee = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'terminal')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsTerminal = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'freightForwarder')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsForwarder = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'transporter')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsTransporter = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'vendor')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsVendor = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'yard')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			Where IsYard = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = CAST(1 as bit)
			Order By MerchantCode
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
	else if(@merchantType = 'all')
		Begin
			Select [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo],
				   [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], 
				   [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks], 
				   [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
				   [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], 
				   [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn],
				   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType],
				   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
			From Master.Merchant 
			WHERE ([CompanyCode] = @CompanyCode Or [CompanyCode] = 1000)
			And IsActive = CAST(1 as bit)
			Order By MerchantCode 
			OFFSET @offset ROWS 
			FETCH NEXT 10 ROWS ONLY
		End
End

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantListByPaging] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Merchant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRecordCount] 
END 
GO
CREATE PROC [Master].[usp_MerchantRecordCount] (
	@merchantType varchar(100), 
	@CompanyCode bigint,
	@MerchantName varchar(100))
AS 
BEGIN
 Declare @Sql nvarchar(max);

 if @merchantType = ''
 Begin
  Select @sql = 'SELECT COUNT(0)  From Master.Merchant Where CompanyCode = ' + Convert(nvarchar(10), @CompanyCode) +
	' And IsActive = Cast(1 as bit) '
 End
 Else
 Begin

	set @sql ='SELECT COUNT(0) From Master.Merchant Where CompanyCode = 1000 ' +
	' And IsActive = Cast(1 as bit) And IsOriginal = Cast(1 as bit) And IsVerified = Cast(1 as bit) '
		

set @sql +=' And MerchantName like ''%'+@MerchantName+'%'''

if(@merchantType='shippingAgent')
set @sql +=' And IsLiner = cast(1 as bit) '

if(@merchantType='billingCustomer')
set @sql +=' And IsBillingCustomer = cast(1 as bit) '

if(@merchantType='consignee')
set @sql +=' And IsConsignee = cast(1 as bit) '

if(@merchantType='terminal')
set @sql +=' And IsTerminal = cast(1 as bit) '

if(@merchantType='forwardingAgent')
set @sql +=' And IsForwarder = cast(1 as bit) '

if(@merchantType='transporter')
set @sql +=' And IsTransporter = cast(1 as bit) '

if(@merchantType='vendor')
set @sql +=' And IsVendor = cast(1 as bit) '

if(@merchantType='yard')
set @sql +=' And IsYard = cast(1 as bit) '


set @sql +=' And RegNo NOT IN (SELECT RegNo From Master.Merchant Where CompanyCode = ' + Convert(nvarchar(10), @CompanyCode)+')'


 End
 

print @sql;
exec(@sql)
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRecordCount] 
-- ========================================================================================================================================

GO



GO



-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Merchant] Record Into [Merchant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantInsert] 
END 
GO
CREATE PROC [Master].[usp_MerchantInsert] 
    @MerchantCode BigInt,
    @MerchantName nvarchar(255),
    @BillingCustomer nvarchar(10),
    @PortCode varchar(10),
    @RegNo nvarchar(50),
    @TaxID nvarchar(50),
    @OwnerCode nvarchar(10),
    @AgentCode nvarchar(10),
    @YardCode nvarchar(10),
    @BillingMethod smallint,
    @CreditTerm int,
    @CreditLimit numeric(18, 4),
    @DefaultCurrency nvarchar(3),
    @CreditorCode nvarchar(10),
    @DebtorCode nvarchar(10),
    @EDIMappingCode nvarchar(20),
    @WebSite nvarchar(100),
    @Remarks nvarchar(100),
    @IsBillingCustomer bit,
    @IsShipper bit,
    @IsConsignee bit,
    @IsLiner bit,
    @IsForwarder bit,
    @IsYard bit,
    @IsCFS bit,
    @IsRailOperator bit,
    @IsTerminal bit,
    @IsPrincipal bit,
    @IsVMIVendor bit,
    @IsHaulier bit,
    @IsTransporter bit,
    @IsVendor bit,
    @IsRailYard bit,
    @IsTaxable bit,
	@IsSelectContainer bit,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@CompanyCode bigint,
	@BusinessType smallint,
	@IsFreightForwarder bit,
	@IsOverseasAgent bit,
	@IsSubscriber bit,
	@IsOriginal bit,
	@SubscriberCompanyCode bigint,
	@NewMerchantCode bigint OUTPUT 
AS 
  

BEGIN
 
	
	INSERT INTO [Master].[Merchant] (
			[MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], 
			[AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], 
			[EDIMappingCode], [WebSite], [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], 
			[IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable], 
			[IsSelectContainer],[IsActive], [CreatedBy], [CreatedOn], [CompanyCode],BusinessType,
			IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode)
	SELECT	@MerchantName, @BillingCustomer, @PortCode, @RegNo, @TaxID, @OwnerCode, 
			@AgentCode, @YardCode, @BillingMethod, @CreditTerm, @CreditLimit, @DefaultCurrency, @CreditorCode, @DebtorCode, 
			@EDIMappingCode, @WebSite, @Remarks, @IsBillingCustomer, @IsShipper, @IsConsignee, @IsLiner, @IsForwarder, @IsYard, @IsCFS, 
			@IsRailOperator, @IsTerminal, @IsPrincipal, @IsVMIVendor, @IsHaulier, @IsTransporter, @IsVendor, @IsRailYard, @IsTaxable, 
			@IsSelectContainer,CAST(1 as bit), @CreatedBy, GETUTCDATE(),@CompanyCode,@BusinessType,
			@IsFreightForwarder,@IsOverseasAgent,@IsSubscriber,@IsOriginal,@SubscriberCompanyCode
			 
	Select @NewMerchantCode = IDENT_CURRENT( '[Master].[Merchant]' )               
END


-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantInsert]
-- ========================================================================================================================================
GO



-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Merchant] Record Into [Merchant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantUpdate] 
END 
GO
CREATE PROC [Master].[usp_MerchantUpdate] 
    @MerchantCode bigInt,
    @MerchantName nvarchar(255),
    @BillingCustomer nvarchar(10),
    @PortCode varchar(10),
    @RegNo nvarchar(50),
    @TaxID nvarchar(50),
    @OwnerCode nvarchar(10),
    @AgentCode nvarchar(10),
    @YardCode nvarchar(10),
    @BillingMethod smallint,
    @CreditTerm int,
    @CreditLimit numeric(18, 4),
    @DefaultCurrency nvarchar(3),
    @CreditorCode nvarchar(10),
    @DebtorCode nvarchar(10),
    @EDIMappingCode nvarchar(20),
    @WebSite nvarchar(100),
    @Remarks nvarchar(100),
    @IsBillingCustomer bit,
    @IsShipper bit,
    @IsConsignee bit,
    @IsLiner bit,
    @IsForwarder bit,
    @IsYard bit,
    @IsCFS bit,
    @IsRailOperator bit,
    @IsTerminal bit,
    @IsPrincipal bit,
    @IsVMIVendor bit,
    @IsHaulier bit,
    @IsTransporter bit,
    @IsVendor bit,
    @IsRailYard bit,
    @IsTaxable bit,
	@IsSelectContainer bit,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60) ,
	@CompanyCode bigint,
	@BusinessType smallint,
	@IsFreightForwarder bit,
	@IsOverseasAgent bit,
	@IsSubscriber bit,
	@IsOriginal bit,
	@SubscriberCompanyCode bigint,
	@NewMerchantCode bigint OUTPUT
AS 
	
BEGIN
 

	UPDATE	[Master].[Merchant]
	SET		[MerchantName] = @MerchantName, [BillingCustomer] = @BillingCustomer, [PortCode] = @PortCode, 
			[RegNo] = @RegNo, [TaxID] = @TaxID, [OwnerCode] = @OwnerCode, [AgentCode] = @AgentCode, [YardCode] = @YardCode, 
			[BillingMethod] = @BillingMethod, [CreditTerm] = @CreditTerm, [CreditLimit] = @CreditLimit, [DefaultCurrency] = @DefaultCurrency, 
			[CreditorCode] = @CreditorCode, [DebtorCode] = @DebtorCode, [EDIMappingCode] = @EDIMappingCode, [WebSite] = @WebSite, 
			[Remarks] = @Remarks, [IsBillingCustomer] = @IsBillingCustomer, [IsShipper] = @IsShipper, [IsConsignee] = @IsConsignee, [IsLiner] = @IsLiner, 
			[IsForwarder] = @IsForwarder, [IsYard] = @IsYard, [IsCFS] = @IsCFS, [IsRailOperator] = @IsRailOperator, [IsTerminal] = @IsTerminal, 
			[IsPrincipal] = @IsPrincipal, [IsVMIVendor] = @IsVMIVendor, [IsHaulier] = @IsHaulier, [IsTransporter] = @IsTransporter, 
			[IsVendor] = @IsVendor, [IsRailYard] = @IsRailYard, [IsTaxable] = @IsTaxable,[IsSelectContainer]=@IsSelectContainer, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),
			[CompanyCode] = @CompanyCode,BusinessType = @BusinessType,
			IsFreightForwarder = @IsFreightForwarder,IsOverseasAgent = @IsOverseasAgent,IsOriginal=@IsOriginal,
			IsSubscriber= @IsSubscriber, IsActive = Cast(1 as bit),SubscriberCompanyCode=@SubscriberCompanyCode
	WHERE	[MerchantCode] = @MerchantCode
			And RegNo = @RegNo
			And TaxID = @TaxID

	Select @NewMerchantCode = @MerchantCode
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Merchant] Record Into [Merchant] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantSave] 
END 
GO
CREATE PROC [Master].[usp_MerchantSave] 
    @MerchantCode BigInt,
    @MerchantName nvarchar(255),
    @BillingCustomer nvarchar(10),
    @PortCode varchar(10),
    @RegNo nvarchar(50),
    @TaxID nvarchar(50),
    @OwnerCode nvarchar(10),
    @AgentCode nvarchar(10),
    @YardCode nvarchar(10),
    @BillingMethod smallint,
    @CreditTerm int,
    @CreditLimit numeric(18, 4),
    @DefaultCurrency nvarchar(3),
    @CreditorCode nvarchar(10),
    @DebtorCode nvarchar(10),
    @EDIMappingCode nvarchar(20),
    @WebSite nvarchar(100),
    @Remarks nvarchar(100),
    @IsBillingCustomer bit,
    @IsShipper bit,
    @IsConsignee bit,
    @IsLiner bit,
    @IsForwarder bit,
    @IsYard bit,
    @IsCFS bit,
    @IsRailOperator bit,
    @IsTerminal bit,
    @IsPrincipal bit,
    @IsVMIVendor bit,
    @IsHaulier bit,
    @IsTransporter bit,
    @IsVendor bit,
    @IsRailYard bit,
    @IsTaxable bit,
	@IsSelectContainer bit,
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60),
	@CompanyCode bigint,
	@BusinessType smallint,
	@IsFreightForwarder bit,
	@IsOverseasAgent bit,
	@IsSubscriber bit,
	@IsOriginal bit,
	@SubscriberCompanyCode bigint,
	@NewMerchantCode bigint OUTPUT
AS 
BEGIN 

	Declare @IsDuplicate int = 0;

	SELECT @IsDuplicate= COUNT(0) FROM [Master].[Merchant] 
		WHERE 
		CompanyCode = @CompanyCode
		And RegNo = @RegNo
		And AgentCode = Case @IsForwarder
					When 1 Then @AgentCode Else AgentCode End
		And TaxID = Case @IsForwarder
					When 1 Then TaxID Else @TaxID End




	IF (@IsDuplicate)>0
	BEGIN
		set @MerchantCode = (SELECT top(1) MerchantCode FROM [Master].[Merchant] 
		WHERE 
		CompanyCode = @CompanyCode
		And RegNo = @RegNo
		And AgentCode = Case @IsForwarder
					When 1 Then @AgentCode Else AgentCode End
		And TaxID = Case @IsForwarder
					When 1 Then TaxID Else @TaxID End)

	    Exec [Master].[usp_MerchantUpdate] 
				@MerchantCode, @MerchantName, @BillingCustomer, @PortCode, @RegNo, @TaxID, @OwnerCode, @AgentCode, 
				@YardCode, @BillingMethod, @CreditTerm, @CreditLimit, @DefaultCurrency, @CreditorCode, @DebtorCode, @EDIMappingCode, @WebSite, 
				@Remarks, @IsBillingCustomer, @IsShipper, @IsConsignee, @IsLiner, @IsForwarder, @IsYard, @IsCFS, @IsRailOperator, @IsTerminal, 
				@IsPrincipal, @IsVMIVendor, @IsHaulier, @IsTransporter, @IsVendor, @IsRailYard, @IsTaxable,@IsSelectContainer, @CreatedBy, 
				@ModifiedBy, @CompanyCode,@BusinessType,@IsFreightForwarder,@IsOverseasAgent,@IsSubscriber,@IsOriginal,
				@SubscriberCompanyCode, @NewMerchantCode = @NewMerchantCode OUTPUT 


	END
	ELSE
	BEGIN

		--Set @MerchantCode=0;

		--Select @MerchantCode = ISNULL(Max(MerchantCode),999)+1
		--From Master.Merchant

	    Exec [Master].[usp_MerchantInsert] 
				@MerchantCode, @MerchantName, @BillingCustomer, @PortCode, @RegNo, @TaxID, @OwnerCode, @AgentCode, 
				@YardCode, @BillingMethod, @CreditTerm, @CreditLimit, @DefaultCurrency, @CreditorCode, @DebtorCode, @EDIMappingCode, @WebSite, 
				@Remarks, @IsBillingCustomer, @IsShipper, @IsConsignee, @IsLiner, @IsForwarder, @IsYard, @IsCFS, @IsRailOperator, @IsTerminal, 
				@IsPrincipal, @IsVMIVendor, @IsHaulier, @IsTransporter, @IsVendor, @IsRailYard, @IsTaxable,@IsSelectContainer, 
				@CreatedBy, @ModifiedBy, @CompanyCode,@BusinessType,@IsFreightForwarder,@IsOverseasAgent,@IsSubscriber,@IsOriginal,	
				@SubscriberCompanyCode,@NewMerchantCode = @NewMerchantCode OUTPUT 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[MerchantSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Merchant] Record  based on [Merchant]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantDelete] 
END 
GO
CREATE PROC [Master].[usp_MerchantDelete] 
    @MerchantCode bigint
AS 

	
BEGIN
SET NOCOUNT ON;
	UPDATE	[Master].[Merchant]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[MerchantCode] = @MerchantCode


SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantDelete]
-- ========================================================================================================================================

GO
 

-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantCount]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	01-May-2017
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantCount] 
END 
GO
CREATE PROCedure [Master].[usp_MerchantCount]
@offset int, 
@merchantType varchar(100), 
@CompanyCode bigint
AS
Begin
	if(@merchantType = 'billingCustomer')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsBillingCustomer = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
			End
	else if(@merchantType = 'shipper')
		Begin
			Select Count(0)
			From Master.Merchant 
			Where IsShipper = Cast(1 as bit) and [CompanyCode] = @CompanyCode	
			And IsActive = Cast(1 as bit)
					End
	else if(@merchantType = 'consignee')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsConsignee = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'terminal')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsTerminal = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'freightForwarder')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsForwarder = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'transporter')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsTransporter = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'vendor')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsVendor = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'yard')
		Begin
			Select Count(0) 
			From Master.Merchant 
			Where IsYard = Cast(1 as bit) and [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
	else if(@merchantType = 'all')
		Begin
			Select Count(0) 
			From Master.Merchant 				
			Where [CompanyCode] = @CompanyCode
			And IsActive = Cast(1 as bit)
		End
End

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantDelete]
-- ========================================================================================================================================
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSearchByRegNo]
-- ========================================================================================================================================
-- Author:			Vijay
-- Create date: 	01-May-2017
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantSearchByRegNo]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantSearchByRegNo] 
END 
GO
CREATE Procedure [Master].[usp_MerchantSearchByRegNo]
@CompanyCode bigint,
@RegNo varchar(20)
As
Begin
 if(Select Count(0) From Master.Merchant Where CompanyCode = @CompanyCode And RegNo = @RegNo And IsSubscriber = cast(1 as bit) And IsOriginal = cast(0 as bit)) > 0
	  Select [MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
	   [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
	   [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
	   [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
	   [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
	   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,BusinessType,
	   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
	  From Master.Merchant Where CompanyCode = @CompanyCode
			And IsSubscriber = cast(1 as bit) And IsOriginal = cast(0 as bit)
	  And RegNo = @RegNo;
 Else
	 Select [MerchantCode], [MerchantName],[RegNo], [TaxID], [BillingCustomer], [PortCode], [OwnerCode], [AgentCode], 
	   [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
	   [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
	   [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
	   [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
	   [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,BusinessType,
	   IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
	  From Master.Merchant Where CompanyCode != @CompanyCode
			And IsSubscriber = cast(1 as bit) And IsOriginal = cast(1 as bit)
	  And RegNo = @RegNo;
End

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantSearchByRegNo]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSearchByRegNo]
-- ========================================================================================================================================
-- Author:			Vijay
-- Create date: 	01-May-2017
-- Description:	 

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantDataTableList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantDataTableList] 
END 
GO
CREATE PROC [Master].[usp_MerchantDataTableList] (
	@limit smallint,
	@offset smallint,
	@sortColumn varchar(50),
	@sortType varchar(50),
	@merchantType varchar(100), 
	@CompanyCode bigint,
	@MerchantName varchar(100))

AS 
 
BEGIN
 Declare @Sql nvarchar(max);

 if @merchantType = ''
 Begin
  Select @sql = 'SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], ' +
	' [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  [DefaultCurrency], [CreditorCode], [DebtorCode], ' +
	' [EDIMappingCode], [WebSite], [Remarks],  [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], ' +
	' [IsYard], [IsCFS],  [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter],  ' +
	' [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn], ' +
	' [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], IsFreightForwarder,' +
	' IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode  From Master.Merchant Where CompanyCode = ' + Convert(nvarchar(10), @CompanyCode) +
	' And IsActive = Cast(1 as bit) ' +
	' Order By ' + @sortColumn + '  '+ @sortType +
	' OFFSET ' +   Convert(nvarchar(10),@offset) + ' ROWS FETCH NEXT ' + Convert(nvarchar(10),@limit) + ' ROWS ONLY ' 
 End
 Else
 Begin

	set @sql ='SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], ' +
	' [TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  [DefaultCurrency], ' +
	' [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks],  [IsBillingCustomer], [IsShipper], [IsConsignee],' +
	' [IsLiner], [IsForwarder], [IsYard], [IsCFS],  [IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], ' +
	' [IsTransporter],  [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],' +
	' [CreatedOn], [ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], IsFreightForwarder,' +
	' IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode From Master.Merchant Where CompanyCode = 1000 ' +
	' And IsActive = Cast(1 as bit) And IsOriginal = Cast(1 as bit) And IsVerified = Cast(1 as bit) '
		

set @sql +=' And MerchantName like ''%'+@MerchantName+'%'''

if(@merchantType='shippingAgent')
set @sql +=' And IsLiner = cast(1 as bit) '

if(@merchantType='billingCustomer')
set @sql +=' And IsBillingCustomer = cast(1 as bit) '

if(@merchantType='consignee')
set @sql +=' And IsConsignee = cast(1 as bit) '

if(@merchantType='terminal')
set @sql +=' And IsTerminal = cast(1 as bit) '

if(@merchantType='forwardingAgent')
set @sql +=' And IsForwarder = cast(1 as bit) '

if(@merchantType='transporter')
set @sql +=' And IsTransporter = cast(1 as bit) '

if(@merchantType='vendor')
set @sql +=' And IsVendor = cast(1 as bit) '

if(@merchantType='yard')
set @sql +=' And IsYard = cast(1 as bit) '


set @sql +=' And RegNo NOT IN (SELECT RegNo From Master.Merchant Where CompanyCode = ' + Convert(nvarchar(10), @CompanyCode)+')'

set @sql +=' Order By ' + @sortColumn + '  '+ @sortType +
' OFFSET ' +   Convert(nvarchar(10),@offset) + ' ROWS FETCH NEXT ' + Convert(nvarchar(10),@limit) + ' ROWS ONLY '

 End
 

print @sql;
exec(@sql)
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantDataTableList]
-- ========================================================================================================================================

GO
