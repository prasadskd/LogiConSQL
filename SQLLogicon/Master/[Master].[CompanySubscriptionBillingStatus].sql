
-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanySubscriptionBillingStatus] Record based on [CompanySubscriptionBillingStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusSelect] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusSelect] 
    @CompanyCode INT,
    @ModuleID SMALLINT
AS 

BEGIN

	SELECT [CompanyCode], [ModuleID], [IsBillable], [ActiveDate], [InActiveDate], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn] 
	FROM   [Master].[CompanySubscriptionBillingStatus]
	WHERE  [CompanyCode] = @CompanyCode  
	       AND [ModuleID] = @ModuleID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionBillingStatusSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanySubscriptionBillingStatus] Records from [CompanySubscriptionBillingStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusList] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusList] 
	@CompanyCode INT
AS 
BEGIN

	SELECT [CompanyCode], [ModuleID], [IsBillable], [ActiveDate], [InActiveDate], [CreatedBy], [CreatedOn], [ModifedBy], [ModifiedOn] 
	FROM   [Master].[CompanySubscriptionBillingStatus]
	WHERE  [CompanyCode] = @CompanyCode  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionBillingStatusList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanySubscriptionBillingStatus] Record Into [CompanySubscriptionBillingStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusInsert] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusInsert] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsBillable bit,
    @ActiveDate datetime,
    @InActiveDate datetime,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)

AS 
  

BEGIN

	
	INSERT INTO [Master].[CompanySubscriptionBillingStatus] (
			[CompanyCode], [ModuleID], [IsBillable], [ActiveDate], [InActiveDate], [CreatedBy], [CreatedOn])
	SELECT	@CompanyCode, @ModuleID, @IsBillable, @ActiveDate, @InActiveDate, @CreatedBy, GETUTCDATE() 
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionBillingStatusInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanySubscriptionBillingStatus] Record Into [CompanySubscriptionBillingStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusUpdate] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusUpdate] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsBillable bit,
    @ActiveDate datetime,
    @InActiveDate datetime,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[CompanySubscriptionBillingStatus]
	SET   [IsBillable] = @IsBillable, [ActiveDate] = @ActiveDate, [InActiveDate] = @InActiveDate, [ModifedBy] = @ModifedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [CompanyCode] = @CompanyCode
	       AND [ModuleID] = @ModuleID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionBillingStatusUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanySubscriptionBillingStatus] Record Into [CompanySubscriptionBillingStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusSave] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusSave] 
    @CompanyCode int,
    @ModuleID smallint,
    @IsBillable bit,
    @ActiveDate datetime,
    @InActiveDate datetime,
    @CreatedBy nvarchar(60),
    @ModifedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CompanySubscriptionBillingStatus] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [ModuleID] = @ModuleID)>0
	BEGIN
	    Exec [Master].[usp_CompanySubscriptionBillingStatusUpdate] 
		@CompanyCode, @ModuleID, @IsBillable, @ActiveDate, @InActiveDate, @CreatedBy, @ModifedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CompanySubscriptionBillingStatusInsert] 
		@CompanyCode, @ModuleID, @IsBillable, @ActiveDate, @InActiveDate, @CreatedBy, @ModifedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CompanySubscriptionBillingStatusSave]
-- ========================================================================================================================================

GO





-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingStatusDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanySubscriptionBillingStatus] Record  based on [CompanySubscriptionBillingStatus]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingStatusDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingStatusDelete] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingStatusDelete] 
    @CompanyCode int,
    @ModuleID smallint,
	@IsBillable bit,
	@ActivityDate datetime
AS 

	
BEGIN

	 

	If @IsBillable = Cast(1 as bit)
	Begin
		UPDATE [Master].[CompanySubscriptionBillingStatus]
		Set IsBillable = @IsBillable ,[ActiveDate]  = @ActivityDate
		FROM   [Master].[CompanySubscriptionBillingStatus]
		WHERE  [CompanyCode] = @CompanyCode
			   AND [ModuleID] = @ModuleID
	End
	Else
	Begin
		UPDATE [Master].[CompanySubscriptionBillingStatus]
		Set IsBillable = @IsBillable ,[InActiveDate]  = @ActivityDate
		FROM   [Master].[CompanySubscriptionBillingStatus]
		WHERE  [CompanyCode] = @CompanyCode
			   AND [ModuleID] = @ModuleID

	End

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanySubscriptionBillingStatusDelete]
-- ========================================================================================================================================

GO
 