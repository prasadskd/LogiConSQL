
-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationClause] Record based on [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DeclarationClauseSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseSelect] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseSelect] 
    @ClauseCode SMALLINT
AS 

BEGIN

	SELECT [ClauseCode], [ClauseTitle], [ClauseText], [CountryCode], [Status] 
	FROM   [Master].[DeclarationClause]
	WHERE  [ClauseCode] = @ClauseCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationClause] Records from [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DeclarationClauseList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseList] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseList] 
	@CountryCode nvarchar(2)
AS 
BEGIN

	SELECT [ClauseCode], [ClauseTitle], [ClauseText], [CountryCode], [Status] 
	FROM   [Master].[DeclarationClause]
	WHERE  [CountryCode] = @CountryCode  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClausePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [DeclarationClause] Records from [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DeclarationClausePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClausePageView] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClausePageView] 
	@CountryCode nvarchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT [ClauseCode], [ClauseTitle], [ClauseText], [CountryCode], [Status] 
	FROM   [Master].[DeclarationClause]
	WHERE  [CountryCode] = @CountryCode
	ORDER BY ClauseTitle
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClausePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DeclarationClauseRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseRecordCount] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseRecordCount] 
	@CountryCode nvarchar(2)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[DeclarationClause]
	WHERE  [CountryCode] = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [DeclarationClause] Record based on [DeclarationClause] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_DeclarationClauseAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseAutoCompleteSearch] 
    @CountryCode nvarchar(2),
	@ClauseTitle nvarchar(50)
AS 

BEGIN

	SELECT [ClauseCode], [ClauseTitle], [ClauseText], [CountryCode], [Status] 
	FROM   [Master].[DeclarationClause]
	WHERE  [CountryCode] = @CountryCode
	And ClauseTitle Like '%' + @ClauseTitle + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DeclarationClauseInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseInsert] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseInsert] 
    @ClauseCode smallint,
    @ClauseTitle nvarchar(50),
    @ClauseText nvarchar(MAX),
    @CountryCode nvarchar(2),
    @Status bit
AS 
  

BEGIN

	
	INSERT INTO [Master].[DeclarationClause] ([ClauseCode], [ClauseTitle], [ClauseText], [CountryCode], [Status])
	SELECT @ClauseCode, @ClauseTitle, @ClauseText, @CountryCode, @Status
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DeclarationClauseUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseUpdate] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseUpdate] 
    @ClauseCode smallint,
    @ClauseTitle nvarchar(50),
    @ClauseText nvarchar(MAX),
    @CountryCode nvarchar(2),
    @Status bit
AS 
 
	
BEGIN

	UPDATE [Master].[DeclarationClause]
	SET    [ClauseTitle] = @ClauseTitle, [ClauseText] = @ClauseText, [CountryCode] = @CountryCode, [Status] = @Status
	WHERE  [ClauseCode] = @ClauseCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [DeclarationClause] Record Into [DeclarationClause] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DeclarationClauseSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseSave] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseSave] 
    @ClauseCode smallint,
    @ClauseTitle nvarchar(50),
    @ClauseText nvarchar(MAX),
    @CountryCode nvarchar(2),
    @Status bit
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[DeclarationClause] 
		WHERE 	[ClauseCode] = @ClauseCode)>0
	BEGIN
	    Exec [Master].[usp_DeclarationClauseUpdate] 
		@ClauseCode, @ClauseTitle, @ClauseText, @CountryCode, @Status


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_DeclarationClauseInsert] 
		@ClauseCode, @ClauseTitle, @ClauseText, @CountryCode, @Status


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[DeclarationClauseSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_DeclarationClauseDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [DeclarationClause] Record  based on [DeclarationClause]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_DeclarationClauseDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_DeclarationClauseDelete] 
END 
GO
CREATE PROC [Master].[usp_DeclarationClauseDelete] 
    @ClauseCode smallint
AS 

	
BEGIN

	UPDATE	[Master].[DeclarationClause]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[ClauseCode] = @ClauseCode

	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_DeclarationClauseDelete]
-- ========================================================================================================================================

GO
 