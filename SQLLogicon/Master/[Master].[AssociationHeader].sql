USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [AssociationHeader] Record based on [AssociationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderSelect] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderSelect] 
    @AssociationID SMALLINT,
    @CountryCode NVARCHAR(3)
AS 

BEGIN

	SELECT [AssociationID], [CountryCode], [AssociationName], [Description] 
	FROM   [Master].[AssociationHeader]
	WHERE  [AssociationID] = @AssociationID  
	       AND [CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [AssociationHeader] Records from [AssociationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderList] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderList] 
	@CountryCode nvarchar(3)
AS 
BEGIN

	SELECT [AssociationID], [CountryCode], [AssociationName], [Description] 
	FROM   [Master].[AssociationHeader]
	Where CountryCode = @CountryCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [AssociationHeader] Records from [AssociationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderPageView] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderPageView] 
	@CountryCode nvarchar(3),
	@fetchrows bigint
AS 
BEGIN

	SELECT [AssociationID], [CountryCode], [AssociationName], [Description] 
	FROM   [Master].[AssociationHeader]
	Where CountryCode = @CountryCode
	ORDER BY  AssociationName
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [AssociationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderRecordCount] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderRecordCount] 
	@CountryCode nvarchar(3)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[AssociationHeader]
	Where CountryCode = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [AssociationHeader] Record based on [AssociationHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderAutoCompleteSearch] 
    @AssociationName nvarchar(50) 
AS 

BEGIN

	SELECT [AssociationID], [CountryCode], [AssociationName], [Description] 
	FROM   [Master].[AssociationHeader]
	WHERE  [AssociationName] LIKE '%' +  @AssociationName  + '%' 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [AssociationHeader] Record Into [AssociationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderInsert] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderInsert] 
    @AssociationID smallint,
    @CountryCode nvarchar(3),
    @AssociationName nvarchar(50),
    @Description nvarchar(100)
AS 
  

BEGIN

	
	INSERT INTO [Master].[AssociationHeader] (
			[AssociationID], [CountryCode], [AssociationName], [Description])
	SELECT	@AssociationID, @CountryCode, @AssociationName, @Description
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [AssociationHeader] Record Into [AssociationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderUpdate] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderUpdate] 
    @AssociationID smallint,
    @CountryCode nvarchar(3),
    @AssociationName nvarchar(50),
    @Description nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [Master].[AssociationHeader]
	SET   [AssociationName] = @AssociationName, [Description] = @Description
	WHERE  [AssociationID] = @AssociationID
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [AssociationHeader] Record Into [AssociationHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderSave] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderSave] 
    @AssociationID smallint,
    @CountryCode nvarchar(3),
    @AssociationName nvarchar(50),
    @Description nvarchar(100)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[AssociationHeader] 
		WHERE 	[AssociationID] = @AssociationID
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_AssociationHeaderUpdate] 
		@AssociationID, @CountryCode, @AssociationName, @Description


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_AssociationHeaderInsert] 
		@AssociationID, @CountryCode, @AssociationName, @Description


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[AssociationHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [AssociationHeader] Record  based on [AssociationHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationHeaderDelete] 
END 
GO
CREATE PROC [Master].[usp_AssociationHeaderDelete] 
    @AssociationID smallint,
    @CountryCode nvarchar(3)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[AssociationHeader]
	WHERE  [AssociationID] = @AssociationID
	       AND [CountryCode] = @CountryCode
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationHeaderDelete]
-- ========================================================================================================================================

GO
 