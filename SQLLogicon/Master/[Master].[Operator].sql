 

-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [Operator] Record based on [Operator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OperatorSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorSelect] 
END 
GO
CREATE PROC [Master].[usp_OperatorSelect] 
    @OperatorType smallint,
    @CountryCode nvarchar(3),
    @OperatorID bigint
AS 
 

BEGIN
	
	;with OperatorTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OperatorType')
	SELECT	Opr.[OperatorType], Opr.[CountryCode], Opr.OperatorID, Opr.[OperatorCode], Opr.[OperatorName], Opr.[ROCNo], Opr.[Address], Opr.[TelephoneNo], Opr.[FaxNo], Opr.[PostCode], 
			Opr.[LicenseNo], Opr.[LicenseStartDate], Opr.[LicenseEndDate], Opr.[Status], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
			ISNULL(Opt.LookupDescription,'') As OperatorTypeDescription,
			ISNULL(Cr.CountryName,'') As CountryName
	FROM	[Master].[Operator] Opr
	Left Outer Join OperatorTypeDescription Opt ON 
		Opr.OperatorType = Opt.LookupID
	Left Outer Join [Master].[Country] Cr ON 
		Opr.CountryCode = Cr.CountryCode
	WHERE  Opr.[OperatorType] = @OperatorType  
	       AND Opr.[CountryCode] = @CountryCode  
	       AND Opr.[OperatorID] = @OperatorID 

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [Operator] Records from [Operator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OperatorList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorList] 
END 
GO
CREATE PROC [Master].[usp_OperatorList] 
	@CountryCode nvarchar(3)
AS 
 
BEGIN
	;with OperatorTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OperatorType')
	SELECT	Opr.[OperatorType], Opr.[CountryCode],Opr.OperatorID, Opr.[OperatorCode], Opr.[OperatorName], Opr.[ROCNo], Opr.[Address], Opr.[TelephoneNo], Opr.[FaxNo], Opr.[PostCode], 
			Opr.[LicenseNo], Opr.[LicenseStartDate], Opr.[LicenseEndDate], Opr.[Status], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
			ISNULL(Opt.LookupDescription,'') As OperatorTypeDescription,
			ISNULL(Cr.CountryName,'') As CountryName
	FROM	[Master].[Operator] Opr
	Left Outer Join OperatorTypeDescription Opt ON 
		Opr.OperatorType = Opt.LookupID
	Left Outer Join [Master].[Country] Cr ON 
		Opr.CountryCode = Cr.CountryCode
	Where Opr.[CountryCode] = @CountryCode  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Operator] PageView Records from [Operator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OperatorPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorPageView]
END 
GO
CREATE PROC [Master].[usp_OperatorPageView] 
	@CountryCode nvarchar(3),
	@fetchrows bigint
AS 
BEGIN

	;with OperatorTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OperatorType')
	SELECT	Opr.[OperatorType], Opr.[CountryCode],Opr.OperatorID, Opr.[OperatorCode], Opr.[OperatorName], Opr.[ROCNo], Opr.[Address], Opr.[TelephoneNo], Opr.[FaxNo], Opr.[PostCode], 
			Opr.[LicenseNo], Opr.[LicenseStartDate], Opr.[LicenseEndDate], Opr.[Status], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
			ISNULL(Opt.LookupDescription,'') As OperatorTypeDescription,
			ISNULL(Cr.CountryName,'') As CountryName
	FROM	[Master].[Operator] Opr
	Left Outer Join OperatorTypeDescription Opt ON 
		Opr.OperatorType = Opt.LookupID
	Left Outer Join [Master].[Country] Cr ON 
		Opr.CountryCode = Cr.CountryCode
	Where Opr.CountryCode = @CountryCode 
	ORDER BY  [OperatorName]
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Operator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OperatorRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorRecordCount] 
END 
GO
CREATE PROC [Master].[usp_OperatorRecordCount] 
	@CountryCode nvarchar(3)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Master].[Operator]
		Where CountryCode = @CountryCode 

	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Operator] auto-complete search based on [Operator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OperatorAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorAutoCompleteSearch]
END 
GO
CREATE PROC [Master].[usp_OperatorAutoCompleteSearch]
    @OperatorType smallint,
    @CountryCode nvarchar(3),
    @OperatorName nvarchar(250)
     
AS 

BEGIN
	;with OperatorTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='OperatorType')
	SELECT	Opr.[OperatorType], Opr.[CountryCode], Opr.[OperatorCode], Opr.[OperatorName], Opr.[ROCNo], Opr.[Address], Opr.[TelephoneNo], Opr.[FaxNo], Opr.[PostCode], 
			Opr.[LicenseNo], Opr.[LicenseStartDate], Opr.[LicenseEndDate], Opr.[Status], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
			ISNULL(Opt.LookupDescription,'') As OperatorTypeDescription,
			ISNULL(Cr.CountryName,'') As CountryName
	FROM	[Master].[Operator] Opr
	Left Outer Join OperatorTypeDescription Opt ON 
		Opr.OperatorType = Opt.LookupID
	Left Outer Join [Master].[Country] Cr ON 
		Opr.CountryCode = Cr.CountryCode
	Where Opr.[OperatorType] = @OperatorType   
	       AND Opr.[CountryCode] = @CountryCode   
	       AND Opr.[OperatorName] LIKE '%' +  @OperatorName  + '%'
END
-- ========================================================================================================================================
-- END  											[Master].[usp_OperatorAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [Operator] Record Into [Operator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OperatorInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorInsert] 
END 
GO
CREATE PROC [Master].[usp_OperatorInsert] 
    @OperatorType smallint,
    @CountryCode nvarchar(3),
	@OperatorID bigint,
    @OperatorCode nvarchar(20),
    @OperatorName nvarchar(100) = NULL,
    @ROCNo nvarchar(50) = NULL,
    @Address nvarchar(250) = NULL,
    @TelephoneNo nvarchar(20) = NULL,
    @FaxNo nvarchar(20) = NULL,
    @PostCode nvarchar(50) = NULL,
    @LicenseNo nvarchar(50) = NULL,
    @LicenseStartDate datetime = NULL,
    @LicenseEndDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
  

BEGIN
	
	INSERT INTO [Master].[Operator] (
			[OperatorType], [CountryCode], [OperatorCode], [OperatorName], [ROCNo], [Address], [TelephoneNo], [FaxNo], [PostCode], [LicenseNo], [LicenseStartDate], 
			[LicenseEndDate], [Status], [CreatedBy], [CreatedOn] )
	SELECT	@OperatorType, @CountryCode, @OperatorCode, @OperatorName, @ROCNo, @Address, @TelephoneNo, @FaxNo, @PostCode, @LicenseNo, @LicenseStartDate, 
			@LicenseEndDate, cast(1 as bit), @CreatedBy,  GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [Operator] Record Into [Operator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OperatorUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorUpdate] 
END 
GO
CREATE PROC [Master].[usp_OperatorUpdate] 
    @OperatorType smallint,
    @CountryCode nvarchar(3),
	@OperatorID bigint,
    @OperatorCode nvarchar(20),
    @OperatorName nvarchar(100) = NULL,
    @ROCNo nvarchar(50) = NULL,
    @Address nvarchar(250) = NULL,
    @TelephoneNo nvarchar(20) = NULL,
    @FaxNo nvarchar(20) = NULL,
    @PostCode nvarchar(50) = NULL,
    @LicenseNo nvarchar(50) = NULL,
    @LicenseStartDate datetime = NULL,
    @LicenseEndDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 
	
BEGIN

	UPDATE [Master].[Operator]
	SET     [OperatorName] = @OperatorName, [ROCNo] = @ROCNo, [Address] = @Address, [TelephoneNo] = @TelephoneNo, [FaxNo] = @FaxNo, [PostCode] = @PostCode, 
			[LicenseNo] = @LicenseNo, [LicenseStartDate] = @LicenseStartDate, [LicenseEndDate] = @LicenseEndDate,  [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [OperatorType] = @OperatorType
	       AND [CountryCode] = @CountryCode
	       AND [OperatorID] = @OperatorID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [Operator] Record Into [Operator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OperatorSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorSave] 
END 
GO
CREATE PROC [Master].[usp_OperatorSave] 
    @OperatorType smallint,
    @CountryCode nvarchar(3),
	@OperatorID bigint,
    @OperatorCode nvarchar(20),
    @OperatorName nvarchar(100) = NULL,
    @ROCNo nvarchar(50) = NULL,
    @Address nvarchar(250) = NULL,
    @TelephoneNo nvarchar(20) = NULL,
    @FaxNo nvarchar(20) = NULL,
    @PostCode nvarchar(50) = NULL,
    @LicenseNo nvarchar(50) = NULL,
    @LicenseStartDate datetime = NULL,
    @LicenseEndDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[Operator] 
		WHERE 	[OperatorType] = @OperatorType
	       AND [CountryCode] = @CountryCode
	       AND [OperatorID] = @OperatorID)>0
	BEGIN
	    Exec [Master].[usp_OperatorUpdate] 
			@OperatorType, @CountryCode,@OperatorID, @OperatorCode, @OperatorName, @ROCNo, @Address, @TelephoneNo, @FaxNo, @PostCode, @LicenseNo, @LicenseStartDate, 
			@LicenseEndDate,  @CreatedBy,     @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_OperatorInsert] 
			@OperatorType, @CountryCode,@OperatorID, @OperatorCode, @OperatorName, @ROCNo, @Address, @TelephoneNo, @FaxNo, @PostCode, @LicenseNo, @LicenseStartDate, 
			@LicenseEndDate,  @CreatedBy,     @ModifiedBy 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[OperatorSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OperatorDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [Operator] Record  based on [Operator]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OperatorDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OperatorDelete] 
END 
GO
CREATE PROC [Master].[usp_OperatorDelete] 
    @OperatorType smallint,
    @CountryCode nvarchar(3),
    @OperatorID bigint,
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[Operator]
	SET	[Status] = CAST(0 as bit),ModifiedBy = @ModifiedBy , ModifiedOn = GETUTCDATE()
	WHERE 	[OperatorType] = @OperatorType
	       AND [CountryCode] = @CountryCode
	       AND [OperatorID] = @OperatorID
 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OperatorDelete]
-- ========================================================================================================================================

GO 