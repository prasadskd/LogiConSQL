-- ========================================================================================================================================
-- START											 [Master].[usp_PortListByPortType]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Ports] Record based on [PortType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortListByPortType]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortListByPortType]
END 
GO
CREATE PROC   [Master].[usp_PortListByPortType] 
	@portType as smallint
AS 
BEGIN

	;with 
	PortTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='PortType')
	SELECT P.[PortCode], P.[PortName], P.[CountryCode], P.[PortType], P.[Latitude], P.[Longitude], P.[ISOCode], P.[MappingCode], 
	P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn] ,
	C.CountryName , ISNULL(PL.LookupDescription,'') As PortTypeDescription
	FROM [Master].[Port] P
	Left Outer Join Master.Country C ON 
		P.CountryCode = C.CountryCode
	Left Outer Join PortTypeLookup PL ON 
		P.PortType = PL.LookupID
	Where p.PortType = @portType



END



-- ========================================================================================================================================
-- END              [Master].[usp_PortList] 
-- ========================================================================================================================================