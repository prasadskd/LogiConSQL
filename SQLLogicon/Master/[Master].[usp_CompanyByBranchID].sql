-- ========================================================================================================================================
-- START            [Master].[usp_CompanyByBranchID]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date:  05-Apr-2017
-- Description: Select the [Company] Record based on BranchID

-- Exec [Master].[usp_CompanyByBranchID] 1017001
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyByBranchID]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyByBranchID] 
END 
GO
CREATE PROC [Master].[usp_CompanyByBranchID]
    @BranchID bigint
AS 
 

BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType],
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks,Rc.AgentCode, Rc.EDIMailBoxNo
	 FROM   [master].[Branch] Br
	 Left Outer Join [Master].[Company] Rc ON
		Br.CompanyCode = Rc.CompanyCode 
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID
	 WHERE  Br.BranchID = @BranchID

END
-- ========================================================================================================================================
-- END              [Master].[usp_CompanyByBranchID]
-- ========================================================================================================================================

GO