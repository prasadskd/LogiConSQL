 


-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ChargeMaster] Record based on [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterSelect] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterSelect] 
    @BranchID BIGINT,
    @ChargeCode NVARCHAR(20)
AS 

BEGIN

	--Declare @DefaultBranchID bigint = 0;
	--select @DefaultBranchID = [Utility].[udf_GetRootBranchCode]()

	
	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT	Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
			Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
			Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
			Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
			ISNULL(Mld.LookupDescription,'') As ModuleDescription,
			ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
			ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
			ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
	       AND Chg.[ChargeCode] = @ChargeCode 
		   --And IsActive = CAST(1 as bit) 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- Exec [Master].[usp_ChargeMasterList] 10
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterList] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterList] 
    @BranchID BIGINT
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
	And IsActive = CAST(1 as bit)

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterPageList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterPageView] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
	ORDER BY Chg.[ChargeCode]
	OFFSET  10 ROWS 
	FETCH NEXT @fetchrows ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterPageList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterListByMovementCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterListByMovementCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterListByMovementCode] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterListByMovementCode] 
	@BranchID BIGINT,
	@Module smallint
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
		   And Chg.Module = @Module
		   And Chg.IsActive = CAST(1 as bit)
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterListByMovementCode] 
-- ========================================================================================================================================

GO





-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterListForGeneralCharges]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterListForGeneralCharges]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterListForGeneralCharges] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterListForGeneralCharges] 
	@BranchID BIGINT,
	@Module smallint
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
		   And Chg.Module = @Module
		   And Chg.IsActive = CAST(1 as bit)
		   And ChgTyp.LookupDescription NOT IN ('Storage Charge','VAS Charge')
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterListForGeneralCharges] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterListForVASCharges]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterListForVASCharges]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterListForVASCharges] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterListForVASCharges] 
	@BranchID BIGINT,
	@Module smallint
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
		   And Chg.Module = @Module
		   And Chg.IsActive = CAST(1 as bit)
		   And ChgTyp.LookupDescription IN ('VAS Charge')
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterListForVASCharges] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterRecordCount] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterRecordCount] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[ChargeMaster] Chg
	Where (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ChargeMaster] Records from [ChargeMaster] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ChargeMasterAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterAutoCompleteSearch] 
	@BranchID BIGINT,
	@Module smallint,
	@ChargeDescription nvarchar(100)
AS 
BEGIN

	;With
	ModuleLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	BillingUnitLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='BillingUnitType'),
	ChargeTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeType'),
	ChargeMethodLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory ='ChargeMethod')
	SELECT Chg.[BranchID], Chg.[ChargeCode], Chg.[ChargeDescription], Chg.[ShortName], 
		   Chg.[Module], Chg.[BillingUnit], Chg.[ChargeType], Chg.[ChargeMethod], Chg.[CreditTerm], Chg.[RevenueCode], Chg.[ExpensesCode], Chg.[GLCode], 
		   Chg.[SurChargeCode], Chg.[Percentage], Chg.[InputGSTCode], Chg.[InputGSTRate], Chg.[OutPutGSTCode], Chg.[OutPutGSTRate], Chg.[IsActive], 
		   Chg.[CreatedBy], Chg.[CreatedOn], Chg.[ModifiedBy], Chg.[ModifiedOn], Chg.[CancelBy], Chg.[CancelOn],
			Chg.IsPreLoadExport,Chg.IsPreLoadImport,Chg.IsPreLoadLocal,Chg.IsJobCategory,Chg.IsProduct,Chg.IsInternal,
			Chg.IsRevenue,Chg.IsVAS,Chg.IsPreLoadInward	,Chg.IsPreLoadOutward,Chg.IsLoadToTruckIn,Chg.IsCrossDock,
			Chg.IsWorkOrder,Chg.IsRequireApproval,IsLoadFullPallet,
		   ISNULL(Mld.LookupDescription,'') As ModuleDescription,
		   ISNULL(Bu.LookupDescription,'') As BillingUnitDescription,
		   ISNULL(ChgTyp.LookupDescription,'') As ChargeTypeDescription,
		   ISNULL(ChgMet.LookupDescription,'') As ChargeMethodDescription
	FROM   [Master].[ChargeMaster] Chg
	Left Outer Join ModuleLookup Mld ON 
		Chg.Module = Mld.LookupID
	Left Outer Join BillingUnitLookup Bu ON 
		Chg.BillingUnit = Bu.LookupID
	Left Outer Join ChargeTypeLookup ChgTyp ON 
		Chg.ChargeType = ChgTyp.LookupID
	Left Outer Join ChargeMethodLookup ChgMet ON 
		Chg.ChargeMethod = ChgMet.LookupID
	WHERE  (Chg.[BranchID] = @BranchID  OR Chg.BranchID = [Utility].[udf_GetRootBranchCode]())
		   And Chg.Module = @Module
		   And Chg.[ChargeDescription] Like '%' + @ChargeDescription  + '%'
		   And Chg.IsActive = CAST(1 as bit)
		   And ChgTyp.LookupDescription NOT IN ('Storage Charge')
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterListForVASCharges] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [ChargeMaster] Record Into [ChargeMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ChargeMasterInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterInsert] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterInsert] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @ChargeDescription nvarchar(100),
    @ShortName nvarchar(10),
    @Module smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @ChargeMethod smallint,
    @CreditTerm smallint,
    @RevenueCode nvarchar(20),
    @ExpensesCode nvarchar(20),
    @GLCode nvarchar(20),
    @SurChargeCode nvarchar(20),
    @Percentage numeric(18, 2),
    @InputGSTCode nvarchar(10),
    @InputGSTRate numeric(18, 2),
    @OutPutGSTCode nvarchar(10),
    @OutPutGSTRate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@IsPreLoadExport bit NULL,
	@IsPreLoadImport bit NULL,
	@IsPreLoadLocal bit NULL,
	@IsJobCategory bit NULL,
	@IsProduct bit NULL,
	@IsInternal bit NULL,
	@IsRevenue bit NULL,
	@IsVAS bit NULL,
	@IsPreLoadInward	 bit NULL,
	@IsPreLoadOutward bit NULL,
	@IsLoadToTruckIn bit NULL,
	@IsCrossDock bit NULL,
	@IsWorkOrder bit NULL,
	@IsRequireApproval bit NULL,
	@IsLoadFullPallet bit NULL

AS 
  

BEGIN

	
	INSERT INTO [Master].[ChargeMaster] (
			[BranchID], [ChargeCode], [ChargeDescription], [ShortName], [Module], [BillingUnit], [ChargeType], [ChargeMethod], [CreditTerm], 
			[RevenueCode], [ExpensesCode], [GLCode], [SurChargeCode], [Percentage], [InputGSTCode], [InputGSTRate], [OutPutGSTCode], [OutPutGSTRate], 
			[IsActive], [CreatedBy], [CreatedOn],IsPreLoadExport,IsPreLoadImport,IsPreLoadLocal,IsJobCategory,IsProduct,IsInternal,
			IsRevenue,IsVAS,IsPreLoadInward	,IsPreLoadOutward,IsLoadToTruckIn,IsCrossDock,
			IsWorkOrder,IsRequireApproval,IsLoadFullPallet)
	SELECT	@BranchID, @ChargeCode, @ChargeDescription, @ShortName, @Module, @BillingUnit, @ChargeType, @ChargeMethod, @CreditTerm, 
			@RevenueCode, @ExpensesCode, @GLCode, @SurChargeCode, @Percentage, @InputGSTCode, @InputGSTRate, @OutPutGSTCode, @OutPutGSTRate, 
			CAST(1 as bit), @CreatedBy, GETUTCDATE(),@IsPreLoadExport,@IsPreLoadImport,@IsPreLoadLocal,@IsJobCategory,@IsProduct,@IsInternal,
			@IsRevenue,@IsVAS,@IsPreLoadInward	,@IsPreLoadOutward,@IsLoadToTruckIn,@IsCrossDock,
			@IsWorkOrder,@IsRequireApproval,@IsLoadFullPallet
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [ChargeMaster] Record Into [ChargeMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ChargeMasterUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterUpdate] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterUpdate] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @ChargeDescription nvarchar(100),
    @ShortName nvarchar(10),
    @Module smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @ChargeMethod smallint,
    @CreditTerm smallint,
    @RevenueCode nvarchar(20),
    @ExpensesCode nvarchar(20),
    @GLCode nvarchar(20),
    @SurChargeCode nvarchar(20),
    @Percentage numeric(18, 2),
    @InputGSTCode nvarchar(10),
    @InputGSTRate numeric(18, 2),
    @OutPutGSTCode nvarchar(10),
    @OutPutGSTRate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@IsPreLoadExport bit NULL,
	@IsPreLoadImport bit NULL,
	@IsPreLoadLocal bit NULL,
	@IsJobCategory bit NULL,
	@IsProduct bit NULL,
	@IsInternal bit NULL,
	@IsRevenue bit NULL,
	@IsVAS bit NULL,
	@IsPreLoadInward	 bit NULL,
	@IsPreLoadOutward bit NULL,
	@IsLoadToTruckIn bit NULL,
	@IsCrossDock bit NULL,
	@IsWorkOrder bit NULL,
	@IsRequireApproval bit NULL,
	@IsLoadFullPallet bit NULL

AS 
 
	
BEGIN

	UPDATE [Master].[ChargeMaster]
	SET    [ChargeDescription] = @ChargeDescription, [ShortName] = @ShortName, [Module] = @Module, [BillingUnit] = @BillingUnit, 
			[ChargeType] = @ChargeType, [ChargeMethod] = @ChargeMethod, [CreditTerm] = @CreditTerm, [RevenueCode] = @RevenueCode, 
			[ExpensesCode] = @ExpensesCode, [GLCode] = @GLCode, [SurChargeCode] = @SurChargeCode, [Percentage] = @Percentage, 
			[InputGSTCode] = @InputGSTCode, [InputGSTRate] = @InputGSTRate, [OutPutGSTCode] = @OutPutGSTCode, [OutPutGSTRate] = @OutPutGSTRate, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),
			IsPreLoadExport=@IsPreLoadExport,IsPreLoadImport=@IsPreLoadImport,IsPreLoadLocal=@IsPreLoadLocal,IsJobCategory=@IsJobCategory,
			IsProduct=@IsProduct,IsInternal=@IsInternal,IsRevenue=@IsRevenue,IsVAS=@IsVAS,IsPreLoadInward=@IsPreLoadInward,
			IsPreLoadOutward=@IsPreLoadOutward,IsLoadToTruckIn=@IsLoadToTruckIn,IsCrossDock=@IsCrossDock,IsWorkOrder=@IsWorkOrder,
			IsRequireApproval=@IsRequireApproval,IsLoadFullPallet=@IsLoadFullPallet

	WHERE  [BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [ChargeMaster] Record Into [ChargeMaster] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ChargeMasterSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterSave] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterSave] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
    @ChargeDescription nvarchar(100),
    @ShortName nvarchar(10),
    @Module smallint,
    @BillingUnit smallint,
    @ChargeType smallint,
    @ChargeMethod smallint,
    @CreditTerm smallint,
    @RevenueCode nvarchar(20),
    @ExpensesCode nvarchar(20),
    @GLCode nvarchar(20),
    @SurChargeCode nvarchar(20),
    @Percentage numeric(18, 2),
    @InputGSTCode nvarchar(10),
    @InputGSTRate numeric(18, 2),
    @OutPutGSTCode nvarchar(10),
    @OutPutGSTRate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@IsPreLoadExport bit NULL,
	@IsPreLoadImport bit NULL,
	@IsPreLoadLocal bit NULL,
	@IsJobCategory bit NULL,
	@IsProduct bit NULL,
	@IsInternal bit NULL,
	@IsRevenue bit NULL,
	@IsVAS bit NULL,
	@IsPreLoadInward	 bit NULL,
	@IsPreLoadOutward bit NULL,
	@IsLoadToTruckIn bit NULL,
	@IsCrossDock bit NULL,
	@IsWorkOrder bit NULL,
	@IsRequireApproval bit NULL,
	@IsLoadFullPallet bit NULL
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[ChargeMaster] 
		WHERE 	[BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode)>0
	BEGIN
	    Exec [Master].[usp_ChargeMasterUpdate] 
				@BranchID, @ChargeCode, @ChargeDescription, @ShortName, @Module, @BillingUnit, @ChargeType, @ChargeMethod, @CreditTerm, 
				@RevenueCode, @ExpensesCode, @GLCode, @SurChargeCode, @Percentage, @InputGSTCode, @InputGSTRate, @OutPutGSTCode, 
				@OutPutGSTRate, @CreatedBy, @ModifiedBy,@IsPreLoadExport,@IsPreLoadImport,@IsPreLoadLocal,@IsJobCategory,@IsProduct,@IsInternal,
				@IsRevenue,@IsVAS,@IsPreLoadInward	,@IsPreLoadOutward,@IsLoadToTruckIn,@IsCrossDock,
				@IsWorkOrder,@IsRequireApproval,@IsLoadFullPallet


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_ChargeMasterInsert] 
				@BranchID, @ChargeCode, @ChargeDescription, @ShortName, @Module, @BillingUnit, @ChargeType, @ChargeMethod, @CreditTerm, 
				@RevenueCode, @ExpensesCode, @GLCode, @SurChargeCode, @Percentage, @InputGSTCode, @InputGSTRate, @OutPutGSTCode, 
				@OutPutGSTRate, @CreatedBy, @ModifiedBy,@IsPreLoadExport,@IsPreLoadImport,@IsPreLoadLocal,@IsJobCategory,@IsProduct,@IsInternal,
				@IsRevenue,@IsVAS,@IsPreLoadInward	,@IsPreLoadOutward,@IsLoadToTruckIn,@IsCrossDock,
				@IsWorkOrder,@IsRequireApproval,@IsLoadFullPallet

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[ChargeMasterSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ChargeMasterDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [ChargeMaster] Record  based on [ChargeMaster]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ChargeMasterDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ChargeMasterDelete] 
END 
GO
CREATE PROC [Master].[usp_ChargeMasterDelete] 
    @BranchID BIGINT,
    @ChargeCode nvarchar(20),
	@CancelBy nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[ChargeMaster]
	SET	IsActive = CAST(0 as bit),CancelBy=@CancelBy,CancelOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [ChargeCode] = @ChargeCode

	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ChargeMasterDelete]
-- ========================================================================================================================================

GO

 