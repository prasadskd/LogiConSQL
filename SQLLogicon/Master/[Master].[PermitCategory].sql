


-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategorySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PermitCategory] Record based on [PermitCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PermitCategorySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategorySelect] 
END 
GO
CREATE PROC [Master].[usp_PermitCategorySelect] 
    @PermitCode NVARCHAR(10),
    @OGACode SMALLINT,
    @CountryCode VARCHAR(2)
AS 

BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT PC.[PermitCode], PC.[OGACode], PC.[CountryCode], PC.[PermitDescription], PC.[IsActive], PC.[CreatedBy], PC.[CreatedOn], PC.[ModifiedBy], PC.[ModifiedOn] ,
		ISNULL(oga.LookupDescription,'') As OGADescription,
		ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[PermitCategory] PC
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Left Outer Join Master.Country C On	
		Pc.CountryCode = C.CountryCode
	WHERE  Pc.[PermitCode] = @PermitCode  
	       AND Pc.[OGACode] = @OGACode  
	       AND Pc.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategorySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PermitCategory] Records from [PermitCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PermitCategoryList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryList] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryList] 
	    @CountryCode VARCHAR(2)

AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT PC.[PermitCode], PC.[OGACode], PC.[CountryCode], PC.[PermitDescription], PC.[IsActive], PC.[CreatedBy], PC.[CreatedOn], PC.[ModifiedBy], PC.[ModifiedOn] ,
		ISNULL(oga.LookupDescription,'') As OGADescription,
		ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[PermitCategory] PC
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Left Outer Join Master.Country C On	
		Pc.CountryCode = C.CountryCode
	Where Pc.CountryCode = @CountryCode


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [PermitCategory] Records from [PermitCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PermitCategoryPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryPageView] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryPageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT PC.[PermitCode], PC.[OGACode], PC.[CountryCode], PC.[PermitDescription], PC.[IsActive], PC.[CreatedBy], PC.[CreatedOn], PC.[ModifiedBy], PC.[ModifiedOn] ,
		ISNULL(oga.LookupDescription,'') As OGADescription,
		ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[PermitCategory] PC
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Left Outer Join Master.Country C On	
		Pc.CountryCode = C.CountryCode
	Where Pc.CountryCode = @CountryCode
	ORDER BY [PermitCode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [PermitCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PermitCategoryRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryRecordCount] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryRecordCount] 
		@CountryCode varchar(2)
AS 
BEGIN

	SELECT COUNT(0)
	FROM   [Master].[PermitCategory] PC
	Left Outer Join Master.Country C On	
		Pc.CountryCode = C.CountryCode
	Where Pc.CountryCode = @CountryCode


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [PermitCategory] Record based on [PermitCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PermitCategoryAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryAutoCompleteSearch] 
    @CountryCode VARCHAR(2),
	@PermitDescription nvarchar(100)
AS 

BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='OGACode')
	SELECT PC.[PermitCode], PC.[OGACode], PC.[CountryCode], PC.[PermitDescription], PC.[IsActive], PC.[CreatedBy], PC.[CreatedOn], PC.[ModifiedBy], PC.[ModifiedOn] ,
		ISNULL(oga.LookupDescription,'') As OGADescription,
		ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[PermitCategory] PC
	Left Outer Join OGADescription oga ON
		PC.OGACode = oga.LookupID
	Left Outer Join Master.Country C On	
		Pc.CountryCode = C.CountryCode
	WHERE  PC.[CountryCode] = @CountryCode  
		And PC.PermitDescription LIKE '%' + @PermitDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [PermitCategory] Record Into [PermitCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PermitCategoryInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryInsert] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryInsert] 
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @PermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[PermitCategory] ([PermitCode], [OGACode], [CountryCode], [PermitDescription], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @PermitCode, @OGACode, @CountryCode, @PermitDescription, cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [PermitCategory] Record Into [PermitCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PermitCategoryUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryUpdate] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryUpdate] 
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @PermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[PermitCategory]
	SET    [PermitDescription] = @PermitDescription, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategorySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [PermitCategory] Record Into [PermitCategory] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PermitCategorySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategorySave] 
END 
GO
CREATE PROC [Master].[usp_PermitCategorySave] 
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @PermitDescription nvarchar(255),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[PermitCategory] 
		WHERE 	[PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_PermitCategoryUpdate] 
		@PermitCode, @OGACode, @CountryCode, @PermitDescription, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_PermitCategoryInsert] 
		@PermitCode, @OGACode, @CountryCode, @PermitDescription, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[PermitCategorySave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_PermitCategoryDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [PermitCategory] Record  based on [PermitCategory]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PermitCategoryDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PermitCategoryDelete] 
END 
GO
CREATE PROC [Master].[usp_PermitCategoryDelete] 
    @PermitCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[PermitCategory]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy, ModifiedOn = GETUTCDATE()
	WHERE 	[PermitCode] = @PermitCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PermitCategoryDelete]
-- ========================================================================================================================================

GO

