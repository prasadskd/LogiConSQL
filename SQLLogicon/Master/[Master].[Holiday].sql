 


-- ========================================================================================================================================
-- START											 [Master].[usp_HolidaySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Holiday] Record based on [Holiday] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HolidaySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidaySelect] 
END 
GO
CREATE PROC [Master].[usp_HolidaySelect] 
    @BranchID BIGINT,
    @HolidayDate DATETIME
AS 

BEGIN

	SELECT [BranchID], [HolidayDate], [HolidayDescription], [CountryCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Holiday]
	WHERE  [BranchID] = @BranchID  
	       AND [HolidayDate] = @HolidayDate 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidaySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Holiday] Records from [Holiday] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HolidayList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayList] 
END 
GO
CREATE PROC [Master].[usp_HolidayList] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT [BranchID], [HolidayDate], [HolidayDescription], [CountryCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Holiday]
	Where BranchID = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Holiday] Records from [Holiday] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HolidayPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayPageView] 
END 
GO
CREATE PROC [Master].[usp_HolidayPageView] 
	@BranchID BIGINT,
	@fetchrows bigint
AS 
BEGIN

	SELECT [BranchID], [HolidayDate], [HolidayDescription], [CountryCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Holiday]
	Where BranchID = @BranchID
	ORDER BY  [HolidayDate]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Holiday] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HolidayRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayRecordCount] 
END 
GO
CREATE PROC [Master].[usp_HolidayRecordCount] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Holiday]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Holiday] Record based on [Holiday] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HolidayAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_HolidayAutoCompleteSearch] 
    @BranchID BIGINT,
    @HolidayDate DATETIME
AS 

BEGIN

	SELECT [BranchID], [HolidayDate], [HolidayDescription], [CountryCode], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Holiday]
	WHERE  [BranchID] = @BranchID  
	       AND [HolidayDate] LIKE '%' + @HolidayDate  + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Holiday] Record Into [Holiday] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HolidayInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayInsert] 
END 
GO
CREATE PROC [Master].[usp_HolidayInsert] 
    @BranchID BIGINT,
    @HolidayDate datetime,
    @HolidayDescription nvarchar(100),
    @CountryCode varchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[Holiday] ([BranchID], [HolidayDate], [HolidayDescription], [CountryCode], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @HolidayDate, @HolidayDescription, @CountryCode, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Holiday] Record Into [Holiday] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HolidayUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayUpdate] 
END 
GO
CREATE PROC [Master].[usp_HolidayUpdate] 
    @BranchID BIGINT,
    @HolidayDate datetime,
    @HolidayDescription nvarchar(100),
    @CountryCode varchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[Holiday]
	SET    [HolidayDescription] = @HolidayDescription, [CountryCode] = @CountryCode, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [HolidayDate] = @HolidayDate
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HolidaySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Holiday] Record Into [Holiday] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HolidaySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidaySave] 
END 
GO
CREATE PROC [Master].[usp_HolidaySave] 
    @BranchID BIGINT,
    @HolidayDate datetime,
    @HolidayDescription nvarchar(100),
    @CountryCode varchar(2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Holiday] 
		WHERE 	[BranchID] = @BranchID
	       AND [HolidayDate] = @HolidayDate)>0
	BEGIN
	    Exec [Master].[usp_HolidayUpdate] 
		@BranchID, @HolidayDate, @HolidayDescription, @CountryCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_HolidayInsert] 
		@BranchID, @HolidayDate, @HolidayDescription, @CountryCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[HolidaySave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_HolidayDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Holiday] Record  based on [Holiday]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HolidayDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HolidayDelete] 
END 
GO
CREATE PROC [Master].[usp_HolidayDelete] 
    @BranchID BIGINT,
    @HolidayDate datetime
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[Holiday]
	WHERE  [BranchID] = @BranchID
	       AND Convert(char(10),[HolidayDate],120) = Convert(Char(10),@HolidayDate,120)
	 
	Return 1;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HolidayDelete]
-- ========================================================================================================================================

GO
 