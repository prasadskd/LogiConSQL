 

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CustomDocumentFormat] Record based on [CustomDocumentFormat] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDocumentFormatSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatSelect] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatSelect] 
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10)
AS 
 

BEGIN

	SELECT	[CountryCode], [DocumentCode], [Description], [DocumentType], [IsSentOut], [IsRequireStation], [IsRequireDate], [DateType], 
			[DateQualifier], [IsRequiredAlert], [AlertMessageType], [DeclarationTypeList], [IsOGACode], [IsOGABranchCode], [IsUseYear], [IsUseAmount] 
	FROM	[Master].[CustomDocumentFormat]
	WHERE	[CountryCode] = @CountryCode   
	       AND [DocumentCode] = @DocumentCode   

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CustomDocumentFormat] Records from [CustomDocumentFormat] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDocumentFormatList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatList] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatList] 
    @CountryCode nvarchar(2)

AS 
 
BEGIN
	SELECT [CountryCode], [DocumentCode], [Description], [DocumentType], [IsSentOut], [IsRequireStation], [IsRequireDate], [DateType], [DateQualifier], [IsRequiredAlert], [AlertMessageType], [DeclarationTypeList], [IsOGACode], [IsOGABranchCode], [IsUseYear], [IsUseAmount] 
	FROM   [Master].[CustomDocumentFormat]
	WHERE	[CountryCode] = @CountryCode   

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomDocumentFormat] PageView Records from [CustomDocumentFormat] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDocumentFormatPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatPageView]
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatPageView] 
    @CountryCode nvarchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT [CountryCode], [DocumentCode], [Description], [DocumentType], [IsSentOut], [IsRequireStation], [IsRequireDate], [DateType], [DateQualifier], [IsRequiredAlert], [AlertMessageType], [DeclarationTypeList], [IsOGACode], [IsOGABranchCode], [IsUseYear], [IsUseAmount] 
	FROM   [Master].[CustomDocumentFormat]
	WHERE	[CountryCode] = @CountryCode   
	ORDER BY  [DocumentCode] 
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CustomDocumentFormat] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDocumentFormatRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatRecordCount] 
    @CountryCode nvarchar(2)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Master].[CustomDocumentFormat]
		WHERE	[CountryCode] = @CountryCode   


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomDocumentFormat] auto-complete search based on [CustomDocumentFormat] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomDocumentFormatAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatAutoCompleteSearch]
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatAutoCompleteSearch]
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10)
     
AS 

BEGIN
	SELECT [CountryCode], [DocumentCode], [Description], [DocumentType], [IsSentOut], [IsRequireStation], [IsRequireDate], [DateType], [DateQualifier], [IsRequiredAlert], [AlertMessageType], [DeclarationTypeList], [IsOGACode], [IsOGABranchCode], [IsUseYear], [IsUseAmount] 
	FROM   [Master].[CustomDocumentFormat]
	WHERE  [CountryCode] = @CountryCode   
	       AND [DocumentCode]  LIKE '%' + @DocumentCode  + '%' 
END
-- ========================================================================================================================================
-- END  											[Master].[usp_CustomDocumentFormatAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CustomDocumentFormat] Record Into [CustomDocumentFormat] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDocumentFormatInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatInsert] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatInsert] 
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10),
    @Description nvarchar(100) = NULL,
    @DocumentType nvarchar(10) = NULL,
    @IsSentOut bit = NULL,
    @IsRequireStation bit = NULL,
    @IsRequireDate bit = NULL,
    @DateType nvarchar(5) = NULL,
    @DateQualifier smallint = NULL,
    @IsRequiredAlert bit = NULL,
    @AlertMessageType nvarchar(255) = NULL,
    @DeclarationTypeList nvarchar(200) = NULL,
    @IsOGACode bit = NULL,
    @IsOGABranchCode bit = NULL,
    @IsUseYear bit = NULL,
    @IsUseAmount bit = NULL
AS 
  

BEGIN
	
	INSERT INTO [Master].[CustomDocumentFormat] ([CountryCode], [DocumentCode], [Description], [DocumentType], [IsSentOut], [IsRequireStation], [IsRequireDate], [DateType], [DateQualifier], [IsRequiredAlert], [AlertMessageType], [DeclarationTypeList], [IsOGACode], [IsOGABranchCode], [IsUseYear], [IsUseAmount])
	SELECT @CountryCode, @DocumentCode, @Description, @DocumentType, @IsSentOut, @IsRequireStation, @IsRequireDate, @DateType, @DateQualifier, @IsRequiredAlert, @AlertMessageType, @DeclarationTypeList, @IsOGACode, @IsOGABranchCode, @IsUseYear, @IsUseAmount
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CustomDocumentFormat] Record Into [CustomDocumentFormat] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDocumentFormatUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatUpdate] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatUpdate] 
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10),
    @Description nvarchar(100) = NULL,
    @DocumentType nvarchar(10) = NULL,
    @IsSentOut bit = NULL,
    @IsRequireStation bit = NULL,
    @IsRequireDate bit = NULL,
    @DateType nvarchar(5) = NULL,
    @DateQualifier smallint = NULL,
    @IsRequiredAlert bit = NULL,
    @AlertMessageType nvarchar(255) = NULL,
    @DeclarationTypeList nvarchar(200) = NULL,
    @IsOGACode bit = NULL,
    @IsOGABranchCode bit = NULL,
    @IsUseYear bit = NULL,
    @IsUseAmount bit = NULL
AS 
 
	
BEGIN

	UPDATE [Master].[CustomDocumentFormat]
	SET    [CountryCode] = @CountryCode, [DocumentCode] = @DocumentCode, [Description] = @Description, [DocumentType] = @DocumentType, [IsSentOut] = @IsSentOut, [IsRequireStation] = @IsRequireStation, [IsRequireDate] = @IsRequireDate, [DateType] = @DateType, [DateQualifier] = @DateQualifier, [IsRequiredAlert] = @IsRequiredAlert, [AlertMessageType] = @AlertMessageType, [DeclarationTypeList] = @DeclarationTypeList, [IsOGACode] = @IsOGACode, [IsOGABranchCode] = @IsOGABranchCode, [IsUseYear] = @IsUseYear, [IsUseAmount] = @IsUseAmount
	WHERE  [CountryCode] = @CountryCode
	       AND [DocumentCode] = @DocumentCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CustomDocumentFormat] Record Into [CustomDocumentFormat] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDocumentFormatSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatSave] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatSave] 
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10),
    @Description nvarchar(100) = NULL,
    @DocumentType nvarchar(10) = NULL,
    @IsSentOut bit = NULL,
    @IsRequireStation bit = NULL,
    @IsRequireDate bit = NULL,
    @DateType nvarchar(5) = NULL,
    @DateQualifier smallint = NULL,
    @IsRequiredAlert bit = NULL,
    @AlertMessageType nvarchar(255) = NULL,
    @DeclarationTypeList nvarchar(200) = NULL,
    @IsOGACode bit = NULL,
    @IsOGABranchCode bit = NULL,
    @IsUseYear bit = NULL,
    @IsUseAmount bit = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[CustomDocumentFormat] 
		WHERE 	[CountryCode] = @CountryCode
	       AND [DocumentCode] = @DocumentCode)>0
	BEGIN
	    Exec [Master].[usp_CustomDocumentFormatUpdate] 
		@CountryCode, @DocumentCode, @Description, @DocumentType, @IsSentOut, @IsRequireStation, @IsRequireDate, @DateType, @DateQualifier, @IsRequiredAlert, @AlertMessageType, @DeclarationTypeList, @IsOGACode, @IsOGABranchCode, @IsUseYear, @IsUseAmount


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CustomDocumentFormatInsert] 
		@CountryCode, @DocumentCode, @Description, @DocumentType, @IsSentOut, @IsRequireStation, @IsRequireDate, @DateType, @DateQualifier, @IsRequiredAlert, @AlertMessageType, @DeclarationTypeList, @IsOGACode, @IsOGABranchCode, @IsUseYear, @IsUseAmount


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CustomDocumentFormatSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomDocumentFormatDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CustomDocumentFormat] Record  based on [CustomDocumentFormat]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomDocumentFormatDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomDocumentFormatDelete] 
END 
GO
CREATE PROC [Master].[usp_CustomDocumentFormatDelete] 
    @CountryCode nvarchar(2),
    @DocumentCode nvarchar(10)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[CustomDocumentFormat]
	WHERE  [CountryCode] = @CountryCode
	       AND [DocumentCode] = @DocumentCode
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomDocumentFormatDelete]
-- ========================================================================================================================================

GO 
