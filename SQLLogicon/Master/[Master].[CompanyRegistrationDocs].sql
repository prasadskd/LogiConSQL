
-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationDocs] Record based on [CompanyRegistrationDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsSelect] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsSelect] 
    @RegistrationType SMALLINT,
    @RegistrationDocType SMALLINT
AS 

BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType'),
	RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationDocType')
	SELECT CR.[RegistrationType], CR.[RegistrationDocType], CR.[IsMandatory],
	ISNULL(LCR.LookupDescription,'') As RegistrationTypeDescription,
	ISNULL(LCD.LookupDescription,'') As RegistrationDocTypeDescription
	FROM   [Master].[CompanyRegistrationDocs] CR
	Left Outer Join RegistrationTypeDescription LCR ON
	CR.RegistrationType = LCR.LookupID
	Left Outer Join RegistrationDocTypeDescription LCD ON
	CR.RegistrationDocType = LCD.LookupID
	WHERE  CR.[RegistrationType] = @RegistrationType 
	       AND CR.[RegistrationDocType] = @RegistrationDocType  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationDocs] Records from [CompanyRegistrationDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsList] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsList] 

AS 
BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType'),
	RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationDocType')
	SELECT CR.[RegistrationType], CR.[RegistrationDocType], CR.[IsMandatory],
	ISNULL(LCR.LookupDescription,'') As RegistrationTypeDescription,
	ISNULL(LCD.LookupDescription,'') As RegistrationDocTypeDescription
	FROM   [Master].[CompanyRegistrationDocs] CR
	Left Outer Join RegistrationTypeDescription LCR ON
	CR.RegistrationType = LCR.LookupID
	Left Outer Join RegistrationDocTypeDescription LCD ON
	CR.RegistrationDocType = LCD.LookupID


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CompanyRegistrationDocs] Records from [CompanyRegistrationDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsPageView] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsPageView] 
	@fetchrows bigint
AS 
BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType'),
	RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationDocType')
	SELECT CR.[RegistrationType], CR.[RegistrationDocType], CR.[IsMandatory],
	ISNULL(LCR.LookupDescription,'') As RegistrationTypeDescription,
	ISNULL(LCD.LookupDescription,'') As RegistrationDocTypeDescription
	FROM   [Master].[CompanyRegistrationDocs] CR
	Left Outer Join RegistrationTypeDescription LCR ON
	CR.RegistrationType = LCR.LookupID
	Left Outer Join RegistrationDocTypeDescription LCD ON
	CR.RegistrationDocType = LCD.LookupID
	ORDER BY RegistrationTypeDescription
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CompanyRegistrationDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[CompanyRegistrationDocs]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CompanyRegistrationDocs] Record based on [CompanyRegistrationDocs] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsAutoCompleteSearch] 
    @RegistrationTypeDescription nvarchar(50) 
AS 

BEGIN

	;with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType'),
	RegistrationDocTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationDocType')
	SELECT CR.[RegistrationType], CR.[RegistrationDocType], CR.[IsMandatory],
	ISNULL(LCR.LookupDescription,'') As RegistrationTypeDescription,
	ISNULL(LCD.LookupDescription,'') As RegistrationDocTypeDescription
	FROM   [Master].[CompanyRegistrationDocs] CR
	Left Outer Join RegistrationTypeDescription LCR ON
	CR.RegistrationType = LCR.LookupID
	Left Outer Join RegistrationDocTypeDescription LCD ON
	CR.RegistrationDocType = LCD.LookupID
	WHERE  LCR.LookupDescription like '%' + @RegistrationTypeDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CompanyRegistrationDocs] Record Into [CompanyRegistrationDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsInsert] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsInsert] 
    @RegistrationType smallint,
    @RegistrationDocType smallint,
    @IsMandatory bit
AS 
  

BEGIN

	
	INSERT INTO [Master].[CompanyRegistrationDocs] ([RegistrationType], [RegistrationDocType], [IsMandatory])
	SELECT @RegistrationType, @RegistrationDocType, @IsMandatory
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CompanyRegistrationDocs] Record Into [CompanyRegistrationDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsUpdate] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsUpdate] 
    @RegistrationType smallint,
    @RegistrationDocType smallint,
    @IsMandatory bit
AS 
 
	
BEGIN

	UPDATE [Master].[CompanyRegistrationDocs]
	SET    [RegistrationType] = @RegistrationType, [RegistrationDocType] = @RegistrationDocType, [IsMandatory] = @IsMandatory
	WHERE  [RegistrationType] = @RegistrationType
	       AND [RegistrationDocType] = @RegistrationDocType
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CompanyRegistrationDocs] Record Into [CompanyRegistrationDocs] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsSave] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsSave] 
    @RegistrationType smallint,
    @RegistrationDocType smallint,
    @IsMandatory bit
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CompanyRegistrationDocs] 
		WHERE 	[RegistrationType] = @RegistrationType
	       AND [RegistrationDocType] = @RegistrationDocType)>0
	BEGIN
	    Exec [Master].[usp_CompanyRegistrationDocsUpdate] 
		@RegistrationType, @RegistrationDocType, @IsMandatory


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CompanyRegistrationDocsInsert] 
		@RegistrationType, @RegistrationDocType, @IsMandatory


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CompanyRegistrationDocsSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyRegistrationDocsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CompanyRegistrationDocs] Record  based on [CompanyRegistrationDocs]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CompanyRegistrationDocsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyRegistrationDocsDelete] 
END 
GO
CREATE PROC [Master].[usp_CompanyRegistrationDocsDelete] 
    @RegistrationType smallint,
    @RegistrationDocType smallint
AS 

	
BEGIN

	DELETE
	FROM   [Master].[CompanyRegistrationDocs]
	WHERE  [RegistrationType] = @RegistrationType
	       AND [RegistrationDocType] = @RegistrationDocType
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyRegistrationDocsDelete]
-- ========================================================================================================================================

GO
 