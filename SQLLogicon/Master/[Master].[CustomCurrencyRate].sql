


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CustomCurrencyRate] Record based on [CustomCurrencyRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomCurrencyRateSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateSelect] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateSelect] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
    @StartDate datetime,
    @EndDate datetime
AS 
 

BEGIN

	SELECT [CountryCode], [CurrencyCode], [StartDate], [EndDate], [ImportRate], [ExportRate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[CustomCurrencyRate]
	WHERE  [CountryCode] = @CountryCode   
	       AND [CurrencyCode] = @CurrencyCode  
	       AND [StartDate] = @StartDate   
	       AND [EndDate] = @EndDate  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CustomCurrencyRate] Records from [CustomCurrencyRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomCurrencyRateList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateList] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateList] 

AS 
 
BEGIN
	SELECT [CountryCode], [CurrencyCode], [StartDate], [EndDate], [ImportRate], [ExportRate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[CustomCurrencyRate]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CustomCurrencyRate] Record Into [CustomCurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomCurrencyRateInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateInsert] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateInsert] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
    @StartDate datetime,
    @EndDate datetime,
    @ImportRate decimal(18, 4) = NULL,
    @ExportRate decimal(18, 4) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
  

BEGIN
	
	INSERT INTO [Master].[CustomCurrencyRate] (
			[CountryCode], [CurrencyCode], [StartDate], [EndDate], [ImportRate], [ExportRate], [CreatedBy], [CreatedOn])
	SELECT	@CountryCode, @CurrencyCode, @StartDate, @EndDate, @ImportRate, @ExportRate, @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CustomCurrencyRate] Record Into [CustomCurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomCurrencyRateUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateUpdate] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateUpdate] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
    @StartDate datetime,
    @EndDate datetime,
    @ImportRate decimal(18, 4) = NULL,
    @ExportRate decimal(18, 4) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 
	
BEGIN

	UPDATE	[Master].[CustomCurrencyRate]
	SET		[CountryCode] = @CountryCode, [CurrencyCode] = @CurrencyCode, [StartDate] = @StartDate, [EndDate] = @EndDate, [ImportRate] = @ImportRate, 
			[ExportRate] = @ExportRate, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[CountryCode] = @CountryCode
			AND [CurrencyCode] = @CurrencyCode
			AND [StartDate] = @StartDate
			AND [EndDate] = @EndDate
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CustomCurrencyRate] Record Into [CustomCurrencyRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomCurrencyRateSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateSave] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateSave] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
    @StartDate datetime,
    @EndDate datetime,
    @ImportRate decimal(18, 4) = NULL,
    @ExportRate decimal(18, 4) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[CustomCurrencyRate] 
		WHERE 	[CountryCode] = @CountryCode
	       AND [CurrencyCode] = @CurrencyCode
	       AND [StartDate] = @StartDate
	       AND [EndDate] = @EndDate)>0
	BEGIN
	    Exec [Master].[usp_CustomCurrencyRateUpdate] 
		@CountryCode, @CurrencyCode, @StartDate, @EndDate, @ImportRate, @ExportRate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CustomCurrencyRateInsert] 
		@CountryCode, @CurrencyCode, @StartDate, @EndDate, @ImportRate, @ExportRate, @CreatedBy, @ModifiedBy 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CustomCurrencyRateSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomCurrencyRateDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CustomCurrencyRate] Record  based on [CustomCurrencyRate]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomCurrencyRateDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomCurrencyRateDelete] 
END 
GO
CREATE PROC [Master].[usp_CustomCurrencyRateDelete] 
    @CountryCode varchar(2),
    @CurrencyCode nvarchar(3),
    @StartDate datetime,
    @EndDate datetime
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[CustomCurrencyRate]
	WHERE  [CountryCode] = @CountryCode
	       AND [CurrencyCode] = @CurrencyCode
	       AND [StartDate] = @StartDate
	       AND [EndDate] = @EndDate
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomCurrencyRateDelete]
-- ========================================================================================================================================

GO 
