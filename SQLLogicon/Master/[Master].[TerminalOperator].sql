

-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TerminalOperator] Record based on [TerminalOperator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TerminalOperatorSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorSelect] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorSelect] 
    @Code NVARCHAR(20),
    @CountryCode VARCHAR(2)
AS 

BEGIN

	SELECT Opr.[Code], Opr.[CountryCode], Opr.[Description], Opr.[IsActive], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[TerminalOperator] Opr
	Left Outer Join [Master].[Country] C On
		Opr.CountryCode = C.CountryCode
	WHERE  Opr.[Code] = @Code  
	       AND Opr.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TerminalOperator] Records from [TerminalOperator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TerminalOperatorList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorList] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorList] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT Opr.[Code], Opr.[CountryCode], Opr.[Description], Opr.[IsActive], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[TerminalOperator] Opr
	Left Outer Join [Master].[Country] C On
		Opr.CountryCode = C.CountryCode
	Where Opr.CountryCode = @CountryCode
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TerminalOperator] Records from [TerminalOperator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TerminalOperatorPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorPageView] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorPageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT Opr.[Code], Opr.[CountryCode], Opr.[Description], Opr.[IsActive], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[TerminalOperator] Opr
	Left Outer Join [Master].[Country] C On
		Opr.CountryCode = C.CountryCode
	Where Opr.CountryCode = @CountryCode
	ORDER BY Opr.Description
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [TerminalOperator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TerminalOperatorRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorRecordCount] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorRecordCount] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[TerminalOperator]
	Where CountryCode = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TerminalOperator] Record based on [TerminalOperator] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_TerminalOperatorAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorAutoCompleteSearch] 
    @CountryCode VARCHAR(2),
	@Description NVARCHAR(100)
    
AS 

BEGIN

	SELECT Opr.[Code], Opr.[CountryCode], Opr.[Description], Opr.[IsActive], Opr.[CreatedBy], Opr.[CreatedOn], Opr.[ModifiedBy], Opr.[ModifiedOn],
	ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[TerminalOperator] Opr
	Left Outer Join [Master].[Country] C On
		Opr.CountryCode = C.CountryCode
	WHERE  Opr.[CountryCode] = @CountryCode 
	And Opr.Description Like '%' + @Description + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [TerminalOperator] Record Into [TerminalOperator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TerminalOperatorInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorInsert] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorInsert] 
    @Code nvarchar(20),
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[TerminalOperator] ([Code], [CountryCode], [Description], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @Code, @CountryCode, @Description, Cast(1 as bit), @CreatedBy, GetUTCDate()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [TerminalOperator] Record Into [TerminalOperator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TerminalOperatorUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorUpdate] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorUpdate] 
    @Code nvarchar(20),
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 
	
BEGIN

	UPDATE [Master].[TerminalOperator]
	SET    [Description] = @Description, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = getutcdate()
	WHERE  [Code] = @Code
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [TerminalOperator] Record Into [TerminalOperator] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TerminalOperatorSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorSave] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorSave] 
    @Code nvarchar(20),
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[TerminalOperator] 
		WHERE 	[Code] = @Code
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_TerminalOperatorUpdate] 
		@Code, @CountryCode, @Description, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_TerminalOperatorInsert] 
		@Code, @CountryCode, @Description, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[TerminalOperatorSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_TerminalOperatorDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [TerminalOperator] Record  based on [TerminalOperator]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_TerminalOperatorDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_TerminalOperatorDelete] 
END 
GO
CREATE PROC [Master].[usp_TerminalOperatorDelete] 
    @Code nvarchar(20),
    @CountryCode varchar(2),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[TerminalOperator]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy= @ModifiedBy, ModifiedOn = getutcDate()
	WHERE 	[Code] = @Code
	       AND [CountryCode] = @CountryCode

	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_TerminalOperatorDelete]
-- ========================================================================================================================================

GO

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

