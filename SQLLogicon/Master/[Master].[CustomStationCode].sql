


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomStationCode] Record based on [CustomStationCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationCodeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeSelect] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeSelect] 
    @Code NVARCHAR(10),
    @CountryCode VARCHAR(2)
AS 

BEGIN

	SELECT Cs.[Code], Cs.[CountryCode], Cs.[StationDescription], Cs.[IsActive], Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
		   ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CustomStationCode] Cs
	Left Outer Join [Master].[Country] C On 
		Cs.CountryCode = C.CountryCode
	WHERE  Cs.[Code] = @Code  
	       AND Cs.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomStationCode] Records from [CustomStationCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationCodeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeList] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeList] 
    @CountryCode VARCHAR(2)

AS 
BEGIN

	SELECT Cs.[Code], Cs.[CountryCode], Cs.[StationDescription], Cs.[IsActive], Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
		   ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CustomStationCode] Cs
	Left Outer Join [Master].[Country] C On 
		Cs.CountryCode = C.CountryCode
	WHERE  Cs.[CountryCode] = @CountryCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomStationCode] Records from [CustomStationCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationCodePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodePageView] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodePageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT Cs.[Code], Cs.[CountryCode], Cs.[StationDescription], Cs.[IsActive], Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
		   ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CustomStationCode] Cs
	Left Outer Join [Master].[Country] C On 
		Cs.CountryCode = C.CountryCode
	WHERE  Cs.[CountryCode] = @CountryCode  
	ORDER BY Cs.StationDescription
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CustomStationCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationCodeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeRecordCount] 
	@CountryCode varchar(2)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[CustomStationCode]
	Where CountryCode = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomStationCode] Record based on [CustomStationCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationCodeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeAutoCompleteSearch] 
    @StationDescription NVARCHAR(100),
    @CountryCode VARCHAR(2)
AS 

BEGIN

	SELECT Cs.[Code], Cs.[CountryCode], Cs.[StationDescription], Cs.[IsActive], Cs.[CreatedBy], Cs.[CreatedOn], Cs.[ModifiedBy], Cs.[ModifiedOn],
		   ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[CustomStationCode] Cs
	Left Outer Join [Master].[Country] C On 
		Cs.CountryCode = C.CountryCode
	WHERE Cs.[CountryCode] = @CountryCode  
		  And Cs.StationDescription like '%' + @StationDescription + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [CustomStationCode] Record Into [CustomStationCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationCodeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeInsert] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeInsert] 
    @Code nvarchar(10),
    @CountryCode varchar(2),
    @StationDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[CustomStationCode] ([Code], [CountryCode], [StationDescription], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @Code, @CountryCode, @StationDescription, Cast(1 as bit), @CreatedBy, GETUTCDATE() 
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [CustomStationCode] Record Into [CustomStationCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationCodeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeUpdate] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeUpdate] 
    @Code nvarchar(10),
    @CountryCode varchar(2),
    @StationDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[CustomStationCode]
	SET    [StationDescription] = @StationDescription, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [Code] = @Code
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [CustomStationCode] Record Into [CustomStationCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationCodeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeSave] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeSave] 
    @Code nvarchar(10),
    @CountryCode varchar(2),
    @StationDescription nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[CustomStationCode] 
		WHERE 	[Code] = @Code
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_CustomStationCodeUpdate] 
		@Code, @CountryCode, @StationDescription, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CustomStationCodeInsert] 
		@Code, @CountryCode, @StationDescription, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CustomStationCodeSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationCodeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [CustomStationCode] Record  based on [CustomStationCode]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationCodeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationCodeDelete] 
END 
GO
CREATE PROC [Master].[usp_CustomStationCodeDelete] 
    @Code nvarchar(10),
    @CountryCode varchar(2),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[CustomStationCode]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy , ModifiedOn = GETUTCDATE()
	WHERE 	[Code] = @Code
	       AND [CountryCode] = @CountryCode

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[CustomStationCode]
	WHERE  [Code] = @Code
	       AND [CountryCode] = @CountryCode
	*/


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationCodeDelete]
-- ========================================================================================================================================

GO

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

