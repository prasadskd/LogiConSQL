
-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySubscriptionBillingTariff]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Company] Records from [Company] table

/*
Exec [Master].[usp_CompanySubscriptionBillingTariff] 1009
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySubscriptionBillingTariff]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySubscriptionBillingTariff] 
END 
GO
CREATE PROC [Master].[usp_CompanySubscriptionBillingTariff]
    @CompanyCode INT 
AS 

BEGIN

--Declare @CompanyCode int;
--Select @CompanyCode = 1009

Declare @UsersCount smallint=0,
		@AffilliationTariffNo nvarchar(50),
		@UserTariffMode smallint=0,
		@ModuleTariffMode smallint=0,
		@StandardQuotationNo nvarchar(50) ='',
		@TariffQuotationNo nvarchar(50)=''

Select TOP(1) @StandardQuotationNo = QuotationNo 
From Master.StandardTariffView
Order By EffectiveDate Desc

Select @UserTariffMode = LookupID From Config.Lookup Where LookupCategory ='TariffDiscountType' And  LookupDescription ='Users'
Select @ModuleTariffMode = LookupID From Config.Lookup Where LookupCategory ='TariffDiscountType' And  LookupDescription ='Modules'
 
 

Select @UsersCount = ISNULL(Count(0),0) 
From [Security].[User] Where CompanyCode = @CompanyCode

Declare @tblAssociationTariff table
	(QuotationNo nvarchar(50), Percentage decimal(18,2))

Declare @tblBillingModules Table
	(Module smallint, Description nvarchar(50), Price decimal(18,2),CurrentQuotationNo nvarchar(50))
 

;With CompanyAssociations As (Select Hd.AssociationID From [Master].[AssociationHeader] Hd
Inner Join [Master].[AssociationDetail] Dt ON 
Hd.AssociationID = Dt.AssociationID
Where 
Dt.MerchantCode = @CompanyCode)
Insert Into @tblAssociationTariff
Select TOP(1) ISNULL(QuotationNo,'') As Quotation,Max(Percentage) As Percentage
From Master.TariffView Tv
Left Outer Join CompanyAssociations Ca On 
Tv.Association = Ca.AssociationID
Where 
@UsersCount Between Tv.SlabFrom  And Tv.SlabTo
And Tv.TariffMode = @UserTariffMode
And Convert(char(10),Tv.EffectiveDate,120) <= Convert(Char(10),GetUTCDate(),120)
And Convert(Char(10),Tv.ExpiryDate,120) >= Convert(Char(10),GetUTCDate(),120)
Group By QuotationNo
Order By Percentage Desc 
   

Select  @TariffQuotationNo = QuotationNo From @tblAssociationTariff

--print @TariffQuotationNo

;with ModuleDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='RegistrationType')
Insert Into @tblBillingModules
Select  Std.Module,ISNULL(Dt.LookupDescription,''),Std.Percentage,@TariffQuotationNo
From Master.StandardTariffView Std 
Left Outer join ModuleDescription Dt ON 
Std.Module = Dt.LookupID
Where
Std.Module > 0
And Std.TariffMode = @ModuleTariffMode
And Std.QuotationNo = @StandardQuotationNo
  
 Update a 
 Set a.Price = 
	Case 
		When a.price > b.Percentage Then a.Price 
		Else b.Percentage 
	END
 From @tblBillingModules a
 Left Outer Join Master.Tariffview B ON 
 a.Module = b.module
 Where 
 b.quotationNo = @TariffQuotationNo

  

SELECT distinct	CS.[CompanyCode], CS.[ModuleID], Con.LookupDescription as ModuleDescription, CS.[IsBillable] as isSelected, CS.[ActiveDate], CS.[InActiveDate], 
		CS.[CreatedBy], CS.[CreatedOn], CS.[ModifedBy], CS.[ModifiedOn], Bl.Price as Rate,Bl.CurrentQuotationNo 
FROM	[Master].[CompanySubscriptionBillingStatus] CS
Left Outer Join @tblBillingModules Bl ON 
		Cs.ModuleID = Bl.Module
Left Outer Join Config.Lookup Con On
		CS.ModuleID = Con.LookupID
WHERE  [CompanyCode] = @CompanyCode  
	         
END
-- ========================================================================================================================================
-- END  											  [Master].[usp_CompanySubscriptionBillingTariff]
-- ========================================================================================================================================
GO

