
-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [QuotationDetail] Record based on [QuotationDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailSelect] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailSelect] 
    @BranchID SMALLINT,
    @QuotationNo VARCHAR(50),
    @ItemId INT
AS 

SET NOCOUNT ON; 

BEGIN
	;With  
	JobTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='JobType'),
	ShipmentTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='ShipmentType'),
	ServiceTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='ServiceType'),
	TransportTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='TransportType'),
	TrailerTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='TrailerType'),
	CargoTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='CargoType'),
	SpecialHandlingTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='SpecialHandlingType'),
	CargoHandlingTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='CargoHandlingType'),
	BillingUnitTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='BillingUnitType'),
	PaymentTermLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='PaymentTerm'),
	PaymentToLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='PaymentTo')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemId], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[ServiceType], 
			Dt.[IncoTerms], Dt.[TransportType], Dt.[TrailerType], Dt.[SpecialHandling], Dt.[CargoHandling],Dt.[CargoType], Dt.[Size], Dt.[Type], 
			Dt.[PickupCode], Dt.[DeliveryCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[SellRate], Dt.[EstCost], Dt.[MinQty], Dt.[MinAmt], Dt.[MaxAmnt], 
			Dt.PaymentTerm, Dt.BilledTo, Dt.[IsSlabRate], Dt.[IsFlatRate], Dt.[IsRoundTrip], Dt.[SlabFrom], Dt.[SlabTo], Dt.[Commodity], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jobt.LookupDescription,'') As JobTypeDescription,
			ISNULL(Shipt.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Svrt.LookupDescription,'') As ServiceTypeDescription,
			ISNULL(Trt.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Trat.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Splt.LookupDescription,'') As SpecialHandlingTypeDescription,
			ISNULL(Carht.LookupDescription,'') As CargoHandlingDescription,
			ISNULL(Cargot.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Billt.LookupDescription,'') As BillingUnitDescription,
			ISNULL(PTm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(BTo.LookupDescription,'') As BilledToDescription,
			Chg.ChargeDescription
	FROM   [Master].[QuotationDetail] Dt
	Left Outer Join JobTypeLookup Jobt ON 
		Jobt.LookupID = Dt.JobType
	Left Outer Join ShipmentTypeLookup Shipt ON 
		Shipt.LookupID = Dt.ShipmentType
	Left Outer Join ServiceTypeLookup Svrt ON 
		Svrt.LookupID = Dt.ServiceType
	Left Outer Join TransportTypeLookup Trt ON 
		Trt.LookupID = Dt.TransportType
	Left Outer Join TrailerTypeLookup Trat ON 
		Trat.LookupID = Dt.TrailerType
	Left Outer Join CargoTypeLookup Cargot ON 
		Cargot.LookupID = Dt.CargoType
	Left Outer Join SpecialHandlingTypeLookup Splt ON 
		Splt.LookupID = Dt.SpecialHandling
	Left Outer Join CargoHandlingTypeLookup Carht ON 
		Carht.LookupID = Dt.CargoHandling
	Left Outer Join BillingUnitTypeLookup Billt ON 
		Billt.LookupID = Dt.BillingUnit
	Left Outer Join PaymentTermLookup PTm ON 
		PTm.LookupID = Dt.PaymentTerm
	Left Outer Join PaymentToLookup BTo ON 
		BTo.LookupID = Dt.BilledTo
	Left Outer Join Master.ChargeMaster Chg ON 
		Dt.[ChargeCode] = Chg.ChargeCode
	WHERE  Dt.[BranchID] = @BranchID  
			AND Dt.[QuotationNo] = @QuotationNo  
			AND [ItemId] = @ItemId  

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [QuotationDetail] Records from [QuotationDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_QuotationDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailList] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailList]
    @BranchID BIGINT,
    @QuotationNo VARCHAR(50)
AS 
BEGIN

SET NOCOUNT ON;

	;With  
	JobTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='JobType'),
	ShipmentTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='ShipmentType'),
	ServiceTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='ServiceType'),
	TransportTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='TransportType'),
	TrailerTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='TrailerType'),
	CargoTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='CargoType'),
	SpecialHandlingTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='SpecialHandlingType'),
	CargoHandlingTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='CargoHandlingType'),
	BillingUnitTypeLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='BillingUnitType'),
	PaymentTermLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='PaymentTerm'),
	PaymentToLookup As (select LookupID,LookupDescription from config.Lookup Where LookupCategory='PaymentTo')
	SELECT	Dt.[BranchID], Dt.[QuotationNo], Dt.[ItemId], Dt.[JobCategory], Dt.[JobType], Dt.[ShipmentType], Dt.[ChargeCode], Dt.[ServiceType], 
			Dt.[IncoTerms], Dt.[TransportType], Dt.[TrailerType], Dt.[SpecialHandling], Dt.[CargoHandling],Dt.[CargoType], Dt.[Size], Dt.[Type], 
			Dt.[PickupCode], Dt.[DeliveryCode], Dt.[BillingUnit], Dt.[CurrencyCode], Dt.[SellRate], Dt.[EstCost], Dt.[MinQty], Dt.[MinAmt], Dt.[MaxAmnt], 
			Dt.PaymentTerm, Dt.BilledTo, Dt.[IsSlabRate], Dt.[IsFlatRate], Dt.[IsRoundTrip], Dt.[SlabFrom], Dt.[SlabTo], Dt.[Commodity], 
			Dt.[Remarks], Dt.[CreatedBy], Dt.[CreatedOn], Dt.[ModifiedBy], Dt.[ModifiedOn],
			ISNULL(Jobt.LookupDescription,'') As JobTypeDescription,
			ISNULL(Shipt.LookupDescription,'') As ShipmentTypeDescription,
			ISNULL(Svrt.LookupDescription,'') As ServiceTypeDescription,
			ISNULL(Trt.LookupDescription,'') As TransportTypeDescription,
			ISNULL(Trat.LookupDescription,'') As TrailerTypeDescription,
			ISNULL(Splt.LookupDescription,'') As SpecialHandlingTypeDescription,
			ISNULL(Carht.LookupDescription,'') As CargoHandlingDescription,
			ISNULL(Cargot.LookupDescription,'') As CargoTypeDescription,
			ISNULL(Billt.LookupDescription,'') As BillingUnitDescription,
			ISNULL(PTm.LookupDescription,'') As PaymentTermDescription,
			ISNULL(BTo.LookupDescription,'') As BilledToDescription,
			Chg.ChargeDescription
	FROM   [Master].[QuotationDetail] Dt
	Left Outer Join JobTypeLookup Jobt ON 
		Jobt.LookupID = Dt.JobType
	Left Outer Join ShipmentTypeLookup Shipt ON 
		Shipt.LookupID = Dt.ShipmentType
	Left Outer Join ServiceTypeLookup Svrt ON 
		Svrt.LookupID = Dt.ServiceType
	Left Outer Join TransportTypeLookup Trt ON 
		Trt.LookupID = Dt.TransportType
	Left Outer Join TrailerTypeLookup Trat ON 
		Trat.LookupID = Dt.TrailerType
	Left Outer Join CargoTypeLookup Cargot ON 
		Cargot.LookupID = Dt.CargoType
	Left Outer Join SpecialHandlingTypeLookup Splt ON 
		Splt.LookupID = Dt.SpecialHandling
	Left Outer Join CargoHandlingTypeLookup Carht ON 
		Carht.LookupID = Dt.CargoHandling
	Left Outer Join BillingUnitTypeLookup Billt ON 
		Billt.LookupID = Dt.BillingUnit
	Left Outer Join PaymentTermLookup PTm ON 
		PTm.LookupID = Dt.PaymentTerm
	Left Outer Join PaymentToLookup BTo ON 
		BTo.LookupID = Dt.BilledTo
	Left Outer Join Master.ChargeMaster Chg ON 
		Dt.[ChargeCode] = Chg.ChargeCode
	WHERE  Dt.[BranchID] = @BranchID  
	    AND Dt.[QuotationNo] = @QuotationNo  


SET NOCOUNT OFF;

END


-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [QuotationDetail] Record Into [QuotationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailInsert] 
END 
GO

CREATE PROC [Master].[usp_QuotationDetailInsert] 
    @BranchID bigint,
    @QuotationNo varchar(50),
    @ItemId int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @ServiceType smallint,
    @IncoTerms nvarchar(10),
    @TransportType smallint,
    @TrailerType smallint,
    @SpecialHandling smallint,
    @CargoHandling smallint,
	@CargoType smallint,
    @Size varchar(10),
    @Type varchar(10),
    @PickupCode nvarchar(10),
    @DeliveryCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @SellRate numeric(18, 2),
    @EstCost numeric(18, 2),
    @MinQty numeric(18, 2),
    @MinAmt numeric(18, 2),
    @MaxAmnt numeric(18, 2),
    @PaymentTerm smallint,
    @BilledTo smallint,
    @IsSlabRate bit,
    @IsFlatRate bit,
    @IsRoundTrip bit,
    @SlabFrom int,
    @SlabTo int,
    @Commodity nvarchar(10),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN
 
	
	INSERT INTO [Master].[QuotationDetail] (
			[BranchID], [QuotationNo], [ItemId], [JobCategory], [JobType], [ShipmentType], [ChargeCode], [ServiceType], [IncoTerms], [TransportType], 
			[TrailerType], [SpecialHandling],[CargoHandling], [CargoType],  [Size], [Type], [PickupCode], [DeliveryCode], [BillingUnit], [CurrencyCode], [SellRate], 
			[EstCost], [MinQty], [MinAmt], [MaxAmnt], PaymentTerm, BilledTo, [IsSlabRate], [IsFlatRate], [IsRoundTrip], [SlabFrom], [SlabTo], 
			[Commodity], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @QuotationNo, @ItemId, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @ServiceType, @IncoTerms, @TransportType, 
			@TrailerType, @SpecialHandling, @CargoHandling,@CargoType, @Size, @Type, @PickupCode, @DeliveryCode, @BillingUnit, @CurrencyCode, @SellRate, 
			@EstCost, @MinQty, @MinAmt, @MaxAmnt, @PaymentTerm, @BilledTo, @IsSlabRate, @IsFlatRate, @IsRoundTrip, @SlabFrom, @SlabTo, 
			@Commodity, @Remarks, @CreatedBy, GETUTCDATE()
 	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [QuotationDetail] Record Into [QuotationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailUpdate] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailUpdate] 
    @BranchID smallint,
    @QuotationNo varchar(50),
    @ItemId int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @ServiceType smallint,
    @IncoTerms nvarchar(10),
    @TransportType smallint,
    @TrailerType smallint,
    @SpecialHandling smallint,
    @CargoHandling smallint,
	@CargoType smallint,
    @Size varchar(10),
    @Type varchar(10),
    @PickupCode nvarchar(10),
    @DeliveryCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @SellRate numeric(18, 2),
    @EstCost numeric(18, 2),
    @MinQty numeric(18, 2),
    @MinAmt numeric(18, 2),
    @MaxAmnt numeric(18, 2),
    @PaymentTerm smallint,
    @BilledTo smallint,
    @IsSlabRate bit,
    @IsFlatRate bit,
    @IsRoundTrip bit,
    @SlabFrom int,
    @SlabTo int,
    @Commodity nvarchar(10),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

 

	UPDATE	[Master].[QuotationDetail]
	SET		[JobCategory] = @JobCategory, [JobType] = @JobType, [ShipmentType] = @ShipmentType, [ChargeCode] = @ChargeCode, [ServiceType] = @ServiceType, 
			[IncoTerms] = @IncoTerms, [TransportType] = @TransportType, [TrailerType] = @TrailerType, [SpecialHandling] = @SpecialHandling, 
			[CargoHandling] = @CargoHandling, CargoType=@CargoType, [Size] = @Size, [Type] = @Type, [PickupCode] = @PickupCode, [DeliveryCode] = @DeliveryCode, 
			[BillingUnit] = @BillingUnit, [CurrencyCode] = @CurrencyCode, [SellRate] = @SellRate, [EstCost] = @EstCost, [MinQty] = @MinQty, 
			[MinAmt] = @MinAmt, [MaxAmnt] = @MaxAmnt, PaymentTerm = @PaymentTerm, BilledTo = @BilledTo, [IsSlabRate] = @IsSlabRate, 
			[IsFlatRate] = @IsFlatRate, [IsRoundTrip] = @IsRoundTrip, [SlabFrom] = @SlabFrom, [SlabTo] = @SlabTo, [Commodity] = @Commodity, 
			[Remarks] = @Remarks, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [QuotationNo] = @QuotationNo
			AND [ItemId] = @ItemId
 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [QuotationDetail] Record Into [QuotationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailSave] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailSave] 
    @BranchID smallint,
    @QuotationNo varchar(50),
    @ItemId int,
    @JobCategory nvarchar(15),
    @JobType smallint,
    @ShipmentType smallint,
    @ChargeCode nvarchar(20),
    @ServiceType smallint,
    @IncoTerms nvarchar(10),
    @TransportType smallint,
    @TrailerType smallint,
    @SpecialHandling smallint,
    @CargoHandling smallint,
	@CargoType smallint,
    @Size varchar(10),
    @Type varchar(10),
    @PickupCode nvarchar(10),
    @DeliveryCode nvarchar(10),
    @BillingUnit smallint,
    @CurrencyCode varchar(3),
    @SellRate numeric(18, 2),
    @EstCost numeric(18, 2),
    @MinQty numeric(18, 2),
    @MinAmt numeric(18, 2),
    @MaxAmnt numeric(18, 2),
    @PaymentTerm smallint,
    @BilledTo smallint,
    @IsSlabRate bit,
    @IsFlatRate bit,
    @IsRoundTrip bit,
    @SlabFrom int,
    @SlabTo int,
    @Commodity nvarchar(10),
    @Remarks nvarchar(100),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
 
	IF (SELECT COUNT(0) FROM [Master].[QuotationDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	       AND [ItemId] = @ItemId)>0
	BEGIN
	    Exec [Master].[usp_QuotationDetailUpdate] 
			@BranchID, @QuotationNo, @ItemId, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @ServiceType, @IncoTerms, @TransportType, 
			@TrailerType, @SpecialHandling, @CargoHandling,@CargoType, @Size, @Type, @PickupCode, @DeliveryCode, @BillingUnit, @CurrencyCode, @SellRate, 
			@EstCost, @MinQty, @MinAmt, @MaxAmnt, @PaymentTerm, @BilledTo, @IsSlabRate, @IsFlatRate, @IsRoundTrip, @SlabFrom, @SlabTo, 
			@Commodity, @Remarks, @CreatedBy,@ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_QuotationDetailInsert] 
			@BranchID, @QuotationNo, @ItemId, @JobCategory, @JobType, @ShipmentType, @ChargeCode, @ServiceType, @IncoTerms, @TransportType, 
			@TrailerType, @SpecialHandling, @CargoHandling,@CargoType, @Size, @Type, @PickupCode, @DeliveryCode, @BillingUnit, @CurrencyCode, @SellRate, 
			@EstCost, @MinQty, @MinAmt, @MaxAmnt, @PaymentTerm, @BilledTo, @IsSlabRate, @IsFlatRate, @IsRoundTrip, @SlabFrom, @SlabTo, 
			@Commodity, @Remarks, @CreatedBy,@ModifiedBy 

	END
	
 
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[QuotationDetailSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_QuotationDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [QuotationDetail] Record  based on [QuotationDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_QuotationDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_QuotationDetailDelete] 
END 
GO
CREATE PROC [Master].[usp_QuotationDetailDelete] 
    @BranchID smallint,
    @QuotationNo varchar(50) 
AS 

	
BEGIN
SET NOCOUNT ON;
	 
	DELETE
	FROM   [Master].[QuotationDetail]
	WHERE  [BranchID] = @BranchID
	       AND [QuotationNo] = @QuotationNo
	       
	 

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_QuotationDetailDelete]
-- ========================================================================================================================================

GO
 
