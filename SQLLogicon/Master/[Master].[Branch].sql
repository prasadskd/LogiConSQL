 


-- ========================================================================================================================================
-- START											 [Master].[usp_BranchSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Branch] Record based on [Branch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_BranchSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchSelect] 
END 
GO
CREATE PROC [Master].[usp_BranchSelect] 
    @CompanyCode Int,
    @BranchID BigInt
AS 
 

BEGIN

	SELECT [BranchID],[CompanyCode], [BranchCode], [BranchName], [RegNo],[GSTNo],[TaxID], [IsActive], 
	AgentCode ,EDIMailBoxNo  ,	[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Branch]
	WHERE  [CompanyCode] = @CompanyCode  
	       AND [BranchID] = @BranchID  
		   And IsActive = CAST(1 as bit)
		
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_BranchList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Branch] Records from [Branch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_BranchList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchList] 
END 
GO
CREATE PROC [Master].[usp_BranchList] 
	@CompanyCode Int
AS 
 
BEGIN
	SELECT [BranchID],[CompanyCode], [BranchCode], [BranchName], [RegNo],[GSTNo],[TaxID], [IsActive],AgentCode ,EDIMailBoxNo  , [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Branch]
	WHERE  [CompanyCode] = @CompanyCode
	And IsActive = CAST(1 as bit)

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CompanyBranchList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Branch] Records from [Branch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanyBranchList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanyBranchList] 
END 
GO
CREATE PROC [Master].[usp_CompanyBranchList] 
	@CompanyCode Int
AS 
 
BEGIN
	SELECT [BranchID],[CompanyCode], [BranchCode], [BranchName], [RegNo],[GSTNo],[TaxID], [IsActive],AgentCode ,EDIMailBoxNo  , [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Branch]
	WHERE  [CompanyCode] = @CompanyCode 
	And IsActive = CAST(1 as bit)

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CompanyBranchList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_BranchInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Branch] Record Into [Branch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_BranchInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchInsert] 
END 
GO
CREATE PROC [Master].[usp_BranchInsert] 
	@BranchID bigint,
    @CompanyCode Int,
    @BranchCode varchar(3),
    @BranchName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@TaxID nvarchar(50),
	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewBranchID bigint OUTPUT
AS 
  

BEGIN
	
	INSERT INTO [Master].[Branch] (BranchID,[CompanyCode], [BranchCode], [BranchName], [RegNo],[GSTNo],TaxID,AgentCode,EDIMailBoxNo, [IsActive], [CreatedBy], [CreatedOn])
	SELECT @BranchID,@CompanyCode, @BranchCode, @BranchName, @RegNo,@GSTNo,@TaxID,@AgentCode,@EDIMailBoxNo, 1, @CreatedBy, GETUTCDATE()
	

	Select @NewBranchID = @BranchID
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_BranchUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Branch] Record Into [Branch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_BranchUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchUpdate] 
END 
GO
CREATE PROC [Master].[usp_BranchUpdate] 
	@BranchID bigint,
    @CompanyCode Int,
    @BranchCode varchar(3),
    @BranchName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@TaxID nvarchar(50),
	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewBranchID bigint OUTPUT
AS 
 
	
BEGIN

	UPDATE [Master].[Branch]
	SET    [CompanyCode] = @CompanyCode, [BranchCode] = @BranchCode, [BranchName] = @BranchName, [RegNo] = @RegNo, GSTNo=@GSTNo,TaxID=@TaxID, [IsActive] = 1, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),AgentCode=@AgentCode,EDIMailBoxNo=@EDIMailBoxNo
	WHERE  BranchID = @BranchID
		   AND [CompanyCode] = @CompanyCode
	        
	

	Select @NewBranchID = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_BranchSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Branch] Record Into [Branch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_BranchSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchSave] 
END 
GO
CREATE PROC [Master].[usp_BranchSave] 
	@BranchID bigint,
    @CompanyCode Int,
    @BranchCode varchar(3),
    @BranchName nvarchar(100),
    @RegNo nvarchar(50),
	@GSTNo nvarchar(50),
	@TaxID nvarchar(50),
	@AgentCode nvarchar(10),
	@EDIMailBoxNo nvarchar(15),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@NewBranchID bigint OUTPUT
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[Branch] 
		WHERE 	BranchID = @BranchID And CompanyCode = @CompanyCode )>0
	BEGIN
	    Exec [Master].[usp_BranchUpdate] 
		@BranchID,@CompanyCode, @BranchCode, @BranchName, @RegNo,@GSTNo,@TaxID,@AgentCode,@EDIMailBoxNo, @CreatedBy, @ModifiedBy ,@NewBranchID=@NewBranchID OUTPUT


	END
	ELSE
	BEGIN

		Declare @BranchCount smallint = 0,
				@NewID bigint =0;


		Select @BranchCount=Count(0) From Master.Branch Where CompanyCode = @CompanyCode

		Select @NewID = Convert(varchar(6),@CompanyCode) + [Utility].[udf_PADL](Convert(varchar(3),@BranchCount+1),3,'0')


	    Exec [Master].[usp_BranchInsert] 
		@NewID,@CompanyCode, @BranchCode, @BranchName, @RegNo,@GSTNo,@TaxID,@AgentCode,@EDIMailBoxNo, @CreatedBy, @ModifiedBy ,@NewBranchID=@NewBranchID OUTPUT


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[BranchSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_BranchDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Branch] Record  based on [Branch]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_BranchDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchDelete] 
END 
GO
CREATE PROC [Master].[usp_BranchDelete] 
    @BranchID bigint,
	@CompanyCode Int
AS 

	
BEGIN

	UPDATE	[Master].[Branch]
	SET	IsActive = CAST(0 as bit)
	WHERE  BranchID = @BranchID	
		   AND [CompanyCode] = @CompanyCode
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchDelete]
-- ========================================================================================================================================

GO
 
 

-- ========================================================================================================================================
-- START											 [Master].[usp_BranchUpdateAgentcodeAndMailBox]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	18-Apr-2017
-- Description:	updates the [Branch Record Into [Branch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_BranchUpdateAgentcodeAndMailBox]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_BranchUpdateAgentcodeAndMailBox] 
END 
GO
CREATE PROC [Master].[usp_BranchUpdateAgentcodeAndMailBox] 
	@BranchID bigint,
    @CompanyCode Int,
	@AgentCode nvarchar(20),
    @EDIMailBoxNo nvarchar(30)
AS 
 
	
BEGIN

	UPDATE	[Master].[Branch]
	SET		AgentCode = @AgentCode, EDIMailBoxNo = @EDIMailBoxNo
	WHERE  BranchID = @BranchID	
	AND [CompanyCode] = @CompanyCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_BranchUpdateAgentcodeAndMailBox] 
-- ========================================================================================================================================

GO
