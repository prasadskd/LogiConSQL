
-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [MerchantRelation] Record based on [MerchantRelation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantRelationSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationSelect] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationSelect] 
    @MerchantCode BIGINT,
    @RelatedMerchantCode BIGINT
AS 

BEGIN
	;With 
	RelationshipTypeLookup As (select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RelationshipType')
	SELECT MR.[MerchantCode], MR.[RelatedMerchantCode], MR.[RelationshipType], MR.[CreatedBy], MR.[CreatedOn], MR.[ModifiedBy], MR.[ModifiedOn] ,
	ISNULL(Rl.LookupDescription,'') As RelationshipTypeDescription,
	ISNULL(M.MerchantName,'') As MerchantName ,ISNULL(Mr1.MerchantName,'')  As RelatedMerchantName
	
	FROM   [Master].[MerchantRelation] MR
	Left Outer Join RelationshipTypeLookup Rl On 
		Mr.RelationshipType = rl.LookupID
	Left Outer Join [Master].[Merchant] M ON 
		Mr.MerchantCode = M.MerchantCode 
	Left Outer Join [Master].[Merchant] Mr1 ON 
		Mr.RelatedMerchantCode = M.MerchantCode 
	WHERE  Mr.[MerchantCode] = @MerchantCode  
	       AND Mr.[RelatedMerchantCode] = @RelatedMerchantCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [MerchantRelation] Records from [MerchantRelation] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantRelationList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationList] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationList] 
    @MerchantCode BIGINT
AS 
BEGIN

	;With 
	RelationshipTypeLookup As (select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RelationshipType')
	SELECT MR.[MerchantCode], MR.[RelatedMerchantCode], MR.[RelationshipType], MR.[CreatedBy], MR.[CreatedOn], MR.[ModifiedBy], MR.[ModifiedOn] ,
	ISNULL(Rl.LookupDescription,'') As RelationshipTypeDescription,
	ISNULL(M.MerchantName,'') As MerchantName ,ISNULL(Mr1.MerchantName,'')  As RelatedMerchantName
	FROM   [Master].[MerchantRelation] MR
	Left Outer Join RelationshipTypeLookup Rl On 
		Mr.RelationshipType = rl.LookupID
	Left Outer Join [Master].[Merchant] M ON 
		Mr.MerchantCode = M.MerchantCode 
	Left Outer Join [Master].[Merchant] Mr1 ON 
		Mr.RelatedMerchantCode = M.MerchantCode 
	WHERE  MR.[MerchantCode] = @MerchantCode
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [MerchantRelation] Record Into [MerchantRelation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantRelationInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationInsert] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationInsert] 
    @MerchantCode BIGINT,
    @RelatedMerchantCode BIGINT,
    @RelationshipType smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[MerchantRelation] (
			[MerchantCode], [RelatedMerchantCode], [RelationshipType], [CreatedBy], [CreatedOn])
	SELECT	@MerchantCode, @RelatedMerchantCode, @RelationshipType, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [MerchantRelation] Record Into [MerchantRelation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantRelationUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationUpdate] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationUpdate] 
    @MerchantCode BIGINT,
    @RelatedMerchantCode BIGINT,
    @RelationshipType smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[MerchantRelation]
	SET    [RelationshipType] = @RelationshipType, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [MerchantCode] = @MerchantCode
	       AND [RelatedMerchantCode] = @RelatedMerchantCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [MerchantRelation] Record Into [MerchantRelation] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantRelationSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationSave] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationSave] 
    @MerchantCode BIGINT,
    @RelatedMerchantCode BIGINT,
    @RelationshipType smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[MerchantRelation] 
		WHERE 	[MerchantCode] = @MerchantCode
	       AND [RelatedMerchantCode] = @RelatedMerchantCode)>0
	BEGIN
	    Exec [Master].[usp_MerchantRelationUpdate] 
		@MerchantCode, @RelatedMerchantCode, @RelationshipType, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_MerchantRelationInsert] 
		@MerchantCode, @RelatedMerchantCode, @RelationshipType, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[MerchantRelationSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [MerchantRelation] Record  based on [MerchantRelation]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantRelationDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationDelete] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationDelete] 
    @MerchantCode BIGINT,
    @RelatedMerchantCode BIGINT
AS 

	
BEGIN


	DELETE
	FROM   [Master].[MerchantRelation]
	WHERE  [MerchantCode] = @MerchantCode
	       AND [RelatedMerchantCode] = @RelatedMerchantCode
 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationDelete]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantRelationDeleteAll]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [MerchantRelation] Record  based on [MerchantRelation]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_MerchantRelationDeleteAll]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantRelationDeleteAll] 
END 
GO
CREATE PROC [Master].[usp_MerchantRelationDeleteAll] 
    @MerchantCode BIGINT
AS 

	
BEGIN


	DELETE
	FROM   [Master].[MerchantRelation]
	WHERE  [MerchantCode] = @MerchantCode
	        
 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantRelationDeleteAll]
-- ========================================================================================================================================

GO

 
 
