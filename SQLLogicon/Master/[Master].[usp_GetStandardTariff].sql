-- ========================================================================================================================================
-- START											[Master].[usp_GetStandardTariff]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	16-Apr-2017
-- Description:	Get The List of Company Affiliations

/*
Exec [Master].[usp_GetStandardTariff]   
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_GetStandardTariff]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GetStandardTariff] 
END 
GO
CREATE PROC [Master].[usp_GetStandardTariff]
   
AS 

BEGIN

 SELECT  QuotationNo, Association, EffectiveDate, ExpiryDate, IsActive, CreatedBy, 
		 CreatedOn, ModifiedBy, ModifiedOn, QuotationType, Module, TariffMode, 
		 Transactions, SellingPrice, SlabFrom, SlabTo, Percentage
 From [Master].[StandardTariffView]
 Where GETUTCDATE() Between EffectiveDate And ExpiryDate

END
 