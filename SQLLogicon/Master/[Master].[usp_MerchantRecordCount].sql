-- Exec [Master].[usp_MerchantRecordCount] 'all',1006

alter Procedure [Master].[usp_MerchantRecordCount] (
	@merchantType varchar(100), 
	@CompanyCode bigint)
As
Begin

 Declare @Sql varchar(max);
 Declare @a bigint = 0 
 Declare @b bigint =0;
 If @merchantType='all'
 Begin
	SELECT @a  = COUNT(0)
	From Master.Merchant  
	Where CompanyCode =  @CompanyCode  
	And IsActive = Cast(1 as bit)  
	 
	SELECT @b = COUNT(0)
	From Master.Merchant  
	Where CompanyCode = 1000 
	And IsActive = Cast(1 as bit) 
	And CompanyCode NOT IN (@CompanyCode) 
 End
 Else
 Begin
 
	SELECT  @a  =  COUNT(0)
	From Master.Merchant  
	Where CompanyCode =  @CompanyCode       
	And IsActive = Cast(1 as bit)  
	And IsShipper = Case  When   @merchantType    = 'shipper' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsBillingCustomer = Case  When   @merchantType     = 'billingCustomer' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsConsignee = Case  When   @merchantType    = 'consignee' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsTerminal = Case  When   @merchantType    ='terminal'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsForwarder = Case  When   @merchantType    = 'freightForwarder' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsTransporter = Case  When   @merchantType   ='transporter'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsVendor =  Case  When   @merchantType   ='vendor'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsYard = Case  When   @merchantType    = 'yard' Then cast(1 as bit) Else Cast(0 as bit) END  


	SELECT @b = COUNT(0)   
	From Master.Merchant  
	Where CompanyCode = 1000  
	And IsActive = Cast(1 as bit)  
	And IsShipper = Case  When   @merchantType    = 'shipper' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsBillingCustomer = Case  When   @merchantType     = 'billingCustomer' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsConsignee = Case  When   @merchantType    = 'consignee' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsTerminal = Case  When   @merchantType    ='terminal'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsForwarder = Case  When   @merchantType    = 'freightForwarder' Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsTransporter = Case  When   @merchantType   ='transporter'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsVendor =  Case  When   @merchantType   ='vendor'  Then cast(1 as bit) Else Cast(0 as bit) END  
	And IsYard = Case  When   @merchantType    = 'yard' Then cast(1 as bit) Else Cast(0 as bit) END  
	And CompanyCode NOT IN (@CompanyCode)  

 End	 
  
	Select ISNULL(@a,0) + ISNULL(@b,0)

End

 