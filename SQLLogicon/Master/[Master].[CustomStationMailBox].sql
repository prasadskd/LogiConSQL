

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [CustomStationMailBox] Record based on [CustomStationMailBox] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationMailBoxSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxSelect] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxSelect] 
    @CustomStationCode nvarchar(10),
    @CountryCode varchar(2),
    @DeclarationType bigint,
    @MailBoxID nvarchar(35)
AS 
 

BEGIN

	SELECT [CustomStationCode], [CountryCode], [DeclarationType], [MailBoxID] ,[Line1],[Line2],[Line3],[IsTradingPartner],[TradingPartner]
		FROM   [Master].[CustomStationMailBox]
	WHERE  [CustomStationCode] = @CustomStationCode  
	       AND [CountryCode] = @CountryCode  
	       AND [DeclarationType] = @DeclarationType  
	       AND [MailBoxID] = ISNULL(@MailBoxID,MailBoxID)

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [CustomStationMailBox] Records from [CustomStationMailBox] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationMailBoxList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxList] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxList] 
    @CountryCode varchar(2)

AS 
 
BEGIN
	SELECT [CustomStationCode], [CountryCode], [DeclarationType], [MailBoxID] ,[Line1],[Line2],[Line3],[IsTradingPartner],[TradingPartner] 
	FROM   [Master].[CustomStationMailBox]
	WHERE  [CountryCode] = @CountryCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [CustomStationMailBox] PageView Records from [CustomStationMailBox] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationMailBoxPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxPageView]
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxPageView] 
	@CountryCode varchar(2),
	@fetchrows bigint
AS 
BEGIN

	SELECT [CustomStationCode], [CountryCode], [DeclarationType], [MailBoxID] ,[Line1],[Line2],[Line3],[IsTradingPartner],[TradingPartner] 
	FROM   [Master].[CustomStationMailBox]
	WHERE  [CountryCode] = @CountryCode  
	ORDER BY  [CountryCode] 
	       
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [CustomStationMailBox] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationMailBoxRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxRecordCount] 
	@CountryCode varchar(2)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Master].[CustomStationMailBox]
		WHERE  [CountryCode] = @CountryCode  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [CustomStationMailBox] auto-complete search based on [CustomStationMailBox] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CustomStationMailBoxAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxAutoCompleteSearch]
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxAutoCompleteSearch]
    @CustomStationCode nvarchar(10)
      
AS 

BEGIN
	SELECT [CustomStationCode], [CountryCode], [DeclarationType], [MailBoxID] ,[Line1],[Line2],[Line3],[IsTradingPartner],[TradingPartner]
	FROM   [Master].[CustomStationMailBox]
	WHERE  [CustomStationCode]  LIKE '%' + @CustomStationCode   + '%'
END
-- ========================================================================================================================================
-- END  											[Master].[usp_CustomStationMailBoxAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [CustomStationMailBox] Record Into [CustomStationMailBox] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationMailBoxInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxInsert] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxInsert] 
    @CustomStationCode nvarchar(10),
    @CountryCode varchar(2),
    @DeclarationType bigint,
    @MailBoxID nvarchar(35),
	@Line1 nvarchar(100),
	@Line2 nvarchar(100),
	@Line3 nvarchar(100),
	@IsTradingPartner bit,
	@TradingPartner nvarchar(100)

AS 
  

BEGIN
	
	INSERT INTO [Master].[CustomStationMailBox] ([CustomStationCode], [CountryCode], [DeclarationType], [MailBoxID] ,[Line1],[Line2],[Line3],[IsTradingPartner],[TradingPartner])
	SELECT @CustomStationCode, @CountryCode, @DeclarationType, @MailBoxID,@Line1,@Line2,@Line3,@IsTradingPartner,@TradingPartner
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [CustomStationMailBox] Record Into [CustomStationMailBox] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationMailBoxUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxUpdate] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxUpdate] 
    @CustomStationCode nvarchar(10),
    @CountryCode varchar(2),
    @DeclarationType bigint,
    @MailBoxID nvarchar(35),
	@Line1 nvarchar(100),
	@Line2 nvarchar(100),
	@Line3 nvarchar(100),
	@IsTradingPartner bit,
	@TradingPartner nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [Master].[CustomStationMailBox]
	SET    [CustomStationCode] = @CustomStationCode, [CountryCode] = @CountryCode, [DeclarationType] = @DeclarationType, [MailBoxID] = @MailBoxID,
	[Line1]= @Line1,[Line2]= @Line2,[Line3]=@Line3,[IsTradingPartner]=@IsTradingPartner,[TradingPartner]=@TradingPartner
	WHERE  [CustomStationCode] = @CustomStationCode
	       AND [CountryCode] = @CountryCode
	       AND [DeclarationType] = @DeclarationType
	        
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [CustomStationMailBox] Record Into [CustomStationMailBox] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationMailBoxSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxSave] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxSave] 
    @CustomStationCode nvarchar(10),
    @CountryCode varchar(2),
    @DeclarationType bigint,
    @MailBoxID nvarchar(35),
	@Line1 nvarchar(100),
	@Line2 nvarchar(100),
	@Line3 nvarchar(100),
	@IsTradingPartner bit,
	@TradingPartner nvarchar(100)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[CustomStationMailBox] 
		WHERE 	[CustomStationCode] = @CustomStationCode
	       AND [CountryCode] = @CountryCode
	       AND [DeclarationType] = @DeclarationType
	        )>0
	BEGIN
	    Exec [Master].[usp_CustomStationMailBoxUpdate] 
			@CustomStationCode, @CountryCode, @DeclarationType, @MailBoxID ,@Line1,@Line2,@Line3,@IsTradingPartner,@TradingPartner


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CustomStationMailBoxInsert] 
			@CustomStationCode, @CountryCode, @DeclarationType, @MailBoxID,@Line1,@Line2,@Line3,@IsTradingPartner,@TradingPartner


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CustomStationMailBoxSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CustomStationMailBoxDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [CustomStationMailBox] Record  based on [CustomStationMailBox]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CustomStationMailBoxDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CustomStationMailBoxDelete] 
END 
GO
CREATE PROC [Master].[usp_CustomStationMailBoxDelete] 
    @CustomStationCode nvarchar(10),
    @CountryCode varchar(2),
    @DeclarationType bigint 
AS 

	
BEGIN

	 
 
	DELETE
	FROM   [Master].[CustomStationMailBox]
	WHERE  [CustomStationCode] = @CustomStationCode
	       AND [CountryCode] = @CountryCode
	       AND [DeclarationType] = @DeclarationType
	      
	 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CustomStationMailBoxDelete]
-- ========================================================================================================================================

GO 