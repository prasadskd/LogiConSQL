-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesVASListByJobCategoryCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryChargesVAS] Record based on [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesVASListByJobCategoryCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesVASListByJobCategoryCode] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesVASListByJobCategoryCode] 
@jobCategoryCode varchar(30),
@branchId int
AS 

BEGIN



	;With PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	Jc.[BranchID], Jc.[ChargeCode], Jc.[JobCategoryCode], Jc.[PaymentTo], Jc.[PaymentTerm], Jc.[IsLoadAtGate], 
			Jc.[CreatedBy], Jc.[CreatedOn], Jc.[ModifiedBy], Jc.[ModifiedOn] ,
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			Jb.Description As JobCategoryDescription,
			Chg.ChargeDescription As ChargeCodeDescription
	FROM   [Master].[JobCategoryChargesVAS] JC 
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Left Outer Join Master.ChargeMaster Chg On 
		Jc.ChargeCode = Chg.ChargeCode
	Left Outer Join Master.JobCategory Jb On
		Jc.JobCategoryCode = Jb.Code
 Where  JC.[BranchID] = @branchId  and  JC.[JobCategoryCode] = @jobCategoryCode

 



END



-- ========================================================================================================================================
-- END								[Master].[usp_JobCategoryChargesVASListByJobCategoryCode] 
-- ========================================================================================================================================


 