

-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Process] Record based on [Process] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProcessSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessSelect] 
END 
GO
CREATE PROC [Master].[usp_ProcessSelect] 
    @Code NVARCHAR(50)
AS 
 

BEGIN
	;With ModuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	ProcessType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ProcessType'),
	TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
	FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='FreightMode'),
	EFIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='EFIndicator'),
	MovementIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='MovementIndicator'),
	PUDOMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='PUDOMode')
	SELECT	P.[Code], P.[Description], P.[Module], P.[ProcessType], P.[PickupFrom], P.[DeliveryTo], P.[EFIndicator],P.MovementIndicator, P.[TransportMode], P.[FreightMode], 
			P.[IsPlanningRequired], P.[IsStagingRequired], P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(M.LookupDescription,'') As ModuleTypeDescription,
			ISNULL(Pt.LookupDescription,'') As ProcessTypeDescription,
			ISNULL(Tm.LookupDescription,'') As TransportModeDescription,
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Pu.LookupDescription,'') As PUModeDescription,
			ISNULL(Do.LookupDescription,'') As DOModeDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription
	FROM	[Master].[Process] P
	Left Outer Join ModuleType M On 
		M.LookupID = P.Module
	Left Outer Join ProcessType Pt On 
		Pt.LookupID = P.ProcessType
	Left Outer Join TransportMode Tm On 
		Tm.LookupID = P.TransportMode
	Left Outer Join FreightMode Fm On 
		Fm.LookupID = P.FreightMode
	Left Outer Join EFIndicator Ef On 
		Ef.LookupID = P.EFIndicator
	Left Outer Join PUDOMode Pu On 
		Pu.LookupID = P.PickupFrom
	Left Outer Join PUDOMode Do On 
		Do.LookupID = P.DeliveryTo
	Left Outer Join MovementIndicator Mi ON
		Mi.LookupID = P.MovementIndicator
	WHERE	[Code] = @Code 

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Process] Records from [Process] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProcessList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessList] 
END 
GO
CREATE PROC [Master].[usp_ProcessList] 

AS 
 
BEGIN
	;With ModuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='Module'),
	ProcessType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ProcessType'),
	TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TransportMode'),
	FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='FreightMode'),
	EFIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='EFIndicator'),
	MovementIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='MovementIndicator'),
	PUDOMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='PUDOMode')
	SELECT	P.[Code], P.[Description], P.[Module], P.[ProcessType], P.[PickupFrom], P.[DeliveryTo], P.[EFIndicator],P.MovementIndicator, P.[TransportMode], P.[FreightMode], 
			P.[IsPlanningRequired], P.[IsStagingRequired], P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(M.LookupDescription,'') As ModuleTypeDescription,
			ISNULL(Pt.LookupDescription,'') As ProcessTypeDescription,
			ISNULL(Tm.LookupDescription,'') As TransportModeDescription,
			ISNULL(Fm.LookupDescription,'') As FreightModeDescription,
			ISNULL(Ef.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(Pu.LookupDescription,'') As PUModeDescription,
			ISNULL(Do.LookupDescription,'') As DOModeDescription,
			ISNULL(Mi.LookupDescription,'') As MovementIndicatorDescription
	FROM	[Master].[Process] P
	Left Outer Join ModuleType M On 
		M.LookupID = P.Module
	Left Outer Join ProcessType Pt On 
		Pt.LookupID = P.ProcessType
	Left Outer Join TransportMode Tm On 
		Tm.LookupID = P.TransportMode
	Left Outer Join FreightMode Fm On 
		Fm.LookupID = P.FreightMode
	Left Outer Join EFIndicator Ef On 
		Ef.LookupID = P.EFIndicator
	Left Outer Join PUDOMode Pu On 
		Pu.LookupID = P.PickupFrom
	Left Outer Join PUDOMode Do On 
		Do.LookupID = P.DeliveryTo
	Left Outer Join MovementIndicator Mi ON
		Mi.LookupID = P.MovementIndicator
		

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Process] Record Into [Process] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProcessInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessInsert] 
END 
GO
CREATE PROC [Master].[usp_ProcessInsert] 
    @Code nvarchar(50),
    @Description nvarchar(255),
    @Module smallint,
    @ProcessType smallint,
    @PickupFrom smallint,
    @DeliveryTo smallint,
    @EFIndicator smallint,
	@MovementIndicator smallint,
    @TransportMode smallint,
    @FreightMode smallint,
    @IsPlanningRequired bit,
    @IsStagingRequired bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
    
AS 
  

BEGIN
	
	INSERT INTO [Master].[Process] (
			[Code], [Description], [Module], [ProcessType], [PickupFrom], [DeliveryTo], [EFIndicator],MovementIndicator, [TransportMode], [FreightMode], 
			[IsPlanningRequired], [IsStagingRequired], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@Code, @Description, @Module, @ProcessType, @PickupFrom, @DeliveryTo, @EFIndicator,@MovementIndicator, @TransportMode, @FreightMode, 
			@IsPlanningRequired, @IsStagingRequired, CAST(1 as bit) , @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Process] Record Into [Process] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProcessUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessUpdate] 
END 
GO
CREATE PROC [Master].[usp_ProcessUpdate] 
    @Code nvarchar(50),
    @Description nvarchar(255),
    @Module smallint,
    @ProcessType smallint,
    @PickupFrom smallint,
    @DeliveryTo smallint,
    @EFIndicator smallint,
	@MovementIndicator smallint,
    @TransportMode smallint,
    @FreightMode smallint,
    @IsPlanningRequired bit,
    @IsStagingRequired bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Process]
	SET		[Description] = @Description, [Module] = @Module, [ProcessType] = @ProcessType, [PickupFrom] = @PickupFrom, [DeliveryTo] = @DeliveryTo, 
			[EFIndicator] = @EFIndicator, MovementIndicator=@MovementIndicator, [TransportMode] = @TransportMode, [FreightMode] = @FreightMode, [IsPlanningRequired] = @IsPlanningRequired, 
			[IsStagingRequired] = @IsStagingRequired, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[Code] = @Code
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Process] Record Into [Process] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProcessSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessSave] 
END 
GO
CREATE PROC [Master].[usp_ProcessSave] 
    @Code nvarchar(50),
    @Description nvarchar(255),
    @Module smallint,
    @ProcessType smallint,
    @PickupFrom smallint,
    @DeliveryTo smallint,
    @EFIndicator smallint,
	@MovementIndicator smallint,
    @TransportMode smallint,
    @FreightMode smallint,
    @IsPlanningRequired bit,
    @IsStagingRequired bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[Process] 
		WHERE 	[Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_ProcessUpdate] 
			@Code, @Description, @Module, @ProcessType, @PickupFrom, @DeliveryTo, @EFIndicator,@MovementIndicator, @TransportMode, @FreightMode, 
			@IsPlanningRequired, @IsStagingRequired, @CreatedBy, @ModifiedBy


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_ProcessInsert] 
			@Code, @Description, @Module, @ProcessType, @PickupFrom, @DeliveryTo, @EFIndicator,@MovementIndicator, @TransportMode, @FreightMode, 
			@IsPlanningRequired, @IsStagingRequired, @CreatedBy, @ModifiedBy

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[ProcessSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Process] Record  based on [Process]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProcessDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessDelete] 
END 
GO
CREATE PROC [Master].[usp_ProcessDelete] 
    @Code nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[Process]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[Code] = @Code

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessDelete]
-- ========================================================================================================================================
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProcessListByMovements]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	14-Jul-2016
-- Description:	List  the [Process] Record  based on Multiple Code params.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProcessListByMovements]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessListByMovements] 
END 
GO
CREATE PROC [Master].[usp_ProcessListByMovements] 
	@params varchar(max)
As

Begin
--Set @params = '1001,1002,1003,1004';
Declare @SqlQuery varchar(max);
--Set @SqlQuery = 'Select * From Master.Process Where Module in (' + @params   + ')'
Set @SqlQuery = ';With ModuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''Module''),
	ProcessType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ProcessType''),
	TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''TransportMode''),
	FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''FreightMode''),
	EFIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''EFIndicator''),
	MovementIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''MovementIndicator''),
	PUDOMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''PUDOMode'')
	SELECT	P.[Code], P.[Description], P.[Module], P.[ProcessType], P.[PickupFrom], P.[DeliveryTo], P.[EFIndicator],P.MovementIndicator, P.[TransportMode], P.[FreightMode], 
			P.[IsPlanningRequired], P.[IsStagingRequired], P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],
			ISNULL(M.LookupDescription,'''') As ModuleTypeDescription,
			ISNULL(Pt.LookupDescription,'''') As ProcessTypeDescription,
			ISNULL(Tm.LookupDescription,'''') As TransportModeDescription,
			ISNULL(Fm.LookupDescription,'''') As FreightModeDescription,
			ISNULL(Ef.LookupDescription,'''') As EFIndicatorDescription,
			ISNULL(Pu.LookupDescription,'''') As PUModeDescription,
			ISNULL(Do.LookupDescription,'''') As DOModeDescription,
			ISNULL(Mi.LookupDescription,'''') As MovementIndicatorDescription
	FROM	[Master].[Process] P
	Left Outer Join ModuleType M On 
		M.LookupID = P.Module
	Left Outer Join ProcessType Pt On 
		Pt.LookupID = P.ProcessType
	Left Outer Join TransportMode Tm On 
		Tm.LookupID = P.TransportMode
	Left Outer Join FreightMode Fm On 
		Fm.LookupID = P.FreightMode
	Left Outer Join EFIndicator Ef On 
		Ef.LookupID = P.EFIndicator
	Left Outer Join PUDOMode Pu On 
		Pu.LookupID = P.PickupFrom
	Left Outer Join PUDOMode Do On 
		Do.LookupID = P.DeliveryTo
	Left Outer Join MovementIndicator Mi ON
		Mi.LookupID = P.MovementIndicator
	WHERE   IsActive = CAST(1 as bit)	And [Module] in (' + @params   + ')'

--print @SqlQuery

execute(@SqlQuery)

End

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProcessListByMovements]
-- ========================================================================================================================================

GO


IF OBJECT_ID('[Master].[usp_ProcessDataTableList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProcessDataTableList] 
END 
GO
CREATE PROC [Master].[usp_ProcessDataTableList] (
	@limit smallint,
	@offset smallint,
	@sortColumn varchar(50),
	@sortType varchar(50))

AS 
 
BEGIN

 Declare @Sql varchar(max);
	Set @Sql = ';With ModuleType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''Module''),'
	Set @Sql += ' ProcessType As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''ProcessType''),'
	Set @Sql += ' TransportMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''TransportMode''),'
	Set @Sql += ' FreightMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''FreightMode''),'
	Set @Sql += ' EFIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''EFIndicator''),'
	Set @Sql += ' MovementIndicator As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''MovementIndicator''),'
	Set @Sql += ' PUDOMode As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory =''PUDOMode'')'
	Set @Sql += ' SELECT	P.[Code], P.[Description], P.[Module], P.[ProcessType], P.[PickupFrom], P.[DeliveryTo], P.[EFIndicator],P.MovementIndicator, P.[TransportMode], P.[FreightMode],'
			Set @Sql += ' P.[IsPlanningRequired], P.[IsStagingRequired], P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn],'
			Set @Sql += ' ISNULL(M.LookupDescription,'''') As ModuleTypeDescription,'
			Set @Sql += ' ISNULL(Pt.LookupDescription,'''') As ProcessTypeDescription,'
			Set @Sql += ' ISNULL(Tm.LookupDescription,'''') As TransportModeDescription,'
			Set @Sql += ' ISNULL(Fm.LookupDescription,'''') As FreightModeDescription,'
			Set @Sql += ' ISNULL(Ef.LookupDescription,'''') As EFIndicatorDescription,'
			Set @Sql += ' ISNULL(Pu.LookupDescription,'''') As PUModeDescription,'
			Set @Sql += ' ISNULL(Do.LookupDescription,'''') As DOModeDescription,'
			Set @Sql += ' ISNULL(Mi.LookupDescription,'''') As MovementIndicatorDescription'
	Set @Sql += ' FROM	[Master].[Process] P'
	Set @Sql += ' Left Outer Join ModuleType M On' 
		Set @Sql += ' M.LookupID = P.Module'
	Set @Sql += ' Left Outer Join ProcessType Pt On' 
		Set @Sql += ' Pt.LookupID = P.ProcessType'
	Set @Sql += ' Left Outer Join TransportMode Tm On' 
		Set @Sql += ' Tm.LookupID = P.TransportMode'
	Set @Sql += ' Left Outer Join FreightMode Fm On' 
		Set @Sql += ' Fm.LookupID = P.FreightMode'
	Set @Sql += ' Left Outer Join EFIndicator Ef On' 
		Set @Sql += ' Ef.LookupID = P.EFIndicator'
	Set @Sql += ' Left Outer Join PUDOMode Pu On' 
		Set @Sql += ' Pu.LookupID = P.PickupFrom'
	Set @Sql += ' Left Outer Join PUDOMode Do On' 
		Set @Sql += ' Do.LookupID = P.DeliveryTo'
	Set @Sql += ' Left Outer Join MovementIndicator Mi ON'
		Set @Sql += ' Mi.LookupID = P.MovementIndicator'
		print @sql;
	Set @Sql += ' Where IsActive = Cast(1 as bit) Order By ' + @sortColumn + ' ' + @sortType + ' OFFSET ' + Convert(varchar(5), @offset) + ' ROWS Fetch Next ' + Convert(varchar(5), @limit) + ' Rows Only';

 Exec(@Sql) 

END

