
/****** Object:  View [Master].[QuotationView]    Script Date: 29-03-2017 17:07:32 ******/
DROP VIEW [Master].[TariffView]
GO

/****** Object:  View [Master].[StandardTariffView]   Script Date: 29-03-2017 17:07:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [Master].[TariffView]
AS
SELECT  Hd.QuotationNo, Hd.Association, Hd.EffectiveDate, Hd.ExpiryDate, Hd.IsActive, Hd.CreatedBy, 
		Hd.CreatedOn, Hd.ModifiedBy, Hd.ModifiedOn, Dt.QuotationType, Dt.Module, Dt.TariffMode, 
		Dt.Transactions, Dt.SellingPrice, Dt.SlabFrom, Dt.SlabTo, Dt.Percentage
FROM	Master.TariffHeader Hd 
INNER JOIN	Master.TariffDetail	Dt ON 
Hd.QuotationNo = Dt.QuotationNo
Where 
Hd.QuotationNo <> 'STANDARD_1000001'

