-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryChargesListByJobCategoryCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryChargesVAS] Record based on [JobCategoryChargesVAS] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryChargesListByJobCategoryCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryChargesListByJobCategoryCode] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryChargesListByJobCategoryCode] 
@jobCategoryCode varchar(30),
@branchId int
AS 

BEGIN

	;With PaymentToLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentTo'),
	PaymentTermLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='PaymentType')
	SELECT	JC.[BranchID], JC.[ChargeCode], JC.[JobCategoryCode], JC.[MovementCode],JC.Module, JC.[PaymentTo], JC.[PaymentTerm], 
			JC.[IsCargoCharge], JC.[CreatedBy], JC.[CreatedOn], JC.[ModifiedBy], JC.[ModifiedOn],
			ISNULL(Pt.LookupDescription,'') As PaymentToDescription,
			ISNULL(Pm.LookupDescription,'') As PaymentTermDescription,
			Jb.Description As JobCategoryDescription,
			Chg.ChargeDescription As ChargeCodeDescription
	FROM	[Master].[JobCategoryCharges] JC 
	Left Outer JOin PaymentToLookup Pt On 
		Jc.PaymentTo = Pt.LookupID
	Left Outer Join PaymentTermLookup Pm ON 
		Jc.PaymentTerm = Pm.LookupID
	Left Outer Join Master.ChargeMaster Chg On 
		Jc.ChargeCode = Chg.ChargeCode
	Left Outer Join Master.JobCategory Jb On
		Jc.JobCategoryCode = Jb.Code
	Where	Jc.BranchID = @branchId And 
			Jc.[JobCategoryCode] = @jobCategoryCode 

END



-- ========================================================================================================================================
-- END              [Master].[usp_JobCategoryChargesListByJobCategoryCode] 
-- ========================================================================================================================================