
-- ========================================================================================================================================
-- START											 [Master].[usp_MerchantSearch]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	16-Jun-2016
-- Description:	Select the [Merchant] Record based on [Merchant] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_MerchantSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_MerchantSearch] 
END 
GO

CREATE PROC [Master].[usp_MerchantSearch](
@MerchantName NVARCHAR(100),
@MerchantType nvarchar(20),
@CompanyCode bigint)
as
Begin

if @CompanyCode != 1000
	Begin
     If @MerchantType = 'all'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%' and ([CompanyCode] = @CompanyCode)-- Or [CompanyCode] = 1000
			   And IsActive = CAST(1 as bit)
else If @MerchantType ='Shipper'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
			   and (IsShipper = CAST(1 as bit) Or IsConsignee = CAST(1 as bit))  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
			   And IsActive = CAST(1 as bit)
	Else If @MerchantType ='Liner'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
			   And IsLiner = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode)-- Or [CompanyCode] = 1000
			   And IsActive = CAST(1 as bit)
    Else if @MerchantType = 'consignee'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsConsignee = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'terminal'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsTerminal = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'freightForwarder'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsFreightForwarder = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'OverseasAgent'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsOverseasAgent = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'forwarder'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE   [MerchantName] like '%' + @MerchantName + '%'
				   And IsForwarder = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode)-- Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'billingCustomer'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE   [MerchantName] like '%' + @MerchantName + '%'
				   And IsBillingCustomer = CAST(1 as bit)  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	  Else if @MerchantType = 'ShipperConsignee'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And (IsShipper = CAST(1 as bit) or IsConsignee = CAST(1 as bit))  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'vendor'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsVendor = CAST(1 as bit))  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
		  And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'yard'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsYard = CAST(1 as bit))  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
		  And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'haulier'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsHaulier = CAST(1 as bit))  and ([CompanyCode] = @CompanyCode) --Or [CompanyCode] = 1000
		  And IsActive = CAST(1 as bit)
	End
else
	Begin
	 If @MerchantType = 'all'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn], [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%' 
			   And IsActive = CAST(1 as bit)
else If @MerchantType ='Shipper'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
			   and (IsShipper = CAST(1 as bit) Or IsConsignee = CAST(1 as bit))  
			   And IsActive = CAST(1 as bit)
	Else If @MerchantType ='Liner'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
			   And IsLiner = CAST(1 as bit)  
			   And IsActive = CAST(1 as bit)
    Else if @MerchantType = 'consignee'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsConsignee = CAST(1 as bit) 
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'terminal'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsTerminal = CAST(1 as bit)  
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'freightForwarder'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsFreightForwarder = CAST(1 as bit)
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'OverseasAgent'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And IsOverseasAgent = CAST(1 as bit)
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'forwarder'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE   [MerchantName] like '%' + @MerchantName + '%'
				   And IsForwarder = CAST(1 as bit)  
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'billingCustomer'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE   [MerchantName] like '%' + @MerchantName + '%'
				   And IsBillingCustomer = CAST(1 as bit) 
				   And IsActive = CAST(1 as bit)
	  Else if @MerchantType = 'ShipperConsignee'
		SELECT	[MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode], 
				[YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
				[Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
				[IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
				[CurrentBalance], [IsActive], [CreatedBy], [CreatedOn], 
				[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		FROM	[Master].[Merchant]
		WHERE  [MerchantName] like '%' + @MerchantName + '%'
				   And (IsShipper = CAST(1 as bit) or IsConsignee = CAST(1 as bit)) 
				   And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'vendor'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn],IsVerified,VerifiedBy,VerifiedOn , [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsVendor = CAST(1 as bit))  
		  And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'yard'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsYard = CAST(1 as bit))  
		  And IsActive = CAST(1 as bit)
	Else if @MerchantType = 'haulier'
		 SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], [TaxID], [OwnerCode], [AgentCode],
		 [YardCode], [BillingMethod], [CreditTerm], [CreditLimit], [DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], 
		 [Remarks], [IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS], [IsRailOperator], [IsTerminal], 
		 [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter], [IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer],
		 [CurrentBalance], [IsActive], [CreatedBy], [CreatedOn],[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, [CompanyCode],[BusinessType],
				IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal,SubscriberCompanyCode
		 FROM [Master].[Merchant]
		 WHERE  [MerchantName] like '%' + @MerchantName + '%'
		  And (IsHaulier = CAST(1 as bit)) 
		  And IsActive = CAST(1 as bit)
	End



End

-- ========================================================================================================================================
-- END  											 [Master].[usp_MerchantSearch] 
-- ========================================================================================================================================

