
-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StockRoom] Record based on [StockRoom] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StockRoomSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomSelect] 
END 
GO
CREATE PROC [Master].[usp_StockRoomSelect] 
    @Code NVARCHAR(10)
AS 

BEGIN

	SELECT	[Code], [Description], [IsOnHold], [IsDamage], [IsCalculateStorage], [IsAllowReplenish], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[StockRoom]
	WHERE  [Code] = @Code  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StockRoom] Records from [StockRoom] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StockRoomList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomList] 
END 
GO
CREATE PROC [Master].[usp_StockRoomList] 

AS 
BEGIN

	SELECT	[Code], [Description], [IsOnHold], [IsDamage], [IsCalculateStorage], [IsAllowReplenish], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[StockRoom]


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [StockRoom] Records from [StockRoom] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StockRoomPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomPageView] 
END 
GO
CREATE PROC [Master].[usp_StockRoomPageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT	[Code], [Description], [IsOnHold], [IsDamage], [IsCalculateStorage], [IsAllowReplenish], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[StockRoom]
	ORDER BY [Code]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [StockRoom] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StockRoomRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomRecordCount] 
END 
GO
CREATE PROC [Master].[usp_StockRoomRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[StockRoom]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [StockRoom] Record based on [StockRoom] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_StockRoomAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_StockRoomAutoCompleteSearch] 
    @Code NVARCHAR(10)
AS 

BEGIN

	SELECT	[Code], [Description], [IsOnHold], [IsDamage], [IsCalculateStorage], [IsAllowReplenish], [Status], 
			[CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Master].[StockRoom]
	WHERE   [Code] LIKE '%' +  @Code  + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [StockRoom] Record Into [StockRoom] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_StockRoomInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomInsert] 
END 
GO
CREATE PROC [Master].[usp_StockRoomInsert] 
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsOnHold bit,
    @IsDamage bit,
    @IsCalculateStorage bit,
    @IsAllowReplenish bit,
    @Status bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[StockRoom] (
			[Code], [Description], [IsOnHold], [IsDamage], [IsCalculateStorage], [IsAllowReplenish], [Status], [CreatedBy], [CreatedOn])
	SELECT	@Code, @Description, @IsOnHold, @IsDamage, @IsCalculateStorage, @IsAllowReplenish, @Status, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [StockRoom] Record Into [StockRoom] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_StockRoomUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomUpdate] 
END 
GO
CREATE PROC [Master].[usp_StockRoomUpdate] 
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsOnHold bit,
    @IsDamage bit,
    @IsCalculateStorage bit,
    @IsAllowReplenish bit,
    @Status bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[StockRoom]
	SET		[Description] = @Description, [IsOnHold] = @IsOnHold, [IsDamage] = @IsDamage, 
			[IsCalculateStorage] = @IsCalculateStorage, [IsAllowReplenish] = @IsAllowReplenish, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[Code] = @Code
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [StockRoom] Record Into [StockRoom] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_StockRoomSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomSave] 
END 
GO
CREATE PROC [Master].[usp_StockRoomSave] 
    @Code nvarchar(10),
    @Description nvarchar(100),
    @IsOnHold bit,
    @IsDamage bit,
    @IsCalculateStorage bit,
    @IsAllowReplenish bit,
    @Status bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[StockRoom] 
		WHERE 	[Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_StockRoomUpdate] 
		@Code, @Description, @IsOnHold, @IsDamage, @IsCalculateStorage, @IsAllowReplenish, @Status, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_StockRoomInsert] 
		@Code, @Description, @IsOnHold, @IsDamage, @IsCalculateStorage, @IsAllowReplenish, @Status, @CreatedBy, @ModifiedBy 

	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[StockRoomSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_StockRoomDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [StockRoom] Record  based on [StockRoom]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_StockRoomDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_StockRoomDelete] 
END 
GO
CREATE PROC [Master].[usp_StockRoomDelete] 
    @Code nvarchar(10)
AS 

	
BEGIN

	UPDATE	[Master].[StockRoom]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[Code] = @Code

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_StockRoomDelete]
-- ========================================================================================================================================

GO

 
