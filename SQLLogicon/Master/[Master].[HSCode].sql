
-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [HSCode] Record based on [HSCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeSelect] 
END 
GO
CREATE PROC [Master].[usp_HSCodeSelect] 
    @TariffCode nvarchar(20)
AS 
 

BEGIN

	SELECT [TariffCode], [Indicator], [HeaderCode], [Description], [UOMCode], [ImportDutyMethod], [ImportDutyRate], [ImportDutySpecificRate], 
		[ImportDutyInQuota], [ImportDutyOutQuota], [ImportExciseDutyMethod], [ImportExciseDutyRate], [ImportExciseSpecificRate], [ExportDutyMethod], 
			[ExportDutyRate], [ExportDutySpecificRate], [ATIGA], [GST], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,OldPDK,OldAHTN, [IsVehicle]
	FROM   [Master].[HSCode]
	WHERE  [TariffCode] = @TariffCode   

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [HSCode] Records from [HSCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeList] 
END 
GO
CREATE PROC [Master].[usp_HSCodeList] 

AS 
 
BEGIN
	SELECT [TariffCode], [Indicator], [HeaderCode], [Description], [UOMCode], [ImportDutyMethod], [ImportDutyRate], [ImportDutySpecificRate], [ImportDutyInQuota], 
	[ImportDutyOutQuota], [ImportExciseDutyMethod], [ImportExciseDutyRate], [ImportExciseSpecificRate], [ExportDutyMethod], [ExportDutyRate], [ExportDutySpecificRate], [ATIGA], [GST], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],OldPDK, OldAHTN, [IsVehicle]  
	FROM   [Master].[HSCode]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodePageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [HSCode] Records from [HSCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodePageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodePageView] 
END 
GO
CREATE PROC [Master].[usp_HSCodePageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [TariffCode], [Indicator], [HeaderCode], [Description], [UOMCode], [ImportDutyMethod], [ImportDutyRate], [ImportDutySpecificRate], [ImportDutyInQuota], 
		[ImportDutyOutQuota], [ImportExciseDutyMethod], [ImportExciseDutyRate], [ImportExciseSpecificRate], [ExportDutyMethod], [ExportDutyRate], [ExportDutySpecificRate], 
		[ATIGA], [GST], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,OldPDK,OldAHTN 
	FROM   [Master].[HSCode]
	ORDER BY [TariffCode] 
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodePageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [HSCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeRecordCount] 
END 
GO
CREATE PROC [Master].[usp_HSCodeRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[HSCode]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [HSCode] Record based on [HSCode] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_HSCodeAutoCompleteSearch]
        @TariffCode nvarchar(20)
     
AS 

BEGIN
	SELECT [TariffCode], [Indicator], [HeaderCode], [Description], [UOMCode], [ImportDutyMethod], [ImportDutyRate], [ImportDutySpecificRate], [ImportDutyInQuota], 
	[ImportDutyOutQuota], [ImportExciseDutyMethod], [ImportExciseDutyRate], [ImportExciseSpecificRate], [ExportDutyMethod], [ExportDutyRate], [ExportDutySpecificRate], 
	[ATIGA], [GST], [Remarks], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],OldPDK,OldAHTN 
	FROM   [Master].[HSCode]
	WHERE  [TariffCode] = @TariffCode  
	END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [HSCode] Record Into [HSCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeInsert] 
END 
GO
CREATE PROC [Master].[usp_HSCodeInsert] 
    @TariffCode nvarchar(20),
    @Indicator nvarchar(5) = NULL,
    @HeaderCode nvarchar(20) = NULL,
    @Description nvarchar(MAX) = NULL,
    @UOMCode nvarchar(20) = NULL,
    @ImportDutyMethod nvarchar(20) = NULL,
    @ImportDutyRate numeric(18, 6) = NULL,
    @ImportDutySpecificRate numeric(18, 6) = NULL,
    @ImportDutyInQuota numeric(18, 6) = NULL,
    @ImportDutyOutQuota numeric(18, 6) = NULL,
    @ImportExciseDutyMethod nvarchar(20) = NULL,
    @ImportExciseDutyRate numeric(18, 6) = NULL,
    @ImportExciseSpecificRate numeric(18, 6) = NULL,
    @ExportDutyMethod nvarchar(20) = NULL,
    @ExportDutyRate numeric(18, 6) = NULL,
    @ExportDutySpecificRate numeric(18, 6) = NULL,
    @ATIGA numeric(18, 6) = NULL,
    @GST numeric(18, 6) = NULL,
    @Remarks nvarchar(255) = NULL,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50) ,
	@OldPDK nvarchar(20),
	@OldAHTN nvarchar(20)
AS 
  

BEGIN
	
	INSERT INTO [Master].[HSCode] (
			[TariffCode], [Indicator], [HeaderCode], [Description], [UOMCode], [ImportDutyMethod], [ImportDutyRate], 
			[ImportDutySpecificRate], [ImportDutyInQuota], [ImportDutyOutQuota], [ImportExciseDutyMethod], 
			[ImportExciseDutyRate], [ImportExciseSpecificRate], [ExportDutyMethod], [ExportDutyRate], 
			[ExportDutySpecificRate], [ATIGA], [GST], [Remarks], [CreatedBy], [CreatedOn])
	SELECT	@TariffCode, @Indicator, @HeaderCode, @Description, @UOMCode, @ImportDutyMethod, @ImportDutyRate, 
			@ImportDutySpecificRate, @ImportDutyInQuota, @ImportDutyOutQuota, @ImportExciseDutyMethod, 
			@ImportExciseDutyRate, @ImportExciseSpecificRate, @ExportDutyMethod, @ExportDutyRate, 
			@ExportDutySpecificRate, @ATIGA, @GST, @Remarks, @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [HSCode] Record Into [HSCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeUpdate] 
END 
GO
CREATE PROC [Master].[usp_HSCodeUpdate] 
    @TariffCode nvarchar(20),
    @Indicator nvarchar(5) = NULL,
    @HeaderCode nvarchar(20) = NULL,
    @Description nvarchar(MAX) = NULL,
    @UOMCode nvarchar(20) = NULL,
    @ImportDutyMethod nvarchar(20) = NULL,
    @ImportDutyRate numeric(18, 6) = NULL,
    @ImportDutySpecificRate numeric(18, 6) = NULL,
    @ImportDutyInQuota numeric(18, 6) = NULL,
    @ImportDutyOutQuota numeric(18, 6) = NULL,
    @ImportExciseDutyMethod nvarchar(20) = NULL,
    @ImportExciseDutyRate numeric(18, 6) = NULL,
    @ImportExciseSpecificRate numeric(18, 6) = NULL,
    @ExportDutyMethod nvarchar(20) = NULL,
    @ExportDutyRate numeric(18, 6) = NULL,
    @ExportDutySpecificRate numeric(18, 6) = NULL,
    @ATIGA numeric(18, 6) = NULL,
    @GST numeric(18, 6) = NULL,
    @Remarks nvarchar(255) = NULL,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@OldPDK nvarchar(20),
	@OldAHTN nvarchar(20)
AS 
 
	
BEGIN

	UPDATE [Master].[HSCode]
	SET    [TariffCode] = @TariffCode, [Indicator] = @Indicator, [HeaderCode] = @HeaderCode, 
			[Description] = @Description, [UOMCode] = @UOMCode, [ImportDutyMethod] = @ImportDutyMethod, [ImportDutyRate] = @ImportDutyRate, 
			[ImportDutySpecificRate] = @ImportDutySpecificRate, [ImportDutyInQuota] = @ImportDutyInQuota, [ImportDutyOutQuota] = @ImportDutyOutQuota, 
			[ImportExciseDutyMethod] = @ImportExciseDutyMethod, [ImportExciseDutyRate] = @ImportExciseDutyRate, [ImportExciseSpecificRate] = @ImportExciseSpecificRate, 
			[ExportDutyMethod] = @ExportDutyMethod, [ExportDutyRate] = @ExportDutyRate, [ExportDutySpecificRate] = @ExportDutySpecificRate, 
			[ATIGA] = @ATIGA, [GST] = @GST, [Remarks] = @Remarks, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),OldPDK=@OldPDK ,OldAHTN=@OldAHTN
	WHERE  [TariffCode] = @TariffCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [HSCode] Record Into [HSCode] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeSave] 
END 
GO
CREATE PROC [Master].[usp_HSCodeSave] 
    @TariffCode nvarchar(20),
    @Indicator nvarchar(5) = NULL,
    @HeaderCode nvarchar(20) = NULL,
    @Description nvarchar(MAX) = NULL,
    @UOMCode nvarchar(20) = NULL,
    @ImportDutyMethod nvarchar(20) = NULL,
    @ImportDutyRate numeric(18, 6) = NULL,
    @ImportDutySpecificRate numeric(18, 6) = NULL,
    @ImportDutyInQuota numeric(18, 6) = NULL,
    @ImportDutyOutQuota numeric(18, 6) = NULL,
    @ImportExciseDutyMethod nvarchar(20) = NULL,
    @ImportExciseDutyRate numeric(18, 6) = NULL,
    @ImportExciseSpecificRate numeric(18, 6) = NULL,
    @ExportDutyMethod nvarchar(20) = NULL,
    @ExportDutyRate numeric(18, 6) = NULL,
    @ExportDutySpecificRate numeric(18, 6) = NULL,
    @ATIGA numeric(18, 6) = NULL,
    @GST numeric(18, 6) = NULL,
    @Remarks nvarchar(255) = NULL,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@OldPDK nvarchar(20),
	@OldAHTN nvarchar(20)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[HSCode] 
		WHERE 	[TariffCode] = @TariffCode)>0
	BEGIN
	    Exec [Master].[usp_HSCodeUpdate] 
				@TariffCode, @Indicator, @HeaderCode, @Description, @UOMCode, @ImportDutyMethod, @ImportDutyRate, @ImportDutySpecificRate, @ImportDutyInQuota, 
				@ImportDutyOutQuota, @ImportExciseDutyMethod, @ImportExciseDutyRate, @ImportExciseSpecificRate, @ExportDutyMethod, @ExportDutyRate, 
				@ExportDutySpecificRate, @ATIGA, @GST, @Remarks, @CreatedBy, @ModifiedBy,@OldPDK,@OldAHTN 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_HSCodeInsert] 
				@TariffCode, @Indicator, @HeaderCode, @Description, @UOMCode, @ImportDutyMethod, @ImportDutyRate, @ImportDutySpecificRate, @ImportDutyInQuota, 
				@ImportDutyOutQuota, @ImportExciseDutyMethod, @ImportExciseDutyRate, @ImportExciseSpecificRate, @ExportDutyMethod, @ExportDutyRate, 
				@ExportDutySpecificRate, @ATIGA, @GST, @Remarks, @CreatedBy, @ModifiedBy,@OldPDK,@OldAHTN

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[HSCodeSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [HSCode] Record  based on [HSCode]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HSCodeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeDelete] 
END 
GO
CREATE PROC [Master].[usp_HSCodeDelete] 
    @Code nvarchar(20)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[HSCode]
	WHERE  [Code] = @Code
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeDelete]
-- ========================================================================================================================================

GO
 