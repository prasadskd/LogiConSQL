
-- ========================================================================================================================================
-- START											 [Config].[usp_LookupListByCategoryDamageList]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	30-Sep-2016
-- Description:	Select the [Lookup] Record based on [Lookup] table
-- ========================================================================================================================================


IF OBJECT_ID('[Config].[usp_LookupListByCategoryDamageList]') IS NOT NULL
BEGIN 
    DROP PROC [Config].[usp_LookupListByCategoryDamageList] 
END 
GO
CREATE PROC [Config].[usp_LookupListByCategoryDamageList]  

 @LookupCategory nvarchar(50),
 @LookupText varchar(20)

AS 
BEGIN

 SELECT [LookupID], [LookupDescription], [LookupCode]
 FROM   [Config].[Lookup]
 Where LookupCategory = @LookupCategory
 and LookupDescription like '%' + @LookupText + '%'

END

-- ========================================================================================================================================
-- END												[Config].[usp_LookupListByCategoryDamageList]
-- ========================================================================================================================================