
-- ========================================================================================================================================
-- START											 [Master].[usp_CompanySearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Company] Records from [Company] table

/*
Exec [Master].[usp_CompanySearch] 'Sdn',NULL,'2016-12-01','2017-01-10'
*/
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CompanySearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CompanySearch] 
END 
GO
CREATE PROC [Master].[usp_CompanySearch]
    @CompanyName nvarchar(255),
    @RegistrationNo nvarchar(50),
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL,
	@Email nvarchar(100)
AS 
BEGIN

	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';with RegistrationTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory=''RegistrationType'')
	SELECT	Rc.[CompanyCode], Rc.[CompanyName], Rc.[RegNo],Rc.[GSTNo],Rc.[RegistrationType],
		Rc.[Logo], Rc.[TaxId], Rc.[IsActive], Rc.[CreatedBy], Rc.[CreatedOn], Rc.[ModifiedBy], Rc.[ModifiedOn],
		ISNULL(RT.LookupDescription,'''') As RegistrationTypeDescription,RegistrationReferenceNo,IsSuspended,SuspensionRemarks
	 FROM   [Master].[Company] Rc
	 Inner Join [Security].[User] Su On Rc.CompanyCode = Su.CompanyCode
	 Left Outer Join RegistrationTypeDescription RT ON
		Rc.RegistrationType = RT.LookupID
	Where 1 = 1 '
	

	if (len(rtrim(@CompanyName)) > 0) set @sql = @sql + ' AND  Rc.[CompanyName] LIKE ''%' + @CompanyName + '%'''
	if (len(rtrim(@RegistrationNo)) > 0) set @sql = @sql + ' AND  Rc.[RegNo] LIKE ''%' + @RegistrationNo + '%'''
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),Rc.CreatedOn,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',Rc.CreatedOn)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),Rc.CreatedOn,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',Rc.CreatedOn)'
	if (len(rtrim(@CompanyName)) > 0) set @sql = @sql + ' AND  Rc.[CompanyName] LIKE ''%' + @CompanyName + '%'''
	if (len(rtrim(@Email)) > 0) set @sql = @sql + ' AND  Su.[UserID] LIKE ''%' + @Email + '%'''
	print @sql;

	exec sp_executesql @sql, N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;


END

-- ========================================================================================================================================
-- END  											[Master].[usp_CompanySearch]
-- ========================================================================================================================================

GO

