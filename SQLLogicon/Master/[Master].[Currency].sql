USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Currency] Record based on [Currency] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CurrencySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencySelect] 
END 
GO
CREATE PROC [Master].[usp_CurrencySelect] 
    @CurrencyCode NVARCHAR(3)
AS 

SET NOCOUNT ON; 

BEGIN

	SELECT [CurrencyCode], [Description], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Currency]
	WHERE  [CurrencyCode] = @CurrencyCode  

SET NOCOUNT OFF;

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Currency] Records from [Currency] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CurrencyList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyList] 
END 
GO
CREATE PROC [Master].[usp_CurrencyList] 

AS 
BEGIN

SET NOCOUNT ON;

	SELECT [CurrencyCode], [Description], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Currency]

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Currency] Record Into [Currency] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyInsert] 
END 
GO
CREATE PROC [Master].[usp_CurrencyInsert] 
    @CurrencyCode nvarchar(3),
    @Description nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN
SET NOCOUNT ON;
	
	INSERT INTO [Master].[Currency] ([CurrencyCode], [Description], [CreatedBy], [CreatedOn] )
	SELECT @CurrencyCode, @Description, @CreatedBy, GETUTCDATE() 

SET NOCOUNT OFF;	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Currency] Record Into [Currency] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyUpdate] 
END 
GO
CREATE PROC [Master].[usp_CurrencyUpdate] 
    @CurrencyCode nvarchar(3),
    @Description nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

SET NOCOUNT ON;

	UPDATE [Master].[Currency]
	SET    [CurrencyCode] = @CurrencyCode, [Description] = @Description, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [CurrencyCode] = @CurrencyCode
	
SET NOCOUNT OFF;
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Currency] Record Into [Currency] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencySave] 
END 
GO
CREATE PROC [Master].[usp_CurrencySave] 
    @CurrencyCode nvarchar(3),
    @Description nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
SET NOCOUNT ON;
	IF (SELECT COUNT(0) FROM [Master].[Currency] 
		WHERE 	[CurrencyCode] = @CurrencyCode)>0
	BEGIN
	    Exec [Master].[usp_CurrencyUpdate] 
		@CurrencyCode, @Description, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CurrencyInsert] 
		@CurrencyCode, @Description, @CreatedBy, @ModifiedBy 


	END
	
SET NOCOUNT OFF;
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CurrencySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CurrencyDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Currency] Record  based on [Currency]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CurrencyDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CurrencyDelete] 
END 
GO
CREATE PROC [Master].[usp_CurrencyDelete] 
    @CurrencyCode nvarchar(3)
AS 

	
BEGIN
SET NOCOUNT ON;
	 
	DELETE
	FROM   [Master].[Currency]
	WHERE  [CurrencyCode] = @CurrencyCode
 

SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CurrencyDelete]
-- ========================================================================================================================================

GO

 
