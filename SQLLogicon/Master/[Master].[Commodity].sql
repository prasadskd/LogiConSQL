-- ========================================================================================================================================
-- START											 [Master].[usp_CommoditySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Commodity] Record based on [Commodity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CommoditySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommoditySelect] 
END 
GO
CREATE PROC [Master].[usp_CommoditySelect] 
    @CommodityCode NVARCHAR(20)
AS 

BEGIN

	SELECT [CommodityCode], [Description], [IsWeightCargo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Commodity]
	WHERE  [CommodityCode] = @CommodityCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CommoditySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Commodity] Records from [Commodity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CommodityList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityList] 
END 
GO
CREATE PROC [Master].[usp_CommodityList] 

AS 
BEGIN

	SELECT [CommodityCode], [Description], [IsWeightCargo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Commodity]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Commodity] Records from [Commodity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CommodityPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityPageView] 
END 
GO
CREATE PROC [Master].[usp_CommodityPageView] 
	@fetchrows bigint
AS 
BEGIN

	SELECT [CommodityCode], [Description], [IsWeightCargo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Commodity]
	ORDER BY [CommodityCode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Commodity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CommodityRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityRecordCount] 
END 
GO
CREATE PROC [Master].[usp_CommodityRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Commodity]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Commodity] Record based on [Commodity] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CommodityAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_CommodityAutoCompleteSearch] 
    @Description NVARCHAR(50)
AS 

BEGIN

	SELECT [CommodityCode], [Description], [IsWeightCargo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[Commodity]
	WHERE  [Description] LIKE '%' +  @Description + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Commodity] Record Into [Commodity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CommodityInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityInsert] 
END 
GO
CREATE PROC [Master].[usp_CommodityInsert] 
    @CommodityCode nvarchar(20),
    @Description nvarchar(50),
    @IsWeightCargo bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[Commodity] ([CommodityCode], [Description], [IsWeightCargo], [CreatedBy], [CreatedOn])
	SELECT @CommodityCode, @Description, @IsWeightCargo, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Commodity] Record Into [Commodity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CommodityUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityUpdate] 
END 
GO
CREATE PROC [Master].[usp_CommodityUpdate] 
    @CommodityCode nvarchar(20),
    @Description nvarchar(50),
    @IsWeightCargo bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE [Master].[Commodity]
	SET    [Description] = @Description, [IsWeightCargo] = @IsWeightCargo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [CommodityCode] = @CommodityCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CommoditySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Commodity] Record Into [Commodity] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CommoditySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommoditySave] 
END 
GO
CREATE PROC [Master].[usp_CommoditySave] 
    @CommodityCode nvarchar(20),
    @Description nvarchar(50),
    @IsWeightCargo bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Commodity] 
		WHERE 	[CommodityCode] = @CommodityCode)>0
	BEGIN
	    Exec [Master].[usp_CommodityUpdate] 
		@CommodityCode, @Description, @IsWeightCargo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CommodityInsert] 
		@CommodityCode, @Description, @IsWeightCargo, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CommoditySave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_CommodityDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Commodity] Record  based on [Commodity]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CommodityDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CommodityDelete] 
END 
GO
CREATE PROC [Master].[usp_CommodityDelete] 
    @CommodityCode nvarchar(20)
AS 

	
BEGIN

	 
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[Commodity]
	WHERE  [CommodityCode] = @CommodityCode
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CommodityDelete]
-- ========================================================================================================================================

GO
 