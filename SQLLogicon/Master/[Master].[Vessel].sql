
-- ========================================================================================================================================
-- START											 [Master].[usp_VesselSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Vessel] Record based on [Vessel] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_VesselSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselSelect] 
END 
GO
CREATE PROC [Master].[usp_VesselSelect] 
    @VesselID NVARCHAR(20)
AS 

BEGIN

	;with VesselTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselType'),
	VesselClassDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselClass')
	SELECT	V.[VesselID], V.[VesselName], V.[VesselType], V.[PlaceOfRegistration], V.[CountryCode], V.[FlagShip], V.[CallSignNo], V.[LloydID], 
			V.[VesselClass], V.[OwnerCode], V.[OwnerAddress1], V.[OwnerAddress2], V.[OwnerAddress3], V.[PSACode], V.[GrossRegisteredTonnage], 
			V.[NetRegisteredTonnage], V.[OverallLength], V.[DeadWeight], V.[StandardDraft], V.[VesselCapacity], V.[Beam], V.[DisplacementWeight], 
			V.[IsActive], V.[CreatedBy], V.[CreatedOn], V.[ModifiedBy], V.[ModifiedOn] , V.[YearBuilt],
			ISNULL(Onr.MerchantName,'') As OwnerName,
			ISNULL(Psa.MerchantName,'') As PSAName,
			ISNULL(C.CountryName,'') As CountryName,
			ISNULL(F.CountryName,'') As FlagShipName,
			ISNULL(VT.LookupDescription,'') As VesselTypeDescription,
			ISNULL(VC.LookupDescription,'') As VesselClassDescription
	FROM	[Master].[Vessel] V
	Left Outer Join Master.Merchant Onr ON
		V.OwnerCode = Convert(nvarchar(20),Onr.MerchantCode)
	Left Outer Join Master.Merchant Psa ON
		V.PSACode = Convert(nvarchar(20),Psa.MerchantCode)
	Left Outer Join Master.Country C ON 
		V.CountryCode = C.CountryCode
	Left Outer Join Master.Country F On 
		V.FlagShip = F.CountryCode
	Left Outer Join VesselTypeDescription VT ON 
		v.VesselType = VT.LookupID
	Left Outer Join VesselClassDescription VC ON 
		V.VesselClass = VC.LookupID
	WHERE  [VesselID] = @VesselID  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_VesselList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Vessel] Records from [Vessel] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_VesselList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselList] 
END 
GO
CREATE PROC [Master].[usp_VesselList] 

AS 
BEGIN

	;with VesselTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselType'),
	VesselClassDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselClass')
	SELECT	V.[VesselID], V.[VesselName], V.[VesselType], V.[PlaceOfRegistration], V.[CountryCode], V.[FlagShip], V.[CallSignNo], V.[LloydID], 
			V.[VesselClass], V.[OwnerCode], V.[OwnerAddress1], V.[OwnerAddress2], V.[OwnerAddress3], V.[PSACode], V.[GrossRegisteredTonnage], 
			V.[NetRegisteredTonnage], V.[OverallLength], V.[DeadWeight], V.[StandardDraft], V.[VesselCapacity], V.[Beam], V.[DisplacementWeight], 
			V.[IsActive], V.[CreatedBy], V.[CreatedOn], V.[ModifiedBy], V.[ModifiedOn] ,V.[YearBuilt],
			ISNULL(Onr.MerchantName,'') As OwnerName,
			ISNULL(Psa.MerchantName,'') As PSAName,
			ISNULL(C.CountryName,'') As CountryName,
			ISNULL(F.CountryName,'') As FlagShipName,
			ISNULL(VT.LookupDescription,'') As VesselTypeDescription,
			ISNULL(VC.LookupDescription,'') As VesselClassDescription
	FROM	[Master].[Vessel] V
	Left Outer Join Master.Merchant Onr ON
		V.OwnerCode = Convert(nvarchar(20),Onr.MerchantCode)
	Left Outer Join Master.Merchant Psa ON
		V.PSACode = Convert(nvarchar(20),Psa.MerchantCode)
	Left Outer Join Master.Country C ON 
		V.CountryCode = C.CountryCode
	Left Outer Join Master.Country F On 
		V.FlagShip = F.CountryCode
	Left Outer Join VesselTypeDescription VT ON 
		v.VesselType = VT.LookupID
	Left Outer Join VesselClassDescription VC ON 
		V.VesselClass = VC.LookupID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_VesselPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Vessel] Records from [Vessel] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_VesselPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselPageView] 
END 
GO
CREATE PROC [Master].[usp_VesselPageView] 
	@fetchrows bigint
AS 
BEGIN

	;with VesselTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselType'),
	VesselClassDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselClass')
	SELECT	V.[VesselID], V.[VesselName], V.[VesselType], V.[PlaceOfRegistration], V.[CountryCode], V.[FlagShip], V.[CallSignNo], V.[LloydID], 
			V.[VesselClass], V.[OwnerCode], V.[OwnerAddress1], V.[OwnerAddress2], V.[OwnerAddress3], V.[PSACode], V.[GrossRegisteredTonnage], 
			V.[NetRegisteredTonnage], V.[OverallLength], V.[DeadWeight], V.[StandardDraft], V.[VesselCapacity], V.[Beam], V.[DisplacementWeight], 
			V.[IsActive], V.[CreatedBy], V.[CreatedOn], V.[ModifiedBy], V.[ModifiedOn] ,V.[YearBuilt],
			ISNULL(Onr.MerchantName,'') As OwnerName,
			ISNULL(Psa.MerchantName,'') As PSAName,
			ISNULL(C.CountryName,'') As CountryName,
			ISNULL(F.CountryName,'') As FlagShipName,
			ISNULL(VT.LookupDescription,'') As VesselTypeDescription,
			ISNULL(VC.LookupDescription,'') As VesselClassDescription
	FROM	[Master].[Vessel] V
	Left Outer Join Master.Merchant Onr ON
		V.OwnerCode = Convert(nvarchar(20),Onr.MerchantCode)
	Left Outer Join Master.Merchant Psa ON
		V.PSACode = Convert(nvarchar(20),Psa.MerchantCode)
	Left Outer Join Master.Country C ON 
		V.CountryCode = C.CountryCode
	Left Outer Join Master.Country F On 
		V.FlagShip = F.CountryCode
	Left Outer Join VesselTypeDescription VT ON 
		v.VesselType = VT.LookupID
	Left Outer Join VesselClassDescription VC ON 
		V.VesselClass = VC.LookupID
	ORDER BY V.VesselName
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_VesselRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Vessel] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_VesselRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselRecordCount] 
END 
GO
CREATE PROC [Master].[usp_VesselRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Vessel]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_VesselAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Vessel] Record based on [Vessel] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_VesselAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_VesselAutoCompleteSearch] 
    @VesselName NVARCHAR(50)
AS 

BEGIN

;with VesselTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselType'),
	VesselClassDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='VesselClass')
	SELECT	V.[VesselID], V.[VesselName], V.[VesselType], V.[PlaceOfRegistration], V.[CountryCode], V.[FlagShip], V.[CallSignNo], V.[LloydID], 
			V.[VesselClass], V.[OwnerCode], V.[OwnerAddress1], V.[OwnerAddress2], V.[OwnerAddress3], V.[PSACode], V.[GrossRegisteredTonnage], 
			V.[NetRegisteredTonnage], V.[OverallLength], V.[DeadWeight], V.[StandardDraft], V.[VesselCapacity], V.[Beam], V.[DisplacementWeight], 
			V.[IsActive], V.[CreatedBy], V.[CreatedOn], V.[ModifiedBy], V.[ModifiedOn] ,V.[YearBuilt],
			ISNULL(Onr.MerchantName,'') As OwnerName,
			ISNULL(Psa.MerchantName,'') As PSAName,
			ISNULL(C.CountryName,'') As CountryName,
			ISNULL(F.CountryName,'') As FlagShipName,
			ISNULL(VT.LookupDescription,'') As VesselTypeDescription,
			ISNULL(VC.LookupDescription,'') As VesselClassDescription
	FROM	[Master].[Vessel] V
	Left Outer Join Master.Merchant Onr ON
		V.OwnerCode = Convert(nvarchar(20),Onr.MerchantCode)
	Left Outer Join Master.Merchant Psa ON
		V.PSACode = Convert(nvarchar(20),Psa.MerchantCode)
	Left Outer Join Master.Country C ON 
		V.CountryCode = C.CountryCode
	Left Outer Join Master.Country F On 
		V.FlagShip = F.CountryCode
	Left Outer Join VesselTypeDescription VT ON 
		v.VesselType = VT.LookupID
	Left Outer Join VesselClassDescription VC ON 
		V.VesselClass = VC.LookupID	
	WHERE  [VesselName] LIKE '%' +  @VesselName + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_VesselInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Vessel] Record Into [Vessel] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_VesselInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselInsert] 
END 
GO
CREATE PROC [Master].[usp_VesselInsert] 
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VesselType smallint,
    @PlaceOfRegistration nvarchar(100),
    @CountryCode varchar(2),
    @FlagShip varchar(2),
    @CallSignNo nvarchar(20),
    @LloydID nvarchar(20),
    @VesselClass smallint,
    @OwnerCode nvarchar(20),
    @OwnerAddress1 nvarchar(100),
    @OwnerAddress2 nvarchar(100),
    @OwnerAddress3 nvarchar(100),
    @PSACode nvarchar(20),
    @GrossRegisteredTonnage float,
    @NetRegisteredTonnage float,
    @OverallLength float,
    @DeadWeight float,
    @StandardDraft float,
    @VesselCapacity float,
    @Beam smallint,
    @DisplacementWeight float,
	@YearBuilt smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[Vessel] (
			[VesselID], [VesselName], [VesselType], [PlaceOfRegistration], [CountryCode], [FlagShip], [CallSignNo], [LloydID], 
			[VesselClass], [OwnerCode], [OwnerAddress1], [OwnerAddress2], [OwnerAddress3], [PSACode], [GrossRegisteredTonnage], 
			[NetRegisteredTonnage], [OverallLength], [DeadWeight], [StandardDraft], [VesselCapacity], [Beam], [DisplacementWeight], 
			[YearBuilt],[IsActive], [CreatedBy], [CreatedOn])
	SELECT	@VesselID, @VesselName, @VesselType, @PlaceOfRegistration, @CountryCode, @FlagShip, @CallSignNo, @LloydID, 
			@VesselClass, @OwnerCode, @OwnerAddress1, @OwnerAddress2, @OwnerAddress3, @PSACode, @GrossRegisteredTonnage, 
			@NetRegisteredTonnage, @OverallLength, @DeadWeight, @StandardDraft, @VesselCapacity, @Beam, @DisplacementWeight, 
			@YearBuilt,Cast(1 as bit), @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_VesselUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Vessel] Record Into [Vessel] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_VesselUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselUpdate] 
END 
GO
CREATE PROC [Master].[usp_VesselUpdate] 
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VesselType smallint,
    @PlaceOfRegistration nvarchar(100),
    @CountryCode varchar(2),
    @FlagShip varchar(2),
    @CallSignNo nvarchar(20),
    @LloydID nvarchar(20),
    @VesselClass smallint,
    @OwnerCode nvarchar(20),
    @OwnerAddress1 nvarchar(100),
    @OwnerAddress2 nvarchar(100),
    @OwnerAddress3 nvarchar(100),
    @PSACode nvarchar(20),
    @GrossRegisteredTonnage float,
    @NetRegisteredTonnage float,
    @OverallLength float,
    @DeadWeight float,
    @StandardDraft float,
    @VesselCapacity float,
    @Beam smallint,
    @DisplacementWeight float,
	@YearBuilt smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Vessel]
	SET		[VesselName] = @VesselName, [VesselType] = @VesselType, [PlaceOfRegistration] = @PlaceOfRegistration, [CountryCode] = @CountryCode, 
			[FlagShip] = @FlagShip, [CallSignNo] = @CallSignNo, [LloydID] = @LloydID, [VesselClass] = @VesselClass, [OwnerCode] = @OwnerCode, 
			[OwnerAddress1] = @OwnerAddress1, [OwnerAddress2] = @OwnerAddress2, [OwnerAddress3] = @OwnerAddress3, [PSACode] = @PSACode, 
			[GrossRegisteredTonnage] = @GrossRegisteredTonnage, [NetRegisteredTonnage] = @NetRegisteredTonnage, [OverallLength] = @OverallLength, 
			[DeadWeight] = @DeadWeight, [StandardDraft] = @StandardDraft, [VesselCapacity] = @VesselCapacity, [Beam] = @Beam, 
			[DisplacementWeight] = @DisplacementWeight, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),YearBuilt =@YearBuilt
	WHERE	[VesselID] = @VesselID
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_VesselSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Vessel] Record Into [Vessel] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_VesselSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselSave] 
END 
GO
CREATE PROC [Master].[usp_VesselSave] 
    @VesselID nvarchar(20),
    @VesselName nvarchar(100),
    @VesselType smallint,
    @PlaceOfRegistration nvarchar(100),
    @CountryCode varchar(2),
    @FlagShip varchar(2),
    @CallSignNo nvarchar(20),
    @LloydID nvarchar(20),
    @VesselClass smallint,
    @OwnerCode nvarchar(20),
    @OwnerAddress1 nvarchar(100),
    @OwnerAddress2 nvarchar(100),
    @OwnerAddress3 nvarchar(100),
    @PSACode nvarchar(20),
    @GrossRegisteredTonnage float,
    @NetRegisteredTonnage float,
    @OverallLength float,
    @DeadWeight float,
    @StandardDraft float,
    @VesselCapacity float,
    @Beam smallint,
    @DisplacementWeight float,
	@YearBuilt smallint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Vessel] 
		WHERE 	[VesselID] = @VesselID)>0
	BEGIN
	    Exec [Master].[usp_VesselUpdate] 
			@VesselID, @VesselName, @VesselType, @PlaceOfRegistration, @CountryCode, @FlagShip, @CallSignNo, @LloydID, @VesselClass, @OwnerCode, 
			@OwnerAddress1, @OwnerAddress2, @OwnerAddress3, @PSACode, @GrossRegisteredTonnage, @NetRegisteredTonnage, @OverallLength, @DeadWeight, 
			@StandardDraft, @VesselCapacity, @Beam, @DisplacementWeight,@YearBuilt, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_VesselInsert] 
			@VesselID, @VesselName, @VesselType, @PlaceOfRegistration, @CountryCode, @FlagShip, @CallSignNo, @LloydID, @VesselClass, @OwnerCode, 
			@OwnerAddress1, @OwnerAddress2, @OwnerAddress3, @PSACode, @GrossRegisteredTonnage, @NetRegisteredTonnage, @OverallLength, @DeadWeight, 
			@StandardDraft, @VesselCapacity, @Beam, @DisplacementWeight,@YearBuilt, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[VesselSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_VesselDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Vessel] Record  based on [Vessel]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_VesselDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_VesselDelete] 
END 
GO
CREATE PROC [Master].[usp_VesselDelete] 
    @VesselID nvarchar(20)
AS 

	
BEGIN

	DELETE [Master].[Vessel]
	WHERE 	[VesselID] = @VesselID

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_VesselDelete]
-- ========================================================================================================================================

GO
 