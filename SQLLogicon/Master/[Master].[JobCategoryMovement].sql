


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryMovement] Record based on [JobCategoryMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementSelect] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementSelect] 
    @BranchID BIGINT,
    @Code NVARCHAR(15),
    @MovementCode NVARCHAR(50),
	@Module smallint,
    @SeqNo TINYINT
AS 
 

BEGIN

	SELECT [BranchID], [Code], [MovementCode],[Module], [SeqNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[JobCategoryMovement]
	WHERE  [BranchID] = @BranchID  
	       AND [Code] = @Code  
	       AND [MovementCode] = @MovementCode  
	       AND [SeqNo] = @SeqNo   

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryMovement] Records from [JobCategoryMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementList] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementList] 
    @BranchID BIGINT,
	@Code NVARCHAR(15)

AS 
 
BEGIN
	SELECT [BranchID], [Code], [MovementCode],Module, [SeqNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[JobCategoryMovement]
		WHERE  [BranchID] = @BranchID  
	       AND [Code] = @Code Order By [SeqNo] asc  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementList] 
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementListJobCategory]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryMovementJobCategory] Records from [JobCategoryMovementJobCategory] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryMovementListJobCategory]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementListJobCategory] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementListJobCategory] 
	@Code NVARCHAR(15)

AS 
 
BEGIN
	SELECT [BranchID], [Code], [MovementCode],Module, [SeqNo], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[JobCategoryMovement]
		WHERE  [Code] = @Code  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementList] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobCategoryMovement] Record Into [JobCategoryMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementInsert] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementInsert] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @SeqNo tinyint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN
	
	INSERT INTO [Master].[JobCategoryMovement] (
			[BranchID], [Code], [MovementCode],[Module], [SeqNo], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @Code, @MovementCode,@Module, @SeqNo, @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobCategoryMovement] Record Into [JobCategoryMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementUpdate] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @SeqNo tinyint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[JobCategoryMovement]
	SET    [BranchID] = @BranchID, [Code] = @Code, [MovementCode] = @MovementCode, [SeqNo] = @SeqNo, 
		   [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [Code] = @Code
	       AND [MovementCode] = @MovementCode
		   AND [Module] = @Module
	       AND [SeqNo] = @SeqNo
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobCategoryMovement] Record Into [JobCategoryMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementSave] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementSave] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @MovementCode nvarchar(50),
	@Module smallint,
    @SeqNo tinyint,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[JobCategoryMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code
	       AND [MovementCode] = @MovementCode
		   And Module = @Module
	       AND [SeqNo] = @SeqNo)>0
	BEGIN
	    Exec [Master].[usp_JobCategoryMovementUpdate] 
				@BranchID, @Code, @MovementCode,@Module, @SeqNo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobCategoryMovementInsert] 
				@BranchID, @Code, @MovementCode,@Module, @SeqNo, @CreatedBy, @ModifiedBy 


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobCategoryMovementSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobCategoryMovement] Record  based on [JobCategoryMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryMovementDelete] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryMovementDelete] 
    @BranchID BIGINT,
    @Code nvarchar(15) 
AS 

	
BEGIN

SET NOCOUNT ON;	

	DELETE
	FROM   [Master].[JobCategoryMovement]
	WHERE  [BranchID] = @BranchID
	       AND [Code] = @Code
	        
 
SET NOCOUNT OFF;	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryMovementDelete]
-- ========================================================================================================================================

GO

 
