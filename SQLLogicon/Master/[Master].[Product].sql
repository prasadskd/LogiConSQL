-- ========================================================================================================================================
-- START											 [Master].[usp_ProductSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Product] Record based on [Product] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProductSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductSelect] 
END 
GO
CREATE PROC [Master].[usp_ProductSelect] 
    @BranchID BIGINT,
    @PrincipalCode NVARCHAR(10),
    @ProductCode NVARCHAR(20)
AS 

BEGIN

	;With ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	ProductControlLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductControl')
	SELECT	Prd.[BranchID], Prd.[PrincipalCode], Prd.[ProductCode],Prd.[TariffCode], Prd.[Description] ,Prd.[ProductCategory], Prd.[CommodityCode], Prd.[BaseUOM], 
			Prd.[BaseVolume], Prd.[BaseWeight], Prd.[BaseWidth], Prd.[BaseHeight], Prd.[BaseDepth], Prd.[BaseSQFt], Prd.[UnitSize], Prd.[StorageUOM], 
			Prd.[IsConsignmentControl], Prd.[IsBatchNoControl], Prd.[IsSerialNoControl], Prd.[IsExpiryDateControl], Prd.[IsPONoControl], Prd.[IsInventoryItem], 
			Prd.[ReplenishRule], Prd.[ReplenishLevel], Prd.[ReplenishQty], Prd.[ShelfLife], Prd.[CountFrequency], Prd.[IsActive], Prd.[ConsignmentControlMode], 
			Prd.[BatchNoControlMode], Prd.[SerialNoControlMode], Prd.[ExpiryDateControlMode], Prd.[PONoControlMode], Prd.[BarCode], 
			Prd.[CreatedBy], Prd.[CreatedOn], Prd.[ModifiedBy], Prd.[ModifiedOn] ,
			ISNULL(Prd.PrincipalCode + ' - ' + Pcpl.MerchantName ,'') As PrincipalName,
			ISNULL(cmd.Description,'' ) As CommodityDescription,
			ISNULL(BU.UOMDescription,'') As BaseUOMDescription,
			ISNULL(SU.UOMDescription,'') As StorageUOMDescription,
			ISNULL(PrdCat.LookupDescription,'' ) As ProductCategoryDescription,
			ISNULL(CCM.LookupDescription,'') As ConsignmentControlDescription,
			ISNULL(BCM.LookupDescription,'') As BatchNoControlDescription,
			ISNULL(SLM.LookupDescription,'') As SerialNoControlDescription,
			ISNULL(EXM.LookupDescription,'') As ExpiryDateControlDescription,
			ISNULL(POM.LookupDescription,'') As PONoControlDescription,
			ISNULL(Prd.TariffCode + ' - ' + Hs.Description,'') As TariffCodeDescription
	FROM	[Master].[Product] Prd
	Left Outer Join Master.Merchant Pcpl ON 
		Prd.PrincipalCode = Pcpl.MerchantCode
	Left Outer Join Master.Commodity Cmd ON 
		Prd.CommodityCode = Cmd.CommodityCode
	Left Outer Join ProductCategoryLookup PrdCat ON 
		Prd.ProductCategory = PrdCat.LookupID
	Left Outer Join ProductControlLookup CCM ON 
		Prd.ConsignmentControlMode = CCM.LookupID
	Left Outer Join ProductControlLookup BCM ON 
		Prd.BatchNoControlMode = BCM.LookupID
	Left Outer Join ProductControlLookup SLM ON 
		Prd.SerialNoControlMode = SLM.LookupID
	Left Outer Join ProductControlLookup EXM ON 
		Prd.ExpiryDateControlMode = EXM.LookupID
	Left Outer Join ProductControlLookup POM ON 
		Prd.PONoControlMode = POM.LookupID
	Left Outer Join Master.UOM BU On 
		Prd.BaseUOM = BU.UOMCode
	Left Outer Join Master.UOM SU ON 
		Prd.StorageUOM = SU.UOMCode
	Left Outer Join Master.HSCode Hs ON 
		Prd.TariffCode = Hs.TariffCode
	WHERE	Prd.[BranchID] = @BranchID  
	       AND Prd.[PrincipalCode] = @PrincipalCode  
	       AND Prd.[ProductCode] = @ProductCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_ProductList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Product] Records from [Product] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProductList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductList] 
END 
GO
CREATE PROC [Master].[usp_ProductList] 
	@BranchID BIGINT,
    @PrincipalCode NVARCHAR(10)
    
AS 
BEGIN

	;With ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	ProductControlLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductControl')
	SELECT	Prd.[BranchID], Prd.[PrincipalCode], Prd.[ProductCode],Prd.[TariffCode], Prd.[Description] ,Prd.[ProductCategory], Prd.[CommodityCode], Prd.[BaseUOM], 
			Prd.[BaseVolume], Prd.[BaseWeight], Prd.[BaseWidth], Prd.[BaseHeight], Prd.[BaseDepth], Prd.[BaseSQFt], Prd.[UnitSize], Prd.[StorageUOM], 
			Prd.[IsConsignmentControl], Prd.[IsBatchNoControl], Prd.[IsSerialNoControl], Prd.[IsExpiryDateControl], Prd.[IsPONoControl], Prd.[IsInventoryItem], 
			Prd.[ReplenishRule], Prd.[ReplenishLevel], Prd.[ReplenishQty], Prd.[ShelfLife], Prd.[CountFrequency], Prd.[IsActive], Prd.[ConsignmentControlMode], 
			Prd.[BatchNoControlMode], Prd.[SerialNoControlMode], Prd.[ExpiryDateControlMode], Prd.[PONoControlMode], Prd.[BarCode], 
			Prd.[CreatedBy], Prd.[CreatedOn], Prd.[ModifiedBy], Prd.[ModifiedOn] ,
			ISNULL(Prd.PrincipalCode + ' - ' + Pcpl.MerchantName ,'') As PrincipalName,
			ISNULL(cmd.Description,'' ) As CommodityDescription,
			ISNULL(BU.UOMDescription,'') As BaseUOMDescription,
			ISNULL(SU.UOMDescription,'') As StorageUOMDescription,
			ISNULL(PrdCat.LookupDescription,'' ) As ProductCategoryDescription,
			ISNULL(CCM.LookupDescription,'') As ConsignmentControlDescription,
			ISNULL(BCM.LookupDescription,'') As BatchNoControlDescription,
			ISNULL(SLM.LookupDescription,'') As SerialNoControlDescription,
			ISNULL(EXM.LookupDescription,'') As ExpiryDateControlDescription,
			ISNULL(POM.LookupDescription,'') As PONoControlDescription,
			ISNULL(Prd.TariffCode + ' - ' + Hs.Description,'') As TariffCodeDescription
	FROM	[Master].[Product] Prd
	Left Outer Join Master.Merchant Pcpl ON 
		Prd.PrincipalCode = Pcpl.MerchantCode
	Left Outer Join Master.Commodity Cmd ON 
		Prd.CommodityCode = Cmd.CommodityCode
	Left Outer Join ProductCategoryLookup PrdCat ON 
		Prd.ProductCategory = PrdCat.LookupID
	Left Outer Join ProductControlLookup CCM ON 
		Prd.ConsignmentControlMode = CCM.LookupID
	Left Outer Join ProductControlLookup BCM ON 
		Prd.BatchNoControlMode = BCM.LookupID
	Left Outer Join ProductControlLookup SLM ON 
		Prd.SerialNoControlMode = SLM.LookupID
	Left Outer Join ProductControlLookup EXM ON 
		Prd.ExpiryDateControlMode = EXM.LookupID
	Left Outer Join ProductControlLookup POM ON 
		Prd.PONoControlMode = POM.LookupID
	Left Outer Join Master.UOM BU On 
		Prd.BaseUOM = BU.UOMCode
	Left Outer Join Master.UOM SU ON 
		Prd.StorageUOM = SU.UOMCode
	Left Outer Join Master.HSCode Hs ON 
		Prd.TariffCode = Hs.TariffCode
	WHERE	Prd.[BranchID] = @BranchID  
	       AND Prd.[PrincipalCode] = @PrincipalCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ProductPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Product] Records from [Product] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProductPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductPageView] 
END 
GO
CREATE PROC [Master].[usp_ProductPageView] 
	@BranchID BIGINT,
    @PrincipalCode NVARCHAR(10),
    @fetchrows bigint
AS 
BEGIN

	;With ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	ProductControlLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductControl')
	SELECT	Prd.[BranchID], Prd.[PrincipalCode], Prd.[ProductCode],Prd.[TariffCode], Prd.[Description] ,Prd.[ProductCategory], Prd.[CommodityCode], Prd.[BaseUOM], 
			Prd.[BaseVolume], Prd.[BaseWeight], Prd.[BaseWidth], Prd.[BaseHeight], Prd.[BaseDepth], Prd.[BaseSQFt], Prd.[UnitSize], Prd.[StorageUOM], 
			Prd.[IsConsignmentControl], Prd.[IsBatchNoControl], Prd.[IsSerialNoControl], Prd.[IsExpiryDateControl], Prd.[IsPONoControl], Prd.[IsInventoryItem], 
			Prd.[ReplenishRule], Prd.[ReplenishLevel], Prd.[ReplenishQty], Prd.[ShelfLife], Prd.[CountFrequency], Prd.[IsActive], Prd.[ConsignmentControlMode], 
			Prd.[BatchNoControlMode], Prd.[SerialNoControlMode], Prd.[ExpiryDateControlMode], Prd.[PONoControlMode], Prd.[BarCode], 
			Prd.[CreatedBy], Prd.[CreatedOn], Prd.[ModifiedBy], Prd.[ModifiedOn] ,
			ISNULL(Prd.PrincipalCode + ' - ' + Pcpl.MerchantName ,'') As PrincipalName,
			ISNULL(cmd.Description,'' ) As CommodityDescription,
			ISNULL(BU.UOMDescription,'') As BaseUOMDescription,
			ISNULL(SU.UOMDescription,'') As StorageUOMDescription,
			ISNULL(PrdCat.LookupDescription,'' ) As ProductCategoryDescription,
			ISNULL(CCM.LookupDescription,'') As ConsignmentControlDescription,
			ISNULL(BCM.LookupDescription,'') As BatchNoControlDescription,
			ISNULL(SLM.LookupDescription,'') As SerialNoControlDescription,
			ISNULL(EXM.LookupDescription,'') As ExpiryDateControlDescription,
			ISNULL(POM.LookupDescription,'') As PONoControlDescription,
			ISNULL(Prd.TariffCode + ' - ' + Hs.Description,'') As TariffCodeDescription
	FROM	[Master].[Product] Prd
	Left Outer Join Master.Merchant Pcpl ON 
		Prd.PrincipalCode = Pcpl.MerchantCode
	Left Outer Join Master.Commodity Cmd ON 
		Prd.CommodityCode = Cmd.CommodityCode
	Left Outer Join ProductCategoryLookup PrdCat ON 
		Prd.ProductCategory = PrdCat.LookupID
	Left Outer Join ProductControlLookup CCM ON 
		Prd.ConsignmentControlMode = CCM.LookupID
	Left Outer Join ProductControlLookup BCM ON 
		Prd.BatchNoControlMode = BCM.LookupID
	Left Outer Join ProductControlLookup SLM ON 
		Prd.SerialNoControlMode = SLM.LookupID
	Left Outer Join ProductControlLookup EXM ON 
		Prd.ExpiryDateControlMode = EXM.LookupID
	Left Outer Join ProductControlLookup POM ON 
		Prd.PONoControlMode = POM.LookupID
	Left Outer Join Master.UOM BU On 
		Prd.BaseUOM = BU.UOMCode
	Left Outer Join Master.UOM SU ON 
		Prd.StorageUOM = SU.UOMCode
	Left Outer Join Master.HSCode Hs ON 
		Prd.TariffCode = Hs.TariffCode
	WHERE	Prd.[BranchID] = @BranchID  
	       AND Prd.[PrincipalCode] = @PrincipalCode  
	ORDER BY Prd.[ProductCode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProductRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Product] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProductRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductRecordCount] 
END 
GO
CREATE PROC [Master].[usp_ProductRecordCount] 
	@BranchID BIGINT,
    @PrincipalCode NVARCHAR(10)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Product] Prd
	WHERE	Prd.[BranchID] = @BranchID  
	       AND Prd.[PrincipalCode] = @PrincipalCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProductAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Product] Record based on [Product] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_ProductAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_ProductAutoCompleteSearch] 
    @BranchID BIGINT,
    @PrincipalCode NVARCHAR(10),
    @Description NVARCHAR(50)
AS 

BEGIN

	;With ProductCategoryLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductCategory'),
	ProductControlLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ProductControl')
	SELECT	Prd.[BranchID], Prd.[PrincipalCode], Prd.[ProductCode],Prd.[TariffCode], Prd.[Description] ,Prd.[ProductCategory], Prd.[CommodityCode], Prd.[BaseUOM], 
			Prd.[BaseVolume], Prd.[BaseWeight], Prd.[BaseWidth], Prd.[BaseHeight], Prd.[BaseDepth], Prd.[BaseSQFt], Prd.[UnitSize], Prd.[StorageUOM], 
			Prd.[IsConsignmentControl], Prd.[IsBatchNoControl], Prd.[IsSerialNoControl], Prd.[IsExpiryDateControl], Prd.[IsPONoControl], Prd.[IsInventoryItem], 
			Prd.[ReplenishRule], Prd.[ReplenishLevel], Prd.[ReplenishQty], Prd.[ShelfLife], Prd.[CountFrequency], Prd.[IsActive], Prd.[ConsignmentControlMode], 
			Prd.[BatchNoControlMode], Prd.[SerialNoControlMode], Prd.[ExpiryDateControlMode], Prd.[PONoControlMode], Prd.[BarCode], 
			Prd.[CreatedBy], Prd.[CreatedOn], Prd.[ModifiedBy], Prd.[ModifiedOn] ,
			ISNULL(Prd.PrincipalCode + ' - ' + Pcpl.MerchantName ,'') As PrincipalName,
			ISNULL(cmd.Description,'' ) As CommodityDescription,
			ISNULL(BU.UOMDescription,'') As BaseUOMDescription,
			ISNULL(SU.UOMDescription,'') As StorageUOMDescription,
			ISNULL(PrdCat.LookupDescription,'' ) As ProductCategoryDescription,
			ISNULL(CCM.LookupDescription,'') As ConsignmentControlDescription,
			ISNULL(BCM.LookupDescription,'') As BatchNoControlDescription,
			ISNULL(SLM.LookupDescription,'') As SerialNoControlDescription,
			ISNULL(EXM.LookupDescription,'') As ExpiryDateControlDescription,
			ISNULL(POM.LookupDescription,'') As PONoControlDescription,
			ISNULL(Prd.TariffCode + ' - ' + Hs.Description,'') As TariffCodeDescription
	FROM	[Master].[Product] Prd
	Left Outer Join Master.Merchant Pcpl ON 
		Prd.PrincipalCode = Pcpl.MerchantCode
	Left Outer Join Master.Commodity Cmd ON 
		Prd.CommodityCode = Cmd.CommodityCode
	Left Outer Join ProductCategoryLookup PrdCat ON 
		Prd.ProductCategory = PrdCat.LookupID
	Left Outer Join ProductControlLookup CCM ON 
		Prd.ConsignmentControlMode = CCM.LookupID
	Left Outer Join ProductControlLookup BCM ON 
		Prd.BatchNoControlMode = BCM.LookupID
	Left Outer Join ProductControlLookup SLM ON 
		Prd.SerialNoControlMode = SLM.LookupID
	Left Outer Join ProductControlLookup EXM ON 
		Prd.ExpiryDateControlMode = EXM.LookupID
	Left Outer Join ProductControlLookup POM ON 
		Prd.PONoControlMode = POM.LookupID
	Left Outer Join Master.UOM BU On 
		Prd.BaseUOM = BU.UOMCode
	Left Outer Join Master.UOM SU ON 
		Prd.StorageUOM = SU.UOMCode
	Left Outer Join Master.HSCode Hs ON 
		Prd.TariffCode = Hs.TariffCode
	WHERE	Prd.[BranchID] = @BranchID  
	       AND Prd.[PrincipalCode] = @PrincipalCode   
	       AND Prd.[Description] LIKE '%' +  @Description + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductAutoCompleteSearch]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_ProductInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Product] Record Into [Product] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProductInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductInsert] 
END 
GO
CREATE PROC [Master].[usp_ProductInsert] 
    @BranchID BIGINT,
    @PrincipalCode nvarchar(10),
    @ProductCode nvarchar(20),
    @Description nvarchar(50),
	@TariffCode nvarchar(20),
    @ProductCategory smallint,
    @CommodityCode nvarchar(10),
    @BaseUOM nvarchar(20),
    @BaseVolume float,
    @BaseWeight float,
    @BaseWidth float,
    @BaseHeight float,
    @BaseDepth float,
    @BaseSQFt float,
    @UnitSize int,
    @StorageUOM nvarchar(20),
    @IsConsignmentControl bit,
    @IsBatchNoControl bit,
    @IsSerialNoControl bit,
    @IsExpiryDateControl bit,
    @IsPONoControl bit,
    @IsInventoryItem bit,
    @ReplenishRule smallint,
    @ReplenishLevel int,
    @ReplenishQty int,
    @ShelfLife int,
    @CountFrequency int,
    @ConsignmentControlMode smallint,
    @BatchNoControlMode smallint,
    @SerialNoControlMode smallint,
    @ExpiryDateControlMode smallint,
    @PONoControlMode smallint,
    @BarCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[Product] (
			[BranchID], [PrincipalCode], [ProductCode], [Description],TariffCode, [ProductCategory], [CommodityCode], [BaseUOM], [BaseVolume], 
			[BaseWeight], [BaseWidth], [BaseHeight], [BaseDepth], [BaseSQFt], [UnitSize], [StorageUOM], [IsConsignmentControl], 
			[IsBatchNoControl], [IsSerialNoControl], [IsExpiryDateControl], [IsPONoControl], [IsInventoryItem], [ReplenishRule], 
			[ReplenishLevel], [ReplenishQty], [ShelfLife], [CountFrequency], [IsActive], [ConsignmentControlMode], 
			[BatchNoControlMode], [SerialNoControlMode], [ExpiryDateControlMode], [PONoControlMode], [BarCode], 
			[CreatedBy], [CreatedOn])
	SELECT	@BranchID, @PrincipalCode, @ProductCode, @Description,@TariffCode, @ProductCategory, @CommodityCode, @BaseUOM, @BaseVolume, 
			@BaseWeight, @BaseWidth, @BaseHeight, @BaseDepth, @BaseSQFt, @UnitSize, @StorageUOM, @IsConsignmentControl, @IsBatchNoControl, 
			@IsSerialNoControl, @IsExpiryDateControl, @IsPONoControl, @IsInventoryItem, @ReplenishRule, 
			@ReplenishLevel, @ReplenishQty, @ShelfLife, @CountFrequency, Cast(1 as bit), @ConsignmentControlMode, 
			@BatchNoControlMode, @SerialNoControlMode, @ExpiryDateControlMode, @PONoControlMode, @BarCode, 
			@CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ProductUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Product] Record Into [Product] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProductUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductUpdate] 
END 
GO
CREATE PROC [Master].[usp_ProductUpdate] 
    @BranchID BIGINT,
    @PrincipalCode nvarchar(10),
    @ProductCode nvarchar(20),
    @Description nvarchar(50),
	@TariffCode nvarchar(20),
    @ProductCategory smallint,
    @CommodityCode nvarchar(10),
    @BaseUOM nvarchar(20),
    @BaseVolume float,
    @BaseWeight float,
    @BaseWidth float,
    @BaseHeight float,
    @BaseDepth float,
    @BaseSQFt float,
    @UnitSize int,
    @StorageUOM nvarchar(20),
    @IsConsignmentControl bit,
    @IsBatchNoControl bit,
    @IsSerialNoControl bit,
    @IsExpiryDateControl bit,
    @IsPONoControl bit,
    @IsInventoryItem bit,
    @ReplenishRule smallint,
    @ReplenishLevel int,
    @ReplenishQty int,
    @ShelfLife int,
    @CountFrequency int,
    @ConsignmentControlMode smallint,
    @BatchNoControlMode smallint,
    @SerialNoControlMode smallint,
    @ExpiryDateControlMode smallint,
    @PONoControlMode smallint,
    @BarCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Product]
	SET		[Description] = @Description, [ProductCategory] = @ProductCategory, [CommodityCode] = @CommodityCode, [BaseUOM] = @BaseUOM, 
			[BaseVolume] = @BaseVolume, [BaseWeight] = @BaseWeight, [BaseWidth] = @BaseWidth, [BaseHeight] = @BaseHeight, [BaseDepth] = @BaseDepth, 
			[BaseSQFt] = @BaseSQFt, [UnitSize] = @UnitSize, [StorageUOM] = @StorageUOM, [IsConsignmentControl] = @IsConsignmentControl, 
			[IsBatchNoControl] = @IsBatchNoControl, [IsSerialNoControl] = @IsSerialNoControl, [IsExpiryDateControl] = @IsExpiryDateControl, 
			[IsPONoControl] = @IsPONoControl, [IsInventoryItem] = @IsInventoryItem, [ReplenishRule] = @ReplenishRule, 
			[ReplenishLevel] = @ReplenishLevel, [ReplenishQty] = @ReplenishQty, [ShelfLife] = @ShelfLife, 
			[CountFrequency] = @CountFrequency, [ConsignmentControlMode] = @ConsignmentControlMode, [BatchNoControlMode] = @BatchNoControlMode, 
			[SerialNoControlMode] = @SerialNoControlMode, [ExpiryDateControlMode] = @ExpiryDateControlMode, [PONoControlMode] = @PONoControlMode, 
			[BarCode] = @BarCode, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),
			TariffCode=@TariffCode
	WHERE	[BranchID] = @BranchID
			AND [PrincipalCode] = @PrincipalCode
			AND [ProductCode] = @ProductCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_ProductSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Product] Record Into [Product] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProductSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductSave] 
END 
GO
CREATE PROC [Master].[usp_ProductSave] 
    @BranchID BIGINT,
    @PrincipalCode nvarchar(10),
    @ProductCode nvarchar(20),
    @Description nvarchar(50),
	@TariffCode nvarchar(20),
    @ProductCategory smallint,
    @CommodityCode nvarchar(10),
    @BaseUOM nvarchar(20),
    @BaseVolume float,
    @BaseWeight float,
    @BaseWidth float,
    @BaseHeight float,
    @BaseDepth float,
    @BaseSQFt float,
    @UnitSize int,
    @StorageUOM nvarchar(20),
    @IsConsignmentControl bit,
    @IsBatchNoControl bit,
    @IsSerialNoControl bit,
    @IsExpiryDateControl bit,
    @IsPONoControl bit,
    @IsInventoryItem bit,
    @ReplenishRule smallint,
    @ReplenishLevel int,
    @ReplenishQty int,
    @ShelfLife int,
    @CountFrequency int,
    @ConsignmentControlMode smallint,
    @BatchNoControlMode smallint,
    @SerialNoControlMode smallint,
    @ExpiryDateControlMode smallint,
    @PONoControlMode smallint,
    @BarCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Product] 
		WHERE 	[BranchID] = @BranchID
	       AND [PrincipalCode] = @PrincipalCode
	       AND [ProductCode] = @ProductCode)>0
	BEGIN
	    Exec [Master].[usp_ProductUpdate] 
			@BranchID, @PrincipalCode, @ProductCode, @Description,@TariffCode, @ProductCategory, @CommodityCode, @BaseUOM, @BaseVolume, 
			@BaseWeight, @BaseWidth, @BaseHeight, @BaseDepth, @BaseSQFt, @UnitSize, @StorageUOM, @IsConsignmentControl, 
			@IsBatchNoControl, @IsSerialNoControl, @IsExpiryDateControl, @IsPONoControl, @IsInventoryItem, @ReplenishRule, 
			@ReplenishLevel, @ReplenishQty, @ShelfLife, @CountFrequency, @ConsignmentControlMode, @BatchNoControlMode, 
			@SerialNoControlMode, @ExpiryDateControlMode, @PONoControlMode, @BarCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_ProductInsert] 
			@BranchID, @PrincipalCode, @ProductCode, @Description,@TariffCode, @ProductCategory, @CommodityCode, @BaseUOM, @BaseVolume, 
			@BaseWeight, @BaseWidth, @BaseHeight, @BaseDepth, @BaseSQFt, @UnitSize, @StorageUOM, @IsConsignmentControl, 
			@IsBatchNoControl, @IsSerialNoControl, @IsExpiryDateControl, @IsPONoControl, @IsInventoryItem, @ReplenishRule, 
			@ReplenishLevel, @ReplenishQty, @ShelfLife, @CountFrequency, @ConsignmentControlMode, @BatchNoControlMode, 
			@SerialNoControlMode, @ExpiryDateControlMode, @PONoControlMode, @BarCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[ProductSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_ProductDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Product] Record  based on [Product]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_ProductDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_ProductDelete] 
END 
GO
CREATE PROC [Master].[usp_ProductDelete] 
    @BranchID BIGINT,
    @PrincipalCode nvarchar(10),
    @ProductCode nvarchar(20)
AS 

	
BEGIN

	UPDATE	[Master].[Product]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [PrincipalCode] = @PrincipalCode
	       AND [ProductCode] = @ProductCode

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_ProductDelete]
-- ========================================================================================================================================

GO
 