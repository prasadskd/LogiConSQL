USE [Logicon];
GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [AssociationDetail] Record based on [AssociationDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailSelect] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailSelect] 
    @AssociationID SMALLINT,
    @MerchantCode BIGINT
AS 

BEGIN

	SELECT [AssociationID], [MerchantCode], [JoinDate] 
	FROM   [Master].[AssociationDetail]
	WHERE  [AssociationID] = @AssociationID  
	       AND [MerchantCode] = @MerchantCode 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [AssociationDetail] Records from [AssociationDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_AssociationDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailList] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailList] 
    @AssociationID SMALLINT

AS 
BEGIN

	SELECT [AssociationID], [MerchantCode], [JoinDate] 
	FROM   [Master].[AssociationDetail]
	WHERE  [AssociationID] = @AssociationID  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationDetailList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [AssociationDetail] Record Into [AssociationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailInsert] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailInsert] 
    @AssociationID smallint,
    @MerchantCode bigint,
    @JoinDate datetime
AS 
  

BEGIN

	
	INSERT INTO [Master].[AssociationDetail] ([AssociationID], [MerchantCode], [JoinDate])
	SELECT @AssociationID, @MerchantCode, @JoinDate
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationDetailInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [AssociationDetail] Record Into [AssociationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailUpdate] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailUpdate] 
    @AssociationID smallint,
    @MerchantCode bigint,
    @JoinDate datetime
AS 
 
	
BEGIN

	UPDATE [Master].[AssociationDetail]
	SET    [AssociationID] = @AssociationID, [MerchantCode] = @MerchantCode, [JoinDate] = @JoinDate
	WHERE  [AssociationID] = @AssociationID
	       AND [MerchantCode] = @MerchantCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationDetailUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [AssociationDetail] Record Into [AssociationDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailSave] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailSave] 
    @AssociationID smallint,
    @MerchantCode bigint,
    @JoinDate datetime
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[AssociationDetail] 
		WHERE 	[AssociationID] = @AssociationID
	       AND [MerchantCode] = @MerchantCode)>0
	BEGIN
	    Exec [Master].[usp_AssociationDetailUpdate] 
		@AssociationID, @MerchantCode, @JoinDate


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_AssociationDetailInsert] 
		@AssociationID, @MerchantCode, @JoinDate


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[AssociationDetailSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_AssociationDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [AssociationDetail] Record  based on [AssociationDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_AssociationDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_AssociationDetailDelete] 
END 
GO
CREATE PROC [Master].[usp_AssociationDetailDelete] 
    @AssociationID smallint 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Master].[AssociationDetail]
	WHERE  [AssociationID] = @AssociationID
 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_AssociationDetailDelete]
-- ========================================================================================================================================

GO
 