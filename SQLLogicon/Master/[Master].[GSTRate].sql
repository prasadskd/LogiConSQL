
-- ========================================================================================================================================
-- START											 [Master].[usp_GSTRateSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [GSTRate] Record based on [GSTRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_GSTRateSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateSelect] 
END 
GO
CREATE PROC [Master].[usp_GSTRateSelect] 
    @BranchID BIGINT,
    @GSTCode NVARCHAR(10)
AS 

BEGIN

	SELECT [BranchID], [GSTCode], [Description], [Rate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[GSTRate]
	WHERE   [BranchID] = @BranchID 
	       AND  [GSTCode] = @GSTCode 
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_GSTRateSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_GSTRateList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [GSTRate] Records from [GSTRate] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_GSTRateList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateList] 
END 
GO
CREATE PROC [Master].[usp_GSTRateList] 
	@BranchID BIGINT
AS 
BEGIN

	SELECT [BranchID], [GSTCode], [Description], [Rate], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[GSTRate]
	WHERE   [BranchID] = @BranchID 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_GSTRateList] 
-- ========================================================================================================================================

GO
--========================================================================================================================================
-- START											 [Master].[usp_GSTRateInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [GSTRate] Record Into [GSTRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_GSTRateInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateInsert] 
END 
GO
CREATE PROC [Master].[usp_GSTRateInsert] 
    @BranchID BIGINT,
    @GSTCode nvarchar(10),
    @Description nvarchar(250),
    @Rate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[GSTRate] ([BranchID], [GSTCode], [Description], [Rate], [CreatedBy], [CreatedOn])
	SELECT @BranchID, @GSTCode, @Description, @Rate, @CreatedBy, GETUTCDATE() 
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_GSTRateInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_GSTRateUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [GSTRate] Record Into [GSTRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_GSTRateUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateUpdate] 
END 
GO
CREATE PROC [Master].[usp_GSTRateUpdate] 
    @BranchID BIGINT,
    @GSTCode nvarchar(10),
    @Description nvarchar(250),
    @Rate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[GSTRate]
	SET    [Description] = @Description, [Rate] = @Rate, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [GSTCode] = @GSTCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_GSTRateUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_GSTRateSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [GSTRate] Record Into [GSTRate] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_GSTRateSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateSave] 
END 
GO
CREATE PROC [Master].[usp_GSTRateSave] 
    @BranchID BIGINT,
    @GSTCode nvarchar(10),
    @Description nvarchar(250),
    @Rate numeric(18, 2),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[GSTRate] 
		WHERE 	[BranchID] = @BranchID
	       AND [GSTCode] = @GSTCode)>0
	BEGIN
	    Exec [Master].[usp_GSTRateUpdate] 
		@BranchID, @GSTCode, @Description, @Rate, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_GSTRateInsert] 
		@BranchID, @GSTCode, @Description, @Rate, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[GSTRateSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_GSTRateDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [GSTRate] Record  based on [GSTRate]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_GSTRateDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_GSTRateDelete] 
END 
GO
CREATE PROC [Master].[usp_GSTRateDelete] 
    @BranchID BIGINT,
    @GSTCode nvarchar(10)
AS 

	
BEGIN

	 
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[GSTRate]
	WHERE  [BranchID] = @BranchID
	       AND [GSTCode] = @GSTCode
	 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_GSTRateDelete]
-- ========================================================================================================================================

GO
 