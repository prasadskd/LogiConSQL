
-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [HoldStatus] Record based on [HoldStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HoldStatusSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusSelect] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusSelect] 
    @Code NVARCHAR(50)
AS 

BEGIN

	SELECT [Code], [Description], [IsOrder], [IsContainer], [IsCargo], [MappingCode], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[HoldStatus]
	WHERE  [Code] = @Code  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [HoldStatus] Records from [HoldStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HoldStatusList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusList] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusList] 

AS 
BEGIN

	SELECT [Code], [Description], [IsOrder], [IsContainer], [IsCargo], [MappingCode], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[HoldStatus]

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [HoldStatus] Records from [HoldStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HoldStatusPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusPageView] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusPageView] 
	@fetchrows bigint
AS 
BEGIN

	
		SELECT [Code], [Description], [IsOrder], [IsContainer], [IsCargo], [MappingCode], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
		FROM   [Master].[HoldStatus]
		ORDER BY [Code] 
		OFFSET @fetchrows ROWS 
		FETCH NEXT 10 ROWS ONLY
	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [HoldStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HoldStatusRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusRecordCount] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusRecordCount] 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[HoldStatus]
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [HoldStatus] Record based on [HoldStatus] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HoldStatusAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusAutoCompleteSearch] 
    @Code NVARCHAR(50)
AS 

BEGIN

	SELECT [Code], [Description], [IsOrder], [IsContainer], [IsCargo], [MappingCode], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Master].[HoldStatus]
	WHERE  [Code] LIKE '%' +  @Code + '%'
	And [IsActive]=cast(1 as bit)
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [HoldStatus] Record Into [HoldStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HoldStatusInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusInsert] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusInsert] 
    @Code nvarchar(50),
    @Description nvarchar(100),
    @IsOrder bit,
    @IsContainer bit,
    @IsCargo bit,
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[HoldStatus] (
			[Code], [Description], [IsOrder], [IsContainer], [IsCargo], [MappingCode], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@Code, @Description, @IsOrder, @IsContainer, @IsCargo, @MappingCode, Cast(1 as bit), @CreatedBy, getutcdate()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [HoldStatus] Record Into [HoldStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HoldStatusUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusUpdate] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusUpdate] 
    @Code nvarchar(50),
    @Description nvarchar(100),
    @IsOrder bit,
    @IsContainer bit,
    @IsCargo bit,
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 
	
BEGIN

	UPDATE	[Master].[HoldStatus]
	SET		[Description] = @Description, [IsOrder] = @IsOrder, [IsContainer] = @IsContainer, [IsCargo] = @IsCargo, 
			[MappingCode] = @MappingCode, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[Code] = @Code
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [HoldStatus] Record Into [HoldStatus] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HoldStatusSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusSave] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusSave] 
    @Code nvarchar(50),
    @Description nvarchar(100),
    @IsOrder bit,
    @IsContainer bit,
    @IsCargo bit,
    @MappingCode nvarchar(50),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[HoldStatus] 
		WHERE 	[Code] = @Code)>0
	BEGIN
	    Exec [Master].[usp_HoldStatusUpdate] 
		@Code, @Description, @IsOrder, @IsContainer, @IsCargo, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_HoldStatusInsert] 
		@Code, @Description, @IsOrder, @IsContainer, @IsCargo, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[HoldStatusSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_HoldStatusDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [HoldStatus] Record  based on [HoldStatus]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_HoldStatusDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HoldStatusDelete] 
END 
GO
CREATE PROC [Master].[usp_HoldStatusDelete] 
    @Code nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Master].[HoldStatus]
	SET	[IsActive] = CAST(0 as bit)
	WHERE 	[Code] = @Code


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_HoldStatusDelete]
-- ========================================================================================================================================

GO

 
