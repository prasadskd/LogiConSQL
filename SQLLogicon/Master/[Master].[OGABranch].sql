

-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OGABranch] Record based on [OGABranch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OGABranchSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchSelect] 
END 
GO
CREATE PROC [Master].[usp_OGABranchSelect] 
    @OGABranchCode NVARCHAR(10),
    @OGACode SMALLINT,
    @CountryCode VARCHAR(2)
AS 

BEGIN
	select * from [Master].[OGABranch]

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory ='OGACode')
	SELECT	Ob.[OGABranchCode], Ob.[OGACode], Ob.[CountryCode], Ob.[Description], Ob.[IsActive], 
			Ob.[CreatedBy], Ob.[CreatedOn], Ob.[ModifiedBy], Ob.[ModifiedOn],
			ISNULL(Ogd.LookupDescription,'') As OGADescription,
			ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[OGABranch] Ob
	Left Outer Join OGADescription Ogd ON
		Ob.OGACode = Ogd.LookupID
	Left Outer Join Master.Country C ON
		Ob.CountryCode = C.CountryCode
	WHERE  Ob.[OGABranchCode] = @OGABranchCode  
	       AND Ob.[OGACode] = @OGACode  
	       AND Ob.[CountryCode] = @CountryCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OGABranch] Records from [OGABranch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OGABranchList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchList] 
END 
GO
CREATE PROC [Master].[usp_OGABranchList] 
    @CountryCode VARCHAR(2)

AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory ='OGACode')
	SELECT	Ob.[OGABranchCode], Ob.[OGACode], Ob.[CountryCode], Ob.[Description], Ob.[IsActive], 
			Ob.[CreatedBy], Ob.[CreatedOn], Ob.[ModifiedBy], Ob.[ModifiedOn],
			ISNULL(Ogd.LookupDescription,'') As OGADescription,
			ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[OGABranch] Ob
	Left Outer Join OGADescription Ogd ON
		Ob.OGACode = Ogd.LookupID
	Left Outer Join Master.Country C ON
		Ob.CountryCode = C.CountryCode
	WHERE   Ob.[CountryCode] = @CountryCode  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [OGABranch] Records from [OGABranch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OGABranchPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchPageView] 
END 
GO
CREATE PROC [Master].[usp_OGABranchPageView]
    @CountryCode VARCHAR(2),
	@fetchrows bigint
AS 
BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory ='OGACode')
	SELECT	Ob.[OGABranchCode], Ob.[OGACode], Ob.[CountryCode], Ob.[Description], Ob.[IsActive], 
			Ob.[CreatedBy], Ob.[CreatedOn], Ob.[ModifiedBy], Ob.[ModifiedOn],
			ISNULL(Ogd.LookupDescription,'') As OGADescription,
			ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[OGABranch] Ob
	Left Outer Join OGADescription Ogd ON
		Ob.OGACode = Ogd.LookupID
	Left Outer Join Master.Country C ON
		Ob.CountryCode = C.CountryCode
	WHERE   Ob.[CountryCode] = @CountryCode  
	ORDER BY [OGACode]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [OGABranch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OGABranchRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchRecordCount] 
END 
GO
CREATE PROC [Master].[usp_OGABranchRecordCount] 
	    @CountryCode VARCHAR(2)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[OGABranch]
	WHERE   [CountryCode] = @CountryCode  


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [OGABranch] Record based on [OGABranch] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_OGABranchAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_OGABranchAutoCompleteSearch] 
    @CountryCode VARCHAR(2),
	@Description nvarchar(100)
AS 

BEGIN

	;with OGADescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory ='OGACode')
	SELECT	Ob.[OGABranchCode], Ob.[OGACode], Ob.[CountryCode], Ob.[Description], Ob.[IsActive], 
			Ob.[CreatedBy], Ob.[CreatedOn], Ob.[ModifiedBy], Ob.[ModifiedOn],
			ISNULL(Ogd.LookupDescription,'') As OGADescription,
			ISNULL(C.CountryName,'') As CountryName
	FROM   [Master].[OGABranch] Ob
	Left Outer Join OGADescription Ogd ON
		Ob.OGACode = Ogd.LookupID
	Left Outer Join Master.Country C ON
		Ob.CountryCode = C.CountryCode
	WHERE  Ob.[CountryCode] = @CountryCode  
			And Ob.[Description] LIKE '%' + @Description + '%'	
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [OGABranch] Record Into [OGABranch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OGABranchInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchInsert] 
END 
GO
CREATE PROC [Master].[usp_OGABranchInsert] 
    @OGABranchCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
  

BEGIN

	
	INSERT INTO [Master].[OGABranch] (
			[OGABranchCode], [OGACode], [CountryCode], [Description], [IsActive], [CreatedBy], [CreatedOn])
	SELECT	@OGABranchCode, @OGACode, @CountryCode, @Description, Cast(1 as bit), @CreatedBy, getutcDate()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [OGABranch] Record Into [OGABranch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OGABranchUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchUpdate] 
END 
GO
CREATE PROC [Master].[usp_OGABranchUpdate] 
    @OGABranchCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)
AS 
 
	
BEGIN

	UPDATE [Master].[OGABranch]
	SET    [Description] = @Description, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GetUtcDate()
	WHERE  [OGABranchCode] = @OGABranchCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchUpdate]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [OGABranch] Record Into [OGABranch] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OGABranchSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchSave] 
END 
GO
CREATE PROC [Master].[usp_OGABranchSave] 
    @OGABranchCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
    @Description nvarchar(100),
    @CreatedBy nvarchar(60),
    @ModifiedBy nvarchar(60)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[OGABranch] 
		WHERE 	[OGABranchCode] = @OGABranchCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_OGABranchUpdate] 
		@OGABranchCode, @OGACode, @CountryCode, @Description, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_OGABranchInsert] 
		@OGABranchCode, @OGACode, @CountryCode, @Description, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[OGABranchSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_OGABranchDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [OGABranch] Record  based on [OGABranch]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_OGABranchDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_OGABranchDelete] 
END 
GO
CREATE PROC [Master].[usp_OGABranchDelete] 
    @OGABranchCode nvarchar(10),
    @OGACode smallint,
    @CountryCode varchar(2),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Master].[OGABranch]
	SET	[IsActive] = CAST(0 as bit),ModifiedBy = @ModifiedBy,ModifiedOn = GetUTCDate()
	WHERE 	[OGABranchCode] = @OGABranchCode
	       AND [OGACode] = @OGACode
	       AND [CountryCode] = @CountryCode

 


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_OGABranchDelete]
-- ========================================================================================================================================

GO
 