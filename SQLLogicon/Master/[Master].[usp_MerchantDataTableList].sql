-- [Master].[usp_MerchantDataTableList]  10,0,'MerchantCode','asc','all',1006

Alter PROC [Master].[usp_MerchantDataTableList] (
	@limit smallint,
	@offset smallint,
	@sortColumn varchar(50),
	@sortType varchar(50),
	@merchantType varchar(100), 
	@CompanyCode bigint)

AS 
 
BEGIN

 Declare @Sql nvarchar(max);

if Lower(@merchantType)='all'
Begin

	Select @sql = 'SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], ' + 
	'[TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  ' +
	'[DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks],  ' +
	'[IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS],  ' +
	'[IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter],  ' +
	'[IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn], ' +
	'[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], ' +
	'IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal   ' +
	'From Master.Merchant ' +
	'Where CompanyCode =  ' + Convert(nvarchar(10), @CompanyCode)  + ' ' + 
	'And IsActive = Cast(1 as bit) ' +
	'UNION ALL ' +
	'SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], ' +
	'[TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  ' +
	'[DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks],  ' +
	'[IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS],  ' +
	'[IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter],  ' +
	'[IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn], ' +
	'[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], ' +
	'IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal   ' +
	'From Master.Merchant ' +
	'Where CompanyCode = 1000 ' +
	'And IsActive = Cast(1 as bit) ' +
	'And CompanyCode NOT IN (' + Convert(nvarchar(10), @CompanyCode) + ') ' +
	'Order By ' + @sortColumn + '  '+ @sortType + '  ' + 
	'OFFSET ' +   Convert(nvarchar(10),@offset) + ' ROWS FETCH NEXT ' + Convert(nvarchar(10),@limit) + ' ROWS ONLY '

End
Else
Begin


	Select @sql = 'SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], ' + 
	'[TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  ' +
	'[DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks],  ' +
	'[IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS],  ' +
	'[IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter],  ' +
	'[IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn], ' +
	'[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], ' +
	'IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal   ' +
	'From Master.Merchant ' +
	'Where CompanyCode =  ' + Convert(nvarchar(10), @CompanyCode)  + ' ' + 
	'And IsActive = Cast(1 as bit) ' +
	'And IsShipper = Case  When ''' + @merchantType +  ''' = ''shipper'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsBillingCustomer = Case  When ''' + @merchantType +  '''  = ''billingCustomer'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsConsignee = Case  When ''' + @merchantType +  ''' = ''consignee'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsTerminal = Case  When ''' + @merchantType +  ''' =''terminal''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsForwarder = Case  When ''' + @merchantType +  ''' = ''freightForwarder'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsTransporter = Case  When ''' + @merchantType +  '''=''transporter''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsVendor =  Case  When ''' + @merchantType +  '''=''vendor''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsYard = Case  When ''' + @merchantType +  ''' = ''yard'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'UNION ALL ' +
	'SELECT [MerchantCode], [MerchantName], [BillingCustomer], [PortCode], [RegNo], ' +
	'[TaxID], [OwnerCode], [AgentCode], [YardCode], [BillingMethod], [CreditTerm], [CreditLimit],  ' +
	'[DefaultCurrency], [CreditorCode], [DebtorCode], [EDIMappingCode], [WebSite], [Remarks],  ' +
	'[IsBillingCustomer], [IsShipper], [IsConsignee], [IsLiner], [IsForwarder], [IsYard], [IsCFS],  ' +
	'[IsRailOperator], [IsTerminal], [IsPrincipal], [IsVMIVendor], [IsHaulier], [IsTransporter],  ' +
	'[IsVendor], [IsRailYard], [IsTaxable],[IsSelectContainer], [CurrentBalance], [IsActive], [CreatedBy],[CreatedOn], ' +
	'[ModifiedBy], [ModifiedOn] ,IsVerified,VerifiedBy,VerifiedOn, CompanyCode,[BusinessType], ' +
	'IsFreightForwarder,IsOverseasAgent,IsSubscriber,IsOriginal   ' +
	'From Master.Merchant ' +
	'Where CompanyCode = 1000 ' +
	'And IsActive = Cast(1 as bit) ' +
	'And IsShipper = Case  When ''' + @merchantType +  ''' = ''shipper'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsBillingCustomer = Case  When ''' + @merchantType +  '''  = ''billingCustomer'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsConsignee = Case  When ''' + @merchantType +  ''' = ''consignee'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsTerminal = Case  When ''' + @merchantType +  ''' =''terminal''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsForwarder = Case  When ''' + @merchantType +  ''' = ''freightForwarder'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsTransporter = Case  When ''' + @merchantType +  '''=''transporter''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsVendor =  Case  When ''' + @merchantType +  '''=''vendor''  Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And IsYard = Case  When ''' + @merchantType +  ''' = ''yard'' Then cast(1 as bit) Else Cast(0 as bit) END ' +
	'And CompanyCode NOT IN (' + Convert(nvarchar(10), @CompanyCode) + ') ' +
	'Order By ' + @sortColumn + '  '+ @sortType + '  ' + 
	'OFFSET ' +   Convert(nvarchar(10),@offset) + ' ROWS FETCH NEXT ' + Convert(nvarchar(10),@limit) + ' ROWS ONLY '
End


print @Sql;
Exec(@Sql) 
 
END
