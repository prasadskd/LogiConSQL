

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryRules] Record based on [JobCategoryRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryRulesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesSelect] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesSelect] 
    @BranchID BIGINT,
    @Code NVARCHAR(15),
    @RuleType SMALLINT,
    @RuleDescription NVARCHAR(100)
AS 

BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType')
	SELECT	Jcr.[BranchID], Jcr.[Code], Jcr.[RuleType], Jcr.[RuleDescription], Jcr.[RuleStatus], 
			Jcr.[CreatedBy], Jcr.[CreatedOn], Jcr.[ModifiedBy], Jcr.[ModifiedOn],
			ISNULL(Jt.LookupDescription ,'') As JobCategoryRuleDescription,
			ISNULL(Jc.Description ,'') As JobCategoryDescription
	FROM   [Master].[JobCategoryRules] Jcr
	Left Outer Join JobCategoryRulesLookup Jt On 
		Jcr.RuleType = Jt.LookupID
	Left Outer Join Master.JobCategory Jc On 
		Jcr.Code = Jc.Code
	WHERE  Jcr.[BranchID] = @BranchID  
	       AND Jcr.[Code] = @Code  
	       AND Jcr.[RuleType] = @RuleType  
	       AND Jcr.[RuleDescription] = @RuleDescription  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryRules] Records from [JobCategoryRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryRulesList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesList] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesList] 
    @BranchID BIGINT,
    @Code NVARCHAR(15)

AS 
BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType')
	SELECT	Jcr.[BranchID], Jcr.[Code], Jcr.[RuleType], Jcr.[RuleDescription], Jcr.[RuleStatus], 
			Jcr.[CreatedBy], Jcr.[CreatedOn], Jcr.[ModifiedBy], Jcr.[ModifiedOn],
			ISNULL(Jt.LookupDescription ,'') As JobCategoryRuleDescription,
			ISNULL(Jc.Description ,'') As JobCategoryDescription
	FROM   [Master].[JobCategoryRules] Jcr
	Left Outer Join JobCategoryRulesLookup Jt On 
		Jcr.RuleType = Jt.LookupID
	Left Outer Join Master.JobCategory Jc On 
		Jcr.Code = Jc.Code
	WHERE Jcr.[BranchID] = @BranchID  
	       AND Jcr.[Code] = @Code  

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [JobCategoryRules] Records from [JobCategoryRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryRulesPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesPageView] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesPageView] 
    @BranchID BIGINT,
    @Code NVARCHAR(15),
	@fetchrows bigint
AS 
BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType')
	SELECT	Jcr.[BranchID], Jcr.[Code], Jcr.[RuleType], Jcr.[RuleDescription], Jcr.[RuleStatus], 
			Jcr.[CreatedBy], Jcr.[CreatedOn], Jcr.[ModifiedBy], Jcr.[ModifiedOn],
			ISNULL(Jt.LookupDescription ,'') As JobCategoryRuleDescription,
			ISNULL(Jc.Description ,'') As JobCategoryDescription
	FROM   [Master].[JobCategoryRules] Jcr
	Left Outer Join JobCategoryRulesLookup Jt On 
		Jcr.RuleType = Jt.LookupID
	Left Outer Join Master.JobCategory Jc On 
		Jcr.Code = Jc.Code
	WHERE Jcr.[BranchID] = @BranchID  
	       AND Jcr.[Code] = @Code  

	ORDER BY [RuleDescription]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [JobCategoryRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryRulesRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesRecordCount] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesRecordCount] 
	@BranchID BIGINT,
    @Code NVARCHAR(15)

AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[JobCategoryRules]
	Where BranchID = @BranchID
	And Code = @Code
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [JobCategoryRules] Record based on [JobCategoryRules] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_JobCategoryRulesAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesAutoCompleteSearch] 
    @BranchID BIGINT,
    @Code NVARCHAR(15),
    @RuleType SMALLINT,
    @RuleDescription NVARCHAR(100)
AS 

BEGIN

	;With JobCategoryRulesLookup As (Select Lookupid,LookupDescription From Config.Lookup Where LookupCategory='JobCategoryRuleType')
	SELECT	Jcr.[BranchID], Jcr.[Code], Jcr.[RuleType], Jcr.[RuleDescription], Jcr.[RuleStatus], 
			Jcr.[CreatedBy], Jcr.[CreatedOn], Jcr.[ModifiedBy], Jcr.[ModifiedOn],
			ISNULL(Jt.LookupDescription ,'') As JobCategoryRuleDescription,
			ISNULL(Jc.Description ,'') As JobCategoryDescription
	FROM   [Master].[JobCategoryRules] Jcr
	Left Outer Join JobCategoryRulesLookup Jt On 
		Jcr.RuleType = Jt.LookupID
	Left Outer Join Master.JobCategory Jc On 
		Jcr.Code = Jc.Code
	WHERE  Jcr.[BranchID] = @BranchID  
	       AND Jcr.[Code] = @Code  
	       AND Jcr.[RuleType] = @RuleType  
	       AND Jcr.[RuleDescription] LIKE '%' +  @RuleDescription  + '%'
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [JobCategoryRules] Record Into [JobCategoryRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryRulesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesInsert] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesInsert] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @RuleStatus bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
  

BEGIN

	
	INSERT INTO [Master].[JobCategoryRules] (
			[BranchID], [Code], [RuleType], [RuleDescription], [RuleStatus], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @Code, @RuleType, @RuleDescription, @RuleStatus, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [JobCategoryRules] Record Into [JobCategoryRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryRulesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesUpdate] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesUpdate] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @RuleStatus bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE [Master].[JobCategoryRules]
	SET    [RuleDescription] = @RuleDescription, [RuleStatus] = @RuleStatus, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [BranchID] = @BranchID
	       AND [Code] = @Code
	       AND [RuleType] = @RuleType
	       AND [RuleDescription] = @RuleDescription
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [JobCategoryRules] Record Into [JobCategoryRules] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryRulesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesSave] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesSave] 
    @BranchID BIGINT,
    @Code nvarchar(15),
    @RuleType smallint,
    @RuleDescription nvarchar(100),
    @RuleStatus bit,
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[JobCategoryRules] 
		WHERE 	[BranchID] = @BranchID
	       AND [Code] = @Code
	       AND [RuleType] = @RuleType
	       AND [RuleDescription] = @RuleDescription)>0
	BEGIN
	    Exec [Master].[usp_JobCategoryRulesUpdate] 
		@BranchID, @Code, @RuleType, @RuleDescription, @RuleStatus, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_JobCategoryRulesInsert] 
		@BranchID, @Code, @RuleType, @RuleDescription, @RuleStatus, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[JobCategoryRulesSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_JobCategoryRulesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [JobCategoryRules] Record  based on [JobCategoryRules]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_JobCategoryRulesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_JobCategoryRulesDelete] 
END 
GO
CREATE PROC [Master].[usp_JobCategoryRulesDelete] 
    @BranchID BIGINT,
    @Code nvarchar(15) 
AS 

	
BEGIN

	 SET NOCOUNT ON;	
 
	DELETE
	FROM   [Master].[JobCategoryRules]
	WHERE  [BranchID] = @BranchID
	       AND [Code] = @Code
	        
SET NOCOUNT OFF;

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_JobCategoryRulesDelete]
-- ========================================================================================================================================

GO
