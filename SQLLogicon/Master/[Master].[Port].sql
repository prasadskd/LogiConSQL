
-- ========================================================================================================================================
-- START											 [Master].[usp_PortSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Port] Record based on [Port] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortSelect] 
END 
GO
CREATE PROC [Master].[usp_PortSelect] 
    @PortCode NVARCHAR(10)
AS 

BEGIN
	;with 
	PortTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='PortType'),
	TradeModeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='TradeMode')
	SELECT	P.[PortCode], P.[PortName], P.[CountryCode], P.[PortType],P.TradeMode, P.[Latitude], P.[Longitude], P.[ISOCode], P.[MappingCode], 
			P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn] ,
			C.CountryName , ISNULL(PL.LookupDescription,'') As PortTypeDescription,
			ISNULL(TR.LookupDescription,'') As TradeModeDescription
	FROM	[Master].[Port] P
	Left Outer Join Master.Country C ON 
	P.CountryCode = C.CountryCode
	Left Outer Join PortTypeLookup PL ON 
		P.PortType = PL.LookupID
	Left Outer Join TradeModeLookup TR ON 
		P.TradeMode = TR.LookupID
	WHERE  [PortCode] = @PortCode  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PortSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PortList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Port] Records from [Port] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortList] 
END 
GO
CREATE PROC [Master].[usp_PortList] 
	@CountryCode nvarchar(2)
AS 
BEGIN

	;with 
	PortTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='PortType'),
	TradeModeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='TradeMode')
	SELECT	P.[PortCode], P.[PortName], P.[CountryCode], P.[PortType],P.TradeMode, P.[Latitude], P.[Longitude], P.[ISOCode], P.[MappingCode], 
			P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn] ,
			C.CountryName , ISNULL(PL.LookupDescription,'') As PortTypeDescription,
			ISNULL(TR.LookupDescription,'') As TradeModeDescription
	FROM	[Master].[Port] P
	Left Outer Join Master.Country C ON 
	P.CountryCode = C.CountryCode
	Left Outer Join PortTypeLookup PL ON 
		P.PortType = PL.LookupID
	Left Outer Join TradeModeLookup TR ON 
		P.TradeMode = TR.LookupID
	Where P.CountryCode = ISNULL(@CountryCode,P.CountryCode)

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PortPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Port] Records from [Port] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortPageView] 
END 
GO
CREATE PROC [Master].[usp_PortPageView] 
	@fetchrows bigint
AS 
BEGIN

	;with 
	PortTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='PortType'),
	TradeModeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='TradeMode')
	SELECT	P.[PortCode], P.[PortName], P.[CountryCode], P.[PortType],P.TradeMode, P.[Latitude], P.[Longitude], P.[ISOCode], P.[MappingCode], 
			P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn] ,
			C.CountryName , ISNULL(PL.LookupDescription,'') As PortTypeDescription,
			ISNULL(TR.LookupDescription,'') As TradeModeDescription
	FROM	[Master].[Port] P
	Left Outer Join Master.Country C ON 
	P.CountryCode = C.CountryCode
	Left Outer Join PortTypeLookup PL ON 
		P.PortType = PL.LookupID
	Left Outer Join TradeModeLookup TR ON 
		P.TradeMode = TR.LookupID
	ORDER BY  P.[PortName] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PortRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [Port] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortRecordCount] 
END 
GO
CREATE PROC [Master].[usp_PortRecordCount] 
	@BranchID smallint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Master].[Port]
 
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PortAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Port] Record based on [Port] table

-- [Master].[usp_PortAutoCompleteSearch] 'PORT KLA'
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_PortAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortAutoCompleteSearch] 
END 
GO
CREATE PROC [Master].[usp_PortAutoCompleteSearch] 
    @PortName nvarchar(200)
AS 

BEGIN

	;with 
	PortTypeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='PortType'),
	TradeModeLookup As (Select LookupId,LookupDescription From Config.Lookup where LookupCategory ='TradeMode')
	SELECT	P.[PortCode], P.[PortName], P.[CountryCode], P.[PortType],P.TradeMode, P.[Latitude], P.[Longitude], P.[ISOCode], P.[MappingCode], 
			P.[IsActive], P.[CreatedBy], P.[CreatedOn], P.[ModifiedBy], P.[ModifiedOn] ,
			C.CountryName , ISNULL(PL.LookupDescription,'') As PortTypeDescription,
			ISNULL(TR.LookupDescription,'') As TradeModeDescription
	FROM	[Master].[Port] P
	Left Outer Join Master.Country C ON 
	P.CountryCode = C.CountryCode
	Left Outer Join PortTypeLookup PL ON 
		P.PortType = PL.LookupID
	Left Outer Join TradeModeLookup TR ON 
		P.TradeMode = TR.LookupID
	WHERE  ([PortName] LIKE '%' + @PortName  + '%' OR P.PortCode LIKE '%' + @PortName + '%' )
	And P.IsActive = CAST(1 as bit)
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_PortAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_PortInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Port] Record Into [Port] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PortInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortInsert] 
END 
GO
CREATE PROC [Master].[usp_PortInsert] 
    @PortCode nvarchar(10),
    @PortName nvarchar(200),
    @CountryCode nvarchar(2),
    @PortType smallint,
	@TradeMode smallint,
    @Latitude nvarchar(50),
    @Longitude nvarchar(50),
    @ISOCode nvarchar(10),
    @MappingCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN

	
	INSERT INTO [Master].[Port] (
			[PortCode], [PortName], [CountryCode], [PortType],[TradeMode], [Latitude], [Longitude], [ISOCode], [MappingCode], [CreatedBy], [CreatedOn])
	SELECT	@PortCode, @PortName, @CountryCode, @PortType,@TradeMode, @Latitude, @Longitude, @ISOCode, @MappingCode, @CreatedBy, GETUTCDATE()
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PortUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Port] Record Into [Port] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PortUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortUpdate] 
END 
GO
CREATE PROC [Master].[usp_PortUpdate] 
    @PortCode nvarchar(10),
    @PortName nvarchar(200),
    @CountryCode nvarchar(2),
    @PortType smallint,
	@TradeMode smallint,
    @Latitude nvarchar(50),
    @Longitude nvarchar(50),
    @ISOCode nvarchar(10),
    @MappingCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Port]
	SET		[PortName] = @PortName, [CountryCode] = @CountryCode, [PortType] = @PortType,[TradeMode]=@TradeMode, [Latitude] = @Latitude, [Longitude] = @Longitude, 
			[ISOCode] = @ISOCode, [MappingCode] = @MappingCode, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[PortCode] = @PortCode
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_PortSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Port] Record Into [Port] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PortSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortSave] 
END 
GO
CREATE PROC [Master].[usp_PortSave] 
    @PortCode nvarchar(10),
    @PortName nvarchar(200),
    @CountryCode nvarchar(2),
    @PortType smallint,
	@TradeMode smallint,
    @Latitude nvarchar(50),
    @Longitude nvarchar(50),
    @ISOCode nvarchar(10),
    @MappingCode nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[Port] 
		WHERE 	[PortCode] = @PortCode)>0
	BEGIN
	    Exec [Master].[usp_PortUpdate] 
			@PortCode, @PortName, @CountryCode, @PortType,@TradeMode, @Latitude, @Longitude, @ISOCode, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_PortInsert] 
		@PortCode, @PortName, @CountryCode, @PortType,@TradeMode, @Latitude, @Longitude, @ISOCode, @MappingCode, @CreatedBy, @ModifiedBy 


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[PortSave]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_PortDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Port] Record  based on [Port]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_PortDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_PortDelete] 
END 
GO
CREATE PROC [Master].[usp_PortDelete] 
    @PortCode nvarchar(10)
AS 

	
BEGIN

	UPDATE	[Master].[Port]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[PortCode] = @PortCode

	/*
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Master].[Port]
	WHERE  [PortCode] = @PortCode
	*/


END

-- ========================================================================================================================================
-- END  											 [Master].[usp_PortDelete]
-- ========================================================================================================================================

GO
 

 