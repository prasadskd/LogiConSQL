
-- ========================================================================================================================================
-- START											 [Master].[usp_HSCodeHeaderByTariffCode]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [HSCode] Record based on [HSCode] table

--select * from master.HSCode
--Exec [Master].[usp_HSCodeHeaderByTariffCode] '8703406490'
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_HSCodeHeaderByTariffCode]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_HSCodeHeaderByTariffCode] 
END 
GO
CREATE PROC [Master].[usp_HSCodeHeaderByTariffCode] 
    @TariffCode NVARCHAR(20)
AS 

BEGIN

	SELECT	Hd.[HeadingCode], Hd.[HeadingDescription]
	FROM	[Master].[HSCodeHeader] Hd
	Left Outer Join [Master].[HSCode] Dt ON 
		Hd.HeadingCode = Dt.HeaderCode
	WHERE	TariffCode = @TariffCode  


END
-- ========================================================================================================================================
-- END  											 [Master].[usp_HSCodeHeaderByTariffCode]
-- ========================================================================================================================================

GO