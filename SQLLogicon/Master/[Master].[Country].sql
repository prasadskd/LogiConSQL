-- ========================================================================================================================================
-- START											 [Master].[usp_CountrySelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [Country] Record based on [Country] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountrySelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountrySelect] 
END 
GO
CREATE PROC [Master].[usp_CountrySelect] 
    @CountryCode VARCHAR(2)
AS 
 

BEGIN

	SELECT [CountryCode], [CountryName], [EDIMapping], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] ,[DailingCode]
	FROM   [Master].[Country]
	WHERE  [CountryCode] = @CountryCode  

END
-- ========================================================================================================================================
-- END  											 [Master].[usp_CountrySelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CountryList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [Country] Records from [Country] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_CountryList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryList] 
END 
GO
CREATE PROC [Master].[usp_CountryList] 

AS 
 
BEGIN
	SELECT [CountryCode], [CountryName], [EDIMapping], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn],[DailingCode] 
	FROM   [Master].[Country]
	Order By CountryName

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryList] 
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_CountryInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [Country] Record Into [Country] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryInsert] 
END 
GO
CREATE PROC [Master].[usp_CountryInsert] 
    @CountryCode varchar(2),
    @CountryName nvarchar(255),
    @EDIMapping nvarchar(255),
	@DailingCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
  

BEGIN
	
	INSERT INTO [Master].[Country] ([CountryCode], [CountryName], [EDIMapping],DailingCode, [CreatedBy], [CreatedOn])
	SELECT @CountryCode, @CountryName, @EDIMapping,@DailingCode, @CreatedBy, GETDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountryUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [Country] Record Into [Country] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryUpdate] 
END 
GO
CREATE PROC [Master].[usp_CountryUpdate] 
    @CountryCode varchar(2),
    @CountryName nvarchar(255),
    @EDIMapping nvarchar(255),
	@DailingCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)
AS 
 
	
BEGIN

	UPDATE	[Master].[Country]
	SET		[CountryCode] = @CountryCode, [CountryName] = @CountryName, [EDIMapping] = @EDIMapping, DailingCode = @DailingCode,
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETDATE()
	WHERE	[CountryCode] = @CountryCode
	

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountrySave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [Country] Record Into [Country] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountrySave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountrySave] 
END 
GO
CREATE PROC [Master].[usp_CountrySave] 
    @CountryCode varchar(2),
    @CountryName nvarchar(255),
    @EDIMapping nvarchar(255),
	@DailingCode nvarchar(10),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50)

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Master].[Country] 
		WHERE 	[CountryCode] = @CountryCode)>0
	BEGIN
	    Exec [Master].[usp_CountryUpdate] 
			@CountryCode, @CountryName, @EDIMapping,@DailingCode, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_CountryInsert] 
			@CountryCode, @CountryName, @EDIMapping,@DailingCode, @CreatedBy, @ModifiedBy 


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[CountrySave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_CountryDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [Country] Record  based on [Country]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_CountryDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_CountryDelete] 
END 
GO
CREATE PROC [Master].[usp_CountryDelete] 
    @CountryCode varchar(2)
AS 
BEGIN


	DELETE
	FROM   [Master].[Country]
	WHERE  [CountryCode] = @CountryCode
 
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_CountryDelete]
-- ========================================================================================================================================

GO
