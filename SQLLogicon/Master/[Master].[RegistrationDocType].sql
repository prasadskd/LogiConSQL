-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [RegistrationDocType] Record based on [RegistrationDocType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_RegistrationDocTypeSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeSelect] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeSelect] 
    @OrganizationType SMALLINT,
    @RegistrationDocType SMALLINT
AS 

BEGIN

	
	SELECT [OrganizationType], [RegistrationDocType], [RegistrationDocTypeCode], [DocumentDescription], [IsMandatory], [Status] 
	FROM   [Master].[RegistrationDocType]
	WHERE  [OrganizationType] = @OrganizationType  
	       AND [RegistrationDocType] = @RegistrationDocType  
END
-- ========================================================================================================================================
-- END  											 [Master].[usp_RegistrationDocTypeSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [RegistrationDocType] Records from [RegistrationDocType] table
-- ========================================================================================================================================


IF OBJECT_ID('[Master].[usp_RegistrationDocTypeList]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeList] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeList] 
	 @OrganizationType SMALLINT
AS 
BEGIN

	SELECT [OrganizationType], [RegistrationDocType] as [RegistrationDocumentType], [RegistrationDocTypeCode], [DocumentDescription], [IsMandatory], [Status] 
 FROM   [Master].[RegistrationDocType]
 WHERE  [OrganizationType] = @OrganizationType

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_RegistrationDocTypeList] 
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [RegistrationDocType] Record Into [RegistrationDocType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_RegistrationDocTypeInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeInsert] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeInsert] 
    @OrganizationType smallint,
    @RegistrationDocType smallint,
    @RegistrationDocTypeCode nvarchar(50),
    @DocumentDescription nvarchar(200),
    @IsMandatory bit 
AS 
  

BEGIN

	
	INSERT INTO [Master].[RegistrationDocType] (
			[OrganizationType], [RegistrationDocType], [RegistrationDocTypeCode], [DocumentDescription], [IsMandatory], [Status])
	SELECT	@OrganizationType, @RegistrationDocType, @RegistrationDocTypeCode, @DocumentDescription, @IsMandatory, Cast(1 as bit)
               
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_RegistrationDocTypeInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [RegistrationDocType] Record Into [RegistrationDocType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_RegistrationDocTypeUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeUpdate] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeUpdate] 
    @OrganizationType smallint,
    @RegistrationDocType smallint,
    @RegistrationDocTypeCode nvarchar(50),
    @DocumentDescription nvarchar(200),
    @IsMandatory bit 
AS 
 
	
BEGIN

	UPDATE	[Master].[RegistrationDocType]
	SET		[OrganizationType] = @OrganizationType, [RegistrationDocType] = @RegistrationDocType, [RegistrationDocTypeCode] = @RegistrationDocTypeCode, 
			[DocumentDescription] = @DocumentDescription, [IsMandatory] = @IsMandatory
	WHERE	[OrganizationType] = @OrganizationType
			AND [RegistrationDocType] = @RegistrationDocType
	
END

-- ========================================================================================================================================
-- END  											 [Master].[usp_RegistrationDocTypeUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [RegistrationDocType] Record Into [RegistrationDocType] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_RegistrationDocTypeSave]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeSave] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeSave] 
    @OrganizationType smallint,
    @RegistrationDocType smallint,
    @RegistrationDocTypeCode nvarchar(50),
    @DocumentDescription nvarchar(200),
    @IsMandatory bit 
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Master].[RegistrationDocType] 
		WHERE 	[OrganizationType] = @OrganizationType
	       AND [RegistrationDocType] = @RegistrationDocType)>0
	BEGIN
	    Exec [Master].[usp_RegistrationDocTypeUpdate] 
		@OrganizationType, @RegistrationDocType, @RegistrationDocTypeCode, @DocumentDescription, @IsMandatory 


	END
	ELSE
	BEGIN
	    Exec [Master].[usp_RegistrationDocTypeInsert] 
		@OrganizationType, @RegistrationDocType, @RegistrationDocTypeCode, @DocumentDescription, @IsMandatory


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Master].usp_[RegistrationDocTypeSave]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Master].[usp_RegistrationDocTypeDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [RegistrationDocType] Record  based on [RegistrationDocType]

-- ========================================================================================================================================

IF OBJECT_ID('[Master].[usp_RegistrationDocTypeDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Master].[usp_RegistrationDocTypeDelete] 
END 
GO
CREATE PROC [Master].[usp_RegistrationDocTypeDelete] 
    @OrganizationType smallint,
    @RegistrationDocType smallint
AS 

	
BEGIN

	UPDATE	[Master].[RegistrationDocType]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[OrganizationType] = @OrganizationType
	       AND [RegistrationDocType] = @RegistrationDocType

	 

END

-- ========================================================================================================================================
-- END  											 [Master].[usp_RegistrationDocTypeDelete]
-- ========================================================================================================================================

GO
 
  