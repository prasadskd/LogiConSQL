
-- ========================================================================================================================================
-- START											 [Security].[usp_UserLogIn]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [User] Record  based on [User]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserLogIn]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserLogIn] 
END 
GO
CREATE PROC [Security].[usp_UserLogIn] 
    @UserID NVARCHAR(60),
	@Password nvarchar(100)
AS 

BEGIN

	


	;with UserGroupLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserGroupType'),
	UserDesignationLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserDesignationType'),
	CompanyTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Usr.[CompanyCode], Usr.[UserID], Usr.[UserName], Usr.[Password], Usr.[UserGroup], Usr.[UserDesignation], Usr.[EmployeeID], 
			Usr.[ICNo], Usr.EmailID,Usr.ContactNo, Usr.[IsActive], Usr.[IsAllowLogOn], Usr.[IsOperational], Usr.[CreatedBy], Usr.[CreatedOn], 
			Usr.[ModifiedBy], Usr.[ModifiedOn],
			ISNULL(Ug.LookupDescription,'') As UserGroupDescription,
			ISNULL(UD.LookupDescription,'') As UserDesignationDescription,
			CT.LookupID As CompanyType, ISNULL(CT.LookupDescription,'') As CompanyTypeDescription, Usr.OTPNo, 
			Usr.IsOTPSent, Usr.OTPSentDate, Usr.IsOTPReSent, Usr.OTPSentCount, Usr.IsOTPVerified,Usr.RoleCode, Usr.BranchID
	FROM	[Security].[User] Usr
	Left Outer Join UserGroupLookup UG ON 
		Usr.UserGroup = UG.LookupID
	Left Outer Join UserDesignationLookup UD ON 
		Usr.UserDesignation = UD.LookupID
	Left Outer Join Master.Company Cmp ON 
		Usr.CompanyCode = Cmp.CompanyCode
	Left Outer Join CompanyTypeLookup CT ON 
		Cmp.RegistrationType= CT.LookupID
	WHERE  Usr.[UserID] = @UserID  
	And Usr.[Password] =@Password
END

GO
