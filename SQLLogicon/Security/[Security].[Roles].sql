 


-- ========================================================================================================================================
-- START											 [Security].[usp_RolesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [Roles] Record based on [Roles] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_RolesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesSelect] 
END 
GO
CREATE PROC [Security].[usp_RolesSelect] 
    @CompanyCode int,
    @RoleCode nvarchar(30)
AS 
 

BEGIN

	SELECT [CompanyCode], [RoleCode], [RoleDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Security].[Roles]
	WHERE  [CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode

END
-- ========================================================================================================================================
-- END  											 [Security].[usp_RolesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_RolesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [Roles] Records from [Roles] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_RolesList]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesList] 
END 
GO
CREATE PROC [Security].[usp_RolesList] 
    @CompanyCode int
AS 
 
BEGIN
	SELECT [CompanyCode], [RoleCode], [RoleDescription], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM   [Security].[Roles]
	WHERE  [CompanyCode] = @CompanyCode


END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RolesList] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [Security].[usp_RolesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [Roles] Record Into [Roles] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RolesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesInsert] 
END 
GO
CREATE PROC [Security].[usp_RolesInsert] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @RoleDescription nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL
AS 
  

BEGIN
	
	INSERT INTO [Security].[Roles] ([CompanyCode], [RoleCode], [RoleDescription], [IsActive], [CreatedBy], [CreatedOn])
	SELECT @CompanyCode, @RoleCode, @RoleDescription, CAST(1 as bit), @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RolesInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_RolesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [Roles] Record Into [Roles] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RolesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesUpdate] 
END 
GO
CREATE PROC [Security].[usp_RolesUpdate] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @RoleDescription nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL
AS 
 
	
BEGIN

	UPDATE [Security].[Roles]
	SET    [RoleDescription] = @RoleDescription,  [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE  [CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode
	

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RolesUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_RolesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [Roles] Record Into [Roles] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RolesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesSave] 
END 
GO
CREATE PROC [Security].[usp_RolesSave] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @RoleDescription nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Security].[Roles] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode)>0
	BEGIN
	    Exec [Security].[usp_RolesUpdate] 
		@CompanyCode, @RoleCode, @RoleDescription, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Security].[usp_RolesInsert] 
		@CompanyCode, @RoleCode, @RoleDescription, @CreatedBy, @ModifiedBy


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Security].usp_[RolesSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_RolesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [Roles] Record  based on [Roles]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RolesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RolesDelete] 
END 
GO
CREATE PROC [Security].[usp_RolesDelete] 
    @CompanyCode int,
    @RoleCode nvarchar(30)
AS 

	
BEGIN

	UPDATE	[Security].[Roles]
	SET	[IsActive] = CAST(0 as bit)
	WHERE 	[CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode

	 
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RolesDelete]
-- ========================================================================================================================================

GO 