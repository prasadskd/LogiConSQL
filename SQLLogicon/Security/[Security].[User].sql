
-- ========================================================================================================================================
-- START											 [Security].[usp_UserSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [User] Record based on [User] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserSelect] 
END 
GO
CREATE PROC [Security].[usp_UserSelect] 
    @CompanyCode Int,
    @UserID NVARCHAR(60)
AS 

BEGIN

	;with UserGroupLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserGroupType'),
	UserDesignationLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserDesignationType'),
	CompanyTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Usr.[CompanyCode], Usr.[UserID], Usr.[UserName], Usr.[Password], Usr.[UserGroup], Usr.[UserDesignation], Usr.[EmployeeID], 
			Usr.[ICNo], Usr.EmailID,Usr.ContactNo, Usr.[IsActive], Usr.[IsAllowLogOn], Usr.[IsOperational], Usr.[CreatedBy], Usr.[CreatedOn], 
			Usr.[ModifiedBy], Usr.[ModifiedOn],
			ISNULL(Ug.LookupDescription,'') As UserGroupDescription,
			ISNULL(UD.LookupDescription,'') As UserDesignationDescription,
			CT.LookupID As CompanyType, ISNULL(CT.LookupDescription,'') As CompanyTypeDescription, Usr.OTPNo, 
			Usr.IsOTPSent, Usr.OTPSentDate, Usr.IsOTPReSent, Usr.OTPSentCount, Usr.IsOTPVerified,Usr.RoleCode, Usr.BranchID
	FROM	[Security].[User] Usr
	Left Outer Join UserGroupLookup UG ON 
		Usr.UserGroup = UG.LookupID
	Left Outer Join UserDesignationLookup UD ON 
		Usr.UserDesignation = UD.LookupID
	Left Outer Join Master.Company Cmp ON 
		Usr.CompanyCode = Cmp.CompanyCode
	Left Outer Join CompanyTypeLookup CT ON 
		Cmp.RegistrationType= CT.LookupID
	WHERE  Usr.[CompanyCode] = @CompanyCode  
	       AND Usr.[UserID] = @UserID  
END
-- ========================================================================================================================================
-- END  											 [Security].[usp_UserSelect]
-- ========================================================================================================================================


GO

-- ========================================================================================================================================
-- START											 [Security].[usp_UserList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [User] Records from [User] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserList]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserList] 
END 
GO
CREATE PROC [Security].[usp_UserList] 
	@CompanyCode Int
AS 
BEGIN

	;with UserGroupLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserGroupType'),
	UserDesignationLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserDesignationType'),
	CompanyTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Usr.[CompanyCode], Usr.[UserID], Usr.[UserName], Usr.[Password], Usr.[UserGroup], Usr.[UserDesignation], Usr.[EmployeeID], 
			Usr.[ICNo], Usr.EmailID,Usr.ContactNo, Usr.[IsActive], Usr.[IsAllowLogOn], Usr.[IsOperational], Usr.[CreatedBy], Usr.[CreatedOn], 
			Usr.[ModifiedBy], Usr.[ModifiedOn],
			ISNULL(Ug.LookupDescription,'') As UserGroupDescription,
			ISNULL(UD.LookupDescription,'') As UserDesignationDescription,
			CT.LookupID As CompanyType, ISNULL(CT.LookupDescription,'') As CompanyTypeDescription, Usr.OTPNo, Usr.IsOTPSent, 
			Usr.OTPSentDate, Usr.IsOTPReSent, Usr.OTPSentCount, Usr.IsOTPVerified,Usr.RoleCode, Usr.BranchID
	FROM	[Security].[User] Usr
	Left Outer Join UserGroupLookup UG ON 
		Usr.UserGroup = UG.LookupID
	Left Outer Join UserDesignationLookup UD ON 
		Usr.UserDesignation = UD.LookupID
	Left Outer Join Master.Company Cmp ON 
		Usr.CompanyCode = Cmp.CompanyCode
	Left Outer Join CompanyTypeLookup CT ON 
		Cmp.RegistrationType= CT.LookupID
	Where Usr.CompanyCode = @CompanyCode
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserList] 
-- ========================================================================================================================================


GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [User] Records from [User] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserPageView] 
END 
GO
CREATE PROC [Security].[usp_UserPageView] 
	@CompanyCode Int,
	@fetchrows bigint
AS 
BEGIN

	;with UserGroupLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserGroupType'),
	UserDesignationLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserDesignationType'),
	CompanyTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Usr.[CompanyCode], Usr.[UserID], Usr.[UserName], Usr.[Password], Usr.[UserGroup], Usr.[UserDesignation], Usr.[EmployeeID], 
			Usr.[ICNo], Usr.EmailID,Usr.ContactNo, Usr.[IsActive], Usr.[IsAllowLogOn], Usr.[IsOperational], Usr.[CreatedBy], Usr.[CreatedOn], 
			Usr.[ModifiedBy], Usr.[ModifiedOn],
			ISNULL(Ug.LookupDescription,'') As UserGroupDescription,
			ISNULL(UD.LookupDescription,'') As UserDesignationDescription,
			CT.LookupID As CompanyType, ISNULL(CT.LookupDescription,'') As CompanyTypeDescription, Usr.OTPNo, Usr.IsOTPSent, 
			Usr.OTPSentDate, Usr.IsOTPReSent, Usr.OTPSentCount, Usr.IsOTPVerified,Usr.RoleCode
	FROM	[Security].[User] Usr
	Left Outer Join UserGroupLookup UG ON 
		Usr.UserGroup = UG.LookupID
	Left Outer Join UserDesignationLookup UD ON 
		Usr.UserDesignation = UD.LookupID
	Left Outer Join Master.Company Cmp ON 
		Usr.CompanyCode = Cmp.CompanyCode
	Left Outer Join CompanyTypeLookup CT ON 
		Cmp.RegistrationType= CT.LookupID
	Where Usr.CompanyCode = @CompanyCode
	ORDER BY [UserID]  
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Security].[usp_UserRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [User] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRecordCount] 
END 
GO
CREATE PROC [Security].[usp_UserRecordCount] 
	@CompanyCode Int
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Security].[User] Usr
	Where Usr.CompanyCode = @CompanyCode

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Security].[usp_UserAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [User] Record based on [User] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserAutoCompleteSearch] 
END 
GO
CREATE PROC [Security].[usp_UserAutoCompleteSearch] 
    @CompanyCode Int,
    @UserID NVARCHAR(60)
AS 

BEGIN

	;with UserGroupLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserGroupType'),
	UserDesignationLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='UserDesignationType'),
	CompanyTypeLookup As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='RegistrationType')
	SELECT	Usr.[CompanyCode], Usr.[UserID], Usr.[UserName], Usr.[Password], Usr.[UserGroup], Usr.[UserDesignation], Usr.[EmployeeID], 
			Usr.[ICNo], Usr.EmailID,Usr.ContactNo, Usr.[IsActive], Usr.[IsAllowLogOn], Usr.[IsOperational], Usr.[CreatedBy], Usr.[CreatedOn], 
			Usr.[ModifiedBy], Usr.[ModifiedOn],
			ISNULL(Ug.LookupDescription,'') As UserGroupDescription,
			ISNULL(UD.LookupDescription,'') As UserDesignationDescription,
			CT.LookupID As CompanyType, ISNULL(CT.LookupDescription,'') As CompanyTypeDescription, 
			Usr.OTPNo, Usr.IsOTPSent, Usr.OTPSentDate, Usr.IsOTPReSent, Usr.OTPSentCount, Usr.IsOTPVerified,Usr.RoleCode
	FROM	[Security].[User] Usr
	Left Outer Join UserGroupLookup UG ON 
		Usr.UserGroup = UG.LookupID
	Left Outer Join UserDesignationLookup UD ON 
		Usr.UserDesignation = UD.LookupID
	Left Outer Join Master.Company Cmp ON 
		Usr.CompanyCode = Cmp.CompanyCode
	Left Outer Join CompanyTypeLookup CT ON 
		Cmp.RegistrationType= CT.LookupID
	WHERE  Usr.[CompanyCode] = @CompanyCode 
	       AND Usr.[UserID] LIKE '%' + @UserID + '%'
END
-- ========================================================================================================================================
-- END  											 [Security].[usp_UserAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [User] Record Into [User] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserInsert] 
END 
GO
CREATE PROC [Security].[usp_UserInsert] 
    @CompanyCode Int,
    @UserID nvarchar(60),
    @UserName nvarchar(50),
    @Password nvarchar(100),
    @UserGroup smallint,
    @UserDesignation smallint,
    @EmployeeID nvarchar(20),
    @ICNo nvarchar(20),
    @EmailID nvarchar(60),
	@ContactNo nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@BranchID int,
	@RoleCode nvarchar(30)

AS 
  

BEGIN

	
	INSERT INTO [Security].[User] (
			[CompanyCode], [UserID], [UserName], [Password], [UserGroup], [UserDesignation], [EmployeeID], [ICNo], [EmailID],[ContactNo], 
			[IsActive], [IsAllowLogOn], [IsOperational], [CreatedBy], [CreatedOn], BranchID, RoleCode)
	SELECT	@CompanyCode, @UserID, @UserName, @Password, @UserGroup, @UserDesignation, @EmployeeID, @ICNo, @EmailID, @ContactNo,
			CAST(1 AS BIT), CAST(1 AS BIT), CAST(1 AS BIT), @CreatedBy, GETUTCDATE(), @BranchID, @RoleCode
               
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserInsert]
-- ========================================================================================================================================


GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [User] Record Into [User] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserUpdate] 
END 
GO
CREATE PROC [Security].[usp_UserUpdate] 
    @CompanyCode Int,
    @UserID nvarchar(60),
    @UserName nvarchar(50),
    @Password nvarchar(100),
    @UserGroup smallint,
    @UserDesignation smallint,
    @EmployeeID nvarchar(20),
    @ICNo nvarchar(20),
    @EmailID nvarchar(60),
	@ContactNo nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@BranchID int,
	@RoleCode nvarchar(30)


AS 
 
	
BEGIN

	UPDATE	[Security].[User]
	SET		[UserName] = @UserName, [Password] = @Password, [UserGroup] = @UserGroup, 
			[UserDesignation] = @UserDesignation, [EmployeeID] = @EmployeeID, [ICNo] = @ICNo, EmailID = @EmailID,ContactNo = @ContactNo, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),RoleCode = @RoleCode, BranchID = @BranchID
	WHERE	[CompanyCode] = @CompanyCode
			AND [UserID] = @UserID
	
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserUpdate]
-- ========================================================================================================================================


GO


-- ========================================================================================================================================
-- START											 [Security].[usp_UserSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [User] Record Into [User] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserSave]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserSave] 
END 
GO
CREATE PROC [Security].[usp_UserSave] 
    @CompanyCode Int,	
    @UserID nvarchar(60),
    @UserName nvarchar(50),
    @Password nvarchar(100),
    @UserGroup smallint,
    @UserDesignation smallint,
    @EmployeeID nvarchar(20),
    @ICNo nvarchar(20),
    @EmailID nvarchar(60),
	@ContactNo nvarchar(20),
    @CreatedBy nvarchar(50),
    @ModifiedBy nvarchar(50),
	@BranchID Int,
	@RoleCode nvarchar(30)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Security].[User] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [UserID] = @UserID)>0
	BEGIN
	    Exec [Security].[usp_UserUpdate] 
				@CompanyCode, @UserID, @UserName, @Password, @UserGroup, @UserDesignation, @EmployeeID, @ICNo, @EmailID,@ContactNo, @CreatedBy, @ModifiedBy, @BranchID, @RoleCode


	END
	ELSE
	BEGIN
	    Exec [Security].[usp_UserInsert] 
			@CompanyCode, @UserID, @UserName, @Password, @UserGroup, @UserDesignation, @EmployeeID, @ICNo, @EmailID,@ContactNo, @CreatedBy, @ModifiedBy, @BranchID, @RoleCode


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Security].usp_[UserSave]
-- ========================================================================================================================================




GO




-- ========================================================================================================================================
-- START											 [Security].[usp_UserDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [User] Record  based on [User]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserDelete] 
END 
GO
CREATE PROC [Security].[usp_UserDelete] 
    @CompanyCode smallint,
    @UserID nvarchar(50)
AS 

	
BEGIN

	UPDATE	[Security].[User]
	SET	IsActive = CAST(0 as bit)
	WHERE 	[CompanyCode] = @CompanyCode
	       AND [UserID] = @UserID

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserDelete]
-- ========================================================================================================================================

GO

 
  

-- ========================================================================================================================================
-- START											 [Security].[usp_UserEmail]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Get Email and Merchant code

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserEmail]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserEmail]
END 
GO
CREATE PROC [Security].[usp_UserEmail] 
    @CompanyCode smallint
AS 

	
BEGIN

	SELECT	Usr.[CompanyCode],  Usr.EmailID, Mer.MerchantCode

	FROM	[Security].[User] Usr
	 Left Outer Join Master.Merchant Mer ON
		Usr.[CompanyCode] = Mer.[CompanyCode]
		AND Mer.[MerchantName] = (Select CompanyName from Master.Company where CompanyCode=@CompanyCode)
	WHERE  Usr.[CompanyCode] = @CompanyCode
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserEmail] 
-- ========================================================================================================================================

GO
GO

 