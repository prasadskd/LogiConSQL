
-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [UserRights] Record based on [UserRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRightsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsSelect] 
END 
GO
CREATE PROC [Security].[usp_UserRightsSelect] 
    @UserID NVARCHAR(60),
    @SecurableItem SMALLINT,
    @LinkGroup SMALLINT,
    @LinkID NVARCHAR(255)
AS 

BEGIN

	;with SecurableItemDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='SecurableItem'),
	LinkGroupDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='LinkGroup')
	SELECT S.[UserID], S.[SecurableItem], S.[LinkGroup], S.[LinkID] ,
	ISNULL(SI.LookupDescription,'') As SecurableItemDescription,
	ISNULL(LG.LookupDescription,'') As LinkGroupDescription
	FROM   [Security].[UserRights] S
	Left Outer Join SecurableItemDescription SI ON 
		S.SecurableItem = SI.LookupID
	Left Outer Join LinkGroupDescription LG ON 
		S.LinkGroup = LG.LookupID
	WHERE  S.[UserID] = @UserID  
	       AND S.[SecurableItem] = @SecurableItem  
	       AND S.[LinkGroup] = @LinkGroup  
	       AND S.[LinkID] = @LinkID  
END
-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [UserRights] Records from [UserRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRightsList]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsList] 
END 
GO
CREATE PROC [Security].[usp_UserRightsList] 
	@UserID NVARCHAR(60)
AS 
BEGIN

		;with SecurableItemDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='SecurableItem'),
	LinkGroupDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='LinkGroup')
	SELECT S.[UserID], S.[SecurableItem], S.[LinkGroup], S.[LinkID] ,
	ISNULL(SI.LookupDescription,'') As SecurableItemDescription,
	ISNULL(LG.LookupDescription,'') As LinkGroupDescription
	FROM   [Security].[UserRights] S
	Left Outer Join SecurableItemDescription SI ON 
		S.SecurableItem = SI.LookupID
	Left Outer Join LinkGroupDescription LG ON 
		S.LinkGroup = LG.LookupID
	WHERE  S.[UserID] = @UserID  

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [UserRights] Records from [UserRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRightsPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsPageView] 
END 
GO
CREATE PROC [Security].[usp_UserRightsPageView] 
	@UserID NVARCHAR(60),
	@fetchrows bigint
AS 
BEGIN

	;with SecurableItemDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='SecurableItem'),
	LinkGroupDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='LinkGroup')
	SELECT S.[UserID], S.[SecurableItem], S.[LinkGroup], S.[LinkID] ,
	ISNULL(SI.LookupDescription,'') As SecurableItemDescription,
	ISNULL(LG.LookupDescription,'') As LinkGroupDescription
	FROM   [Security].[UserRights] S
	Left Outer Join SecurableItemDescription SI ON 
		S.SecurableItem = SI.LookupID
	Left Outer Join LinkGroupDescription LG ON 
		S.LinkGroup = LG.LookupID
	WHERE  S.[UserID] = @UserID  
	ORDER BY [UserID] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsPageView] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [UserRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRightsRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsRecordCount] 
END 
GO
CREATE PROC [Security].[usp_UserRightsRecordCount] 
	@UserID NVARCHAR(60)
AS 
BEGIN

	SELECT COUNT(0) 
	FROM   [Security].[UserRights]
	WHERE  [UserID] = @UserID  


END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsRecordCount] 
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [UserRights] Record based on [UserRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_UserRightsAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsAutoCompleteSearch] 
END 
GO
CREATE PROC [Security].[usp_UserRightsAutoCompleteSearch] 
    @UserID NVARCHAR(60)
     
AS 

BEGIN

	;with SecurableItemDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='SecurableItem'),
	LinkGroupDescription As (Select LookupID,LookupDescription From Config.LookUp Where LookupCategory ='LinkGroup')
	SELECT S.[UserID], S.[SecurableItem], S.[LinkGroup], S.[LinkID] ,
	ISNULL(SI.LookupDescription,'') As SecurableItemDescription,
	ISNULL(LG.LookupDescription,'') As LinkGroupDescription
	FROM   [Security].[UserRights] S
	Left Outer Join SecurableItemDescription SI ON 
		S.SecurableItem = SI.LookupID
	Left Outer Join LinkGroupDescription LG ON 
		S.LinkGroup = LG.LookupID
	
	WHERE  [UserID] LIKE '%' +  @UserID + '%'
END
-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsAutoCompleteSearch]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Inserts the [UserRights] Record Into [UserRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserRightsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsInsert] 
END 
GO
CREATE PROC [Security].[usp_UserRightsInsert] 
    @UserID nvarchar(60),
    @SecurableItem smallint,
    @LinkGroup smallint,
    @LinkID nvarchar(255)
AS 
  

BEGIN

	
	INSERT INTO [Security].[UserRights] ([UserID], [SecurableItem], [LinkGroup], [LinkID])
	SELECT @UserID, @SecurableItem, @LinkGroup, @LinkID
               
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsInsert]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	updates the [UserRights] Record Into [UserRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserRightsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsUpdate] 
END 
GO
CREATE PROC [Security].[usp_UserRightsUpdate] 
    @UserID nvarchar(60),
    @SecurableItem smallint,
    @LinkGroup smallint,
    @LinkID nvarchar(255)
AS 
 
	
BEGIN

	UPDATE [Security].[UserRights]
	SET    [UserID] = @UserID, [SecurableItem] = @SecurableItem, [LinkGroup] = @LinkGroup, [LinkID] = @LinkID
	WHERE  [UserID] = @UserID
	       AND [SecurableItem] = @SecurableItem
	       AND [LinkGroup] = @LinkGroup
	       AND [LinkID] = @LinkID
	
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Either INSERT or UPDATE the [UserRights] Record Into [UserRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserRightsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsSave] 
END 
GO
CREATE PROC [Security].[usp_UserRightsSave] 
    @UserID nvarchar(60),
    @SecurableItem smallint,
    @LinkGroup smallint,
    @LinkID nvarchar(255)
AS 
 

BEGIN
	IF (SELECT COUNT(0) FROM [Security].[UserRights] 
		WHERE 	[UserID] = @UserID
	       AND [SecurableItem] = @SecurableItem
	       AND [LinkGroup] = @LinkGroup
	       AND [LinkID] = @LinkID)>0
	BEGIN
	    Exec [Security].[usp_UserRightsUpdate] 
		@UserID, @SecurableItem, @LinkGroup, @LinkID


	END
	ELSE
	BEGIN
	    Exec [Security].[usp_UserRightsInsert] 
		@UserID, @SecurableItem, @LinkGroup, @LinkID


	END
	
END

	

-- ========================================================================================================================================
-- END  											 [Security].usp_[UserRightsSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_UserRightsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Deletes the [UserRights] Record  based on [UserRights]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_UserRightsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_UserRightsDelete] 
END 
GO
CREATE PROC [Security].[usp_UserRightsDelete] 
    @UserID nvarchar(60),
    @SecurableItem smallint,
    @LinkGroup smallint,
    @LinkID nvarchar(255)
AS 

	
BEGIN

	
	-- Use the SOFT DELETE.
	DELETE
	FROM   [Security].[UserRights]
	WHERE  [UserID] = @UserID
	       AND [SecurableItem] = @SecurableItem
	       AND [LinkGroup] = @LinkGroup
	       AND [LinkID] = @LinkID
	 


END

-- ========================================================================================================================================
-- END  											 [Security].[usp_UserRightsDelete]
-- ========================================================================================================================================

GO
