 

-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [Securables] Record based on [Securables] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_SecurablesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesSelect] 
END 
GO
CREATE PROC [Security].[usp_SecurablesSelect] 
    @RegistrationType bigint,
    @PageID nvarchar(100),
    @SecurableItem nvarchar(100)
AS 
 

BEGIN

	SELECT [RegistrationType], [PageID], [SecurableItem], [Description] 
	FROM   [Security].[Securables]
	WHERE  [RegistrationType] = @RegistrationType 
	       AND [PageID] = @PageID 
	       AND [SecurableItem] = @SecurableItem 

END
-- ========================================================================================================================================
-- END  											 [Security].[usp_SecurablesSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [Securables] Records from [Securables] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_SecurablesList]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesList] 
END 
GO
CREATE PROC [Security].[usp_SecurablesList] 

AS 
 
BEGIN
	SELECT [RegistrationType], [PageID], [SecurableItem], [Description] 
	FROM   [Security].[Securables]

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_SecurablesList] 
-- ========================================================================================================================================

GO
 
-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [Securables] Record Into [Securables] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_SecurablesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesInsert] 
END 
GO
CREATE PROC [Security].[usp_SecurablesInsert] 
    @RegistrationType bigint,
    @PageID nvarchar(100),
    @SecurableItem nvarchar(100),
    @Description nvarchar(255) = NULL
AS 
  

BEGIN
	
	INSERT INTO [Security].[Securables] ([RegistrationType], [PageID], [SecurableItem], [Description])
	SELECT @RegistrationType, @PageID, @SecurableItem, @Description
	
               
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_SecurablesInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [Securables] Record Into [Securables] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_SecurablesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesUpdate] 
END 
GO
CREATE PROC [Security].[usp_SecurablesUpdate] 
    @RegistrationType bigint,
    @PageID nvarchar(100),
    @SecurableItem nvarchar(100),
    @Description nvarchar(255) = NULL
AS 
 
	
BEGIN

	UPDATE [Security].[Securables]
	SET    [RegistrationType] = @RegistrationType, [PageID] = @PageID, [SecurableItem] = @SecurableItem, [Description] = @Description
	WHERE  [RegistrationType] = @RegistrationType
	       AND [PageID] = @PageID
	       AND [SecurableItem] = @SecurableItem
	

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_SecurablesUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [Securables] Record Into [Securables] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_SecurablesSave]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesSave] 
END 
GO
CREATE PROC [Security].[usp_SecurablesSave] 
    @RegistrationType bigint,
    @PageID nvarchar(100),
    @SecurableItem nvarchar(100),
    @Description nvarchar(255) = NULL
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Security].[Securables] 
		WHERE 	[RegistrationType] = @RegistrationType
	       AND [PageID] = @PageID
	       AND [SecurableItem] = @SecurableItem)>0
	BEGIN
	    Exec [Security].[usp_SecurablesUpdate] 
		@RegistrationType, @PageID, @SecurableItem, @Description


	END
	ELSE
	BEGIN
	    Exec [Security].[usp_SecurablesInsert] 
		@RegistrationType, @PageID, @SecurableItem, @Description


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Security].usp_[SecurablesSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Security].[usp_SecurablesDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [Securables] Record  based on [Securables]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_SecurablesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_SecurablesDelete] 
END 
GO
CREATE PROC [Security].[usp_SecurablesDelete] 
    @RegistrationType bigint,
    @PageID nvarchar(100),
    @SecurableItem nvarchar(100)
AS 

	
BEGIN

	 
	DELETE
	FROM   [Security].[Securables]
	WHERE  [RegistrationType] = @RegistrationType
	       AND [PageID] = @PageID
	       AND [SecurableItem] = @SecurableItem
	 
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_SecurablesDelete]
-- ========================================================================================================================================

GO 