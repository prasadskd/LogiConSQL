

-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [RoleRights] Record based on [RoleRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_RoleRightsSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsSelect] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsSelect] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @SecurableItem nvarchar(100)
AS 
 

BEGIN

	SELECT [CompanyCode], [RoleCode], [SecurableItem] 
	FROM   [Security].[RoleRights]
	WHERE  [CompanyCode] = @CompanyCode   
	       AND [RoleCode] = @RoleCode   
	       AND [SecurableItem] = @SecurableItem  
END
-- ========================================================================================================================================
-- END  											 [Security].[usp_RoleRightsSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [RoleRights] Records from [RoleRights] table
-- ========================================================================================================================================


IF OBJECT_ID('[Security].[usp_RoleRightsList]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsList] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsList] 
    @CompanyCode int

AS 
 
BEGIN
	SELECT [CompanyCode], [RoleCode], [SecurableItem] 
	FROM   [Security].[RoleRights]
	WHERE  [CompanyCode] = @CompanyCode   

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RoleRightsList] 
-- ========================================================================================================================================

GO

 
-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [RoleRights] Record Into [RoleRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RoleRightsInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsInsert] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsInsert] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @SecurableItem nvarchar(100)
AS 
  

BEGIN
	
	INSERT INTO [Security].[RoleRights] ([CompanyCode], [RoleCode], [SecurableItem])
	SELECT @CompanyCode, @RoleCode, @SecurableItem
	
               
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RoleRightsInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [RoleRights] Record Into [RoleRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RoleRightsUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsUpdate] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsUpdate] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @SecurableItem nvarchar(100)
AS 
 
	
BEGIN

	UPDATE [Security].[RoleRights]
	SET    [CompanyCode] = @CompanyCode, [RoleCode] = @RoleCode, [SecurableItem] = @SecurableItem
	WHERE  [CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode
	       AND [SecurableItem] = @SecurableItem
	

END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RoleRightsUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [RoleRights] Record Into [RoleRights] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RoleRightsSave]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsSave] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsSave] 
    @CompanyCode int,
    @RoleCode nvarchar(30),
    @SecurableItem nvarchar(100)
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Security].[RoleRights] 
		WHERE 	[CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode
	       AND [SecurableItem] = @SecurableItem)>0
	BEGIN
	    Exec [Security].[usp_RoleRightsUpdate] 
		@CompanyCode, @RoleCode, @SecurableItem


	END
	ELSE
	BEGIN
	    Exec [Security].[usp_RoleRightsInsert] 
		@CompanyCode, @RoleCode, @SecurableItem


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Security].usp_[RoleRightsSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Security].[usp_RoleRightsDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [RoleRights] Record  based on [RoleRights]

-- ========================================================================================================================================

IF OBJECT_ID('[Security].[usp_RoleRightsDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Security].[usp_RoleRightsDelete] 
END 
GO
CREATE PROC [Security].[usp_RoleRightsDelete] 
    @CompanyCode int,
    @RoleCode nvarchar(30) 
AS 

	
BEGIN

	 
	DELETE
	FROM   [Security].[RoleRights]
	WHERE  [CompanyCode] = @CompanyCode
	       AND [RoleCode] = @RoleCode
	       
	 
END

-- ========================================================================================================================================
-- END  											 [Security].[usp_RoleRightsDelete]
-- ========================================================================================================================================

GO 