
/****** Object:  Table [Port].[BookingMovement]    Script Date: 17-02-2017 13:52:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[BookingMovement](
	[BranchID] [bigint] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[ContainerKey] [nvarchar](50) NOT NULL,
	[MovementCode] [nvarchar](50) NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[MovementDate] [datetime] NULL,
	[TransactionNo] [nvarchar](50) NULL,
	[TruckTransactionNo] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_BookingMovement] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC,
	[MovementCode] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Port].[BookingMovement] ADD  CONSTRAINT [DF_BookingMovement_Satus]  DEFAULT ((1)) FOR [Status]
GO

