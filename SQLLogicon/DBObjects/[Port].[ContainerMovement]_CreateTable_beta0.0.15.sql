 

/****** Object:  Table [Port].[ContainerMovement]    Script Date: 20-02-2017 16:29:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[ContainerMovement](
	[BranchID] [bigint] NOT NULL,
	[DocumentNo] [nvarchar](50) NOT NULL,
	[TruckTransactionNo] [nvarchar](50) NULL,
	[OrderNo] [nvarchar](50) NULL,
	[ContainerKey] [nvarchar](50) NULL,
	[MovementCode] [nvarchar](50) NULL,
	[MovementType] [smallint] NULL,
	[EFIndicator] [smallint] NULL,
	[BookingType] [smallint] NULL,
	[AgentCode] [bigint] NULL,
	[CustomerCode] [bigint] NULL,
	[VesselID] [nvarchar](20) NULL,
	[VesselName] [nvarchar](100) NULL,
	[VoyageNo] [nvarchar](20) NULL,
	[ShipCallNo] [nvarchar](20) NULL,
	[ContainerNo] [nvarchar](15) NULL,
	[Size] [nvarchar](10) NULL,
	[Type] [nvarchar](10) NULL,
	[ContainerGrade] [smallint] NULL,
	[ContainerStatus] [nvarchar](20) NULL,
	[Temperature] [decimal](18, 2) NULL,
	[TemperatureType] [smallint] NULL,
	[Vent] [decimal](18, 2) NULL,
	[VentType] [smallint] NULL,
	[Material] [smallint] NULL,
	[Height] [smallint] NULL,
	[TareWeight] [decimal](18, 4) NULL,
	[MGW] [decimal](18, 4) NULL,
	[SealNo1] [nvarchar](20) NULL,
	[SealNo2] [nvarchar](20) NULL,
	[YearBuilt] [nvarchar](10) NULL,
	[Remarks] [nvarchar](100) NULL,
	[Status] [bit] NULL,
	[EDIDateTime] [datetime] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_ContainerMovement_1] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DocumentNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

