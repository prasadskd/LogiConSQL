/****** Object:  Table [Master].[TariffHeader]    Script Date: 12-02-2017 10:18:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[TariffHeader](
	[QuotationNo] [nvarchar](50) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[ExpiryDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_TariffHeader] PRIMARY KEY CLUSTERED 
(
	[QuotationNo] ASC,
	[EffectiveDate] ASC,
	[ExpiryDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Master].[TariffHeader] ADD  CONSTRAINT [DF_TariffHeader_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Master].[TariffHeader] ADD  CONSTRAINT [DF_TariffHeader_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Master].[TariffHeader] ADD  CONSTRAINT [DF_TariffHeader_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


