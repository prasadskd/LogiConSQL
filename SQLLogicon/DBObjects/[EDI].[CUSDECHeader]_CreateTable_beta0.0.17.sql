 

ALTER TABLE [EDI].[CUSDECHeader] DROP CONSTRAINT [DF_CUSDECHeader_IsUploaded]
GO

ALTER TABLE [EDI].[CUSDECHeader] DROP CONSTRAINT [DF_CUSDECHeader_CreatedOn]
GO

ALTER TABLE [EDI].[CUSDECHeader] DROP CONSTRAINT [DF_CUSDECHeader_MessageType]
GO

ALTER TABLE [EDI].[CUSDECHeader] DROP CONSTRAINT [DF_CUSDECHeader_DocumentType]
GO

ALTER TABLE [EDI].[CUSDECHeader] DROP CONSTRAINT [DF_CUSDECHeader_FileName]
GO

/****** Object:  Table [EDI].[CUSDECHeader]    Script Date: 26-02-2017 22:24:46 ******/
DROP TABLE [EDI].[CUSDECHeader]
GO

/****** Object:  Table [EDI].[CUSDECHeader]    Script Date: 26-02-2017 22:24:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[CUSDECHeader](
	[BranchID] [bigint] NOT NULL,
	[ReferenceNo] [bigint] IDENTITY(10000,1) NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](100) NULL,
	[DocumentType] [smallint] NULL,
	[MessageType] [tinyint] NULL,
	[CreatedOn] [datetime] NULL,
	[IsUploaded] [bit] NULL,
	[EDIDateTime] [datetime] NULL,
	[MovementType] [nvarchar](5) NOT NULL,
 CONSTRAINT [PK_CUSDECHeader] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ReferenceNo] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [EDI].[CUSDECHeader] ADD  CONSTRAINT [DF_CUSDECHeader_FileName]  DEFAULT ('') FOR [FileName]
GO

ALTER TABLE [EDI].[CUSDECHeader] ADD  CONSTRAINT [DF_CUSDECHeader_DocumentType]  DEFAULT ((0)) FOR [DocumentType]
GO

ALTER TABLE [EDI].[CUSDECHeader] ADD  CONSTRAINT [DF_CUSDECHeader_MessageType]  DEFAULT ((0)) FOR [MessageType]
GO

ALTER TABLE [EDI].[CUSDECHeader] ADD  CONSTRAINT [DF_CUSDECHeader_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [EDI].[CUSDECHeader] ADD  CONSTRAINT [DF_CUSDECHeader_IsUploaded]  DEFAULT ((0)) FOR [IsUploaded]
GO

