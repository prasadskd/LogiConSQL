 
/****** Object:  Table [EDI].[K1Declaration]    Script Date: 27-02-2017 18:18:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[K1Declaration](
	[BranchID] [bigint] NOT NULL,
	[ReferenceNo] [bigint] IDENTITY(1000,1) NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[EDIDateTime] [datetime] NULL,
	[FileName] [nvarchar](100) NULL,
 CONSTRAINT [PK_K1Declaration] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ReferenceNo] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

