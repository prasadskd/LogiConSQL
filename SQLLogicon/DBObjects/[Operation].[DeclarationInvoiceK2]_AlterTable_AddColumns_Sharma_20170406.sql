ALTER TABLE [Operation].[DeclarationInvoiceK2]
ADD 
[FOBAmount] decimal(18,7) NULL,
[CIFAmount]  decimal(18,7) NULL,
[EXWAmount]  decimal(18,7) NULL,
[CNFAmount]  decimal(18,7) NULL,
[CNIAmount]  decimal(18,7) NULL,
[FreightAmountItem]  decimal(18,7) NULL,
[InsuranceAmountItem]  decimal(18,7) NULL,
[CIFCAmount]  decimal(18,7) NULL



