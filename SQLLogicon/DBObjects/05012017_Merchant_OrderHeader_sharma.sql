ALTER TABLE [Master].[Merchant]
ADD IsFreightForwarder BIT NULL,
IsOverseasAgent BIT NULL
GO


ALTER TABLE [Operation].[OrderHeader]
ADD OverseasAgentCode BIGINT NULL,
OverseasAgentAddressID INT NULL,
FregihtForwarderCode BIGINT NULL,
FreightForwarderAddressID INT NULL
GO