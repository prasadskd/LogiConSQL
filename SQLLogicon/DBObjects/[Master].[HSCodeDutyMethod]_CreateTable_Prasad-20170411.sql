SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[HSCodeDutyMethod](
	[HSCodeDutyMethodID] [int] NOT NULL,
	[HSCodeDutyMethodDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_Master]].[HSCodeDutyMethod] PRIMARY KEY CLUSTERED 
(
	[HSCodeDutyMethodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


