

/****** Object:  Table [Operation].[DeclarationHeader]    Script Date: 16-05-2017 08:17:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationHeaderK8](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[DeclarationDate] [datetime] NULL,
	[DeclarationType] [smallint] NULL,
	[OpenDate] [datetime] NULL,
	[ImportDate] [datetime] NULL,
	[OrderNo] [nvarchar](50) NULL,
	[TransportMode] [smallint] NULL,
	[ShipmentType] [smallint] NULL,
	[TransactionType] [smallint] NULL,
	[Importer] [bigint] NULL,
	[Exporter] [nvarchar](50) NULL,
	[ExporterAddress1] [nvarchar](50) NULL,
	[ExporterAddress2] [nvarchar](50) NULL,
	[ExporterCity] [nvarchar](50) NULL,
	[ExporterState] [nvarchar](50) NULL,
	[ExporterCountry] [nvarchar](2) NULL,
	[ExporterOrganizationType] [smallint] NULL,
	[ExporterTelNo] [nvarchar](20) NULL,
	[SMKCode] [smallint] NOT NULL,
	[CustomStationCode] [nvarchar](10) NULL,
	[ShippingAgent] [bigint] NULL,
	[DeclarantID] [nvarchar](35) NULL,
	[DeclarantName] [nvarchar](35) NULL,
	[DeclarantDesignation] [nvarchar](35) NULL,
	[DeclarantNRIC] [nvarchar](35) NULL,
	[DeclarantAddress1] [nvarchar](35) NULL,
	[DeclarantAddress2] [nvarchar](35) NULL,
	[DeclarantAddressCity] [nvarchar](35) NULL,
	[DeclarantAddressState] [nvarchar](35) NULL,
	[DeclarantAddressCountry] [nvarchar](2) NULL,
	[DeclarantAddressPostCode] [nvarchar](10) NULL,
	[CargoClass] [smallint] NULL,
	[CargoDescription] [nvarchar](150) NULL,
	[CustomerReferenceNo] [nvarchar](50) NULL,
	[ExtraCargoDescription] [nvarchar](280) NULL,
	[MarksAndNos] [nvarchar](350) NULL,
	[DeclarationShipmentType] [smallint] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsApproved] [bit] NULL,
	[ApprovedBy] [nvarchar](60) NULL,
	[ApprovedOn] [datetime] NULL,

 CONSTRAINT [PK_DeclarationHeaderK8] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
