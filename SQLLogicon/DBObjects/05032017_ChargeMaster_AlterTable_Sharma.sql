Alter Table [Master].[ChargeMaster]
Add
IsPreLoadExport bit NULL,
IsPreLoadImport bit NULL,
IsPreLoadLocal bit NULL,
IsJobCategory bit NULL,
IsProduct bit NULL,
IsInternal bit NULL,
IsRevenue bit NULL,
IsVAS bit NULL,
IsPreLoadInward	 bit NULL,
IsPreLoadOutward bit NULL,
IsLoadToTruckIn bit NULL,
IsCrossDock bit NULL,
IsWorkOrder bit NULL,
IsRequireApproval bit NULL,
IsLoadFullPallet bit NULL
GO

 