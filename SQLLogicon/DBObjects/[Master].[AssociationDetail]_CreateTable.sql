 
/****** Object:  Table [Master].[AssociationDetail]    Script Date: 10-02-2017 16:31:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[AssociationDetail](
	[AssociationID] [smallint] NOT NULL,
	[MerchantCode] [bigint] NOT NULL,
	[JoinDate] [datetime] NULL,
 CONSTRAINT [PK_AssociationDetail] PRIMARY KEY CLUSTERED 
(
	[AssociationID] ASC,
	[MerchantCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


