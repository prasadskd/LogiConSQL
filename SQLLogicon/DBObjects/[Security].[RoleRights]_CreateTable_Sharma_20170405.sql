/****** Object:  Table [Security].[RoleRights]    Script Date: 05-04-2017 14:52:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Security].[RoleRights](
	[CompanyCode] [int] NOT NULL,
	[RoleCode] [nvarchar](30) NOT NULL,
	[SecurableItem] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_RoleRights] PRIMARY KEY CLUSTERED 
(
	[CompanyCode] ASC,
	[RoleCode] ASC,
	[SecurableItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

