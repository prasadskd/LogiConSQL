
/****** Object:  Table [Port].[BookingHeader]    Script Date: 17-02-2017 17:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[BookingHeader](
	[BranchID] [bigint] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[BookingNo] [nvarchar](50) NULL,
	[BLNo] [nvarchar](50) NULL,
	[OrderDate] [datetime] NULL,
	[BookingType] [smallint] NOT NULL,
	[TransportType] [smallint] NOT NULL,
	[OrderType] [nvarchar](15) NOT NULL,
	[CustomerCode] [bigint] NOT NULL,
	[CustomerName] [nvarchar](50) NULL,
	[ForwarderCode] [bigint] NULL,
	[ForwarderName] [nvarchar](50) NULL,
	[ShippingAgent] [bigint] NULL,
	[ShippingAgentName] [nvarchar](50) NULL,
	[OwnerCode] [bigint] NULL,
	[LoadingPort] [nvarchar](10) NULL,
	[DischargePort] [nvarchar](10) NULL,
	[DestinationPort] [nvarchar](10) NULL,
	[VesselScheduleID] [int] NOT NULL,
	[VesselID] [nvarchar](20) NULL,
	[VesselName] [nvarchar](100) NULL,
	[VoyageNo] [nvarchar](20) NULL,
	[ShipCallNo] [nvarchar](20) NULL,
	[Wharf] [bigint] NULL,
	[IsAllowLateGate] [bit] NULL,
	[ETA] [datetime] NULL,
	[ETD] [datetime] NULL,
	[PortCutOffDry] [datetime] NULL,
	[PortCutOffReefer] [datetime] NULL,
	[FlightNo] [nvarchar](35) NULL,
	[ARNNo] [nvarchar](35) NULL,
	[VehicleNo1] [nvarchar](35) NULL,
	[VehicleNo2] [nvarchar](35) NULL,
	[WagonNo] [nvarchar](35) NULL,
	[JKNo] [nvarchar](35) NULL,
	[TotalQty] [nvarchar](20) NULL,
	[UOM] [nvarchar](20) NULL,
	[TotalVolume] [nvarchar](20) NULL,
	[TotalWeight] [nvarchar](20) NULL,
	[Commodity] [nvarchar](200) NULL,
	[MarksNos] [nvarchar](200) NULL,
	[SpecialInstructions] [nvarchar](200) NULL,
	[Remarks] [nvarchar](200) NULL,
	[IsEDI] [bit] NULL,
	[IsCancel] [bit] NULL,
	[IsBillable] [bit] NULL,
	[IsPrinted] [bit] NULL,
	[EDIDateTime] [datetime] NULL,
	[CancelDateTime] [datetime] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[RelationBranchID] [bigint] NULL,
 CONSTRAINT [PK_BookingHeader] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[OrderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

