 

ALTER TABLE [Operation].[OrderTruckMovement] DROP CONSTRAINT [DF_OrderTruckMovement_DriverID1]
GO

ALTER TABLE [Operation].[OrderTruckMovement] DROP CONSTRAINT [DF_OrderTruckMovement_TrailerID]
GO

/****** Object:  Table [Operation].[OrderTruckMovement]    Script Date: 05-05-2017 17:42:26 ******/
DROP TABLE [Operation].[OrderTruckMovement]
GO

/****** Object:  Table [Operation].[OrderTruckMovement]    Script Date: 05-05-2017 17:42:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[OrderTruckMovement](
	[BranchID] [bigint] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[ContainerKey] [nvarchar](50) NOT NULL,
	[MovementCode] [nvarchar](50) NOT NULL,
	[MovementSeqNo] [smallint] NOT NULL,
	[FromLocationCode] [nvarchar](10) NULL,
	[FromLocationAddressID] [int] NULL,
	[ToLocationCode] [nvarchar](10) NULL,
	[ToLocationAddressID] [int] NULL,
	[RequiredDate] [datetime] NULL,
	[PlannedBy] [nvarchar](60) NULL,
	[PlannedDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TruckID] [nvarchar](20) NULL,
	[TrailerID] [nvarchar](20) NULL,
	[DriverID1] [nvarchar](60) NULL,
	[DriverID2] [nvarchar](60) NULL,
	[Remarks] [nvarchar](200) NULL,
	[MovementStatus] [smallint] NULL,
	[Size] [nvarchar](2) NULL,
	[Type] [nvarchar](2) NULL,
	[IsDropOff] [bit] NULL,
	[IsRequireEDI] [bit] NULL,
	[TransactionNo] [nvarchar](50) NULL,
	[MasterJobBranchID] [bigint] NULL,
	[MasterJobNo] [nvarchar](50) NULL,
	[MasterItemNo] [nvarchar](50) NULL,
	[ContractorCode] [bigint] NULL,
	[AssignedDate] [datetime] NULL,
	[EDIDateTime] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_OrderTruckMovement_1] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC,
	[MovementCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Operation].[OrderTruckMovement] ADD  CONSTRAINT [DF_OrderTruckMovement_TrailerID]  DEFAULT ('') FOR [TrailerID]
GO

ALTER TABLE [Operation].[OrderTruckMovement] ADD  CONSTRAINT [DF_OrderTruckMovement_DriverID1]  DEFAULT ('') FOR [DriverID1]
GO

