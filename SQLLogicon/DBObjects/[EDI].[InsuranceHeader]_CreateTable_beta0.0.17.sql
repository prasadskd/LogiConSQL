SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[InsuranceHeader](
	[BranchID] [bigint] NOT NULL,
	[InsuranceReferenceNo] [bigint] IDENTITY(10000,1) NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[EDIDateTime] [datetime] NULL,
	[FileName] [nvarchar](50) NULL,
 CONSTRAINT [PK_InsuranceHeader] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[InsuranceReferenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


