 

/****** Object:  Table [Port].[BookingContainer]    Script Date: 17-02-2017 13:51:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[BookingContainer](
	[BranchID] [bigint] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[ContainerKey] [nvarchar](50) NOT NULL,
	[ContainerNo] [nvarchar](15) NOT NULL,
	[Size] [nvarchar](10) NOT NULL,
	[Type] [nvarchar](10) NOT NULL,
	[ContainerGrade] [smallint] NULL,
	[EFIndicator] [smallint] NULL,
	[IMOCode] [nvarchar](20) NULL,
	[UNNo] [nvarchar](20) NULL,
	[Temperature] [decimal](18, 2) NULL,
	[TempratureType] [smallint] NULL,
	[Vent] [decimal](18, 2) NULL,
	[VentType] [smallint] NULL,
	[CargoCategory] [smallint] NULL,
	[PickupDate] [datetime] NULL,
	[SealNo1] [nvarchar](20) NULL,
	[SealNo2] [nvarchar](20) NULL,
	[VGM] [decimal](18, 4) NULL,
	[StowageCell] [nvarchar](20) NULL,
	[PUDOMode] [smallint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[Commodity] [nvarchar](max) NULL,
	[SpecialInstructions] [nvarchar](max) NULL,
 CONSTRAINT [PK_BookingContainer] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [DF_BookingContainer_ContainerNo]  DEFAULT ('') FOR [ContainerNo]
GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [DF_BookingContainer_Size]  DEFAULT ('') FOR [Size]
GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [DF_BookingContainer_Type]  DEFAULT ('') FOR [Type]
GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [DF_BookingContainer_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [[DF_BookingContainer_Commodity]  DEFAULT ('') FOR [Commodity]
GO

ALTER TABLE [Port].[BookingContainer] ADD  CONSTRAINT [DF_BookingContainer_SpecialInstructions]  DEFAULT ('') FOR [SpecialInstructions]
GO

