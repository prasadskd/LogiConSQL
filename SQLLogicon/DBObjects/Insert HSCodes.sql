--DELETE FROM MASTER.HSCode


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '101210000','Purebred breeding animals','UNT',0,0,0 UNION ALL
Select '0101290000','Other','UNT',0,0,0 UNION ALL
Select '0101301000','Purebred breeding animals','UNT',0,0,0 UNION ALL
Select '0101309000','Other','UNT',0,0,0 UNION ALL
Select '0101900000','Other','UNT',0,0,0 UNION ALL
Select '0102210000','Purebred breeding animals','UNT',0,5,0 UNION ALL
Select '0102291100','Oxen','UNT',0,0,0 UNION ALL
Select '0102291900','Other','UNT',0,5,0 UNION ALL
Select '0102299000','Other','UNT',0,5,0 UNION ALL
Select '0102310000','Purebred breeding animals','UNT',0,5,0 UNION ALL
Select '0102390000','Other','UNT',0,5,0 UNION ALL
Select '0102901000','Purebred breeding animals','UNT',0,5,0 UNION ALL
Select '0102909000','Other','UNT',0,5,0 UNION ALL
Select '0103100000','Purebred breeding animals','UNT',0,0,0 UNION ALL
Select '0103910000','Weighing less than 50 kg','UNT',15,0,0 UNION ALL
Select '0103920000','Weighing 50 kg or more','UNT',15,0,0 UNION ALL
Select '0104101000','Purebred breeding animals','UNT',0,0,0 UNION ALL
Select '0104109000','Other','UNT',0,0,0 UNION ALL
Select '0104201000','Purebred breeding animals','UNT',0,5,0 UNION ALL
Select '0104209000','Other','UNT',0,5,0 UNION ALL
Select '0105111000','Breeding fowls','UNT',0,0,0 UNION ALL
Select '0105119000','Other','UNT',10 ,0,0 UNION ALL
Select '0105121000','Breeding turkeys','UNT',0,0,0 UNION ALL
Select '0105129000','Other','UNT',0,0,0 UNION ALL
Select '0105131000','Breeding ducklings','UNT',0,0,0 UNION ALL
Select '0105139000','Other','UNT',0,0,0 UNION ALL
Select '0105141000','Breeding goslings','UNT',0,0,0 UNION ALL
Select '0105149000','Other','UNT',0,0,0 UNION ALL
Select '0105151000','Breeding guinea fowls','UNT',0,0,0 UNION ALL
Select '0105159000','Other','UNT',0,0,0 UNION ALL
Select '0105941000','Breeding fowls, other than fighting cocks','UNT',0,0,0 UNION ALL
Select '0105944100','Weighing not more than 2 kg','UNT',10 ,0,0 UNION ALL
Select '0105944900','Other','UNT',0,0,0 UNION ALL
Select '0105949100','Weighing not more than 2 kg','UNT',10,0,0 UNION ALL
Select '0105949900','Other','UNT',0,0,0 UNION ALL

Select '0105991000','Breeding ducks','UNT',0,0,0 UNION ALL
Select '0105992000','Other ducks','UNT',0,0,0 UNION ALL
Select '0105993000','Breeding geese, turkeys and guinea fowls','UNT',0,0,0 UNION ALL
Select '0105994000','Other geese, turkeys and guinea fowls ','UNT',0,0,0 

INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '01061100','Primates:','',0,0,0 UNION ALL
Select '0106110010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106110090','Other','UNT',0,5,0 UNION ALL
Select '01061200','Whales, dolphins and porpoises (mammals of the order Cetacea)','',0,0,0 UNION ALL
Select '0106120010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106120090','Other','UNT',0,5,0 UNION ALL
Select '01061300','Camels and other camelids (Camelidae):','',0,0,0 UNION ALL
Select '0106130010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106130090','Other','UNT',0,5,0 UNION ALL
Select '01061400','Rabbits and hares:','',0,0,0 UNION ALL
Select '0106140010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106140090','Other','UNT',0,5,0 UNION ALL
Select '01061900','Other:','',0,0,0 UNION ALL
Select '0106190010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106190090','Other','UNT',0,5,0 UNION ALL
Select '01062000','Reptiles (including snakes and turtles):','',0,0,0 UNION ALL
Select '0106200010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106200090','Other','UNT',0,5,0 UNION ALL

Select '01063100','Birds of prey:','',0,0,0 UNION ALL
Select '0106310010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106310090','Other','UNT',0,5,0 UNION ALL
Select '01063200','Psittaciformes (including parrots, parakeets, macaws and cockatoos):','',0,0,0 UNION ALL
Select '0106320010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106320090','Other','UNT',0,5,0 UNION ALL
Select '01063300','Ostriches; emus (Dromaius novaehollandiae):','',0,0,0 UNION ALL
Select '0106330010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106330090','Other','UNT',0,5,0 UNION ALL
Select '01063900','Other:','',0,0,0 UNION ALL
Select '0106390010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106390090','Other','UNT',0,5,0 UNION ALL

Select '01064100','Bees:','',0,0,0 UNION ALL
Select '0106410010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106410090','Other','UNT',0,5,0 UNION ALL
Select '01064900','Other:','',0,0,0 UNION ALL
Select '0106490010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106490090','Other','UNT',0,5,0 UNION ALL
Select '01069000','Other:','',0,0,0 UNION ALL
Select '0106900010','For zoos and pets','UNT',0,0,0 UNION ALL
Select '0106900090','Other','UNT',0,5,0 UNION ALL

Select '0201100000','Carcasses and halfcarcasses','KGM',0,0,0 UNION ALL
Select '0201200000','Other cuts with bone in','KGM',0,0,0 UNION ALL
Select '0201300000','Boneless','KGM',0,0,0 UNION ALL

Select '0202100000','Carcasses and halfcarcasses','KGM',0,0,0 UNION ALL
Select '0202200000','Other cuts with bone in','KGM',0,0,0 UNION ALL
Select '0202300000','Boneless','KGM',0,0,0 UNION ALL


Select '0203110000','Carcasses and halfcarcasses','KGM',25 ,0,0 UNION ALL
Select '0203120000','Hams, shoulders and cuts thereof, with bone in','KGM',0,0,0 UNION ALL
Select '0203190000','Other','KGM',0,0,0 UNION ALL

Select '0203210000','Carcasses and halfcarcasses','KGM',25,0,0 UNION ALL
Select '0203220000','Hams, shoulders and cuts thereof, with bone in','KGM',0,0,0 UNION ALL
Select '0203290000','Other','KGM',0,0,0 UNION ALL

Select '0204100000','Carcasses and halfcarcasses of lamb, fresh or chilled','KGM',0,0,0 UNION ALL

Select '0204210000','Carcasses and halfcarcasses','KGM',0,0,0 UNION ALL
Select '0204220000','Other cuts with bone in','KGM',0,0,0 UNION ALL
Select '0204230000','Boneless','KGM',0,0,0 UNION ALL
Select '0204300000','Carcasses and halfcarcasses of lamb, frozen','KGM',0,0,0 UNION ALL

Select '0204410000','Carcasses and halfcarcasses','KGM',0,0,0 UNION ALL
Select '0204420000','Other cuts with bone in','KGM',0,0,0 UNION ALL
Select '0204430000','Boneless','KGM',0,0,0 UNION ALL
Select '0204500000','Meat of goats','KGM',0,0,0 UNION ALL

Select '0205000000','Meat of horses, asses, mules or hinnies, fresh, chilled or frozen.','KGM',0,0,0 UNION ALL

Select '0206100000','Of bovine animals, fresh or chilled','KGM',0,0,0 UNION ALL

Select '0206210000','Tongues','KGM',0,0,0 UNION ALL
Select '0206220000','Livers','KGM',0,0,0 UNION ALL
Select '0206290000','Other','KGM',0,0,0 UNION ALL
Select '0206300000','Of swine, fresh or chilled','KGM',0,0,0 UNION ALL

Select '0206410000','Livers','KGM',0,0,0 UNION ALL
Select '0206490000','Other','KGM',0,0,0 UNION ALL
Select '0206800000','Other, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0206900000','Other, frozen','KGM',0,0,0 UNION ALL


Select '0207110000','Not cut in pieces, fresh or chilled','KGM',20,0,0 UNION ALL
Select '0207120000','Not cut in pieces, frozen','KGM',20 ,0,0 UNION ALL
Select '0207130000','Cuts and offal, fresh or chilled','KGM',20 ,0,0 UNION ALL

Select '0207141000','Wings','KGM',20 ,0,0 UNION ALL
Select '0207142000','Thighs','KGM',20,0,0 UNION ALL
Select '0207143000','Livers','KGM',20,0,0 UNION ALL
Select '0207149100','Mechanically deboned or separated meat','KGM',20,0,0  


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0207149900','Other','KGM',20,0,0 UNION ALL
Select '0207240000','Not cut in pieces, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207250000','Not cut in pieces, frozen','KGM',0,0,0 UNION ALL
Select '0207260000','Cuts and offal, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207271000','Livers','KGM',0,0,0 UNION ALL
Select '0207279100','Mechanically deboned or separated meat','KGM',0,0,0 UNION ALL
Select '0207279900','Other','KGM',0,0,0 UNION ALL
Select '0207410000','Not cut in pieces, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207420000','Not cut in pieces, frozen','KGM',0,0,0 UNION ALL
Select '0207430000','Fatty livers, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207440000','Other, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207450000','Other, frozen','KGM',0,0,0 UNION ALL
Select '0207510000','Not cut in pieces, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207520000','Not cut in pieces, frozen','KGM',0,0,0 UNION ALL
Select '0207530000','Fatty livers, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207540000','Other, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207550000','Other, frozen','KGM',0,0,0 UNION ALL
Select '02076000','Of guinea fowls:','',0,0,0 UNION ALL
Select '0207600010','Not cut in pieces, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207600020','Not cut in pieces, frozen','KGM',0,0,0 UNION ALL
Select '0207600030','Fatty livers, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207600040','Other, fresh or chilled','KGM',0,0,0 UNION ALL
Select '0207600050','Other, frozen','KGM',0,0,0 UNION ALL
Select '0208100000','Of rabbits or hares','KGM',0,0,0 UNION ALL
Select '0208300000','Of primates','KGM',0,0,0  

INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0208401000','Of whales, dolphins and porpoises (mammals of the order Cetacea);','KGM',0,0,0 UNION ALL
Select '0208409000','Other','KGM',0,0,0 UNION ALL
Select '0208500000','Of reptiles (including snakes and turtles)','KGM',0,0,0 UNION ALL
Select '0208600000',' Of camels and other camelids (Camelidae)','KGM',0,0,0 UNION ALL

Select '0208901000','Frogs legs','KGM',0,0,0 UNION ALL
Select '0208909000','Other','KGM',0,0,0 UNION ALL

Select '0209100000',' Of pigs','KGM',0,0,0 UNION ALL
Select '0209900000',' Other','KGM',0,0,0 UNION ALL


Select '0210110000','Hams, shoulders and cuts thereof, with bone in','KGM',0,0,0 UNION ALL
Select '0210120000','Bellies (streaky) and cuts thereof','KGM',0,0,0 UNION ALL

Select '0210193000','Bacon or boneless hams','KGM',0,0,0 UNION ALL
Select '0210199000','Other','KGM',0,0,0 UNION ALL
Select '02102000','Meat of bovine animals:','',0,0,0 UNION ALL
Select '0210200010','Beef including veal','KGM',0,0,0 UNION ALL
Select '0210200090','Other','KGM',0,0,0 UNION ALL

Select '0210910000','Of primates','KGM',0,0,0 UNION ALL

Select '0210921000','Of whales, dolphins and porpoises (mammals of the order Cetacea);','KGM',0,0,0 UNION ALL
Select '0210929000','Other','KGM',0,0,0 UNION ALL
Select '0210930000','Of reptiles (including snakes and turtles)','KGM',0,0,0 UNION ALL

Select '0210991000','Freeze dried chicken dice','KGM',0,0,0 UNION ALL
Select '0210992000','Dried pork skin','KGM',0,0,0 UNION ALL
Select '0210999000','Other','KGM',0,0,0 UNION ALL




Select '0301111100','Botia (Chromobotia macracanthus)','UNT',0,0,0 UNION ALL
Select '0301111900','Other','UNT',0,0,0 UNION ALL

Select '0301119100','Koi carp (Cyprinus carpio)','UNT',0,0,0 UNION ALL
Select '0301119200','Goldfish (Carassius auratus)','UNT',0,0,0 UNION ALL
Select '0301119300','Siamese fighting fish (Beta splendens)','UNT',0,0,0 UNION ALL
Select '0301119400','Oscars (Astonotus ocellatus)','UNT',0,0,0 UNION ALL
Select '0301119500','Arowanas (Scleropages formosus)','UNT',0,0,0 UNION ALL
Select '0301119600','Arowanas Jardini (Scleropages jardini)','UNT',0,0,0 UNION ALL
Select '0301119900','Other','UNT',0,0,0 UNION ALL

Select '0301191000','Fry','UNT',0,0,0  

INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0301199100','Banggai cardinal fish (Pterapogon kauderni)','UNT',0,0,0 UNION ALL
Select '0301199200','Napoleon wrasse (Cheilinus undulatus)','UNT',0,0,0 UNION ALL
Select '0301199900','Other','UNT',0,0,0 UNION ALL

Select '0301910000','�Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki','KGM',0,0,0 UNION ALL
Select '0301920000','Eels (Anguilla spp.)','KGM',0,0,0 UNION ALL

Select '0301931000','Breeding, other than fry ','KGM',0,0,0 UNION ALL
Select '0301939000','Other','KGM',0,0,0 UNION ALL
Select '0301940000','Atlantic and Pacific bluefin tunas (Thunnus thynnus, Thunnus orientalis)','KGM',0,0,0 UNION ALL
Select '0301950000','Southern bluefin tunas (Thunnus maccoyii) ','KGM',0,0,0 UNION ALL


Select '0301991100','Breeding','KGM',0,0,0 UNION ALL
Select '0301991900','Other','KGM',0,0,0 UNION ALL

Select '0301992100','Breeding','KGM',0,0,0 UNION ALL
Select '0301992900','Other','KGM',0,0,0 UNION ALL

Select '0301994100','Tilapias (Oreochromis spp.)','KGM',0,0,0 UNION ALL
Select '0301994200','Other Carp, for breeding','KGM',0,0,0 UNION ALL
Select '0301994900','Other','KGM',0,0,0 UNION ALL

Select '0301995100','Milkfish, breeding','KGM',0,0,0 UNION ALL
Select '0301995200','Grouper','KGM',0,0,0 UNION ALL
Select '0301995900','Other','KGM',0,0,0 UNION ALL
Select '0301999000','Other','KGM',0,0,0 UNION ALL


Select '0302110000','Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki, Oncorhynchus aguabonita,','KGM',0,0,0 UNION ALL
Select '0302130000','Pacific salmon (Oncorhynchus nerka, Oncorhynchus gorbuscha, Oncorhynchus keta,','KGM',0,0,0 UNION ALL
Select '0302140000','Atlantic salmon (Salmo salar) and Danube salmon (Hucho hucho)','KGM',0,0,0 UNION ALL
Select '0302190000','Other','KGM',0,0,0 UNION ALL

Select '0302210000','Halibut (Reinhardtius hippoglossoides, Hippoglossus hippoglossus, Hippoglossus stenolepis)','KGM',0,0,0 UNION ALL
Select '0302220000','Plaice (Pleuronectes platessa)','KGM',0,0,0 UNION ALL
Select '0302230000','Sole (Solea spp.)','KGM',0,0,0 UNION ALL
Select '0302240000','Turbots (Psetta maxima)','KGM',0,0,0 UNION ALL
Select '0302290000','Other','KGM',0,0,0 UNION ALL

Select '0302310000','Albacore or longfinned tunas (Thunnus alalunga)','KGM',0,0,0 UNION ALL
Select '0302320000','Yellowfin tunas (Thunnus albacares)','KGM',0,0,0 UNION ALL
Select '0302330000','Skipjack or stripe bellied bonito','KGM',0,0,0 UNION ALL
Select '0302340000','Bigeye tunas (Thunnus obesus) ','KGM',0,0,0 UNION ALL
Select '0302350000','Atlantic and Pacific bluefin tunas (Thunnus thynnus, Thunnus orientalis)','KGM',0,0,0 UNION ALL
Select '0302360000','Southern bluefin tunas (Thunnus maccoyii) ','KGM',0,0,0 UNION ALL
Select '0302390000','Other','KGM',0,0,0 UNION ALL

Select '0302410000','Herrings (Clupea harengus, Clupea pallasii)','KGM',0,0,0 UNION ALL
Select '0302420000','Anchovies (Engraulis spp.)','KGM',0,0,0 UNION ALL
Select '0302430000','Sardines (Sardina pilchardus, Sardinops spp.), sardinella (Sardinella spp.), brisling or sprats (Sprattus sprattus)','KGM',0,0,0 UNION ALL
Select '0302440000','Mackerel (Scomber scombrus, Scomber australasicus, Scomber japonicus)','KGM',0,0,0 UNION ALL
Select '0302450000','Jack and horse mackerel (Trachurus spp.)','KGM',0,0,0 UNION ALL
Select '0302460000','Cobia (Rachycentron canadum)','KGM',0,0,0 UNION ALL
Select '0302470000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL
Select '0302490000','Other','KGM',0,0,0 UNION ALL

Select '0302510000','Cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)','KGM',0,0,0 UNION ALL
Select '0302520000','Haddock (Melanogrammus aeglefinus)','KGM',0,0,0 UNION ALL
Select '0302530000','Coalfish (Pollachius virens)','KGM',0,0,0 UNION ALL
Select '0302540000','Hake (Merluccius spp., Urophycis spp.)','KGM',0,0,0 UNION ALL
Select '0302550000','Alaska Pollack (Theragra chalcogramma)','KGM',0,0,0 UNION ALL
Select '0302560000','Blue whitings (Micromesistius poutassou, Micromesistius australis)','KGM',0,0,0 UNION ALL
Select '0302590000','Other','KGM',0,0,0 UNION ALL

Select '0302710000','Tilapias (Oreochromis spp.)','KGM',0,0,0 UNION ALL

Select '0302721000','Yellowtail catfish (Pangasius pangasius)','KGM',0,0,0 UNION ALL
Select '0302729000','Other','KGM',0,0,0 UNION ALL
Select '0302730000','Carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp.','KGM',0,0,0 UNION ALL
Select '0302740000','Eels (Anguilla spp.)','KGM',0,0,0 UNION ALL
Select '0302790000','Other','KGM',0,0,0 UNION ALL

Select '0302810000','Dogfish and other sharks','KGM',0,0,0 UNION ALL
Select '0302820000','Rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0302830000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0302840000','Seabass (Dicentrarchus spp.)','KGM',0,0,0 UNION ALL
Select '0302850000','Seabream (Sparidae)','KGM',0,0,0 UNION ALL


Select '0302891100','Grouper','KGM',0,0,0 UNION ALL
Select '0302891200','Longfin mojarra (Pentaprion longimanus)','KGM',0,0,0 UNION ALL
Select '0302891300','Bluntnose lizardfish (Trachinocephalus myops)','KGM',0,0,0 UNION ALL
Select '0302891400','Savalai hairtails (Lepturacanthus savala), Belanger''s croakers (Johnius belangerii)','KGM',0,0,0 UNION ALL
Select '0302891600','Torpedo scads (Megalaspis cordyla), spotted sicklefish (Drepane punctata)','KGM',0,0,0 UNION ALL
Select '0302891700','Black pomfrets (Parastromatus niger)','KGM',0,0,0 UNION ALL
Select '0302891800','Mangrove red snappers (Lutjanus argentimaculatus)','KGM',0,0,0 UNION ALL
Select '0302891900','Other','KGM',0,0,0 UNION ALL

Select '0302892200','Swamp barb (Puntius chola)','KGM',0,0,0 UNION ALL
Select '0302892600','Indian threadfins (Polynemus indicus) and silver grunts (Pomadasys argenteus)','KGM',0,0,0 UNION ALL
Select '0302892700','Hilsa shad (Tenualosa ilisha)','KGM',0,0,0 UNION ALL
Select '0302892800','Wallago (Wallago attu) and giant rivercatfish (Sperata seenghala)','KGM',0,0,0 UNION ALL
Select '0302892900','Other','KGM',0,0,0 UNION ALL

Select '0302910000','Livers, roes and milt ','KGM',0,0,0 UNION ALL
Select '0302920000','Shark fins ','KGM',0,0,0 UNION ALL
Select '0302990000','Other','KGM',0,0,0 UNION ALL


Select '0303110000','Sockeye salmon (red salmon) (Oncorhynchus nerka)','KGM',0,0,0 UNION ALL
Select '0303120000','Other Pacific salmon (Oncorhynchus gorbuscha, Oncorhynchus keta, Oncorhynchus','KGM',0,0,0 UNION ALL
Select '0303130000','Atlantic salmon (Salmo salar) and Danube salmon (Hucho hucho)','KGM',0,0,0 UNION ALL
Select '0303140000','Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki, Oncorhynchus aguabonita','KGM',0,0,0 UNION ALL
Select '0303190000','Other','KGM',0,0,0 UNION ALL

Select '0303230000','Tilapias (Oreochromis spp.)','KGM',0,0,0 UNION ALL
Select '0303240000','Catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.)','KGM',0,0,0 UNION ALL
Select '0303250000','Carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp.)','KGM',0,0,0 UNION ALL
Select '0303260000','Eels (Anguilla spp.)','KGM',0,0,0 UNION ALL
Select '0303290000','Other','KGM',0,0,0 UNION ALL

Select '0303310000','Halibut (Reinhardtius hippoglossoides, Hippoglossus hippoglossus, Hippoglossus stenolepis)','KGM',0,0,0 UNION ALL
Select '0303320000','Plaice (Pleuronectes platessa)','KGM',0,0,0 UNION ALL
Select '0303330000','Sole (Solea spp.)','KGM',0,0,0 UNION ALL
Select '0303340000','Turbots (Psetta maxima)','KGM',0,0,0 UNION ALL
Select '0303390000','Other','KGM',0,0,0 UNION ALL

Select '0303410000','Albacore or longfinned tunas (Thunnas alalunga)','KGM',0,0,0 UNION ALL
Select '0303420000','Yellowfin tunas (Thunnus albacares)','KGM',0,0,0 UNION ALL
Select '0303430000','Skipjack or stripbellied bonito','KGM',0,0,0 UNION ALL
Select '0303440000','Bigeye tunas (Thunnus obesus) ','KGM',0,0,0 UNION ALL

Select '0303451000','Atlantic bluefin tunas (Thunnus thynnus)','KGM',0,0,0 UNION ALL
Select '0303459000','Pacific bluefin tunas (Thunnus orientalis)','KGM',0,0,0 UNION ALL
Select '0303460000','Southern bluefin tunas (Thunnus maccoyii) ','KGM',0,0,0 UNION ALL
Select '0303490000','Other','KGM',0,0,0 UNION ALL

Select '0303510000','Herrings (Clupea harengus, Clupea pallasii)','KGM',0,0,0 UNION ALL
Select '0303530000','Sardines (Sardina pilchardus, Sardinops spp.), sardinella (Sardinella spp.)','KGM',0,0,0 UNION ALL

Select '0303541000','Mackerel (Scomber scombrus, Scomber australasicus)','KGM',0,0,0 UNION ALL
Select '0303542000','Pacific mackerel (Scomber japonicus)','KGM',0,0,0 UNION ALL
Select '0303550000','Jack and horse mackerel (Trachurus spp.)','KGM',0,0,0 UNION ALL
Select '0303560000','Cobia (Rachycentron canadum)','KGM',0,0,0 UNION ALL
Select '0303570000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL

Select '0303591000','Indian mackerels (Rastelliger kanagurta); Island mackerels (Rastrelliger faughni) ','KGM',0,0,0 UNION ALL
Select '0303592000','Silver pomfrets (Pampus spp.)','KGM',0,0,0 UNION ALL
Select '0303599000','Other','KGM',0,0,0 UNION ALL

Select '0303630000','Cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)','KGM',0,0,0 UNION ALL
Select '0303640000','Haddock (Melanogrammus aeglefinus)','KGM',0,0,0 UNION ALL
Select '0303650000','Coalfish (Pollachius virens)','KGM',0,0,0 UNION ALL
Select '0303660000','Hake (Merluccius spp., Urophycis spp.)','KGM',0,0,0 UNION ALL
Select '0303670000','Alaska Pollack (Theragra chalcogramma)','KGM',0,0,0 UNION ALL
Select '0303680000','Blue whitings (Micromesistius poutassou, Micromesistius australis)','KGM',0,0,0 UNION ALL
Select '0303690000','Other','KGM',0,0,0 UNION ALL

Select '0303810000','Dogfish and other sharks','KGM',0,0,0 UNION ALL
Select '0303820000','Rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0303830000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0303840000','Seabass (Dicentrarchus spp.)','KGM',0,0,0 UNION ALL


Select '0303891100','Grouper ','KGM',0,0,0 UNION ALL
Select '0303891200','Longfin mojarra (Pentaprion longimanus)','KGM',0,0,0 UNION ALL
Select '0303891300','Bluntnose lizardfish (Trachinocephalus myops)','KGM',0,0,0 UNION ALL
Select '0303891400','Savalai hairtails (Lepturacanthus savala), Belanger''s croakers (Johnius belangerii)','KGM',0,0,0 UNION ALL
Select '0303891600','Torpedo scads (Megalaspis cordyla), spotted sicklefish (Drepane punctata) and great barracudas (Sphyraena barracuda)','KGM',0,0,0 UNION ALL
Select '0303891700','Black pomfrets (Parastromatus niger)','KGM',0,0,0 UNION ALL
Select '0303891800','Mangrove red snappers (Lutjanus argentimaculatus)','KGM',0,0,0 UNION ALL
Select '0303891900','Other','KGM',0,0,0 UNION ALL

Select '0303892200','Swamp barb (Puntius chola)','KGM',0,0,0 UNION ALL
Select '0303892600','Indian threadfins (Polynemus indicus) and silver grunts (Pomadasys argenteus)','KGM',0,0,0 UNION ALL
Select '0303892700','Hilsa shad (Tenualosa ilisha)','KGM',0,0,0 UNION ALL
Select '0303892800','Wallago (Wallago attu) and giant rivercatfish (Sperata seenghala)','KGM',0,0,0 UNION ALL
Select '0303892900','Other','KGM',0,0,0 UNION ALL

Select '0303910000','Livers , roes and milt','KGM',0,0,0 UNION ALL
Select '0303920000','Shark fins','KGM',0,0,0 UNION ALL
Select '0303990000','Other','KGM',0,0,0  


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0304310000','Tilapias (Oreochromis spp.)','KGM',0,0,0 UNION ALL
Select '0304320000','Catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.)','KGM',0,0,0 UNION ALL
Select '0304330000','Nile Perch (Lates Niloticus)','KGM',0,0,0 UNION ALL
Select '0304390000','Other','KGM',0,0,0 UNION ALL

Select '0304410000','Pacific salmon (Oncorhynchus nerka, Oncorhynchus gorbuscha, Oncorhynchus keta','KGM',0,0,0 UNION ALL
Select '0304420000','Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki, Oncorhynchus  ','KGM',0,0,0 UNION ALL
Select '0304430000','Flat fish (Pleuronectidae, Bothidae, Cynoglossidae, Soleidae, Scophthalmidae and Citharidae)','KGM',0,0,0 UNION ALL
Select '0304440000','Fish of the families Bregmacerotidae, Euclichthyidae, Gadidae, Macrouridae, ','KGM',0,0,0 UNION ALL
Select '0304450000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL
Select '0304460000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0304470000','Dogfish and other sharks','KGM',0,0,0 UNION ALL
Select '0304480000','Rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0304490000','Other','KGM',0,0,0 UNION ALL

Select '0304510000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.).','KGM',0,0,0 UNION ALL
Select '0304520000','Salmonidae','KGM',0,0,0 UNION ALL
Select '0304530000','Fish of the families Bregmacerotidae, Euclichthyidae, Gadidae, Macrouridae, Melanonidae, Merlucciidae, Moridae and Muraenolepididae','KGM',0,0,0 UNION ALL
Select '0304540000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL
Select '0304550000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0304560000','Dogfish and other sharks','KGM',0,0,0 UNION ALL
Select '0304570000','Rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0304590000','Other','KGM',0,0,0 UNION ALL

Select '0304610000','Tilapias (Oreochromis spp.)','KGM',0,0,0 UNION ALL
Select '0304620000','Catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.)','KGM',0,0,0 UNION ALL
Select '0304630000','Nile Perch (Lates niloticus)','KGM',0,0,0 UNION ALL
Select '0304690000','Other','KGM',0,0,0 UNION ALL

Select '0304710000','Cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)','KGM',0,0,0 UNION ALL
Select '0304720000','Haddock (Melanogrammus aeglefinus)','KGM',0,0,0 UNION ALL
Select '0304730000','Coalfish (Pollachius virens)','KGM',0,0,0 UNION ALL
Select '0304740000','Hake (Merluccius spp., Urophycis spp.)','KGM',0,0,0 UNION ALL
Select '0304750000','Alaska Pollack (Theragra chalcogramma)','KGM',0,0,0 UNION ALL
Select '0304790000','Other','KGM',0,0,0 UNION ALL

Select '0304810000','Pacific salmon (Oncorhynchus nerka, Oncorhynchus gorbuscha, Oncorhynchus keta, Oncorhynchus tschawytscha, Oncorhynchus kisutch, Oncorhynchus masou and Oncorhynchus rhodurus), Atlantic salmon (Salmo salar) and Danube salmon (Hucho hucho)','KGM',0,0,0 UNION ALL
Select '0304820000','Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki, Oncorhynchus aguabonita, Oncorhynchus gilae, Oncorhynchus apache and Oncorhynchus chrysogaster)','KGM',0,0,0 UNION ALL
Select '0304830000','Flat fish (Pleuronectidae, Bothidae, Cynoglossidae, Soleidae, Scophthalmidae and Citharidae)','KGM',0,0,0 UNION ALL
Select '0304840000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL
Select '0304850000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0304860000','Herrings (Clupea harengus, Clupea pallasii)','KGM',0,0,0 UNION ALL
Select '0304870000','Tunas (of the genus Thunnus), skipjack or stripebellied bonito (Euthynnus (Katsuwonus) pelamis)','KGM',0,0,0 UNION ALL
Select '0304880000','Dogfish, other sharks, rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0304890000','Other','KGM',0,0,0 UNION ALL

Select '0304910000','Swordfish (Xiphias gladius)','KGM',0,0,0 UNION ALL
Select '0304920000','Toothfish (Dissostichus spp.)','KGM',0,0,0 UNION ALL
Select '0304930000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.)','KGM',0,0,0 UNION ALL
Select '0304940000','Alaska Pollack (Theragra chalcogramma)','KGM',0,0,0 UNION ALL
Select '0304950000','Fish of the families Bregmacerotidae, Euclichthyidae, Gadidae, Macrouridae, Melanonidae, Merlucciidae, Moridae and Muraenolepididae, other than Alaska Pollack (Theragra chalcogramma)','KGM',0,0,0 UNION ALL
Select '0304960000','Dogfish and other sharks','KGM',0,0,0 UNION ALL
Select '0304970000','Rays and skates (Rajidae)','KGM',0,0,0 UNION ALL
Select '0304990000','Other','KGM',0,0,0 UNION ALL

Select '0305100000','Flours, meals and pellets of fish, fit for human consumption ','KGM',0,0,0 UNION ALL

Select '0305201000','Of freshwater fish, dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0305209000','Other','KGM',0,0,0 UNION ALL

Select '0305310000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.)','KGM',0,0,0 UNION ALL
Select '0305320000','Fish of the families Bregmacerotidae, Euclichthyidae, Gadidae, Macrouridae, Melanonidae, Merlucciidae, Moridae and Muraenolepididae','KGM',7,0,0 UNION ALL

Select '0305391000','Freshwater garfish (Xenentodon cancila), yellowstriped goatfish (Upeneus vittatus) and longrakered trevally (Ulua mentalis)','KGM',0,0,0 UNION ALL
Select '0305392000','Savalai hairtails (Lepturacanthus savala), Belanger''s croakers (Johnius belangerii), Reeve''s croakers (Chrysochir aureus) and Bigeye croakers (Pennahia anea)','KGM',7,0,0 UNION ALL

Select '0305399100','Of freshwater fish','KGM',0,0,0 UNION ALL
Select '0305399200','Of marine fish','KGM',7,0,0 UNION ALL
Select '0305399900','Other','KGM',5,0,0 UNION ALL

Select '0305410000','Pacific Salmon (Oncorhynchus nerka, Oncorhynchus gorbuscha, Oncorhynchus keta, Oncorhynchus tschawytscha, Oncorhynchus  kisutch, Oncorhynchus masou  and  Oncorhynchus rhodurus), Atlantic salmon (Salmo salar) and  Danube salmon (Hucho hucho)','KGM',7,0,0 UNION ALL
Select '0305420000','Herrings (Clupea harengus, Clupea pallasii)','KGM',7,0,0 UNION ALL
Select '0305430000','Trout (Salmo trutta, Oncorhynchus mykiss, Oncorhynchus clarki, Oncorhynchus aguabonita, Oncorhynchus gilae, Oncorhynchus apache and Oncorhynchus chrysogaster)','KGM',7,0,0 UNION ALL
Select '0305440000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.)','KGM',7,0,0 UNION ALL
Select '0305490000','Other','KGM',7,0,0 UNION ALL

Select '0305510000','Cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)','KGM',0,0,0 UNION ALL
Select '0305520000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.)','KGM',0,0,0 UNION ALL
Select '0305530000','Fish of the families Bregmacerotidae, Euclichthyidae, Gadidae, Macrouridae, Melanonidae, Merlucciidae, Moridae and Muraenolepididae, other than cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)  ','KGM',7,0,0 UNION ALL
Select '0305540000','Herrings (Clupea harengus, Clupea pallasii), anchovies (Engraulis spp.), sardines (Sardina pilchardus, Sardinops spp.), sardinella (Sardinella spp.), brisling or sprats (Sprattus sprattus), mackerel (Scomber scombrus, Scomber australasicus, Scomber japonicus)','KGM',7,0,0 UNION ALL


Select '0305592100','Anchovies (Stolephorus spp., Coilia spp., Setipinna spp., Lycothrissa spp. and Thryssa spp., Encrasicholina spp.)','KGM',7,0,0 UNION ALL
Select '0305592900','Other','KGM',7,0,0 UNION ALL
Select '0305599000','Other','KGM',0,0,0 UNION ALL

Select '0305610000','Herrings (Clupea harengus, Clupea pallasii)','KGM',7,0,0 UNION ALL
Select '0305620000','Cod (Gadus morhua, Gadus ogac, Gadus macrocephalus)','KGM',7,0,0 UNION ALL
Select '0305630000','Anchovies (Engraulis spp.)','KGM',7,0,0 UNION ALL
Select '0305640000','Tilapias (Oreochromis spp.), catfish (Pangasius spp., Silurus spp., Clarias spp., Ictalurus spp.), carp (Cyprinus spp., Carassius spp., Ctenopharyngodon idellus, Hypophthalmichthys spp., Cirrhinus spp., Mylopharyngodon piceus, Catla Catla, Labeo spp., Osteochilus hasselti, Leptobarbus hoeveni, Megalobrama spp.), eels (Anguilla spp.), Nile perch (Lates niloticus) and snakeheads (Channa spp.)','KGM',0,0,0 UNION ALL

Select '0305691000','Marine fish','KGM',7,0,0 UNION ALL
Select '0305699000','Other','KGM',0,0,0 UNION ALL

Select '0305710000','Shark fins','KGM',7,0,0 UNION ALL


Select '0305721100','Of cod','KGM',0,0,0 UNION ALL
Select '0305721900','Other','KGM',7,0,0 UNION ALL

Select '0305729100','Of cod','KGM',0,0,0 UNION ALL
Select '0305729900','Other','KGM',0,0,0 UNION ALL

Select '0305791000','Of cod','KGM',0,0,0 UNION ALL
Select '0305799000','Other','KGM',0,0,0 UNION ALL



Select '0306111000',' Smoked','KGM',0,0,0 UNION ALL
Select '0306119000',' Other','KGM',0,0,0 UNION ALL

Select '0306121000',' Smoked','KGM',0,0,0 UNION ALL
Select '0306129000',' Other','KGM',0,0,0 UNION ALL

Select '0306141000','Soft shell crabs','KGM',0,0,0 UNION ALL
Select '0306149000','Other','KGM',0,0,0 UNION ALL
Select '0306150000','Norway lobsters (Nephrops norvegicus)','KGM',0,0,0 UNION ALL
Select '0306160000','Coldwater shrimps and prawns (Pandalus spp., Crangon crangon)','KGM',0,0,0 UNION ALL


Select '0306171100','Headless','KGM',0,0,0 UNION ALL
Select '0306171900','Other','KGM',0,0,0 UNION ALL

Select '0306172100','Headless, with tail','KGM',0,0,0 UNION ALL
Select '0306172200','Headless, without tail','KGM',0,0,0 UNION ALL
Select '0306172900','Other','KGM',0,0,0 UNION ALL
Select '0306173000','Giant river prawns (Macrobrachium rosenbergii)','KGM',0,0,0 UNION ALL
Select '0306179000','Other','KGM',0,0,0 UNION ALL
Select '0306190000','Other, including flours, meals and pellets of crustaceans, fit for human consumption','KGM',0,0,0 UNION ALL


Select '0306311000','Breeding','KGM',0,0,0 UNION ALL
Select '0306312000','Other, live ','KGM',0,0,0 UNION ALL
Select '0306313000','Fresh or chilled','KGM',0,0,0 UNION ALL

Select '0306321000','Breeding','KGM',0,0,0 UNION ALL
Select '0306322000','Other, live ','KGM',0,0,0 UNION ALL
Select '0306323000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0306330000','Crabs     ','KGM',0,0,0 UNION ALL
Select '0306340000','Norway lobsters (Nephrops norvegicus)','KGM',0,0,0 UNION ALL

Select '0306351000','Breeding','KGM',0,0,0 UNION ALL
Select '0306352000','Other, live','KGM',0,0,0 UNION ALL
Select '0306353000','Fresh or chilled','KGM',0,0,0 UNION ALL


Select '0306361100','Giant tiger prawns (Penaeus monodon)','KGM',0,0,0 UNION ALL
Select '0306361200','Whiteleg shrimps (Litopenaeus vannamei)','KGM',0,0,0 UNION ALL
Select '0306361300','Giant river prawns (Macrobrachium rosenbergii)','KGM',0,0,0 UNION ALL
Select '0306361900','Other','KGM',0,0,0 UNION ALL

Select '0306362100','Giant tiger prawns (Penaeus monodon)','KGM',0,0,0 UNION ALL
Select '0306362200','Whiteleg shrimps (Litopenaeus vannamei)','KGM',0,0,0 UNION ALL
Select '0306362300','Giant river prawns (Macrobrachium rosenbergii)','KGM',0,0,0 UNION ALL
Select '0306362900','Other','KGM',0,0,0 UNION ALL

Select '0306363100','Giant tiger prawns (Penaeus monodon)','KGM',0,0,0 UNION ALL
Select '0306363200','Whiteleg shrimps (Litopenaeus vannamei)','KGM',0,0,0 UNION ALL
Select '0306363300','Giant river prawns (Macrobrachium rosenbergii)','KGM',0,0,0 UNION ALL
Select '0306363900','Other','KGM',0,0,0 UNION ALL

Select '0306391000','Live ','KGM',0,0,0 UNION ALL
Select '0306392000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0306393000','Flours, meals and pellets','KGM',0,0,0 UNION ALL



Select '0306912100','Smoked','KGM',0,0,0 UNION ALL
Select '0306912900','Other','KGM',8,0,0 UNION ALL

Select '0306913100','Smoked','KGM',0,0,0 UNION ALL
Select '0306913900','Other','KGM',0,0,0 UNION ALL


Select '0306922100','Smoked','KGM',0,0,0 UNION ALL
Select '0306922900','Other','KGM',8,0,0 UNION ALL

Select '0306923100','Smoked','KGM',0,0,0 UNION ALL
Select '0306923900','Other','KGM',0,0,0 UNION ALL


Select '0306932100','Smoked','KGM',0,0,0 UNION ALL
Select '0306932900','Other','KGM',8,0,0 UNION ALL
Select '0306933000','Other','KGM',0,0,0 UNION ALL


Select '0306942100','Smoked','KGM',0,0,0 UNION ALL
Select '0306942900','Other','KGM',8,0,0 UNION ALL

Select '0306943100','Smoked','KGM',0,0,0 UNION ALL
Select '0306943900','Other','KGM',0,0,0 UNION ALL


Select '0306952100','In shell, cooked by steaming or boiling in water','KGM',8,0,0 UNION ALL
Select '0306952900','Other','KGM',0,0,0 UNION ALL
Select '0306953000','Other','KGM',0,0,0 UNION ALL


Select '0306992100','Smoked','KGM',0,0,0 UNION ALL
Select '0306992900','Other','KGM',8,0,0 UNION ALL

Select '0306993100','Smoked','KGM',0,0,0 UNION ALL
Select '0306993900','Other','KGM',0,0,0 UNION ALL



Select '0307111000','Live','KGM',0,0,0 UNION ALL
Select '0307112000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0307120000','Frozen','KGM',0,0,0 UNION ALL

Select '0307192000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307193000','Smoked','KGM',0,0,0 UNION ALL


Select '0307211000','Live ','KGM',0,0,0 UNION ALL
Select '0307212000','Fresh or chilled ','KGM',0,0,0 UNION ALL
Select '0307220000','Frozen','KGM',0,0,0 UNION ALL

Select '0307293000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307294000','Smoked','KGM',0,0,0 UNION ALL


Select '0307311000','Live ','KGM',0,0,0 UNION ALL
Select '0307312000','Fresh or chilled ','KGM',0,0,0 UNION ALL
Select '0307320000','Frozen     ','KGM',0,0,0 UNION ALL

Select '0307393000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307394000','Smoked','KGM',0,0,0  


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0307421100','Cuttle fish (Sepia officinalis, Rossia macrosoma,Sepiola spp) and squid (Ommastrephes spp., Loligo spp., Nototodarus spp., Sepioteuthis spp.)','KGM',0,0,0 UNION ALL
Select '0307421900','Other','KGM',0,0,0 UNION ALL

Select '0307422100','Cuttle fish (Sepia officinalis, Rossia macrosoma,Sepiola spp.) and squid (Ommastrephes spp., Loligo spp., Nototodarus spp., Sepioteuthis spp.)','KGM',0,0,0 UNION ALL
Select '0307422900','Other','KGM',0,0,0 UNION ALL

Select '0307431000','Cuttle fish (Sepia officinalis, Rossia macrosoma,Sepiola spp.) and squid (Ommastrephes spp., Loligospp., Nototodarus spp., Sepioteuthis spp.)','KGM',0,0,0 UNION ALL
Select '0307439000','Other ','KGM',0,0,0 UNION ALL


Select '0307492100','Cuttle fish (Sepia officinalis, Rossia macrosoma,Sepiola spp.) and squid (Ommastrephes spp., Loligo spp., Nototodarus spp., Sepioteuthis spp.)','KGM',0,0,0 UNION ALL
Select '0307492900','Other','KGM',0,0,0 UNION ALL
Select '0307493000','Smoked','KGM',0,0,0 UNION ALL


Select '0307511000','Live ','KGM',0,0,0 UNION ALL
Select '0307512000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0307520000','Frozen','KGM',0,0,0 UNION ALL

Select '0307592000','Dried, salted or in brine','KGM',10,0,0 UNION ALL
Select '0307593000','Smoked','KGM',0,0,0 UNION ALL

Select '0307601000','Live ','KGM',0,0,0 UNION ALL
Select '0307602000','Fresh, chilled or frozen','KGM',0,0,0 UNION ALL
Select '0307604000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307605000','Smoked','KGM',0,0,0 UNION ALL


Select '0307711000','Live','KGM',0,0,0 UNION ALL
Select '0307712000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0307720000','Frozen','KGM',0,0,0 UNION ALL

Select '0307793000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307794000','Smoked','KGM',0,0,0 UNION ALL


Select '0307811000','Live','KGM',0,0,0 UNION ALL
Select '0307812000','Fresh or chilled','KGM',0,0,0 UNION ALL

Select '0307821000','Live','KGM',0,0,0 UNION ALL
Select '0307822000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0307830000','Frozen abalone (Haliotis spp.) ','KGM',0,0,0 UNION ALL
Select '0307840000','Frozen stromboid conchs (Strombus spp.) ','KGM',0,0,0 UNION ALL

Select '0307871000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307872000','Smoked','KGM',0,0,0 UNION ALL

Select '0307881000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307882000','Smoked','KGM',0,0,0 UNION ALL


Select '0307911000','   Live','KGM',0,0,0 UNION ALL
Select '0307912000','   Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0307920000','Frozen','KGM',0,0,0 UNION ALL

Select '0307993000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0307994000','Smoked','KGM',0,0,0 UNION ALL
Select '0307995000','Flours, meals and pellets of molluscs','KGM',0,0,0  



INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0308111000','Live','KGM',0,0,0 UNION ALL
Select '0308112000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0308120000','Frozen','KGM',0,0,0 UNION ALL

Select '0308192000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0308193000','Smoked','KGM',0,0,0 UNION ALL


Select '0308211000','Live','KGM',0,0,0 UNION ALL
Select '0308212000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0308220000','Frozen','KGM',0,0,0 UNION ALL

Select '0308292000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0308293000','Smoked','KGM',0,0,0 UNION ALL

Select '0308301000','Live','KGM',0,0,0 UNION ALL
Select '0308302000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0308303000','Frozen','KGM',0,0,0 UNION ALL
Select '0308304000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0308305000','Smoked','KGM',0,0,0 UNION ALL

Select '0308901000','Live','KGM',0,0,0 UNION ALL
Select '0308902000','Fresh or chilled','KGM',0,0,0 UNION ALL
Select '0308903000','Frozen','KGM',0,0,0 UNION ALL
Select '0308904000','Dried, salted or in brine','KGM',0,0,0 UNION ALL
Select '0308905000','Smoked','KGM',0,0,0 UNION ALL
Select '0308909000','Other','KGM',0,0,0 UNION ALL


Select '0401101000','In liquid form','KGM',20,0,0 UNION ALL
Select '0401109000','Other','KGM',5,0,0 UNION ALL

Select '0401201000','In liquid form','KGM',20 ,0,0 UNION ALL
Select '0401209000','Other','KGM',5,0,0 UNION ALL

Select '0401401000','Milk in liquid form','KGM',20,0,0 UNION ALL
Select '0401402000','Milk in frozen form','KGM',5,0,0 UNION ALL
Select '0401409000','Other','KGM',0,0,0 UNION ALL

Select '0401501000','In liquid form','KGM',0,0,0 UNION ALL
Select '0401509000','Other','KGM',0,0,0  



INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0402104100','In containers of a net weight of 20 kg or more','KGM',0,0,0 UNION ALL
Select '0402104200','In containers of a net weight of 2 kg or less','KGM',0,0,0 UNION ALL
Select '0402104900','Other','KGM',0,0,0 UNION ALL

Select '0402109100','In containers of a net weight of 20 kg or more','KGM',0,0,0 UNION ALL
Select '0402109200','In containers of a net weight of 2 kg or less','KGM',0,0,0 UNION ALL
Select '0402109900','Other','KGM',0,0,0 UNION ALL


Select '0402212000','In containers of a net weight of 20 kg or more','KGM',0,0,0 UNION ALL
Select '0402213000','In containers of a net weight of 2 kg or less','KGM',0,0,0 UNION ALL
Select '0402219000','Other','KGM',0,0,0 UNION ALL

Select '0402292000','In containers of a net weight of 20 kg or more','KGM',0,0,0 UNION ALL
Select '0402293000','In containers of a net weight of 2 kg or less','KGM',0,0,0 UNION ALL
Select '0402299000','Other','KGM',0,0,0 UNION ALL

Select '0402910000','Not containing added sugar or other sweetening matter','KGM',5,0,0 UNION ALL
Select '0402990000','Other','KGM',0,0,0 UNION ALL



Select '0403102100','Flavoured or containing added fruits (including pulp and jams), nuts or cocoa','KGM',0,0,0 UNION ALL
Select '0403102900','Other','KGM',0,0,0 UNION ALL

Select '0403109100','Flavoured or containing added fruits (including pulp and jams), nuts or cocoa','KGM',0,0,0 UNION ALL
Select '0403109900','Other','KGM',0,0,0 UNION ALL

Select '0403901000','Buttermilk','KGM',0,0,0 UNION ALL
Select '04039090','Other:','',0,0,0 UNION ALL
Select '0403909060','Flavoured or containing added fruit (including pulp and jams), nuts or cocoa','KGM',0,0,0 UNION ALL
Select '0403909090','Other','KGM',0,0,0 UNION ALL


Select '0404101000','In powder form','KGM',0,0,0 UNION ALL
Select '0404109000','Other','KGM',0,0,0 UNION ALL
Select '0404900000','Other','KGM',0,0,0 UNION ALL

Select '0405100000','Butter','KGM',0,0,0 UNION ALL
Select '0405200000','Dairy spreads','KGM',0,0,0 UNION ALL

Select '0405901000','Anhydrous butterfat','KGM',0,0,0 UNION ALL
Select '0405902000','Butteroil','KGM',0,0,0 UNION ALL
Select '0405903000','Ghee','KGM',0,0,0 UNION ALL
Select '0405909000','Other','KGM',0,0,0 UNION ALL
Select '0406101000','Fresh (unripened or uncured) cheese, including whey cheese','KGM',0,0,0 UNION ALL
Select '0406102000','Curd','KGM',0,0,0 UNION ALL
Select '0406201000','In packages of a gross weight exceeding 20 kg ','KGM',0,0,0 UNION ALL
Select '0406209000','Other','KGM',0,0,0 UNION ALL
Select '0406300000','Processed cheese, not grated or powdered ','KGM',0,0,0 UNION ALL
Select '0406400000','Blueveined cheese and other cheese containing veins produced by Penicillium roqueforti','KGM',0,0,0 UNION ALL
Select '0406900000','Other cheese ','KGM',0,0,0  


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0407111000','For breeding','UNT',10,0,0 UNION ALL
Select '0407119000','Other','UNT',10,0,0 UNION ALL
Select '0407191100','For breeding','UNT',10 ,0,0 UNION ALL
Select '0407191900','Other','UNT',10,0,0 UNION ALL
Select '0407199100','For breeding','UNT',0,0,0 UNION ALL
Select '0407199900','Other','UNT',0,0,0 UNION ALL
Select '0407210000','Of fowls of the species Gallus domesticus ','UNT',10 ,0,0 UNION ALL
Select '0407291000','Of ducks','UNT',10,0,0 UNION ALL
Select '0407299000','Other','UNT',0,0,0 UNION ALL
Select '0407901000','Of fowls of the species Gallus domesticus ','UNT',10,0,0 UNION ALL
Select '0407902000','Of ducks','UNT',10,0,0 UNION ALL
Select '0407909000','Other','UNT',0,0,0 UNION ALL
Select '0408110000','Dried ','KGM',0,0,0 UNION ALL
Select '0408190000','Other ','KGM',0,0,0 UNION ALL
Select '0408910000','Dried ','KGM',0,0,0 UNION ALL
Select '0408990000','Other ','KGM',0,0,0 UNION ALL
Select '0409000000','Natural honey.','KGM',0,0,0 UNION ALL
Select '0410001000','Birds'' nests','KGM',0,0,0 UNION ALL
Select '0410009010','Turtles'' eggs','KGM',0,0,0 UNION ALL
Select '0410009090','Other','KGM',0,0,0 UNION ALL
Select '0501000000','Human hair, unworked, whether or not washed or scoured; waste of human hair.','KGM',0,0,0 UNION ALL
Select '0502100000','Pigs'', hogs'' or boars'' bristles and hair and waste thereof','KGM',0,0,0 UNION ALL
Select '0502900000','Other','KGM',0,0,0 UNION ALL
Select '0504000000','Guts, bladders and stomachs of animals (other than fish), whole and pieces thereof, fresh, chilled, frozen, salted, in brine, dried or smoked.','KGM',0,0,0 UNION ALL
Select '0505101000','Duck feathers','KGM',0,0,0 UNION ALL
Select '0505109000','Other','KGM',0,0,0 UNION ALL
Select '0505901000','Duck feathers','KGM',0,0,0 UNION ALL
Select '0505909000','Other','KGM',0,0,0 UNION ALL
Select '0506100000','Ossein and bones treated with acid','KGM',0,0,0 UNION ALL
Select '0506900000','Other','KGM',0,0,0 UNION ALL
Select '0507100000','Ivory; ivory powder and waste','KGM',0,0,0 UNION ALL
Select '0507902000','Tortoiseshell ','KGM',0,0,0 UNION ALL
Select '0507909000','Other','KGM',0,0,0 UNION ALL
Select '0508002000','Shells of molluscs, crustaceans or echinoderms ','KGM',0,0,0 UNION ALL
Select '0508009000','Other','KGM',0,0,0 UNION ALL
Select '0510000000','Ambergris, castoreum, civet and musk; cantharides; bile, whether or not dried; glands and other animal products used in the preparation of pharmaceutical products, fresh, chilled, frozen or otherwise provisionally preserved.','KGM',0,0,0 UNION ALL
Select '0511100000','Bovine semen ','KGM',0,0,0 UNION ALL
Select '0511911000','Roes and milt','KGM',0,0,0 UNION ALL
Select '0511912000','Artemia egg (Brine shrimp egg)','KGM',0,0,0 UNION ALL
Select '0511913000','Fish Skin','KGM',0,0,0 UNION ALL
Select '0511919000','Other','KGM',0,0,0 UNION ALL
Select '0511991000','Domestic animal semen','KGM',0,0,0 UNION ALL
Select '0511992000','Silk worm eggs','KGM',0,0,0 UNION ALL
Select '0511993000','Natural sponges','KGM',0,0,0 UNION ALL
Select '0511999000','Other','KGM',0,0,0 UNION ALL
Select '0601100000','Bulbs, tubers, tuberous roots, corms, crowns and rhizomes, dormant','UNT',0,0,0 UNION ALL
Select '0601201000','Chicory plants ','UNT',0,0,0 UNION ALL
Select '0601202000','Chicory roots ','UNT',0,0,0 UNION ALL
Select '0601209000','Other ','UNT',0,0,0 UNION ALL
Select '0602101000','Of orchids ','UNT',0,0,0 UNION ALL
Select '0602102000','Of rubber trees','UNT',0,0,0 UNION ALL
Select '0602109000','Other','UNT',0,0,0 UNION ALL
Select '0602200000','Trees, shrubs and bushes, grafted or not, of kinds which bear  edible fruit or nuts','UNT',0,0,0 UNION ALL
Select '0602300000','Rhododendrons and azaleas, grafted or not ','UNT',0,0,0 UNION ALL
Select '0602400000','Roses, grafted or not','UNT',0,0,0 UNION ALL
Select '0602901000','Rooted orchid cuttings and slips ','KGM',0,0,0 UNION ALL
Select '0602902000','Orchid seedlings ','KGM',0,0,0 UNION ALL
Select '0602904000','Budded stumps of the genus Hevea','KGM',0,0,0 UNION ALL
Select '0602905000','Seedlings of the genus Hevea','KGM',0,0,0 UNION ALL
Select '0602906000','Budwood of the genus Hevea','KGM',0,2.19,0 UNION ALL
Select '0602909000','Other','KGM',0,0,0 UNION ALL
Select '0603110000','Roses','KGM',0,0,0 UNION ALL
Select '0603120000','Carnations','KGM',0,0,0 UNION ALL
Select '0603130000','Orchids','KGM',0,0,0 UNION ALL
Select '0603140000','Chrysanthemums','KGM',0,0,0 UNION ALL
Select '0603150000','Lilies (Lilium spp.)','KGM',0,0,0 UNION ALL
Select '0603190000','Other','KGM',0,0,0 UNION ALL
Select '0603900000','Other','KGM',0,0,0 UNION ALL
Select '0604201000','Mosses and lichens','KGM',0,0,0 UNION ALL
Select '0604209000','Other','KGM',0,0,0 UNION ALL
Select '0604901000','Mosses and lichens','KGM',0,0,0 UNION ALL
Select '0604909000','Other','KGM',0,0,0 UNION ALL
Select '0701100000','Seed','KGM',0,0,0 UNION ALL
Select '0701901000','Chipping potatoes','KGM',0,0,0 UNION ALL
Select '0701909000','Other','KGM',0,0,0 UNION ALL
Select '0702000000','Tomatoes, fresh or chilled.','KGM',0,0,0 UNION ALL
Select '0703101100','Bulbs for propagation ','KGM',0,0,0 UNION ALL
Select '0703101900','Other ','KGM',0,0,0 UNION ALL
Select '0703102100','Bulbs for propagation ','KGM',0,0,0 UNION ALL
Select '0703102900','Other ','KGM',0,0,0 UNION ALL
Select '0703201000','Bulbs for propagation ','KGM',0,0,0 UNION ALL
Select '0703209000','Other ','KGM',0,0,0 UNION ALL
Select '0703901000','Bulbs for propagation ','KGM',0,0,0 UNION ALL
Select '0703909000','Other ','KGM',0,0,0 UNION ALL
Select '0704101000','Cauliflowers','KGM',0,0,0 UNION ALL
Select '0704102000','Headed broccoli','KGM',0,0,0 UNION ALL
Select '0704200000','Brussels sprouts','KGM',0,0,0 UNION ALL
Select '0704901000','Round (drumhead) cabbages','KGM',0,0,0 UNION ALL
Select '0704902000','Chinese mustard ','KGM',0,0,0 UNION ALL
Select '0704909000','Other','KGM',0,0,0 UNION ALL
Select '0705110000','Cabbage lettuce (head lettuce)','KGM',0,0,0 UNION ALL
Select '0705190000','Other','KGM',0,0,0 UNION ALL
Select '0705210000','Witloof chicory (Cichorium intybus var. foliosum)','KGM',0,0,0 UNION ALL
Select '0705290000','Other','KGM',0,0,0  


INSERT INTO master.HSCode (Code,Description,UOMCode,ImportRate,ExportRate,ExciseRate)
Select '0706101000','Carrots','KGM',0,0,0 UNION ALL
Select '0706102000','Turnips','KGM',0,0,0 UNION ALL
Select '0706900000','Other','KGM',0,0,0 UNION ALL
Select '0707000000','Cucumbers and gherkins, fresh or chilled.','KGM',0,0,0 UNION ALL
Select '0708100000','Peas (Pisum sativum)','KGM',0,0,0 UNION ALL
Select '0708201000','French beans ','KGM',0,0,0 UNION ALL
Select '0708202000','Long beans ','KGM',0,0,0 UNION ALL
Select '0708209000','Other','KGM',5,0,0 UNION ALL
Select '0708900000','Other leguminous vegetables','KGM',0,0,0 UNION ALL
Select '0709200000','Asparagus','KGM',0,0,0 UNION ALL
Select '0709300000','Aubergines (eggplants)','KGM',0,0,0 UNION ALL
Select '0709400000','Celery other than celeriac','KGM',0,0,0 UNION ALL
Select '0709510000','Mushrooms of the genus Agaricus ','KGM',0,0,0 UNION ALL
Select '0709591000','Truffles','KGM',0,0,0 UNION ALL
Select '0709599000','Other','KGM',0,0,0 UNION ALL
Select '0709601000','Chillies (fruits of genus Capsicum)','KGM',0,0,0 UNION ALL
Select '0709609000','Other','KGM',0,0,0 UNION ALL
Select '0709700000','Spinach, New Zealand spinach and orache spinach (garden spinach) ','KGM',0,0,0 UNION ALL
Select '0709910000','Globe artichokes','KGM',0,0,0 UNION ALL
Select '0709920000','Olives','KGM',0,0,0 UNION ALL
Select '0709930000','Pumpkins, squash and gourds (Cucurbita spp.)','KGM',0,0,0 UNION ALL
Select '0709991000','Sweet corn','KGM',0,0,0 UNION ALL
Select '0709992000','Lady''s fingers (Okra) ','KGM',0,0,0 UNION ALL
Select '0709999000','Other','KGM',0,0,0 UNION ALL
Select '0710100000','Potatoes','KGM',0,0,0 UNION ALL
Select '0710210000','Peas (Pisum sativum)','KGM',0,0,0 UNION ALL
Select '0710220000','Beans (Vigna spp., Phaseolus spp.)','KGM',0,0,0 UNION ALL
Select '0710290000','Other','KGM',0,0,0 UNION ALL
Select '0710300000','Spinach, New Zealand spinach and orache spinach (garden spinach)','KGM',0,0,0 UNION ALL
Select '0710400000','Sweet corn','KGM',5,0,0 UNION ALL
Select '0710800000','Other vegetables','KGM',0,0,0 UNION ALL
Select '0710900000','Mixtures of vegetables','KGM',0,0,0   UNION ALL
Select '0711201000','Preserved by sulphur dioxide gas ','KGM',5,0,0 UNION ALL
Select '0711209000','Other ','KGM',0,0,0 UNION ALL
Select '0711401000','Preserved by sulphur dioxide gas ','KGM',7,0,0 UNION ALL
Select '0711409000','Other ','KGM',0,0,0 UNION ALL
Select '0711511000','Preserved by sulphur dioxide gas','KGM',7,0,0 UNION ALL
Select '0711519000','Other','KGM',0,0,0 UNION ALL
Select '0711591000','Preserved by sulphur dioxide gas','KGM',7,0,0 UNION ALL
Select '0711599000','Other','KGM',0,0,0 UNION ALL
Select '0711901000','Sweet corn ','KGM',0,0,0 UNION ALL
Select '0711902000','Chillies (fruits of genus Capsicum)','KGM',0,0,0 UNION ALL
Select '0711903000','Capers','KGM',0,0,0 UNION ALL
Select '0711904000','Onions, preserved by sulphur dioxide gas ','KGM',7,0,0 UNION ALL
Select '0711905000','Onions, preserved other than by sulphur dioxide gas ','KGM',0,0,0 UNION ALL
Select '0711906000','Other, preserved by sulphur dioxide gas ','KGM',7,0,0 UNION ALL
Select '0711909000','Other','KGM',0,0,0 UNION ALL
Select '0712200000','Onions','KGM',0,0,0 UNION ALL
Select '0712310000','Mushrooms of the genus Agaricus ','KGM',5,0,0 UNION ALL
Select '0712320000','Wood ears (Auricularia spp.) ','KGM',0,0,0 UNION ALL
Select '0712330000','Jelly fungi (Tremella spp.) ','KGM',0,0,0 UNION ALL
Select '0712391000','Truffles ','KGM',0,0,0 UNION ALL
Select '0712392000','Shiitake (Donggu)','KGM',5,0,0 UNION ALL
Select '0712399000','Other ','KGM',5,0,0 UNION ALL
Select '0712901000','Garlic','KGM',0,0,0 UNION ALL
Select '07129090','Other:','',0,0,0 UNION ALL
Select '0712909010','Sweet corn','KGM',0,0,0 UNION ALL
Select '0712909090','Other','KGM',0,0,0 UNION ALL
Select '0713101000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713109000','Other ','KGM',0,0,0 UNION ALL
Select '0713201000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713209000','Other ','KGM',0,0,0 UNION ALL
Select '0713311000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713319000','Other ','KGM',0,0,0 UNION ALL
Select '0713321000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713329000','Other ','KGM',0,0,0 UNION ALL
Select '0713331000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713339000','Other ','KGM',0,0,0 UNION ALL
Select '0713341000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713349000','Other','KGM',0,0,0 UNION ALL
Select '0713351000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713359000','Other','KGM',0,0,0 UNION ALL
Select '0713391000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713399000','Other ','KGM',0,0,0 UNION ALL
Select '0713401000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713409000','Other ','KGM',0,0,0 UNION ALL
Select '0713501000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713509000','Other ','KGM',0,0,0 UNION ALL
Select '0713601000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713609000','Other ','KGM',0,0,0 UNION ALL
Select '0713901000','Suitable for sowing','KGM',0,0,0 UNION ALL
Select '0713909000','Other','KGM',0,0,0 UNION ALL
Select '0714101100','Dried chips','KGM',0,0,0 UNION ALL
Select '0714101900','Other','KGM',0,0,0 UNION ALL
Select '0714109100','Frozen','KGM',0,0,0 UNION ALL
Select '0714109900','Other','KGM',0,0,0 UNION ALL
Select '0714201000','Frozen','KGM',0,0,0 UNION ALL
Select '0714209000','Other','KGM',0,0,0 UNION ALL
Select '0714301000','Frozen','KGM',0,0,0 UNION ALL
Select '0714309000','Other','KGM',0,0,0 UNION ALL
Select '0714401000','Frozen','KGM',0,0,0 UNION ALL
Select '0714409000','Other','KGM',0,0,0 UNION ALL
Select '0714501000','Frozen','KGM',0,0,0 UNION ALL
Select '0714509000','Other','KGM',0,0,0 UNION ALL
Select '0714901100','Frozen','KGM',0,0,0 UNION ALL
Select '0714901900','Other','KGM',0,0,0 UNION ALL
Select '0714909100','Frozen','KGM',0,0,0 UNION ALL
Select '0714909900','Other','KGM',0,0,0 UNION ALL
Select '0801110000','Desiccated','KGM',20,0,0 UNION ALL
Select '0801120000','In the inner shell (endocarp)','KGM',5,0,0 UNION ALL
Select '0801191000','   Young coconut     ','KGM',0.05,0,0 UNION ALL
Select '0801199000','   Other     ','KGM',0.05,0,0 UNION ALL
Select '0801210000','In shell','KGM',0,0,0 UNION ALL
Select '0801220000','Shelled','KGM',0,0,0 UNION ALL
Select '0801310000','In shell','KGM',0,0,0 UNION ALL
Select '0801320000','Shelled','KGM',0,0,0 UNION ALL
Select '0802110000','In shell','KGM',0,0,0 UNION ALL
Select '0802120000','Shelled','KGM',0,0,0 UNION ALL
Select '0802210000','In shell','KGM',0,0,0 UNION ALL
Select '0802220000','Shelled','KGM',0,0,0 UNION ALL
Select '0802310000','In shell','KGM',0,0,0 UNION ALL
Select '0802320000','Shelled','KGM',0,0,0 UNION ALL
Select '0802410000','In shell','KGM',0,0,0 UNION ALL
Select '0802420000','Shelled','KGM',0,0,0 UNION ALL
Select '0802510000','In shell','KGM',0,0,0 UNION ALL
Select '0802520000','Shelled','KGM',0,0,0  UNION ALL  
Select '0802610000','In shell','KGM',0,0,0 UNION ALL
Select '0802620000','Shelled','KGM',0,0,0 UNION ALL
Select '0802700000','Kola nuts (Cola spp.)','KGM',0,0,0 UNION ALL
Select '0802800000','Areca nuts','KGM',0,0,0 UNION ALL
Select '0802900000','Other','KGM',0,0,0 UNION ALL
Select '0803100000','Plantains','KGM',1.30,0,0 UNION ALL
Select '0803901000','Lady''s finger banana ','KGM',1.30,0,0 UNION ALL
Select '0803909030','Pisang berangan','KGM',1.30,0,0 UNION ALL
Select '0803909050','Dwarf cavendish','KGM',1.30,0,0 UNION ALL
Select '0803909090','Other','KGM',1.30,0,0 UNION ALL

Select '0804100000','Dates','KGM',0,0,0 UNION ALL
Select '0804200000','Figs','KGM',5,0,0 UNION ALL
Select '0804300000','Pineapples ','KGM',0.60 ,0,0 UNION ALL
Select '0804400000','Avocados ','KGM',5,0,0 UNION ALL

Select '0804501000','Guavas ','KGM',0.40,0,0 UNION ALL
Select '0804502000','Mangoes ','KGM',0.20,0,0 UNION ALL
Select '0804503000','Mangosteens ','KGM',0.40,0,0 UNION ALL
Select '0805101000','Fresh','KGM',0,0,0 UNION ALL
Select '0805102000','Dried','KGM',5,0,0 UNION ALL

Select '0805210000','Mandarins (including tangerines and satsumas)','KGM',5,0,0 UNION ALL
Select '0805220000','Clementines ','KGM',5,0,0 UNION ALL
Select '0805290000','Other','KGM',5,0,0 UNION ALL
Select '0805400000','Grapefruit, including pomelos','KGM',5,0,0 UNION ALL

Select '0805501000','Lemons (Citrus limon, Citrus limonum)','KGM',5,0,0 UNION ALL
Select '0805502000','Limes (Citrus aurantifolia, Citrus latifolia)','KGM',5,0,0 UNION ALL
Select '0805900000','Other','KGM',10,0,0 UNION ALL

Select '0806100000','Fresh ','KGM',5,0,0 UNION ALL
Select '0806200000','Dried ','KGM',0,0,0 UNION ALL


Select '0807110000','Watermelons','KGM',0.65,0,0 UNION ALL
Select '0807190000','Other','KGM',0.65,0,0 UNION ALL
Select '0807200000','Papaws  (papayas)','KGM',0.65,0,0 UNION ALL

Select '0808100000','Apples ','KGM',5,0,0 UNION ALL
Select '0808300000','Pears ','KGM',5,0,0 UNION ALL
Select '0808400000','Quinces ','KGM',5,0,0 UNION ALL

Select '0809100000','Apricots ','KGM',5,0,0 UNION ALL

Select '0809210000','Sour cherries (Prunus cerasus)','KGM',5,0,0 UNION ALL
Select '0809290000','Other','KGM',5,0,0 UNION ALL
Select '0809300000','Peaches, including nectarines ','KGM',5,0,0 UNION ALL

Select '0809401000','Plums','KGM',5,0,0 UNION ALL
Select '0809402000','Sloes','KGM',0,0,0 UNION ALL

Select '0810100000','Strawberries ','KGM',5,0,0 UNION ALL
Select '0810200000','Raspberries, blackberries, mulberries and loganberries','KGM',5,0,0 UNION ALL
Select '0810300000','Black, white or red currants and gooseberries','KGM',5,0,0 UNION ALL
Select '0810400000','Cranberries, bilberries and other fruits of the genus Vaccinium ','KGM',5,0,0 UNION ALL
Select '0810500000','Kiwifruit','KGM',15,0,0 UNION ALL
Select '0810600000','Durians ','KGM',0.30,0,0 UNION ALL
Select '0810700000','Persimmons','KGM',30,0,0 UNION ALL

Select '0810901000','Longans; Mata Kucing','KGM',30,0,0 UNION ALL
Select '0810902000','Lychees','KGM',30,0,0 UNION ALL
Select '0810903000','Rambutan ','KGM',0.65,0,0 UNION ALL
Select '0810904000','Langsat (Lanzones)','KGM',0.65,0,0 UNION ALL
Select '0810905000','Jackfruit (including Cempedak and Nangka)','KGM',0.30 ,0,0 UNION ALL
Select '0810906000','Tamarinds','KGM',0,0,0 UNION ALL
Select '0810907000','Starfruit','KGM',0.65,0,0 UNION ALL

Select '0810909100','Salacca (snake fruit)','KGM',0.65,0,0 UNION ALL
Select '0810909200','Dragon fruit','KGM',0.65,0,0 UNION ALL
Select '0810909300','Sapodilla (ciku fruit)','KGM',0.65,0,0 UNION ALL
Select '0810909400','Pomegranate (Punica spp.), soursop or sweetsops (Annona spp.), bell fruit (Syzygium Spp., Eugenia spp.),  marian plum (Bouea spp.), passion fruit (Passiflora spp.), cottonfruit (Sandoricum spp.) jujube (Ziziphus spp.) and  tampoi or rambai (Baccaurea spp.)','KGM', 0.65,0,0 UNION ALL
Select '0810909900','Other','KGM',30,0,0 UNION ALL

Select '0811100000','Strawberries','KGM',5,0,0 UNION ALL
Select '0811200000','Raspberries, blackberries, mulberries, loganberries, black, white or red currants and gooseberries','KGM',0,0,0 UNION ALL
Select '0811900000','Other','KGM',5,0,0 UNION ALL

Select '0812100000','Cherries ','KGM',5,0,0 UNION ALL

Select '0812901000','Strawberries','KGM',5,0,0 UNION ALL
Select '0812909000','Other','KGM',5,0,0 UNION ALL

Select '0813100000','Apricots ','KGM',5,0,0 UNION ALL
Select '0813200000','Prunes ','KGM',5,0,0 UNION ALL
Select '0813300000','Apples','KGM',5,0,0 UNION ALL

Select '0813401000','Longans','KGM',5,0,0 UNION ALL
Select '0813402000','Tamarinds','KGM',0,0,0 UNION ALL
Select '0813409000','Other','KGM',5,0,0 UNION ALL

Select '0813501000','Of which cashew nuts or Brazil nuts predominate by weight     ','KGM',7,0,0 UNION ALL
Select '0813502000','Of which other nuts predominate by weight','KGM',0,0,0 UNION ALL
Select '0813503000','Of which dates predominate by weight','KGM',0,0,0 UNION ALL
Select '08135040','Of which avocados or oranges or mandarins (including tangerines and satsumas) predominate by weight:','',0,0,0 UNION ALL
Select '0813504010','Of which avocados predominate','KGM',5,0,0 UNION ALL
Select '0813504020','Of which oranges or mandarins (including tangerines and  satsumas) predominate ','KGM',5,0,0 UNION ALL
Select '0813509000','Other','KGM',20,0,0 UNION ALL

Select '0814000000','Peel of citrus fruit or melons (including watermelons), fresh, frozen, dried or provisionally preserved in brine, in sulphur water or in other preservative solutions.','KGM',10,0,0 UNION ALL



Select '0901111000','Arabica WIB or Robusta OIB ','KGM',0,0,0 UNION ALL
Select '0901119000','Other ','KGM',0,0,0 UNION ALL

Select '0901121000','Arabica WIB or Robusta OIB ','KGM',0,0,0 UNION ALL
Select '0901129000','Other ','KGM',0,0,0 UNION ALL


Select '0901211000','Unground','KGM',0,0,0 UNION ALL
Select '0901212000','Ground','KGM',0,0,0 UNION ALL

Select '0901221000','Unground','KGM',0,0,0 UNION ALL
Select '0901222000','Ground','KGM',0,0,0 UNION ALL

Select '0901901000','Coffee husks and skins','KGM',0,0,0 UNION ALL
Select '0901902000','Coffee substitutes containing coffee','KGM',0,0,0 UNION ALL


Select '0902101000','Leaves','KGM',0,0,0 UNION ALL
Select '0902109000','Other ','KGM',0,0,0 UNION ALL

Select '0902201000','Leaves','KGM',0,0,0 UNION ALL
Select '0902209000','Other ','KGM',0,0,0 UNION ALL

Select '0902301000','Leaves','KGM',5,0,0 UNION ALL
Select '0902309000','Other ','KGM',5,0,0 UNION ALL

Select '0902401000','Leaves','KGM',5,0,0 UNION ALL
Select '0902409000','Other ','KGM',5,0,0 UNION ALL

Select '0903000000','Mate.','KGM',0,0,0 UNION ALL



Select '0904111000','White','KGM',0,0,0 UNION ALL
Select '0904112000','Black','KGM',0,0,0 UNION ALL
Select '0904119000','Other','KGM',0,0,0 UNION ALL

Select '0904121000','White','KGM',0,0,0 UNION ALL
Select '0904122000','Black','KGM',0,0,0 UNION ALL
Select '0904129000','Other ','KGM',0,0,0 UNION ALL


Select '0904211000','Chillies (Fruits of the genus Capsicum)','KGM',0,0,0 UNION ALL
Select '0904219000','Other','KGM',0,0,0 UNION ALL

Select '0904221000','Chillies (Fruits of the genus Capsicum)','KGM',0,0,0 UNION ALL
Select '0904229000','Other','KGM',0,0,0 UNION ALL

Select '0905100000','Neither crushed nor ground ','KGM',0,0,0 UNION ALL
Select '0905200000','Crushed or ground','KGM',0,0,0 UNION ALL


Select '0906110000','Cinnamon (Cinnamomum zeylanicum Blume)','KGM',0,0,0 UNION ALL
Select '0906190000','Other','KGM',0,0,0 UNION ALL
Select '0906200000','Crushed or ground','KGM',0,0,0 UNION ALL

Select '0907100000','Neither crushed nor ground ','KGM',0,0,0 UNION ALL
Select '0907200000','Crushed or ground','KGM',0,0,0 UNION ALL


Select '0908110000','Neither crushed nor ground ','KGM',0,0,0 UNION ALL
Select '0908120000','Crushed or ground','KGM',0,0,0 UNION ALL

Select '0908210000','Neither crushed nor ground ','KGM',0,0,0 UNION ALL
Select '0908220000','Crushed or ground','KGM',0,0,0 UNION ALL

Select '0908310000','Neither crushed nor ground ','KGM',0,0,0 UNION ALL
Select '0908320000','Crushed or ground','KGM',0,0,0 UNION ALL


Select '0909210000','Neither crushed nor ground','KGM',0,0,0 UNION ALL
Select '0909220000','Crushed or ground','KGM',0,0,0 UNION ALL

Select '0909310000','Neither crushed nor ground','KGM',0,0,0 UNION ALL
Select '0909320000','Crushed or ground','KGM',0,0,0 UNION ALL


Select '0909611000','Of anise','KGM',0,0,0 UNION ALL
Select '0909612000','Of badian','KGM',0,0,0 UNION ALL
Select '0909613000','Of caraway','KGM',0,0,0 UNION ALL
Select '0909619000','Other','KGM',0,0,0 UNION ALL

Select '0909621000','Of anise','KGM',0,0,0 UNION ALL
Select '0909622000','Of badian','KGM',0,0,0 UNION ALL
Select '0909623000','Of caraway','KGM',0,0,0 UNION ALL
Select '0909629000','Other','KGM',0,0,0 UNION ALL


Select '0910110000','Neither crushed nor ground','KGM',0,0,0 UNION ALL
Select '0910120000','Crushed or ground','KGM',0,0,0 UNION ALL
Select '0910200000','Saffron ','KGM',0,0,0 UNION ALL
Select '0910300000','Turmeric (curcuma) ','KGM',0,0,0 UNION ALL


Select '0910911000','Curry','KGM',0,0,0 UNION ALL
Select '0910919000','Other','KGM',0,0,0 UNION ALL

Select '0910991000','Thyme; bay leaves  ','KGM',0,0,0 UNION ALL
Select '0910999000','Other','KGM',0,0,0 UNION ALL


Select '1001110000','Seed','KGM',0,0,0 UNION ALL
Select '1001190000','Other','KGM',0,0,0 UNION ALL

Select '1001910000','Seed','KGM',0,0,0 UNION ALL


Select '1001991100','Meslin','KGM',0,0,0 UNION ALL
Select '1001991200','Wheat grain without husk','KGM',0,0,0 UNION ALL
Select '1001991900','Other','KGM',0,0,0 UNION ALL

Select '1001999100','Meslin','KGM',0,0,0 UNION ALL
Select '1001999900','Other','KGM',0,0,0 UNION ALL

Select '1002100000','Seed','KGM',0,0,0 UNION ALL
Select '1002900000','Other','KGM',0,0,0 UNION ALL

Select '1003100000','Seed','KGM',0,0,0 UNION ALL
Select '1003900000','Other','KGM',0,0,0 UNION ALL

Select '1004100000','Seed','KGM',0,0,0 UNION ALL
Select '1004900000','Other','KGM',0,0,0 UNION ALL

Select '1005100000','Seed ','KGM',0,0,0 UNION ALL

Select '1005901000','Popcorn','KGM',0,0,0 UNION ALL
Select '1005909000','Other','KGM',0,0,0 UNION ALL


Select '1006101000','Suitable for sowing','KGM',40,0,0 UNION ALL
Select '1006109000','Other','KGM',40,0,0 UNION ALL

Select '1006201000','Hom Mali rice ','KGM',40,0,0 UNION ALL
Select '10062090','Other: ','',0,0,0 UNION ALL
Select '1006209010','Glutinous rice ','KGM',40,0,0 UNION ALL
Select '1006209090','Other ','KGM',40,0,0 UNION ALL

Select '1006303000','Glutinous rice ','KGM',40,0,0 UNION ALL
Select '1006304000','Hom Mali rice','KGM',40,0,0 UNION ALL

Select '1006309100','Parboiled rice','KGM',40,0,0 UNION ALL
Select '1006309900','Other','KGM',40,0,0 UNION ALL

Select '1006401000','Of a kind used for animal feed','KGM',15,0,0 UNION ALL
Select '1006409000','Other','KGM',40,0,0 UNION ALL

Select '1007100000','Seed','KGM',0,0,0 UNION ALL
Select '1007900000','Other','KGM',0,0,0 UNION ALL

Select '1008100000','Buckwheat ','KGM',0,0,0 UNION ALL

Select '1008210000','Seed','KGM',0,0,0 UNION ALL
Select '1008290000','Other','KGM',0,0,0 UNION ALL
Select '1008300000','Canary seeds','KGM',0,0,0 UNION ALL
Select '1008400000','Fonio (Digitaria spp.)','KGM',0,0,0 UNION ALL
Select '1008500000','Quinoa (Chenopodium quinoa) ','KGM',0,0,0 UNION ALL
Select '1008600000','Triticale','KGM',0,0,0 UNION ALL
Select '1008900000','Other cereals ','KGM',0,0,0 UNION ALL


Select '1101001100','Fortified','KGM',0,0,0 UNION ALL
Select '1101001900','Other','KGM',0,0,0 UNION ALL
Select '1101002000','Meslin flour','KGM',0,0,0 UNION ALL

Select '1102200000','Maize (corn) flour ','KGM',0,0,0 UNION ALL

Select '1102901000','Rice flour','KGM',0,0,0 UNION ALL
Select '1102902000','Rye flour','KGM',0,0,0 UNION ALL
Select '1102909000','Other','KGM',0,0,0 UNION ALL


Select '1103110000','Of wheat','KGM',0,0,0 UNION ALL
Select '1103130000','Of maize (corn)','KGM',0,0,0 UNION ALL

Select '1103191000','Of meslin ','KGM',0,0,0 UNION ALL
Select '1103192000','Of rice','KGM',0,0,0 UNION ALL
Select '1103199000','Other ','KGM',0,0,0 UNION ALL
Select '1103200000','Pellets','KGM',0,0,0 UNION ALL


Select '1104120000','Of oats','KGM',0,0,0 UNION ALL

Select '1104191000','Of maize (corn)','KGM',0,0,0 UNION ALL
Select '1104199000','Other','KGM',0,0,0 UNION ALL

Select '1104220000','Of oats','KGM',0,0,0 UNION ALL
Select '1104230000','Of maize (corn)','KGM',0,0,0 UNION ALL

Select '1104292000','Of barley','KGM',0,0,0 UNION ALL
Select '1104299000','Other','KGM',0,0,0 UNION ALL
Select '1104300000','Germ of cereals, whole, rolled, flaked or ground','KGM',0,0,0 UNION ALL

Select '1105100000','Flour, meal and powder','KGM',0,0,0 UNION ALL
Select '1105200000',' Flakes, granules and pellets','KGM',0,0,0 UNION ALL

Select '1106100000','Of the dried leguminous vegetables of heading 07.13','KGM',0,0,0 UNION ALL

Select '1106201000','Of manioc (cassava)','KGM',0,0,0 UNION ALL
Select '1106202000','Of sago','KGM',5,0,0 UNION ALL
Select '1106203000','Of sweet potato (Ipomoea batatas)','KGM',0,0,0 UNION ALL
Select '1106209000','Other','KGM',0,0,0 UNION ALL
Select '1106300000','Of the products of Chapter 8','KGM',0,0,0 UNION ALL

Select '1107100000','Not roasted ','KGM',0,0,0 UNION ALL
Select '1107200000','Roasted ','KGM',0,0,0 UNION ALL


Select '1108110000','Wheat starch ','KGM',0,0,0 UNION ALL
Select '1108120000','Maize (corn) starch ','KGM',0,0,0 UNION ALL
Select '1108130000','Potato starch ','KGM',0,0,0 UNION ALL
Select '1108140000','Manioc (cassava) starch ','KGM',0,0,0 UNION ALL

Select '1108191000','Sago  ','KGM',0,0,0 UNION ALL
Select '1108199000','Other ','KGM',0,0,0 UNION ALL
Select '1108200000','Inulin ','KGM',0,0,0 UNION ALL

Select '1109000000','Wheat gluten, whether or not dried.','KGM',0,0,0 UNION ALL

Select '1201100000','Seed','KGM',0,0,0 UNION ALL
Select '1201900000','Other','KGM',0,0,0 UNION ALL

Select '1202300000','Seed','KGM',5,0,0 UNION ALL

Select '1202410000','In shell','KGM',5,0,0 UNION ALL
Select '1202420000','Shelled, whether or not broken','KGM',5,0,0 UNION ALL

Select '1203000000','Copra. ','KGM',0,0,0 UNION ALL

Select '1204000000','Linseed, whether or not broken. ','KGM',0,0,0 UNION ALL

Select '1205100000','Low erucic acid rape or colza seeds ','KGM',0,0,0 UNION ALL
Select '1205900000','Other ','KGM',0,0,0 UNION ALL

Select '1206000000','Sunflower seeds, whether or not broken.','KGM',0,0,0 UNION ALL


Select '1207101000','Palm nuts suitable for sowing/planting','TNE',0,5,0 UNION ALL
Select '1207103000','Kernels ','TNE',0,20,0 UNION ALL
Select '1207109000','Other','TNE',0,20,0 UNION ALL

Select '1207210000','Seed','KGM',0,0,0 UNION ALL
Select '1207290000','Other','KGM',0,0,0 UNION ALL
Select '1207300000','Castor oil seeds','KGM',0,0,0 UNION ALL

Select '1207401000','Edible','KGM',0,0,0 UNION ALL
Select '1207409000','Other','KGM',0,0,0 UNION ALL
Select '1207500000','Mustard seeds','KGM',0,0,0 UNION ALL
Select '1207600000','Safflower (Carthamus tinctorius) seeds','KGM',0,0,0 UNION ALL
Select '1207700000','Melon seeds','KGM',0,0,0 UNION ALL

Select '1207910000','Poppy seeds','KGM',0,0,0 UNION ALL

Select '1207994000','Illipe seeds (Illipe nuts)','KGM',0,0.08 ,0 UNION ALL
Select '1207995000','Fresh fruit bunch of oil palm','KGM',0,0,0 UNION ALL
Select '1207999000','Other','KGM',0,0,0 UNION ALL

Select '1208100000','Of soya beans','KGM',0,0,0 UNION ALL
Select '1208900000','Other','KGM',0,0,0 UNION ALL

Select '1209100000','Sugar beet seeds','KGM',0,0,0 UNION ALL

Select '1209210000','Lucerne (alfalfa) seeds','KGM',0,0,0 UNION ALL
Select '1209220000','Clover (Trifolium spp.) seeds','KGM',0,0,0 UNION ALL
Select '1209230000','Fescue seeds','KGM',0,0,0 UNION ALL
Select '1209240000','Kentucky blue grass (Poa pratensis L.) seeds','KGM',0,0,0 UNION ALL
Select '1209250000','Rye grass (Lolium multiflorum Lam., Lolium perenne L.) seeds','KGM',0,0,0 UNION ALL

Select '1209291000','Timothy grass (Phleum pratense) seeds','KGM',0,0,0 UNION ALL
Select '1209292000','Other beet seeds','KGM',0,0,0 UNION ALL
Select '1209299000','Other','KGM',0,0,0 UNION ALL
Select '1209300000','Seeds of herbaceous plants cultivated principally for their flowers','KGM',0,0,0 UNION ALL


Select '1209911000','Onion seeds','KGM',0,0,0 UNION ALL
Select '1209919000','Other','KGM',0,0,0 UNION ALL

Select '12099910','Rubber tree seeds; Kenaf seeds:','',0,0,0 UNION ALL

Select '1209991011','Sterile','KGM',0,0,0 UNION ALL
Select '1209991019','Other','KGM',0, 22.05,0 UNION ALL
Select '1209991030','Kenaf seeds','KGM',0,0,0 UNION ALL
Select '1209999000','Other ','KGM',0,0,0 UNION ALL

Select '1210100000','Hop cones, neither ground nor powdered nor in the form of pellets','KGM',0,0,0 UNION ALL
Select '1210200000','Hop cones, ground, powdered or in the form of pellets; lupulin ','KGM',0,0,0 UNION ALL

Select '1211200000','Ginseng roots','KGM',0,0,0 UNION ALL
Select '1211300000','Coca leaf','KGM',0,0,0 UNION ALL
Select '1211400000','Poppy straw ','KGM',0,0,0 UNION ALL
Select '1211500000','Ephedra','KGM',0,0,0 UNION ALL


Select '1211901100','Cannabis, in cut, crushed or powdered form','KGM',0,0,0 UNION ALL
Select '1211901200','Cannabis, in other forms','KGM',0,0,0 UNION ALL
Select '1211901300','Rauwolfia serpentina roots','KGM',0,0,0 UNION ALL
Select '1211901500','Liquorice roots','KGM',0,0,0 UNION ALL
Select '1211901600','Other, in cut, crushed or powdered form','KGM',0,0,0 UNION ALL
Select '1211901900','Other','KGM',0,0,0 UNION ALL

Select '1211909100','Pyrethrum, in cut, crushed or powdered form','KGM',0,0,0 UNION ALL
Select '1211909200','Pyrethrum, in other forms','KGM',0,0,0 UNION ALL
Select '1211909400','Sandalwood chips','KGM',0,0,0 UNION ALL
Select '1211909500','Agarwood (Gaharu) chips','KGM',0,0,0 UNION ALL
Select '1211909700','Bark of persea (Persea Kurzii Kosterm)','KGM',0,0,0 UNION ALL
Select '12119098','Other, in cut, crushed or powdered form:','',0,0,0 UNION ALL
Select '1211909810','Of Agarwood (Gaharu)','KGM',0,0,0 UNION ALL
Select '1211909890','Other','KGM',0,0,0 UNION ALL
Select '1211909900','Other','KGM',0,0,0 UNION ALL


Select '12122100','Fit for human consumption:','',0,0,0 UNION ALL

Select '1212211100','Eucheuma spinosum','KGM',0,0,0 UNION ALL
Select '1212211200','Eucheuma cottonii','KGM',0,0,0 UNION ALL
Select '1212211300','Gracilaria spp. ','KGM',0,0,0 UNION ALL
Select '1212211400','Gelidium spp. ','KGM',0,0,0 UNION ALL
Select '1212211500','Sargassum spp.','KGM',0,0,0 UNION ALL
Select '1212211900','Other','KGM',0,0,0 UNION ALL
Select '1212219000','Other','KGM',0,0,0 UNION ALL


Select '1212291100','Of a kind used in pharmacy','KGM',0,0,0 UNION ALL
Select '1212291900','Other','KGM',0,0,0 UNION ALL
Select '1212292000','Other, fresh, chilled or dried','KGM',0,0,0 UNION ALL
Select '1212293000','Other, frozen','KGM',0,0,0 UNION ALL

Select '1212910000','Sugar beet ','KGM',0,0,0 UNION ALL
Select '1212920000','Locust beans (carob)','KGM',0,0,0 UNION ALL

Select '1212931000','Suitable for planting','KGM',5,0,0 UNION ALL
Select '1212939000','Other','KGM',5,0,0 UNION ALL
Select '1212940000','Chicory roots','KGM',5,0,0 UNION ALL

Select '1212991000','Stones and kernels of apricot, peach (including nectarine) or plum','KGM',0,0,0 UNION ALL
Select '1212999000','Other','KGM',5,0,0 UNION ALL

Select '1213000000','Cereal straw and husks, unprepared, whether or not chopped, ground, pressed or in the form of pellets.','KGM',0,0,0 UNION ALL

Select '1214100000','Lucerne (alfalfa) meal and pellets','KGM',0,0,0 UNION ALL
Select '1214900000','Other','KGM',0,0,0 UNION ALL

Select '1301200000','Gum Arabic','KGM',0,0,0 UNION ALL

Select '1301903000','Cannabis resins ','KGM',0,0,0 UNION ALL
Select '1301904000','Lac','KGM',0,0,0 UNION ALL
Select '1301909000','Other','KGM',0,0,0 UNION ALL



Select '1302111000','Pulvis opii','KGM',0,0,0 UNION ALL
Select '1302119000','Other ','KGM',0,0,0 UNION ALL
Select '1302120000','Of liquorice','KGM',0,0,0 UNION ALL
Select '1302130000','Of hops','KGM',0,0,0 UNION ALL
Select '1302140000','Of ephedra','KGM',0,0,0 UNION ALL

Select '1302192000','Extracts and tinctures of cannabis ','KGM',0,0,0 UNION ALL
Select '1302194000','Vegetable saps and extracts of pyrethrum or of the roots  of plants containing rotenone','KGM',0,0,0 UNION ALL
Select '1302195000','Japan (or Chinese) lacquer (natural lacquer)','KGM',0,0,0 UNION ALL
Select '1302199000','Other ','KGM',0,0,0 UNION ALL
Select '1302200000','Pectic substances, pectinates and pectates','KGM',0,0,0 UNION ALL

Select '1302310000','Agaragar','KGM',0,0,0 UNION ALL
Select '1302320000','Mucilages and thickeners, whether or not modified, derived  from locust beans, locust bean seeds or guar seeds ','KGM',0,0,0 UNION ALL


Select '1302391100','Powder, semirefined ','KGM',0,0,0 UNION ALL
Select '1302391200','Powder, refined ','KGM',0,0,0 UNION ALL
Select '1302391300','Alkali treated carrageenan chips (ATCC)','KGM',0,0,0 UNION ALL
Select '1302391900','Other','KGM',0,0,0 UNION ALL
Select '1302399000','Other ','KGM',0,0,0 UNION ALL

Select '1401100000','Bamboos','KGM',0,0,0 UNION ALL

Select '1401201000','Whole','KGM',0, 2.70,0 UNION ALL

Select '1401202100','Not exceeding 12 mm in diameter','KGM',0, 1.00,0 UNION ALL
Select '1401202900','Other','KGM',0, 2.70,0 UNION ALL
Select '1401203000','Splitskin','KGM',0, 1.00,0 UNION ALL
Select '1401209000','Other','KGM',0, 2.70,0 UNION ALL
Select '1401900000','Other','KGM',0,0,0 UNION ALL

Select '1404200000','Cotton linters','KGM',0,0,0 UNION ALL

Select '1404902000','Of a kind used primarily in tanning or dyeing','KGM',0,0,0 UNION ALL
Select '1404903000','Kapok','KGM',0,0,0 UNION ALL

Select '1404909100','Palm kernel shells','KGM',0,0,0 UNION ALL
Select '1404909200','Empty fruit bunch of oil palm','KGM',0,0,0 UNION ALL
Select '1404909900','Other','KGM',0,0,0 UNION ALL

Select '1501100000','Lard','KGM',10,0,0 UNION ALL
Select '1501200000','Other pig fat','KGM',5,0,0 UNION ALL
Select '1501900000','Other','KGM',5,0,0 UNION ALL

Select '1502100000','Tallow','KGM',5,0,0 UNION ALL

Select '1502901000','Edible','KGM',5,0,0 UNION ALL
Select '1502909000','Other','KGM',5,0,0 UNION ALL


Select '1503001000','Lard stearin or oleostearin ','KGM',5,0,0 UNION ALL
Select '1503009000','Other ','KGM',5,0,0 UNION ALL


Select '1504102000','Solid fractions','KGM',5,0,0 UNION ALL
Select '1504109000','Other','KGM',15,0,0 UNION ALL

Select '1504201000','Solid fractions','KGM',5,0,0 UNION ALL
Select '1504209000','Other','KGM',5,0,0 UNION ALL
Select '1504300000','Fats and oils and their fractions, of marine mammals','KGM',5,0,0 UNION ALL


Select '1505001000','Lanolin ','KGM',0,0,0 UNION ALL
Select '1505009000','Other ','KGM',0,0,0 UNION ALL

Select '1506000000','Other animal fats and oils and their fractions, whether or not refined, but not chemically modified.','KGM',5,0,0 UNION ALL

Select '1507100000','Crude oil, whether or not degummed ','KGM',5,0,0 UNION ALL

Select '1507901000','Fractions of unrefined soyabean oil','KGM',5,0,0 UNION ALL
Select '1507909000','Other','KGM',5,0,0 UNION ALL

Select '1508100000','Crude oil','KGM',5,0,0 UNION ALL
Select '1508900000','Other','KGM',5,0,0 UNION ALL


Select '1509101000','In packings of net weight not exceeding 30 kg ','KGM',0,0,0 UNION ALL
Select '1509109000','Other ','KGM',0,0,0 UNION ALL


Select '1509901100','In packings of a net weight not exceeding 30 kg','KGM',0,0,0 UNION ALL
Select '1509901900','Other','KGM',0,0,0 UNION ALL

Select '1509909100','In packings of a net weight not exceeding 30 kg','KGM',0,0,0 UNION ALL
Select '1509909900','Other','KGM',0,0,0 UNION ALL


Select '1510001000','Crude oil','KGM',0,0,0 UNION ALL
Select '1510002000','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1510009000','Other','KGM',0,0,0 UNION ALL

Select '1511100000','Crude oil','TNE',0,0,0 UNION ALL

Select '1511902000','Refined oil','TNE',5,0,0 UNION ALL


Select '1511903100','With iodine value 30 or more, but less than 40','TNE',5,0,0 UNION ALL
Select '1511903200','Other','TNE',5,0,0 UNION ALL

Select '1511903600','In packing of a net weight not exceeding 25 kg','TNE',5,0,0 UNION ALL
Select '1511903700','Other, with iodine value 55 or more but less than 60','TNE',5,0,0 UNION ALL
Select '1511903900','Other','TNE',5,0,0 UNION ALL

Select '1511904100','Solid fractions','TNE',5,0,0 UNION ALL
Select '1511904200','Other, with packing of a net weight not exceeding 25 kg','TNE',5,0,0 UNION ALL
Select '1511904900','Other','TNE',5,0,0 UNION ALL


Select '1512110000','Crude oil','KGM',0,0,0 UNION ALL

Select '1512191000','Fractions of unrefined sunflowerseed oil or safflower oil','KGM',0,0,0 UNION ALL
Select '1512192000','Refined','KGM',0,0,0 UNION ALL
Select '1512199000','Other','KGM',0,0,0 UNION ALL

Select '1512210000','Crude oil, whether or not gossypol has been removed','KGM',0,0,0 UNION ALL

Select '1512291000','Fractions of unrefined cottonseed oil','KGM',0,0,0 UNION ALL
Select '1512299000','Other','KGM',0,0,0 UNION ALL


Select '1513110000','Crude oil ','KGM',5,0,0 UNION ALL

Select '1513191000','Fractions of unrefined coconut oil ','KGM',5,0,0 UNION ALL
Select '1513199000','Other ','KGM',5,0,0 UNION ALL


Select '1513211000','Palm kernel oil ','TNE',0,10,0 UNION ALL
Select '1513219000','Other','KGM',0,0,0 UNION ALL


Select '1513291100','Solid fractions of unrefined palm kernel oil','TNE',5,0,0 UNION ALL
Select '1513291200','Solid fractions of unrefined babassu oil','KGM',5,0,0 UNION ALL
Select '1513291300','Other, of unrefined palm kernel oil (palm kernel olein) ','TNE',0,0,0 UNION ALL
Select '1513291400','Other, of unrefined babassu oil','KGM',0,0,0 UNION ALL

Select '1513299100','Solid fractions of palm kernel oil','TNE',5,0,0 UNION ALL
Select '1513299200','Solid fractions of babassu oil','KGM',5,0,0 UNION ALL
Select '1513299400','Palm kernel olein, refined, bleached and deodorized (RBD)','TNE',0,0,0 UNION ALL
Select '1513299500','Palm kernel oil, RBD','TNE',0,5,0 UNION ALL
Select '1513299600','Other, palm kernel oil','TNE',2,0,0 UNION ALL
Select '1513299700','Other, of babassu oil','KGM',0,0,0 UNION ALL


Select '1514110000','Crude oil ','KGM',0,0,0 UNION ALL

Select '1514191000','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1514192000','Refined','KGM',0,0,0 UNION ALL
Select '1514199000','Other','KGM',0,0,0 UNION ALL


Select '1514911000','Other rape or colza oil ','KGM',0,0,0 UNION ALL
Select '1514919000','Other ','KGM',0,0,0 UNION ALL

Select '1514991000','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1514999000','Other','KGM',0,0,0 UNION ALL


Select '1515110000','Crude oil','KGM',0,0,0 UNION ALL
Select '1515190000','Other','KGM',0,0,0 UNION ALL

Select '1515210000','Crude oil','KGM',0,0,0 UNION ALL


Select '1515291100','Solid fractions ','KGM',5,0,0 UNION ALL
Select '1515291900','Other','KGM',0,0,0 UNION ALL

Select '1515299100','Solid fractions','KGM',5,0,0 UNION ALL
Select '1515299900','Other','KGM',0,0,0 UNION ALL

Select '1515301000','Crude oil','KGM',0,0,0 UNION ALL
Select '1515309000','Other','KGM',0,0,0 UNION ALL

Select '1515501000','Crude oil','KGM',0,0,0 UNION ALL
Select '1515502000','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1515509000','Other','KGM',0,0,0 UNION ALL


Select '1515901100','Crude oil','KGM',0,0,0 UNION ALL
Select '1515901200','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1515901900','Other','KGM',0,0,0 UNION ALL

Select '1515902100','Crude oil','KGM',0,0,0 UNION ALL
Select '1515902200','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1515902900','Other','KGM',0,0,0 UNION ALL

Select '1515903100','Crude oil','KGM',0,0,0 UNION ALL
Select '1515903200','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1515903900','Other','KGM',0,0,0 UNION ALL

Select '1515909100','Crude oil','KGM',0,0,0 UNION ALL
Select '1515909200','Fractions of unrefined oil','KGM',0,0,0 UNION ALL
Select '1515909900','Other','KGM',0,0,0 UNION ALL


Select '1516102000','Reesterified','KGM',5,0,0 UNION ALL
Select '1516109000','Other','KGM',5,0,0 UNION ALL


Select '1516201100','Of soya beans','KGM',5,0,0 UNION ALL
Select '1516201200','Of the fruit of the oil palm, crude','TNE',0,0,0 UNION ALL
Select '1516201300','Of the fruit of the oil palm, other than crude','TNE',5,0,0 UNION ALL
Select '1516201400','Of coconuts','KGM',5,0,0 UNION ALL
Select '1516201500','Of palm kernels, crude','TNE',0,10,0 UNION ALL
Select '1516201600','Of palm kernels, refined, bleached and deodorized (RBD)','TNE',0,5,0 UNION ALL
Select '1516201700','Of groundnuts','KGM',0,0,0 UNION ALL
Select '1516201800','Of linseed','KGM',0,0,0 UNION ALL
Select '15162019','Other:','',0,0,0 UNION ALL
Select '1516201941','Of palm kernel olein, crude','TNE',0,0,0 UNION ALL
Select '1516201942','Of palm kernel olein, refined, bleached and deodorised (RBD)','TNE',0,0,0 UNION ALL
Select '1516201960','Of maize','KGM',0,0,0 UNION ALL
Select '1516201990','Other','KGM',0,0,0 UNION ALL

Select '1516203100','Of groundnuts; of coconut; of soya beans','KGM',5,0,0 UNION ALL
Select '1516203200','Of linseed','KGM',5,0,0 UNION ALL
Select '1516203300','Of olives','KGM',5,0,0 UNION ALL
Select '1516203400','Of fruit of the oil palm','KGM',5,0,0 UNION ALL
Select '1516203500','Of palm kernels','KGM',5,0,0 UNION ALL
Select '1516203900','Other','KGM',5,0,0 UNION ALL

Select '1516204100','Of castor seeds (Opal wax)','KGM',0,0,0 UNION ALL
Select '1516204200','Of coconuts','KGM',5,0,0 UNION ALL
Select '1516204300','Of groundnuts','KGM',5,0,0 UNION ALL
Select '1516204400','Of linseed','KGM',5,0,0 UNION ALL
Select '1516204500','Of olives','KGM',5,0,0 UNION ALL
Select '1516204600','Of fruit of the oil palm','KGM',5,0,0 UNION ALL
Select '1516204700','Of palm kernels','TNE',5,0,0 UNION ALL
Select '1516204800','Of soya beans','KGM',5,0,0 UNION ALL
Select '1516204900','Other','KGM',5,0,0 UNION ALL

Select '1516205100','Of linseed','KGM',5,0,0 UNION ALL
Select '1516205200','Of olives','KGM',5,0,0 UNION ALL
Select '1516205300','Of soya beans','KGM',5,0,0 UNION ALL
Select '1516205400','Of groundnuts, of oil palm or coconuts','KGM',5,0,0 UNION ALL
Select '1516205900','Other','KGM',5,0,0 UNION ALL

Select '1516206100','Crude','TNE',5,0,0 UNION ALL
Select '1516206200','Refined, bleached and deodorised (RBD)','TNE',0,0,0 UNION ALL
Select '1516206900','Other','TNE',5,0,0 UNION ALL

Select '1516209100','Palm stearin, with an iodine value exceeding 48','TNE',5,0,0 UNION ALL
Select '1516209200','Of linseed','KGM',5,0,0 UNION ALL
Select '1516209300','Of olives','KGM',5,0,0 UNION ALL
Select '1516209400','Of soya beans','KGM',5,0,0 UNION ALL
Select '1516209600','Refined, bleached and deodorised (RBD) palm kernel stearin','TNE',5,0,0 UNION ALL
Select '1516209800','Of groundnuts, of oil palm or of coconuts','KGM',5,0,0 UNION ALL
Select '1516209900','Other','KGM',5,0,0 UNION ALL


Select '1517101000','In airtight containers for retail sale','KGM',10,0,0 UNION ALL
Select '1517109000','Other','KGM',10,0,0 UNION ALL

Select '1517901000','Imitation ghee ','KGM',10,0,0 UNION ALL
Select '1517902000','Liquid margarine ','KGM',20,0,0 UNION ALL
Select '1517903000','Of a kind used as mould release preparations ','KGM',10,0,0 UNION ALL

Select '1517904300','Shortening ','KGM',10,0,0 UNION ALL
Select '1517904400','Imitation lard  ','KGM',10,0,0 UNION ALL
Select '1517905000','Other solid mixtures or preparations of vegetable fats or oils or of their fractions','KGM',10,0,0 UNION ALL

Select '1517906100','In which groundnut oil predominates','KGM',10,0,0 UNION ALL
Select '1517906200','In which crude palm oil predominates','TNE',0,0,0 UNION ALL
Select '1517906300','In which other palm oil predominates, in packings of a net weight not exceeding 25 kg','TNE',5,0,0 UNION ALL
Select '1517906400','In which other palm oil predominates, in packings of a net weight exceeding 25kg','TNE',5,0,0 UNION ALL
Select '15179065','In which palm kernel oil predominates:','',0,0,0 UNION ALL
Select '1517906510','Crude','TNE',0,0,0 UNION ALL
Select '1517906520','Neutralised or refined, bleached and deodorised (NBD or RBD)','TNE',0,0,0 UNION ALL
Select '15179066','In which palm kernel olein predominates:','',0,0,0 UNION ALL
Select '1517906610','Crude','TNE',0,0,0 UNION ALL
Select '1517906620','Neutralised or refined, bleached and deodorised (NBD or RBD)','TNE',0,0,0 UNION ALL
Select '1517906700','In which either soyabean oil or coconut oil predominates','KGM',5,0,0 UNION ALL
Select '1517906800','In which illipe nut oil predominates','KGM',0,0,0 UNION ALL
Select '15179069','Other:','',0,0,0 UNION ALL
Select '1517906911','In which linseed oil predominates','KGM',0,0,0 UNION ALL
Select '1517906912','In which castor oil predominates','KGM',0,0,0 UNION ALL
Select '1517906913','In which tung oil predominates','KGM',0,0,0 UNION ALL
Select '1517906914','In which sesame oil predominates','KGM',0,0,0 UNION ALL
Select '1517906915','In which almond oil predominates','KGM',0,0,0 UNION ALL
Select '1517906916','In which maize (corn oil) predominates','KGM',0,0,0 UNION ALL
Select '1517906917','In which cotton seed oil predominates','KGM',0,0,0 UNION ALL
Select '1517906918','In which olive oil predominates','KGM',0,0,0 UNION ALL
Select '1517906919','In which sunflower oil predominates','KGM',0,0,0 UNION ALL
Select '1517906920','In which rape, colza or mustard oil predominates','KGM',0,0,0 UNION ALL
Select '1517906990','Other','KGM',0,0,0 UNION ALL
Select '1517908000','Of mixtures or preparations of animal fats or oils or of their fractions','KGM',10,0,0 UNION ALL
Select '1517909000','Other','KGM',10,0,0 UNION ALL



Select '1518001200','Animal fats and oils','KGM',0,0,0 UNION ALL
Select '1518001400','Groundnut, soyabean, palm or coconut oil','KGM',0,0,0 UNION ALL
Select '1518001500','Linseed oil and its fractions','KGM',0,0,0 UNION ALL
Select '1518001600','Olive oil and its fractions','KGM',0,0,0 UNION ALL
Select '1518001900','Other','KGM',0,0,0 UNION ALL
Select '1518002000','Inedible mixtures or preparations of animal fats or oils or of fractions of different fats or oils ','KGM',0,0,0 UNION ALL

Select '15180031','Of the fruit of the oil palm or of palm kernels:','',0,0,0 UNION ALL

Select '1518003111','Crude','TNE',0,0,0 UNION ALL
Select '1518003119','Other','TNE',5,0,0 UNION ALL

Select '1518003121','Crude palm kernel olein','TNE',0,0,0 UNION ALL
Select '1518003122','Neutralised or refined, bleached and deodorised  palm kernel olein (NBD or RBD)','TNE',5,0,0 UNION ALL
Select '1518003129','Other','TNE',0,0,0 UNION ALL
Select '1518003300','Of linseed ','KGM',0,0,0 UNION ALL
Select '1518003400','Of olives','KGM',0,0,0 UNION ALL
Select '1518003500','Of groundnuts','KGM',10,0,0 UNION ALL
Select '15180036','Of soya beans or coconuts:','',0,0,0 UNION ALL
Select '1518003610','Of soya beans','KGM',5,0,0 UNION ALL
Select '1518003620','Of coconuts','KGM',5,0,0 UNION ALL
Select '1518003700','Of cotton seeds','KGM',5,0,0 UNION ALL
Select '15180039','Other:','',0,0,0 UNION ALL
Select '1518003911','Of castor oil ','KGM',0,0,0 UNION ALL
Select '1518003912','Of tung oil','KGM',0,0,0 UNION ALL
Select '1518003913','Of sesame oil','KGM',0,0,0 UNION ALL
Select '1518003914','Of almond oil','KGM',0,0,0 UNION ALL
Select '1518003915','Of maize (corn) oil','KGM',0,0,0 UNION ALL
Select '1518003917','Of sunflower oil','KGM',0,0,0 UNION ALL
Select '1518003918','Of rape, colza or mustard oil','KGM',0,0,0 UNION ALL
Select '1518003919','Of illipenut oil ','KGM',0,0,0 UNION ALL
Select '1518003990','Other','KGM',0,0,0 UNION ALL
Select '1518006000','Inedible mixtures or preparations of animal fats or oils or of fractions thereof and vegetable fats or oils or fractions thereof','KGM',0,0,0 UNION ALL


Select '1520001000','Crude glycerol ','KGM',5,0,0 UNION ALL
Select '1520009000','Other ','KGM',5,0,0 UNION ALL

Select '1521100000','Vegetable waxes','KGM',0,0,0 UNION ALL

Select '1521901000','Beeswax and other insect waxes ','KGM',0,0,0 UNION ALL
Select '1521902000','Spermaceti ','KGM',0,0,0 UNION ALL


Select '1522001000','Degras ','KGM',5,0,0 UNION ALL
Select '1522009000','Other ','KGM',5,0,0 UNION ALL


Select '1601001000','In airtight containers for retail sale','KGM',15,0,0 UNION ALL
Select '1601009000','Other','KGM',10,0,0 UNION ALL


Select '1602101000','Containing pork, in airtight containers for retail sale  ','KGM',15,0,0 UNION ALL
Select '1602109000','Other','KGM',0,0,0 UNION ALL
Select '1602200000','Of liver of any animal','KGM',0,0,0 UNION ALL


Select '1602311000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL

Select '1602319100','Of mechanically deboned or separated meat','KGM',0,0,0 UNION ALL
Select '1602319900','Other','KGM',0,0,0 UNION ALL

Select '1602321000','Chicken curry, in airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1602329000','Other','KGM',0,0,0 UNION ALL
Select '1602390000','Other','KGM',0,0,0 UNION ALL


Select '16024110','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '1602411010','Infant or young children food','KGM',0,0,0 UNION ALL
Select '1602411090','Other','KGM',10,0,0 UNION ALL
Select '1602419000','Other','KGM',0,0,0 UNION ALL

Select '16024210','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '1602421010','Infant or young children food','KGM',0,0,0 UNION ALL
Select '1602421090','Other','KGM',10,0,0 UNION ALL
Select '1602429000','Other','KGM',0,0,0 UNION ALL


Select '16024911','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '1602491110','Infant or young children food','KGM',0,0,0 UNION ALL
Select '1602491190','Other','KGM',10,0,0 UNION ALL
Select '1602491900','Other','KGM',0,0,0 UNION ALL

Select '16024991','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '1602499110','Infant or young children food','KGM',0,0,0 UNION ALL
Select '1602499190','Other','KGM',10,0,0 UNION ALL
Select '1602499900','Other','KGM',0,0,0 UNION ALL
Select '16025000','Of bovine animals:','',0,0,0 UNION ALL
Select '1602500010','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1602500090','Other','KGM',0,0,0 UNION ALL

Select '1602901000','Mutton curry, in airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '16029090','Other:','',0,0,0 UNION ALL
Select '1602909020','Preparations of blood','KGM',15,0,0 UNION ALL
Select '1602909090','Other','KGM',0,0,0 UNION ALL


Select '1603000010','Of chicken','KGM',20,0,0 UNION ALL
Select '1603000020','Of meat','KGM',20,0,0 UNION ALL
Select '1603000090','Other','KGM',20,0,0 UNION ALL



Select '1604111000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604119000','Other ','KGM',0,0,0 UNION ALL

Select '1604121000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604129000','Other ','KGM',0,0,0 UNION ALL


Select '1604131100','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604131900','Other ','KGM',0,0,0 UNION ALL

Select '1604139100','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604139900','Other ','KGM',0,0,0 UNION ALL


Select '1604141100','Tunas','KGM',0,0,0 UNION ALL
Select '1604141900','Other ','KGM',0,0,0 UNION ALL
Select '1604149000','Other','KGM',0,0,0 UNION ALL

Select '1604151000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604159000','Other ','KGM',0,0,0 UNION ALL

Select '1604161000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604169000','Other ','KGM',0,0,0 UNION ALL

Select '1604171000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604179000','Other ','KGM',0,0,0 UNION ALL

Select '1604181000','Ready for immediate consumption','KGM',0,0,0 UNION ALL

Select '1604189100','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604189900','Other','KGM',6,0,0 UNION ALL

Select '1604192000','Horse mackerel, in airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604193000','Other, in airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604199000','Other','KGM',0,0,0 UNION ALL

Select '1604202000','Fish sausages','KGM',0,0,0 UNION ALL
Select '1604203000','Fish ball','KGM',0,0,0 UNION ALL
Select '1604204000','Fish paste','KGM',0,0,0 UNION ALL

Select '1604209100','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1604209900','Other','KGM',0,0,0 UNION ALL

Select '1604310000','Caviar','KGM',8,0,0 UNION ALL
Select '1604320000','Caviar substitutes','KGM',8,0,0 UNION ALL


Select '1605101000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1605109000','Other','KGM',0,0,0 UNION ALL

Select '1605210000','Not in airtight container','KGM',0,0,0 UNION ALL

Select '1605292000','Shrimp ball','KGM',0,0,0 UNION ALL
Select '1605293000','Breaded shrimp','KGM',0,0,0 UNION ALL
Select '1605299000','Other','KGM',0,0,0 UNION ALL
Select '1605300000','Lobster','KGM',0,0,0 UNION ALL
Select '1605400000','Other crustaceans','KGM',0,0,0 UNION ALL

Select '1605510000','Oysters','KGM',0,0,0 UNION ALL
Select '1605520000','Scallops, including queen scallops','KGM',0,0,0 UNION ALL
Select '1605530000','Mussels','KGM',0,0,0 UNION ALL

Select '1605541000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1605549000','Other','KGM',0,0,0 UNION ALL
Select '1605550000','Octopus','KGM',0,0,0 UNION ALL
Select '1605560000','Clams, cockles and arkshells','KGM',0,0,0 UNION ALL

Select '1605571000','In airtight containers for retail sale','KGM',0,0,0 UNION ALL
Select '1605579000','Other      ','KGM',0,0,0 UNION ALL
Select '1605580000','Snails, other than sea snails','KGM',0,0,0 UNION ALL
Select '1605590000','Other','KGM',0,0,0 UNION ALL

Select '1605610000','Sea cucumbers','KGM',0,0,0 UNION ALL
Select '1605620000','Sea urchins','KGM',0,0,0 UNION ALL
Select '1605630000','Jellyfish','KGM',0,0,0 UNION ALL
Select '1605690000','Other','KGM',0,0,0 UNION ALL


Select '1701120000','Beet sugar','KGM',0,0,0 UNION ALL
Select '1701130000','Cane sugar specified in Subheading Note 2 to this Chapter','KGM',0,0,0 UNION ALL
Select '1701140000','Other cane sugar','KGM',0,0,0 UNION ALL

Select '1701910000','Containing added flavouring or colouring matter','KGM',0,0,0 UNION ALL

Select '1701991000','Refined sugar','KGM',0,0,0 UNION ALL
Select '1701999000','Other ','KGM',0,0,0 UNION ALL


Select '1702110000','Containing by weight 99% or more lactose, expressed as anhydrous lactose, calculated on the dry matter','KGM',0,0,0 UNION ALL
Select '1702190000','Other','KGM',0,0,0 UNION ALL
Select '1702200000','Maple sugar and  maple syrup','KGM',0,0,0 UNION ALL

Select '1702301000','Glucose ','KGM',0,0,0 UNION ALL
Select '1702302000','Glucose syrup ','KGM',10,0,0 UNION ALL
Select '1702400000','Glucose and glucose syrup, containing in the dry state at least  20% but less than 50% by weight of fructose, excluding invert sugar','KGM',0,0,0 UNION ALL
Select '1702500000','Chemically pure fructose ','KGM',0,0,0 UNION ALL

Select '1702601000','Fructose ','KGM',0,0,0 UNION ALL
Select '1702602000','Fructose syrup ','KGM',10,0,0 UNION ALL


Select '1702901100','Chemically pure maltose','KGM',0,0,0 UNION ALL
Select '1702901900','Other','KGM',0,0,0 UNION ALL
Select '1702902000','Artificial honey, whether or not mixed with natural honey ','KGM',0,0,0 UNION ALL
Select '1702903000','Flavoured or coloured sugars (excluding maltose)','KGM',0,0,0 UNION ALL
Select '1702904000','Caramel','KGM',0,0,0 UNION ALL

Select '1702909100','Sugar syrups','KGM',0,0,0 UNION ALL
Select '1702909900','Other','KGM',0,0,0 UNION ALL


Select '1703101000','Containing added flavouring or colouring matter','KGM',0,0,0 UNION ALL
Select '1703109000','Other','KGM',0,0,0 UNION ALL

Select '1703901000','Containing added flavouring or colouring matter','KGM',0,0,0 UNION ALL
Select '1703909000','Other','KGM',0,0,0 UNION ALL

Select '1704100000','Chewing gum, whether or not sugarcoated','KGM',15,0,0 UNION ALL

Select '1704901000','Medicated pastilles and drops','KGM',15,0,0 UNION ALL
Select '1704902000','White chocolate','KGM',15,0,0 UNION ALL

Select '1704909100','Soft, containing gelatin','KGM',15,0,0 UNION ALL
Select '1704909900','Other','KGM',15,0,0 UNION ALL

Select '1801000000','Cocoa beans, whole or broken, raw or roasted.','KGM',0,0,0 UNION ALL

Select '1802000000','Cocoa shells, husks, skins and other cocoa waste.','KGM',0,0,0 UNION ALL

Select '1803100000','Not defatted','KGM',10,0,0 UNION ALL
Select '1803200000','Wholly or partly defatted','KGM',10,0,0 UNION ALL

Select '1804000000','Cocoa butter, fat and oil.','KGM',10,0,0 UNION ALL

Select '1805000000','Cocoa powder, not containing added sugar or other sweetening matter.','KGM',10,0,0 UNION ALL

Select '1806100000','Cocoa powder, containing added sugar or other sweetening matter','KGM',10,0,0 UNION ALL

Select '1806201000','Chocolate confectionery in blocks, slabs or bars','KGM',15,0,0 UNION ALL
Select '1806209000','Other','KGM',15,0,0 UNION ALL

Select '1806310000','Filled ','KGM',15,0,0 UNION ALL
Select '1806320000','Not filled','KGM',15,0,0 UNION ALL

Select '1806901000','Chocolate confectionery in tablets or pastilles','KGM',15,0,0 UNION ALL
Select '1806903000','Food preparations of flour, meal, starch or malt extract, containing 40% or more but not exceeding 50% by weight of cocoa calculated on a totally defatted basis','KGM',15,0,0 UNION ALL
Select '1806904000','Food preparations of goods of headings 04.01 to 04.04, containing 5% or more but not exceeding 10% by weight of cocoa calculated on a totally defatted basis, specially prepared for infants or young children not put up for retail sale','KGM',15,0,0 UNION ALL
Select '1806909000','Other','KGM',15,0,0 UNION ALL


Select '1901101000','Of malt extract','KGM',0,0,0 UNION ALL
Select '1901102000','Of goods of headings 04.01 to 04.04','KGM',0,0,0 UNION ALL
Select '1901103000','Of soyabean powder','KGM',0,0,0 UNION ALL

Select '1901109100','Medical foods','KGM',0,0,0 UNION ALL
Select '1901109200','Other, for children age over 1 year but not exceeding 3 years','KGM',0,0,0 UNION ALL
Select '1901109900','Other','KGM',6,0,0 UNION ALL

Select '1901201000','Of flour, groats, meal, starch or malt extract, not containing cocoa ','KGM',0,0,0 UNION ALL
Select '1901202000','Of flour, groats, meal, starch or malt extract, containing cocoa ','KGM',0,0,0 UNION ALL
Select '1901203000','Other, not containing cocoa','KGM',0,0,0 UNION ALL
Select '1901204000','Other, containing cocoa ','KGM',0,0,0 UNION ALL


Select '1901901100','Medical foods','KGM',0,0,0 UNION ALL
Select '1901901900','Other','KGM',0,0,0 UNION ALL
Select '1901902000','Malt extract','KGM',0,0,0 UNION ALL

Select '1901903100','Filled milk','KGM',0,0,0 UNION ALL
Select '1901903200','Other, containing cocoa powder','KGM',7,0,0 UNION ALL
Select '1901903900','Other','KGM',0,0,0 UNION ALL

Select '1901904100','In powder form','KGM',6,0,0 UNION ALL
Select '1901904900','In other forms','KGM',6,0,0 UNION ALL

Select '1901909100','Medical foods','KGM',6,0,0 UNION ALL
Select '1901909900','Other','KGM',6,0,0 UNION ALL


Select '1902110000','Containing eggs      ','KGM',5,0,0 UNION ALL

Select '1902192000','Rice vermicelli (including bee hoon)','KGM',6,0,0 UNION ALL

Select '1902193100','Of corn','KGM',6,0,0 UNION ALL
Select '1902193900','Other','KGM',6,0,0 UNION ALL
Select '1902194000','Other noodles','KGM',5,0,0 UNION ALL
Select '1902199000','Other','KGM',6,0,0 UNION ALL

Select '1902201000','Stuffed with meat or meat offal','KGM',0,0,0 UNION ALL
Select '1902203000','Stuffed with fish, crustaceans or molluscs','KGM',8,0,0 UNION ALL
Select '1902209000','Other','KGM',6,0,0 UNION ALL

Select '1902302000','Rice vermicelli (including bee hoon)','KGM',5,0,0 UNION ALL
Select '1902303000','Transparent vermicelli','KGM',6,0,0 UNION ALL
Select '1902304000','Other instant noodles','KGM',8,0,0 UNION ALL
Select '1902309000','Other','KGM',8,0,0 UNION ALL
Select '1902400000','Couscous','KGM',0,0,0 UNION ALL

Select '1903000000','Tapioca and substitutes therefor prepared from starch, in the form of flakes, grains, pearls, siftings or in similar forms.','KGM',5,0,0 UNION ALL


Select '1904101000','Containing cocoa','KGM',5,0,0 UNION ALL
Select '1904109000','Other','KGM',2,0,0 UNION ALL

Select '1904201000','Prepared foods obtained from unroasted cereal flakes','KGM',7,0,0 UNION ALL
Select '1904209000','Other','KGM',7,0,0 UNION ALL
Select '1904300000','Bulgur wheat','KGM',7,0,0 UNION ALL

Select '1904901000','Rice preparations, including precooked rice','KGM',7,0,0 UNION ALL
Select '1904909000','Other','KGM',7,0,0 UNION ALL

Select '1905100000','Crispbread','KGM',0,0,0 UNION ALL
Select '1905200000','Gingerbread and the like','KGM',0,0,0 UNION ALL


Select '1905311000','Not containing cocoa','KGM',6,0,0 UNION ALL
Select '1905312000','Containing cocoa','KGM',6,0,0 UNION ALL

Select '1905321000','Waffles','KGM',6,0,0 UNION ALL
Select '1905322000','Wafers','KGM',6,0,0 UNION ALL

Select '1905401000','Not containing added sugar, honey, eggs, fats, cheese or fruit','KGM',0,0,0 UNION ALL
Select '1905409000','Other','KGM',6,0,0 UNION ALL

Select '1905901000','Unsweetened teething biscuits','KGM',6,0,0 UNION ALL
Select '1905902000','Other unsweetened biscuits','KGM',0,0,0 UNION ALL
Select '1905903000','Cakes','KGM',0,0,0 UNION ALL
Select '1905904000','Pastries ','KGM',0,0,0 UNION ALL
Select '1905905000','Flourless bakers"wares','KGM',0,0,0 UNION ALL
Select '1905906000','Empty cachets and similar products of a kind suitable for pharmaceutical use','KGM',0,0,0 UNION ALL
Select '1905907000','Communion wafers, sealing wafers, rice paper and similar  products','KGM',0,0,0 UNION ALL
Select '1905908000','Other crisp savoury food products','KGM',0,0,0 UNION ALL
Select '1905909000','Other','KGM',0,0,0 UNION ALL

Select '2001100000','Cucumbers and gherkins','KGM',0,0,0 UNION ALL

Select '2001901000','Onions','KGM',0,0,0 UNION ALL
Select '2001909000','Other','KGM',0,0,0 UNION ALL

Select '2002100000','Tomatoes, whole or in pieces','KGM',0,0,0 UNION ALL

Select '2002901000','Tomato paste','KGM',0,0,0 UNION ALL
Select '2002902000','Tomato powder','KGM',0,0,0 UNION ALL
Select '2002909000','Other','KGM',0,0,0 UNION ALL

Select '2003100000','Mushrooms of the genus Agaricus   ','KGM',0,0,0 UNION ALL

Select '2003901000','Truffles','KGM',0,0,0 UNION ALL
Select '2003909000','Other','KGM',0,0,0 UNION ALL

Select '2004100000','Potatoes','KGM',0,0,0 UNION ALL

Select '2004901000','Suitable for infants or young children ','KGM',0,0,0 UNION ALL
Select '2004909000','Other','KGM',0,0,0 UNION ALL


Select '20051010','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '2005101010','Infant and young children food','KGM',0,0,0 UNION ALL
Select '2005101090','Other','KGM',8,0,0 UNION ALL
Select '2005109000','Other','KGM',0,0,0 UNION ALL


Select '2005201100','In airtight containers for retail sale','KGM',8,0,0 UNION ALL
Select '2005201900','Other','KGM',0,0,0 UNION ALL

Select '20052091','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '2005209110','Infant and young children food','KGM',0,0,0 UNION ALL
Select '2005209190','Other','KGM',8,0,0 UNION ALL
Select '2005209900','Other','KGM',0,0,0 UNION ALL
Select '2005400000','Peas (Pisum sativum)','KGM',0,0,0 UNION ALL

Select '2005510000','Beans, shelled','KGM',0,0,0 UNION ALL

Select '20055910','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '2005591010','Infant and young children food','KGM',0,0,0 UNION ALL
Select '2005591090','Other','KGM',8,0,0 UNION ALL
Select '2005599000','Other','KGM',0,0,0 UNION ALL
Select '2005600000','Asparagus','KGM',0,0,0 UNION ALL
Select '2005700000','Olives','KGM',0,0,0 UNION ALL
Select '2005800000','Sweet corn (Zea mays var. saccharata)','KGM',8,0,0 UNION ALL

Select '2005910000','Bamboo shoots','KGM',0,0,0 UNION ALL

Select '20059910','In airtight containers for retail sale:','',0,0,0 UNION ALL
Select '2005991010','Infant and young children food','KGM',0,0,0 UNION ALL
Select '2005991090','Other','KGM',8,0,0 UNION ALL
Select '2005999000','Other','KGM',0,0,0 UNION ALL

Select '2006000000','Vegetables, fruit, nuts, fruitpeel and other parts of plants, preserved by sugar (drained, glace or crystallised).','KGM',0,0,0 UNION ALL

Select '2007100000','Homogenised preparations','KGM',0,0,0 UNION ALL

Select '2007910000','Citrus fruit','KGM',0,0,0 UNION ALL

Select '2007991000','Fruit pastes other than of mangoes, pineapples or strawberries','KGM',0,0,0 UNION ALL
Select '2007992000','Jams and fruit jellies','KGM',0,0,0 UNION ALL
Select '2007999000','Other','KGM',0,0,0 UNION ALL



Select '2008111000','Roasted ','KGM',0,0,0 UNION ALL
Select '2008112000','Peanut butter','KGM',0,0,0 UNION ALL
Select '2008119000','Other','KGM',0,0,0 UNION ALL

Select '2008191000','Cashew nuts','KGM',0,0,0 UNION ALL

Select '2008199100','Roasted','KGM',0,0,0 UNION ALL
Select '2008199900','Other','KGM',0,0,0 UNION ALL

Select '2008201000','In airtight containers for retail sale','KGM',10,0,0 UNION ALL
Select '2008209000','Other ','KGM',10,0,0 UNION ALL

Select '2008301000','Containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008309000','Other','KGM',0,0,0 UNION ALL
Select '2008400000','Pears','KGM',0,0,0 UNION ALL
Select '2008500000','Apricots','KGM',0,0,0 UNION ALL

Select '2008601000','Containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008609000','Other','KGM',0,0,0 UNION ALL

Select '2008701000','Containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008709000','Other','KGM',0,0,0 UNION ALL
Select '2008800000','Strawberries','KGM',0,0,0 UNION ALL

Select '2008910000','Palm hearts','KGM',8,0,0 UNION ALL

Select '2008931000','Containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008939000','Other','KGM',0,0,0 UNION ALL

Select '2008971000','Of stems, roots and other edible parts of plants, not including fruits or nuts whether or not containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008972000','Other, containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008979000','Other','KGM',0,0,0 UNION ALL

Select '2008991000','Lychees','KGM',0,0,0 UNION ALL
Select '2008992000','Longans','KGM',0,0,0 UNION ALL
Select '2008993000','Of stems, roots and other edible parts of plants, not including fruits or nuts whether or not containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008994000','Other, containing added sugar or other sweetening matter or spirit','KGM',0,0,0 UNION ALL
Select '2008999000','Other','KGM',0,0,0 UNION ALL


Select '2009110000','Frozen','KGM',0,0,0 UNION ALL
Select '2009120000','Not frozen, of a Brix value not exceeding 20','KGM',0,0,0 UNION ALL
Select '2009190000','Other','KGM',0,0,0 UNION ALL

Select '2009210000','Of a Brix value not exceeding 20','KGM',0,0,0 UNION ALL
Select '2009290000','Other','KGM',0,0,0 UNION ALL

Select '2009310000','Of a Brix value not exceeding 20','KGM',0,0,0 UNION ALL
Select '2009390000','Other','KGM',0,0,0 UNION ALL

Select '2009410000','Of a Brix value not exceeding 20','KGM',20,0,0 UNION ALL
Select '2009490000','Other ','KGM',20,0,0 UNION ALL
Select '2009500000','Tomato juice','KGM',0,0,0 UNION ALL

Select '2009610000','Of a Brix value not exceeding 30','KGM',0,0,0 UNION ALL
Select '2009690000','Other','KGM',0,0,0 UNION ALL

Select '2009710000','Of a Brix value not exceeding 20','KGM',0,0,0 UNION ALL
Select '2009790000','Other','KGM',0,0,0 UNION ALL


Select '2009811000','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2009819000','Other','KGM',6,0,0 UNION ALL

Select '20098910','Blackcurrant juice:','',0,0,0 UNION ALL
Select '2009891010','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2009891090','Other','KGM',6,0,0 UNION ALL

Select '2009899100','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2009899900','Other','KGM',6,0,0 UNION ALL

Select '2009901000','Suitable for infants or young children','KGM',0,0,0 UNION ALL

Select '2009909100','Ready for immediate consumption','KGM',10,0,0 UNION ALL
Select '2009909900','Other','KGM',5,0,0 UNION ALL



Select '2101111000','Instant coffee','KGM',5,0,0 UNION ALL
Select '2101119000','Other ','KGM',5,0,0 UNION ALL

Select '2101121000','Mixtures in paste form with a basis of ground roasted coffee, containing vegetable fats','KGM',10,0,0 UNION ALL

Select '2101129100','Coffee preparation with a basis of extracts, essences or concentrate containing added sugar,  whether or not containing creamer','KGM',5,0,0 UNION ALL
Select '2101129200','Coffee preparation with a basis of ground roasted coffee containing added sugar,whether or not containing creamer','KGM',5,0,0 UNION ALL
Select '2101129900','Other','KGM',5,0,0 UNION ALL

Select '2101202000','Tea extracts for the manufacture of tea preparations, in powder form','KGM',0,0,0 UNION ALL
Select '2101203000','Preparations of tea consisting of a mixture of tea, milk powder and sugar','KGM',10,0,0 UNION ALL
Select '2101209000','Other','KGM',0,0,0 UNION ALL
Select '2101300000','Roasted chicory and other roasted coffee substitutes, and extracts, essences and concentrates thereof','KGM',0,0,0 UNION ALL

Select '2102100000','Active yeasts','KGM',15,0,0 UNION ALL

Select '2102201000','Of a kind used in animal feeding','KGM',0,0,0 UNION ALL
Select '2102209000','Other','KGM',0,0,0 UNION ALL
Select '2102300000','Prepared baking powders ','KGM',0,0,0 UNION ALL

Select '2103100000','Soya sauce ','KGM',10,0,0 UNION ALL
Select '2103200000','Tomato ketchup and other tomato sauces','KGM',10,0,0 UNION ALL
Select '2103300000','Mustard flour and meal and prepared mustard','KGM',0,0,0 UNION ALL


Select '2103901100','Chilli sauce','KGM',10,0,0 UNION ALL
Select '2103901200','Fish sauce ','KGM',10,0,0 UNION ALL
Select '2103901300','Other sauces','KGM',10,0,0 UNION ALL
Select '2103901900','Other','KGM',5,0,0 UNION ALL

Select '2103902100','Shrimp paste including belachan (blachan)','KGM',5,0,0 UNION ALL
Select '2103902900','Other','KGM',5,0,0 UNION ALL



Select '2104101100','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2104101900','Other','KGM',20,0,0 UNION ALL

Select '2104109100','Suitable for infant or young children','KGM',0,0,0 UNION ALL
Select '2104109900','Other','KGM',20,0,0 UNION ALL


Select '2104201100','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2104201900','Other','KGM',20,0,0 UNION ALL

Select '2104209100','Suitable for infants or young children','KGM',0,0,0 UNION ALL
Select '2104209900','Other','KGM',20,0,0 UNION ALL

Select '2105000000','Ice cream and other edible ice, whether or not containing cocoa.','KGM',5,0,0 UNION ALL

Select '2106100000','Protein concentrates and textured protein substances','KGM',15,0,0 UNION ALL


Select '2106901100','Dried bean curd and dried bean curd stick','KGM',15,0,0 UNION ALL
Select '2106901200','Fresh soybean curd (tofu)','KGM',15,0,0 UNION ALL
Select '2106901900','Other','KGM',15,0,0 UNION ALL
Select '2106902000','Powdered alcohol ','KGM',20,0,0 UNION ALL
Select '2106903000','Nondairy creamer','KGM',15,0,0 UNION ALL

Select '2106904100','In powder form','KGM',20,0,0 UNION ALL
Select '2106904900','Other','KGM',20,0,0 UNION ALL

Select '2106905300','Ginseng based products ','KGM',0,0,0 UNION ALL
Select '2106905400','Other preparations of a kind used as raw material for the manufacture of composite concentrates','KGM',0,0,0 UNION ALL
Select '2106905500','Other, composite concentrates for simple dilution with water to make beverages','KGM',0,0,0 UNION ALL
Select '2106905900','Other  ','KGM',0,0,0 UNION ALL


Select '2106906100','Of a kind used for the manufacture of alcoholic beverages, in liquid form ','KGM',20,0,0 UNION ALL
Select '2106906200','Of a kind used for the manufacture of alcoholic beverages,  in other forms ','KGM',20,0,0 UNION ALL

Select '2106906400','Of a kind used for making  alcoholic beverages, in liquid form ','KGM',20,0,0 UNION ALL
Select '2106906500','Of a kind used for making alcoholic beverages,  in other forms ','KGM',20,0,0 UNION ALL
Select '2106906600','Other, of kind used for the manufacture of alcoholic beverages, in liquid form','KGM',20,0,0 UNION ALL
Select '2106906700','Other, of kind used for the manufacture of alcoholic beverages, in other forms','KGM',20,0,0 UNION ALL
Select '2106906900','Other ','KGM',20,0,0 UNION ALL

Select '2106907100','Food supplements based on ginseng','KGM',0,0,0 UNION ALL
Select '2106907200','Other food supplements','KGM',0,0,0 UNION ALL
Select '2106907300','Fortificant premixes','KGM',15,0,0 UNION ALL

Select '2106908100','Food preparations for lactase deficient infants or young children','KGM',0,0,0 UNION ALL
Select '2106908900','Other','KGM',0,0,0 UNION ALL

Select '2106909100','Other, mixtures of chemicals with foodstuffs or other substances with nutritive value, of a kind used for food processing','KGM',15,0,0 UNION ALL
Select '2106909200','Flavoured or coloured syrups','KGM',15,0,0 UNION ALL
Select '2106909500','Seri kaya','KGM',15,0,0 UNION ALL
Select '2106909600','Other medical foods','KGM',15,0,0 UNION ALL
Select '2106909700','Tempeh','KGM',15,0,0 UNION ALL
Select '2106909800','Other flavouring preparations','KGM',15,0,0 UNION ALL
Select '2106909900','Other','KGM',15,0,0 UNION ALL


Select '2201101000','Mineral waters','LTR',20,0,0 UNION ALL
Select '2201102000','Aerated waters','LTR',20,0,0 UNION ALL

Select '2201901000','Ice and snow ','LTR',0,0,0 UNION ALL
Select '2201909000','Other ','LTR',5,0,0 UNION ALL


Select '2202101000','Sparkling mineral waters or aerated waters, flavoured','LTR',20,0,0 UNION ALL
Select '2202109000','Other ','LTR',20,0,0 UNION ALL

Select '2202910000','Nonalcoholic beer','LTR',20,0,0 UNION ALL

Select '2202991000','Flavoured UHT milk based drinks','LTR',20,0,0 UNION ALL
Select '2202992000','Soya milk drinks ','LTR',20,0,0 UNION ALL
Select '2202994000','Coffee based drinks or coffee flavoured drinks ','LTR',20,0,0 UNION ALL
Select '2202995000','Other nonaerated beverages ready for immediate consumption without dilution','LTR',20,0,0 UNION ALL
Select '2202999000','Other ','LTR',20,0,0 UNION ALL



Select '2203001100','Of an alcoholic strength by volume not exceeding 5.8% vol.','LTR',5.00,0,175.00 UNION ALL
Select '2203001900','Other ','LTR',5.00,0,175.00 UNION ALL

Select '2203009100','Of an alcoholic strength by volume not exceeding 5.8% vol.','LTR',5.00,0,175.00 UNION ALL
Select '2203009900','Other ','LTR',5.00,0,175.00 UNION ALL

Select '2204100000','Sparkling wine','LTR', 23.00,0,450.00 UNION ALL



Select '2204211100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204211300','Of an alcoholic strength by volume exceeding 15% vol. but not exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204211400','Of an alcoholic strength by volume exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL

Select '2204212100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204212200','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL


Select '2204221100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204221200','Of an alcoholic strength by volume exceeding 15% vol. but not exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204221300','Of an alcoholic strength by volume exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL

Select '2204222100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204222200','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL


Select '2204291100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204291300','Of an alcoholic strength by volume exceeding 15% vol. but not exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204291400','Of an alcoholic strength by volume exceeding 23% vol.','LTR', 7.00,0,150.00 UNION ALL

Select '2204292100','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204292200','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL

Select '2204301000','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2204302000','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL


Select '2205101000','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2205102000','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL

Select '2205901000','Of an alcoholic strength by volume not exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL
Select '2205902000','Of an alcoholic strength by volume exceeding 15% vol.','LTR', 7.00,0,150.00 UNION ALL


Select '2206001000','Cider and perry','LTR', 7.00,0,60.00 UNION ALL
Select '2206002000','Sake','LTR', 25.50,0,60.00 UNION ALL

Select '2206003100','In container holding 2 l or less','LTR', 4.00,0,40.00 UNION ALL
Select '2206003900','Other','LTR', 108.50,0,40.00 UNION ALL

Select '2206004100','Of an alcoholic strength by volume not exceeding 1.14% vol. ','LTR', 3.00,0,60.00 UNION ALL
Select '2206004900','Other','LTR', 64.50,0,60.00 UNION ALL

Select '2206009100','Other rice wine (including medicated rice wine)','LTR', 25.50,0,60.00 UNION ALL
Select '22060099','Other:','',0,0,0 UNION ALL
Select '2206009910','Mead','LTR', 23.00,0,40.00 UNION ALL
Select '2206009920','Wines obtained by the fermentation of fruit juices, other than   juice of fresh grapes (fig, date or berry wines) or of vegetables  juices','LTR', 108.50,0,60.00 UNION ALL
Select '2206009990','Other','LTR', 108.50 ,0,40.00 UNION ALL

Select '2207100000','Undenatured ethyl alcohol of an alcoholic strength by volume of 80% vol. or higher','LTR', 60.00,0, 22.50 UNION ALL


Select '2207201100','Ethyl alcohol  of an alcoholic strength by volume of  exceeding 99% vol.          ','LTR', 1.00,0, 1.10 UNION ALL
Select '2207201900','Other ','LTR', 1.00,0, 1.10 UNION ALL
Select '2207209000','Other ','LTR', 1.00,0, 1.10 UNION ALL


Select '2208205000','Brandy','LTR', 58.00,0,150.00 UNION ALL
Select '2208209000','Other','LTR', 58.00,0,150.00 UNION ALL
Select '2208300000','Whiskies','LTR', 58.00,0,150.00 UNION ALL
Select '2208400000','Rum and other spirits obtained by distilling fermented sugarcane products','LTR', 55.00,0,150.00 UNION ALL
Select '2208500000','Gin and Geneva','LTR', 55.00,0,150.00 UNION ALL
Select '2208600000','Vodka','LTR', 55.00,0,150.00 UNION ALL

Select '2208701000','Of an alcoholic strength by volume not exceeding 57% vol.','LTR', 93.50,0,60.00 UNION ALL
Select '2208709000','Other','LTR', 64.50,0,60.00 UNION ALL

Select '2208901000','Medicated samsu of an alcoholic strength by volume not  exceeding 40% vol.','LTR', 26.50 ,0,60.00 UNION ALL
Select '2208902000','Medicated samsu of an alcoholic strength by volume exceeding 40 % vol.   ','LTR', 26.50,0,60.00 UNION ALL
Select '2208903000','Other samsu of an alcoholic strength by volume not exceeding 40% vol.','LTR', 26.50,0,60.00 UNION ALL
Select '2208904000','Other samsu of an alcoholic strength by volume exceeding 40%  vol.','LTR', 26.50,0,60.00 UNION ALL
Select '2208905000','Arrack  or pineapple spirit of an alcoholic strength by volume not exceeding 40% vol.','LTR', 20.00,0,60.00 UNION ALL
Select '2208906000','Arrack or pineapple spirit of an alcoholic strength by volume   exceeding 40 % vol.','LTR', 20.00,0,60.00 UNION ALL
Select '2208907000','Bitters and similar beverages of an alcoholic strength not exceeding 57% vol.','LTR', 30.00,0,40.00 UNION ALL
Select '2208908000','Bitters and similar beverages of an alcoholic strength exceeding 57% vol.','LTR', 30.00,0,40.00 UNION ALL

Select '2208909100','Of an alcoholic strength by volume not exceeding 1.14% vol. ','LTR', 3.00,0,60.00 UNION ALL
Select '2208909900','Other','LTR', 64.50,0,60.00 UNION ALL

Select '2209000000','Vinegar and substitutes for vinegar obtained from acetic acid.','LTR',5,0,0 UNION ALL

Select '2301100000','Flours, meals and pellets, of meat or meat offal; greaves','KGM',0,0,0 UNION ALL

Select '2301201000','Of fish, with a protein content of less than 60% by weight','KGM',0,0,0 UNION ALL
Select '2301202000','Of fish, with a protein content of 60% or more by weight','KGM',0,0,0 UNION ALL
Select '2301209000','Other','KGM',0,0,0 UNION ALL

Select '2302100000','Of maize (corn)','KGM',0,0,0 UNION ALL

Select '2302301000','Bran and pollard','KGM',0,0,0 UNION ALL
Select '2302309000','Other','KGM',0,0,0 UNION ALL

Select '2302401000','Of rice','KGM',0,0,0 UNION ALL
Select '2302409000','Other','KGM',0,0,0 UNION ALL
Select '2302500000','Of leguminous plants ','KGM',0,0,0 UNION ALL


Select '2303101000','Of manioc (cassava) or sago ','KGM',0,0,0 UNION ALL
Select '2303109000','Other','KGM',0,0,0 UNION ALL
Select '2303200000','Beetpulp, bagasse and other waste of sugar manufacture ','KGM',0,0,0 UNION ALL
Select '2303300000','Brewing or distilling dregs and waste ','KGM',0,0,0 UNION ALL

Select '2304001000','Defatted soyabean flour, fit for human consumption','KGM',0,0,0 UNION ALL
Select '2304009000','Other','KGM',0,0,0 UNION ALL

Select '2305000000','Oilcake and other solid residues, whether or not ground or in the form of pellets, resulting from the extraction of groundnut oil.','KGM',0,0,0 UNION ALL

Select '2306100000','Of cotton seeds','KGM',0,0,0 UNION ALL
Select '2306200000','Of linseed','KGM',0,0,0 UNION ALL
Select '2306300000','Of sunflower seeds','KGM',0,0,0 UNION ALL


Select '2306411000','Of low erucic acid rape seeds','KGM',0,0,0 UNION ALL
Select '2306412000','Of low erucic acid colza seeds','KGM',0,0,0 UNION ALL

Select '2306491000','Of other rape seeds','KGM',0,0,0 UNION ALL
Select '2306492000','Of other colza seeds','KGM',0,0,0 UNION ALL
Select '2306500000','Of coconut or copra ','KGM',0,0,0 UNION ALL

Select '2306601000','Ground or in the form of pellets','TNE',0,0,0 UNION ALL
Select '2306609000','Other','TNE',0,0,0 UNION ALL

Select '2306901000','Of maize (corn) germ','KGM',0,0,0 UNION ALL
Select '2306909000','Other','KGM',0,0,0 UNION ALL

Select '2307000000','Wine lees; argol.','KGM',0,0,0 UNION ALL

Select '2308000000','Vegetable materials and vegetable waste, vegetable residues and byproducts, whether or not in the form of pellets, of a kind used in animal feeding, not elsewhere specified or included.','KGM',0,0,0 UNION ALL


Select '2309101000','Containing meat','KGM',0,0,0 UNION ALL
Select '2309109000','Other','KGM',0,0,0 UNION ALL


Select '2309901100','Of a kind suitable for poultry','KGM',0,0,0 UNION ALL
Select '2309901200','Of a kind suitable for swine','KGM',0,0,0 UNION ALL
Select '2309901300','Of a kind suitable for prawns','KGM',0,0,0 UNION ALL
Select '2309901400',' Of a kind suitable for primates','KGM',0,0,0 UNION ALL
Select '2309901900','Other ','KGM',0,0,0 UNION ALL
Select '2309902000','Premixes, feed supplements or feed additives','KGM',0,0,0 UNION ALL
Select '2309909000','Other','KGM',0,0,0 UNION ALL


Select '2401101000','Virginia type, fluecured ','KGM',40.00,0,0 UNION ALL
Select '2401102000','Virginia type, other than fluecured ','KGM',40.00,0,0 UNION ALL
Select '2401104000',' Burley type','KGM',40.00,0,0 UNION ALL
Select '2401105000','Other, fluecured ','KGM',40.00,0,0 UNION ALL
Select '2401109000','Other','KGM',40.00,0,0 UNION ALL

Select '2401201000','Virginia type, fluecured','KGM',40.00,0,0 UNION ALL
Select '2401202000','Virginia type, other than fluecured ','KGM',40.00,0,0 UNION ALL
Select '2401203000','Oriental type ','KGM',40.00,0,0 UNION ALL
Select '2401204000','Burley type ','KGM',40.00,0,0 UNION ALL
Select '2401205000','Other, fluecured ','KGM',40.00,0,0 UNION ALL
Select '2401209000','Other','KGM',40.00,0,0 UNION ALL

Select '2401301000','Tobacco stems ','KGM',40.00,0,0 UNION ALL
Select '2401309000','Other ','KGM',40.00,0,0 UNION ALL

Select '2402100000','Cigars, cheroots and cigarillos, containing tobacco','KGM',200.00,0,400.00  UNION ALL

Select '2402201000','Beedies','KGM',14.50,0,7.50  UNION ALL
Select '2402202000','Clove cigarettes','KGM',0.20,0,0.40  UNION ALL
Select '2402209000','Other','KGM',0.20,0,0.40  UNION ALL

Select '2402901000','Cigars, cheroots and cigarillos of tobacco substitutes ','KGM',200.00,0,400.00  UNION ALL
Select '2402902000','Cigarettes of tobacco substitutes ','KGM',0.20,0,0.40  UNION ALL


Select '24031100','Water pipe tobacco specified in Subheading Note 1 to this Chapter:','',0,0,0 UNION ALL
Select '2403110010','Packed for retail sale','KGM',40.00,0,27.00  UNION ALL
Select '2403110090','Other','KGM',40.00,0,15.00 UNION ALL


Select '2403191100','Ang Hoon','KGM',40.00,0,27.00  UNION ALL
Select '2403191900','Other','KGM',40.00,0,27.00  UNION ALL
Select '24031920','Other manufactured tobacco for the manufacture of cigarettes:','',0,0,0 UNION ALL
Select '2403192010','Cutrags','KGM',70.00,0,0 UNION ALL
Select '2403192090','Other','KGM',40.00,0,15.00 UNION ALL

Select '2403199100','Ang Hoon','KGM',40.00,0,15.00 UNION ALL
Select '2403199900','Other','KGM',40.00,0,15.00 UNION ALL


Select '2403911000','Packed for retail sale','KGM',40.00,0,27.00  UNION ALL
Select '2403919000','Other','KGM',50.00,0,0 UNION ALL

Select '2403991000','Tobacco extracts and essences ','KGM',50.00,0,0 UNION ALL
Select '2403993000','Manufactured tobacco substitutes','KGM',50.00,0,0 UNION ALL
Select '2403994000','Snuff, whether or not dry ','KGM',40.00,0,27.00  UNION ALL
Select '2403995000','Chewing and sucking tobacco','KGM',50.00,0,0 UNION ALL
Select '2403999000','Other','KGM',50.00,0,0 UNION ALL

Select '2501001000','Table salt','KGM',0,0,0 UNION ALL
Select '2501002000','Unprocessed rock salt ','KGM',0,0,0 UNION ALL
Select '2501005000','Sea water','KGM',0,0,0 UNION ALL

Select '2501009100','With sodium chloride content more than 60% but less than 97%, calculated on a dry basis, fortified with iodine ','KGM',0,0,0 UNION ALL
Select '2501009200','Other with sodium chloride content more than 97% but less than 99.9%, calculated on a dry basis','KGM',0,0,0 UNION ALL
Select '2501009900','Other ','KGM',0,0,0 UNION ALL

Select '2502000000','Unroasted iron pyrites.','KGM',0,0,0 UNION ALL

Select '2503000000','Sulphur of all kinds, other than sublimed sulphur, precipitated sulphur and colloidal sulphur.','KGM',0,0,0 UNION ALL

Select '2504100000','In powder or in flakes ','KGM',0,0,0 UNION ALL
Select '2504900000','Other ','KGM',0,0,0 UNION ALL

Select '25051000','Silica sands and quartz sands:','',0,0,0 UNION ALL
Select '2505100010','Silica sands','KGM',0,0,0 UNION ALL
Select '2505100020','Quartz sands','KGM',0,0,0 UNION ALL
Select '2505900000','Other','KGM',0,0,0 UNION ALL

Select '2506100000','Quartz ','KGM',0,0,0 UNION ALL
Select '2506200000','Quartzite','KGM',0,0,0 UNION ALL

Select '2507000000','Kaolin and other kaolinic clays, whether or not calcined.','KGM',0,0,0 UNION ALL

Select '2508100000','Bentonite','KGM',0,0,0 UNION ALL
Select '2508300000','Fireclay','KGM',0,0,0 UNION ALL

Select '2508401000','Fuller''s earth ','KGM',30,0,0 UNION ALL
Select '2508409000','Other','KGM',0,0,0 UNION ALL
Select '2508500000','Andalusite, kyanite and sillimanite ','KGM',0,0,0 UNION ALL
Select '2508600000','Mullite ','KGM',0,0,0 UNION ALL
Select '2508700000','Chamotte or dinas earths','KGM',0,0,0 UNION ALL

Select '2509000000','Chalk.','KGM',0,0,0 UNION ALL


Select '2510101000','Apatite ','KGM',0,0,0 UNION ALL
Select '2510109000','Other ','KGM',0,0,0 UNION ALL

Select '2510201000','Apatite ','KGM',0,0,0 UNION ALL
Select '2510209000','Other ','KGM',0,0,0 UNION ALL

Select '2511100000','Natural barium sulphate (barytes) ','KGM',0,0,0 UNION ALL
Select '2511200000','Natural barium carbonate (witherite) ','KGM',0,0,0 UNION ALL

Select '2512000000','Siliceous fossil meals (for example, kieselguhr, tripolite and diatomite) and similar siliceous earths, whether or not calcined, of an apparent specific gravity of 1 or less.','KGM',0,0,0 UNION ALL

Select '2513100000','Pumice stone','KGM',0,0,0 UNION ALL
Select '2513200000','Emery, natural corundum, natural garnet and other natural abrasives','KGM',0,0,0 UNION ALL

Select '2514000000','Slate, whether or not roughly trimmed or merely cut, by sawing or otherwise, into blocks or slabs of a rectangular (including square) shape.','KGM',0,0,0 UNION ALL


Select '2515110000','Crude or roughly trimmed','KGM',0,0,0 UNION ALL

Select '2515121000','Blocks','KGM',15,0,0 UNION ALL
Select '2515122000','Slabs ','KGM',15,0,0 UNION ALL
Select '2515200000','Ecaussine and other calcareous monumental or building stone; alabaster','KGM',15,0,0 UNION ALL


Select '2516110000','Crude or roughly trimmed ','KGM',0,0,0 UNION ALL

Select '2516121000','Blocks ','KGM',5,0,0 UNION ALL
Select '2516122000','Slabs ','KGM',5,0,0 UNION ALL

Select '2516201000','Crude or roughly trimmed ','KGM',0,0,0 UNION ALL
Select '2516202000','Merely cut, by sawing or otherwise, into blocks or slabs of a rectangular (including square) shape','KGM',5,0,0 UNION ALL
Select '2516900000','Other monumental or building stone','KGM',5,0,0 UNION ALL

Select '2517100000','Pebbles, gravel, broken or crushed stone, of a kind commonly used for concrete aggregates, for road metalling or for railway or other ballast, shingle and flint, whether or not heattreated','KGM',0,0,0 UNION ALL
Select '2517200000','Macadam of slag, dross or similar industrial waste, whether or not incorporating the materials cited in subheading 2517.10','KGM',0,0,0 UNION ALL
Select '2517300000','Tarred macadam','KGM',0,0,0 UNION ALL

Select '2517410000','Of marble ','KGM',0,0,0 UNION ALL
Select '2517490000','Other','KGM',0,0,0 UNION ALL

Select '2518100000','Dolomite, not calcined or sintered ','KGM',5,0,0 UNION ALL
Select '2518200000','Calcined or sintered dolomite ','KGM',5,0,0 UNION ALL
Select '2518300000','Dolomite ramming mix ','KGM',5,0,0 UNION ALL

Select '2519100000','Natural magnesium carbonate (magnesite)','KGM',0,0,0 UNION ALL

Select '2519901000','Fused magnesia; deadburned (sintered) magnesia','KGM',0,0,0 UNION ALL
Select '2519909000','Other','KGM',0,0,0 UNION ALL

Select '2520100000','Gypsum; anhydrite ','KGM',0,0,0 UNION ALL

Select '2520201000','Of a kind suitable for use in dentistry ','KGM',0,0,0 UNION ALL
Select '2520209000','Other ','KGM',20,0,0 UNION ALL

Select '2521000000','Limestone flux; limestone and other calcareous stone, of a kind used for the manufacture of lime or cement.','KGM',5,0,0 UNION ALL

Select '2522100000','Quicklime ','KGM',10,0,0 UNION ALL
Select '2522200000','Slaked lime ','KGM',10,0,0 UNION ALL
Select '2522300000','Hydraulic lime ','KGM',10,0,0 UNION ALL


Select '2523101000','Of a kind used in the manufacture of white cement ','TNE',0,0,0 UNION ALL
Select '2523109000','Other ','TNE',0,0,0 UNION ALL

Select '2523210000','White cement, whether or not artificially coloured','TNE',25,0,0 UNION ALL

Select '2523291000','Coloured cement ','TNE',5,0,0 UNION ALL
Select '2523299000','Other ','TNE',50,0,0 UNION ALL
Select '2523300000','Aluminous cement','TNE',0,0,0 UNION ALL
Select '2523900000','Other hydraulic cements','TNE',25,0,0 UNION ALL

Select '2524100000','Crocidolite','KGM',0,0,0 UNION ALL
Select '2524900000','Other','KGM',0,0,0 UNION ALL

Select '2525100000','Crude mica and mica rifted into sheets or splittings ','KGM',0,0,0 UNION ALL
Select '2525200000','Mica powder ','KGM',0,0,0 UNION ALL
Select '2525300000','Mica waste','KGM',0,0,0 UNION ALL

Select '2526100000','Not crushed, not powdered','KGM',0,0,0 UNION ALL

Select '2526201000','Talc powder ','KGM',0,0,0 UNION ALL
Select '2526209000','Other','KGM',0,0,0 UNION ALL

Select '2528000000','Natural borates and concentrates thereof (whether or not calcined), but not including borates separated from natural brine; natural boric acid containing not more than 85% of H3B03 calculated on the dry weight.','KGM',0,0,0 UNION ALL


Select '2529101000','Potash feldspar; soda feldspar','KGM',0,0,0 UNION ALL
Select '2529109000','Other','KGM',0,0,0 UNION ALL

Select '2529210000','Containing by weight 97% or less of calcium fluoride','KGM',0,0,0 UNION ALL
Select '2529220000','Containing by weight more than 97% of calcium fluoride ','KGM',0,0,0 UNION ALL
Select '2529300000','Leucite, nepheline and nepheline syenite ','KGM',0,0,0 UNION ALL

Select '2530100000','Vermiculite, perlite and chlorites, unexpanded','KGM',0,0,0 UNION ALL

Select '2530201000','Kieserite ','KGM',0,0,0 UNION ALL
Select '2530202000','Epsomite (natural magnesium sulphates)','KGM',0,0,0 UNION ALL

Select '2530901000','Micronized zircon sand (zirconium silicate) of a kind used as an opacifier

','KGM',0,0,0 UNION ALL
Select '2530909000','Other','KGM',0,0,0 UNION ALL



Select '2601111000','Haematite and concentrate','KGM',0,0,0 UNION ALL
Select '2601119000','Other','KGM',0,0,0 UNION ALL

Select '2601121000','Haematite and concentrates','KGM',0,0,0 UNION ALL
Select '2601129000','Other','KGM',0,0,0 UNION ALL
Select '2601200000','Roasted iron pyrites','KGM',0,0,0 UNION ALL

Select '2602000000','Manganese ores and concentrates, including ferruginous manganese ores and concentrates with a manganese content of 20% or more, calculated on the dry weight.','KGM',0,0,0 UNION ALL

Select '2603000000','Copper ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2604000000','Nickel ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2605000000','Cobalt ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2606000000','Aluminium ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2607000000','Lead ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2608000000','Zinc ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2609000000','Tin ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2610000000','Chromium ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2611000000','Tungsten ores and concentrates.','KGM',0,0,0 UNION ALL

Select '2612100000','Uranium ores and concentrates','KGM',0,0,0 UNION ALL
Select '2612200000','Thorium ores and concentrates','KGM',0,0,0 UNION ALL

Select '2613100000','Roasted','KGM',0,0,0 UNION ALL
Select '2613900000','Other','KGM',0,0,0 UNION ALL


Select '2614001000','Ilmenite ores and concentrates ','KGM',0,0,0 UNION ALL
Select '2614009000','Other ','KGM',0,0,0 UNION ALL

Select '2615100000','Zirconium ores and concentrates','KGM',0,0,0 UNION ALL
Select '2615900000','Other','KGM',0,0,0 UNION ALL

Select '2616100000','Silver ores and concentrates','KGM',0,0,0 UNION ALL
Select '2616900000','Other','KGM',0,0,0 UNION ALL

Select '2617100000','Antimony ores and concentrates','KGM',0,0,0 UNION ALL
Select '2617900000','Other','KGM',0,0,0 UNION ALL

Select '2618000000','Granulated slag (slag sand) from the manufacture of iron or steel.','TNE',0,0,0 UNION ALL

Select '2619000000','Slag, dross (other than granulated slag), scalings and other waste from the manufacture of iron or steel.','KGM',0,0,0 UNION ALL


Select '2620110000','Hard zinc spelter ','KGM',0,5,0 UNION ALL
Select '2620190000','Other ','KGM',0,5,0 UNION ALL

Select '2620210000','Leaded gasoline sludges and leaded antiknock compound  sludges ','KGM',0,5,0 UNION ALL
Select '2620290000','Other ','KGM',0,5,0 UNION ALL
Select '2620300000','Containing mainly copper ','KGM',0,5,0 UNION ALL
Select '2620400000','Containing mainly aluminium ','KGM',0,5,0 UNION ALL
Select '2620600000','Containing arsenic, mercury, thallium or their mixtures, of a kind used for the extraction of arsenic or those metals or for the manufacture of their chemical compounds','KGM',0,5,0 UNION ALL

Select '2620910000','Containing antimony, beryllium, cadmium, chromium or their mixtures ','KGM',0,5,0 UNION ALL

Select '2620991000','Slag and hardhead of tin','KGM',0,0,0 UNION ALL
Select '2620999000','Other','KGM',0,5,0 UNION ALL

Select '2621100000','Ash and residues from the incineration of municipal waste ','KGM',0,5,0 UNION ALL
Select '26219000','Other:','',0,0,0 UNION ALL
Select '2621900010','Crude potassium salts obtained in the sugar industry from residues of beet molasses','KGM',0,0,0 UNION ALL
Select '2621900090','Other','KGM',0,5,0 UNION ALL


Select '2701110000','Anthracite','KGM',0,0,0 UNION ALL

Select '2701121000','Coking coal ','KGM',0,0,0 UNION ALL
Select '2701129000','Other ','KGM',0,0,0 UNION ALL
Select '2701190000','Other coal','KGM',0,0,0 UNION ALL
Select '2701200000','Briquettes, ovoids and similar solid fuels manufactured from coal','KGM',0,0,0 UNION ALL

Select '2702100000','Lignite, whether or not pulverised, but not agglomerated','KGM',0,0,0 UNION ALL
Select '2702200000','Agglomerated lignite','KGM',0,0,0 UNION ALL


Select '2703001000','Peat, whether or not compressed into bales, but not agglomerated','KGM',0,0,0 UNION ALL
Select '2703002000','Agglomerated peat','KGM',0,0,0 UNION ALL


Select '2704001000','Coke and semicoke of coal ','KGM',0,0,0 UNION ALL
Select '2704002000','Coke and semicoke of lignite or of peat ','KGM',0,0,0 UNION ALL
Select '2704003000','Retort carbon','KGM',0,0,0 UNION ALL

Select '2705000000','Coal gas, water gas, producer gas and similar gases, other than petroleum gases and other gaseous hydrocarbons. ','KGM',0,0,0 UNION ALL

Select '2706000000','Tar distilled from coal, from lignite or from peat, and other mineral tars, whether or not dehydrated or partially distilled, including reconstituted tars.','KGM',0,0,0 UNION ALL

Select '2707100000','Benzol (benzene) ','KGM',0,0,0 UNION ALL
Select '2707200000','Toluol (toluene) ','KGM',0,0,0 UNION ALL
Select '2707300000','Xylol (xylenes) ','KGM',0,0,0 UNION ALL
Select '2707400000','Naphthalene','KGM',0,0,0 UNION ALL
Select '2707500000','Other aromatic hydrocarbon mixtures of which 65 % or more by volume (including losses) distils at 250 �C by the ISO 3405 method (equivalent to the ASTM D 86 method)','KGM',0,0,0 UNION ALL

Select '2707910000','Creosote oils','KGM',0,0,0 UNION ALL

Select '2707991000','Carbon black feedstock','KGM',0,0,0 UNION ALL
Select '2707999000','Other','KGM',0,0,0 UNION ALL

Select '2708100000','Pitch','KGM',0,0,0 UNION ALL
Select '2708200000','Pitch coke','KGM',0,0,0 UNION ALL


Select '2709001000','Crude petroleum oils ','KGM',0,10,0 UNION ALL
Select '2709002000','Condensates ','KGM',5,10,0 UNION ALL
Select '2709009000','Other ','KGM',5,10,0 UNION ALL




Select '2710121100','Of RON 97 and above','KGM',0,0,0 UNION ALL
Select '2710121200','Of RON 90 and above but below RON 97','KGM',0,0,0 UNION ALL
Select '2710121300','Of other RON','KGM',0,0,0 UNION ALL


Select '2710122100','Unblended','KGM',0,0,0 UNION ALL
Select '2710122200','Blended with ethanol','KGM',0,0,0 UNION ALL
Select '2710122300','Other','KGM',0,0,0 UNION ALL

Select '2710122400','Unblended','KGM',0,0,0 UNION ALL
Select '2710122500','Blended with ethanol','KGM',0,0,0 UNION ALL
Select '2710122600','Other','KGM',0,0,0 UNION ALL

Select '2710122700','Unblended','KGM',0,0,0 UNION ALL
Select '2710122800','Blended with ethanol','KGM',0,0,0 UNION ALL
Select '2710122900','Other','KGM',0,0,0 UNION ALL

Select '2710123100','100 octane and above','KGM',0,0,0 UNION ALL
Select '2710123900','Other','KGM',0,0,0 UNION ALL
Select '2710124000','Tetrapropylene','KGM',0,0,0 UNION ALL
Select '2710125000','White spirit','KGM',0,0,0 UNION ALL
Select '2710126000','Low aromatic solvents containing by weight less than 1% aromatic content
','KGM',0,0,0 UNION ALL
Select '2710127000','Other solvent spirits','KGM',0,0,0 UNION ALL
Select '2710128000','Naphtha, reformates and other preparations of a kind used for blending into motor spirits','KGM',0,0,0 UNION ALL

Select '2710129100','Alpha olefins

','KGM',0,0,0 UNION ALL
Select '2710129200','Other, petroleum spirit, having a flashpoint of less than 23 oC','KGM',0,0,0 UNION ALL
Select '2710129900','Other','KGM',0,0,0 UNION ALL

Select '2710192000','Topped crudes','KGM',0,0,0 UNION ALL
Select '2710193000','Carbon black feedstock ','KGM',0,0,0 UNION ALL

Select '2710194100','Lubricating oil feedstock','KGM',0,0,0 UNION ALL
Select '2710194200','Lubricating oils for aircraft engines','KGM',0,0,0 UNION ALL
Select '2710194300','Other lubricating oils','KGM',0,0,0 UNION ALL
Select '2710194400','Lubricating greases','KGM',0,0,0 UNION ALL
Select '2710195000','Hydraulic brake fluid','KGM',0,0,0 UNION ALL
Select '2710196000','Transformer and circuit breakers oils','KGM',0,0,0 UNION ALL

Select '2710197100','Automotive diesel fuel','KGM',0,0,0 UNION ALL
Select '2710197200','Other diesel fuels','KGM',0,0,0 UNION ALL
Select '2710197900','Fuel oils','KGM',0,0,0 UNION ALL
Select '2710198100','Aviation turbine fuel (jet fuel) having a flash point of 23 oC or more','KGM',0,0,0 UNION ALL
Select '2710198200','Aviation turbine fuel (jet fuel) having a flash point of less than 23 oC','KGM',0,0,0 UNION ALL
Select '2710198300','Other kerosene','KGM',0,0,0 UNION ALL
Select '2710198900','Other medium oils and preparations','KGM',5,0,0 UNION ALL
Select '2710199000','Other','KGM',0,0,0 UNION ALL
Select '2710200000','Petroleum oils and oils obtained from bituminous minerals (other than crude) and preparations not elsewhere specified or included, containing by weight 70 % or more of petroleum oils or of oils obtained from bituminous minerals, these oils being the basic constituents of the preparations, containing biodiesel, other than waste oils','KGM',0,0,0 UNION ALL

Select '2710910000','Containing polychlorinated biphenyls (PCBs), polychlorinated terphenyls (PCTs) or polybrominated biphenyls (PBBs) ','KGM',0,0,0 UNION ALL
Select '2710990000','Other ','KGM',0,0,0 UNION ALL


Select '2711110000','Natural gas','KGM',0,0,0 UNION ALL
Select '2711120000','Propane','KGM',0,0,0 UNION ALL
Select '2711130000','Butanes','KGM',0,0,0 UNION ALL

Select '2711141000','Ethylene ','KGM',0,0,0 UNION ALL
Select '2711149000','Other ','KGM',0,0,0 UNION ALL
Select '2711190000','Other ','KGM',0,0,0 UNION ALL


Select '2711211000','Of a kind used as a motor fuel','KGM',0,0,0 UNION ALL
Select '2711219000','Other','KGM',0,0,0 UNION ALL
Select '2711290000','Other','KGM',0,0,0 UNION ALL

Select '2712100000','Petroleum jelly','KGM',0,0,0 UNION ALL
Select '2712200000','Paraffin wax containing by weight less than 0.75% of oil','KGM',0,0,0 UNION ALL

Select '2712901000','Paraffin wax ','KGM',0,0,0 UNION ALL
Select '2712909000','Other','KGM',0,0,0 UNION ALL


Select '2713110000','Not calcined','KGM',0,0,0 UNION ALL
Select '2713120000','Calcined','KGM',0,0,0 UNION ALL
Select '2713200000','Petroleum bitumen','KGM',0,0,0 UNION ALL
Select '2713900000','Other residues of petroleum oils or of oils obtained from bituminous minerals','KGM',0,0,0 UNION ALL

Select '2714100000','Bituminous or oil shale and tar sands','KGM',5,0,0 UNION ALL
Select '2714900000','Other','KGM',5,0,0 UNION ALL

Select '2715001000','Polyurethane tar coatings','KGM',25,0,0 UNION ALL
Select '2715009000','Other','KGM',5,0,0 UNION ALL

Select '2716000000','Electrical energy. ','KWH',0,0,0 UNION ALL


Select '2801100000','Chlorine','KGM',10,0,0 UNION ALL
Select '2801200000','Iodine','KGM',0,0,0 UNION ALL
Select '2801300000','Fluorine; bromine','KGM',0,0,0 UNION ALL

Select '2802000000','Sulphur, sublimed or precipitated; colloidal sulphur.','KGM',0,0,0 UNION ALL


Select '2803002000','Acetylene black ','KGM',0,0,0 UNION ALL

Select '2803004100','Of a kind used for rubber processing','KGM',0,0,0 UNION ALL
Select '2803004900','Other','KGM',0,0,0 UNION ALL
Select '2803009000','Other ','KGM',0,0,0 UNION ALL

Select '2804100000','Hydrogen','M3',5,0,0 UNION ALL

Select '2804210000','Argon','M3',5,0,0 UNION ALL
Select '2804290000','Other','M3',5,0,0 UNION ALL
Select '2804300000','Nitrogen','M3',5,0,0 UNION ALL
Select '2804400000','Oxygen','M3',5,0,0 UNION ALL
Select '2804500000','Boron; tellurium','KGM',0,0,0 UNION ALL

Select '2804610000','Containing by weight not less than 99.99% of silicon','KGM',0,0,0 UNION ALL
Select '2804690000','Other','KGM',0,0,0 UNION ALL
Select '2804700000','Phosphorus','KGM',0,0,0 UNION ALL
Select '2804800000','Arsenic','KGM',0,0,0 UNION ALL
Select '2804900000','Selenium','KGM',0,0,0 UNION ALL


Select '2805110000','Sodium ','KGM',0,0,0 UNION ALL
Select '2805120000','Calcium ','KGM',0,0,0 UNION ALL
Select '2805190000','Other ','KGM',0,0,0 UNION ALL
Select '2805300000','Rareearth metals, scandium and yttrium, whether or not  intermixed or interalloyed','KGM',0,0,0 UNION ALL
Select '2805400000','Mercury','KGM',0,0,0 UNION ALL


Select '2806100000','Hydrogen chloride (hydrochloric acid)','KGM',10,0,0 UNION ALL
Select '2806200000','Chlorosulphuric acid','KGM',20,0,0 UNION ALL

Select '2807000000','Sulphuric acid; oleum.','KGM',20,0,0 UNION ALL

Select '2808000000','Nitric acid; sulphonitric acids.','KGM',5,0,0 UNION ALL

Select '2809100000','Diphosphorus pentaoxide','KGM',30,0,0 UNION ALL


Select '2809203100','Hypophosphoric acid    ','KGM',0,0,0 UNION ALL
Select '2809203200','Phosphoric acid','KGM',30,0,0 UNION ALL
Select '2809203900','Other','KGM',30,0,0 UNION ALL

Select '2809209100','Hypophosphoric acid    ','KGM',0,0,0 UNION ALL
Select '2809209200','Phosphoric acid','KGM',30,0,0 UNION ALL
Select '2809209900','Other','KGM',30,0,0 UNION ALL

Select '28100000','Oxides of boron; boric acids.','',0,0,0 UNION ALL
Select '2810000010','Oxides of boron','KGM',0,0,0 UNION ALL
Select '2810000020','Boric acids','KGM',0,0,0 UNION ALL


Select '2811110000','Hydrogen fluoride (hydrofluoric acid)','KGM',0,0,0 UNION ALL
Select '2811120000','Hydrogen cyanide (hydrocyanic acid)','KGM',0,0,0 UNION ALL

Select '2811191000','Arsenic acid ','KGM',20,0,0 UNION ALL
Select '2811192000','Aminosulphonic acid (sulphamic acid)','KGM',0,0,0 UNION ALL
Select '2811199000','Other','KGM',0,0,0 UNION ALL

Select '2811210000','Carbon dioxide ','KGM',0,0,0 UNION ALL

Select '2811221000','In powder form','KGM',0,0,0 UNION ALL
Select '2811229000','Other ','KGM',0,0,0 UNION ALL

Select '2811291000','Diarsenic pentaoxide ','KGM',20,0,0 UNION ALL
Select '2811292000','Sulphur dioxide','KGM',0,0,0 UNION ALL
Select '2811299000','Other','KGM',0,0,0 UNION ALL



Select '2812110000','Carbonyl dichloride (phosgene) ','KGM',0,0,0 UNION ALL
Select '2812120000','Phosphorus oxychloride ','KGM',0,0,0 UNION ALL
Select '2812130000','Phosphorus trichloride','KGM',0,0,0 UNION ALL
Select '2812140000','Phosphorus pentachloride','KGM',0,0,0 UNION ALL
Select '2812150000','Sulphur monochloride ','KGM',0,0,0 UNION ALL
Select '2812160000','Sulphur dichloride ','KGM',0,0,0 UNION ALL
Select '2812170000','Thionyl chloride ','KGM',0,0,0 UNION ALL
Select '2812190000','Other','KGM',0,0,0 UNION ALL
Select '2812900000','Other','KGM',0,0,0 UNION ALL

Select '2813100000','Carbon disulphide','KGM',0,0,0 UNION ALL
Select '2813900000','Other','KGM',0,0,0 UNION ALL


Select '2814100000','Anhydrous ammonia','KGM',0,0,0 UNION ALL
Select '2814200000','Ammonia in aqueous solution','KGM',0,0,0 UNION ALL


Select '2815110000','Solid','KGM',20,0,0 UNION ALL
Select '2815120000','In aqueous solution (soda lye or liquid soda)','KGM',20,0,0 UNION ALL
Select '2815200000','Potassium hydroxide (caustic potash)','KGM',0,0,0 UNION ALL
Select '2815300000','Peroxides of sodium or potassium','KGM',0,0,0 UNION ALL

Select '2816100000','Hydroxide and peroxide of magnesium','KGM',0,0,0 UNION ALL
Select '2816400000','Oxides, hydroxides and peroxides, of strontium or barium','KGM',0,0,0 UNION ALL

Select '2817001000','Zinc oxide ','KGM',30,0,0 UNION ALL
Select '2817002000','Zinc peroxide ','KGM',30,0,0 UNION ALL

Select '2818100000','Artificial corundum, whether or not chemically defined','KGM',0,0,0 UNION ALL
Select '2818200000','Aluminium oxide, other than artificial corundum','KGM',0,0,0 UNION ALL
Select '2818300000','Aluminium hydroxide','KGM',0,0,0 UNION ALL

Select '2819100000','Chromium trioxide','KGM',0,0,0 UNION ALL
Select '2819900000','Other','KGM',0,0,0 UNION ALL

Select '2820100000','Manganese dioxide','KGM',0,0,0 UNION ALL
Select '2820900000','Other','KGM',0,0,0 UNION ALL

Select '2821100000','Iron oxides and hydroxides','KGM',0,0,0 UNION ALL
Select '2821200000','Earth colours','KGM',0,0,0 UNION ALL

Select '2822000000','Cobalt oxides and hydroxides; commercial cobalt oxides.','KGM',0,0,0 UNION ALL

Select '2823000000','Titanium oxides.','KGM',15,0,0 UNION ALL

Select '2824100000','Lead monoxide (litharge, massicot)','KGM',0,0,0 UNION ALL
Select '2824900000','Other','KGM',0,0,0 UNION ALL

Select '2825100000','Hydrazine and hydroxylamine and their inorganic salts','KGM',0,0,0 UNION ALL
Select '2825200000','Lithium oxide and hydroxide','KGM',0,0,0 UNION ALL
Select '2825300000','Vanadium oxides and hydroxides','KGM',0,0,0 UNION ALL
Select '2825400000','Nickel oxides and hydroxides','KGM',0,0,0 UNION ALL
Select '2825500000','Copper oxides and hydroxides','KGM',0,0,0 UNION ALL
Select '2825600000','Germanium oxides and zirconium dioxide','KGM',0,0,0 UNION ALL
Select '2825700000','Molybdenum oxides and hydroxides','KGM',0,0,0 UNION ALL
Select '2825800000','Antimony oxides','KGM',0,0,0 UNION ALL
Select '2825900000','Other','KGM',0,0,0 UNION ALL



Select '2826120000','Of aluminium','KGM',0,0,0 UNION ALL
Select '2826190000','Other','KGM',0,0,0 UNION ALL
Select '2826300000','Sodium hexafluoroaluminate (synthetic cryolite)','KGM',0,0,0 UNION ALL
Select '2826900000','Other','KGM',0,0,0 UNION ALL

Select '2827100000','Ammonium chloride','KGM',0,0,0 UNION ALL

Select '2827201000','Containing 73%  80% by weight
','KGM',0,0,0 UNION ALL
Select '2827209000','Other','KGM',0,0,0 UNION ALL

Select '2827310000','Of magnesium ','KGM',0,0,0 UNION ALL
Select '2827320000','Of aluminium ','KGM',0,0,0 UNION ALL
Select '2827350000','Of nickel ','KGM',0,0,0 UNION ALL

Select '2827391000','Of barium or of cobalt','KGM',0,0,0 UNION ALL
Select '2827392000','Of iron','KGM',0,0,0 UNION ALL
Select '2827393000','Of zinc ','KGM',0,0,0 UNION ALL
Select '2827399000','Other ','KGM',0,0,0 UNION ALL

Select '2827410000','Of copper ','KGM',0,0,0 UNION ALL
Select '2827490000','Other','KGM',0,0,0 UNION ALL

Select '2827510000','Bromides of sodium or of potassium','KGM',0,0,0 UNION ALL
Select '2827590000','Other','KGM',0,0,0 UNION ALL
Select '2827600000','Iodides and iodide oxides','KGM',0,0,0 UNION ALL

Select '2828100000','Commercial calcium hypochlorite and other calcium hypochlorites','KGM',5,0,0 UNION ALL

Select '2828901000','Sodium hypochlorite ','KGM',10,0,0 UNION ALL
Select '2828909000','Other ','KGM',5,0,0 UNION ALL


Select '2829110000','Of sodium','KGM',0,0,0 UNION ALL
Select '2829190000','Other','KGM',0,0,0 UNION ALL

Select '2829901000','Sodium perchlorate','KGM',0,0,0 UNION ALL
Select '2829909000','Other','KGM',0,0,0 UNION ALL

Select '2830100000','Sodium sulphides','KGM',0,0,0 UNION ALL

Select '2830901000','Cadmium sulphide or zinc sulphide','KGM',0,0,0 UNION ALL
Select '2830909000','Other','KGM',0,0,0 UNION ALL

Select '2831100000','Of sodium','KGM',0,0,0 UNION ALL
Select '2831900000','Other','KGM',0,0,0 UNION ALL

Select '2832100000','Sodium sulphites','KGM',0,0,0 UNION ALL
Select '2832200000','Other sulphites','KGM',0,0,0 UNION ALL
Select '2832300000','Thiosulphates','KGM',0,0,0 UNION ALL


Select '2833110000','Disodium sulphate','KGM',0,0,0 UNION ALL
Select '2833190000','Other      ','KGM',0,0,0 UNION ALL

Select '2833210000','Of magnesium','KGM',0,0,0 UNION ALL

Select '2833221000','Commercial grade','KGM',20,0,0 UNION ALL
Select '2833229000','Other','KGM',20,0,0 UNION ALL
Select '2833240000','Of nickel','KGM',0,0,0 UNION ALL
Select '2833250000','Of copper','KGM',0,0,0 UNION ALL
Select '2833270000','Of barium','KGM',0,0,0 UNION ALL

Select '2833292000','Tribasic lead sulphate','KGM',0,0,0 UNION ALL
Select '2833293000','Of chromium','KGM',0,0,0 UNION ALL
Select '2833299000','Other','KGM',0,0,0 UNION ALL
Select '2833300000','Alums','KGM',5,0,0 UNION ALL
Select '2833400000','Peroxosulphates (persulphates) ','KGM',0,0,0 UNION ALL

Select '2834100000','Nitrites','KGM',0,0,0 UNION ALL

Select '2834210000','Of potassium ','KGM',0,0,0 UNION ALL

Select '2834291000','Of bismuth','KGM',0,0,0 UNION ALL
Select '2834299000','Other','KGM',0,0,0 UNION ALL

Select '2835100000','Phosphinates (hypophosphites) and phosphonates (phosphites)','KGM',0,0,0 UNION ALL

Select '2835220000','Of mono or disodium','KGM',0,0,0 UNION ALL
Select '2835240000','Of potassium','KGM',0,0,0 UNION ALL

Select '2835251000','Feed grade','KGM',0,0,0 UNION ALL
Select '2835259000','Other','KGM',0,0,0 UNION ALL
Select '2835260000','Other phosphates of calcium','KGM',0,0,0 UNION ALL

Select '2835291000','Of trisodium','KGM',0,0,0 UNION ALL
Select '2835299000','Other','KGM',0,0,0 UNION ALL

Select '2835310000','Sodium triphosphate (sodium tripolyphosphate)','KGM',0,0,0 UNION ALL

Select '2835391000','Tetrasodium pyrophosphate','KGM',0,0,0 UNION ALL
Select '2835399000','Other','KGM',0,0,0 UNION ALL

Select '2836200000','Disodium carbonate ','KGM',0,0,0 UNION ALL
Select '2836300000','Sodium hydrogencarbonate (sodium bicarbonate) ','KGM',0,0,0 UNION ALL
Select '2836400000','Potassium carbonates ','KGM',0,0,0 UNION ALL

Select '2836501000','Food or pharmaceutical grade','KGM',0,0,0 UNION ALL
Select '2836509000','Other','KGM',0,0,0 UNION ALL
Select '2836600000','Barium carbonate ','KGM',0,0,0 UNION ALL

Select '2836910000','Lithium carbonates','KGM',0,0,0 UNION ALL
Select '2836920000','Strontium carbonate','KGM',0,0,0 UNION ALL

Select '2836991000','Commercial ammonium carbonate','KGM',0,0,0 UNION ALL
Select '2836992000','Lead carbonates','KGM',0,0,0 UNION ALL
Select '2836999000','Other','KGM',0,0,0 UNION ALL


Select '2837110000','Of sodium','KGM',0,0,0 UNION ALL
Select '2837190000','Other','KGM',0,0,0 UNION ALL
Select '2837200000','Complex cyanides','KGM',0,0,0 UNION ALL


Select '2839110000','Sodium metasilicates','KGM',20,0,0 UNION ALL

Select '2839191000','Sodium silicates ','KGM',20,0,0 UNION ALL
Select '2839199000','Other ','KGM',20,0,0 UNION ALL
Select '2839900000','Other','KGM',0,0,0 UNION ALL


Select '2840110000','Anhydrous ','KGM',0,0,0 UNION ALL
Select '2840190000','Other ','KGM',0,0,0 UNION ALL
Select '2840200000','Other borates','KGM',0,0,0 UNION ALL
Select '2840300000','Peroxoborates (perborates)','KGM',0,0,0 UNION ALL

Select '2841300000','Sodium dichromate ','KGM',0,0,0 UNION ALL
Select '2841500000','Other chromates and dichromates; peroxochromates ','KGM',0,0,0 UNION ALL

Select '2841610000','Potassium permanganate','KGM',0,0,0 UNION ALL
Select '2841690000','Other','KGM',0,0,0 UNION ALL
Select '2841700000','Molybdates ','KGM',0,0,0 UNION ALL
Select '2841800000','Tungstates (wolframates) ','KGM',0,0,0 UNION ALL
Select '2841900000','Other','KGM',0,0,0 UNION ALL

Select '2842100000','Double or complex silicates, including aluminosilicates whether or not chemically defined','KGM',0,0,0 UNION ALL

Select '2842901000','Sodium arsenite ','KGM',0,0,0 UNION ALL
Select '2842902000','Copper or chromium salts','KGM',20,0,0 UNION ALL
Select '2842903000','Other fulminates, cyanates and thiocyanates','KGM',0,0,0 UNION ALL
Select '2842909000','Other ','KGM',0,0,0 UNION ALL


Select '2843100000','Colloidal precious metals','KGM',0,0,0 UNION ALL

Select '2843210000','Silver nitrate','KGM',0,0,0 UNION ALL
Select '2843290000','Other','KGM',0,0,0 UNION ALL
Select '2843300000','Gold compounds','KGM',0,0,0 UNION ALL
Select '2843900000','Other compounds; amalgams','KGM',0,0,0 UNION ALL


Select '2844101000','Natural uranium and its compounds','KGM',0,0,0 UNION ALL
Select '2844109000','Other','KGM',0,0,0 UNION ALL

Select '2844201000','Uranium enriched in U 235 and its compounds; plutonium and its compounds','KGM',0,0,0 UNION ALL
Select '2844209000','Other ','KGM',0,0,0 UNION ALL

Select '2844301000','Uranium depleted in U 235 and its compounds; thorium and its compounds','KGM',0,0,0 UNION ALL
Select '2844309000','Other','KGM',0,0,0 UNION ALL

Select '2844401000','Radioactive elements and isotopes and compounds; radioactive residues','KGM',0,0,0 UNION ALL
Select '2844409000','Other ','KGM',0,0,0 UNION ALL
Select '2844500000','Spent (irradiated) fuel elements (cartridges) of nuclear reactors ','KGM',0,0,0 UNION ALL

Select '2845100000','Heavy water (deuterium oxide) ','KGM',0,0,0 UNION ALL
Select '2845900000','Other ','KGM',0,0,0 UNION ALL

Select '2846100000','Cerium compounds ','KGM',0,0,0 UNION ALL
Select '2846900000','Other ','KGM',0,0,0 UNION ALL

Select '2847001000','In liquid form','KGM',0,0,0 UNION ALL
Select '2847009000','Other ','KGM',0,0,0 UNION ALL

Select '2849100000','Of calcium','KGM',20,0,0 UNION ALL
Select '2849200000','Of silicon','KGM',0,0,0 UNION ALL
Select '2849900000','Other','KGM',0,0,0 UNION ALL

Select '2850000000','Hydrides, nitrides, azides, silicides and borides, whether or not chemically defined, other than compounds which are also carbides of heading 28.49.','KGM',0,0,0 UNION ALL


Select '2852101000','Mercury sulphate','KGM',0,0,0 UNION ALL
Select '2852102000','Mercury compounds of a kind used as luminophores','KGM',0,0,0 UNION ALL
Select '2852109000','Other','KGM',0,0,0 UNION ALL

Select '2852901000','Mercury tannates','KGM',0,0,0 UNION ALL
Select '2852902000','Mercury sulphides; mercury polysulphides; mercury polyphosphates; mercury carbides;  heterocyclic mercury compounds of subheading 2934.99.90; mercury peptone derivatives; other protein derivatives of mercury','KGM',0,0,0 UNION ALL
Select '2852909000','Other','KGM',0,0,0 UNION ALL

Select '2853100000','Cyanogen chloride (chlorcyan)','KGM',0,0,0 UNION ALL

Select '2853901000','Demineralized waters','KGM',0,0,0 UNION ALL
Select '2853909000','Other','KGM',0,0,0 UNION ALL


Select '2901100000','Saturated','KGM',0,0,0 UNION ALL

Select '2901210000','Ethylene ','KGM',0,0,0 UNION ALL
Select '2901220000','Propene (propylene)','KGM',0,0,0 UNION ALL
Select '2901230000','Butene (butylene) and isomers thereof','KGM',0,0,0 UNION ALL
Select '2901240000','Buta1,3diene and isoprene','KGM',0,0,0 UNION ALL

Select '2901291000','Acetylene ','KGM',20,0,0 UNION ALL
Select '2901292000','Hexene and isomers thereof','KGM',0,0,0 UNION ALL
Select '2901299000','Other ','KGM',0,0,0 UNION ALL


Select '2902110000','Cyclohexane','KGM',0,0,0 UNION ALL
Select '2902190000',' Other','KGM',0,0,0 UNION ALL
Select '2902200000','Benzene','KGM',0,0,0 UNION ALL
Select '2902300000','Toluene','KGM',0,0,0 UNION ALL

Select '2902410000','oXylene','KGM',0,0,0 UNION ALL
Select '2902420000','mXylene','KGM',0,0,0 UNION ALL
Select '2902430000','pXylene','KGM',0,0,0 UNION ALL
Select '2902440000','Mixed xylene isomers','KGM',0,0,0 UNION ALL
Select '2902500000','Styrene','KGM',0,0,0 UNION ALL
Select '2902600000','Ethylbenzene','KGM',0,0,0 UNION ALL
Select '2902700000','Cumene','KGM',0,0,0 UNION ALL

Select '2902901000','Dodecylbenzene','KGM',0,0,0 UNION ALL
Select '2902902000','Other alkylbenzenes ','KGM',0,0,0 UNION ALL
Select '2902909000','Other','KGM',0,0,0 UNION ALL



Select '2903111000','Chloromethane (methyl chloride)','KGM',15,0,0 UNION ALL
Select '2903119000','Other ','KGM',0,0,0 UNION ALL
Select '2903120000','Dichloromethane (methylene chloride)','KGM',0,0,0 UNION ALL
Select '2903130000','Chloroform (trichloromethane)','KGM',0,0,0 UNION ALL
Select '2903140000','Carbon tetrachloride','KGM',0,0,0 UNION ALL
Select '2903150000','Ethylene dichloride (ISO) (1,2dichloroethane)','KGM',0,0,0 UNION ALL

Select '2903191000','1,2  Dichloropropane (propylene dichloride) and dichlorobutanes','KGM',0,0,0 UNION ALL
Select '2903192000','1,1,1Trichloroethane (methyl chloroform)','KGM',0,0,0 UNION ALL
Select '2903199000','Other','KGM',0,0,0 UNION ALL

Select '2903210000','Vinyl chloride (chloroethylene)','KGM',0,0,0 UNION ALL
Select '2903220000','Trichloroethylene','KGM',0,0,0 UNION ALL
Select '2903230000','Tetrachloroethylene (perchloroethylene)','KGM',0,0,0 UNION ALL
Select '2903290000','Other','KGM',0,0,0 UNION ALL

Select '2903310000','Ethylene dibromide (ISO) (1,2dibromoethane)','KGM',0,0,0 UNION ALL

Select '2903391000','Bromomethane (methyl bromide)','KGM',0,0,0 UNION ALL
Select '2903399000','Other ','KGM',0,0,0 UNION ALL

Select '2903710000','  Chlorodifluoromethane','KGM',0,0,0 UNION ALL
Select '2903720000','  Dichlorotrifluoroethanes','KGM',0,0,0 UNION ALL
Select '2903730000','  Dichlorofluoroethanes   ','KGM',0,0,0 UNION ALL
Select '2903740000','  Chlorodifluoroethanes','KGM',0,0,0 UNION ALL
Select '2903750000','  Dichloropentafluoropropanes','KGM',0,0,0 UNION ALL
Select '2903760000','  Bromochlorodifluoromethane, bromotrifluoromethane and dibromotetrafluoroethanes','KGM',0,0,0 UNION ALL
Select '29037700','  Other, perhalogenated only with fluorine and chlorine:','',0,0,0 UNION ALL

Select '2903770011','    Trichlorofluoromethane (CFC11)','KGM',0,0,0 UNION ALL
Select '2903770012','    Dichlorodifluoromethane (CFC12)','KGM',0,0,0 UNION ALL
Select '2903770014','    Chlorotrifluoromethane (CFC13)','KGM',0,0,0 UNION ALL

Select '2903770021','    Trichlorotrifluoroethanes  (CFC113)','KGM',0,0,0 UNION ALL
Select '2903770022','    Dichlorotetrafluoroethanes (CFC114) and chloropentafluoroethane (CFC115)     ','KGM',0,0,0 UNION ALL
Select '2903770026','    Pentachlorofluoroethane (CFC111)','KGM',0,0,0 UNION ALL
Select '2903770027','    Tetrachlorodifluoroethanes (CFC112)','KGM',0,0,0 UNION ALL
Select '2903770029','    Other ','KGM',0,0,0 UNION ALL

Select '2903770041','    Heptachlorodifluoropropane (CFC211)','KGM',0,0,0 UNION ALL
Select '2903770042','    Hexachlorodifluoropropane   (CFC212)   ','KGM',0,0,0 UNION ALL
Select '2903770043','    Pentachlorotrifluoropropane (CFC213)','KGM',0,0,0 UNION ALL
Select '2903770044','    Tetrachlorotetrafluoropropane (CFC214)','KGM',0,0,0 UNION ALL
Select '2903770045','    Trichloropentafluoropropane (CFC215)','KGM',0,0,0 UNION ALL
Select '2903770046','    Dichlorohexafluoropropane (CFC216)','KGM',0,0,0 UNION ALL
Select '2903770047','    Chloroheptafluoropropane (CFC217)','KGM',0,0,0 UNION ALL
Select '2903770049','Other ','KGM',0,0,0 UNION ALL
Select '2903770090','Other ','KGM',0,0,0 UNION ALL
Select '2903780000','  Other perhalogenated derivatives','KGM',0,0,0 UNION ALL
Select '29037900','  Other:','',0,0,0 UNION ALL

Select '2903790011','    Dichlorofluoromethane (HCFC21)','KGM',0,0,0 UNION ALL
Select '2903790012','    Tetrachlorofluoroethane  (HCFC121)','KGM',0,0,0 UNION ALL
Select '2903790013','    Trichlorodifluoroethane (HCFC122)','KGM',0,0,0 UNION ALL
Select '2903790014','    Chlorotetrafluoroethane (HCFC124)','KGM',0,0,0 UNION ALL
Select '2903790015','    Trichlorotetrafluoropropane (HCFC224)','KGM',0,0,0 UNION ALL
Select '2903790016','    Chloropentafluoropropane (HCFC235)','KGM',0,0,0 UNION ALL
Select '2903790019','    Other','KGM',0,0,0 UNION ALL

Select '2903790021','    Bromodifluoromethane (HBFC22B1)','KGM',0,0,0 UNION ALL
Select '2903790022','    Bromotrifluoroethane (Halon 1301)','KGM',0,0,0 UNION ALL
Select '2903790029','    Other','KGM',0,0,0 UNION ALL
Select '2903790030','   Bromochlorodifluoroethane (Halon 1211)','KGM',0,0,0 UNION ALL
Select '2903790090','   Other ','KGM',0,0,0 UNION ALL

Select '2903810000','  1,2,3,4,5,6Hexachlorocyclohexane (HCH (ISO)), including lindane (ISO, INN)','KGM',0,0,0 UNION ALL
Select '2903820000','  Aldrin (ISO), chlordane (ISO) and heptachlor (ISO)','KGM',0,0,0 UNION ALL
Select '2903830000','  Mirex (ISO)','KGM',0,0,0 UNION ALL
Select '2903890000','  Other','KGM',0,0,0 UNION ALL

Select '2903910000','  Chlorobenzene, odichlorobenzene and pdichlorobenzene','KGM',0,0,0 UNION ALL
Select '2903920000','  Hexachlorobenzene (ISO) and DDT (ISO) (clofenotane (INN),1,1,1trichloro2,2bis(pchlorophenyl)ethane)','KGM',0,0,0 UNION ALL
Select '2903930000','  Pentachlorobenzene (ISO) ','KGM',0,0,0 UNION ALL
Select '2903940000','  Hexabromobiphenyls','KGM',0,0,0 UNION ALL
Select '2903990000','  Other','KGM',0,0,0 UNION ALL

Select '2904100000','Derivatives containing only sulpho groups, their salts and ethyl esters','KGM',0,0,0 UNION ALL

Select '2904201000','Trinitrotoluene','KGM',0,0,0 UNION ALL
Select '2904209000','Other','KGM',0,0,0 UNION ALL

Select '2904310000','Perfluorooctane sulphonic acid ','KGM',0,0,0 UNION ALL
Select '2904320000','Ammonium perfluorooctane sulphonate','KGM',0,0,0 UNION ALL
Select '2904330000','Lithium perfluorooctane sulphonate ','KGM',0,0,0 UNION ALL
Select '2904340000','Potassium perfluorooctane sulphonate','KGM',0,0,0 UNION ALL
Select '2904350000','Other salts of perfluorooctane sulphonic acid ','KGM',0,0,0 UNION ALL
Select '2904360000','Perfluorooctane sulphonyl fluoride','KGM',0,0,0 UNION ALL

Select '2904910000','Trichloronitromethane (chloropicrin)','KGM',0,0,0 UNION ALL
Select '2904990000','Other','KGM',0,0,0 UNION ALL



Select '2905110000','Methanol (methyl alcohol)','KGM',0,0,0 UNION ALL
Select '2905120000','Propan1ol (propyl alcohol) and propan2ol (isopropyl  alcohol)  ','KGM',0,0,0 UNION ALL
Select '2905130000','Butan1ol (nbutyl alcohol)','KGM',0,0,0 UNION ALL
Select '2905140000','Other butanols','KGM',0,0,0 UNION ALL
Select '2905160000','Octanol (octyl alcohol) and isomers thereof','KGM',0,0,0 UNION ALL
Select '2905170000','Dodecan1ol (lauryl alcohol), hexadecan1ol (cetyl alcohol) and octadecan 1ol (stearyl alcohol)','KGM',0,0,0 UNION ALL
Select '2905190000','Other ','KGM',0,0,0 UNION ALL

Select '2905220000','Acyclic terpene alcohols','KGM',0,0,0 UNION ALL
Select '2905290000','Other','KGM',0,0,0 UNION ALL

Select '2905310000','Ethylene glycol (ethanediol)','KGM',0,0,0 UNION ALL
Select '2905320000','Propylene glycol (propane1,2diol)','KGM',0,0,0 UNION ALL
Select '2905390000','Other','KGM',0,0,0 UNION ALL

Select '2905410000','2Ethyl2(hydroxymethyl) propane1 ,3diol (trimethylolpropane)','KGM',0,0,0 UNION ALL
Select '2905420000','Pentaerythritol','KGM',0,0,0 UNION ALL
Select '2905430000','Mannitol','KGM',0,0,0 UNION ALL
Select '2905440000','Dglucitol (sorbitol)','KGM',0,0,0 UNION ALL
Select '2905450000','Glycerol','KGM',5,0,0 UNION ALL
Select '2905490000','Other','KGM',0,0,0 UNION ALL

Select '2905510000','Ethchlorvynol (INN) ','KGM',0,0,0 UNION ALL
Select '2905590000','Other ','KGM',0,0,0 UNION ALL


Select '2906110000','Menthol','KGM',0,0,0 UNION ALL
Select '2906120000','Cyclohexanol, methylcyclohexanols and dimethylcyclohexanols','KGM',0,0,0 UNION ALL
Select '2906130000','Sterols and inositols','KGM',0,0,0 UNION ALL
Select '2906190000','Other','KGM',0,0,0 UNION ALL

Select '2906210000','Benzyl alcohol','KGM',0,0,0 UNION ALL
Select '2906290000','Other','KGM',0,0,0 UNION ALL



Select '2907110000','Phenol (hydroxybenzene) and its salts','KGM',0,0,0 UNION ALL
Select '2907120000','Cresols and their salts','KGM',0,0,0 UNION ALL
Select '2907130000','Octylphenol, nonylphenol and their isomers; salts thereof','KGM',0,0,0 UNION ALL
Select '2907150000','Naphthols and their salts','KGM',0,0,0 UNION ALL
Select '2907190000','Other     ','KGM',0,0,0 UNION ALL

Select '2907210000','Resorcinol and its salts ','KGM',0,0,0 UNION ALL
Select '2907220000','Hydroquinone (quinol) and its salts ','KGM',0,0,0 UNION ALL
Select '2907230000','4,4''Isopropylidenediphenol (bisphenol A, diphenylolpropane) and its salts','KGM',0,0,0 UNION ALL

Select '2907291000','Phenolalcohols','KGM',0,0,0 UNION ALL
Select '2907299000','Other','KGM',0,0,0 UNION ALL


Select '2908110000','Pentachlorophenol (ISO)','KGM',0,0,0 UNION ALL
Select '2908190000','Other','KGM',0,0,0 UNION ALL

Select '2908910000','Dinoseb (ISO) and its salts','KGM',0,0,0 UNION ALL
Select '2908920000','4,6Dinitroocresol (DNOC (ISO)) and its salts','KGM',0,0,0 UNION ALL
Select '2908990000','Other','KGM',0,0,0 UNION ALL



Select '2909110000','Diethyl ether ','KGM',0,0,0 UNION ALL
Select '2909190000','Other','KGM',0,0,0 UNION ALL
Select '2909200000','Cyclanic, cyclenic or cycloterpenic ethers and their halogenated, sulphonated, nitrated or nitrosated derivatives','KGM',0,0,0 UNION ALL
Select '2909300000','Aromatic ethers and their halogenated, sulphonated, nitrated or nitrosated derivatives','KGM',0,0,0 UNION ALL

Select '2909410000','2,2''Oxydiethanol (diethylene glycol, digol)','KGM',0,0,0 UNION ALL
Select '2909430000','Monobutyl ethers of ethylene glycol or of diethylene glycol','KGM',0,0,0 UNION ALL
Select '2909440000','Other monoalkylethers of ethylene glycol or of diethylene glycol','KGM',0,0,0 UNION ALL
Select '2909490000','Other','KGM',0,0,0 UNION ALL
Select '2909500000','Etherphenols, etheralcoholphenols and their halogenated, sulphonated, nitrated or nitrosated derivatives','KGM',0,0,0 UNION ALL
Select '2909600000','Alcohol peroxides, ether peroxides, ketone peroxides and their halogenated, sulphonated, nitrated or nitrosated derivatives','KGM',0,0,0 UNION ALL

Select '2910100000','Oxirane (ethylene oxide)','KGM',0,0,0 UNION ALL
Select '2910200000','Methyloxirane (propylene oxide)','KGM',0,0,0 UNION ALL
Select '2910300000','1Chloro2,3 epoxypropane (epichlorohydrin)','KGM',0,0,0 UNION ALL
Select '2910400000','Dieldrin (ISO, INN)','KGM',0,0,0 UNION ALL
Select '2910500000','Endrin (ISO)','KGM',0,0,0 UNION ALL
Select '2910900000','Other','KGM',0,0,0 UNION ALL

Select '2911000000','Acetals and hemiacetals, whether or not with other oxygen function, and their halogenated, sulphonated, nitrated or nitrosated derivatives.','KGM',0,0,0 UNION ALL




Select '2912111000','Formalin','KGM',0,0,0 UNION ALL
Select '2912119000','Other','KGM',0,0,0 UNION ALL
Select '2912120000','Ethanal (acetaldehyde)','KGM',0,0,0 UNION ALL
Select '2912190000','Other','KGM',0,0,0 UNION ALL

Select '2912210000','Benzaldehyde','KGM',0,0,0 UNION ALL
Select '2912290000','Other','KGM',0,0,0 UNION ALL

Select '2912410000','Vanillin (4hydroxy3methoxybenzaldehyde)','KGM',0,0,0 UNION ALL
Select '2912420000','Ethylvanillin (3ethoxy4hydroxybenzaldehyde)','KGM',0,0,0 UNION ALL

Select '2912491000','Other aldehydealcohols','KGM',0,0,0 UNION ALL
Select '2912499000','Other','KGM',0,0,0 UNION ALL
Select '2912500000','Cyclic polymers of aldehydes','KGM',0,0,0 UNION ALL
Select '2912600000','Paraformaldehyde','KGM',0,0,0 UNION ALL

Select '2913000000','Halogenated, sulphonated, nitrated or nitrosated derivatives of products of heading 29.12.','KGM',0,0,0 UNION ALL



Select '2914110000','Acetone','KGM',0,0,0 UNION ALL
Select '2914120000','Butanone (methyl ethyl ketone)','KGM',0,0,0 UNION ALL
Select '2914130000','4Methylpentan2one (methyl isobutyl ketone) ','KGM',0,0,0 UNION ALL
Select '2914190000','Other','KGM',0,0,0 UNION ALL

Select '2914220000','Cyclohexanone and methylcyclohexanones','KGM',0,0,0 UNION ALL
Select '2914230000','Ionones and methylionones','KGM',0,0,0 UNION ALL

Select '2914291000','Camphor','KGM',0,0,0 UNION ALL
Select '2914299000','Other','KGM',0,0,0 UNION ALL

Select '2914310000','Phenylacetone (phenylpropan2one) ','KGM',0,0,0 UNION ALL
Select '2914390000','Other','KGM',0,0,0 UNION ALL
Select '2914400000','Ketonealcohols and ketonealdehydes','KGM',0,0,0 UNION ALL
Select '2914500000','Ketonephenols and ketones with other oxygen function','KGM',0,0,0 UNION ALL

Select '2914610000','Anthraquinone','KGM',0,0,0 UNION ALL
Select '2914620000','Coenzyme Q10 (ubidecarenone (INN))','KGM',0,0,0 UNION ALL
Select '2914690000','Other','KGM',0,0,0 UNION ALL

Select '2914710000','Chlordecone (ISO) ','KGM',0,0,0 UNION ALL
Select '2914790000','Other','KGM',0,0,0 UNION ALL



Select '2915110000','Formic acid','KGM',0,0,0 UNION ALL
Select '2915120000','Salts of formic acid','KGM',0,0,0 UNION ALL
Select '2915130000','Esters of formic acid','KGM',0,0,0 UNION ALL

Select '2915210000','Acetic acid','KGM',0,0,0 UNION ALL
Select '2915240000','Acetic anhydride','KGM',0,0,0 UNION ALL

Select '2915291000','Sodium acetate; cobalt acetates','KGM',0,0,0 UNION ALL
Select '2915299000','Other','KGM',0,0,0 UNION ALL

Select '2915310000','Ethyl acetate','KGM',0,0,0 UNION ALL
Select '2915320000','Vinyl acetate','KGM',0,0,0 UNION ALL
Select '2915330000','nButyl acetate','KGM',0,0,0 UNION ALL
Select '2915360000','Dinoseb (ISO) acetate','KGM',0,0,0 UNION ALL

Select '2915391000','Isobutyl acetate','KGM',0,0,0 UNION ALL
Select '2915392000','2  Ethoxyethyl acetate','KGM',0,0,0 UNION ALL
Select '2915399000','Other','KGM',0,0,0 UNION ALL
Select '2915400000','Mono , di or trichloroacetic acids, their salts and esters','KGM',0,0,0 UNION ALL
Select '2915500000','Propionic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2915600000','Butanoic acids, pentanoic acids, their salts and esters ','KGM',0,0,0 UNION ALL

Select '2915701000','Palmitic acid, its salts and esters ','KGM',0,0,0 UNION ALL
Select '2915702000','Stearic acid ','KGM',0,0,0 UNION ALL
Select '2915703000','Salts and esters of stearic acid ','KGM',0,0,0 UNION ALL

Select '2915901000','Acetyl chloride','KGM',0,0,0 UNION ALL
Select '2915902000','Lauric acid, myristic acid, their salts and esters','KGM',0,0,0 UNION ALL
Select '2915903000','Caprylic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2915904000','Capric acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2915909000','Other','KGM',0,0,0 UNION ALL


Select '2916110000','Acrylic acid and its salts','KGM',0,0,0 UNION ALL
Select '2916120000','Esters of acrylic acid','KGM',0,0,0 UNION ALL
Select '2916130000','Methacrylic acid and its salts','KGM',0,0,0 UNION ALL

Select '2916141000','Methyl methacrylate ','KGM',0,0,0 UNION ALL
Select '2916149000','Other ','KGM',0,0,0 UNION ALL
Select '2916150000','Oleic, linoleic or linolenic acids, their salts and esters','KGM',0,0,0 UNION ALL
Select '2916160000','Binapacryl (ISO)','KGM',0,0,0 UNION ALL
Select '2916190000','Other','KGM',0,0,0 UNION ALL
Select '2916200000','Cyclanic, cyclenic or cycloterpenic monocarboxylic acids, their anhydrides, halides, peroxides, peroxyacids and their derivatives','KGM',0,0,0 UNION ALL

Select '2916310000','Benzoic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2916320000','Benzoyl peroxide and benzoyl chloride','KGM',0,0,0 UNION ALL
Select '2916340000','Phenylacetic acid and its salts','KGM',0,0,0 UNION ALL

Select '2916391000','2,4Dichlorophenyl acetic acid and its salts and esters ','KGM',0,0,0 UNION ALL
Select '2916392000','Esters of phenylacetic acid','KGM',0,0,0 UNION ALL
Select '2916399000','Other','KGM',0,0,0 UNION ALL


Select '2917110000','Oxalic acid, its salts and esters','KGM',0,0,0 UNION ALL

Select '2917121000','Dioctyl adipate ','KGM',0,0,0 UNION ALL
Select '2917129000','Other ','KGM',0,0,0 UNION ALL
Select '2917130000','Azelaic acid, sebacic acid, their salts and esters','KGM',0,0,0 UNION ALL
Select '2917140000','Maleic anhydride','KGM',0,0,0 UNION ALL
Select '2917190000','Other','KGM',0,0,0 UNION ALL
Select '2917200000','Cyclanic, cyclenic or cycloterpenic polycarboxylic acids, their anhydrides, halides, peroxides, peroxyacids and their derivatives','KGM',0,0,0 UNION ALL

Select '2917320000','Dioctyl orthophthalates','KGM',10,0,0 UNION ALL
Select '2917330000','Dinonyl or didecyl orthophthalates','KGM',0,0,0 UNION ALL

Select '2917341000','Dibutyl orthophthalates','KGM',0,0,0 UNION ALL
Select '2917349000','Other','KGM',0,0,0 UNION ALL
Select '2917350000','Phthalic anhydride','KGM',0,0,0 UNION ALL
Select '2917360000','Terephthalic acid and its salts ','KGM',0,0,0 UNION ALL
Select '2917370000','Dimethyl terephthalate ','KGM',0,0,0 UNION ALL

Select '2917391000','Trioctyltrimellitate ','KGM',0,0,0 UNION ALL
Select '2917392000','Other phthalic compounds of a kind used as plasticisers and esters of phthalic anhydride','KGM',0,0,0 UNION ALL
Select '2917399000','Other','KGM',0,0,0 UNION ALL


Select '2918110000','Lactic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2918120000','Tartaric acid','KGM',0,0,0 UNION ALL
Select '2918130000','Salts and esters of tartaric acid','KGM',0,0,0 UNION ALL
Select '2918140000','Citric acid','KGM',0,0,0 UNION ALL

Select '2918151000','Calcium citrate ','KGM',0,0,0 UNION ALL
Select '2918159000','Other ','KGM',0,0,0 UNION ALL
Select '2918160000','Gluconic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2918170000','2,2Diphenyl2hydroxyacetic acid (benzilic acid)','KGM',0,0,0 UNION ALL
Select '2918180000','Chlorobenzilate (ISO)','KGM',0,0,0 UNION ALL
Select '2918190000','Other','KGM',0,0,0 UNION ALL

Select '2918210000','Salicylic acid and its salts','KGM',0,0,0 UNION ALL
Select '2918220000','oAcetylsalicylic acid, its salts and esters','KGM',0,0,0 UNION ALL
Select '2918230000','Other esters of salicylic acid and their salts','KGM',0,0,0 UNION ALL

Select '2918291000','Alkyl sulphonic ester of phenol ','KGM',0,0,0 UNION ALL
Select '2918299000','Other ','KGM',0,0,0 UNION ALL
Select '2918300000','Carboxylic acids with aldehyde or ketone function but without other oxygen function, their anhydrides, halides, peroxides, peroxyacids and their derivatives','KGM',0,0,0 UNION ALL

Select '2918910000','2,4,5T (ISO) (2,4,5trichlorophenoxyacetic acid), its salt and esters','KGM',0,0,0 UNION ALL
Select '2918990000','Other','KGM',0,0,0 UNION ALL


Select '2919100000','Tris(2,3dibromopropyl) phosphate','KGM',0,0,0 UNION ALL
Select '2919900000','Other','KGM',0,0,0 UNION ALL


Select '2920110000','Parathion (ISO) and parathionmethyl (ISO) (methylparathion)','KGM',0,0,0 UNION ALL
Select '2920190000','Other','KGM',0,0,0 UNION ALL

Select '2920210000','Dimethyl phosphite ','KGM',0,0,0 UNION ALL
Select '2920220000','Diethyl phosphite ','KGM',0,0,0 UNION ALL
Select '2920230000','Trimethyl phosphite ','KGM',0,0,0 UNION ALL
Select '2920240000','Triethyl phosphite ','KGM',0,0,0 UNION ALL
Select '2920290000','Other','KGM',0,0,0 UNION ALL
Select '2920300000','Endosulfan (ISO)','KGM',0,0,0 UNION ALL
Select '2920900000','Other','KGM',0,0,0 UNION ALL



Select '2921110000','Methylamine, di or trimethylamine and their salts','KGM',0,0,0 UNION ALL
Select '2921120000','2(N,NDimethylamino)ethylchloride hydrochloride ','KGM',0,0,0 UNION ALL
Select '2921130000','2(N,NDiethylamino)ethylchloride hydrochloride ','KGM',0,0,0 UNION ALL
Select '2921140000','2(N,NDiisopropylamino)ethylchloride hydrochloride','KGM',0,0,0 UNION ALL
Select '2921190000','Other','KGM',0,0,0 UNION ALL

Select '2921210000','Ethylenediamine and its salts','KGM',0,0,0 UNION ALL
Select '2921220000','Hexamethylenediamine and its salts','KGM',0,0,0 UNION ALL
Select '2921290000','Other','KGM',0,0,0 UNION ALL
Select '2921300000','Cyclanic, cyclenic or cycloterpenic mono or polyamines, and their derivatives; salts thereof','KGM',0,0,0 UNION ALL

Select '2921410000','Aniline and its salts','KGM',0,0,0 UNION ALL
Select '2921420000','Aniline derivatives and their salts','KGM',0,0,0 UNION ALL
Select '2921430000','Toluidines and their derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2921440000','Diphenylamine and its derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2921450000','1Naphthylamine (alphanaphthylamine), 2naphthylamine (betanaphthylamine) and their derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2921460000','Amfetamine (INN), benzfetamine (INN), dexamfetamine (INN), etilamfetamine (INN), fencamfamin (INN), lefetamine (INN), levamfetamine (INN), mefenorex (INN) and phentermine (INN); salts thereof','KGM',0,0,0 UNION ALL
Select '2921490000','Other ','KGM',0,0,0 UNION ALL

Select '2921510000','o, m, pPhenylenediamine, diaminotoluenes, and their   derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2921590000','Other','KGM',0,0,0 UNION ALL


Select '2922110000','Monoethanolamine and its salts','KGM',0,0,0 UNION ALL
Select '2922120000','Diethanolamine and its salts','KGM',0,0,0 UNION ALL
Select '2922140000','Dextropropoxyphene (INN) and its salts ','KGM',0,0,0 UNION ALL
Select '2922150000','Triethanolamine ','KGM',0,0,0 UNION ALL
Select '2922160000','Diethanolammonium perfluorooctane sulphonate','KGM',0,0,0 UNION ALL
Select '2922170000','Methyldiethanolamine and ethyldiethanolamine ','KGM',0,0,0 UNION ALL
Select '2922180000','2(N,NDiisopropylamino)ethanol','KGM',0,0,0 UNION ALL

Select '2922191000','Ethambutol and its salts, esters and other derivatives','KGM',0,0,0 UNION ALL
Select '2922192000','D2Aminonbutylalcohol','KGM',0,0,0 UNION ALL
Select '2922199000','Other','KGM',0,0,0 UNION ALL

Select '2922210000','Aminohydroxynaphthalenesulphonic acids and their salts','KGM',0,0,0 UNION ALL
Select '2922290000','Other','KGM',0,0,0 UNION ALL

Select '2922310000','Amfepramone (INN), methadone (INN) and normethadone (INN); salts thereof','KGM',0,0,0 UNION ALL
Select '2922390000','Other ','KGM',0,0,0 UNION ALL

Select '2922410000','Lysine and its esters; salts thereof','KGM',0,0,0 UNION ALL

Select '2922421000','Glutamic acid','KGM',15,0,0 UNION ALL
Select '2922422000','Monosodium glutamate (MSG) ','KGM',15,0,0 UNION ALL
Select '2922429000','Other salts','KGM',0,0,0 UNION ALL
Select '2922430000','Anthranilic acid and its salts','KGM',0,0,0 UNION ALL
Select '2922440000','Tilidine (INN) and its salts ','KGM',0,0,0 UNION ALL
Select '2922490000','Other','KGM',0,0,0 UNION ALL

Select '2922501000','pAminosalicylic acid and its salts, ester and other derivatives ','KGM',0,0,0 UNION ALL
Select '2922509000','Other ','KGM',0,0,0 UNION ALL

Select '2923100000','Choline and its salts','KGM',0,0,0 UNION ALL

Select '2923201000','Lecithins, whether or not chemically defined','KGM',0,0,0 UNION ALL
Select '2923209000','Other','KGM',0,0,0 UNION ALL
Select '2923300000','Tetraethylammonium perfluorooctane sulphonate ','KGM',0,0,0 UNION ALL
Select '2923400000','Didecyldimethylammonium perfluorooctane sulphonate','KGM',0,0,0 UNION ALL
Select '2923900000','Other','KGM',0,0,0 UNION ALL


Select '2924110000','Meprobamate (INN) ','KGM',0,0,0 UNION ALL

Select '2924121000','Fluoroacetamide (ISO) and phosphamidon (ISO)','KGM',0,0,0 UNION ALL
Select '2924122000','Monocrotophos (ISO)','KGM',0,0,0 UNION ALL

Select '2924191000','Carisophrodol','KGM',0,0,0 UNION ALL
Select '2924199000','Other','KGM',0,0,0 UNION ALL


Select '2924211000','4Ethoxyphenylurea (dulcin)','KGM',0,0,0 UNION ALL
Select '2924212000','Diuron and monuron      ','KGM',5,0,0 UNION ALL
Select '2924219000','Other','KGM',0,0,0 UNION ALL
Select '2924230000','2Acetamidobenzoic acid (Nacetylanthranilic acid) and its salts','KGM',0,0,0 UNION ALL
Select '2924240000','Ethinamate (INN)','KGM',0,0,0 UNION ALL
Select '2924250000','Alachlor (ISO)','KGM',0,0,0 UNION ALL

Select '2924291000','Aspartame ','KGM',0,0,0 UNION ALL
Select '2924292000','Butylphenylmethyl carbamate; methyl isopropyl phenyl carbamate','KGM',0,0,0 UNION ALL
Select '2924293000','Acetaminophen (paracetamol); salicylamide; ethoxybenzamide','KGM',0,0,0 UNION ALL
Select '2924299000','Other ','KGM',0,0,0 UNION ALL


Select '2925110000','Saccharin and its salts','KGM',0,0,0 UNION ALL
Select '2925120000','Glutethimide (INN)','KGM',0,0,0 UNION ALL
Select '2925190000','Other','KGM',0,0,0 UNION ALL

Select '2925210000','Chlordimeform (ISO)','KGM',0,0,0 UNION ALL
Select '2925290000','Other','KGM',0,0,0 UNION ALL

Select '2926100000','Acrylonitrile ','KGM',0,0,0 UNION ALL
Select '2926200000','1Cyanoguanidine (dicyandiamide)','KGM',0,0,0 UNION ALL
Select '2926300000','Fenproporex (INN) and its salts; methadone (INN) intermediate (4cyano2dimethylamino4, 4diphenylbutane) ','KGM',0,0,0 UNION ALL
Select '2926400000','alphaPhenylacetoacetonitrile','KGM',0,0,0 UNION ALL
Select '2926900000','Other ','KGM',0,0,0 UNION ALL


Select '2927001000','Azodicarbonamide ','KGM',0,0,0 UNION ALL
Select '2927009000','Other ','KGM',0,0,0 UNION ALL


Select '2928001000','Linuron ','KGM',5,0,0 UNION ALL
Select '2928009000','Other ','KGM',0,0,0 UNION ALL


Select '2929101000','Diphenylmethane diisocyanate (MDI)','KGM',0,0,0 UNION ALL
Select '2929102000','Toluene diisocyanate','KGM',0,0,0 UNION ALL
Select '2929109000','Other','KGM',0,0,0 UNION ALL

Select '2929901000','Sodium cyclamate','KGM',0,0,0 UNION ALL
Select '2929902000','Other cyclamates','KGM',0,0,0 UNION ALL
Select '2929909000','Other','KGM',0,0,0 UNION ALL


Select '2930200000','Thiocarbamates and dithiocarbamates','KGM',0,0,0 UNION ALL
Select '2930300000','Thiuram mono, di or tetrasulphides','KGM',0,0,0 UNION ALL
Select '2930400000','Methionine','KGM',0,0,0 UNION ALL
Select '2930600000','2(N,NDiethylamino)ethanethiol ','KGM',0,0,0 UNION ALL
Select '2930700000','Bis(2hydroxyethyl)sulfide (thiodiglycol (INN))','KGM',0,0,0 UNION ALL
Select '2930800000','Aldicarb (ISO), captafol (ISO) and methamidophos (ISO)','KGM',0,0,0 UNION ALL

Select '2930901000','Dithiocarbonates','KGM',0,0,0 UNION ALL
Select '2930909000','Other','KGM',0,0,0 UNION ALL


Select '2931101000','Tetramethyl lead ','KGM',0,0,0 UNION ALL
Select '2931102000','Tetraethyl lead','KGM',0,0,0 UNION ALL
Select '2931200000','Tributyltin compounds','KGM',0,0,0 UNION ALL

Select '2931310000','Dimethyl methylphosphonate ','KGM',0,0,0 UNION ALL
Select '2931320000','Dimethyl propylphosphonate ','KGM',0,0,0 UNION ALL
Select '2931330000','Diethyl ethylphosphonate ','KGM',0,0,0 UNION ALL
Select '2931340000','Sodium 3(trihydroxysilyl)propyl methylphosphonate ','KGM',0,0,0 UNION ALL
Select '2931350000','2,4,6Tripropyl1,3,5,2,4,6trioxatriphosphinane 2,4,6trioxide ','KGM',0,0,0 UNION ALL
Select '2931360000','(5Ethyl2methyl2oxido1,3,2dioxaphosphinan5yl)methyl methyl methylphosphonate ','KGM',0,0,0 UNION ALL
Select '2931370000','Bis[(5ethyl2methyl2oxido1,3,2dioxaphosphinan5yl)methyl] methylphosphonate','KGM',0,0,0 UNION ALL
Select '2931380000','Salt of methylphosphonic acid and (aminoiminomethyl)urea (1 : 1) ','KGM',0,0,0 UNION ALL
Select '2931390000','Other','KGM',0,0,0 UNION ALL


Select '2931902100','N(phosphonomethyl) glycine','KGM',0,0,0 UNION ALL
Select '2931902200','Salts of N(phosphonomethyl) glycine','KGM',5,0,0 UNION ALL
Select '2931903000','Ethephone','KGM',0,0,0 UNION ALL

Select '2931904100','In liquid form     ','KGM',0,0,0 UNION ALL
Select '2931904900','Other','KGM',5,0,0 UNION ALL
Select '2931905000','Dimethyltin dichloride ','KGM',0,0,0 UNION ALL
Select '2931909000','Other','KGM',0,0,0 UNION ALL


Select '2932110000','Tetrahydrofuran','KGM',0,0,0 UNION ALL
Select '2932120000','2Furaldehyde (furfuraldehyde)','KGM',0,0,0 UNION ALL
Select '2932130000','Furfuryl alcohol and tetrahydrofurfuryl alcohol','KGM',0,0,0 UNION ALL
Select '2932140000','Sucralose','KGM',0,0,0 UNION ALL
Select '2932190000','Other ','KGM',0,0,0 UNION ALL

Select '2932201000','Coumarin N(1,2Benzopyrone) methylcoumarins and ethylcoumarin','KGM',0,0,0 UNION ALL
Select '2932209000','Other','KGM',0,0,0 UNION ALL

Select '2932910000','Isosafrole','KGM',0,0,0 UNION ALL
Select '2932920000','1(1,3Benzodioxol5yl)propan2one','KGM',0,0,0 UNION ALL
Select '2932930000','Piperonal','KGM',0,0,0 UNION ALL
Select '2932940000','Safrole','KGM',0,0,0 UNION ALL
Select '2932950000','Tetrahydrocannabinols (all isomers) ','KGM',0,0,0 UNION ALL

Select '2932991000','Carbofuran ','KGM',0,0,0 UNION ALL
Select '2932999000','Other ','KGM',0,0,0 UNION ALL


Select '2933110000','Phenazone (antipyrin) and its derivatives','KGM',0,0,0 UNION ALL
Select '2933190000','Other ','KGM',0,0,0 UNION ALL

Select '2933210000','Hydantoin and its derivatives','KGM',0,0,0 UNION ALL
Select '2933290000','Other ','KGM',0,0,0 UNION ALL

Select '2933310000','Pyridine and its salts ','KGM',0,0,0 UNION ALL
Select '2933320000','Piperidine and its salts','KGM',0,0,0 UNION ALL
Select '2933330000','Alfentanil (INN), anileridine (INN), bezitramide (INN), bromazepam (INN), difenoxin (INN), diphenoxylate (INN), dipipanone (INN), fentanyl (INN), ketobemidone (INN), methylphenidate (INN), pentazocine (INN), pethidine (INN), pethidine (INN) intermediate A, phencyclidine (INN) (PCP), phenoperidine (INN), pipradrol (INN), piritramide (INN), propiram (INN) and trimeperidine (INN); salts thereof','KGM',0,0,0 UNION ALL

Select '2933391000','Chlorpheniramine and isoniazid','KGM',0,0,0 UNION ALL
Select '2933393000','Paraquat salts','KGM',0,0,0 UNION ALL
Select '2933399000','Other','KGM',0,0,0 UNION ALL

Select '2933410000','Levorphanol (INN) and its salts ','KGM',0,0,0 UNION ALL

Select '2933491000','Dextromethorphan','KGM',0,0,0 UNION ALL
Select '2933499000','Other','KGM',0,0,0 UNION ALL

Select '2933520000','Malonylurea (barbituric acid) and its salts','KGM',0,0,0 UNION ALL
Select '2933530000','Allobarbital (INN), amobarbital (INN), barbital (INN), butalbital (INN), butobarbital, cyclobarbital (INN), methylphenobarbital (INN), pentobarbital (INN), phenobarbital (INN), secbutabarbital (INN), secobarbital (INN) and vinylbital (INN); salts thereof ','KGM',0,0,0 UNION ALL
Select '2933540000','Other derivatives of malonylurea (barbituric acid); salts thereof','KGM',0,0,0 UNION ALL
Select '2933550000','Loprazolam (INN), mecloqualone (INN), methaqualone (INN) and zipeprol (INN); salts thereof','KGM',0,0,0 UNION ALL

Select '2933591000','Diazinon ','KGM',0,0,0 UNION ALL
Select '2933599000','Other ','KGM',0,0,0 UNION ALL

Select '2933610000','Melamine','KGM',0,0,0 UNION ALL
Select '2933690000','Other','KGM',0,0,0 UNION ALL

Select '2933710000','6Hexanelactam (epsiloncaprolactam)','KGM',0,0,0 UNION ALL
Select '2933720000','Clobazam (INN) and methyprylon (INN)','KGM',0,0,0 UNION ALL
Select '2933790000','Other lactams','KGM',0,0,0 UNION ALL

Select '2933910000','Alprazolam (INN), camazepam (INN), chlordiazepoxide (INN), clonazepam (INN), clorazepate, delorazepam (INN), diazepam (INN), estazolam (INN), ethyl loflazepate (INN), fludiazepam (INN), flunitrazepam (INN), flurazepam (INN), halazepam (INN), lorazepam (INN), lormetazepam (INN), mazindol (INN), medazepam (INN), midazolam (INN), nimetazepam (INN), nitrazepam (INN), nordazepam (INN), oxazepam (INN), pinazepam (INN), prazepam (INN), pyrovalerone (INN), temazepam (INN), tetrazepam (INN) and triazolam (INN); salts thereof','KGM',0,0,0 UNION ALL
Select '2933920000','Azinphosmethyl (ISO)','KGM',0,0,0 UNION ALL

Select '2933991000','Mebendazole and parbendazole','KGM',0,0,0 UNION ALL
Select '2933999000','Other ','KGM',0,0,0 UNION ALL

Select '2934100000','Compounds containing an unfused thiazole ring (whether or not hydrogenated) in the structure','KGM',0,0,0 UNION ALL
Select '2934200000','Compounds containing in the structure a benzothiazole ringsystem (whether or not hydrogenated), not further fused','KGM',0,0,0 UNION ALL
Select '2934300000','Compounds containing in the structure a phenothiazine ringsystem (whether or not hydrogenated), not further fused','KGM',0,0,0 UNION ALL

Select '2934910000','Aminorex (INN), brotizolam (INN), clotiazepam (INN), cloxazolam (INN), dextromoramide (INN), haloxazolam (INN), ketazolam (INN), mesocarb (INN), oxazolam (INN), pemoline (INN), phendimetrazine (INN), phenmetrazine (INN) and sufentanil (INN); salts thereof','KGM',0,0,0 UNION ALL

Select '2934991000','Nucleic acid and its salts ','KGM',0,0,0 UNION ALL
Select '29349920','Sultones; sultams; diltiazem:','',0,0,0 UNION ALL
Select '2934992010','Sultones and sultams','KGM',0,0,0 UNION ALL
Select '2934992020','Diltiazem','KGM',0,0,0 UNION ALL
Select '2934993000','6Aminopenicillanic acid','KGM',0,0,0 UNION ALL
Select '2934994000','3Azido3deoxythymidine','KGM',0,0,0 UNION ALL
Select '2934995000','Oxadiazon, with a purity of 94% or more','KGM',0,0,0 UNION ALL
Select '2934999000','Other','KGM',0,0,0 UNION ALL

Select '2935100000','NMethylperfluorooctane sulphonamide ','KGM',0,0,0 UNION ALL
Select '2935200000','NEthylperfluorooctane sulphonamide ','KGM',0,0,0 UNION ALL
Select '2935300000','NEthylN(2hydroxyethyl) perfluorooctane sulphonamide ','KGM',0,0,0 UNION ALL
Select '2935400000','N(2Hydroxyethyl)Nmethylperfluorooctane sulphonamide ','KGM',0,0,0 UNION ALL
Select '2935500000','Other perfluorooctane sulphonamides','KGM',0,0,0 UNION ALL
Select '2935900000','Other','KGM',0,0,0 UNION ALL



Select '2936210000','Vitamins A and their derivatives','KGM',0,0,0 UNION ALL
Select '2936220000','Vitamin B1 and its derivatives','KGM',0,0,0 UNION ALL
Select '2936230000','Vitamin B2 and its derivatives','KGM',0,0,0 UNION ALL
Select '2936240000','D  or DLPantothenic acid (Vitamin B3 or Vitamin B5) and its derivatives','KGM',0,0,0 UNION ALL
Select '2936250000','Vitamin B6 and its derivatives','KGM',0,0,0 UNION ALL
Select '2936260000','Vitamin B12 and its derivatives','KGM',0,0,0 UNION ALL
Select '2936270000','Vitamin C and its derivatives','KGM',0,0,0 UNION ALL
Select '2936280000','Vitamin E and its derivatives','KGM',0,0,0 UNION ALL
Select '2936290000','Other vitamins and their derivatives','KGM',0,0,0 UNION ALL
Select '2936900000','Other, including natural concentrates','KGM',0,0,0 UNION ALL


Select '2937110000','Somatotropin, its derivatives and structural analogues ','KGM',0,0,0 UNION ALL
Select '2937120000','Insulin and its salts ','KGM',0,0,0 UNION ALL
Select '2937190000','Other ','KGM',0,0,0 UNION ALL

Select '2937210000','Cortisone, hydrocortisone, prednisone (dehydrocortisone) and prednisolone (dehydrohydrocortisone)','KGM',0,0,0 UNION ALL
Select '2937220000','Halogenated derivatives of corticosteroidal hormones ','KGM',0,0,0 UNION ALL
Select '2937230000','Oestrogens and progestogens ','KGM',0,0,0 UNION ALL
Select '29372900','Other:','',0,0,0 UNION ALL
Select '2937290010','Adrenal cortical hormones','KGM',0,0,0 UNION ALL
Select '2937290090','Other','KGM',0,0,0 UNION ALL
Select '2937500000','Prostaglandins, thromboxanes and leukotrienes, their derivatives and structural analogues ','KGM',0,0,0 UNION ALL

Select '2937901000','Of oxygenfunction aminocompounds','KGM',0,0,0 UNION ALL
Select '2937902000','Epinephrine; aminoacid derivatives','KGM',0,0,0 UNION ALL
Select '2937909000','Other','KGM',0,0,0 UNION ALL


Select '2938100000','Rutoside (rutin) and its derivatives','KGM',0,0,0 UNION ALL
Select '2938900000','Other','KGM',0,0,0 UNION ALL



Select '2939111000','Concentrates of poppy straw and salts thereof','KGM',0,0,0 UNION ALL
Select '2939119000','Other ','KGM',0,0,0 UNION ALL
Select '2939190000','Other ','KGM',0,0,0 UNION ALL

Select '2939201000','Quinine and its salts','KGM',0,0,0 UNION ALL
Select '2939209000','Other','KGM',0,0,0 UNION ALL
Select '2939300000','Caffeine and its salts','KGM',0,0,0 UNION ALL

Select '2939410000','Ephedrine and its salts','KGM',0,0,0 UNION ALL
Select '2939420000','Pseudoephedrine (INN) and its salts','KGM',0,0,0 UNION ALL
Select '2939430000','Cathine (INN) and its salts ','KGM',0,0,0 UNION ALL
Select '2939440000','Norephedrine and its salts','KGM',0,0,0 UNION ALL

Select '2939491000','Phenylpropanolamine (PPA)','KGM',0,0,0 UNION ALL
Select '2939499000','Other','KGM',0,0,0 UNION ALL

Select '2939510000','Fenetylline (INN) and its salts','KGM',0,0,0 UNION ALL
Select '2939590000','Other ','KGM',0,0,0 UNION ALL

Select '2939610000','Ergometrine (INN) and its salts','KGM',0,0,0 UNION ALL
Select '2939620000','Ergotamine(INN) and its salts','KGM',0,0,0 UNION ALL
Select '2939630000','Lysergic acid and its salts','KGM',0,0,0 UNION ALL
Select '2939690000','Other','KGM',0,0,0 UNION ALL

Select '2939710000','Cocaine, ecgonine, levometamfetamine, metamfetamine (INN), metamfetamine racemate; salts, esters and other derivatives thereof','KGM',0,0,0 UNION ALL
Select '29397900','Other:','',0,0,0 UNION ALL
Select '2939790010','Nicotine sulphate','KGM',0,0,0 UNION ALL
Select '2939790090','Other ','KGM',0,0,0 UNION ALL
Select '2939800000','Other','KGM',0,0,0 UNION ALL


Select '2940000000','Sugars, chemically pure, other than sucrose, lactose, maltose, glucose and fructose; sugar ethers, sugar acetals and sugar esters, and their salts, other than products of heading 29.37, 29.38 or 29.39.','KGM',0,0,0 UNION ALL



Select '2941101100','Nonsterile ','KGM',0,0,0 UNION ALL
Select '2941101900','Other ','KGM',0,0,0 UNION ALL
Select '2941102000','Ampicillin and its salts ','KGM',0,0,0 UNION ALL
Select '2941109000','Other ','KGM',0,0,0 UNION ALL
Select '2941200000','Streptomycins and their derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2941300000','Tetracyclines and their derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2941400000','Chloramphenicol and its derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2941500000','Erythromycin and its derivatives; salts thereof','KGM',0,0,0 UNION ALL
Select '2941900000','Other','KGM',0,0,0 UNION ALL

Select '2942000000','Other organic compounds.','KGM',0,0,0 UNION ALL

Select '3001200000','Extracts of glands or other organs or of their secretions','KGM',0,0,0 UNION ALL
Select '3001900000','Other ','KGM',0,0,0 UNION ALL


Select '3002110000','Malaria diagnostic test kits','KGM',0,0,0 UNION ALL

Select '3002121000','Antisera; plasma protein solutions; haemoglobin powder','KGM',0,0,0 UNION ALL
Select '3002129000','Other','KGM',0,0,0 UNION ALL
Select '3002130000','Immunological products, unmixed, not put up in measured doses or in forms or packings for retail sale','KGM',0,0,0 UNION ALL
Select '3002140000','Immunological products, mixed, not put up in measured doses or in forms or packings for retail sale','KGM',0,0,0 UNION ALL
Select '3002150000','Immunological products, put up in measured doses or in forms or packings for retail sale','KGM',0,0,0 UNION ALL
Select '3002190000','Other','KGM',0,0,0 UNION ALL

Select '3002201000','Tetanus toxoid','KGM',0,0,0 UNION ALL
Select '3002202000','Pertusis, measles, meningitis or polio vaccines ','KGM',0,0,0 UNION ALL
Select '3002209000','Other','KGM',0,0,0 UNION ALL
Select '3002300000','Vaccines for veterinary medicine','KGM',0,0,0 UNION ALL
Select '3002900000','Other','KGM',0,0,0 UNION ALL


Select '30031010','Containing amoxicillin (INN) or its salts:','',0,0,0 UNION ALL
Select '3003101010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003101090','Other','KGM',0,0,0 UNION ALL
Select '30031020','Containing ampicillin (INN) or its salts:','',0,0,0 UNION ALL
Select '3003102010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003102090','Other','KGM',0,0,0 UNION ALL
Select '30031090','Other:','',0,0,0 UNION ALL
Select '3003109010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003109090','Other','KGM',0,0,0 UNION ALL
Select '30032000','Other, containing antibiotics:','',0,0,0 UNION ALL
Select '3003200010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003200090','Other','KGM',0,0,0 UNION ALL

Select '3003310000','Containing insulin     ','KGM',0,0,0 UNION ALL
Select '30033900','Other:','',0,0,0 UNION ALL
Select '3003390010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003390090','Other','KGM',0,0,0 UNION ALL

Select '30034100','Containing ephedrine or its salts:','',0,0,0 UNION ALL
Select '3003410010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003410090','Other','KGM',0,0,0 UNION ALL
Select '30034200','Containing pseudoephedrine (INN) or its salts:','',0,0,0 UNION ALL
Select '3003420010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003420090','Other','KGM',0,0,0 UNION ALL
Select '30034300','Containing norephedrine or its salts:','',0,0,0 UNION ALL
Select '3003430010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003430090','Other','KGM',0,0,0 UNION ALL
Select '30034900','Other:','',0,0,0 UNION ALL
Select '3003490010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003490090','Other','KGM',0,0,0 UNION ALL
Select '30036000','Other, containing antimalarial active principles described in Subheading Note 2 to this Chapter:','',0,0,0 UNION ALL
Select '3003600010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003600090','Other','KGM',0,0,0 UNION ALL
Select '30039000','Other','',0,0,0 UNION ALL
Select '3003900010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3003900090','Other','KGM',0,0,0 UNION ALL



Select '3004101500','Containing penicillin G (excluding penicillin G benzathine), phenoxymethyl penicillin or salts thereof','KGM',0,0,0 UNION ALL
Select '3004101600','Containing ampicillin, amoxycillin or salts thereof, of a kind taken orally','KGM',0,0,0 UNION ALL
Select '3004101900','Other','KGM',0,0,0 UNION ALL

Select '3004102100','In ointment form','KGM',0,0,0 UNION ALL
Select '3004102900','Other','KGM',0,0,0 UNION ALL

Select '30042010','Containing gentamycin, lincomycin, sulfamethoxazole or their derivatives, of a kind taken orally or in ointment form:','',0,0,0 UNION ALL
Select '3004201010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004201090','Other','KGM',0,0,0 UNION ALL

Select '30042031','Of a kind taken orally:','',0,0,0 UNION ALL
Select '3004203110','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004203190','Other','KGM',0,0,0 UNION ALL
Select '30042032','In ointment form:','',0,0,0 UNION ALL
Select '3004203210','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004203290','Other ','KGM',0,0,0 UNION ALL
Select '30042039','Other:','',0,0,0 UNION ALL
Select '3004203910','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004203990','Other ','KGM',0,0,0 UNION ALL

Select '30042071','Of a kind taken orally or in ointment form:','',0,0,0 UNION ALL
Select '3004207110','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004207190','Other','KGM',0,0,0 UNION ALL
Select '30042079','Other:','',0,0,0 UNION ALL
Select '3004207910','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004207990','Other','KGM',0,0,0 UNION ALL

Select '30042091','Of a kind taken orally or in ointment form:','',0,0,0 UNION ALL
Select '3004209110','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004209190','Other','KGM',0,0,0 UNION ALL
Select '30042099','Other:','',0,0,0 UNION ALL
Select '3004209910','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004209990','Other','KGM',0,0,0 UNION ALL

Select '3004310000','Containing insulin','KGM',0,0,0 UNION ALL

Select '3004321000','Containing dexamethasone or their derivatives','KGM',0,0,0 UNION ALL
Select '3004324000','Containing hydrocortisone sodium succinate or fluocinolone acetonide','KGM',0,0,0 UNION ALL
Select '3004329000','Other','KGM',0,0,0 UNION ALL
Select '3004390000','Other','KGM',0,0,0 UNION ALL

Select '3004410000','Containing ephedrine or its salts','KGM',0,0,0 UNION ALL
Select '3004420000','Containing pseudoephedrine (INN) or its salts','KGM',0,0,0 UNION ALL
Select '3004430000','Containing norephedrine or its salts','KGM',0,0,0 UNION ALL

Select '30044910','Containing morphine or its derivatives:','',0,0,0 UNION ALL
Select '3004491010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004491090','Other      ','KGM',0,0,0 UNION ALL
Select '3004495000','Containing papaverine or berberine, of a kind taken orally','KGM',0,0,0 UNION ALL
Select '3004496000','Containing theophyline, of a kind taken orally','KGM',0,0,0 UNION ALL
Select '3004497000','Containing atropine sulphate','KGM',0,0,0 UNION ALL
Select '3004498000','Containing quinine hydrochloride or dihydroquinine chloride, for injection; Containing quinine sulphate or bisulphate, of a kind taken orally','KGM',0,0,0 UNION ALL
Select '30044990','Other:','',0,0,0 UNION ALL
Select '3004499010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004499090','Other','KGM',0,0,0 UNION ALL

Select '3004501000','Of a kind suitable for children, in syrup form','KGM',0,0,0 UNION ALL

Select '3004502100','Of a kind taken orally','KGM',0,0,0 UNION ALL
Select '3004502900','Other','KGM',0,0,0 UNION ALL

Select '3004509100','Containing vitamin A, B or C','KGM',0,0,0 UNION ALL
Select '3004509900','Other','KGM',0,0,0 UNION ALL

Select '3004601000','Containing artemisinin combined with other pharmaceutical active ingredients','KGM',0,0,0 UNION ALL
Select '3004602000','Containing artesunate or chloroquine','KGM',0,0,0 UNION ALL
Select '3004609000','Other','KGM',0,0,0 UNION ALL

Select '3004901000','Transdermal therapeutic system patches for the treatment of cancer or heart diseases','KGM',0,0,0 UNION ALL
Select '3004902000','Closed sterile water for inhalation, pharmaceutical grade','KGM',0,0,0 UNION ALL
Select '30049030','Antiseptics:','',0,0,0 UNION ALL
Select '3004903010','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004903090','Other','KGM',0,0,0 UNION ALL

Select '30049041','Containing procaine hydrochloride:','',0,0,0 UNION ALL
Select '3004904110','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004904190','Other','KGM',0,0,0 UNION ALL
Select '30049049','Other:','',0,0,0 UNION ALL
Select '3004904910','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004904990','Other','KGM',0,0,0 UNION ALL

Select '30049051','Containing acetylsalicylic acid, paracetamol or dipyrone (INN), of a kind taken orally:','',0,0,0 UNION ALL
Select '3004905110','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905190','Other','KGM',0,0,0 UNION ALL
Select '30049052','Containing chlorpheniramine maleate:','',0,0,0 UNION ALL
Select '3004905210','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905290','Other','KGM',0,0,0 UNION ALL
Select '30049053','Containing diclofenac, of a kind taken orally:','',0,0,0 UNION ALL
Select '3004905310','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905390','Other','KGM',0,0,0 UNION ALL
Select '30049054','Containing piroxicam (INN) or ibuprofen:','',0,0,0 UNION ALL
Select '3004905410','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905490','Other','KGM',0,0,0 UNION ALL
Select '30049055','Other, in liniment form:','',0,0,0 UNION ALL
Select '3004905510','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905590','Other','KGM',0,0,0 UNION ALL
Select '30049059','Other: ','',0,0,0 UNION ALL
Select '3004905910','Veterinary medicaments','KGM',0,0,0 UNION ALL
Select '3004905990','Other ','KGM',0,0,0 UNION ALL

Select '3004906200','Containing primaquine','KGM',0,0,0 UNION ALL
Select '3004906400','Containing artemisinin other than of subheading 3004.60.10','KGM',0,0,0 UNION ALL

Select '3004906500','Herbal medicaments','KGM',0,0,0 UNION ALL
Select '3004906900','Other','KGM',0,0,0 UNION ALL

Select '3004907100','Containing piperazine or mebendazole (INN)','KGM',0,0,0 UNION ALL

Select '3004907200','Herbal medicaments','KGM',0,0,0 UNION ALL
Select '3004907900','Other','KGM',0,0,0 UNION ALL

Select '3004908100','Containing deferoxamine, for injection','KGM',0,0,0 UNION ALL
Select '3004908200','Anti HIV/AIDS medicaments','KGM',0,0,0 UNION ALL
Select '3004908900','Other','KGM',0,0,0 UNION ALL

Select '3004909100','Containing sodium chloride or glucose, for infusion','KGM',0,0,0 UNION ALL
Select '3004909200','Containing sorbitol or salbutamol, for infusion','KGM',0,0,0 UNION ALL
Select '3004909300','Containing sorbitol or salbutamol, in other forms','KGM',0,0,0 UNION ALL
Select '3004909400','Containing cimetidine (INN) or ranitidine (INN) other than for injection','KGM',0,0,0 UNION ALL
Select '3004909500','Containing phenobarbital, diazepam or chlorpromazine, other than for injection or infusion','KGM',0,0,0 UNION ALL
Select '3004909600','Nasaldrop medicaments containing naphazoline, xylometazoline or oxymetazoline','KGM',0,0,0 UNION ALL

Select '3004909800','Herbal medicaments','KGM',0,0,0 UNION ALL
Select '3004909900','Other','KGM',0,0,0 UNION ALL


Select '3005101000','Impregnated or coated with pharmaceutical substances','KGM',0,0,0 UNION ALL
Select '3005109000','Other','KGM',0,0,0 UNION ALL

Select '3005901000','Bandages','KGM',0,0,0 UNION ALL
Select '3005902000','Gauze','KGM',0,0,0 UNION ALL
Select '3005909000','Other','KGM',0,0,0 UNION ALL


Select '3006101000','Sterile absorbable surgical or dental yarn; sterile surgical or dental adhesion barriers, whether or not absorbable','KGM',0,0,0 UNION ALL
Select '3006109000','Other','KGM',0,0,0 UNION ALL
Select '3006200000','Bloodgrouping reagents','KGM',0,0,0 UNION ALL

Select '3006301000','Barium sulphate, of a kind taken orally ','KGM',0,0,0 UNION ALL
Select '3006302000','Reagents of microbial origin, of a kind suitable for veterinary  biological diagnosis ','KGM',0,0,0 UNION ALL
Select '3006303000','Other microbial diagnostic reagents ','KGM',0,0,0 UNION ALL
Select '3006309000','Other ','KGM',0,0,0 UNION ALL

Select '30064010','Dental cements and other dental fillings:','',0,0,0 UNION ALL
Select '3006401010','Dental alloys','KGM',0,0,0 UNION ALL
Select '3006401090','Other','KGM',0,0,0 UNION ALL
Select '3006402000','Bone reconstruction cements','KGM',0,0,0 UNION ALL
Select '3006500000','Firstaid boxes and kits','KGM',0,0,0 UNION ALL
Select '3006600000','Chemical contraceptive preparations based on hormones, on other products of heading 29.37 or on spermicides','KGM',0,0,0 UNION ALL
Select '3006700000','Gel preparations designed to be used in human or veterinary medicine as a lubricant for parts of the body for surgical operations or physical examinations or as a coupling agent between the body and medical instruments','KGM',0,0,0 UNION ALL

Select '3006910000','Appliances identifiable for ostomy use','KGM',0,0,0 UNION ALL

Select '3006921000','Of medicaments for the treatment of cancer, HIV/AIDS or other intractable diseases','KGM',0,0,0 UNION ALL
Select '3006929000','Other','KGM',0,0,0 UNION ALL

Select '31010010','Of solely vegetable origin:','',0,0,0 UNION ALL
Select '3101001010','Chemically treated','KGM',5,0,0 UNION ALL
Select '3101001090','Other','KGM',0,0,0 UNION ALL

Select '3101009200','Of animal origin (other than guano), chemically treated','KGM',5,0,0 UNION ALL
Select '3101009900','Other:','',0,0,0 UNION ALL

Select '3101009911','Chemically treated','KGM',0,0,0 UNION ALL
Select '3101009919','Other','KGM',0,0,0 UNION ALL
Select '3101009990','Other','KGM',0,0,0 UNION ALL

Select '3102100000','Urea, whether or not in aqueous solution','KGM',0,0,0 UNION ALL

Select '3102210000','Ammonium sulphate','KGM',0,0,0 UNION ALL
Select '3102290000','Other','KGM',0,0,0 UNION ALL
Select '3102300000','Ammonium nitrate, whether or not in aqueous solution','KGM',0,0,0 UNION ALL
Select '3102400000','Mixtures of ammonium nitrate with calcium carbonate or other inorganic nonfertilising substances','KGM',0,0,0 UNION ALL
Select '3102500000','Sodium nitrate','KGM',0,0,0 UNION ALL
Select '3102600000','Double salts and mixtures of calcium nitrate and ammonium  nitrate','KGM',0,0,0 UNION ALL
Select '3102800000','Mixtures of urea and ammonium nitrate in aqueous or ammoniacal solution','KGM',0,0,0 UNION ALL
Select '3102900000','Other, including mixtures not specified in the foregoing  subheadings','KGM',0,0,0 UNION ALL



Select '3103111000','Feed grade','KGM',0,0,0 UNION ALL
Select '3103119000','Other','KGM',0,0,0 UNION ALL

Select '3103191000','Feed grade','KGM',0,0,0 UNION ALL
Select '3103199000','Other','KGM',0,0,0 UNION ALL

Select '3103901000','Calcined phosphatic fertiliser','KGM',0,0,0 UNION ALL
Select '3103909000','Other','KGM',0,0,0 UNION ALL

Select '3104200000','Potassium chloride','KGM',0,0,0 UNION ALL
Select '3104300000','Potassium sulphate','KGM',0,0,0 UNION ALL
Select '3104900000','Other','KGM',0,0,0 UNION ALL


Select '3105101000','Superphosphates and calcined phosphatic fertilisers','KGM',0,0,0 UNION ALL
Select '3105102000','Mineral or chemical fertilisers containing two or three of the fertilising elements nitrogen, phosphorus and potassium','KGM',0,0,0 UNION ALL
Select '3105109000','Other','KGM',0,0,0 UNION ALL
Select '3105200000','Mineral or chemical fertilisers containing the three fertilising elements nitrogen, phosphorus and potassium','KGM',0,0,0 UNION ALL
Select '3105300000','Diammonium hydrogenorthophosphate (diammonium  phosphate)','KGM',0,0,0 UNION ALL
Select '3105400000','Ammonium dihydrogenorthophosphate (monoammonium phosphate) and mixtures thereof with diammonium hydrogenorthophosphate (diammonium phosphate)','KGM',0,0,0 UNION ALL

Select '3105510000','Containing nitrates and phosphates','KGM',0,0,0 UNION ALL
Select '3105590000','Other','KGM',0,0,0 UNION ALL
Select '3105600000','Mineral or chemical fertilisers containing the two fertilising elements phosphorus and potassium','KGM',0,0,0 UNION ALL
Select '3105900000','Other','KGM',0,0,0 UNION ALL

Select '3201100000','Quebracho extract','KGM',0,0,0 UNION ALL
Select '3201200000','Wattle extract','KGM',0,0,0 UNION ALL
Select '3201900000','Other','KGM',0,0,0 UNION ALL

Select '3202100000','Synthetic organic tanning substances','KGM',0,0,0 UNION ALL
Select '3202900000','Other','KGM',0,0,0 UNION ALL


Select '3203001000','Of a kind used in the food or drink industries','KGM',0,0,0 UNION ALL
Select '3203009000','Other','KGM',0,0,0 UNION ALL



Select '3204111000','Crude','KGM',0,0,0 UNION ALL
Select '3204119000','Other','KGM',0,0,0 UNION ALL

Select '3204121000','Acid dyes','KGM',0,0,0 UNION ALL
Select '3204129000','Other','KGM',0,0,0 UNION ALL
Select '3204130000','Basic dyes and preparations based thereon','KGM',0,0,0 UNION ALL
Select '3204140000','Direct dyes and preparations based thereon','KGM',0,0,0 UNION ALL
Select '3204150000','Vat dyes (including those usable in that state as pigments)  and preparations based thereon','KGM',0,0,0 UNION ALL
Select '3204160000','Reactive dyes and preparations based thereon','KGM',0,0,0 UNION ALL

Select '3204171000','Synthetic organic pigment in powder form','KGM',0,0,0 UNION ALL
Select '3204179000','Other','KGM',0,0,0 UNION ALL
Select '3204190000','Other, including mixtures of colouring matter of two or more of the subheadings 3204.11 to 3204.19','KGM',0,0,0 UNION ALL
Select '3204200000','Synthetic organic products of a kind used as fluorescent brightening agents','KGM',0,0,0 UNION ALL
Select '3204900000','Other','KGM',0,0,0 UNION ALL

Select '3205000000','Colour lakes; preparations as specified in Note 3 to this Chapter based on colour lakes.','KGM',0,0,0 UNION ALL



Select '3206111000','Pigments','KGM',15,0,0 UNION ALL
Select '3206119000','Other','KGM',15,0,0 UNION ALL

Select '3206191000','Pigments','KGM',15,0,0 UNION ALL
Select '3206199000','Other','KGM',15,0,0 UNION ALL

Select '3206201000','Chrome yellow, chrome green, molybdate orange or red based on chromium compounds','KGM',0,0,0 UNION ALL
Select '3206209000','Other','KGM',0,0,0 UNION ALL


Select '3206411000','Preparations ','KGM',0,0,0 UNION ALL
Select '3206419000','Other','KGM',0,0,0 UNION ALL

Select '3206421000','Preparations','KGM',0,0,0 UNION ALL
Select '3206429000','Other','KGM',0,0,0 UNION ALL

Select '3206491000','Preparations','KGM',0,0,0 UNION ALL
Select '3206499000','Other','KGM',0,0,0 UNION ALL

Select '3206501000','Preparations','KGM',0,0,0 UNION ALL
Select '3206509000','Other','KGM',0,0,0 UNION ALL

Select '3207100000','Prepared pigments, prepared opacifiers, prepared colours and similar preparations','KGM',0,0,0 UNION ALL

Select '3207201000','Enamel frits','KGM',0,0,0 UNION ALL
Select '3207209000','Other','KGM',0,0,0 UNION ALL
Select '3207300000','Liquid lustres and similar preparations','KGM',0,0,0 UNION ALL
Select '3207400000','Glass frit and other glass, in the form of powder, granules or  flakes','KGM',0,0,0 UNION ALL



Select '3208101100','Of a kind used in dentistry','KGM',25,0,0 UNION ALL
Select '3208101900','Other','KGM',25,0,0 UNION ALL
Select '3208102000','Antifouling and/or anticorrosive paints for ships"hulls','KGM',25,0,0 UNION ALL
Select '3208109000','Other','KGM',25,0,0 UNION ALL

Select '3208204000','Antifouling and/or anticorrosive paints for ships"hulls','KGM',25,0,0 UNION ALL
Select '3208207000','Varnishes (including lacquers), of a kind used in dentistry','KGM',25,0,0 UNION ALL
Select '3208209000','Other','KGM',25,0,0 UNION ALL


Select '3208901100','Of a kind used in dentistry','KGM',25,0,0 UNION ALL
Select '3208901900','Other','KGM',25,0,0 UNION ALL

Select '3208902100','Of a kind used in dentistry','KGM',25,0,0 UNION ALL
Select '3208902900','Other','KGM',25,0,0 UNION ALL
Select '3208903000','Antifouling and/or anticorrosive paints for ships"hulls','KGM',25,0,0 UNION ALL
Select '3208909000','Other','KGM',25,0,0 UNION ALL


Select '3209101000','Varnishes (including lacquers)','KGM',25,0,0 UNION ALL
Select '3209104000','Leather paints','KGM',25,0,0 UNION ALL
Select '3209105000','Antifouling or anticorrosive paints for ships'' hulls','KGM',25,0,0 UNION ALL
Select '3209109000','Other','KGM',25,0,0 UNION ALL
Select '3209900000','Other','KGM',25,0,0 UNION ALL


Select '3210001000','Varnishes (including lacquers)','KGM',25,0,0 UNION ALL
Select '3210002000','Distempers','KGM',25,0,0 UNION ALL
Select '3210003000','Prepared water pigments of a kind used for finishing leather','KGM',25,0,0 UNION ALL

Select '3210009100','Antifouling and/or anticorrosive paints for ships"hulls','KGM',25,0,0 UNION ALL
Select '3210009900','Other','KGM',25,0,0 UNION ALL

Select '3211000000','Prepared driers.','KGM',0,0,0 UNION ALL

Select '3212100000','Stamping foils','KGM',0,0,0 UNION ALL


Select '3212901100','Aluminium paste','KGM',0,0,0 UNION ALL
Select '3212901300','White lead dispersed in oil ','KGM',0,0,0 UNION ALL
Select '3212901400','Other, for the manufacture of leather paint','KGM',15,0,0 UNION ALL
Select '3212901900','Other ','KGM',15,0,0 UNION ALL

Select '3212902100','Of a kind used in the foods or drinks industries','KGM',15,0,0 UNION ALL
Select '3212902200','Other dyes','KGM',5,0,0 UNION ALL
Select '3212902900','Other','KGM',15,0,0 UNION ALL

Select '3213100000','Colours in sets','KGM',5,0,0 UNION ALL
Select '3213900000','Other','KGM',5,0,0 UNION ALL

Select '3214100000','Glaziers'' putty, grafting putty, resin cements, caulking compounds and other mastics; painters'' fillings','KGM',5,0,0 UNION ALL
Select '3214900000','Other','KGM',5,0,0 UNION ALL



Select '3215111000','Ultraviolet curable inks','KGM',25,0,0 UNION ALL
Select '3215119000','Other','KGM',25,0,0 UNION ALL
Select '3215190000','Other','KGM',25,0,0 UNION ALL

Select '3215901000','Carbon mass of a kind used to manufacture carbon paper','KGM',5,0,0 UNION ALL
Select '3215906000','Writing or drawing ink','KGM',0,0,0 UNION ALL
Select '3215907000','Ink of a kind suitable for use with duplicating machines of heading 84.72','KGM',25,0,0 UNION ALL
Select '3215909000','Other','KGM',5,0,0 UNION ALL


Select '3301120000','Of orange','KGM',0,0,0 UNION ALL
Select '3301130000','Of lemon','KGM',0,0,0 UNION ALL
Select '3301190000','Other','KGM',0,0,0 UNION ALL

Select '3301240000','Of peppermint (Mentha piperita)','KGM',0,0,0 UNION ALL
Select '3301250000','Of other mints','KGM',0,0,0 UNION ALL

Select '3301291000','Of lemon grass, citronella, nutmeg, cinnamon, ginger, cardamom, fennel or palmrose','KGM',0,0,0 UNION ALL
Select '3301292000','Of sandalwood','KGM',0,0,0 UNION ALL
Select '3301299000','Other','KGM',0,0,0 UNION ALL
Select '3301300000','Resinoids','KGM',0,0,0 UNION ALL

Select '3301901000','Aqueous distillates and aqueous solutions of essential oils  suitable for medicinal use','KGM',0,0,0 UNION ALL
Select '3301909000','Other','KGM',0,0,0 UNION ALL


Select '3302101000','Odoriferous alcoholic preparations of a kind used in the manufacture of alcoholic beverages, in liquid form','KGM',0,0,0 UNION ALL
Select '3302102000','Odoriferous alcoholic preparations of a kind used in the manufacture of alcoholic beverages, in other forms','KGM',0,0,0 UNION ALL
Select '3302109000','Other','KGM',0,0,0 UNION ALL
Select '3302900000','Other','KGM',0,0,0 UNION ALL

Select '3303000000','Perfumes and toilet waters.','KGM',0,0,0 UNION ALL

Select '3304100000','Lip makeup preparations','KGM',0,0,0 UNION ALL
Select '3304200000','Eye makeup preparations','KGM',0,0,0 UNION ALL
Select '3304300000','Manicure or pedicure preparations','KGM',0,0,0 UNION ALL

Select '3304910000','Powders, whether or not compressed','KGM',0,0,0 UNION ALL

Select '3304992000','Antiacne preparations','KGM',0,0,0 UNION ALL
Select '3304993000','Other face or skin creams and lotions','KGM',0,0,0 UNION ALL
Select '3304999000','Other','KGM',0,0,0 UNION ALL


Select '3305101000','Having antifungal properties','KGM',0,0,0 UNION ALL
Select '3305109000','Other','KGM',0,0,0 UNION ALL
Select '3305200000','Preparations for permanent waving or straightening','KGM',0,0,0 UNION ALL
Select '3305300000','Hair lacquers','KGM',0,0,0 UNION ALL
Select '3305900000','Other','KGM',0,0,0 UNION ALL


Select '3306101000','Powders and pastes for dental prophylaxis','KGM',0,0,0 UNION ALL
Select '3306109000','Other','KGM',0,0,0 UNION ALL
Select '3306200000','Yarn used to clean between the teeth (dental floss)','KGM',0,0,0 UNION ALL
Select '3306900000','Other','KGM',0,0,0 UNION ALL

Select '3307100000','Preshave, shaving or aftershave preparations','KGM',0,0,0 UNION ALL
Select '3307200000','Personal deodorants and antiperspirants','KGM',0,0,0 UNION ALL
Select '3307300000','Perfumed bath salts and other bath preparations','KGM',0,0,0 UNION ALL


Select '3307411000','Scented powders (incense) of a kind used during religious rites  ','KGM',0,0,0 UNION ALL
Select '3307419000','Other','KGM',0,0,0 UNION ALL

Select '3307491000','Room perfuming preparations, whether or not having disinfectant properties','KGM',0,0,0 UNION ALL
Select '3307499000','Other','KGM',0,0,0 UNION ALL

Select '3307901000','Animal toilet preparations','KGM',0,0,0 UNION ALL
Select '3307903000','Papers and tissues, impregnated or coated with perfume or cosmetics','KGM',0,0,0 UNION ALL
Select '3307904000','Other perfumery or cosmetics, including depilatories','KGM',0,0,0 UNION ALL
Select '3307905000','Contact lens or artificial eye solutions','KGM',0,0,0 UNION ALL
Select '3307909000','Other     ','KGM',0,0,0 UNION ALL



Select '3401114000','Medicated soap including disinfectant soap','KGM',0,0,0 UNION ALL
Select '3401115000','Other soap including bath soap','KGM',0,0,0 UNION ALL
Select '34011160','Other, of felt or nonwovens, impregnated, coated or covered with soap or detergent:','',0,0,0 UNION ALL
Select '3401116010','Of felt, perfumed','KGM',20,0,0 UNION ALL
Select '3401116020','Of nonwovens in packings for retail sale, perfumed ','KGM',0,0,0 UNION ALL
Select '3401116030','Other','KGM',20,0,0 UNION ALL
Select '34011190','Other:','',0,0,0 UNION ALL
Select '3401119010','Of paper, impregnated, coated or covered with soap or detergent','KGM',20,0,0 UNION ALL
Select '3401119090','Other','KGM',0,0,0 UNION ALL

Select '3401191000','Of felt or nonwovens, impregnated, coated or covered with soap or detergent ','KGM',0,0,0 UNION ALL
Select '34011990','Other:','',0,0,0 UNION ALL
Select '3401199010','Of paper, impregnated, coated or covered with soap or detergent','KGM',20,0,0 UNION ALL
Select '3401199090','Other','KGM',0,0,0 UNION ALL

Select '3401202000','Soap chips','KGM',0,0,0 UNION ALL

Select '3401209100','Of a kind used for flotation deinking of recycled paper','KGM',0,0,0 UNION ALL
Select '3401209900','Other','KGM',0,0,0 UNION ALL
Select '3401300000','Organic surfaceactive products and preparations for washing the skin, in the form of liquid or cream and put up for retail sale, whether or not containing soap','KGM',0,0,0 UNION ALL



Select '3402111000','Sulphated fatty alcohols','KGM',0,0,0 UNION ALL
Select '3402114000','Sulphonated alkylbenzene','KGM',0,0,0 UNION ALL
Select '3402119000','Other','KGM',0,0,0 UNION ALL
Select '3402120000','Cationic','KGM',0,0,0 UNION ALL

Select '3402131000','Hydroxylterminated polybutadiene','KGM',0,0,0 UNION ALL
Select '3402139000','Other','KGM',0,0,0 UNION ALL

Select '3402191000',' Of a kind suitable for use in fireextinguishing preparations','KGM',0,0,0 UNION ALL
Select '3402199000','Other','KGM',0,0,0 UNION ALL


Select '3402201400','Surface active preparations','KGM',0,0,0 UNION ALL
Select '3402201500','Washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL

Select '3402209400','Surface active preparations','KGM',0,0,0 UNION ALL
Select '3402209500','Washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL


Select '3402901100','Anionic wetting agents','KGM',0,0,0 UNION ALL
Select '3402901200','Anionic washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL
Select '3402901300','Other washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL
Select '3402901400','Other anionic surface active preparations ','KGM',0,0,0 UNION ALL
Select '3402901900','Other','KGM',0,0,0 UNION ALL

Select '3402909100','Anionic wetting agents','KGM',0,0,0 UNION ALL
Select '3402909200','Anionic washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL
Select '3402909300','Other washing preparations or cleaning preparations, including bleaching, cleansing or degreasing preparations','KGM',0,0,0 UNION ALL
Select '3402909400','Other anionic surface active preparations ','KGM',0,0,0 UNION ALL
Select '3402909900','Other','KGM',0,0,0 UNION ALL




Select '3403111100','Lubricating preparations','KGM',10,0,0 UNION ALL
Select '3403111900','Other','KGM',10,0,0 UNION ALL
Select '3403119000','Other','KGM',10,0,0 UNION ALL


Select '3403191100','Preparations for aircraft engines','KGM',10,0,0 UNION ALL
Select '3403191200','Other preparations containing silicone oil','KGM',10,0,0 UNION ALL
Select '3403191900','Other','KGM',10,0,0 UNION ALL
Select '3403199000','Other','KGM',10,0,0 UNION ALL



Select '3403911100','Preparations containing silicone oil','KGM',10,0,0 UNION ALL
Select '3403911900','Other','KGM',10,0,0 UNION ALL
Select '3403919000','Other','KGM',10,0,0 UNION ALL


Select '3403991100','Preparations for aircraft engines','KGM',10,0,0 UNION ALL
Select '3403991200','Other preparations containing silicone oil','KGM',10,0,0 UNION ALL
Select '3403991900','Other','KGM',10,0,0 UNION ALL
Select '3403999000','Other','KGM',10,0,0 UNION ALL

Select '3404200000','Of poly(oxyethylene) (polyethylene glycol)','KGM',0,0,0 UNION ALL

Select '3404901000','Of chemically modified lignite','KGM',0,0,0 UNION ALL
Select '3404909000','Other','KGM',0,0,0 UNION ALL

Select '3405100000','Polishes, creams and similar preparations for footwear or leather','KGM',10,0,0 UNION ALL
Select '3405200000','Polishes, creams and similar preparations for the maintenance of wooden furniture, floors or other woodwork','KGM',10,0,0 UNION ALL
Select '3405300000','Polishes and similar preparations for coachwork, other than metal polishes','KGM',10,0,0 UNION ALL

Select '3405402000','In packaging of a net weight not exceeding 1 kg ','KGM',8,0,0 UNION ALL
Select '3405409000','Other','KGM',8,0,0 UNION ALL

Select '3405901000','Metal polishes','KGM',10,0,0 UNION ALL
Select '3405909000','Other','KGM',10,0,0 UNION ALL

Select '3406000000','Candles, tapers and the like.','KGM',15,0,0 UNION ALL


Select '3407001000','Modelling pastes, including those put up for children''s amusement','KGM',10,0,0 UNION ALL
Select '3407002000','Preparations known as "dental wax" or "dental impression compounds", put up in sets, in packings for retail sale or in plates, horseshoe shapes, sticks or similar forms','KGM',5,0,0 UNION ALL
Select '3407003000','Other preparation for use in dentistry, with a basis of plaster (of  calcined gypsum or calcium sulphate)','KGM',0,0,0 UNION ALL

Select '3501100000','Casein','KGM',0,0,0 UNION ALL

Select '3501901000','Caseinates and other casein derivatives','KGM',0,0,0 UNION ALL
Select '3501902000','Casein glues','KGM',25,0,0 UNION ALL


Select '3502110000','Dried','KGM',5,0,0 UNION ALL
Select '3502190000','Other','KGM',5,0,0 UNION ALL
Select '3502200000','Milk albumin, including concentrates of two or more whey proteins','KGM',5,0,0 UNION ALL
Select '3502900000','Other','KGM',5,0,0 UNION ALL



Select '3503001100','Fish glues','KGM',0,0,0 UNION ALL
Select '3503001900','Other','KGM',25,0,0 UNION ALL
Select '3503003000','Isinglass','KGM',5,0,0 UNION ALL

Select '3503004100','In powder form with a bloating level of A250 or B230 or higher on the Bloom scale','KGM',5,0,0 UNION ALL
Select '3503004900','Other','KGM',5,0,0 UNION ALL

Select '3504000000','Peptones and their derivatives; other protein substances and their derivatives, not elsewhere specified or included; hide powder, whether or not chromed.','KGM',0,0,0 UNION ALL


Select '3505101000','Dextrins; soluble or roasted starches','KGM',0,0,0 UNION ALL
Select '3505109000','Other','KGM',0,0,0 UNION ALL
Select '3505200000','Glues','KGM',25,0,0 UNION ALL

Select '3506100000','Products suitable for use as glues or adhesives, put up for retail sale as glues or adhesives, not exceeding a net weight of 1 kg','KGM',25,0,0 UNION ALL

Select '3506910000','Adhesives based on polymers of headings 39.01 to 39.13 or on  rubber','KGM',25,0,0 UNION ALL
Select '35069900','Other:','',0,0,0 UNION ALL
Select '3506990010','   Optically clear freefilm adhesives and optically clear curable liquid adhesives of a kind used solely or principally for the manufacture of flat panel displays or touchsensitive screen panels','KGM',25,0,0 UNION ALL
Select '3506990090','    Other','KGM',25,0,0 UNION ALL

Select '3507100000','Rennet and concentrates thereof','KGM',0,0,0 UNION ALL
Select '3507900000','Other','KGM',0,0,0 UNION ALL

Select '3601000000','Propellent powders.','KGM',0,0,0 UNION ALL

Select '3602000000','Prepared explosives, other than propellent powders.','KGM',25,0,0 UNION ALL

Select '3603001000','Semifuses; elemented caps; signal tubes','KGM',0,0,0 UNION ALL
Select '3603002000','Safety fuses; detonating fuses','KGM',5,0,0 UNION ALL
Select '3603009000','Other','KGM',0,0,0 UNION ALL

Select '3604100000','Fireworks','KGM',50,0,0 UNION ALL

Select '3604902000','Miniature pyrotechnic munitions and percussion caps for toys','KGM',50,0,0 UNION ALL
Select '3604903000','Signalling flares or rockets','KGM',5,0,0 UNION ALL
Select '3604909000','Other','KGM',50,0,0 UNION ALL

Select '3605000000','Matches, other than pyrotechnic articles of heading 36.04.','KGM',5,0,0 UNION ALL

Select '3606100000','Liquid or liquefiedgas fuels in containers of a kind used for filling or refilling cigarette or similar lighters and of a capacity not exceeding 300 cm3','KGM',30,0,0 UNION ALL

Select '3606901000','Solid or semisolid fuels, solidified alcohol and similar  prepared fuels','KGM',50,0,0 UNION ALL
Select '3606902000','Lighter flints','KGM',0,0,0 UNION ALL
Select '3606903000','Other ferrocerium and other pyrophoric alloys in all forms','KGM',5,0,0 UNION ALL
Select '3606904000','Resin torches, firelighters and the like','KGM',5,0,0 UNION ALL
Select '3606909000','Other','KGM',5,0,0 UNION ALL

Select '3701100000','For Xray','MTK',0,0,0 UNION ALL
Select '3701200000','Instant print film','KGM',0,0,0 UNION ALL
Select '3701300000','Other plates and film, with any side exceeding 255 mm','MTK',0,0,0 UNION ALL


Select '3701911000','Of a kind suitable for use in the printing industry','KGM',0,0,0 UNION ALL
Select '3701919000','Other','KGM',0,0,0 UNION ALL

Select '3701991000','Of a kind suitable for use in the printing industry','MTK',0,0,0 UNION ALL
Select '3701999000','Other','MTK',0,0,0 UNION ALL

Select '3702100000','For Xray','MTK',0,0,0 UNION ALL

Select '3702310000','For colour photography (polychrome)','UNT',0,0,0 UNION ALL
Select '3702320000','Other, with silver halide emulsion','MTK',0,0,0 UNION ALL
Select '3702390000','Other','MTK',0,0,0 UNION ALL

Select '3702410000','Of a width exceeding 610 mm and of a length exceeding 200 m, for colour photography (polychrome)','MTK',0,0,0 UNION ALL

Select '3702421000','Of a kind suitable for use in medical, surgical, dental or veterinary sciences or in the printing industry','MTK',0,0,0 UNION ALL
Select '3702429000','Other','MTK',0,0,0 UNION ALL
Select '3702430000','Of  a  width  exceeding  610 mm  and  of  a length not exceeding 200 m','MTK',0,0,0 UNION ALL
Select '3702440000','Of a width exceeding 105 mm but not exceeding 610 mm','MTK',0,0,0 UNION ALL


Select '3702522000','Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702529000','Other','MTR',0,0,0 UNION ALL
Select '3702530000','Of a width exceeding 16 mm but not exceeding 35 mm and of a length not exceeding 30 m, for slides','MTR',0,0,0 UNION ALL

Select '3702544000','Of a kind suitable for used in medical, surgical, dental or veterinary sciences or in the printing industry','MTR',0,0,0 UNION ALL
Select '3702549000','Other','MTR',0,0,0 UNION ALL

Select '3702552000','Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702555000','Of a kind suitable for used in medical, surgical, dental or veterinary sciences or in the printing industry','MTR',0,0,0 UNION ALL
Select '3702559000','Other','MTR',0,0,0 UNION ALL

Select '3702562000','Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702569000','Other','MTR',0,0,0 UNION ALL


Select '3702961000','         Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702969000','         Other','MTR',0,0,0 UNION ALL

Select '3702971000','         Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702979000','         Other','MTR',0,0,0 UNION ALL

Select '3702981000','         Of a kind suitable for use in cinematography','MTR',0,0,0 UNION ALL
Select '3702983000','         Other, of a length of 120 m or more','MTR',0,0,0 UNION ALL
Select '3702989000','         Other','MTR',0,0,0 UNION ALL


Select '3703101000',' Of a width not exceeding 1,000 mm','KGM',0,0,0 UNION ALL
Select '3703109000','Other','KGM',0,0,0 UNION ALL
Select '3703200000','Other, for colour photography (polychrome)','KGM',0,0,0 UNION ALL
Select '3703900000','Other','KGM',0,0,0 UNION ALL

Select '3704001000','Xray plates or film','KGM',0,0,0 UNION ALL
Select '3704009000','Other','KGM',0,0,0 UNION ALL

Select '3705001000','Xray','KGM',0,0,0 UNION ALL
Select '3705002000','Microfilm','KGM',0,0,0 UNION ALL
Select '3705009000','Other','KGM',0,0,0 UNION ALL


Select '3706101000','Newsreels, travelogues, technical and scientific films','MTR',0,0,0 UNION ALL
Select '3706103000','Other documentary films','MTR',0,0,0 UNION ALL
Select '3706104000','Other, consisting only of sound track','MTR',0,0,0 UNION ALL
Select '3706109000','Other','MTR',0,0,0 UNION ALL

Select '3706901000','Newsreels, travelogues, technical and scientific films','MTR',0,0,0 UNION ALL
Select '3706903000','Other documentary films','MTR',0,0,0 UNION ALL
Select '3706904000','Other, consisting only of sound track','MTR',0,0,0 UNION ALL
Select '3706909000','Other','MTR',0,0,0 UNION ALL

Select '3707100000','Sensitising emulsions','KGM',20,0,0 UNION ALL

Select '3707901000','Flashlight materials','KGM',16,0,0 UNION ALL
Select '3707909000','Other','KGM',16,0,0 UNION ALL

Select '3801100000','Artificial graphite','KGM',0,0,0 UNION ALL
Select '3801200000','Colloidal or semicolloidal graphite','KGM',0,0,0 UNION ALL
Select '3801300000','Carbonaceous pastes for electrodes and similar pastes for furnace  linings','KGM',0,0,0 UNION ALL
Select '3801900000','Other','KGM',0,0,0 UNION ALL

Select '3802100000','Activated carbon','KGM',5,0,0 UNION ALL

Select '3802901000','Activated bauxite','KGM',0,0,0 UNION ALL
Select '3802902000','Activated clays or activated earths','KGM',0,0,0 UNION ALL
Select '3802909000','Other','KGM',0,0,0 UNION ALL

Select '3803000000','Tall oil, whether or not refined.','KGM',0,0,0 UNION ALL


Select '3804001000','Concentrated sulphite lye','KGM',0,0,0 UNION ALL
Select '3804002000','Calcium lignin sulphonates (Ca2LS) binder used for firebrick production
','KGM',0,0,0 UNION ALL
Select '3804009000','Other','KGM',0,0,0 UNION ALL

Select '3805100000','Gum, wood or sulphate turpentine oils','KGM',0,0,0 UNION ALL
Select '3805900000','Other','KGM',0,0,0 UNION ALL

Select '3806100000','Rosin and resin acids','KGM',0,0,0 UNION ALL
Select '3806200000','Salts of rosin, of resin acids or of derivatives of rosin or resin acids, other than salts of rosin adducts','KGM',0,0,0 UNION ALL

Select '3806301000','In blocks','KGM',0,0,0 UNION ALL
Select '3806309000','Other','KGM',0,0,0 UNION ALL

Select '3806901000','Run gums in blocks','KGM',0,0,0 UNION ALL
Select '3806909000','Other','KGM',0,0,0 UNION ALL

Select '3807000000','Wood tar; wood tar oils; wood creosote; wood naphtha; vegetable pitch; brewers'' pitch and similar preparations based on rosin, resin acids or on vegetable pitch.','KGM',0,0,0 UNION ALL



Select '3808521000','Wood preservatives, being preparations other than surface coatings, containing insecticides or fungicides','KGM',20,0,0 UNION ALL
Select '3808529000','Other','KGM',0,0,0 UNION ALL

Select '38085910','Insecticides:','',0,0,0 UNION ALL
Select '3808591010','Having a deodorising function not in liquid form','KGM',10,0,0 UNION ALL
Select '3808591090','Other','KGM',0,0,0 UNION ALL

Select '3808592100','In aerosol containers','KGM',0,0,0 UNION ALL
Select '3808592900','Other','KGM',0,0,0 UNION ALL

Select '3808593100','In aerosol containers','KGM',0,0,0 UNION ALL
Select '3808593900','Other','KGM',0,0,0 UNION ALL
Select '3808594000','Antisprouting products','KGM',0,0,0 UNION ALL
Select '3808595000','Plantgrowth regulators','KGM',0,0,0 UNION ALL
Select '3808596000','Disinfectants','KGM',0,0,0 UNION ALL

Select '3808599100','Wood preservatives, being preparations other than surface coatings, containing insecticides or fungicides','KGM',20,0,0 UNION ALL
Select '3808599900','Other','KGM',0,0,0 UNION ALL


Select '3808611000','Mosquito repellent coils','KGM',10,0,0 UNION ALL
Select '3808612000','Mosquito repellent mats','KGM',5,0,0 UNION ALL
Select '3808613000','In aerosol cans','KGM',5,0,0 UNION ALL
Select '3808614000','Other, in liquid form','KGM',0,0,0 UNION ALL
Select '3808615000','Other, having deodorising function','KGM',10,0,0 UNION ALL
Select '3808619000','Other ','KGM',0,0,0 UNION ALL

Select '3808621000','Powder for moulding into mosquito coils ','KGM',0,0,0 UNION ALL
Select '3808622000','Mosquito repellent coils','KGM',10,0,0 UNION ALL
Select '3808623000','Mosquito repellent mats','KGM',5,0,0 UNION ALL
Select '3808624000','In aerosol cans','KGM',5,0,0 UNION ALL
Select '3808625000','Other, in liquid form','KGM',0,0,0 UNION ALL
Select '38086260','Other:','',0,0,0 UNION ALL
Select '3808626010','Having deodorising function','KGM',10,0,0 UNION ALL
Select '3808626090','Other ','KGM',0,0,0 UNION ALL

Select '3808691000','Powder for moulding into mosquito coils ','KGM',0,0,0 UNION ALL
Select '38086990','Other:','',0,0,0 UNION ALL
Select '3808699010','Having deodorising function','KGM',10,0,0 UNION ALL
Select '3808699090','Other ','KGM',0,0,0 UNION ALL


Select '3808911000','Intermediate preparations containing 2 (methylpropylphenol methylcarbamate)','KGM',0,0,0 UNION ALL
Select '3808912000','Other, powder for moulding into mosquito coils','KGM',0,0,0 UNION ALL
Select '3808913000','In aerosol containers','KGM',5,0,0 UNION ALL
Select '3808914000','Mosquito repellent coils','KGM',10,0,0 UNION ALL
Select '3808915000','Mosquito repellent mats','KGM',5,0,0 UNION ALL
Select '38089190','Other:','',0,0,0 UNION ALL
Select '3808919010','Having a deodorising function','KGM',10,0,0 UNION ALL
Select '3808919090','Other','KGM',0,0,0 UNION ALL


Select '3808921100','With a Validamycin content not exceeding 3% by net weight','KGM',0,0,0 UNION ALL
Select '3808921900','Other','KGM',0,0,0 UNION ALL
Select '3808929000','Other','KGM',0,0,0 UNION ALL


Select '3808931100','In aerosol containers','KGM',0,0,0 UNION ALL
Select '3808931900','Other','KGM',0,0,0 UNION ALL
Select '3808932000','Antisprouting products','KGM',0,0,0 UNION ALL
Select '3808933000','Plantsgrowth regulators','KGM',0,0,0 UNION ALL

Select '3808941000','Containing mixtures of coal tar acid and alkalis','KGM',0,0,0 UNION ALL
Select '3808942000','Other, in aerosol containers','KGM',0,0,0 UNION ALL
Select '3808949000','Other','KGM',0,0,0 UNION ALL

Select '3808991000','Wood preservatives, containing insecticides or fungicides','KGM',20,0,0 UNION ALL
Select '3808999000','Other','KGM',0,0,0 UNION ALL

Select '3809100000','With a basis of amylaceous substances','KGM',0,0,0 UNION ALL


Select '3809911000','Softening agents','KGM',0,0,0 UNION ALL
Select '3809919000','Other','KGM',0,0,0 UNION ALL
Select '3809920000','Of a kind used in the paper or like industries','KGM',0,0,0 UNION ALL
Select '3809930000','Of a kind used in the leather or like industries','KGM',0,0,0 UNION ALL

Select '3810100000','Pickling preparations for metal surfaces; soldering, brazing or welding powders and pastes consisting of metal and other materials','KGM',0,0,0 UNION ALL
Select '3810900000','Other','KGM',0,0,0 UNION ALL


Select '3811110000','Based on lead compounds','KGM',5,0,0 UNION ALL
Select '3811190000','Other','KGM',5,0,0 UNION ALL


Select '3811211000','Put up for retail sale','KGM',0,0,0 UNION ALL
Select '3811219000','Other','KGM',0,0,0 UNION ALL
Select '3811290000','Other','KGM',0,0,0 UNION ALL

Select '3811901000','Rust preventatives or corrosion inhibitors','KGM',0,0,0 UNION ALL
Select '3811909000','Other','KGM',0,0,0 UNION ALL

Select '3812100000','Prepared rubber accelerators','KGM',0,0,0 UNION ALL
Select '3812200000','Compound plasticisers for rubber or plastics','KGM',0,0,0 UNION ALL

Select '3812310000','Mixtures of oligomers of 2,2,4trimethyl1,2dihydroquinoline (TMQ)','KGM',0,0,0 UNION ALL
Select '3812390000','Other','KGM',0,0,0 UNION ALL

Select '3813000000','Preparations and charges for fireextinguishers; charged fireextinguishing grenades.','KGM',0,0,0 UNION ALL

Select '38140000','Organic composite solvents and thinners, not elsewhere specified or included; prepared paint or varnish removers.','',0,0,0 UNION ALL
Select '3814000010','Paint removers','KGM',15,0,0 UNION ALL
Select '3814000020','Thinners','KGM',15,0,0 UNION ALL
Select '3814000090','Other','KGM',15,0,0 UNION ALL


Select '3815110000','With nickel or nickel compounds as the active substance','KGM',0,0,0 UNION ALL
Select '3815120000','With precious metal or precious metal compounds as the active substance','KGM',0,0,0 UNION ALL
Select '3815190000','Other','KGM',0,0,0 UNION ALL
Select '3815900000','Other','KGM',0,0,0 UNION ALL

Select '3816001000','Refractory cements','KGM',0,0,0 UNION ALL
Select '3816009000','Other','KGM',0,0,0 UNION ALL

Select '3817000000','Mixed alkylbenzenes and mixed alkylnaphthalenes, other than those of heading 27.07 or 29.02.','KGM',0,0,0 UNION ALL

Select '3818000000','Chemical elements doped for use in electronics, in the form of discs, wafers or similar forms; chemical compounds doped for use in electronics.','KGM',0,0,0 UNION ALL

Select '3819000000','Hydraulic brake fluids and other prepared liquids for hydraulic transmission, not containing or containing less than 70% by weight of petroleum oils or oils obtained from bituminous minerals.','KGM',0,0,0 UNION ALL

Select '3820000000','Antifreezing preparations and prepared deicing fluids.','KGM',0,0,0 UNION ALL


Select '3821001000','Prepared culture media for the development of microorganisms','KGM',0,0,0 UNION ALL
Select '3821009000','Other','KGM',0,0,0 UNION ALL


Select '3822001000','Plates, sheets, film, foil and strip of plastics impregnated or coated with diagnostic or laboratory reagents','KGM',0,0,0 UNION ALL
Select '3822002000','Paperboard, cellulose wadding and web of cellulose fibres impregnated or coated with diagnostic or laboratory reagents','KGM',0,0,0 UNION ALL
Select '3822003000','Sterilisation indicator strips and tapes','KGM',0,0,0 UNION ALL
Select '3822009000','Other','KGM',0,0,0 UNION ALL


Select '3823110000','Stearic acid','TNE',5,0,0 UNION ALL
Select '3823120000','Oleic acid','KGM',5,0,0 UNION ALL
Select '3823130000','Tall oil fatty acids','TNE',5,0,0 UNION ALL

Select '3823191000','Acid oils from refining','TNE',5,0,0 UNION ALL
Select '3823192000','Palm fatty acid distillate','TNE',5,0,0 UNION ALL
Select '3823193000','Palm kernel fatty acid distillate','TNE',5,0,0 UNION ALL
Select '3823199000','Other','TNE',5,0,0 UNION ALL

Select '3823701000','In the form of wax   ','KGM',0,0,0 UNION ALL
Select '3823709000','Other','KGM',5,0,0 UNION ALL

Select '3824100000','Prepared binders for foundry moulds or cores','KGM',0,0,0 UNION ALL
Select '3824300000','Nonagglomerated metal carbides mixed together or with metallic binders','KGM',0,0,0 UNION ALL
Select '3824400000','Prepared additives for cements, mortars or concretes','KGM',0,0,0 UNION ALL
Select '3824500000','Nonrefractory mortars and concretes','KGM',0,0,0 UNION ALL
Select '3824600000','Sorbitol other than that of subheading 2905.44','KGM',0,0,0 UNION ALL


Select '3824711000','Transformer and circuit breaker oils, containing by weight less than 70% or of petroleum oils or of oils obtained from bituminous minerals','KGM',0,0,0 UNION ALL
Select '3824719000','Other','KGM',0,0,0 UNION ALL
Select '3824720000','Containing bromochlorodifluoromethane, bromotrifluoromethane or dibromotetrafluoroethanes','KGM',0,0,0 UNION ALL
Select '3824730000','Containing hydrobromofluorocarbons (HBFCs)','KGM',0,0,0 UNION ALL

Select '3824741000','Transformer and circuit breaker oils, containing by weight less than 70% or of petroleum oils or of oils obtained from bituminous minerals','KGM',0,0,0 UNION ALL
Select '3824749000','Other','KGM',0,0,0 UNION ALL
Select '3824750000','Containing carbon tetrachloride','KGM',0,0,0 UNION ALL
Select '3824760000','Containing 1,1,1trichloroethane (methyl chloroform)','KGM',0,0,0 UNION ALL
Select '3824770000','Containing bromomethane (methyl bromide) or bromochloromethane','KGM',0,0,0 UNION ALL
Select '3824780000','Containing perfluorocarbons (PFCs) or hydrofluorocarbons (HFCs), but not containing chlorofluorocarbons (CFCs) or hydrochlorofluorocarbons (HCFCs)','KGM',0,0,0 UNION ALL
Select '3824790000','Other','KGM',0,0,0 UNION ALL

Select '3824810000','Containing oxirane (ethylene oxide)','KGM',0,0,0 UNION ALL
Select '3824820000','Containing polychlorinated biphenyls (PCBs), polychlorinated terphenyls (PCTs) or polybrominated biphenyls (PBBs)','KGM',0,0,0 UNION ALL
Select '3824830000','Containing tris(2,3dibromopropyl) phosphate','KGM',0,0,0 UNION ALL
Select '3824840000','Containing aldrin (ISO), camphechlor (ISO) (toxaphene), chlordane (ISO), chlordecone (ISO), DDT (ISO) (clofenotane (INN), 1,1,1trichloro2,2bis(pchlorophenyl)ethane), dieldrin (ISO, INN), endosulfan (ISO), endrin (ISO), heptachlor (ISO) or mirex (ISO)','KGM',0,0,0 UNION ALL
Select '3824850000','Containing 1,2,3,4,5,6hexachlorocyclohexane (HCH (ISO)), including lindane (ISO, INN)','KGM',0,0,0 UNION ALL
Select '3824860000','Containing pentachlorobenzene (ISO) or hexachlorobenzene (ISO)','KGM',0,0,0 UNION ALL
Select '3824870000','Containing perfluorooctane sulphonic acid, its salts, perfluorooctane sulphonamides, or perfluorooctane sulphonyl fluoride','KGM',0,0,0 UNION ALL
Select '3824880000','Containing tetra, penta, hexa hepta or octabromodiphenyl ethers','KGM',0,0,0 UNION ALL

Select '3824910000','Mixtures and preparations consisting mainly of (5ethyl2methyl2oxido1,3,2dioxaphosphinan5yl)methyl methyl methylphosphonate and bis[(5ethyl2methyl2oxido1,3,2dioxaphosphinan5yl)methyl] methylphosphonate','KGM',0,0,0 UNION ALL

Select '3824991000','Ink removers, stencil correctors, other correcting fluids and correction tapes (other than those of heading 96.12), put up in packings for retail sale','KGM',0,0,0 UNION ALL
Select '3824993000','Copying pastes with a basis of gelatin, whether presented in bulk or ready for use (for example, on a paper or textile backing)','KGM',0,0,0 UNION ALL
Select '3824994000','Composite inorganic solvents','KGM',15,0,0 UNION ALL
Select '3824995000','Acetone oil','KGM',15,0,0 UNION ALL
Select '3824996000','Chemical preparations containing monosodium glutamate (MSG)','KGM',30,0,0 UNION ALL
Select '3824997000','Other chemical preparations , of a kind used in the manufacture of foodstuff','KGM',0,0,0 UNION ALL

Select '3824999100','Naphthenic acids, their water insoluble salts and their esters','KGM',0,0,0 UNION ALL
Select '3824999900','Other','KGM',0,0,0 UNION ALL

Select '3825100000','Municipal waste','KGM',0,0,0 UNION ALL
Select '3825200000','Sewage sludge','KGM',0,0,0 UNION ALL

Select '3825301000','Syringes, needles, cannulae and the like','KGM',0,0,0 UNION ALL
Select '3825309000','Other','KGM',0,0,0 UNION ALL

Select '3825410000','Halogenated','KGM',0,0,0 UNION ALL
Select '3825490000','Other','KGM',0,0,0 UNION ALL
Select '3825500000','Wastes of metal pickling liquors, hydraulic fluids, brake fluids and antifreeze fluids','KGM',0,0,0 UNION ALL

Select '3825610000','Mainly containing organic constituents','KGM',0,0,0 UNION ALL
Select '3825690000','Other','KGM',0,0,0 UNION ALL
Select '3825900000','Other','KGM',0,0,0 UNION ALL



Select '3826001000','Coconut methyl ester (CME)','KGM',0,0,0 UNION ALL

Select '3826002100','With ester alkyl content 96.5% or more but not exceeding 98%  ','TNE',0,0,0 UNION ALL
Select '3826002200','With ester alkyl content exceeding 98%','TNE',0,0,0 UNION ALL
Select '3826002900','Other','TNE',0,0,0 UNION ALL
Select '3826003000','Other','KGM',0,0,0 UNION ALL
Select '3826009000','Other','KGM',0,0,0 UNION ALL




Select '3901101200','Linear LowDensity Polyethylene (LLDPE)','KGM',10,0,0 UNION ALL
Select '3901101900','Other','KGM',10,0,0 UNION ALL

Select '3901109200','Linear LowDensity Polyethylene (LLDPE)','KGM',10,0,0 UNION ALL
Select '3901109900','Other','KGM',10,0,0 UNION ALL
Select '3901200000','Polyethylene having a specific gravity of  0.94 or more  ','KGM',10,0,0 UNION ALL
Select '3901300000','Ethylenevinyl acetate copolymers','KGM',0,0,0 UNION ALL
Select '39014000','Ethylenealphaolefin copolymers, having a specific gravity of less than 0.94:','',0,0,0 UNION ALL
Select '3901400010','In dispersion','KGM',0,0,0 UNION ALL
Select '3901400090','Other','KGM',0,0,0 UNION ALL

Select '3901904000','In dispersion','KGM',0,0,0 UNION ALL
Select '3901909000','Other','KGM',0,0,0 UNION ALL


Select '3902103000','In dispersion','KGM',0,0,0 UNION ALL
Select '3902104000','Granules, pellets, beads, flakes, chips and similar forms','KGM',10,0,0 UNION ALL
Select '3902109000','Other','KGM',0,0,0 UNION ALL
Select '3902200000','Polyisobutylene','KGM',0,0,0 UNION ALL

Select '3902303000','In the form of liquids or pastes','KGM',10,0,0 UNION ALL
Select '3902309000','Other','KGM',10,0,0 UNION ALL

Select '3902901000','Chlorinated polypropylene of a kind suitable for use in printing ink formulation','KGM',0,0,0 UNION ALL
Select '3902909000','Other','KGM',0,0,0 UNION ALL



Select '3903111000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3903119000','Other','KGM',0,0,0 UNION ALL

Select '3903191000','In dispersion','KGM',0,0,0 UNION ALL
Select '3903192000','Granules, pellets, beads, flakes, chips and similar forms','KGM',0,0,0 UNION ALL
Select '3903199000','Other','KGM',0,0,0 UNION ALL

Select '3903204000','In aqueous dispersion','KGM',10,0,0 UNION ALL
Select '3903205000','In nonaqueous dispersion ','KGM',10,0,0 UNION ALL
Select '3903209000','Other','KGM',0,0,0 UNION ALL

Select '3903304000','In aqueous dispersion','KGM',10,0,0 UNION ALL
Select '3903305000','In nonaqueous dispersion ','KGM',10,0,0 UNION ALL
Select '3903306000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3903309000','Other','KGM',0,0,0 UNION ALL

Select '3903903000','In dispersion','KGM',10,0,0 UNION ALL

Select '3903909100','Impact Polystyrene of notched izod impact at 230C less than 80 J/m','KGM',0,0,0 UNION ALL
Select '3903909900','Other','KGM',0,0,0 UNION ALL


Select '3904101000','Homopolymers, suspension type','KGM',10,0,0 UNION ALL

Select '3904109100','In the form of granules','KGM',10,0,0 UNION ALL
Select '3904109200','In the form of powder','KGM',10,0,0 UNION ALL
Select '3904109900','Other','KGM',10,0,0 UNION ALL


Select '3904211000','In the form of granules','KGM',10,0,0 UNION ALL
Select '3904212000','In the form of powder','KGM',10,0,0 UNION ALL
Select '3904219000','Other','KGM',10,0,0 UNION ALL

Select '3904221000','In dispersion','KGM',0,0,0 UNION ALL
Select '3904222000','In the form of  granules','KGM',10,0,0 UNION ALL
Select '3904223000','In the form of  powder','KGM',10,0,0 UNION ALL
Select '3904229000','Other','KGM',10,0,0 UNION ALL

Select '3904301000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904302000','In the form of powder','KGM',0,0,0 UNION ALL
Select '3904309000','Other','KGM',0,0,0 UNION ALL

Select '3904401000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904402000','In the form of  powder','KGM',0,0,0 UNION ALL
Select '3904409000','Other','KGM',0,0,0 UNION ALL

Select '3904504000','In dispersion','KGM',10,0,0 UNION ALL
Select '3904505000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904506000','In the form of  powder','KGM',0,0,0 UNION ALL
Select '3904509000','Other','KGM',0,0,0 UNION ALL


Select '3904611000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904612000','In the form of  powder','KGM',0,0,0 UNION ALL
Select '3904619000','Other','KGM',0,0,0 UNION ALL

Select '3904693000','In dispersion','KGM',0,0,0 UNION ALL
Select '3904694000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904695000','In the form of powder','KGM',0,0,0 UNION ALL
Select '3904699000','Other','KGM',0,0,0 UNION ALL

Select '3904903000','In dispersion','KGM',0,0,0 UNION ALL
Select '3904904000','In the form of granules','KGM',0,0,0 UNION ALL
Select '3904905000','In the form of powder','KGM',0,0,0 UNION ALL
Select '3904909000','Other','KGM',0,0,0 UNION ALL


Select '3905120000','In aqueous dispersion','KGM',10,0,0 UNION ALL

Select '3905191000','In the form of liquids or pastes','KGM',0,0,0 UNION ALL
Select '3905199000','Other','KGM',0,0,0 UNION ALL

Select '3905210000','In aqueous dispersion','KGM',10,0,0 UNION ALL
Select '3905290000','Other','KGM',0,0,0 UNION ALL

Select '3905301000','In dispersion','KGM',10,0,0 UNION ALL
Select '3905309000','Other','KGM',0,0,0 UNION ALL


Select '3905911000','In dispersion','KGM',10,0,0 UNION ALL
Select '3905919000','Other','KGM',0,0,0 UNION ALL

Select '3905991000','In aqueous dispersion','KGM',0,0,0 UNION ALL
Select '3905992000','In nonaqueous dispersion','KGM',0,0,0 UNION ALL
Select '3905999000','Other','KGM',0,0,0 UNION ALL


Select '3906101000','In dispersion','KGM',10,0,0 UNION ALL
Select '3906109000','Other','KGM',0,0,0 UNION ALL

Select '3906902000','In dispersion','KGM',10,0,0 UNION ALL

Select '3906909200','Sodium polyacrylate','KGM',0,0,0 UNION ALL
Select '3906909900','Other','KGM',0,0,0 UNION ALL

Select '3907100000','Polyacetals      ','KGM',0,0,0 UNION ALL

Select '3907201000','Polytetramethylene ether glycol','KGM',0,0,0 UNION ALL
Select '3907209000','Other','KGM',0,0,0 UNION ALL

Select '3907302000','Of a kind used for coating, in  powder form','KGM',0,0,0 UNION ALL
Select '3907303000','In the form of liquids or pastes','KGM',0,0,0 UNION ALL
Select '3907309000','Other','KGM',0,0,0 UNION ALL
Select '3907400000','Polycarbonates','KGM',0,0,0 UNION ALL

Select '3907501000','In the form of liquids or pastes','KGM',10,0,0 UNION ALL
Select '3907509000','Other','KGM',10,0,0 UNION ALL

Select '3907610000','Having a viscosity number of 78 ml/g or higher','KGM',15,0,0 UNION ALL

Select '3907691000','In the form of granules','KGM',15,0,0 UNION ALL
Select '3907699000','Other','KGM',15,0,0 UNION ALL
Select '3907700000','Poly(lactic acid)','KGM',5,0,0 UNION ALL


Select '3907912000','Granules and similar forms','KGM',0,0,0 UNION ALL
Select '3907913000','In the form of liquids or pastes','KGM',10,0,0 UNION ALL
Select '3907919000','Other','KGM',10,0,0 UNION ALL

Select '3907994000','Of a kind used for coating, in  powder form','KGM',10,0,0 UNION ALL
Select '39079990','Other:','',0,0,0 UNION ALL
Select '3907999010','    Thermoplastic liquid crystal aromatic polyester copolymers','KGM',10,0,0 UNION ALL
Select '3907999090','    Other','KGM',10,0,0 UNION ALL


Select '3908101000','Polyamide 6','KGM',0,0,0 UNION ALL
Select '3908109000','Other','KGM',0,0,0 UNION ALL
Select '3908900000','Other','KGM',0,0,0 UNION ALL


Select '3909101000','Moulding compounds','KGM',0,0,0 UNION ALL
Select '3909109000','Other','KGM',0,0,0 UNION ALL

Select '3909201000','Moulding compounds','KGM',0,0,0 UNION ALL
Select '3909209000','Other','KGM',0,0,0 UNION ALL

Select '3909310000','  Poly(methylene phenyl isocyanate) (crude MDI, polymeric MDI)','KGM',0,0,0 UNION ALL

Select '3909391000','   Moulding compounds','KGM',0,0,0 UNION ALL

Select '3909399100','    Glyoxal monourein resin','KGM',10,0,0 UNION ALL
Select '3909399900','    Other','KGM',0,0,0 UNION ALL

Select '3909401000','Moulding compounds other than phenol formaldehyde','KGM',0,0,0 UNION ALL
Select '3909409000','Other','KGM',0,0,0 UNION ALL
Select '3909500000','Polyurethanes','KGM',0,0,0 UNION ALL

Select '3910002000','In dispersion and in solutions','KGM',0,0,0 UNION ALL
Select '3910009000','Other','KGM',0,0,0 UNION ALL

Select '3911100000','Petroleum resins, coumarone, indene or coumaroneindene resins and polyterpenes  ','KGM',0,0,0 UNION ALL
Select '3911900000','Other','KGM',0,0,0 UNION ALL


Select '3912110000','Nonplasticised','KGM',0,0,0 UNION ALL
Select '3912120000','Plasticised','KGM',0,0,0 UNION ALL


Select '3912201100','Waterbased semifinished nitrocellulose','KGM',0,0,0 UNION ALL
Select '3912201900','Other','KGM',0,0,0 UNION ALL
Select '3912202000','Plasticised','KGM',0,0,0 UNION ALL

Select '3912310000','Carboxymethylcellulose and its salts','KGM',0,0,0 UNION ALL
Select '3912390000','Other','KGM',0,0,0 UNION ALL

Select '3912902000','In the forms of granules','KGM',0,0,0 UNION ALL
Select '3912909000','Other','KGM',0,0,0 UNION ALL

Select '3913100000','Alginic acid, its salts and esters','KGM',0,0,0 UNION ALL

Select '3913901000','Hardened proteins','KGM',5,0,0 UNION ALL
Select '3913902000','Chemical derivatives of natural rubber','KGM',10,0,0 UNION ALL
Select '3913903000','Starchbased polymers','KGM',20,0,0 UNION ALL
Select '3913909000','Other','KGM',0,0,0 UNION ALL

Select '3914000000','Ionexchangers based on polymers of headings 39.01 to 39.13, in primary forms.','KGM',0,0,0 UNION ALL



Select '3915101000','Of nonrigid cellular products','KGM',30,0,0 UNION ALL
Select '3915109000','Other','KGM',25,0,0 UNION ALL

Select '3915201000','Of nonrigid cellular products','KGM',30,0,0 UNION ALL
Select '3915209000','Other','KGM',25,0,0 UNION ALL

Select '3915301000','Of nonrigid cellular products','KGM',30,0,0 UNION ALL
Select '3915309000','Other','KGM',25,0,0 UNION ALL
Select '39159000','Of other plastics:','',0,0,0 UNION ALL

Select '3915900011','Of  nonrigid cellular products ','KGM',30,0,0 UNION ALL
Select '3915900019','Other','KGM',25,0,0 UNION ALL

Select '3915900021','Of nonrigid cellular products ','KGM',30,0,0 UNION ALL

Select '3915900022','Of phenolic resins','KGM',5,0,0 UNION ALL
Select '3915900023','Of aminoresins','KGM',5,0,0 UNION ALL
Select '3915900029','Other','KGM',25,0,0 UNION ALL
Select '3915900030','Of regenerated cellulose; cellulose nitrate, cellulose acetate and other cellulose esters, cellulose ethers and other chemical  derivatives of cellulose ','KGM',25,0,0 UNION ALL
Select '3915900040','Of hardened proteins    ','KGM',5,0,0 UNION ALL
Select '3915900050','Of chemical derivatives of natural rubber      ','KGM',5,0,0 UNION ALL
Select '3915900090','Other','KGM',0,0,0 UNION ALL


Select '3916101000','Monofilament','KGM',20,0,0 UNION ALL
Select '3916102000','Rods, sticks and profile shapes','KGM',20,0,0 UNION ALL

Select '3916201000','Monofilament','KGM',20,0,0 UNION ALL
Select '3916202000','Rods, sticks and profile shapes','KGM',20,0,0 UNION ALL


Select '3916904100','Monofilament','KGM',5,0,0 UNION ALL
Select '3916904200','Rods, sticks and profile shapes','KGM',5,0,0 UNION ALL
Select '3916905000','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3916906000','Of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3916907000','Of other addition polymerisation products; of regenerated cellulose; of cellulose nitrate, cellulose acetate and other cellulose esters, cellulose ethers and other chemical derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '39169080','Of condensation or rearrangement polymerisation products:','',0,0,0 UNION ALL
Select '3916908010','Of phenolic resins; of aminoresins','KGM',5,0,0 UNION ALL
Select '3916908020','Of polyamides','KGM',20,0,0 UNION ALL
Select '3916908090','Other','KGM',20,0,0 UNION ALL

Select '3916909100','Monofilament','KGM',0,0,0 UNION ALL
Select '3916909200','Rods, sticks and profile shapes','KGM',0,0,0 UNION ALL


Select '3917101000','Of hardened proteins','KGM',0,0,0 UNION ALL
Select '3917109000','Other','KGM',20,0,0 UNION ALL

Select '3917210000','Of polymers of ethylene   ','KGM',20,0,0 UNION ALL
Select '3917220000','Of polymers of propylene','KGM',20,0,0 UNION ALL
Select '3917230000','Of polymers of vinyl chloride','KGM',20,0,0 UNION ALL


Select '3917291100','Of other addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917291200','Of aminoresins; of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised; of vulcanised fibre; of hardened proteins ; of chemical derivatives of natural rubber','KGM',20,0,0 UNION ALL
Select '3917291900','Other','KGM',20,0,0 UNION ALL

Select '3917292100','Of other addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917292200','Of phenolic resins','KGM',5,0,0 UNION ALL
Select '3917292300','Of aminoresins; of hardened proteins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3917292400','Of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised; of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3917292500','Of other condensation or rearrangement polymerisation products','KGM',20,0,0 UNION ALL
Select '3917292900','Other','KGM',0,0,0 UNION ALL

Select '3917310000','Flexible tubes, pipes and hoses, having a minimum burst pressure of 27.6 MPa:      ','',0,0,0 UNION ALL

Select '3917311100','Of addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917311200','Of aminoresins; of phenolic resins; of vulcanised fibre; of chemical derivatives of natural rubber','KGM',20,0,0 UNION ALL
Select '3917311900','Other','KGM',20,0,0 UNION ALL

Select '3917312100','Of addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917312300','Of aminoresins;  of phenolic resins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3917312400','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3917312500','Of other condensation or rearrangement polymerisation products; of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3917312900','Other','KGM',0,0,0 UNION ALL

Select '3917321000','Sausage or ham casings','KGM',0,0,0 UNION ALL
Select '39173220','Thermoplastic hoses for gas stove :','',0,0,0 UNION ALL

Select '3917322011','Of addition polymerisation products; of phenolic resins or amino resins; of chemical derivatives of natural rubber   ','KGM',20,0,0 UNION ALL
Select '3917322019','Other','KGM',20,0,0 UNION ALL

Select '3917322091','Of  addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917322092','Of aminoresins or phenolic resins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3917322093','Of other condensation or rearrangement polymerisation products; of vulcanised fibre;  of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3917322099','Other','KGM',0,0,0 UNION ALL


Select '3917329100','Of addition polymerisation products; of phenolic resins or amino resins; of chemical derivatives of natural rubber   ','KGM',20,0,0 UNION ALL
Select '3917329200','Other','KGM',20,0,0 UNION ALL

Select '3917329300','Of addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917329400','Of aminoresins or phenolic resins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3917329500','Of other condensation or rearrangement polymerisation products; of vulcanised fibre;  of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3917329900','Other','KGM',0,0,0 UNION ALL

Select '3917331000','Other, further worked than merely surface worked','KGM',20,0,0 UNION ALL

Select '3917339100','Of other addition polymerisation products','KGM',20,0,0 UNION ALL
Select '3917339200','Of condensation or rearrangement polymerisation products','KGM',20,0,0 UNION ALL
Select '3917339300','Of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised ','KGM',20,0,0 UNION ALL
Select '3917339400','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3917339500','Of hardened proteins','KGM',20,0,0 UNION ALL
Select '3917339600','Of chemical derivatives of natural rubber','KGM',20,0,0 UNION ALL
Select '3917339900','Other','KGM',20,0,0 UNION ALL


Select '3917391100','Of addition polymerisation products; of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3917391200','Of phenolic resins or amino resins ; of chemical derivatives of natural rubber','KGM',20,0,0 UNION ALL
Select '3917391300','Of cellulose nitrate, cellulose acetates and other chemical derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3917391900','Other','KGM',20,0,0 UNION ALL

Select '3917399100','Of addition polymerisation products; of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3917399200','Of phenolic resins or amino resins ; of chemical derivatives of natural rubber ','KGM',5,0,0 UNION ALL
Select '3917399300','Of other condensation or rearrangement polymerisation products','KGM',20,0,0 UNION ALL
Select '3917399400','Of cellulose nitrate, cellulose acetates and other chemical derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3917399900','Other','KGM',3,0,0 UNION ALL
Select '3917400000','Fittings','KGM',20,0,0 UNION ALL



Select '3918101100','Tiles','KGM',20,0,0 UNION ALL
Select '3918101900','Other','KGM',20,0,0 UNION ALL
Select '3918109000','Other','KGM',20,0,0 UNION ALL


Select '3918901100','Tiles, of polyethylene','KGM',20,0,0 UNION ALL
Select '3918901300','Other, of polyethylene','KGM',20,0,0 UNION ALL
Select '3918901400','Of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3918901500','Of other addition polymerisation products; of condensation or rearrangement polymerisation products; of cellulose nitrate, cellulose acetate or other chemical derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3918901600','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3918901900','Other','KGM',0,0,0 UNION ALL

Select '3918909100','Of polyethylene','KGM',20,0,0 UNION ALL
Select '3918909200','Of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3918909300','Of other addition polymerisation products; of condensation or rearrangement polymerisation products; of cellulose nitrate, cellulose acetate or other chemical derivatives of cellulose, plasticised','KGM',20,0,0 UNION ALL
Select '3918909400','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3918909900','Other','KGM',0,0,0 UNION ALL


Select '3919101000','Of polymers of vinyl chloride','KGM',20,0,0 UNION ALL
Select '3919102000','Of polyethylene','KGM',20,0,0 UNION ALL

Select '3919109100','Of hardened protein or chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3919109200','Of addition polymerisation products; of condensation or rearrangement polymerisation products; of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised ','KGM',20,0,0 UNION ALL
Select '3919109900','Other','KGM',0,0,0 UNION ALL

Select '3919901000','Of polymers of vinyl chloride','KGM',20,0,0 UNION ALL
Select '3919902000','Of hardened proteins','KGM',5,0,0 UNION ALL

Select '3919909100','Of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3919909200','Of addition polymerisation products; of condensation or rearrangement polymerisation products; of cellulose nitrate, cellulose acetates and other chemical  derivatives of cellulose, plasticised ','KGM',20,0,0 UNION ALL
Select '3919909900','Other','KGM',20,0,0 UNION ALL



Select '3920101100','Rigid ','KGM',20,0,0 UNION ALL
Select '3920101900','Other','KGM',20,0,0 UNION ALL
Select '3920109000','Other','KGM',20,0,0 UNION ALL

Select '3920201000','Biaxially oriented polypropylene (BOPP) film','KGM',20,0,0 UNION ALL

Select '3920209100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920209900','Other','KGM',20,0,0 UNION ALL

Select '3920302000','Acrylonitrile butadiene styrene (ABS) sheets of a kind used in the manufacture of refrigerators','KGM',20,0,0 UNION ALL

Select '3920309100','Plates and sheets, rigid','KGM',20,0,0 UNION ALL
Select '3920309200','Other, plates and sheets','KGM',20,0,0 UNION ALL
Select '3920309900','Other','KGM',20,0,0 UNION ALL


Select '3920431000','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920439000','Other','KGM',20,0,0 UNION ALL
Select '3920490000','Other','KGM',20,0,0 UNION ALL



Select '3920511100','Rigid ','KGM',20,0,0 UNION ALL
Select '3920511900','Other','KGM',20,0,0 UNION ALL
Select '3920519000','Other','KGM',20,0,0 UNION ALL


Select '3920591100','Rigid','KGM',20,0,0 UNION ALL
Select '3920591900','Other','KGM',20,0,0 UNION ALL
Select '3920599000','Other','KGM',20,0,0 UNION ALL


Select '3920611000','Plates and sheets','KGM',25,0,0 UNION ALL
Select '3920619000','Other','KGM',20,0,0 UNION ALL

Select '3920621000','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920629000','Other','KGM',20,0,0 UNION ALL

Select '3920631000','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920639000','Other','KGM',20,0,0 UNION ALL

Select '3920691000','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920699000','Other','KGM',20,0,0 UNION ALL


Select '3920711000','Cellophane film','KGM',20,0,0 UNION ALL

Select '3920719100','Printed sheets','KGM',20,0,0 UNION ALL
Select '3920719900','Other','KGM',20,0,0 UNION ALL
Select '3920730000','Of cellulose acetate','KGM',20,0,0 UNION ALL

Select '3920791000','Of nitrocellulose (gun cotton)','KGM',5,0,0 UNION ALL
Select '3920792000','Of vulcanised fibre','KGM',20,0,0 UNION ALL

Select '39207991','Plates and sheets:','',0,0,0 UNION ALL
Select '3920799110','Non rigid products','KGM',20,0,0 UNION ALL
Select '3920799190','Other','KGM',20,0,0 UNION ALL
Select '3920799900','Other','KGM',20,0,0 UNION ALL


Select '3920911000','Film of a kind used in safety glass, of a thickness exceeding 0.38 mm but not exceeding 0.76 mm, and of a width not exceeding 2 m ','KGM',20,0,0 UNION ALL

Select '3920919100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920919900','Other','KGM',20,0,0 UNION ALL

Select '3920921000','Of polyamide6     ','KGM',20,0,0 UNION ALL

Select '3920929100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920929900','Other','KGM',20,0,0 UNION ALL

Select '3920931000','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920939000','Other','KGM',20,0,0 UNION ALL

Select '3920941000','Phenol formaldehyde (bakelite) sheets','KGM',20,0,0 UNION ALL

Select '3920949100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920949900','Other','KGM',20,0,0 UNION ALL

Select '3920991000','Of hardened proteins; of chemical derivatives of natural rubber      ','KGM',5,0,0 UNION ALL

Select '3920992100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920992900','Other','KGM',20,0,0 UNION ALL

Select '3920993100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3920993900','Other','KGM',20,0,0 UNION ALL
Select '3920999000','Other','KGM',0,0,0 UNION ALL



Select '39211120','Rigid:','',0,0,0 UNION ALL
Select '3921112010','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921112090','Other','KGM',0,0,0 UNION ALL

Select '3921119100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921119200','Film','KGM',20,0,0 UNION ALL
Select '39211199','Other:','',0,0,0 UNION ALL
Select '3921119910','Blocks','KGM',20,0,0 UNION ALL
Select '3921119990','Other','KGM',20,0,0 UNION ALL
Select '3921120000','Of polymers of vinyl chloride   ','KGM',20,0,0 UNION ALL

Select '39211310','Rigid:','',0,0,0 UNION ALL
Select '3921131010','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921131090','Other','KGM',0,0,0 UNION ALL

Select '3921139100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921139200','Film','KGM',20,0,0 UNION ALL
Select '3921139900','Other','KGM',20,0,0 UNION ALL

Select '39211420','Rigid:','',0,0,0 UNION ALL
Select '3921142010','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921142090','Other','KGM',0,0,0 UNION ALL

Select '3921149100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921149200','Film','KGM',20,0,0 UNION ALL
Select '39211499','Other:','',0,0,0 UNION ALL
Select '3921149910','Blocks','KGM',20,0,0 UNION ALL
Select '3921149990','Other','KGM',20,0,0 UNION ALL

Select '39211920','Rigid:','',0,0,0 UNION ALL

Select '3921192015','Plates and sheets, of polypropylene','KGM',20,0,0 UNION ALL
Select '3921192016','Other, plates and sheets','KGM',20,0,0 UNION ALL
Select '3921192019','Other','KGM',0,0,0 UNION ALL

Select '3921192024','Plates and sheets','KGM',0,0,0 UNION ALL
Select '3921192029','Other','KGM',20,0,0 UNION ALL

Select '3921192034','Of nitrocellulose (guncotton)','KGM',5,0,0 UNION ALL
Select '3921192035','Plates and sheets','KGM',0,0,0 UNION ALL
Select '3921192039','Other','KGM',20,0,0 UNION ALL
Select '3921192040','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3921192070','Of hardened proteins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3921192090','Other','KGM',0,0,0 UNION ALL

Select '39211991','Plates and sheets:','',0,0,0 UNION ALL

Select '3921199111','Of nitrocellulose (guncotton)','KGM',5,0,0 UNION ALL
Select '3921199119','Other','KGM',20,0,0 UNION ALL
Select '3921199120','Of other addition polymerisation products; of other condensation or rearrangement polymerisation products; of vulcanised fibre  ','KGM',20,0,0 UNION ALL
Select '3921199130','Of hardened proteins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3921199190','Other','KGM',0,0,0 UNION ALL
Select '39211992','Film:','',0,0,0 UNION ALL

Select '3921199211','Of nitrocellulose (guncotton)','KGM',5,0,0 UNION ALL
Select '3921199219','Other','KGM',20,0,0 UNION ALL
Select '3921199220','Of other addition polymerisation products; of other condensation or rearrangement polymerisation products; of vulcanised fibre  ','KGM',20,0,0 UNION ALL
Select '3921199230','Of hardened proteins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3921199290','Other','KGM',0,0,0 UNION ALL
Select '39211999','Other :','',0,0,0 UNION ALL

Select '3921199911','Of nitrocellulose (guncotton)','KGM',5,0,0 UNION ALL
Select '3921199919','Other','KGM',20,0,0 UNION ALL
Select '3921199920','Of other addition polymerisation products; of other condensation or rearrangement polymerisation products; of vulcanised fibre  ','KGM',20,0,0 UNION ALL
Select '3921199930','Of hardened proteins; of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL
Select '3921199990','Other','KGM',0,0,0 UNION ALL

Select '3921901000','Of vulcanised fibre','KGM',20,0,0 UNION ALL
Select '3921902000','Of hardened proteins','KGM',5,0,0 UNION ALL
Select '3921903000','Of chemical derivatives of natural rubber','KGM',5,0,0 UNION ALL

Select '3921904100','Plates and sheets','KGM',20,0,0 UNION ALL
Select '3921904200','Film','KGM',20,0,0 UNION ALL
Select '3921904300','Textile laminated strip','KGM',20,0,0 UNION ALL
Select '3921904900','Other','KGM',5,0,0 UNION ALL
Select '3921905000','Of regenerated cellulose    ','KGM',20,0,0 UNION ALL
Select '3921906000','Of other cellulose or its chemical derivatives','KGM',20,0,0 UNION ALL
Select '39219090','Other','KGM',0,0,0 UNION ALL
Select '3921909010','Other','KGM',20,0,0 UNION ALL
Select '3921909090','Other','KGM',0,0,0 UNION ALL



Select '3922101100','Bathubs having rectangular or oblong interior shape','KGM',20,0,0 UNION ALL
Select '3922101900','Other','KGM',5,0,0 UNION ALL
Select '3922109000','Other','KGM',5,0,0 UNION ALL
Select '3922200000','Lavatory seats and covers','KGM',5,0,0 UNION ALL


Select '3922901100','Parts of flushing cisterns','KGM',20,0,0 UNION ALL
Select '3922901200','Flushing cisterns equipped with their mechanisms  ','KGM',0,0,0 UNION ALL
Select '3922901900','Other','KGM',5,0,0 UNION ALL
Select '3922909000','Other','KGM',5,0,0 UNION ALL


Select '3923101000','Cases for film, tape and optical discs','KGM',20,0,0 UNION ALL
Select '3923109000','Other','KGM',20,0,0 UNION ALL



Select '3923211100','Of a width of 315 mm or more and of a length of 410 mm or more, incorporating a sealed gland','KGM',20,0,0 UNION ALL
Select '3923211900','Other','KGM',20,0,0 UNION ALL

Select '3923219100','Aseptic bags not reinforced with aluminium foil (other than retort pouches), of a width of 315 mm or more and of a length of 410 mm or more, incorporating a sealed gland','KGM',20,0,0 UNION ALL
Select '3923219900','Other','KGM',20,0,0 UNION ALL

Select '3923291000','Aseptic bags whether or not reinforced with aluminium foil (other than retort pouches), of a width of 315 mm or more and of a length of 410 mm or more, incorporating a sealed gland','KGM',20,0,0 UNION ALL
Select '3923299000','Other','KGM',20,0,0 UNION ALL

Select '3923302000','Multilayer fibreglass reinforced  containers, for compressed or liquefied gas ','KGM',20,0,0 UNION ALL
Select '3923309000','Other','KGM',20,0,0 UNION ALL

Select '3923401000','Suitable for use with the machines of heading 84.44, 84.45 or 84.48','KGM',5,0,0 UNION ALL
Select '3923409000','Other','KGM',20,0,0 UNION ALL
Select '3923500000','Stoppers, lids, caps and other closures','KGM',20,0,0 UNION ALL

Select '3923901000','Toothpaste tubes','KGM',20,0,0 UNION ALL
Select '3923909000','Other','KGM',20,0,0 UNION ALL


Select '3924101000','Of melamine','KGM',20,0,0 UNION ALL
Select '39241090','Other','',0,0,0 UNION ALL
Select '3924109010','Baby feeding bottles','KGM',0,0,0 UNION ALL
Select '3924109090','Other','KGM',20,0,0 UNION ALL

Select '3924901000','Bed pans, urinals (portable type) or chamberpots','KGM',0,0,0 UNION ALL
Select '3924902000','Nipple former, breastshells, nipple shields, hand expression funnel','KGM',0,0,0 UNION ALL
Select '3924903000','Supplementary feeding system for babies','KGM',0,0,0 UNION ALL
Select '39249090','Other:','',0,0,0 UNION ALL
Select '3924909020','Articles related to baby feeding','KGM',0,0,0 UNION ALL
Select '3924909090','Other','KGM',20,0,0 UNION ALL

Select '3925100000','Reservoirs, tanks, vats and similar containers, of a capacity exceeding 300 l','KGM',20,0,0 UNION ALL
Select '3925200000','Doors, windows and their frames and thresholds for doors','KGM',20,0,0 UNION ALL
Select '3925300000','Shutters, blinds (including Venetian blinds) and similar articles  and parts thereof','KGM',20,0,0 UNION ALL
Select '3925900000','Other','KGM',20,0,0 UNION ALL

Select '3926100000','Office or school supplies','KGM',20,0,0 UNION ALL

Select '3926206000','Articles of apparel used for protection from chemical substances, radiation or fire','KGM',20,0,0 UNION ALL
Select '3926209000','Other','KGM',20,0,0 UNION ALL
Select '3926300000','Fittings for furniture, coachwork or the like','KGM',0,0,0 UNION ALL
Select '3926400000','Statuettes and other ornamental articles','KGM',20,0,0 UNION ALL

Select '3926901000','Floats for fishing nets','KGM',20,0,0 UNION ALL
Select '3926902000','Fans and handscreens, frames and handles therefor, and parts  thereof','KGM',5,0,0 UNION ALL

Select '3926903200','Plastic moulds with denture imprints','KGM',20,0,0 UNION ALL
Select '3926903900','Other','KGM',20,0,0 UNION ALL

Select '3926904100','Police shields','KGM',20,0,0 UNION ALL
Select '3926904200','Protective masks for use in welding and similar work','KGM',20,0,0 UNION ALL
Select '3926904400','Life saving cushions for the protection of persons falling from  heights','KGM',20,0,0 UNION ALL
Select '3926904900','Other','KGM',20,0,0 UNION ALL

Select '3926905300','Transmission or conveyor belts or belting','KGM',20,0,0 UNION ALL
Select '3926905500','Plastic Jhooks or bunch blocks for detonators','KGM',20,0,0 UNION ALL
Select '3926905900','Other','KGM',20,0,0 UNION ALL
Select '3926906000','Poultry feeders','KGM',20,0,0 UNION ALL
Select '3926907000','Padding for articles of apparel or clothing accessories','KGM',5,0,0 UNION ALL

Select '3926908100','Shoe lasts','KGM',20,0,0 UNION ALL
Select '3926908200','Prayer beads','KGM',0,0,0 UNION ALL
Select '3926908900','Other','KGM',20,0,0 UNION ALL

Select '3926909100','Of a kind used for grain storage','KGM',20,0,0 UNION ALL
Select '3926909200','Empty capsules of a kind suitable for pharmaceutical use','KGM',20,0,0 UNION ALL
Select '3926909900','Other','KGM',20,0,0 UNION ALL



Select '4001101100','Centrifuged concentrate rubber latex','KGM',0,0,0 UNION ALL
Select '4001101900','Other','KGM',0,0,0 UNION ALL

Select '4001102100','Centrifuged concentrate rubber latex','KGM',0,0,0 UNION ALL
Select '4001102900','Other','KGM',0,0,0 UNION ALL


Select '4001211000','RSS Grade 1','KGM',0,0,0 UNION ALL
Select '4001212000','RSS Grade 2','KGM',0,0,0 UNION ALL
Select '4001213000','RSS Grade 3','KGM',0,0,0 UNION ALL
Select '4001214000','RSS Grade 4','KGM',0,0,0 UNION ALL
Select '4001215000','RSS Grade 5','KGM',0,0,0 UNION ALL
Select '4001219000','Other','KGM',0,0,0 UNION ALL

Select '4001221000','TSNR 10','KGM',0,0,0 UNION ALL
Select '4001222000','TSNR 20','KGM',0,0,0 UNION ALL
Select '4001223000','TSNR L','KGM',0,0,0 UNION ALL
Select '4001224000','TSNR CV','KGM',0,0,0 UNION ALL
Select '4001225000','TSNR GP','KGM',0,0,0 UNION ALL
Select '40012290','Other:','',0,0,0 UNION ALL
Select '4001229010','TSNR 5 ','KGM',0,0,0 UNION ALL
Select '4001229090','Other','KGM',0,0,0 UNION ALL

Select '4001291000','Airdried sheets','KGM',0,0,0 UNION ALL
Select '4001292000','Latex crepes','KGM',0,0,0 UNION ALL
Select '4001293000','Sole crepes','KGM',0,0,0 UNION ALL
Select '4001294000','Remilled crepes, including flat bark crepes','KGM',0,0,0 UNION ALL
Select '4001295000','Other crepes','KGM',0,0,0 UNION ALL
Select '4001296000','Superior processing rubber','KGM',0,0,0 UNION ALL
Select '4001297000','Skim rubber','KGM',0,0,0 UNION ALL
Select '4001298000','Scrap (tree, earth or smoked) and cup lump','KGM',0,0,0 UNION ALL

Select '40012991','In primary forms:','',0,0,0 UNION ALL
Select '4001299110','Processing Aid Rubber Latex (PA)','KGM',0,0,0 UNION ALL
Select '4001299120','Methyl Methacrylate Grafted Rubber (MG)','KGM',0,0,0 UNION ALL
Select '4001299130','Purified rubber  DPNR','KGM',0,0,0 UNION ALL
Select '4001299140','Rubber oil extended  OENR','KGM',0,0,0 UNION ALL
Select '4001299150','Epoxidised rubber 25 (ENR 25)','KGM',0,0,0 UNION ALL
Select '4001299160','Epoxidised rubber 50 (ENR 50)','KGM',0,0,0 UNION ALL
Select '4001299190','Other','KGM',0,0,0 UNION ALL
Select '4001299900','Other','KGM',0,0,0 UNION ALL

Select '40013020','In primary forms:','',0,0,0 UNION ALL

Select '4001302011','Raw','KGM',0,10,0 UNION ALL
Select '4001302012','Pressed but not refined','KGM',0,5,0 UNION ALL
Select '4001302019','Other','KGM',0,0,0 UNION ALL
Select '4001302020','Of Guttapercha','KGM',0,10,0 UNION ALL
Select '4001302090','Other','KGM',0,0,0 UNION ALL
Select '4001309000','Other','KGM',0,0,0 UNION ALL


Select '4002110000','Latex','KGM',0,0,0 UNION ALL

Select '4002191000','In primary forms or in unvulcanised, uncompounded plates, sheets or strip','KGM',0,0,0 UNION ALL
Select '4002199000','Other','KGM',25,0,0 UNION ALL

Select '4002201000','In primary forms','KGM',0,0,0 UNION ALL
Select '4002209000','Other      ','KGM',25,0,0 UNION ALL


Select '4002311000','Unvulcanised, uncompounded plates, sheets or strip      ','KGM',25,0,0 UNION ALL
Select '4002319000','Other','KGM',0,0,0 UNION ALL

Select '4002391000','Unvulcanised, uncompounded plates, sheets or strip      ','KGM',25,0,0 UNION ALL
Select '4002399000','Other','KGM',0,0,0 UNION ALL

Select '4002410000','Latex','KGM',0,0,0 UNION ALL

Select '4002491000','In primary forms   ','KGM',0,0,0 UNION ALL
Select '4002499000','Other','KGM',25,0,0 UNION ALL

Select '4002510000','Latex','KGM',0,0,0 UNION ALL

Select '4002591000','In primary forms   ','KGM',0,0,0 UNION ALL
Select '4002599000','Other','KGM',25,0,0 UNION ALL

Select '4002601000','In primary forms','KGM',0,0,0 UNION ALL
Select '4002609000','Other','KGM',25,0,0 UNION ALL

Select '4002701000','In primary forms','KGM',0,0,0 UNION ALL
Select '4002709000','Other','KGM',25,0,0 UNION ALL

Select '4002801000','Mixtures of natural rubber latex with synthetic rubber latex','KGM',25,0,0 UNION ALL
Select '4002809000','Other','KGM',25,0,0 UNION ALL

Select '4002910000','Latex','KGM',0,0,0 UNION ALL

Select '4002992000','In primary forms or in unvulcanised, uncompounded plates, sheets or strip','KGM',0,0,0 UNION ALL
Select '4002999000','Other','KGM',25,0,0 UNION ALL

Select '4003000000','Reclaimed rubber in primary forms or in plates, sheets or strip.','KGM',25,0,0 UNION ALL

Select '4004000000','Waste, parings and scrap of rubber (other than hard rubber) and powders and granules obtained therefrom.','KGM',25,0,0 UNION ALL


Select '4005101000','Of natural gums','KGM',30,0,0 UNION ALL
Select '4005109000','Other','KGM',25,0,0 UNION ALL
Select '4005200000','Solutions; dispersions other than those of subheading 4005.10','KGM',30,0,0 UNION ALL


Select '4005911000','Of natural gums','KGM',30,0,0 UNION ALL
Select '4005919000','Other','KGM',25,0,0 UNION ALL

Select '4005991000','Latex','KGM',0,0,0 UNION ALL
Select '4005992000','Natural rubber compounded with substances other than carbon or silica','KGM',5,0,0 UNION ALL
Select '4005999000','Other','KGM',25,0,0 UNION ALL

Select '4006100000','"Camelback" strips for retreading rubber tyres','KGM',25,0,0 UNION ALL


Select '4006901100','Articles','KGM',5,0,0 UNION ALL
Select '4006901900','Other','KGM',30,0,0 UNION ALL
Select '4006909000','Other','KGM',25,0,0 UNION ALL

Select '4007000000','Vulcanised rubber thread and cord.','KGM',15,0,0 UNION ALL



Select '4008111000','Exceeding 5 mm in thickness, lined with textile fabric on one side','KGM',5,0,0 UNION ALL
Select '4008112000','Other, floor tiles and wall tiles','KGM',30,0,0 UNION ALL
Select '4008119000','Other','KGM',30,0,0 UNION ALL
Select '4008190000','Other','KGM',30,0,0 UNION ALL


Select '4008211000','Exceeding 5 mm in thickness, lined with textile fabric on one side','KGM',5,0,0 UNION ALL
Select '4008212000','Other, floor tiles and wall tiles','KGM',30,0,0 UNION ALL
Select '4008213000','Water stop','KGM',30,0,0 UNION ALL
Select '4008214000','Rubber soling sheet','KGM',30,0,0 UNION ALL
Select '4008219000','Other','KGM',30,0,0 UNION ALL
Select '4008290000','Other','KGM',30,0,0 UNION ALL


Select '4009110000','Without fittings','KGM',30,0,0 UNION ALL

Select '4009121000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009129000',' Other','KGM',30,0,0 UNION ALL


Select '4009211000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009219000','Other','KGM',30,0,0 UNION ALL

Select '4009221000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009229000','Other','KGM',30,0,0 UNION ALL


Select '4009311000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009312000','Rubber hose of a kind used for gas stove','KGM',30,0,0 UNION ALL

Select '4009319100','Fuel hoses, heater hoses and water hoses, of a kind used on motor vehicles of heading 87.02, 87.03, 87.04 or 87.11 ','KGM',30,0,0 UNION ALL
Select '4009319900','Other','KGM',30,0,0 UNION ALL

Select '4009321000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009322000','Rubber hose of a kind used for gas stove','KGM',30,0,0 UNION ALL
Select '4009329000','Other','KGM',30,0,0 UNION ALL


Select '4009411000','Rubber hose of a kind used for gas stove','KGM',30,0,0 UNION ALL
Select '4009419000','Other','KGM',30,0,0 UNION ALL

Select '4009421000','Mining slurry suction and discharge hoses','KGM',30,0,0 UNION ALL
Select '4009422000','Rubber hose of a kind used for gas stove','KGM',30,0,0 UNION ALL
Select '4009429000','Other','KGM',30,0,0 UNION ALL


Select '4010110000','Reinforced only with metal','KGM',30,0,0 UNION ALL
Select '4010120000','Reinforced only with textile materials','KGM',30,0,0 UNION ALL
Select '4010190000','Other','KGM',30,0,0 UNION ALL

Select '4010310000','Endless transmission belts of trapezoidal crosssection (Vbelts), Vribbed, of an outside circumference exceeding 60 cm but not exceeding 180 cm','KGM',30,0,0 UNION ALL
Select '4010320000','Endless transmission belts of trapezoidal crosssection (Vbelts), other than Vribbed, of an outside circumference exceeding 60 cm but not exceeding 180 cm','KGM',30,0,0 UNION ALL
Select '4010330000','Endless transmission belts of trapezoidal crosssection (Vbelts), Vribbed, of an outside circumference exceeding 180 cm but not exceeding 240 cm','KGM',30,0,0 UNION ALL
Select '4010340000','Endless transmission belts of trapezoidal crosssection (Vbelts), other than Vribbed, of an outside circumference exceeding 180 cm but not exceeding 240 cm','KGM',30,0,0 UNION ALL
Select '4010350000','Endless  synchronous belts,  of an outside circumference exceeding 60 cm but not exceeding 150 cm','KGM',30,0,0 UNION ALL
Select '4010360000','Endless  synchronous belts,  of an outside circumference exceeding 150 cm but not exceeding 198 cm','KGM',30,0,0 UNION ALL
Select '4010390000','Other','KGM',30,0,0 UNION ALL

Select '4011100000','Of a kind used on motor cars (including station wagons and racing cars)','UNT',40,0,0 UNION ALL

Select '4011201000','Of a width not exceeding 450 mm','UNT',40,0,0 UNION ALL
Select '4011209000','Other','UNT',40,0,0 UNION ALL
Select '4011300000','Of a kind used on aircraft','UNT',5,0,0 UNION ALL
Select '4011400000','Of a kind used on motorcycles','UNT',30,0,0 UNION ALL
Select '4011500000','Of a kind used on bicycles','UNT',30,0,0 UNION ALL
Select '40117000','Of a kind used on agricultural or forestry vehicles and machines:','',0,0,0 UNION ALL
Select '4011700010','Of a kind used on agricultural or forestry tractors of 87.01 ; of wheelbarrows','UNT',30,0,0 UNION ALL
Select '4011700090','Other','UNT',5,0,0 UNION ALL


Select '4011801100','Of a kind used on tractors, machinery of heading 84.29 or 84.30, forklifts, wheelbarrows or other industrial handling vehicles and machines','UNT',30,0,0 UNION ALL
Select '4011801900','Other','UNT',5,0,0 UNION ALL

Select '4011802100','Of a kind used on tractors, machinery of heading 84.29 or 84.30, forklifts or other industrial handling vehicles and machines','UNT',30,0,0 UNION ALL
Select '4011802900','Other','UNT',5,0,0 UNION ALL

Select '4011901000','Of a kind used on vehicles of Chapter 87','UNT',5,0,0 UNION ALL
Select '4011902000','Of a kind used on machinery of heading 84.29 or 84.30','UNT',5,0,0 UNION ALL
Select '4011903000','Other, of a width exceeding 450 mm','UNT',5,0,0 UNION ALL
Select '4011909000','Other','UNT',5,0,0 UNION ALL


Select '4012110000','Of a kind used on motor cars (including station wagons and racing cars)','UNT',30,0,0 UNION ALL

Select '4012121000','Of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4012129000','Other','UNT',30,0,0 UNION ALL
Select '4012130000','Of a kind used on aircraft','UNT',5,0,0 UNION ALL

Select '4012193000','Of a kind used on machinery of heading 84.29 or 84.30','UNT',30,0,0 UNION ALL
Select '40121940','Of a kind used on other vehicles of Chapter 87:','',0,0,0 UNION ALL
Select '4012194020','Of a kind used on tractors','UNT',30,0,0 UNION ALL
Select '4012194090','Other','UNT',5,0,0 UNION ALL
Select '40121990','Other:','',0,0,0 UNION ALL
Select '4012199040','Of a kind used on forklifts or other industrial handling vehicles and machines','UNT',30,0,0 UNION ALL
Select '4012199090','Other','UNT',5,0,0 UNION ALL

Select '4012201000','Of a kind used on motor cars (including station wagons, racing cars)','UNT',30,0,0 UNION ALL

Select '4012202100','Of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4012202900','Other','UNT',30,0,0 UNION ALL
Select '4012203000','Of a kind used on aircraft','UNT',5,0,0 UNION ALL
Select '4012204000','Of a kind used on motorcycles ','UNT',30,0,0 UNION ALL
Select '4012205000','Of a kind used on bicycles','UNT',30,0,0 UNION ALL
Select '4012206000','Of a kind used on machinery of heading 84.29 or 84.30','UNT',30,0,0 UNION ALL
Select '40122070','Of a kind used on other vehicles of Chapter 87:','',0,0,0 UNION ALL
Select '4012207020','Of a kind used on tractors ','UNT',30,0,0 UNION ALL
Select '4012207090','Other','UNT',5,0,0 UNION ALL

Select '4012209100','Buffed tyres','UNT',30,0,0 UNION ALL
Select '40122099','Other:','',0,0,0 UNION ALL
Select '4012209930','Of a kind used on forklifts or other industrial handling vehicles and machines','UNT',30,0,0 UNION ALL
Select '4012209990','Other','UNT',5,0,0 UNION ALL


Select '4012901400','Solid tyres exceeding 250 mm in external diameter, of a width not exceeding 450 mm','KGM',30,0,0 UNION ALL
Select '4012901500','Solid tyres exceeding 250 mm in external diameter, of a width exceeding 450 mm, for use on vehicles of heading 87.09','KGM',30,0,0 UNION ALL
Select '4012901600','Other solid tyres exceeding 250 mm in external diameter, of a width exceeding 450 mm','KGM',30,0,0 UNION ALL
Select '4012901900','Other','KGM',30,0,0 UNION ALL

Select '4012902100','Of a width not exceeding 450 mm','KGM',5,0,0 UNION ALL
Select '4012902200','Of a width exceeding 450 mm','KGM',5,0,0 UNION ALL
Select '4012907000','Replaceable tyre treads of a width not exceeding 450 mm','KGM',30,0,0 UNION ALL
Select '4012908000','Tyre flaps','KGM',5,0,0 UNION ALL
Select '40129090','Other:','',0,0,0 UNION ALL
Select '4012909010','Replaceable tyre treads of a width exceeding 450 mm','KGM',30,0,0 UNION ALL
Select '4012909090','Other','KGM',5,0,0 UNION ALL



Select '4013101100','Suitable for fitting to tyres of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013101900','Suitable for fitting to tyres of a width exceeding 450 mm','UNT',30,0,0 UNION ALL

Select '4013102100','Suitable for fitting to tyres of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013102900','Suitable for fitting to tyres of a width exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013200000','Of a kind used on bicycles','UNT',30,0,0 UNION ALL


Select '4013901100','Suitable for fitting to tyres of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013901900','Suitable for fitting to tyres of a width exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013902000','Of a kind used on motorcycles','UNT',30,0,0 UNION ALL

Select '4013903100','Suitable for fitting to tyres of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013903900','Suitable for fitting to tyres of a width exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013904000','Of a kind used on aircraft','UNT',5,0,0 UNION ALL

Select '4013909100','Suitable for fitting to tyres of a width not exceeding 450 mm','UNT',30,0,0 UNION ALL
Select '4013909900','Suitable for fitting to tyres of a width exceeding 450 mm','UNT',30,0,0 UNION ALL

Select '4014100000','Sheath contraceptives','KGM',30,0,0 UNION ALL

Select '4014901000','Teats for feeding bottles and similar articles','KGM',10,0,0 UNION ALL
Select '4014904000','Stoppers for pharmaceutical use','KGM',5,0,0 UNION ALL
Select '4014905000','Finger stalls','KGM',0,0,0 UNION ALL
Select '40149090','Other:','',0,0,0 UNION ALL
Select '4014909020','Hot water bottles','KGM',5,0,0 UNION ALL
Select '4014909090','Other','KGM',5,0,0 UNION ALL


Select '40151100','Surgical:','',0,0,0 UNION ALL
Select '4015110010','Of natural rubber','KGM',0,0,0 UNION ALL
Select '4015110090','Other','KGM',0,0,0 UNION ALL
Select '40151900','Other:','',0,0,0 UNION ALL

Select '4015190012','Examination glove including medical glove','KGM',0,0,0 UNION ALL
Select '4015190019','Other','KGM',0,0,0 UNION ALL
Select '4015190090','Other','KGM',0,0,0 UNION ALL

Select '4015901000','Lead aprons','KGM',15,0,0 UNION ALL
Select '4015902000','Divers'' suits (wet suits)','KGM',15,0,0 UNION ALL
Select '4015909000','Other','KGM',15,0,0 UNION ALL


Select '4016101000','Padding for articles of apparel or clothing accessories','KGM',5,0,0 UNION ALL
Select '4016102000','Floor tiles and wall tiles','KGM',30,0,0 UNION ALL
Select '4016109000','Other','KGM',30,0,0 UNION ALL


Select '4016911000','Mats','KGM',5,0,0 UNION ALL
Select '4016912000','Tiles','KGM',5,0,0 UNION ALL
Select '4016919000','Other','KGM',5,0,0 UNION ALL

Select '4016921000','Eraser tips','KGM',0,0,0 UNION ALL
Select '4016929000','Other','KGM',5,0,0 UNION ALL

Select '4016931000','Of a kind used to insulate the terminal leads of electrolytic capacitors','KGM',5,0,0 UNION ALL
Select '4016932000','Gaskets and orings, of a kind used on motor vehicles of heading 87.02, 87.03, 87.04 or 87.11','KGM',5,0,0 UNION ALL
Select '40169390','Other:','',0,0,0 UNION ALL
Select '4016939010','Pipe seal rings','KGM',5,0,0 UNION ALL
Select '4016939090','Other','KGM',5,0,0 UNION ALL
Select '4016940000','Boat or dock fenders, whether or not inflatable','KGM',5,0,0 UNION ALL
Select '4016950000','Other inflatable articles','KGM',5,0,0 UNION ALL


Select '4016991100','For vehicles of heading 87.02, 87.03, 87.04 or 87.05, other than weatherstripping','KGM',25,0,0 UNION ALL
Select '4016991200','For vehicles of heading  87.11','KGM',25,0,0 UNION ALL
Select '4016991300','Weatherstripping, of a kind used on motor vehicles of heading 87.02, 87.03 or 87.04','KGM',25,0,0 UNION ALL
Select '4016991500','For vehicles of heading 87.09, 87.13, 87.15 or 87.16','KGM',5,0,0 UNION ALL
Select '4016991600','Bicycle mudguards ','KGM',30,0,0 UNION ALL
Select '4016991700','Bicycle parts ','KGM',5,0,0 UNION ALL
Select '4016991800','Other bicycle accessories','KGM',25,0,0 UNION ALL
Select '4016991900','Other ','KGM',5,0,0 UNION ALL
Select '4016992000','Parts and accessories of rotochutes of heading 88.04','KGM',5,0,0 UNION ALL
Select '4016993000','Rubber bands','KGM',30,0,0 UNION ALL
Select '4016994000','Wall tiles','KGM',5,0,0 UNION ALL

Select '4016995100','Rubber rollers','KGM',5,0,0 UNION ALL
Select '4016995200','Tyre mould bladders','KGM',5,0,0 UNION ALL
Select '4016995300','Electrical insulator hoods','KGM',5,0,0 UNION ALL
Select '4016995400','Rubber  grommets  and rubber  covers  for  automative wiring harnesses','KGM',5,0,0 UNION ALL
Select '4016995900','Other','KGM',5,0,0 UNION ALL
Select '4016996000','Rail pad','KGM',5,0,0 UNION ALL
Select '4016997000','Structural bearings including bridge bearings','KGM',5,0,0 UNION ALL

Select '4016999100','Table coverings','KGM',5,0,0 UNION ALL
Select '4016999900','Other','KGM',5,0,0 UNION ALL

Select '4017001000','Floor tiles and wall tiles','KGM',5,0,0 UNION ALL
Select '4017002000','Other articles of hard rubber ','KGM',5,0,0 UNION ALL
Select '4017009000','Other','KGM',25,0,0 UNION ALL

Select '4101200000','Whole hides and skins, unsplit, of a weight per skin not exceeding 8 kg when simply dried, 10 kg when drysalted, or 16 kg when fresh, wetsalted or otherwise preserved','KGM',0,0,0 UNION ALL
Select '4101500000','Whole hides and skins, of a weight exceeding 16 kg','KGM',0,0,0 UNION ALL

Select '4101901000','Pretanned','KGM',0,0,0 UNION ALL
Select '4101909000','Other','KGM',0,0,0 UNION ALL

Select '4102100000','With wool on','KGM',0,0,0 UNION ALL

Select '4102210000','Pickled','KGM',0,0,0 UNION ALL
Select '4102290000','Other','KGM',0,0,0 UNION ALL

Select '41032000','Of reptiles:','',0,0,0 UNION ALL
Select '4103200020','Crocodiles','KGM',0,0,0 UNION ALL
Select '4103200090','Other','KGM',0,0,0 UNION ALL
Select '4103300000','Of swine','KGM',0,0,0 UNION ALL
Select '4103900000','Other','KGM',0,0,0 UNION ALL



Select '4104111000','Of bovine, vegetable tanned','KGM',0,0,0 UNION ALL
Select '4104119000','Other','KGM',0,0,0 UNION ALL
Select '4104190000','Other','KGM',0,0,0 UNION ALL

Select '4104410000','Full grains, unsplit; grain splits ','KGM',0,0,0 UNION ALL
Select '4104490000','Other','KGM',0,0,0 UNION ALL

Select '4105100000','In the wet state (including wetblue)','KGM',0,0,0 UNION ALL
Select '4105300000','In the dry state (crust)','KGM',0,0,0 UNION ALL


Select '4106210000','In the wet state (including wetblue)','KGM',0,0,0 UNION ALL
Select '4106220000','In the dry state (crust)','KGM',0,0,0 UNION ALL

Select '4106310000','In the wet state (including wetblue)','KGM',10,0,0 UNION ALL
Select '4106320000','In the dry state (crust)','KGM',10,0,0 UNION ALL
Select '4106400000','Of reptiles','KGM',0,0,0 UNION ALL

Select '4106910000','In the wet state (including wetblue)','KGM',0,0,0 UNION ALL
Select '4106920000','In the dry state (crust)','KGM',0,0,0 UNION ALL


Select '4107110000','Full grains, unsplit','KGM',0,0,0 UNION ALL
Select '4107120000','Grain splits','KGM',0,0,0 UNION ALL
Select '4107190000','Other','KGM',0,0,0 UNION ALL

Select '4107910000','Full grains, unsplit','KGM',0,0,0 UNION ALL
Select '4107920000','Grain splits','KGM',0,0,0 UNION ALL
Select '4107990000','Other','KGM',0,0,0 UNION ALL

Select '4112000000','Leather further prepared after tanning or crusting, including parchmentdressed leather, of sheep or lamb, without wool on, whether or not split, other than leather of heading 41.14.','KGM',0,0,0 UNION ALL

Select '4113100000','Of goats or kids','KGM',0,0,0 UNION ALL
Select '4113200000','Of swine','KGM',10,0,0 UNION ALL
Select '4113300000','Of reptiles','KGM',0,0,0 UNION ALL
Select '4113900000','Other','KGM',0,0,0 UNION ALL

Select '4114100000','Chamois (including combination chamois) leather','KGM',0,0,0 UNION ALL
Select '4114200000','Patent leather and patent laminated leather; metallised leather','KGM',0,0,0 UNION ALL

Select '4115100000','Composition leather with a basis of leather or leather fibre, in slabs, sheets or strip, whether or not in rolls','KGM',0,0,0 UNION ALL
Select '4115200000','Parings and other waste of leather or of composition leather, not suitable for the manufacture of leather articles; leather dust, powder and flour','KGM',0,0,0 UNION ALL

Select '4201000000','Saddlery and harness for any animal (including traces, leads, knee pads, muzzles, saddle cloths, saddle bags, dog coats and the like), of any material.','KGM',0,0,0 UNION ALL



Select '4202111000','Suitcase or briefcase with maximum dimensions of 56 cm x 45 cm x 25 cm','UNT',0,0,0 UNION ALL
Select '4202119000','Other','UNT',0,0,0 UNION ALL


Select '4202121100','With outer surface of vulcanised fibre','UNT',0,0,0 UNION ALL
Select '4202121900','Other','UNT',0,0,0 UNION ALL

Select '4202129100','With outer surface of vulcanised fibre','UNT',0,0,0 UNION ALL
Select '4202129900','Other','UNT',0,0,0 UNION ALL

Select '4202192000','With outer surface of paperboard','UNT',0,0,0 UNION ALL
Select '4202199000','Other','UNT',0,0,0 UNION ALL

Select '4202210000','With outer surface of leather or of composition leather','UNT',0,0,0 UNION ALL
Select '4202220000','With outer surface of sheeting of plastics or of textile materials','UNT',0,0,0 UNION ALL
Select '4202290000','Other','UNT',0,0,0 UNION ALL

Select '4202310000','With outer surface of leather or of composition leather','KGM',0,0,0 UNION ALL
Select '4202320000','With outer surface of sheeting of plastics or of textile materials','KGM',0,0,0 UNION ALL
Select '4202390000','Other','KGM',0,0,0 UNION ALL



Select '4202911100','Bowling bags','KGM',0,0,0 UNION ALL
Select '4202911900','Other','KGM',0,0,0 UNION ALL
Select '4202919000','Other','KGM',0,0,0 UNION ALL

Select '4202921000','Toiletry bags, of sheeting of plastic','KGM',0,0,0 UNION ALL
Select '4202922000','Bowling bags','KGM',0,0,0 UNION ALL
Select '4202929000','Other','KGM',0,0,0 UNION ALL

Select '4202991000','With outer surface of vulcanised fibre or paperboard','KGM',0,0,0 UNION ALL
Select '4202992000','Of copper','KGM',0,0,0 UNION ALL
Select '4202999000','Other','KGM',0,0,0 UNION ALL

Select '4203100000','Articles of apparel','KGM',0,0,0 UNION ALL

Select '4203210000','Specially designed for use in sports','KGM',0,0,0 UNION ALL

Select '4203291000','Protective work gloves','KGM',0,0,0 UNION ALL
Select '4203299000','Other','KGM',0,0,0 UNION ALL
Select '4203300000','Belts and bandoliers','KGM',0,0,0 UNION ALL
Select '4203400000','Other clothing accessories','KGM',0,0,0 UNION ALL

Select '4205001000','Boot laces; mats','KGM',0,0,0 UNION ALL
Select '4205002000','Industrial safety belts and harnesses','KGM',0,0,0 UNION ALL
Select '4205003000','Leather strings or chords of a kind used for jewellery or articles of personal adornment','KGM',0,0,0 UNION ALL
Select '4205004000','Other articles of a kind used in machinery or mechanical appliances or for other technical uses','KGM',0,0,0 UNION ALL
Select '4205009000','Other','KGM',0,0,0 UNION ALL

Select '4206000000','Articles of gut (other than silkworm gut), of goldbeater''s skin, of bladders or of tendons.','KGM',0,0,0 UNION ALL

Select '4301100000','Of mink, whole, with or without head, tail or paws','KGM',0,0,0 UNION ALL
Select '4301300000','Of lamb, the following: Astrakhan, Broadtail, Caracul, Persian and similar lamb, Indian, Chinese, Mongolian or Tibetan lamb, whole, with or without head, tail or paws','KGM',0,0,0 UNION ALL
Select '4301600000','Of fox, whole, with or without head, tail or paws','KGM',0,0,0 UNION ALL
Select '4301800000','Other furskins, whole, with or without head, tail or paws','KGM',0,0,0 UNION ALL
Select '4301900000','Heads, tails, paws and other pieces or cuttings, suitable for furriers''  use','KGM',0,0,0 UNION ALL


Select '4302110000','Of mink','KGM',0,0,0 UNION ALL
Select '4302190000','Other','KGM',0,0,0 UNION ALL
Select '4302200000','Heads, tails, paws and other pieces or cuttings, not assembled','KGM',0,0,0 UNION ALL
Select '4302300000','Whole skins and pieces or cuttings thereof, assembled','KGM',0,0,0 UNION ALL

Select '4303100000','Articles of apparel and clothing accessories','KGM',0,0,0 UNION ALL

Select '4303902000','Articles for industrial uses','KGM',0,0,0 UNION ALL
Select '4303909000','Other','KGM',0,0,0 UNION ALL

Select '4304001000','Artificial fur','KGM',0,0,0 UNION ALL
Select '4304002000','Articles for industrial uses','KGM',0,0,0 UNION ALL

Select '4304009100','Sports bags','KGM',0,0,0 UNION ALL
Select '4304009900','Other','KGM',0,0,0 UNION ALL


Select '4401110000','Coniferous ','KGM',20,0,0 UNION ALL
Select '4401120000','Nonconiferous','KGM',20,0,0 UNION ALL

Select '4401210000','Coniferous','KGM',20,0,0 UNION ALL
Select '4401220000','Nonconiferous','KGM',20,0,0 UNION ALL

Select '4401310000','Wood pellets','KGM',20,0,0 UNION ALL
Select '4401390000','Other','KGM',20,0,0 UNION ALL
Select '4401400000','Sawdust and wood waste and scrap, not agglomerated','KGM',20,0,0 UNION ALL

Select '4402100000','Of bamboo','KGM',20,0,0 UNION ALL

Select '4402901000','Of coconut shell ','KGM',20,0,0 UNION ALL
Select '4402909000','Other','KGM',20,0,0 UNION ALL



Select '44031110','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL
Select '4403111010','Of Damar Minyak','MTQ',0,15,0 UNION ALL
Select '4403111020','Of Podo','MTQ',0,15,0 UNION ALL
Select '4403111030','Of Sempilor','MTQ',0,15,0 UNION ALL
Select '4403111090','Other','MTQ',0,15,0 UNION ALL
Select '4403119000','Other','MTQ',0,0,0 UNION ALL

Select '44031210','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL

Select '4403121011','Of Balau','MTQ',0,15,0 UNION ALL
Select '4403121012','Of Belian','MTQ',0,15,0 UNION ALL
Select '4403121013','Of Bitis','MTQ',0,15,0 UNION ALL
Select '4403121014','Of Chengal','MTQ',0,15,0 UNION ALL
Select '4403121015','Of Kekatong','MTQ',0,15,0 UNION ALL
Select '4403121016','Of Keranji','MTQ',0,15,0 UNION ALL
Select '4403121017','Of Penaga','MTQ',0,15,0 UNION ALL
Select '4403121018','Of Resak','MTQ',0,15,0 UNION ALL
Select '4403121019','Other','MTQ',0,15,0 UNION ALL

Select '4403121021','Of Kayu Malam','MTQ',0,15,0 UNION ALL
Select '4403121022','Of Kulim','MTQ',0,15,0 UNION ALL
Select '4403121023','Of Mempening','MTQ',0,15,0 UNION ALL
Select '4403121024','Of Mengkulang (Kembang)','MTQ',0,15,0 UNION ALL
Select '4403121025','Of Merawan (Gagil)','MTQ',0,15,0 UNION ALL
Select '4403121026','Of Rengas','MTQ',0,15,0 UNION ALL
Select '4403121027','Of Simpoh','MTQ',0,15,0 UNION ALL
Select '4403121028','Of Tualang (Tapang)','MTQ',0,15,0 UNION ALL
Select '4403121029','Other','MTQ',0,15,0 UNION ALL

Select '4403121031','Of Acacia Mangium','MTQ',0,15,0 UNION ALL
Select '4403121032','Of Binuang','MTQ',0,15,0 UNION ALL
Select '4403121033','Of Geronggang (Serungan)','MTQ',0,15,0 UNION ALL
Select '4403121034','Of Laran','MTQ',0,15,0 UNION ALL
Select '4403121035','Of Medang','MTQ',0,15,0 UNION ALL
Select '4403121036','Of Melunak','MTQ',0,15,0 UNION ALL
Select '4403121037','Of Nyatoh','MTQ',0,15,0 UNION ALL
Select '4403121038','Of Sepetir','MTQ',0,15,0 UNION ALL
Select '4403121039','Other','MTQ',0,15,0 UNION ALL
Select '4403121090','Other','MTQ',0,15,0 UNION ALL
Select '4403129000','Other','MTQ',0,0,0 UNION ALL


Select '4403211000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403219000','Other','MTQ',0,0,0 UNION ALL

Select '4403221000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403229000','Other','MTQ',0,0,0 UNION ALL

Select '4403231000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403239000','Other','MTQ',0,0,0 UNION ALL

Select '4403241000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403249000','Other','MTQ',0,0,0 UNION ALL

Select '44032510','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL
Select '4403251010','Of Damar Minyak','MTQ',0,15,0 UNION ALL
Select '4403251011','Of Podo','MTQ',0,15,0 UNION ALL
Select '4403251012','Of Sempilor','MTQ',0,15,0 UNION ALL
Select '4403251013','Other','MTQ',0,15,0 UNION ALL
Select '4403259000','Other','MTQ',0,0,0 UNION ALL

Select '44032610','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL
Select '4403261010','Of Damar Minyak','MTQ',0,15,0 UNION ALL
Select '4403261011','Of Podo','MTQ',0,15,0 UNION ALL
Select '4403261012','Of Sempilor','MTQ',0,15,0 UNION ALL
Select '4403261013','Other','MTQ',0,15,0 UNION ALL
Select '4403269000','Other','MTQ',0,0,0 UNION ALL


Select '4403411000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403419000','Other','MTQ',0,0,0 UNION ALL

Select '44034910','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL

Select '4403491011','Of Balau (Selangan Batu)','MTQ',0,15,0 UNION ALL
Select '4403491012','Of Belian','MTQ',0,15,0 UNION ALL
Select '4403491013','Of Chengal','MTQ',0,15,0 UNION ALL
Select '4403491014','Of Kekatong','MTQ',0,15,0 UNION ALL
Select '4403491015','Of Keranji','MTQ',0,15,0 UNION ALL
Select '4403491016','Of Penaga','MTQ',0,15,0 UNION ALL
Select '4403491017','Of Red Balau (Selangan Batu Merah)','MTQ',0,15,0 UNION ALL
Select '4403491018','Of Resak','MTQ',0,15,0 UNION ALL
Select '4403491019','Other','MTQ',0,15,0 UNION ALL

Select '4403491021','Of Kayu Malam','MTQ',0,15,0 UNION ALL
Select '4403491022','Of Kulim','MTQ',0,15,0 UNION ALL
Select '4403491023','Of Mempening','MTQ',0,15,0 UNION ALL
Select '4403491024','Of Mengkulang (Kembang)','MTQ',0,15,0 UNION ALL
Select '4403491025','Of Merawan (Gagil)','MTQ',0,15,0 UNION ALL
Select '4403491026','Of Rengas','MTQ',0,15,0 UNION ALL
Select '4403491027','Of Simpoh','MTQ',0,15,0 UNION ALL
Select '4403491028','Of Tualang (Tapang)','MTQ',0,15,0 UNION ALL
Select '4403491029','Other','MTQ',0,15,0 UNION ALL

Select '4403491031','Of Balau (Selangan Batu)','MTQ',0,15,0 UNION ALL
Select '4403491032','Of Belian','MTQ',0,15,0 UNION ALL
Select '4403491033','Of Chengal','MTQ',0,15,0 UNION ALL
Select '4403491034','Of Kekatong','MTQ',0,15,0 UNION ALL
Select '4403491035','Of Keranji','MTQ',0,15,0 UNION ALL
Select '4403491036','Of Penaga','MTQ',0,15,0 UNION ALL
Select '4403491037','Of Red Balau (Selangan Batu Merah)','MTQ',0,15,0 UNION ALL
Select '4403491038','Of Resak','MTQ',0,15,0 UNION ALL
Select '4403491039','Other','MTQ',0,15,0 UNION ALL

Select '4403491041','Of Kayu Malam','MTQ',0,15,0 UNION ALL
Select '4403491042','Of Kulim','MTQ',0,15,0 UNION ALL
Select '4403491043','Of Mempening','MTQ',0,15,0 UNION ALL
Select '4403491044','Of Mengkulang (Kembang)','MTQ',0,15,0 UNION ALL
Select '4403491045','Of Merawan (Gagil)','MTQ',0,15,0 UNION ALL
Select '4403491046','Of Rengas','MTQ',0,15,0 UNION ALL
Select '4403491047','Of Simpoh','MTQ',0,15,0 UNION ALL
Select '4403491048','Of Tualang (Tapang)','MTQ',0,15,0 UNION ALL
Select '4403491049','Other','MTQ',0,15,0 UNION ALL

Select '4403491051','Of Acacia Mangium','MTQ',0,15,0 UNION ALL
Select '4403491052','Of Binuang','MTQ',0,15,0 UNION ALL
Select '4403491053','Of Geronggang (Serungan)','MTQ',0,15,0 UNION ALL
Select '4403491054','Of Laran','MTQ',0,15,0 UNION ALL
Select '4403491055','Of Medang','MTQ',0,15,0 UNION ALL
Select '4403491056','Of Melunak','MTQ',0,15,0 UNION ALL
Select '4403491057','Of Nyatoh','MTQ',0,15,0 UNION ALL
Select '4403491058','Of Sepetir','MTQ',0,15,0 UNION ALL
Select '4403491059','Other','MTQ',0,15,0 UNION ALL
Select '4403491090','Other','MTQ',0,15,0 UNION ALL
Select '4403499000','Other','MTQ',0,0,0 UNION ALL


Select '4403911000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403919000','Other','MTQ',0,0,0 UNION ALL

Select '4403931000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403939000','Other','MTQ',0,0,0 UNION ALL

Select '4403941000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403949000','Other','MTQ',0,0,0 UNION ALL

Select '4403951000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403959000','Other','MTQ',0,0,0 UNION ALL

Select '4403961000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403969000','Other','MTQ',0,0,0 UNION ALL

Select '4403971000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403979000','Other','MTQ',0,0,0 UNION ALL

Select '4403981000','Baulks, sawlogs and veneer logs','MTQ',0,15,0 UNION ALL
Select '4403989000','Other','MTQ',0,0,0 UNION ALL

Select '44039910','Baulks, sawlogs and veneer logs:','',0,0,0 UNION ALL

Select '4403991006','Of Giam','MTQ',0,15,0 UNION ALL
Select '4403991011','Of Penyau','MTQ',0,15,0 UNION ALL
Select '4403991012','Of Belian','MTQ',0,15,0 UNION ALL
Select '4403991013','Of Chengal','MTQ',0,15,0 UNION ALL
Select '4403991014','Of Kekatong','MTQ',0,15,0 UNION ALL
Select '4403991015','Other','MTQ',0,15,0 UNION ALL
Select '4403991016','Of Bekak','MTQ',0,15,0 UNION ALL
Select '4403991017','Of Penyau','MTQ',0,15,0 UNION ALL
Select '4403991018','Of Resak','MTQ',0,15,0 UNION ALL
Select '4403991019','Other','MTQ',0,15,0 UNION ALL

Select '4403991020','Of Kandis','MTQ',0,15,0 UNION ALL
Select '4403991021','Of Kayu Malam','MTQ',0,15,0 UNION ALL
Select '4403991022','Of Kelat','MTQ',0,15,0 UNION ALL
Select '4403991023','Of Kulim','MTQ',0,15,0 UNION ALL
Select '4403991024','Of Mempening','MTQ',0,15,0 UNION ALL
Select '4403991025','Of Ranggu','MTQ',0,15,0 UNION ALL
Select '4403991026','Of Rengas','MTQ',0,15,0 UNION ALL
Select '4403991027','Of Simpoh','MTQ',0,15,0 UNION ALL
Select '4403991028','Of Tualang (Tapang)','MTQ',0,15,0 UNION ALL
Select '4403991029','Other','MTQ',0,15,0 UNION ALL

Select '4403991031','Of Acacia Mangium','MTQ',0,15,0 UNION ALL
Select '4403991032','Of Merbatu','MTQ',0,15,0 UNION ALL
Select '4403991033','Of Binuang','MTQ',0,15,0 UNION ALL
Select '4403991034','Of Mertas','MTQ',0,15,0 UNION ALL
Select '4403991035','Of Nyalin','MTQ',0,15,0 UNION ALL
Select '4403991036','Of Melunak','MTQ',0,15,0 UNION ALL
Select '4403991037','Of Perah','MTQ',0,15,0 UNION ALL
Select '4403991038','Of Rubberwood','MTQ',0,15,0 UNION ALL
Select '4403991039','Other','MTQ',0,15,0 UNION ALL
Select '4403991047','Of Tulang Daing (Kedang Belum)','MTQ',0,15,0 UNION ALL
Select '4403991049','Other','MTQ',0,15,0 UNION ALL

Select '4403991051','Of Ara','MTQ',0,15,0 UNION ALL
Select '4403991056','Of Berangan','MTQ',0,15,0 UNION ALL
Select '4403991067','Of Ketapang','MTQ',0,15,0 UNION ALL
Select '4403991069','Of Laran','MTQ',0,15,0 UNION ALL
Select '4403991076','Of Mempisang (Karai)','MTQ',0,15,0 UNION ALL
Select '4403991082','Of Penarahan (Kumpang)','MTQ',0,15,0 UNION ALL
Select '4403991083','Of Perupok','MTQ',0,15,0 UNION ALL
Select '4403991090','Other','MTQ',0,15,0 UNION ALL
Select '4403991092','Of Terentang','MTQ',0,15,0 UNION ALL
Select '4403991095','Other','MTQ',0,15,0 UNION ALL
Select '4403999000','Other','MTQ',0,0,0 UNION ALL

Select '44041000','Coniferous:','',0,0,0 UNION ALL
Select '4404100010','Chipwood','KGM',20,0,0 UNION ALL
Select '4404100090','Other','KGM',20,0,0 UNION ALL

Select '4404201000','Chipwood','KGM',20,0,0 UNION ALL
Select '4404209000','Other','KGM',10,0,0 UNION ALL

Select '4405001000','Wood wool','KGM',20,0,0 UNION ALL
Select '4405002000','Wood flour','KGM',5,0,0 UNION ALL


Select '4406110000','Coniferous 
','MTQ',5,0,0 UNION ALL
Select '44061200','Nonconiferous:
','',0,0,0 UNION ALL

Select '4406120011','Of Keruing','MTQ',5,0,0 UNION ALL
Select '4406120012','Of Kempas (Menggeris)','MTQ',5,0,0 UNION ALL
Select '4406120013','Of Kapur','MTQ',5,0,0 UNION ALL
Select '4406120019','Other','MTQ',5,0,0 UNION ALL

Select '4406120021','Of Balau (Selangan Batu)','MTQ',5,0,0 UNION ALL
Select '4406120022','Of Chengal','MTQ',5,0,0 UNION ALL
Select '4406120023','Of Merbau','MTQ',5,0,0 UNION ALL
Select '4406120029','Other','MTQ',5,0,0 UNION ALL

Select '4406910000','Coniferous 
','MTQ',5,0,0 UNION ALL
Select '44069200','Nonconiferous:','',0,0,0 UNION ALL

Select '4406920011','Of Keruing','MTQ',5,0,0 UNION ALL
Select '4406920012','Of Kempas (Menggeris)','MTQ',5,0,0 UNION ALL
Select '4406920013','Of Kapur','MTQ',5,0,0 UNION ALL
Select '4406920014','Other','MTQ',5,0,0 UNION ALL

Select '4406920021','Of Balau (Selangan Batu)','MTQ',5,0,0 UNION ALL
Select '4406920022','Of Chengal','MTQ',5,0,0 UNION ALL
Select '4406920023','Of Merbau','MTQ',5,0,0 UNION ALL
Select '4406920024','Other','MTQ',5,0,0 UNION ALL


Select '44071100','Of pine (Pinus spp.):
','',0,0,0 UNION ALL
Select '4407110010','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407110020','Sliced or peeled ','MTQ',0,0,0 UNION ALL
Select '4407110090','Other','MTQ',0,0,0 UNION ALL
Select '44071200','Of fir (Abies spp.) and spruce (Picea spp.):
','',0,0,0 UNION ALL
Select '4407120010','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407120020','Sliced or peeled ','MTQ',0,0,0 UNION ALL
Select '4407120090','Other','MTQ',0,0,0 UNION ALL
Select '44071900','Other:','',0,0,0 UNION ALL
Select '4407190010','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407190020','Sliced or peeled ','MTQ',0,0,0 UNION ALL
Select '4407190090','Other','MTQ',0,0,0 UNION ALL


Select '4407211000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072190','Other:','',0,0,0 UNION ALL
Select '4407219040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407219050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407219090','Other','MTQ',0,0,0 UNION ALL

Select '4407221000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072290','Other:','',0,0,0 UNION ALL
Select '4407229040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407229050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407229090','Other','MTQ',0,0,0 UNION ALL


Select '4407251100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072519','Other:','',0,0,0 UNION ALL
Select '4407251940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407251950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407251990','Other','MTQ',0,0,0 UNION ALL

Select '4407252100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072529','Other:','',0,0,0 UNION ALL
Select '4407252940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407252950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407252990','Other','MTQ',0,0,0 UNION ALL

Select '4407261000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072690','Other:','',0,0,0 UNION ALL
Select '4407269040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407269050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407269090','Other','MTQ',0,0,0 UNION ALL

Select '4407271000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072790','Other:','',0,0,0 UNION ALL
Select '4407279040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407279050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407279090','Other','MTQ',0,0,0 UNION ALL

Select '4407281000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072890','Other:','',0,0,0 UNION ALL
Select '4407289040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407289050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407289090','Other','MTQ',0,0,0 UNION ALL


Select '4407291100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072919','Other:','',0,0,0 UNION ALL
Select '4407291940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407291950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407291990','Other','MTQ',0,0,0 UNION ALL

Select '4407292100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072929','Other:','',0,0,0 UNION ALL
Select '4407292940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407292950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407292990','Other','MTQ',0,0,0 UNION ALL

Select '4407293100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072939','Other:','',0,0,0 UNION ALL
Select '4407293940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407293950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407293990','Other','MTQ',0,0,0 UNION ALL

Select '4407294100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072949','Other:','',0,0,0 UNION ALL
Select '4407294940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407294950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407294990','Other','MTQ',0,0,0 UNION ALL

Select '4407295100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072959','Other:','',0,0,0 UNION ALL
Select '4407295940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407295950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407295990','Other','MTQ',0,0,0 UNION ALL

Select '4407296100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072969','Other:','',0,0,0 UNION ALL
Select '4407296940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407296950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407296990','Other','MTQ',0,0,0 UNION ALL

Select '4407297100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072979','Other:','',0,0,0 UNION ALL
Select '4407297940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407297950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407297990','Other','MTQ',0,0,0 UNION ALL

Select '4407298100','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072989','Other:','',0,0,0 UNION ALL
Select '4407298940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407298950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407298990','Other','MTQ',0,0,0 UNION ALL

Select '4407299100','Jongkong (Dactylocladus spp.) and Merbau (Intsia spp.), planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072992','Jongkong (Dactylocladus spp.) and Merbau (Intsia spp.), other:','',0,0,0 UNION ALL
Select '4407299241','Sawn lengthwise of Jongkong','MTQ',0,0,0 UNION ALL
Select '4407299242','Sawn lengthwise of Merbau','MTQ',0,0,0 UNION ALL
Select '4407299250','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407299290','Other','MTQ',0,0,0 UNION ALL
Select '4407299400','Albizia (Paraserianthes falcataria), planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072995','Albizia (Paraserianthes falcataria), other:','',0,0,0 UNION ALL
Select '4407299510','Sawn lengthwise ','MTQ',0,0,0 UNION ALL
Select '4407299520','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407299590','Other','MTQ',0,0,0 UNION ALL
Select '4407299600','Rubber (Hevea Brasiliensis), planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072997','Rubber (Hevea Brasiliensis), other:','',0,0,0 UNION ALL
Select '4407299710','Sawn lengthwise ','MTQ',0,0,0 UNION ALL
Select '4407299720','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407299790','Other','MTQ',0,0,0 UNION ALL
Select '4407299800','Other, planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44072999','Other:','',0,0,0 UNION ALL
Select '4407299940','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407299950','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407299990','Other','MTQ',0,0,0 UNION ALL


Select '4407911000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079190','Other:','',0,0,0 UNION ALL
Select '4407919040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407919050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407919090','Other','MTQ',0,0,0 UNION ALL

Select '4407921000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079290','Other:','',0,0,0 UNION ALL
Select '4407929040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407929050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407929090','Other','MTQ',0,0,0 UNION ALL

Select '4407931000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079390','Other:','',0,0,0 UNION ALL
Select '4407939040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407939050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407939090','Other','MTQ',0,0,0 UNION ALL

Select '4407941000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079490','Other:','',0,0,0 UNION ALL
Select '4407949040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407949050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407949090','Other','MTQ',0,0,0 UNION ALL

Select '4407951000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079590','Other:','',0,0,0 UNION ALL
Select '4407959040','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407959050','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407959090','Other','MTQ',0,0,0 UNION ALL

Select '4407961000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079690','Other :','',0,0,0 UNION ALL
Select '4407969010','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407969020','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407969090','Other','MTQ',0,0,0 UNION ALL

Select '4407971000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079790','Other :','',0,0,0 UNION ALL
Select '4407979010','Sawn lengthwise','MTQ',0,0,0 UNION ALL
Select '4407979020','Sliced or peeled','MTQ',0,0,0 UNION ALL
Select '4407979090','Other','MTQ',0,0,0 UNION ALL

Select '4407991000','Planed, sanded or endjointed','MTQ',0,0,0 UNION ALL
Select '44079990','Other:','',0,0,0 UNION ALL

Select '4407999006','Of Giam','MTQ',0,0,0 UNION ALL
Select '4407999011','Of Penyau','MTQ',0,0,0 UNION ALL
Select '4407999012','Of Bitis','MTQ',0,0,0 UNION ALL
Select '4407999013','Of Chengal','MTQ',0,0,0 UNION ALL
Select '4407999014','Of Kekatong','MTQ',0,0,0 UNION ALL
Select '4407999015','Other','MTQ',0,0,0 UNION ALL

Select '4407999016','Of Penaga','MTQ',0,0,0 UNION ALL
Select '4407999017','Of Resak','MTQ',0,0,0 UNION ALL
Select '4407999018','Of Tembusu','MTQ',0,0,0 UNION ALL
Select '4407999019','Other','MTQ',0,0,0 UNION ALL
Select '4407999020','Of Kandis','MTQ',0,0,0 UNION ALL
Select '4407999021','Of Kasai','MTQ',0,0,0 UNION ALL
Select '4407999022','Of Kayu Malam','MTQ',0,0,0 UNION ALL
Select '4407999023','Of Kelat','MTQ',0,0,0 UNION ALL
Select '4407999024','Of Keledang','MTQ',0,0,0 UNION ALL
Select '4407999025','Of Perah','MTQ',0,0,0 UNION ALL
Select '4407999026','Of Rengas','MTQ',0,0,0 UNION ALL
Select '4407999027','Of Simpoh','MTQ',0,0,0 UNION ALL
Select '4407999028','Of Tualang','MTQ',0,0,0 UNION ALL
Select '4407999029','Other','MTQ',0,0,0 UNION ALL
Select '4407999031','Of Acacia Mangium','MTQ',0,0,0 UNION ALL
Select '4407999032','Of Bintangor','MTQ',0,0,0 UNION ALL
Select '4407999033','Of Durian','MTQ',0,0,0 UNION ALL
Select '4407999034','Of Gerutu','MTQ',0,0,0 UNION ALL
Select '4407999035','Of Kedondong','MTQ',0,0,0 UNION ALL
Select '4407999036','Of Medang','MTQ',0,0,0 UNION ALL
Select '4407999037','Of Melunak','MTQ',0,0,0 UNION ALL
Select '4407999038','Of Pelajau','MTQ',0,0,0 UNION ALL
Select '4407999039','Other','MTQ',0,0,0 UNION ALL

Select '4407999051','Of Ara','MTQ',0,0,0 UNION ALL
Select '4407999056','Of Berangan','MTQ',0,0,0 UNION ALL
Select '4407999067','Of Ketapang','MTQ',0,0,0 UNION ALL
Select '4407999069','Of Laran','MTQ',0,0,0 UNION ALL
Select '4407999076','Of Mempisang (Karai)','MTQ',0,0,0 UNION ALL
Select '4407999082','Of Penarahan (Kumpang)','MTQ',0,0,0 UNION ALL
Select '4407999083','Of Perupok','MTQ',0,0,0 UNION ALL
Select '4407999090','Other','MTQ',0,0,0 UNION ALL
Select '4407999092','Of Terentang','MTQ',0,0,0 UNION ALL
Select '4407999095','Other','MTQ',0,0,0 UNION ALL


Select '4408101000','Cedar wood pencil slats; Radiata pinewood of a kind used for blockboard manufacture','MTQ',0,0,0 UNION ALL
Select '4408103000','Face veneer sheets','MTQ',0,0,0 UNION ALL
Select '44081090','Other:','',0,0,0 UNION ALL
Select '4408109030','Core veneer ','MTQ',0,0,0 UNION ALL
Select '4408109090','Other','MTQ',0,0,0 UNION ALL

Select '44083100','Dark Red Meranti, Light Red Meranti and Meranti Bakau:','',0,0,0 UNION ALL
Select '4408310010','Face veneer sheets','MTQ',0,0,0 UNION ALL
Select '4408310020','Core veneer','MTQ',0,0,0 UNION ALL
Select '4408310090','Other','MTQ',0,0,0 UNION ALL

Select '4408391000','Jelutong wood pencil slats ','MTQ',0,0,0 UNION ALL
Select '4408392000','Face veneer sheets','MTQ',0,0,0 UNION ALL
Select '44083990','Other:','',0,0,0 UNION ALL
Select '4408399093','Core veneer ','MTQ',0,0,0 UNION ALL
Select '4408399099','Other','MTQ',0,0,0 UNION ALL

Select '4408901000','Face veneer sheets','MTQ',0,0,0 UNION ALL
Select '44089090','Other:','',0,0,0 UNION ALL
Select '4408909093','Core veneer ','MTQ',0,0,0 UNION ALL
Select '4408909099','Other','MTQ',0,0,0 UNION ALL

Select '44091000','Coniferous:','',0,0,0 UNION ALL
Select '4409100010','Moulded','MTQ',0,0,0 UNION ALL
Select '4409100020','Rounded','MTQ',0,0,0 UNION ALL
Select '4409100030','Strip and friezes for parquet flooring, not assembled','MTQ',0,0,0 UNION ALL
Select '4409100090','Other','MTQ',0,0,0 UNION ALL

Select '44092100','Of bamboo:','',0,0,0 UNION ALL
Select '4409210010','Moulded','MTQ',0,0,0 UNION ALL
Select '4409210020','Rounded','MTQ',0,0,0 UNION ALL
Select '4409210090','Other','MTQ',0,0,0 UNION ALL
Select '44092200','Of tropical wood:','',0,0,0 UNION ALL
Select '4409220010','Moulded','MTQ',0,0,0 UNION ALL
Select '4409220020','Rounded','MTQ',0,0,0 UNION ALL
Select '4409220030','Strip and friezes for parquet flooring, not assembled','MTQ',0,0,0 UNION ALL
Select '4409220090','Other','MTQ',0,0,0 UNION ALL
Select '44092900','Other:','',0,0,0 UNION ALL
Select '4409290010','Moulded','MTQ',0,0,0 UNION ALL
Select '4409290020','Rounded','MTQ',0,0,0 UNION ALL
Select '4409290030','Strip and friezes for parquet flooring, not assembled','MTQ',0,0,0 UNION ALL
Select '4409290090','Other','MTQ',0,0,0 UNION ALL


Select '4410110000','Particle board','MTQ',20,0,0 UNION ALL
Select '4410120000','Oriented stand board (OSB)','MTQ',20,0,0 UNION ALL
Select '4410190000','Other','MTQ',20,0,0 UNION ALL
Select '4410900000','Other','MTQ',20,0,0 UNION ALL


Select '4411120000','Of a thickness not exceeding 5 mm','MTQ',20,0,0 UNION ALL
Select '4411130000','Of a thickness exceeding 5 mm but not exceeding 9 mm','MTQ',20,0,0 UNION ALL
Select '4411140000','Of a thickness exceeding 9 mm','MTQ',20,0,0 UNION ALL

Select '4411920000','Of a density exceeding 0.8 g/cm3','MTQ',20,0,0 UNION ALL
Select '4411930000','Of a density exceeding 0.5 g/cm3 but not exceeding 0.8 g/cm3','MTQ',20,0,0 UNION ALL
Select '4411940000','Of a density not exceeding 0.5 g/cm3','MTQ',20,0,0 UNION ALL

Select '4412100000','Of bamboo','MTQ',35,0,0 UNION ALL

Select '4412310000','With at least one outer ply of tropical wood','MTQ',35,0,0 UNION ALL
Select '4412330000','Other, with at least one outer ply of nonconiferous wood of the species alder
(Alnus spp.), ash (Fraxinus spp.), beech (Fagus spp.), birch (Betula spp.),
cherry (Prunus spp.), chestnut (Castanea spp.), elm (Ulmus spp.), eucalyptus
(Eucalyptus spp.), hickory (Carya spp.), horse chestnut (Aesculus spp.), lime
(Tilia spp.), maple (Acer spp.), oak (Quercus spp.), plane tree (Platanus spp.), poplar and aspen (Populus spp.), robinia (Robinia spp.), tulipwood (Liriodendron
spp.) or walnut (Juglans spp.)','MTQ',35,0,0 UNION ALL
Select '4412340000','Other, with at least one outer ply of nonconiferous wood not specified under
subheading 4412.33
','MTQ',35,0,0 UNION ALL
Select '4412390000','Other, with both outer plies of coniferous wood�','MTQ',35,0,0 UNION ALL

Select '4412940000','Blockboard, laminboard and battenboard','MTQ',35,0,0 UNION ALL

Select '4412991000','With at least one side faced with plastics','MTQ',30,0,0 UNION ALL
Select '4412992000','With at least one side faced with teak','MTQ',35,0,0 UNION ALL
Select '4412993000','With at least one  side faced with other tropical wood','MTQ',40,0,0 UNION ALL
Select '4412999000','Other','MTQ',25,0,0 UNION ALL

Select '4413000000','Densified wood, in blocks, plates, strips or profile shapes.','KGM',20,0,0 UNION ALL

Select '4414000000','Wooden frames for paintings, photographs, mirrors or similar objects.','KGM',20,0,0 UNION ALL

Select '4415100000','Cases, boxes, crates, drums and similar packings; cabledrums','UNT',20,0,0 UNION ALL
Select '4415200000','Pallets, box pallets and other load boards; pallet collars','UNT',20,0,0 UNION ALL

Select '4416001000','Staves','KGM',20,0,0 UNION ALL
Select '4416009000','Other','KGM',20,0,0 UNION ALL

Select '4417001000','Boot or shoe lasts','KGM',20,0,0 UNION ALL
Select '4417002000','Boot or shoe trees','KGM',20,0,0 UNION ALL
Select '4417009000','Other','KGM',20,0,0 UNION ALL

Select '4418100000','Windows, Frenchwindows and their frames','KGM',20,0,0 UNION ALL
Select '4418200000','Doors and their frames and thresholds','KGM',20,0,0 UNION ALL
Select '4418400000','Shuttering for concrete constructional work','KGM',20,0,0 UNION ALL
Select '4418500000','Shingles and shakes','KGM',20,0,0 UNION ALL
Select '4418600000','Posts and beams','KGM',20,0,0 UNION ALL


Select '4418731000','For mosaic floors','KGM',20,0,0 UNION ALL
Select '44187390','Other:','',0,0,0 UNION ALL
Select '4418739010','Multilayer','KGM',5,0,0 UNION ALL
Select '4418739020','Other','KGM',20,0,0 UNION ALL
Select '4418740000','Other, for mosaic floors','KGM',20,0,0 UNION ALL
Select '4418750000','Other, multilayer','KGM',20,0,0 UNION ALL
Select '4418790000','Other','KGM',20,0,0 UNION ALL

Select '4418910000','Of bamboo','KGM',20,0,0 UNION ALL

Select '4418991000','Cellular wood panels','KGM',20,0,0 UNION ALL
Select '4418999000','Other','KGM',20,0,0 UNION ALL


Select '4419110000','Bread boards, chopping boards and similar boards','KGM',20,0,0 UNION ALL
Select '4419120000','Chopsticks','KGM',20,0,0 UNION ALL
Select '4419190000','Other','KGM',20,0,0 UNION ALL
Select '4419900000','Other','KGM',20,0,0 UNION ALL

Select '4420100000','Statuettes and other ornaments, of wood','KGM',20,0,0 UNION ALL

Select '4420901000','Wooden articles of furniture not falling in Chapter 94','KGM',0,0,0 UNION ALL
Select '44209090','Other:','',0,0,0 UNION ALL
Select '4420909040','Articles of a kind normally carried in the pocket, in the handbag or on the person ','KGM',5,0,0 UNION ALL
Select '4420909090','Other','KGM',20,0,0 UNION ALL

Select '4421100000','Clothes hangers','KGM',20,0,0 UNION ALL


Select '4421911000','Spools, cops and bobbins, sewing thread reels and the like','KGM',5,0,0 UNION ALL
Select '4421912000','Match splints','KGM',5,0,0 UNION ALL
Select '4421913000','Candysticks, icecream sticks and icecream spoons','KGM',10,0,0 UNION ALL
Select '4421914000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4421915000','Prayer beads','KGM',0,0,0 UNION ALL
Select '4421916000','Toothpicks','KGM',20,0,0 UNION ALL
Select '44219190','Other:','',0,0,0 UNION ALL
Select '4421919010','Sticks for making joss sticks','KGM',0,0,0 UNION ALL
Select '4421919090','Other','KGM',20,0,0 UNION ALL

Select '4421991000','Spools, cops and bobbins, sewing thread reels and the like','KGM',5,0,0 UNION ALL
Select '4421992000','Match splints','KGM',5,0,0 UNION ALL
Select '4421993000','Wooden pegs or pins for footwear','KGM',20,0,0 UNION ALL
Select '4421994000','Candysticks, icecream sticks and icecream spoons','KGM',10,0,0 UNION ALL
Select '4421997000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4421998000','Toothpicks','KGM',20,0,0 UNION ALL

Select '4421999300','Prayer beads','KGM',0,0,0 UNION ALL
Select '4421999400','Other beads','KGM',20,0,0 UNION ALL
Select '4421999500','Sticks for making joss sticks','KGM',0,0,0 UNION ALL
Select '4421999600','Barecore','KGM',20,0,0 UNION ALL
Select '44219999','Other:','',0,0,0 UNION ALL
Select '4421999910','Wood paving blocks','KGM',20,0,0 UNION ALL
Select '4421999930','Blind and blind fittings','KGM',20,0,0 UNION ALL
Select '4421999990','Other','KGM',20,0,0 UNION ALL

Select '4501100000','Natural cork, raw or simply prepared','KGM',0,0,0 UNION ALL
Select '4501900000','Other','KGM',0,0,0 UNION ALL

Select '4502000000','Natural cork, debacked or roughly squared, or in rectangular (including square) blocks, plates, sheets or strip (including sharpedged blanks for corks or stoppers).','KGM',0,0,0 UNION ALL

Select '4503100000','Corks and stoppers','KGM',5,0,0 UNION ALL
Select '4503900000','Other','KGM',5,0,0 UNION ALL

Select '4504100000','Blocks, plates, sheets and strip; tiles of any shape; solid cylinders, including discs','KGM',20,0,0 UNION ALL
Select '4504900000','Other','KGM',15,0,0 UNION ALL


Select '4601210000','Of bamboo','KGM',20,0,0 UNION ALL
Select '4601220000','Of rattan','KGM',20,0,0 UNION ALL
Select '4601290000','Other','KGM',20,0,0 UNION ALL


Select '4601921000','Plaits and similar products of plaiting materials, whether or not  assembled into strips','KGM',15,0,0 UNION ALL
Select '4601922000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4601929000','Other','KGM',25,0,0 UNION ALL

Select '4601931000','Plaits and similar products of plaiting materials, whether or not  assembled into strips','KGM',15,0,0 UNION ALL
Select '4601932000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4601939000','Other','KGM',25,0,0 UNION ALL

Select '4601941000','Plaits and similar products of plaiting materials, whether or not  assembled into strips','KGM',15,0,0 UNION ALL
Select '4601942000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4601949000','Other','KGM',25,0,0 UNION ALL

Select '4601991000','Mats and matting','KGM',20,0,0 UNION ALL
Select '4601992000','Plaits and similar products of plaiting materials, whether or not assembled into strips','KGM',15,0,0 UNION ALL
Select '4601993000','Fans and handscreens, frames and handles therefor, and parts thereof','KGM',5,0,0 UNION ALL
Select '4601999000','Other','KGM',20,0,0 UNION ALL



Select '4602111000','Travelling bags and suitcases','KGM',20,0,0 UNION ALL
Select '4602112000','Envelopes for bottles','KGM',25,0,0 UNION ALL
Select '4602119000','Other','KGM',15,0,0 UNION ALL

Select '4602121000','Travelling bags and suitcases','KGM',20,0,0 UNION ALL
Select '4602122000','Envelopes for bottles','KGM',25,0,0 UNION ALL
Select '4602129000','Other','KGM',15,0,0 UNION ALL

Select '4602191000','Travelling bags and suitcases','KGM',20,0,0 UNION ALL
Select '4602192000','Envelopes for bottles','KGM',25,0,0 UNION ALL
Select '4602199000','Other','KGM',15,0,0 UNION ALL

Select '4602901000','Travelling bags and suitcases','KGM',20,0,0 UNION ALL
Select '4602902000','Envelopes for bottles','KGM',25,0,0 UNION ALL
Select '4602909000','Other','KGM',15,0,0 UNION ALL

Select '4701000000','Mechanical wood pulp.','KGM',0,0,0 UNION ALL

Select '4702000000','Chemical wood pulp, dissolving grades.','KGM',0,0,0 UNION ALL


Select '4703110000','Coniferous','KGM',0,0,0 UNION ALL
Select '4703190000','Nonconiferous','KGM',0,0,0 UNION ALL

Select '4703210000','Coniferous','KGM',0,0,0 UNION ALL
Select '4703290000','Nonconiferous','KGM',0,0,0 UNION ALL


Select '4704110000','Coniferous','KGM',0,0,0 UNION ALL
Select '4704190000','Nonconiferous','KGM',0,0,0 UNION ALL

Select '4704210000','Coniferous','KGM',0,0,0 UNION ALL
Select '4704290000','Nonconiferous','KGM',0,0,0 UNION ALL

Select '4705000000','Wood pulp obtained by a combination of mechanical and chemical pulping processes. ','KGM',0,0,0 UNION ALL

Select '4706100000','Cotton linters pulp','KGM',0,0,0 UNION ALL
Select '4706200000','Pulps of fibres derived from recovered (waste and scrap) paper or paperboard','KGM',0,0,0 UNION ALL
Select '4706300000','Other, of bamboo','KGM',0,0,0 UNION ALL

Select '4706910000','Mechanical','KGM',0,0,0 UNION ALL
Select '4706920000','Chemical','KGM',0,0,0 UNION ALL
Select '4706930000','Obtained by a combination of mechanical and chemical processes','KGM',0,0,0 UNION ALL

Select '4707100000','Unbleached kraft paper or paperboard or corrugated paper or paperboard','KGM',0,0,0 UNION ALL
Select '4707200000','Other paper or paperboard made mainly of bleached chemical  pulp, not coloured in the mass','KGM',0,0,0 UNION ALL
Select '4707300000','Paper or paperboard made mainly of mechanical pulp (for  example, newspapers, journals and similar printed matter)','KGM',0,0,0 UNION ALL
Select '4707900000','Other, including unsorted waste and scrap','KGM',0,0,0 UNION ALL


Select '4801001100','In rolls, of a width exceeding 28 cm but not exceeding 36 cm','KGM',10,0,0 UNION ALL
Select '4801001200','In rolls, other ','KGM',10,0,0 UNION ALL
Select '4801001300','In sheets, square or rectangular, one side of which exceeds 28 cm but not exceeding 36 cm, and the other side exceeding 15 cm in the unfolded state','KGM',10,0,0 UNION ALL
Select '4801001400','In sheets, square or rectangular, one side of which exceeds 36 cm, and the other side exceeding 15 cm in the unfolded state','KGM',10,0,0 UNION ALL

Select '4801002100','In rolls, of a width exceeding 28 cm but not exceeding 36 cm','KGM',10,0,0 UNION ALL
Select '4801002200','In rolls, other','KGM',10,0,0 UNION ALL
Select '4801002300','In sheets, square or rectangular, one side of which exceeds 28 cm but not exceeding 36 cm, and the other side exceeding 15 cm in the unfolded state','KGM',10,0,0 UNION ALL
Select '4801002400','In sheets, square or rectangular, one side of which exceeds 36 cm, and the other side exceeding 15 cm in the unfolded state','KGM',10,0,0 UNION ALL

Select '4802100000','Handmade paper and paperboard','KGM',0,0,0 UNION ALL

Select '4802201000','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802209000','Other','KGM',0,0,0 UNION ALL

Select '4802401000',' In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802409000','Other','KGM',0,0,0 UNION ALL



Select '4802541100','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',25,0,0 UNION ALL
Select '4802541900','Other','KGM',0,0,0 UNION ALL

Select '4802542100','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',25,0,0 UNION ALL
Select '4802542900','Other','KGM',0,0,0 UNION ALL
Select '4802543000','Base paper of a kind used to manufacture aluminium coated paper','KGM',0,0,0 UNION ALL
Select '4802544000','Of a kind used for writing, printing and other graphic purposes, in rolls of not more  than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in unfolded the state','KGM',20,0,0 UNION ALL
Select '4802545000','Multiply paper and paperboard','KGM',10,0,0 UNION ALL
Select '4802549000','Other','KGM',0,0,0 UNION ALL

Select '48025520','Fancy paper and paperboard, including paper and paperboard with watermarks, a granitized felt finish, a fibre finish, a vellum antique finish or a blend of specks:','',0,0,0 UNION ALL

Select '4802552011','Of not more than 150 mm in width ','KGM',20,0,0 UNION ALL
Select '4802552019','Other','KGM',7,0,0 UNION ALL
Select '4802552090','Other','KGM',0,0,0 UNION ALL
Select '4802554000','Base paper of a kind used to manufacture aluminium coated paper','KGM',0,0,0 UNION ALL
Select '4802555000','Base paper of a kind used to manufacture release paper','KGM',0,0,0 UNION ALL

Select '4802556100','Of a width not exceeding 15cm ','KGM',20,0,0 UNION ALL
Select '4802556900','Other','KGM',7,0,0 UNION ALL
Select '4802557000','Multiply paper ','KGM',10,0,0 UNION ALL
Select '4802559000','Other','KGM',0,0,0 UNION ALL

Select '48025620','Fancy paper and paperboard including paper and paperboard with watermarks, a granitized felt finish, a fibre finish, a vellum antique finish or a blend of specks:','',0,0,0 UNION ALL

Select '4802562011','In rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802562019','Other','KGM',7,0,0 UNION ALL
Select '4802562090','Other','KGM',0,0,0 UNION ALL

Select '4802563100','With no side exceeding 36 cm in the unfolded state','KGM',25,0,0 UNION ALL
Select '4802563900','Other','KGM',0,0,0 UNION ALL

Select '4802564100','Of a width not exceeding 36 cm in rectangular (including square) sheets and in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802564900','Other','KGM',7,0,0 UNION ALL
Select '4802565000','Multiply paper ','KGM',10,0,0 UNION ALL
Select '4802569000','Other','KGM',0,0,0 UNION ALL


Select '4802571100','With no side exceeding 36 cm in the unfolded state','KGM',25,0,0 UNION ALL
Select '4802571900','Other','KGM',0,0,0 UNION ALL

Select '4802572100','With no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802572900','Other','KGM',7,0,0 UNION ALL
Select '4802573000','Multiply paper ','KGM',10,0,0 UNION ALL
Select '4802579000','Other','KGM',0,0,0 UNION ALL


Select '48025821','In rolls of a width of 15 cm or less or in rectangular (including square) sheets with one side 36 cm or less and the other side 15 cm or less in the unfolded state:','',0,0,0 UNION ALL
Select '4802582110','For writing, printing and  other graphic purposes','KGM',20,0,0 UNION ALL

Select '4802582191','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802582192','Weighing 225 g/m2 or more','KGM',10,0,0 UNION ALL
Select '48025829','Other:','',0,0,0 UNION ALL
Select '4802582910','For writing, printing and  other graphic purposes','KGM',5,0,0 UNION ALL

Select '4802582991','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802582992','Weighing 225 g/m2 or more','KGM',10,0,0 UNION ALL

Select '4802583100','In rolls of not more  than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802583900','Other','KGM',5,0,0 UNION ALL
Select '4802584000','Multiply paper ','KGM',10,0,0 UNION ALL

Select '4802589100','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802589900','Other','KGM',10,0,0 UNION ALL


Select '48026130','Fancy paper and paperboard, including paper and paperboard with watermarks, a granitized felt finish, a fibre finish, a vellum antique finish or a blend of specks:','',0,0,0 UNION ALL

Select '4802613011','Of not more  than 15 cm in width ','KGM',20,0,0 UNION ALL
Select '4802613019','Other ','KGM',0,0,0 UNION ALL

Select '4802613092','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802613099','Other ','KGM',10,0,0 UNION ALL
Select '48026140','Base paper of a kind used to manufacture aluminium coated paper:','',0,0,0 UNION ALL
Select '4802614020','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802614090','Other','KGM',10,0,0 UNION ALL

Select '4802615100','Of a width not exceeding 15 cm','KGM',20,0,0 UNION ALL
Select '4802615900','Other','KGM',0,0,0 UNION ALL
Select '4802616000','Multiply paper ','KGM',10,0,0 UNION ALL

Select '4802619100','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802619900','Other','KGM',10,0,0 UNION ALL

Select '48026210','Fancy paper and paperboard, including paper and paperboard with watermarks, a granitized felt finish, a fibre finish, a vellum antique finish or a blend of specks, in rectangular (including square) sheets with one side 36 cm or less and the other side 15 cm or less in the unfolded state:','',0,0,0 UNION ALL
Select '4802621010','For writing, printing and  other graphic purposes','KGM',20,0,0 UNION ALL

Select '4802621092','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802621099','Other','KGM',10,0,0 UNION ALL
Select '48026220','Other fancy paper and paperboard, including paper and paperboard with watermarks, a granitized felt finish, a fibre finish, a vellum antique finish or a blend of specks:','',0,0,0 UNION ALL
Select '4802622010','For writing, printing and  other graphic purposes','KGM',0,0,0 UNION ALL

Select '4802622092','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802622099','Weighing 225 g/m2 or more','KGM',10,0,0 UNION ALL

Select '4802623100','In rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802623900','Other','KGM',0,0,0 UNION ALL
Select '4802624000','Multiply paper ','KGM',10,0,0 UNION ALL

Select '4802629100','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802629900','Other','KGM',10,0,0 UNION ALL


Select '4802691100','In rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4802691900','Other','KGM',0,0,0 UNION ALL
Select '4802692000','Multiply paper ','KGM',10,0,0 UNION ALL

Select '4802699100','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',0,0,0 UNION ALL
Select '4802699900','Other','KGM',10,0,0 UNION ALL


Select '4803003000','Of cellulose wadding or of webs of cellulose fibres','KGM',0,0,0 UNION ALL
Select '4803009000','Other','KGM',25,0,0 UNION ALL


Select '4804110000','Unbleached','KGM',10,0,0 UNION ALL
Select '4804190000','Other ','KGM',10,0,0 UNION ALL


Select '4804211000','Of a kind used for making cement bags','KGM',0,0,0 UNION ALL
Select '4804219000','Other','KGM',0,0,0 UNION ALL

Select '4804291000','Of a kind used for making cement bags','KGM',5,0,0 UNION ALL
Select '4804299000','Other','KGM',5,0,0 UNION ALL


Select '4804311000','Electrical grade insulating kraft paper','KGM',10,0,0 UNION ALL
Select '4804313000','Of a wet strength of 40 g to 60 g, of a kind used in the manufacture for plywood adhesive tape','KGM',10,0,0 UNION ALL
Select '4804314000','Sandpaper base paper','KGM',10,0,0 UNION ALL
Select '4804315000','Of a kind used for making cement bags','KGM',10,0,0 UNION ALL
Select '4804319000','Other','KGM',10,0,0 UNION ALL

Select '4804391000','Of a wet strength 40 g to 60 g, of a kind used in the manufacture for plywood adhesive tape','KGM',10,0,0 UNION ALL
Select '4804392000','Paper and paperboard of a kind used for making food packaging','KGM',0,0,0 UNION ALL
Select '4804399000','Other','KGM',10,0,0 UNION ALL


Select '4804411000','Electrical grade insulating kraft paper','KGM',10,0,0 UNION ALL
Select '4804419000','Other','KGM',10,0,0 UNION ALL

Select '4804421000','Paper and paperboard of a kind used for making food packaging','KGM',0,0,0 UNION ALL
Select '4804429000','Other','KGM',10,0,0 UNION ALL

Select '4804491000','Paper and paperboard of a kind used for making food packaging','KGM',0,0,0 UNION ALL
Select '4804499000','Other','KGM',10,0,0 UNION ALL


Select '4804511000','Electrical grade insulating kraft paper','KGM',10,0,0 UNION ALL
Select '4804512000','Pressboard weighing 600 g/m2 or more','KGM',10,0,0 UNION ALL
Select '4804513000','Of a wet strength of 40 g to 60 g, of a kind used in the manufacture of plywood adhesive tape','KGM',10,0,0 UNION ALL
Select '4804519000','Other','KGM',10,0,0 UNION ALL

Select '4804521000','Paper and paperboard of a kind used for making food packaging','KGM',0,0,0 UNION ALL
Select '4804529000','Other','KGM',10,0,0 UNION ALL

Select '4804591000','Paper and paperboard of a kind used for making food packaging','KGM',0,0,0 UNION ALL
Select '4804599000','Other','KGM',10,0,0 UNION ALL


Select '4805110000','Semichemical fluting paper','KGM',10,0,0 UNION ALL
Select '4805120000','Straw fluting paper','KGM',10,0,0 UNION ALL

Select '4805191000','Weighing more than 150 g/m2 but less than 225 g/m2','KGM',10,0,0 UNION ALL
Select '4805199000','Other','KGM',10,0,0 UNION ALL

Select '4805240000','Weighing 150 g/m2 or less','KGM',10,0,0 UNION ALL

Select '4805251000','Weighing less than 225 g/m2','KGM',10,0,0 UNION ALL
Select '4805259000','Other','KGM',10,0,0 UNION ALL

Select '4805301000','Coloured paper of a kind used for wrapping wooden match box','KGM',0,0,0 UNION ALL
Select '4805309000','Other','KGM',0,0,0 UNION ALL
Select '4805400000','Filter paper and paperboard','KGM',0,0,0 UNION ALL
Select '4805500000','Felt paper and paperboard','KGM',0,0,0 UNION ALL


Select '4805911000','Paper of a kind used as interleaf material for the packing of flat glass products, with a resin content by weight of not more than 0.6%','KGM',10,0,0 UNION ALL
Select '4805912000','Of a kind used to manufacture joss paper','KGM',0,0,0 UNION ALL
Select '4805919000','Other','KGM',10,0,0 UNION ALL

Select '4805921000','Multiply paper and paperboard','KGM',10,0,0 UNION ALL
Select '4805929000','Other','KGM',0,0,0 UNION ALL

Select '4805931000','Multiply paper and paperboard','KGM',10,0,0 UNION ALL
Select '4805932000','Blotting paper','KGM',0,0,0 UNION ALL
Select '4805939000','Other','KGM',10,0,0 UNION ALL

Select '4806100000','Vegetable parchment','KGM',0,0,0 UNION ALL
Select '4806200000','Greaseproof papers','KGM',0,0,0 UNION ALL
Select '4806300000','Tracing papers','KGM',0,0,0 UNION ALL
Select '4806400000','Glassine and other glazed transparent or translucent papers','KGM',0,0,0 UNION ALL

Select '4807000000','Composite paper and paperboard (made by sticking flat layers of paper or paperboard together with an adhesive), not surfacecoated or impregnated, whether or not internally reinforced, in rolls or sheets.','KGM',0,0,0 UNION ALL

Select '4808100000','Corrugated paper and paperboard, whether or not perforated','KGM',15,0,0 UNION ALL
Select '4808400000','Kraft paper, creped or crinkled, whether or not embossed or perforated','KGM',25,0,0 UNION ALL

Select '4808902000','Creped or crinkled paper','KGM',25 ,0,0 UNION ALL
Select '4808903000','Embossed paper','KGM',15,0,0 UNION ALL
Select '4808909000','Other','KGM',15,0,0 UNION ALL

Select '4809200000','Selfcopy paper','KGM',5,0,0 UNION ALL

Select '4809901000','Carbon paper and similar copying papers','KGM',20,0,0 UNION ALL
Select '4809909000','Other','KGM',5,0,0 UNION ALL



Select '4810131000','Printed, of a kind used for selfrecording apparatus, of a width of 150 mm or less','KGM',20,0,0 UNION ALL

Select '4810139100','Of a width of 150 mm or less','KGM',0,0,0 UNION ALL
Select '4810139900','Other','KGM',0,0,0 UNION ALL


Select '4810141100','Electrocardiograph, ultrasonography, spirometer, electroencephalograph and fetal monitoring papers','KGM',20,0,0 UNION ALL
Select '4810141900','Other','KGM',20,0,0 UNION ALL

Select '4810149100','Of which no side exceeds 360 mm','KGM',0,0,0 UNION ALL
Select '4810149900','Other','KGM',0,0,0 UNION ALL

Select '4810191000','Printed, of a kind used for selfrecording apparatus, of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL

Select '4810199100','Of which no side exceeds 360 mm','KGM',0,0,0 UNION ALL
Select '4810199900','Other','KGM',0,0,0 UNION ALL


Select '4810221000','Printed, of a kind used for selfrecording apparatus, in rolls of a width of 150 mm or less, or in sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL

Select '4810229100','In rolls of a width of 150 mm or less, or in sheets of which no side exceeds 360 mm in the unfolded state','KGM',0,0,0 UNION ALL
Select '4810229900','Other','KGM',0,0,0 UNION ALL

Select '4810291000','Printed, of a kind used for selfrecording apparatus, in rolls of a width of 150 mm or less, or in sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL

Select '4810299100',' In rolls of a width of 150 mm or less, or in sheets of which no side exceeds 360 mm in the unfolded state','KGM',0,0,0 UNION ALL
Select '4810299900','Other','KGM',0,0,0 UNION ALL


Select '4810313000','In rolls of not more than 150 mm in width or sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4810319000','Other','KGM',0,0,0 UNION ALL

Select '4810323000','In rolls of not more than 150 mm in width or sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4810329000','Other','KGM',0,0,0 UNION ALL

Select '4810393000','In rolls of not more than 150 mm in width or sheets of which no side exceeds 360 mm in the unfolded state','KGM',25,0,0 UNION ALL
Select '4810399000','Other','KGM',0,0,0 UNION ALL


Select '4810924000','In rolls of not more than 150 mm in width or sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4810929000','Other','KGM',5,0,0 UNION ALL

Select '4810994000','In rolls of not more than 150 mm in width or sheets of which no side exceeds 360 mm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4810999000','Other','KGM',0,0,0 UNION ALL


Select '4811102000','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '48111090','Other:','',0,0,0 UNION ALL
Select '4811109010','Floor coverings ','KGM',20,0,0 UNION ALL
Select '4811109090','Other','KGM',0,0,0 UNION ALL


Select '4811412000','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4811419000','Other','KGM',20,0,0 UNION ALL

Select '4811492000','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4811499000','Other ','KGM',20,0,0 UNION ALL



Select '4811513100','Floor coverings','KGM',20,0,0 UNION ALL
Select '4811513900','Other','KGM',20,0,0 UNION ALL

Select '4811519100','Floor coverings','KGM',20,0,0 UNION ALL
Select '4811519900','Other','KGM',0,0,0 UNION ALL

Select '48115920','Paper and paperboard covered on both faces with transparent films of plastics and with a lining of aluminium foil, for the packaging of liquid food products:','',0,0,0 UNION ALL
Select '4811592010','In rolls of not more  than 15 cm in width or in rectangular (including square) sheets of which no side exceeding 36 cm in the unfolded state','KGM',20,0,0 UNION ALL
Select '4811592090','Other','KGM',0,0,0 UNION ALL

Select '4811594100','Floor coverings','KGM',20,0,0 UNION ALL
Select '4811594900','Other','KGM',20,0,0 UNION ALL

Select '4811599100','Floor coverings','KGM',20,0,0 UNION ALL
Select '4811599900','Other','KGM',0,0,0 UNION ALL

Select '4811602000','In rolls of not more than 15 cm in width or in rectangular (including square) sheets of which no side exceeds 36 cm in the unfolded state','KGM',20,0,0 UNION ALL

Select '4811609100','Floor coverings','KGM',0,0,0 UNION ALL
Select '4811609900','Other','KGM',0,0,0 UNION ALL


Select '4811904100','Floor coverings','KGM',20,0,0 UNION ALL
Select '4811904200','Marbled paper','KGM',20,0,0 UNION ALL
Select '4811904900','Other','KGM',20,0,0 UNION ALL

Select '4811909100','Floor coverings ','KGM',20,0,0 UNION ALL
Select '4811909200','Marbled paper','KGM',0,0,0 UNION ALL
Select '48119099','Other:','',0,0,0 UNION ALL
Select '4811909910','Of cellulose wadding and webs of cellulose fibres in rolls of not more than 36 cm in width ','KGM',20,0,0 UNION ALL
Select '4811909990','Other','KGM',0,0,0 UNION ALL

Select '4812000000','Filter blocks, slabs and plates, of paper pulp.','KGM',20,0,0 UNION ALL

Select '4813100000','In the form of booklets or tubes','KGM',20,0,0 UNION ALL
Select '4813200000','In rolls of a width not exceeding 5 cm','KGM',20,0,0 UNION ALL

Select '4813901000','In rolls of a width exceeding 5 cm, coated ','KGM',5,0,0 UNION ALL
Select '48139090','Other:','',0,0,0 UNION ALL
Select '4813909010','Cut to shape, other than rectangular (including square)','KGM',10,0,0 UNION ALL
Select '4813909090','Other','KGM',20,0,0 UNION ALL


Select '4814201000','Of a width not exceeding 60cm','KGM',20,0,0 UNION ALL
Select '48142090','Other:','',0,0,0 UNION ALL
Select '4814209010','Photo murals','KGM',20,0,0 UNION ALL
Select '4814209020','Other, of plastics the thickness of which constitutes  more than half the total thickness','KGM',20,0,0 UNION ALL
Select '4814209090','Other','KGM',20,0,0 UNION ALL
Select '48149000','Other:','',0,0,0 UNION ALL
Select '4814900040','Embossed','KGM',15,0,0 UNION ALL
Select '4814900050','Coated, impregnated,  covered, surfacecoloured, surfacedecorated or printed','KGM',5,0,0 UNION ALL
Select '4814900090','Other','KGM',20,0,0 UNION ALL


Select '4816201000','In rolls of a width exceeding 15 cm but not exceeding 36 cm ','KGM',5,0,0 UNION ALL
Select '4816209000','Other','KGM',20,0,0 UNION ALL

Select '4816901000','Carbon paper','KGM',0,0,0 UNION ALL
Select '4816902000','Other copying paper','KGM',20,0,0 UNION ALL
Select '4816903000',' Offset plates','KGM',20,0,0 UNION ALL
Select '4816904000','Heat transfer paper','KGM',20,0,0 UNION ALL
Select '4816905000','Other, in rolls of a width exceeding 15cm but not exceeding 36cm','KGM',5,0,0 UNION ALL
Select '4816909000','Other','KGM',20,0,0 UNION ALL

Select '4817100000','Envelopes','KGM',20,0,0 UNION ALL
Select '4817200000','Letter cards, plain postcards and correspondence cards','KGM',20,0,0 UNION ALL
Select '4817300000','Boxes, pouches, wallets and writing compendiums, of paper or  paperboard, containing an assortment of paper stationery','KGM',20,0,0 UNION ALL

Select '4818100000','Toilet paper','KGM',25,0,0 UNION ALL
Select '4818200000','Handkerchiefs, cleansing or facial tissues and towels','KGM',25,0,0 UNION ALL

Select '4818301000','Tablecloths','KGM',20,0,0 UNION ALL
Select '4818302000','Serviettes','KGM',25,0,0 UNION ALL
Select '4818500000','Articles of apparel and clothing accessories','KGM',20,0,0 UNION ALL
Select '4818900000','Other','KGM',20,0,0 UNION ALL

Select '4819100000','Cartons, boxes and cases, of corrugated paper or paperboard','KGM',25,0,0 UNION ALL
Select '4819200000','Folding cartons, boxes and cases, of noncorrugated paper or paperboard','KGM',25,0,0 UNION ALL
Select '4819300000','Sacks and bags, having a base of a width of 40 cm or more','KGM',25,0,0 UNION ALL
Select '4819400000','Other sacks and bags, including cones','KGM',25,0,0 UNION ALL
Select '4819500000','Other packing containers, including record sleeves','KGM',25,0,0 UNION ALL
Select '4819600000','Box files, letter trays, storage boxes and similar articles, of a kind used in offices, shops or the like','KGM',25,0,0 UNION ALL

Select '4820100000','Registers, account books, note books, order books, receipt books, letter pads, memorandum pads, diaries and similar articles','KGM',20,0,0 UNION ALL
Select '4820200000','Exercise books','KGM',0,0,0 UNION ALL
Select '4820300000','Binders (other than book covers), folders and file covers','KGM',20,0,0 UNION ALL
Select '4820400000','Manifold business forms and interleaved carbon sets','KGM',20,0,0 UNION ALL
Select '4820500000','Albums for samples or for collections','KGM',20,0,0 UNION ALL
Select '4820900000','Other','KGM',20,0,0 UNION ALL


Select '4821101000','Labels of a kind used for jewellery, including  objects  of personal adornment or articles of personal use normally  carried in the pocket, in the handbag or on the person','KGM',25,0,0 UNION ALL
Select '4821109000','Other','KGM',25,0,0 UNION ALL

Select '4821901000','Labels of a kind used for jewellery, including  objects of   personal adornment or articles of personal use normally carried in the pocket, in the handbag or on the person','KGM',25,0,0 UNION ALL
Select '4821909000','Other','KGM',25,0,0 UNION ALL


Select '4822101000','Cones','KGM',15,0,0 UNION ALL
Select '4822109000','Other','KGM',5,0,0 UNION ALL

Select '4822901000','Cones','KGM',15,0,0 UNION ALL
Select '4822909000','Other','KGM',5,0,0 UNION ALL


Select '4823201000','In strips, rolls or sheets','KGM',25,0,0 UNION ALL
Select '4823209000','Other','KGM',10,0,0 UNION ALL


Select '4823402100','Cardiograph recording paper','KGM',25,0,0 UNION ALL
Select '4823402900','Other','KGM',25,0,0 UNION ALL
Select '4823409000','Other','KGM',25,0,0 UNION ALL

Select '4823610000','Of bamboo','KGM',25,0,0 UNION ALL
Select '4823690000','Other','KGM',25,0,0 UNION ALL
Select '4823700000','Moulded or pressed articles of paper pulp','KGM',25,0,0 UNION ALL

Select '4823901000','Cocooning frames for silkworms','KGM',25,0,0 UNION ALL
Select '4823902000','Display cards of a kind used for jewellery, including objects of personal  adornment or articles of personal use normally carried in the pocket, in the handbag or on the person','KGM',25,0,0 UNION ALL
Select '4823903000','Diecut polyethylene coated paperboard  of a kind used for the   manufacture of paper cups','KGM',25,0,0 UNION ALL
Select '4823904000','Paper tube sets of a kind used for the manufacture of fireworks','KGM',25,0,0 UNION ALL

Select '4823905100','Weighing 150 g/m2 or less','KGM',25,0,0 UNION ALL
Select '4823905900','Other','KGM',25,0,0 UNION ALL
Select '4823906000','Punched jacquard cards','KGM',0,0,0 UNION ALL
Select '4823907000','Fans and handscreens','KGM',20,0,0 UNION ALL

Select '4823909100','Silicone paper','KGM',0,0,0 UNION ALL
Select '4823909200','Joss paper','KGM',0,0,0 UNION ALL
Select '48239094','Cellulose wadding and webs of cellulose fibers, coloured or marbled throughout the mass:','',0,0,0 UNION ALL
Select '4823909410','Cut to shape other than rectangular or square','KGM',10,0,0 UNION ALL
Select '4823909490','Other','KGM',25,0,0 UNION ALL
Select '4823909500','Floor coverings','KGM',20,0,0 UNION ALL
Select '4823909600','Other, cut to shape other than rectangular or square','KGM',10,0,0 UNION ALL
Select '48239099','Other:','',0,0,0 UNION ALL
Select '4823909920','Cards for statistical machines','KGM',10,0,0 UNION ALL
Select '4823909990','Other','KGM',25,0,0 UNION ALL

Select '4901100000','In single sheets, whether or not folded','KGM',0,0,0 UNION ALL

Select '4901910000','Dictionaries and encyclopaedias, and serial instalments thereof','KGM',0,0,0 UNION ALL

Select '4901991000','Educational, technical, scientific, historical or cultural books','KGM',0,0,0 UNION ALL
Select '4901999000','Other','KGM',0,0,0 UNION ALL

Select '4902100000','�Appearing at least four times a week','KGM',0,0,0 UNION ALL

Select '4902901000','Educational, technical, scientific, historical or cultural journals and periodicals','KGM',0,0,0 UNION ALL
Select '4902909000','Other','KGM',0,0,0 UNION ALL

Select '4903000000','Children''s picture, drawing or colouring books.','KGM',0,0,0 UNION ALL

Select '4904000000','Music, printed or in manuscript, whether or not bound or illustrated.','KGM',0,0,0 UNION ALL

Select '4905100000','Globes','KGM',0,0,0 UNION ALL

Select '4905910000',' In book form','KGM',0,0,0 UNION ALL
Select '4905990000',' Other','KGM',0,0,0 UNION ALL

Select '4906000000','Plans and drawings for architectural, engineering, industrial, commercial, topographical or similar purposes, being originals drawn by hand; handwritten texts; photographic reproductions on sensitised paper and carbon copies of the foregoing.','KGM',0,0,0 UNION ALL

Select '4907001000','Banknotes, being legal tender','KGM',0,0,0 UNION ALL

Select '4907002100','Postage stamps','KGM',0,0,0 UNION ALL
Select '4907002900','Other','KGM',0,0,0 UNION ALL
Select '4907004000','Stock, share or bond certificates and similar documents of   title; cheque forms','KGM',20,0,0 UNION ALL
Select '4907009000',' Other','KGM',20,0,0 UNION ALL

Select '4908100000','Transfers (decalcomanias), vitrifiable','KGM',20,0,0 UNION ALL
Select '4908900000','�Other','KGM',20,0,0 UNION ALL

Select '4909000000','Printed or illustrated postcards; printed cards bearing personal greetings, messages or announcements, whether or not illustrated, with or without envelopes or trimmings.','KGM',20,0,0 UNION ALL

Select '49100000','Calendars of any kind, printed, including calendar blocks.','',0,0,0 UNION ALL

Select '4910000016','Of nickel','KGM',15,0,0 UNION ALL
Select '4910000017','Of copper','KGM',0,0,0 UNION ALL
Select '4910000018','Of plastic; of paper or paperboard; of glass; of wood; of aluminium','KGM',20,0,0 UNION ALL
Select '4910000019','Other','KGM',5,0,0 UNION ALL
Select '4910000090',' Other','KGM',20,0,0 UNION ALL


Select '4911101000','Catalogues listing only educational, technical, scientific, historical or cultural books and publications','KGM',0,0,0 UNION ALL
Select '4911109000','Other','KGM',0,0,0 UNION ALL


Select '4911911000','Designs','KGM',0,0,0 UNION ALL

Select '4911912100','Anatomical and botanical','KGM',20,0,0 UNION ALL
Select '4911912900','Other','KGM',20,0,0 UNION ALL

Select '4911913100','Anatomical and botanical','KGM',20,0,0 UNION ALL
Select '4911913900','Other','KGM',20,0,0 UNION ALL

Select '4911991000','Printed cards for jewellery or for small objects of personal adornment or articles of personal use normally carried in the pocket, handbag or on the person','KGM',20,0,0 UNION ALL
Select '4911992000','Printed labels for explosives','KGM',20,0,0 UNION ALL
Select '4911993000','Educational, technical, scientific, historical or cultural material printed on a set of cards','KGM',20,0,0 UNION ALL
Select '4911999000',' Other','KGM',20,0,0 UNION ALL

Select '5001000000','Silkworm cocoons suitable for reeling.','KGM',0,0,0 UNION ALL

Select '5002000000','Raw silk (not thrown).','KGM',0,0,0 UNION ALL

Select '5003000000','Silk waste (including cocoons unsuitable for reeling, yarn waste and garnetted stock).','KGM',0,0,0 UNION ALL

Select '5004000000','Silk yarn (other than yarn spun from silk waste) not put up for retail sale. ','KGM',0,0,0 UNION ALL

Select '5005000000','Yarn spun from silk waste, not put up for retail sale.  ','KGM',0,0,0 UNION ALL

Select '5006000000','Silk yarn and yarn spun from silk waste, put up for retail sale; silkworm gut. ','KGM',0,0,0 UNION ALL


Select '5007102000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5007103000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5007109000','Other','KGM',10,0,0 UNION ALL

Select '5007202000','Unbleached or bleached','KGM',0,0,0 UNION ALL
Select '5007203000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '5007209000','Other','KGM',0,0,0 UNION ALL

Select '5007902000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5007903000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5007909000','Other','KGM',10,0,0 UNION ALL


Select '5101110000',' Shorn wool','KGM',0,0,0 UNION ALL
Select '5101190000','Other','KGM',0,0,0 UNION ALL

Select '5101210000',' Shorn wool','KGM',0,0,0 UNION ALL
Select '5101290000',' Other','KGM',0,0,0 UNION ALL
Select '5101300000',' Carbonised','KGM',0,0,0 UNION ALL


Select '5102110000',' Of Kashmir (cashmere) goats','KGM',0,0,0 UNION ALL
Select '5102190000',' Other','KGM',0,0,0 UNION ALL
Select '5102200000',' Coarse animal hair','KGM',0,0,0 UNION ALL

Select '5103100000',' Noils of wool or of fine animal hair','KGM',0,0,0 UNION ALL
Select '5103200000','Other waste of wool or of fine animal hair','KGM',0,0,0 UNION ALL
Select '5103300000',' Waste of coarse animal hair','KGM',0,0,0 UNION ALL

Select '5104000000','Garnetted stock of wool or of fine or coarse animal hair.','KGM',0,0,0 UNION ALL

Select '5105100000',' Carded wool','KGM',0,0,0 UNION ALL

Select '5105210000','Combed wool in fragments','KGM',0,0,0 UNION ALL
Select '5105290000','Other','KGM',0,0,0 UNION ALL

Select '5105310000','Of Kashmir (cashmere) goats','KGM',0,0,0 UNION ALL
Select '5105390000','Other','KGM',0,0,0 UNION ALL
Select '5105400000','Coarse animal hair, carded or combed','KGM',0,0,0 UNION ALL

Select '5106100000','Containing 85% or more by weight of wool','KGM',0,0,0 UNION ALL
Select '5106200000','Containing less than 85% by weight of wool','KGM',0,0,0 UNION ALL

Select '5107100000','Containing 85% or more by weight of wool','KGM',0,0,0 UNION ALL
Select '5107200000','Containing less than 85% by weight of wool','KGM',0,0,0 UNION ALL

Select '5108100000','Carded','KGM',0,0,0 UNION ALL
Select '5108200000','Combed','KGM',0,0,0 UNION ALL

Select '5109100000','Containing 85% or more by weight of wool or of fine animal hair','KGM',0,0,0 UNION ALL
Select '5109900000','Other','KGM',0,0,0 UNION ALL

Select '5110000000','Yarn of coarse animal hair or of horsehair (including gimped horsehair yarn), whether or not put up for retail sale.','KGM',0,0,0 UNION ALL


Select '5111110000','Of a weight not exceeding 300 g/m2','KGM',0,0,0 UNION ALL
Select '5111190000','Other','KGM',0,0,0 UNION ALL
Select '5111200000','Other, mixed mainly or solely with manmade filaments','KGM',0,0,0 UNION ALL
Select '5111300000','Other, mixed mainly or solely with manmade staple fibres','KGM',0,0,0 UNION ALL
Select '5111900000','Other','KGM',0,0,0 UNION ALL


Select '5112110000','Of a weight not exceeding 200 g/m2','KGM',0,0,0 UNION ALL

Select '5112191000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '5112199000','Other','KGM',0,0,0 UNION ALL
Select '5112200000','Other, mixed mainly or solely with manmade filaments','KGM',0,0,0 UNION ALL
Select '5112300000','Other, mixed mainly or solely with manmade staple fibres','KGM',0,0,0 UNION ALL
Select '5112900000','Other','KGM',0,0,0 UNION ALL

Select '5113000000','Woven fabrics of coarse animal hair or of horsehair.','KGM',0,0,0 UNION ALL

Select '5201000000','Cotton, not carded or combed.','KGM',0,0,0 UNION ALL

Select '5202100000','Yarn waste (including thread waste)','KGM',0,0,0 UNION ALL

Select '5202910000','Garnetted stock','KGM',0,0,0 UNION ALL
Select '5202990000','Other','KGM',0,0,0 UNION ALL

Select '5203000000','Cotton, carded or combed.','KGM',0,0,0 UNION ALL


Select '52041100','Containing 85% or more by weight of cotton:      ','',0,0,0 UNION ALL
Select '5204111000','Unbleached','KGM',15,0,0 UNION ALL
Select '5204119000','Other','KGM',15,0,0 UNION ALL
Select '5204190000',' Other','KGM',15,0,0 UNION ALL
Select '5204200000','Put up for retail sale','KGM',15,0,0 UNION ALL


Select '5205110000','Measuring 714.29 decitex or more (not exceeding 14 metric  number)','KGM',10,0,0 UNION ALL
Select '5205120000','Measuring less than 714.29 decitex but not less than 232.56 decitex (exceeding 14 metric number but not exceeding 43 metric number)','KGM',10,0,0 UNION ALL
Select '5205130000','Measuring less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not exceeding 52 metric number)','KGM',10,0,0 UNION ALL
Select '5205140000','Measuring less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric  number)','KGM',10,0,0 UNION ALL
Select '5205150000','Measuring less than 125 decitex (exceeding 80 metric number) ','KGM',10,0,0 UNION ALL

Select '5205210000','Measuring 714.29 decitex or more (not exceeding 14 metric number)','KGM',10,0,0 UNION ALL
Select '5205220000','Measuring less than 714.29 decitex but not less than 232.56 decitex (exceeding 14 metric number but not exceeding 43 metric number)','KGM',10,0,0 UNION ALL
Select '5205230000','Measuring less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not exceeding 52  metric number)','KGM',10,0,0 UNION ALL
Select '5205240000','Measuring less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric number)','KGM',10,0,0 UNION ALL
Select '5205260000','Measuring less than 125 decitex but not less than 106.38 decitex  (exceeding 80 metric number but not exceeding 94 metric  number)','KGM',10,0,0 UNION ALL
Select '5205270000','Measuring less than 106.38 decitex but not less than 83.33 decitex (exceeding 94 metric number but not exceeding 120 metric number)','KGM',10,0,0 UNION ALL
Select '5205280000','Measuring less than 83.33 decitex (exceeding 120 metric  number)','KGM',10,0,0 UNION ALL

Select '5205310000','Measuring per single yarn 714.29 decitex or more (not exceeding 14 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205320000','Measuring per single yarn less than 714.29 decitex but not less  than 232.56 decitex (exceeding 14 metric number but not  exceeding 43 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205330000','Measuring per single yarn less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not exceeding 52 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205340000',' Measuring per single yarn less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding  80 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205350000','Measuring per single yarn less than 125 decitex (exceeding 80 metric number per single yarn)','KGM',10,0,0 UNION ALL

Select '5205410000','Measuring per single yarn 714.29 decitex or more (not exceeding 14 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205420000','Measuring per single yarn less than 714.29 decitex but not less than 232.56 decitex (exceeding 14 metric number but not    exceeding 43 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205430000','Measuring per single yarn less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not   exceeding 52 metric number per single yarn)  ','KGM',10,0,0 UNION ALL
Select '5205440000','Measuring per single yarn less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205460000','Measuring per single yarn less than 125 decitex but not less than 106.38 decitex (exceeding 80 metric number but not exceeding 94 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205470000','Measuring per single yarn less than 106.38 decitex but not less than 83.33 decitex (exceeding 94 metric number but not  exceeding 120 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5205480000','Measuring per single yarn less than 83.33 decitex (exceeding 120 metric number per single yarn)','KGM',10,0,0 UNION ALL


Select '5206110000','Measuring 714.29 decitex or more (not exceeding 14 metric number)','KGM',10,0,0 UNION ALL
Select '5206120000','Measuring less than 714.29 decitex but not less than 232.56   decitex   (exceeding 14 metric number but not exceeding 43 metric number)','KGM',10,0,0 UNION ALL
Select '5206130000','Measuring less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not exceeding 52 metric number)','KGM',10,0,0 UNION ALL
Select '5206140000','Measuring less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric number)','KGM',10,0,0 UNION ALL
Select '5206150000','Measuring less than 125 decitex (exceeding 80 metric number)','KGM',10,0,0 UNION ALL

Select '5206210000','Measuring 714.29 decitex or more (not exceeding 14 metric  number) ','KGM',10,0,0 UNION ALL
Select '5206220000',' Measuring less than 714.29 decitex but not less than 232.56 decitex (exceeding 14 metric number but not exceeding 43  metric number) ','KGM',10,0,0 UNION ALL
Select '5206230000',' Measuring less than 232.56 decitex but not less than 192.31  decitex (exceeding 43 metric number but not exceeding 52  metric number) ','KGM',10,0,0 UNION ALL
Select '5206240000','Measuring less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric number) ','KGM',10,0,0 UNION ALL
Select '5206250000','Measuring less than 125 decitex (exceeding 80 metric number) ','KGM',10,0,0 UNION ALL

Select '5206310000','Measuring per single yarn 714.29 decitex or more (not exceeding 14 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5206320000','Measuring per single yarn less than 714.29 decitex but not less   than 232.56 decitex (exceeding 14 metric number but not   exceeding 43 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5206330000','Measuring per single yarn less than 232.56 decitex but not less  than 192.31 decitex (exceeding 43 metric number but not  exceeding 52 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5206340000','Measuring per single yarn less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding 80 metric number per single yarn) ','KGM',10,0,0 UNION ALL
Select '5206350000','Measuring per single yarn less than 125 decitex (exceeding 80  metric number per single yarn) ','KGM',10,0,0 UNION ALL

Select '5206410000','Measuring per single yarn 714.29 decitex or more (not exceeding  14 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5206420000','Measuring per single yarn less than 714.29 decitex but not less  than 232.56 decitex (exceeding 14 metric number but not    exceeding 43 metric number per single yarn)','KGM',10,0,0 UNION ALL
Select '5206430000','Measuring per single yarn less than 232.56 decitex but not less than 192.31 decitex (exceeding 43 metric number but not exceeding 52 metric number per single yarn)   ','KGM',10,0,0 UNION ALL
Select '5206440000','Measuring per single yarn less than 192.31 decitex but not less than 125 decitex (exceeding 52 metric number but not exceeding  80 metric number per single yarn )  ','KGM',10,0,0 UNION ALL
Select '5206450000','Measuring per single yarn less than 125 decitex (exceeding 80  metric number per single yarn) ','KGM',10,0,0 UNION ALL

Select '5207100000','Containing 85 % or more by weight of cotton','KGM',10,0,0 UNION ALL
Select '5207900000','Other','KGM',10,0,0 UNION ALL


Select '5208110000','Plain weave, weighing not more than 100 g/m2','KGM',10,0,0 UNION ALL
Select '5208120000','Plain weave, weighing more than 100 g/m2','KGM',10,0,0 UNION ALL
Select '5208130000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5208190000','Other fabrics','KGM',10,0,0 UNION ALL

Select '5208210000','Plain weave, weighing not more than 100 g/m2','KGM',10,0,0 UNION ALL
Select '5208220000','Plain weave, weighing more than 100 g/m2','KGM',10,0,0 UNION ALL
Select '5208230000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5208290000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5208311000','Voile 
','KGM',10,0,0 UNION ALL
Select '5208319000','Other','KGM',10,0,0 UNION ALL
Select '5208320000','Plain weave, weighing more than 100 g/m2','KGM',10,0,0 UNION ALL
Select '5208330000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5208390000','Other fabrics','KGM',10,0,0 UNION ALL


Select '5208411000','Ikat fabric','KGM',10,0,0 UNION ALL
Select '5208419000','Other','KGM',10,0,0 UNION ALL

Select '5208421000','Ikat fabric','KGM',10,0,0 UNION ALL
Select '5208429000','Other','KGM',10,0,0 UNION ALL
Select '5208430000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5208490000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5208511000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5208519000','Other','KGM',10,0,0 UNION ALL

Select '5208521000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5208529000','Other','KGM',10,0,0 UNION ALL

Select '5208591000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5208592000','Other, 3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5208599000','Other','KGM',10,0,0 UNION ALL



Select '5209111000','Duck and canvas','KGM',10,0,0 UNION ALL
Select '5209119000','Other','KGM',10,0,0 UNION ALL
Select '5209120000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5209190000',' Other fabrics','KGM',10,0,0 UNION ALL

Select '5209210000','Plain weave','KGM',10,0,0 UNION ALL
Select '5209220000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5209290000','Other fabrics','KGM',10,0,0 UNION ALL

Select '5209310000','Plain weave','KGM',10,0,0 UNION ALL
Select '5209320000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5209390000',' Other fabrics','KGM',10,0,0 UNION ALL

Select '5209410000',' Plain weave','KGM',10,0,0 UNION ALL
Select '5209420000','Denim','KGM',10,0,0 UNION ALL
Select '5209430000','Other fabrics of 3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5209490000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5209511000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5209519000','Other','KGM',10,0,0 UNION ALL

Select '5209521000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5209529000','Other','KGM',10,0,0 UNION ALL

Select '5209591000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5209599000','Other','KGM',10,0,0 UNION ALL


Select '5210110000','�Plain weave ','KGM',10,0,0 UNION ALL
Select '5210190000','Other fabrics ','KGM',10,0,0 UNION ALL

Select '5210210000','Plain weave','KGM',10,0,0 UNION ALL
Select '5210290000',' Other fabrics','KGM',10,0,0 UNION ALL

Select '5210310000','Plain weave','KGM',10,0,0 UNION ALL
Select '5210320000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5210390000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5210411000','Ikat fabric','KGM',10,0,0 UNION ALL
Select '5210419000','Other','KGM',10,0,0 UNION ALL
Select '5210490000','Other fabrics','KGM',10,0,0 UNION ALL


Select '5210511000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5210519000','Other','KGM',10,0,0 UNION ALL

Select '5210591000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5210599000','Other','KGM',10,0,0 UNION ALL


Select '5211110000','Plain weave','KGM',10,0,0 UNION ALL
Select '5211120000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5211190000','Other fabrics','KGM',10,0,0 UNION ALL
Select '5211200000',' Bleached','KGM',10,0,0 UNION ALL

Select '5211310000',' Plain weave','KGM',10,0,0 UNION ALL
Select '5211320000','3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5211390000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5211411000','Ikat fabric','KGM',10,0,0 UNION ALL
Select '5211419000','Other','KGM',10,0,0 UNION ALL
Select '5211420000',' Denim','KGM',10,0,0 UNION ALL
Select '5211430000','Other fabrics of 3thread or 4thread twill, including cross twill','KGM',10,0,0 UNION ALL
Select '5211490000',' Other fabrics','KGM',10,0,0 UNION ALL


Select '5211511000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5211519000','Other','KGM',10,0,0 UNION ALL

Select '5211521000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5211529000','Other','KGM',10,0,0 UNION ALL

Select '5211591000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5211599000','Other','KGM',10,0,0 UNION ALL


Select '5212110000','Unbleached','KGM',10,0,0 UNION ALL
Select '5212120000','Bleached','KGM',10,0,0 UNION ALL
Select '5212130000',' Dyed','KGM',10,0,0 UNION ALL
Select '5212140000',' Of yarns of different colours','KGM',10,0,0 UNION ALL

Select '5212151000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5212159000','Other','KGM',10,0,0 UNION ALL

Select '5212210000','Unbleached','KGM',10,0,0 UNION ALL
Select '5212220000','Bleached','KGM',10,0,0 UNION ALL
Select '5212230000',' Dyed','KGM',10,0,0 UNION ALL
Select '5212240000',' Of yarns of different colours','KGM',10,0,0 UNION ALL

Select '5212251000','Printed by the traditional batik process','KGM',10,0,0 UNION ALL
Select '5212259000','Other','KGM',10,0,0 UNION ALL

Select '5301100000','Flax, raw or retted','KGM',0,0,0 UNION ALL

Select '5301210000','Broken or scutched','KGM',0,0,0 UNION ALL
Select '5301290000','Other','KGM',0,0,0 UNION ALL
Select '5301300000','Flax tow and waste','KGM',0,0,0 UNION ALL

Select '5302100000','True hemp, raw or retted','KGM',0,0,0 UNION ALL
Select '5302900000','Other','KGM',0,0,0 UNION ALL

Select '5303100000','Jute and other textile bast fibres, raw or retted','KGM',0,0,0 UNION ALL
Select '5303900000','Other','KGM',0,0,0 UNION ALL

Select '5305001000','Sisal and other textile fibres of the genus Agave; tow and waste of these fibres (including yarn waste and garnetted stock)','KGM',0,0,0 UNION ALL

Select '5305002100','Coconut fibres, raw','KGM',0,0,0 UNION ALL
Select '5305002200','Other coconut fibres','KGM',0,0,0 UNION ALL
Select '5305002300','Abaca fibres','KGM',0,0,0 UNION ALL
Select '5305009000','Other','KGM',0,0,0 UNION ALL

Select '5306100000','Single','KGM',0,0,0 UNION ALL
Select '5306200000','Multiple (folded) or cabled','KGM',0,0,0 UNION ALL

Select '5307100000','Single','KGM',0,0,0 UNION ALL
Select '5307200000',' Multiple (folded) or cabled','KGM',0,0,0 UNION ALL

Select '5308100000','Coir yarn','KGM',0,0,0 UNION ALL
Select '5308200000','True hemp yarn','KGM',0,0,0 UNION ALL

Select '5308901000','Paper yarn','KGM',0,0,0 UNION ALL
Select '5308909000',' Other','KGM',0,0,0 UNION ALL


Select '5309110000',' Unbleached or bleached','KGM',0,0,0 UNION ALL
Select '5309190000',' Other','KGM',0,0,0 UNION ALL

Select '5309210000','Unbleached or bleached','KGM',0,0,0 UNION ALL
Select '5309290000',' Other','KGM',0,0,0 UNION ALL


Select '5310101000','Plain','KGM',0,0,0 UNION ALL
Select '5310109000','Other','KGM',0,0,0 UNION ALL
Select '5310900000','Other','KGM',0,0,0 UNION ALL

Select '5311001000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '5311002000','Abaca Burlap','KGM',0,0,0 UNION ALL
Select '5311009000','Other','KGM',0,0,0 UNION ALL


Select '5401101000','Put up for retail sale','KGM',20,0,0 UNION ALL
Select '5401109000','Other','KGM',0,0,0 UNION ALL

Select '5401201000',' Put up for retail sale','KGM',10,0,0 UNION ALL
Select '5401209000','Other','KGM',0,0,0 UNION ALL


Select '5402110000','Of aramids','KGM',0,0,0 UNION ALL
Select '5402190000','Other','KGM',0,0,0 UNION ALL
Select '5402200000','High tenacity yarn of polyesters, whether or not textured','KGM',0,0,0 UNION ALL

Select '5402310000','Of nylon or other polyamides, measuring per single yarn not more than 50 tex','KGM',10,0,0 UNION ALL
Select '5402320000','Of nylon or other polyamides, measuring per single yarn more  than 50 tex','KGM',10,0,0 UNION ALL
Select '5402330000','Of polyesters','KGM',10,0,0 UNION ALL
Select '5402340000','Of polypropylene','KGM',10,0,0 UNION ALL
Select '5402390000','Other','KGM',10,0,0 UNION ALL


Select '5402441000','Of polyesters','KGM',0,0,0 UNION ALL
Select '5402442000','Of polypropylene','KGM',15,0,0 UNION ALL
Select '5402449000','Other','KGM',0,0,0 UNION ALL
Select '5402450000','Other, of nylon or other polyamides','KGM',0,0,0 UNION ALL
Select '5402460000','Other, of polyesters, partially oriented','KGM',0,0,0 UNION ALL
Select '5402470000','Other, of polyesters','KGM',0,0,0 UNION ALL
Select '5402480000','Other, of polypropylene','KGM',15,0,0 UNION ALL
Select '5402490000','Other','KGM',0,0,0 UNION ALL

Select '5402510000','Of nylon or other polyamides','KGM',0,0,0 UNION ALL
Select '5402520000',' Of polyesters','KGM',0,0,0 UNION ALL
Select '5402530000',' Of polypropylene','KGM',15,0,0 UNION ALL
Select '5402590000','Other','KGM',0,0,0 UNION ALL

Select '5402610000','Of nylon or other polyamides','KGM',0,0,0 UNION ALL
Select '5402620000','Of polyesters','KGM',0,0,0 UNION ALL
Select '5402630000','Of polypropylene','KGM',15,0,0 UNION ALL
Select '5402690000',' Other','KGM',0,0,0 UNION ALL

Select '5403100000','High tenacity yarn of viscose rayon','KGM',0,0,0 UNION ALL


Select '5403311000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403319000','Other','KGM',0,0,0 UNION ALL

Select '5403321000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403329000','Other','KGM',0,0,0 UNION ALL

Select '5403331000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403339000','Other','KGM',0,0,0 UNION ALL

Select '5403391000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403399000','Other','KGM',0,0,0 UNION ALL


Select '5403411000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403419000','Other','KGM',0,0,0 UNION ALL

Select '5403421000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403429000','Other','KGM',0,0,0 UNION ALL

Select '5403491000','Textured yarn','KGM',0,0,0 UNION ALL
Select '5403499000','Other','KGM',0,0,0 UNION ALL


Select '5404110000','�Elastomeric','KGM',10,0,0 UNION ALL
Select '5404120000','�Other, of polypropylene','KGM',10,0,0 UNION ALL
Select '5404190000','�Other','KGM',10,0,0 UNION ALL
Select '5404900000','Other','KGM',10,0,0 UNION ALL

Select '5405000000','Artificial monofilament of 67 decitex or more and of which no crosssectional dimension exceeds 1 mm; strip and the like (for example, artificial straw) of artificial textile materials of an apparent width not exceeding 5 mm.','KGM',10,0,0 UNION ALL

Select '5406000000','Manmade filament yarn (other than sewing thread), put up for retail sale.','KGM',10,0,0 UNION ALL



Select '5407102100','Unbleached ','KGM',10,0,0 UNION ALL
Select '5407102900','Other','KGM',10,0,0 UNION ALL

Select '5407109100','Unbleached ','KGM',10,0,0 UNION ALL
Select '5407109900','Other','KGM',10,0,0 UNION ALL
Select '5407200000','Woven fabrics obtained from strip or the like','KGM',10,0,0 UNION ALL
Select '5407300000','Fabrics specified in Note 9 to Section XI','KGM',10,0,0 UNION ALL


Select '5407411000','Woven nylon mesh fabrics of untwisted filament yarn suitable  for use as reinforcing material for tarpaulins','KGM',10,0,0 UNION ALL
Select '5407419000','Other','KGM',10,0,0 UNION ALL
Select '5407420000',' Dyed','KGM',10,0,0 UNION ALL
Select '5407430000',' Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5407440000',' Printed','KGM',10,0,0 UNION ALL

Select '5407510000','Unbleached or bleached      ','KGM',10,0,0 UNION ALL
Select '5407520000','Dyed','KGM',10,0,0 UNION ALL
Select '5407530000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5407540000','Printed','KGM',10,0,0 UNION ALL


Select '5407611000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5407619000','Other','KGM',10,0,0 UNION ALL

Select '5407691000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5407699000','Other','KGM',10,0,0 UNION ALL

Select '5407710000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5407720000',' Dyed','KGM',10,0,0 UNION ALL
Select '5407730000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5407740000',' Printed','KGM',10,0,0 UNION ALL

Select '5407810000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5407820000',' Dyed','KGM',10,0,0 UNION ALL
Select '5407830000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5407840000',' Printed','KGM',10,0,0 UNION ALL

Select '5407910000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5407920000',' Dyed','KGM',10,0,0 UNION ALL
Select '5407930000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5407940000','Printed','KGM',10,0,0 UNION ALL


Select '5408101000','Unbleached','KGM',10,0,0 UNION ALL
Select '5408109000','Other','KGM',10,0,0 UNION ALL

Select '5408210000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5408220000','Dyed','KGM',10,0,0 UNION ALL
Select '5408230000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5408240000','Printed','KGM',10,0,0 UNION ALL

Select '5408310000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5408320000',' Dyed','KGM',10,0,0 UNION ALL
Select '5408330000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5408340000','Printed','KGM',10,0,0 UNION ALL

Select '5501100000','Of nylon or other polyamides','KGM',0,0,0 UNION ALL
Select '5501200000','Of polyesters','KGM',0,0,0 UNION ALL
Select '5501300000','Acrylic or modacrylic','KGM',0,0,0 UNION ALL
Select '5501400000','Of polypropylene','KGM',0,0,0 UNION ALL
Select '5501900000','Other','KGM',0,0,0 UNION ALL

Select '5502100000','Of cellulose acetate','KGM',0,0,0 UNION ALL
Select '5502900000','Other','KGM',0,0,0 UNION ALL


Select '5503110000','Of aramids','KGM',0,0,0 UNION ALL
Select '5503190000','Other','KGM',0,0,0 UNION ALL
Select '5503200000','Of polyesters','KGM',0,0,0 UNION ALL
Select '5503300000','Acrylic or modacrylic','KGM',0,0,0 UNION ALL
Select '5503400000','Of polypropylene','KGM',0,0,0 UNION ALL

Select '5503901000','Of polyvinyl alcohol','KGM',0,0,0 UNION ALL
Select '5503909000','Other','KGM',0,0,0 UNION ALL

Select '5504100000','Of viscose rayon','KGM',0,0,0 UNION ALL
Select '5504900000','Other','KGM',0,0,0 UNION ALL

Select '5505100000','Of synthetic fibres','KGM',0,0,0 UNION ALL
Select '5505200000','Of artificial fibres','KGM',0,0,0 UNION ALL

Select '5506100000','Of nylon or other polyamides','KGM',0,0,0 UNION ALL
Select '5506200000','Of polyesters','KGM',0,0,0 UNION ALL
Select '5506300000','Acrylic or modacrylic','KGM',0,0,0 UNION ALL
Select '5506400000','Of polypropylene','KGM',0,0,0 UNION ALL
Select '5506900000','Other','KGM',0,0,0 UNION ALL

Select '5507000000','Artificial staple fibres, carded, combed or otherwise processed for spinning.','KGM',0,0,0 UNION ALL


Select '5508101000','Put up for retail sales','KGM',20,0,0 UNION ALL
Select '5508109000','Other','KGM',20,0,0 UNION ALL

Select '5508201000','Put up for retail sales','KGM',20,0,0 UNION ALL
Select '5508209000','Other','KGM',30,0,0 UNION ALL


Select '5509110000',' Single yarn','KGM',10,0,0 UNION ALL
Select '5509120000',' Multiple (folded) or cabled yarn','KGM',10,0,0 UNION ALL

Select '5509210000',' Single yarn','KGM',10,0,0 UNION ALL
Select '5509220000',' Multiple (folded) or cabled yarn','KGM',10,0,0 UNION ALL

Select '5509310000',' Single yarn','KGM',0,0,0 UNION ALL
Select '5509320000',' Multiple (folded) or cabled yarn','KGM',0,0,0 UNION ALL

Select '5509410000','Single yarn','KGM',10,0,0 UNION ALL
Select '5509420000','Multiple (folded) or cabled yarn','KGM',10,0,0 UNION ALL

Select '5509510000','Mixed mainly or solely with artificial staple fibres','KGM',10,0,0 UNION ALL

Select '5509521000','Single yarn','KGM',10,0,0 UNION ALL
Select '5509529000','Other','KGM',10,0,0 UNION ALL
Select '5509530000','Mixed mainly or solely with cotton','KGM',10,0,0 UNION ALL
Select '5509590000','Other','KGM',10,0,0 UNION ALL

Select '5509610000','Mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5509620000','Mixed mainly or solely with cotton','KGM',10,0,0 UNION ALL
Select '5509690000','Other','KGM',10,0,0 UNION ALL

Select '5509910000','Mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5509920000','Mixed mainly or solely with cotton','KGM',10,0,0 UNION ALL
Select '5509990000','Other','KGM',10,0,0 UNION ALL


Select '5510110000','Single yarn','KGM',10,0,0 UNION ALL
Select '5510120000','Multiple (folded) or cabled yarn','KGM',10,0,0 UNION ALL
Select '5510200000','Other yarn, mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5510300000','Other yarn, mixed mainly or solely with cotton','KGM',10,0,0 UNION ALL
Select '5510900000','Other yarn','KGM',10,0,0 UNION ALL


Select '5511101000','Knitting yarn, crochet thread and embroidery thread','KGM',10,0,0 UNION ALL
Select '5511109000',' Other','KGM',20,0,0 UNION ALL

Select '5511201000','Knitting yarn, crochet thread and embroidery thread','KGM',10,0,0 UNION ALL
Select '5511209000',' Other','KGM',20,0,0 UNION ALL
Select '5511300000','Of artificial staple fibres','KGM',10,0,0 UNION ALL


Select '5512110000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5512190000',' Other','KGM',10,0,0 UNION ALL

Select '5512210000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5512290000',' Other','KGM',10,0,0 UNION ALL

Select '5512910000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5512990000',' Other','KGM',10,0,0 UNION ALL


Select '5513110000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5513120000','3thread or 4thread twill, including cross twill, of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5513130000','Other woven fabrics of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5513190000','Other woven fabrics','KGM',10,0,0 UNION ALL

Select '5513210000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5513230000','Other woven fabrics of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5513290000','Other woven fabrics','KGM',10,0,0 UNION ALL

Select '5513310000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5513390000','Other woven fabrics','KGM',10,0,0 UNION ALL

Select '5513410000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5513490000','Other woven fabrics','KGM',10,0,0 UNION ALL


Select '5514110000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5514120000','3thread or 4thread twill, including cross twill, of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5514190000','Other woven fabrics','KGM',10,0,0 UNION ALL

Select '5514210000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5514220000','3thread or 4thread twill, including cross twill, of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5514230000','Other woven fabrics of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5514290000',' Other woven fabrics','KGM',10,0,0 UNION ALL
Select '5514300000',' Of yarns of different colours ','KGM',10,0,0 UNION ALL

Select '5514410000','Of polyester staple fibres, plain weave','KGM',10,0,0 UNION ALL
Select '5514420000','3thread or 4thread twill, including cross twill, of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5514430000','Other woven fabrics of polyester staple fibres','KGM',10,0,0 UNION ALL
Select '5514490000','Other woven fabrics','KGM',10,0,0 UNION ALL


Select '5515110000','Mixed mainly or solely with viscose rayon staple fibres','KGM',10,0,0 UNION ALL
Select '5515120000','Mixed mainly or solely with man made filaments','KGM',10,0,0 UNION ALL
Select '5515130000','Mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5515190000',' Other','KGM',10,0,0 UNION ALL

Select '5515210000','Mixed mainly or solely with manmade filaments','KGM',10,0,0 UNION ALL
Select '5515220000','Mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5515290000',' Other','KGM',10,0,0 UNION ALL

Select '5515910000','Mixed mainly or solely with manmade filaments','KGM',10,0,0 UNION ALL

Select '5515991000','Mixed mainly or solely with wool or fine animal hair','KGM',10,0,0 UNION ALL
Select '5515999000','Other','KGM',10,0,0 UNION ALL


Select '5516110000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5516120000',' Dyed','KGM',10,0,0 UNION ALL
Select '5516130000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5516140000',' Printed','KGM',10,0,0 UNION ALL

Select '5516210000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5516220000',' Dyed','KGM',10,0,0 UNION ALL
Select '5516230000','Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5516240000',' Printed','KGM',10,0,0 UNION ALL

Select '5516310000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5516320000','Dyed','KGM',10,0,0 UNION ALL
Select '5516330000',' Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5516340000',' Printed','KGM',10,0,0 UNION ALL

Select '5516410000','Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5516420000',' Dyed','KGM',10,0,0 UNION ALL
Select '5516430000',' Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5516440000','Printed','KGM',10,0,0 UNION ALL

Select '5516910000',' Unbleached or bleached','KGM',10,0,0 UNION ALL
Select '5516920000',' Dyed','KGM',10,0,0 UNION ALL
Select '5516930000',' Of yarns of different colours','KGM',10,0,0 UNION ALL
Select '5516940000',' Printed','KGM',10,0,0 UNION ALL


Select '5601210000','Of cotton','KGM',20,0,0 UNION ALL

Select '5601221000','Wrapped cigarette tow','KGM',20,0,0 UNION ALL
Select '5601229000','Other','KGM',20,0,0 UNION ALL
Select '5601290000','Other','KGM',20,0,0 UNION ALL

Select '5601301000','Polyamide fibre flock','KGM',20,0,0 UNION ALL
Select '5601302000','Polypropylene fibre flock','KGM',20,0,0 UNION ALL
Select '5601309000','Other','KGM',20,0,0 UNION ALL

Select '5602100000','Needleloom felt and stitchbonded fibre fabrics','KGM',20,0,0 UNION ALL

Select '5602210000','Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '5602290000',' Of other textile materials      ','KGM',20,0,0 UNION ALL
Select '5602900000',' Other','KGM',20,0,0 UNION ALL


Select '5603110000','Weighing not more than 25 g/m2','KGM',20,0,0 UNION ALL
Select '5603120000','Weighing more than 25 g/m2 but not more than 70 g/m2','KGM',20,0,0 UNION ALL
Select '5603130000','Weighing more than 70 g/m2 but not more than 150 g/m2','KGM',20,0,0 UNION ALL
Select '5603140000',' Weighing more than 150 g/m2','KGM',20,0,0 UNION ALL

Select '5603910000',' Weighing not more than 25 g/m2','KGM',20,0,0 UNION ALL
Select '5603920000','Weighing more than 25 g/m2 but not more than 70 g/m2','KGM',20,0,0 UNION ALL
Select '5603930000','Weighing more than 70 g/m2 but not more than 150 g/m2','KGM',20,0,0 UNION ALL
Select '5603940000',' Weighing more than 150 g/m2','KGM',20,0,0 UNION ALL

Select '5604100000','Rubber thread and cord, textile covered','KGM',15,0,0 UNION ALL

Select '5604901000','Imitation catgut, of silk yarn','KGM',5,0,0 UNION ALL
Select '5604902000','Rubber impregnated textile thread yarn','KGM',15,0,0 UNION ALL
Select '5604903000','High tenacity yarn of polyesters, of nylon or other polyamides or of viscose rayon','KGM',0,0,0 UNION ALL
Select '5604909000','Other','KGM',20,0,0 UNION ALL

Select '5605000000','Metallised yarn, whether or not gimped, being textile yarn, or strip or the like of heading 54.04 or 54.05, combined with metal in the form of thread, strip or powder or covered with metal.','KGM',0,0,0 UNION ALL

Select '5606000000','Gimped yarn, and strip and the like of heading 54.04 or 54.05, gimped (other than those of heading 56.05 and gimped horsehair yarn); chenille yarn (including flock chenille yarn); loop waleyarn.','KGM',0,0,0 UNION ALL


Select '5607210000','Binder or baler twine','KGM',0,0,0 UNION ALL
Select '5607290000','�Other','KGM',0,0,0 UNION ALL

Select '5607410000',' Binder or baler twine','KGM',5,0,0 UNION ALL
Select '5607490000',' Other','KGM',5,0,0 UNION ALL

Select '5607501000','Vbelt cord of manmade fibres treated with resorcinol formaldehyde; polyamide and polytetrafluoroethylene yarns measuring more than 10,000 decitex, of a kind used for sealing pumps, valves and similar articles','KGM',5,0,0 UNION ALL
Select '5607509000','Other','KGM',5,0,0 UNION ALL

Select '5607901000','Of artificial fibres','KGM',5,0,0 UNION ALL

Select '5607902100','Of Abaca (Manila hemp or Musa textilis Nee) ','KGM',0,0,0 UNION ALL
Select '5607902200','Of other hard (leaf) fibres','KGM',0,0,0 UNION ALL
Select '5607903000','Of jute or other textile bast fibres of heading 53.03      ','KGM',0,0,0 UNION ALL
Select '5607909000','Other','KGM',2,0,0 UNION ALL


Select '5608110000','Made up fishing nets','KGM',5,0,0 UNION ALL

Select '5608192000','Net bags','KGM',5,0,0 UNION ALL
Select '5608199000','Other','KGM',5,0,0 UNION ALL

Select '5608901000','Net bags','KGM',5,0,0 UNION ALL
Select '5608909000','Other','KGM',5,0,0 UNION ALL

Select '5609000000','Articles of yarn, strip or the like of heading 54.04 or 54.05, twine, cordage, rope or cables, not elsewhere specified or included.','KGM',5,0,0 UNION ALL


Select '5701101000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5701109000',' Other','MTK',20,0,0 UNION ALL


Select '5701901100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5701901900','Other','MTK',20,0,0 UNION ALL
Select '5701902000','Of jute fibres','MTK',0,0,0 UNION ALL

Select '5701909100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5701909900','Other','MTK',20,0,0 UNION ALL

Select '5702100000','"Kelem", "Schumacks", "Karamanie" and similar handwoven rugs','MTK',20,0,0 UNION ALL
Select '5702200000','Floor coverings of coconut fibres (coir)','MTK',0,0,0 UNION ALL

Select '5702310000','Of wool or fine animal hair','MTK',20,0,0 UNION ALL
Select '5702320000','Of manmade textile materials','MTK',20,0,0 UNION ALL

Select '5702391000','Of cotton','MTK',20,0,0 UNION ALL
Select '5702392000','Of jute fibres','MTK',0,0,0 UNION ALL
Select '5702399000','Other','MTK',20,0,0 UNION ALL


Select '5702411000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702419000','Other','MTK',20,0,0 UNION ALL

Select '5702421000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702429000','Other','MTK',20,0,0 UNION ALL


Select '5702491100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702491900','Other','MTK',20,0,0 UNION ALL
Select '5702492000','Of jute fibres','MTK',0,0,0 UNION ALL

Select '5702499100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702499900','Other','MTK',20,0,0 UNION ALL

Select '5702501000','Of cotton','MTK',20,0,0 UNION ALL
Select '5702502000','Of jute fibres','MTK',0,0,0 UNION ALL
Select '5702509000','Other','MTK',20,0,0 UNION ALL


Select '5702911000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702919000','Other','MTK',20,0,0 UNION ALL

Select '5702921000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702929000','Other','MTK',20,0,0 UNION ALL


Select '5702991100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702991900','Other','MTK',20,0,0 UNION ALL
Select '5702992000','Of jute fibres','MTK',0,0,0 UNION ALL

Select '5702999100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5702999900','Other','MTK',20,0,0 UNION ALL


Select '5703101000','Floor mats, of a kind used for motor vehicles of heading 87.02, 87.03 or 87.04 ','MTK',20,0,0 UNION ALL
Select '5703102000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5703103000','Flooring carpets of a kind used for motor vehicles of headings 87.02, 87.03 or 87.04','MTK',20,0,0 UNION ALL
Select '5703109000','Other','MTK',20,0,0 UNION ALL

Select '5703201000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5703209000','Other','MTK',20,0,0 UNION ALL

Select '5703301000','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5703309000','Other','MTK',20,0,0 UNION ALL


Select '5703901100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5703901900','Other','MTK',20,0,0 UNION ALL

Select '5703902100','Floor mats, of a kind used for motor vehicles of heading 87.02, 87.03 or 87.04 ','MTK',0,0,0 UNION ALL
Select '5703902200','Flooring carpets of a kind used for motor vehicles of headings 87.02, 87.03 or 87.04','MTK',0,0,0 UNION ALL
Select '5703902900','Other','MTK',0,0,0 UNION ALL

Select '5703909100','Floor mats of a kind used for motor vehicles of heading 87.02, 87.03 or 87.04','MTK',20,0,0 UNION ALL
Select '5703909200','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5703909300','Flooring carpets of a kind used for motor vehicles of headings 87.02, 87.03 or 87.04','MTK',20,0,0 UNION ALL
Select '5703909900','Other','MTK',20,0,0 UNION ALL

Select '5704100000','Tiles, having a maximum surface area of 0.3 m2','MTK',20,0,0 UNION ALL
Select '5704200000','Tiles, having a maximum surface area exceeding 0.3 m� but not exceeding 1 m�','MTK',20,0,0 UNION ALL
Select '5704900000','Other','MTK',20,0,0 UNION ALL



Select '5705001100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5705001900','Other','MTK',20,0,0 UNION ALL

Select '5705002100','Nonwoven floor coverings, of a kind used for motor vehicles of heading 87.02, 87.03 or 87.04 ','MTK',0,0,0 UNION ALL
Select '5705002900','Other','MTK',0,0,0 UNION ALL

Select '5705009100','Prayer rugs','MTK',0,0,0 UNION ALL
Select '5705009200','Nonwoven floor coverings, of a kind used for motor vehicles of heading 87.02, 87.03 or 87.04 ','MTK',20,0,0 UNION ALL
Select '5705009900','Other','MTK',20,0,0 UNION ALL


Select '5801101000','Impregnated, coated, covered or laminated','KGM',0,0,0 UNION ALL
Select '5801109000','Other','KGM',7,0,0 UNION ALL


Select '5801211000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801219000','Other','KGM',7,0,0 UNION ALL

Select '5801221000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801229000','Other','KGM',7,0,0 UNION ALL

Select '5801231000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801239000','Other','KGM',7,0,0 UNION ALL

Select '5801261000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801269000','Other','KGM',7,0,0 UNION ALL

Select '5801271000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801279000','Other','KGM',7,0,0 UNION ALL


Select '5801311000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801319000','Other','KGM',7,0,0 UNION ALL

Select '5801321000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801329000','Other','KGM',7,0,0 UNION ALL

Select '5801331000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801339000','Other','KGM',7,0,0 UNION ALL

Select '5801361000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801369000','Other','KGM',7,0,0 UNION ALL

Select '58013710','Impregnated, coated, covered or laminated:','',0,0,0 UNION ALL
Select '5801371010','Warp pile fabrics, cut','KGM',20,0,0 UNION ALL
Select '5801371020','Warp pile fabrics, uncut','KGM',20,0,0 UNION ALL
Select '58013790','Other:','',0,0,0 UNION ALL
Select '5801379010','Warp pile fabrics, cut','KGM',7,0,0 UNION ALL
Select '5801379020','Warp pile fabrics, uncut','KGM',7,0,0 UNION ALL


Select '5801901100','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801901900','Other','KGM',0,0,0 UNION ALL

Select '5801909100','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5801909900','Other','KGM',0,0,0 UNION ALL


Select '5802110000','Unbleached','KGM',20,0,0 UNION ALL
Select '5802190000','�Other','KGM',20,0,0 UNION ALL

Select '5802201000','Of wool or  fine animal hair ','KGM',0,0,0 UNION ALL
Select '5802209000','Other','KGM',20,0,0 UNION ALL

Select '5802301000','Impregnated, coated or covered','KGM',20,0,0 UNION ALL
Select '5802302000','Woven, of cotton or of manmade fibres','KGM',7,0,0 UNION ALL
Select '5802303000','Woven, of other materials','KGM',0,0,0 UNION ALL
Select '5802309000','Other','KGM',20,0,0 UNION ALL

Select '5803001000','Of cotton','KGM',10,0,0 UNION ALL
Select '5803002000','Of manmade fibres ','KGM',20,0,0 UNION ALL
Select '5803003000','Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '5803009000','Other ','KGM',10,0,0 UNION ALL



Select '5804101100','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5804101900','Other','KGM',0,0,0 UNION ALL

Select '5804102100','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5804102900','Other','KGM',0,0,0 UNION ALL

Select '5804109100','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5804109900','Other','KGM',0,0,0 UNION ALL


Select '5804211000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5804219000','Other','KGM',0,0,0 UNION ALL

Select '5804291000','Impregnated, coated, covered or laminated','KGM',20,0,0 UNION ALL
Select '5804299000','Other','KGM',0,0,0 UNION ALL
Select '5804300000','Handmade lace','KGM',0,0,0 UNION ALL

Select '5805001000',' Of cotton','KGM',0,0,0 UNION ALL
Select '5805009000','Other','KGM',0,0,0 UNION ALL


Select '5806101000','Of silk','KGM',20,0,0 UNION ALL
Select '5806102000',' Of cotton','KGM',20,0,0 UNION ALL
Select '5806109000',' Other','KGM',20,0,0 UNION ALL

Select '5806201000','Sports tape of a kind used to wrap sports equipment grips     ','KGM',0,0,0 UNION ALL
Select '5806209000','Other','KGM',20,0,0 UNION ALL


Select '5806311000','Narrow  woven  fabrics  suitable  for  the  manufacture of inked  ribbons for typewriters or similar machines','KGM',20,0,0 UNION ALL
Select '5806312000','Backing of a kind used for electrical insulating paper','KGM',20,0,0 UNION ALL
Select '5806313000','Ribbons of a kind used for making slide fasteners and of a width not exceeding 12 mm','KGM',20,0,0 UNION ALL
Select '5806319000','Other','KGM',20,0,0 UNION ALL

Select '5806321000','Narrow woven fabrics suitable for the manufacture of inked  ribbons for typewriters or similar machines; safety seat belt fabrics','KGM',20,0,0 UNION ALL
Select '5806324000','Backing of a kind used for electrical insulating paper','KGM',20,0,0 UNION ALL
Select '5806325000','Ribbons of a kind used for making slide fasteners and of a width not exceeding 12 mm','KGM',20,0,0 UNION ALL
Select '5806329000','Other','KGM',20,0,0 UNION ALL

Select '5806391000','Of silk','KGM',20,0,0 UNION ALL

Select '5806399100','Backing of a kind used for electrical insulating paper','KGM',20,0,0 UNION ALL
Select '5806399200','Narrow woven fabrics suitable for the manufacture of inked ribbons for typewriters or similar machines','KGM',20,0,0 UNION ALL
Select '5806399300','Ribbons of a kind used for making slide fastener and of a width not exceeding 12 mm','KGM',20,0,0 UNION ALL
Select '5806399900','Other','KGM',20,0,0 UNION ALL
Select '5806400000','Fabrics consisting of warp without weft assembled by means of an adhesive (bolducs)','KGM',20,0,0 UNION ALL

Select '5807100000','Woven','KGM',20,0,0 UNION ALL

Select '5807901000','Of nonwoven fabrics','KGM',20,0,0 UNION ALL
Select '5807909000','Other','KGM',20,0,0 UNION ALL


Select '5808101000','Combined with rubber thread','KGM',20,0,0 UNION ALL
Select '5808109000','Other','KGM',20,0,0 UNION ALL

Select '5808901000','Combined with rubber thread','KGM',20,0,0 UNION ALL
Select '5808909000','Other','KGM',20,0,0 UNION ALL

Select '5809000000','Woven fabrics of metal thread and woven fabrics of metallised yarn of heading 56.05, of a kind used in apparel, as furnishing fabrics or for similar purposes, not elsewhere specified or included.','KGM',0,0,0 UNION ALL

Select '5810100000','Embroidery without visible ground','KGM',20,0,0 UNION ALL

Select '5810910000',' Of cotton','KGM',20,0,0 UNION ALL
Select '5810920000',' Of manmade fibres','KGM',20,0,0 UNION ALL
Select '5810990000',' Of other textile materials','KGM',20,0,0 UNION ALL


Select '5811001000','Of wool or fine or coarse animal hair','KGM',0,0,0 UNION ALL
Select '5811009000','Other','KGM',20,0,0 UNION ALL

Select '5901100000','Textile fabrics coated with gum or amylaceous substances, of a  kind used for the outer covers of books or the like','KGM',0,0,0 UNION ALL

Select '5901901000','Tracing cloth','KGM',0,0,0 UNION ALL
Select '5901902000','Prepared painting canvas','KGM',0,0,0 UNION ALL
Select '5901909000','Other','KGM',0,0,0 UNION ALL



Select '5902101100','Of nylon6 yarn','KGM',0,0,0 UNION ALL
Select '5902101900','Other','KGM',0,0,0 UNION ALL

Select '5902109100','Of nylon6 yarn','KGM',0,0,0 UNION ALL
Select '5902109900','Other','KGM',0,0,0 UNION ALL

Select '5902202000','Chafer fabric, rubberised','KGM',0,0,0 UNION ALL

Select '5902209100','Containing cotton','KGM',0,0,0 UNION ALL
Select '5902209900','Other','KGM',0,0,0 UNION ALL

Select '5902901000','Chafer fabric, rubberised','KGM',0,0,0 UNION ALL
Select '5902909000','Other','KGM',0,0,0 UNION ALL


Select '5903101000','Interlining','KGM',20,0,0 UNION ALL
Select '5903109000','Other','KGM',20,0,0 UNION ALL
Select '5903200000','With polyurethane','KGM',20,0,0 UNION ALL

Select '5903901000','Canvastype fabrics impregnated, coated, covered or laminated with nylon or other polyamides','KGM',20,0,0 UNION ALL
Select '5903909000','Other ','KGM',20,0,0 UNION ALL

Select '5904100000','�Linoleum','MTK',20,0,0 UNION ALL
Select '5904900000','�Other','MTK',20,0,0 UNION ALL


Select '5905001000','Of wool or fine or coarse animal hair','MTK',0,0,0 UNION ALL
Select '5905009000','Other','MTK',20,0,0 UNION ALL

Select '5906100000','Adhesive tape of a width not exceeding 20 cm','KGM',0,0,0 UNION ALL

Select '5906910000','Knitted or crocheted','KGM',0,0,0 UNION ALL

Select '5906991000','Rubberised sheeting suitable for hospital use','KGM',0,0,0 UNION ALL
Select '5906999000','Other','KGM',0,0,0 UNION ALL


Select '5907001000','Fabrics  impregnated,  coated  or  covered with oil or oilbased preparations','KGM',0,0,0 UNION ALL
Select '5907003000','Fabrics impregnated, coated or covered with fire resistant substances ','KGM',0,0,0 UNION ALL
Select '5907004000','Fabrics impregnated, coated or covered with flock velvet, the  entire surface of which is covered with textile flock','KGM',0,0,0 UNION ALL
Select '5907005000','Fabrics impregnated, coated or covered with wax, tar, bitumen or similar products','KGM',0,0,0 UNION ALL
Select '5907006000','Fabrics impregnated, coated or covered with other substances ','KGM',0,0,0 UNION ALL
Select '5907009000','Other','KGM',0,0,0 UNION ALL


Select '5908001000',' Wicks; incandescent gas mantles','KGM',0,0,0 UNION ALL
Select '5908009000',' Other','KGM',0,0,0 UNION ALL


Select '5909001000',' Fire hoses','KGM',0,0,0 UNION ALL
Select '5909009000',' Other','KGM',0,0,0 UNION ALL

Select '5910000000','Transmission or conveyor belts or belting, of textile material, whether or not impregnated, coated, covered or laminated with plastics, or reinforced with metal or other material.','KGM',0,0,0 UNION ALL

Select '5911100000','Textile fabrics, felt and feltlined  woven  fabrics, coated, covered  or laminated  with rubber, leather or other material, of a kind used for  card clothing,  and  similar fabrics  of a  kind  used  for other technical  purposes,  including  narrow  fabrics  made  of   velvet impregnated with rubber, for covering weaving spindles (weaving beams)','KGM',0,0,0 UNION ALL
Select '5911200000','Bolting cloth, whether or not made up','KGM',0,0,0 UNION ALL

Select '5911310000','Weighing less than 650 g/m2','KGM',0,0,0 UNION ALL
Select '5911320000','�Weighing 650 g/m2 or more','KGM',0,0,0 UNION ALL
Select '5911400000','�Straining cloth of a kind used in oil presses or the like, including  that of human hair','KGM',0,0,0 UNION ALL

Select '5911901000','Gaskets and seals','KGM',0,0,0 UNION ALL
Select '5911909000',' Other ','KGM',0,0,0 UNION ALL

Select '6001100000','"Long pile" fabrics','KGM',15,0,0 UNION ALL

Select '6001210000','Of cotton','KGM',15,0,0 UNION ALL
Select '6001220000','Of manmade fibres','KGM',15,0,0 UNION ALL
Select '6001290000','Of other textile materials','KGM',15,0,0 UNION ALL

Select '6001910000',' Of cotton','KGM',15,0,0 UNION ALL

Select '6001922000','Pile fabrics of 100% polyester staple fibres, of a width not less than 63.5 mm but not more than 76.2 mm, suitable for use in the manufacture of paint rollers','KGM',15,0,0 UNION ALL
Select '6001923000','Containing elastomeric yarn or rubber thread','KGM',15,0,0 UNION ALL
Select '6001929000','Other','KGM',15,0,0 UNION ALL


Select '6001991100','Containing elastomeric yarn or rubber thread','KGM',15,0,0 UNION ALL
Select '6001991900','Other','KGM',15,0,0 UNION ALL
Select '6001999000','Other','KGM',15,0,0 UNION ALL

Select '6002400000','Containing by weight 5% or more of elastomeric yarn but not containing rubber thread','KGM',15,0,0 UNION ALL
Select '6002900000','�Other ','KGM',15,0,0 UNION ALL

Select '6003100000','Of wool or fine animal hair','KGM',15,0,0 UNION ALL
Select '6003200000','Of cotton','KGM',15,0,0 UNION ALL
Select '6003300000','Of synthetic fibres','KGM',15,0,0 UNION ALL
Select '6003400000','Of artificial fibres','KGM',15,0,0 UNION ALL
Select '6003900000','Other','KGM',15,0,0 UNION ALL


Select '6004101000','Containing by  weight  not  more  than 20%  of  elastomeric yarn','KGM',15,0,0 UNION ALL
Select '6004109000','Other','KGM',15,0,0 UNION ALL
Select '6004900000','�Other','KGM',15,0,0 UNION ALL


Select '6005210000',' Unbleached or bleached','KGM',15,0,0 UNION ALL
Select '6005220000',' Dyed','KGM',15,0,0 UNION ALL
Select '6005230000',' Of yarns of different colours','KGM',15,0,0 UNION ALL
Select '6005240000',' Printed','KGM',15,0,0 UNION ALL

Select '6005350000','Fabrics specified in Subheading Note 1 to this Chapter','KGM',15,0,0 UNION ALL

Select '6005361000','Knitted swimwear fabrics of polyester and polybutylene terephthalate in which polyester predominates by weight','KGM',15,0,0 UNION ALL
Select '6005369000',' Other','KGM',15,0,0 UNION ALL

Select '6005371000','Knitted swimwear fabrics of polyester and polybutylene terephthalate in which polyester predominates by weight','KGM',15,0,0 UNION ALL
Select '6005379000',' Other','KGM',15,0,0 UNION ALL

Select '6005381000','Knitted swimwear fabrics of polyester and polybutylene terephthalate in which polyester predominates by weight','KGM',15,0,0 UNION ALL
Select '6005389000',' Other','KGM',15,0,0 UNION ALL

Select '6005391000','Knitted swimwear fabrics of polyester and polybutylene terephthalate in which polyester predominates by weight','KGM',15,0,0 UNION ALL
Select '6005399000','Other','KGM',15,0,0 UNION ALL

Select '6005410000','�Unbleached or bleached','KGM',15,0,0 UNION ALL
Select '6005420000','�Dyed ','KGM',15,0,0 UNION ALL
Select '6005430000','Of yarns of different colours ','KGM',15,0,0 UNION ALL
Select '6005440000','�Printed ','KGM',15,0,0 UNION ALL

Select '6005901000','Of wool or fine animal hair ','KGM',15,0,0 UNION ALL
Select '6005909000','Other','KGM',15,0,0 UNION ALL

Select '6006100000','Of wool or fine animal hair','KGM',15,0,0 UNION ALL

Select '6006210000',' Unbleached or bleached','KGM',15,0,0 UNION ALL
Select '6006220000',' Dyed','KGM',15,0,0 UNION ALL
Select '6006230000',' Of yarns of different colours','KGM',15,0,0 UNION ALL
Select '6006240000',' Printed','KGM',15,0,0 UNION ALL


Select '6006311000','Nylon fibre mesh of a kind used as backing material for mosaic tiles','KGM',15,0,0 UNION ALL
Select '6006312000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006319000','Other','KGM',15,0,0 UNION ALL

Select '6006321000','Nylon fibre mesh of a kind used as backing material for mosaic tiles','KGM',15,0,0 UNION ALL
Select '6006322000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006329000','Other','KGM',15,0,0 UNION ALL

Select '6006331000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006339000','Other','KGM',15,0,0 UNION ALL

Select '6006341000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006349000','Other','KGM',15,0,0 UNION ALL


Select '6006411000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006419000','Other','KGM',15,0,0 UNION ALL

Select '6006421000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006429000','Other','KGM',15,0,0 UNION ALL

Select '6006431000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006439000','Other','KGM',15,0,0 UNION ALL

Select '6006441000','Elastic (combined with rubber threads)','KGM',15,0,0 UNION ALL
Select '6006449000','Other','KGM',15,0,0 UNION ALL
Select '6006900000','Other','KGM',15,0,0 UNION ALL

Select '6101200000','Of cotton','UNT',0,0,0 UNION ALL
Select '6101300000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6101900000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6102100000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6102200000','Of cotton','UNT',0,0,0 UNION ALL
Select '6102300000',' Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6102900000',' Of other textile materials','UNT',0,0,0 UNION ALL

Select '6103100000','Suits','UNT',0,0,0 UNION ALL

Select '6103220000','Of cotton','UNT',0,0,0 UNION ALL
Select '6103230000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6103290000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6103310000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6103320000','Of cotton','UNT',0,0,0 UNION ALL
Select '6103330000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6103391000','Of ramie, linen or silk','UNT',0,0,0 UNION ALL
Select '6103399000','Other','UNT',0,0,0 UNION ALL

Select '6103410000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6103420000','Of cotton','UNT',0,0,0 UNION ALL
Select '6103430000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6103490000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6104130000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6104192000',' Of cotton','UNT',0,0,0 UNION ALL
Select '6104199000',' Other','UNT',0,0,0 UNION ALL

Select '6104220000','Of cotton','UNT',0,0,0 UNION ALL
Select '6104230000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6104290000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6104310000','�Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6104320000','�Of cotton','UNT',0,0,0 UNION ALL
Select '6104330000','�Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6104390000','�Of other textile materials','UNT',0,0,0 UNION ALL

Select '6104410000','�Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6104420000','Of cotton','UNT',0,0,0 UNION ALL
Select '6104430000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6104440000','Of artificial fibres','UNT',0,0,0 UNION ALL
Select '6104490000','�Of other textile materials','UNT',0,0,0 UNION ALL

Select '6104510000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6104520000','Of cotton','UNT',0,0,0 UNION ALL
Select '6104530000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6104590000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6104610000','�Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6104620000','�Of cotton','UNT',0,0,0 UNION ALL
Select '6104630000','�Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6104690000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6105100000','Of cotton','UNT',0,0,0 UNION ALL

Select '6105201000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6105202000','Of artificial fibres','UNT',0,0,0 UNION ALL
Select '6105900000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6106100000','�Of cotton','UNT',0,0,0 UNION ALL
Select '6106200000','�Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6106900000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6107110000',' Of cotton','UNT',0,0,0 UNION ALL
Select '6107120000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6107190000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6107210000','Of cotton','UNT',0,0,0 UNION ALL
Select '6107220000','�Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6107290000','�Of other textile materials','UNT',0,0,0 UNION ALL

Select '6107910000','Of cotton','UNT',0,0,0 UNION ALL
Select '6107990000','�Of other textile materials','UNT',0,0,0 UNION ALL


Select '6108110000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6108192000',' Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6108193000','Of cotton','UNT',0,0,0 UNION ALL
Select '6108194000','Of silk','UNT',0,0,0 UNION ALL
Select '6108199000',' Other','UNT',0,0,0 UNION ALL

Select '6108210000','Of cotton','UNT',0,0,0 UNION ALL
Select '6108220000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6108290000',' Of other textile materials','UNT',0,0,0 UNION ALL

Select '6108310000','�Of cotton','UNT',0,0,0 UNION ALL
Select '6108320000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6108390000','�Of other textile materials','UNT',0,0,0 UNION ALL

Select '6108910000','Of cotton','UNT',0,0,0 UNION ALL
Select '6108920000',' Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6108990000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6109101000',' For men or boys','UNT',0,0,0 UNION ALL
Select '6109102000','For women or girls','UNT',0,0,0 UNION ALL

Select '6109901000','For men or boys, of ramie, linen or silk','UNT',0,0,0 UNION ALL
Select '6109902000','For men or boys, of other textile materials','UNT',0,0,0 UNION ALL
Select '6109903000',' For women or girls','UNT',0,0,0 UNION ALL


Select '6110110000','Of wool','UNT',0,0,0 UNION ALL
Select '6110120000','�Of Kashmir (cashmere) goats ','UNT',0,0,0 UNION ALL
Select '6110190000','Other ','UNT',0,0,0 UNION ALL
Select '6110200000','Of cotton','UNT',0,0,0 UNION ALL
Select '6110300000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6110900000','Of other textile materials','UNT',0,0,0 UNION ALL

Select '6111200000','�Of cotton','KGM',0,0,0 UNION ALL
Select '6111300000','�Of synthetic fibres','KGM',0,0,0 UNION ALL

Select '6111901000','Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6111909000','Other','KGM',0,0,0 UNION ALL


Select '6112110000','Of cotton','UNT',0,0,0 UNION ALL
Select '6112120000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6112190000','Of other textile materials','UNT',0,0,0 UNION ALL
Select '6112200000','�Ski suits','UNT',0,0,0 UNION ALL

Select '6112310000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6112390000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6112411000','Mastectomy Swimwear (post breast surgery swimwear)','UNT',0,0,0 UNION ALL
Select '6112419000','Other ','UNT',0,0,0 UNION ALL

Select '6112491000','Mastectomy Swimwear (post breast surgery swimwear)','UNT',0,0,0 UNION ALL
Select '6112499000','Other ','UNT',0,0,0 UNION ALL

Select '6113001000','Divers"suits (wetsuits)','KGM',0,0,0 UNION ALL
Select '6113003000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6113004000','Other protective work garments','KGM',0,0,0 UNION ALL
Select '6113009000','Other','KGM',0,0,0 UNION ALL

Select '6114200000','Of cotton','KGM',0,0,0 UNION ALL

Select '6114302000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6114309000','Other','KGM',0,0,0 UNION ALL

Select '6114901000','Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6114909000','Other','KGM',0,0,0 UNION ALL


Select '6115101000','Stockings for varicose veins, of synthetic fibres','KGM',0,0,0 UNION ALL
Select '6115109000','Other','KGM',0,0,0 UNION ALL

Select '6115210000','Of synthetic  fibres,  measuring  per  single  yarn  less  than  67  decitex ','KGM',0,0,0 UNION ALL
Select '6115220000','Of synthetic fibres, measuring per single yarn 67 decitex or more','KGM',0,0,0 UNION ALL

Select '6115291000','Of cotton','KGM',0,0,0 UNION ALL
Select '6115299000','�Other','KGM',0,0,0 UNION ALL

Select '6115301000','�Of cotton','KGM',0,0,0 UNION ALL
Select '6115309000','�Other','KGM',0,0,0 UNION ALL

Select '6115940000','�Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6115950000','Of cotton','KGM',0,0,0 UNION ALL
Select '6115960000','Of synthetic fibres','KGM',0,0,0 UNION ALL
Select '6115990000',' Of other textile materials','KGM',0,0,0 UNION ALL


Select '6116101000','Divers'' gloves','KGM',0,0,0 UNION ALL
Select '6116109000','Other','KGM',0,0,0 UNION ALL

Select '6116910000',' Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6116920000','�Of cotton','KGM',0,0,0 UNION ALL
Select '6116930000','Of synthetic fibres','KGM',0,0,0 UNION ALL
Select '6116990000','Of other textile materials','KGM',0,0,0 UNION ALL


Select '6117101000','Of cotton','UNT',0,0,0 UNION ALL
Select '6117109000','Other   ','UNT',0,0,0 UNION ALL


Select '6117801100',' Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6117801900',' Other ','KGM',0,0,0 UNION ALL
Select '6117802000','Wrist bands, knee bands or ankle bands','KGM',0,0,0 UNION ALL
Select '6117809000',' Other ','KGM',0,0,0 UNION ALL
Select '6117900000','�Parts','KGM',15,0,0 UNION ALL


Select '6201110000','Of wool of fine animal hair','UNT',0,0,0 UNION ALL
Select '6201120000','Of cotton','UNT',0,0,0 UNION ALL
Select '6201130000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6201191000','Of silk','UNT',0,0,0 UNION ALL
Select '6201192000','Of ramie','UNT',0,0,0 UNION ALL
Select '6201199000','Other','UNT',0,0,0 UNION ALL

Select '6201910000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6201920000','Of cotton','UNT',0,0,0 UNION ALL
Select '6201930000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6201991000','Of silk','UNT',0,0,0 UNION ALL
Select '6201992000','Of ramie','UNT',0,0,0 UNION ALL
Select '6201999000','Other','UNT',0,0,0 UNION ALL


Select '6202110000','Of wool of fine animal hair','UNT',0,0,0 UNION ALL
Select '6202120000','Of cotton','UNT',0,0,0 UNION ALL
Select '6202130000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6202191000','Of silk','UNT',0,0,0 UNION ALL
Select '6202192000','Of ramie','UNT',0,0,0 UNION ALL
Select '6202199000','Other','UNT',0,0,0 UNION ALL

Select '6202910000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6202920000','Of cotton','UNT',0,0,0 UNION ALL
Select '6202930000',' Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6202991000','Of silk','UNT',0,0,0 UNION ALL
Select '6202992000','Of ramie','UNT',0,0,0 UNION ALL
Select '6202999000','Other','UNT',0,0,0 UNION ALL


Select '6203110000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6203120000','Of synthetic fibres','UNT',0,0,0 UNION ALL


Select '6203191100','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6203191900','Other','UNT',0,0,0 UNION ALL

Select '6203192100','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6203192900','Other','UNT',0,0,0 UNION ALL
Select '6203199000','Other','UNT',0,0,0 UNION ALL


Select '6203221000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6203229000','Other','UNT',0,0,0 UNION ALL
Select '6203230000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6203291000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6203299000','Other','UNT',0,0,0 UNION ALL

Select '6203310000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6203321000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6203329000','Other','UNT',0,0,0 UNION ALL
Select '6203330000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6203390000','�Of other textile materials','UNT',0,0,0 UNION ALL

Select '6203410000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6203421000','Bib and brace overalls','UNT',0,0,0 UNION ALL
Select '6203429000','Other','UNT',0,0,0 UNION ALL
Select '6203430000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6203491000','Of silk','UNT',0,0,0 UNION ALL
Select '6203499000','Other','UNT',0,0,0 UNION ALL


Select '6204110000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6204121000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204129000','Other','UNT',0,0,0 UNION ALL
Select '6204130000','Of synthetic fibres','UNT',0,0,0 UNION ALL


Select '6204191100','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204191900','Other','UNT',0,0,0 UNION ALL
Select '6204199000','Other','UNT',0,0,0 UNION ALL

Select '6204210000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6204221000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204229000','Other','UNT',0,0,0 UNION ALL
Select '6204230000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6204291000','Of silk','UNT',0,0,0 UNION ALL
Select '6204299000','Other','UNT',0,0,0 UNION ALL

Select '6204310000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6204321000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204329000','Other','UNT',0,0,0 UNION ALL
Select '6204330000','Of synthetic fibres','UNT',0,0,0 UNION ALL


Select '6204391100','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204391900','Other','UNT',0,0,0 UNION ALL
Select '6204399000','Other','UNT',0,0,0 UNION ALL

Select '6204410000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6204421000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204429000','Other','UNT',0,0,0 UNION ALL
Select '6204430000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6204440000','Of artificial fibres','UNT',0,0,0 UNION ALL

Select '6204491000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204499000','Other','UNT',0,0,0 UNION ALL

Select '6204510000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6204521000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204529000','Other','UNT',0,0,0 UNION ALL
Select '6204530000','Of synthetic fibres','UNT',0,0,0 UNION ALL

Select '6204591000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6204599000','Other','UNT',0,0,0 UNION ALL

Select '6204610000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL
Select '6204620000','Of cotton','UNT',0,0,0 UNION ALL
Select '6204630000','Of synthetic fibres','UNT',0,0,0 UNION ALL
Select '6204690000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6205201000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6205202000','Barong Tagalog','UNT',0,0,0 UNION ALL
Select '6205209000','Other','UNT',0,0,0 UNION ALL

Select '6205301000','Barong Tagalog','UNT',0,0,0 UNION ALL
Select '6205309000','Other','UNT',0,0,0 UNION ALL

Select '6205901000','Of wool or fine animal hair ','UNT',0,0,0 UNION ALL

Select '6205909100','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6205909200','Barong Tagalog','UNT',0,0,0 UNION ALL
Select '6205909900','Other','UNT',0,0,0 UNION ALL


Select '6206101000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6206109000','Other','UNT',0,0,0 UNION ALL
Select '6206200000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6206301000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6206309000','Other','UNT',0,0,0 UNION ALL
Select '6206400000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6206900000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6207110000','Of cotton','UNT',0,0,0 UNION ALL
Select '6207190000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6207211000','Printed by traditional batik process','UNT',0,0,0 UNION ALL
Select '6207219000','Other','UNT',0,0,0 UNION ALL
Select '6207220000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6207291000','Of silk','UNT',0,0,0 UNION ALL
Select '6207299000','Other','UNT',0,0,0 UNION ALL

Select '6207910000','Of cotton ','KGM',0,0,0 UNION ALL

Select '6207991000','Of manmade fibres','KGM',0,0,0 UNION ALL
Select '6207999000','Other','KGM',0,0,0 UNION ALL


Select '6208110000','Of manmade fibres','UNT',0,0,0 UNION ALL
Select '6208190000','Of other textile materials','UNT',0,0,0 UNION ALL


Select '6208211000','Printed by traditional batik process','KGM',0,0,0 UNION ALL
Select '6208219000','Other','KGM',0,0,0 UNION ALL
Select '6208220000','Of manmade fibres','UNT',0,0,0 UNION ALL

Select '6208291000','Printed by traditional batik process','KGM',0,0,0 UNION ALL
Select '6208299000','Other','KGM',0,0,0 UNION ALL


Select '6208911000','Printed by traditional batik process','KGM',0,0,0 UNION ALL
Select '6208919000','Other','KGM',0,0,0 UNION ALL

Select '6208921000','Printed by traditional batik process','KGM',0,0,0 UNION ALL
Select '6208929000','Other','KGM',0,0,0 UNION ALL

Select '6208991000','Of wool or fine animal hair ','KGM',0,0,0 UNION ALL
Select '6208999000','Other ','KGM',0,0,0 UNION ALL


Select '6209203000','Tshirts, shirts, pyjamas and similar articles ','KGM',0,0,0 UNION ALL
Select '6209204000','Suits, pants and similar articles','KGM',0,0,0 UNION ALL
Select '6209209000','Other','KGM',0,0,0 UNION ALL

Select '6209301000','Suits, pants and similar articles  ','KGM',0,0,0 UNION ALL
Select '6209303000','Tshirts, shirts, pyjamas and similar articles ','KGM',0,0,0 UNION ALL
Select '6209304000','Clothing accessories ','KGM',0,0,0 UNION ALL
Select '6209309000','Other','KGM',0,0,0 UNION ALL
Select '6209900000','Of other textile materials','KGM',0,0,0 UNION ALL



Select '6210101100','Garments used for protection from chemical substances, radiation or fire','KGM',0,0,0 UNION ALL
Select '6210101900','Other','KGM',0,0,0 UNION ALL
Select '6210109000','Other ','KGM',0,0,0 UNION ALL

Select '6210202000','Garments used for protection from fire','UNT',0,0,0 UNION ALL
Select '6210203000','Garments used for protection from chemical substances or radiation','UNT',0,0,0 UNION ALL
Select '6210204000','Other protective work garments','UNT',0,0,0 UNION ALL
Select '6210209000','Other','UNT',0,0,0 UNION ALL

Select '6210302000','Garments used for protection from fire','UNT',0,0,0 UNION ALL
Select '6210303000','Garments used for protection from chemical substances or radiation','UNT',0,0,0 UNION ALL
Select '6210304000','Other protective work garments','UNT',0,0,0 UNION ALL
Select '6210309000','Other ','UNT',0,0,0 UNION ALL

Select '6210401000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6210402000','Garments used for protection from chemical substances or radiation','KGM',0,0,0 UNION ALL
Select '6210409000','Other','KGM',0,0,0 UNION ALL

Select '6210501000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6210502000','Garments used for protection from chemical substances or radiation','KGM',0,0,0 UNION ALL
Select '6210509000','Other','KGM',0,0,0 UNION ALL


Select '6211110000','Men''s or boys''','UNT',0,0,0 UNION ALL
Select '6211120000','Women''s or girls''','UNT',0,0,0 UNION ALL
Select '6211200000','Ski suits','UNT',0,0,0 UNION ALL


Select '6211321000','Garments for fencing or wrestling ','KGM',0,0,0 UNION ALL
Select '6211322000','Pilgrimage robes (Ehram)','KGM',0,0,0 UNION ALL
Select '6211329000','Other','KGM',0,0,0 UNION ALL

Select '6211331000','Garments for fencing or wrestling ','KGM',0,0,0 UNION ALL
Select '6211332000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6211333000','Garments used for protection from chemical substances or radiation','KGM',0,0,0 UNION ALL
Select '6211339000','Other','KGM',0,0,0 UNION ALL

Select '6211391000','Garments for fencing or wrestling ','KGM',0,0,0 UNION ALL
Select '6211392000','Garments used for protection from fire','KGM',0,0,0 UNION ALL
Select '6211393000','Garments used for protection from chemical substances or radiation','KGM',0,0,0 UNION ALL
Select '6211399000','Other','KGM',0,0,0 UNION ALL


Select '6211421000','Garments for fencing or wrestling','KGM',0,0,0 UNION ALL
Select '6211422000','Prayer cloaks','KGM',0,0,0 UNION ALL
Select '6211429000','Other','KGM',0,0,0 UNION ALL

Select '6211431000','Surgical gowns','KGM',5,0,0 UNION ALL
Select '6211432000','Prayer cloaks','KGM',0,0,0 UNION ALL
Select '6211433000','Antiexplosive protective suits ','KGM',0,0,0 UNION ALL
Select '6211434000','Garments for fencing or wrestling','KGM',0,0,0 UNION ALL
Select '6211435000','Garments used for protection from chemical substances, radiation or fire','KGM',0,0,0 UNION ALL
Select '6211436000','Flyers'' coveralls','KGM',0,0,0 UNION ALL
Select '6211439000','Other','KGM',0,0,0 UNION ALL

Select '6211491000','Garments for fencing or wrestling','KGM',0,0,0 UNION ALL
Select '6211492000','Garments used for protection from chemical substances, radiation or fire','KGM',0,0,0 UNION ALL

Select '6211493100','Of wool or of fine animal hair','KGM',0,0,0 UNION ALL
Select '6211493900','Other','KGM',0,0,0 UNION ALL
Select '6211494000','Other, of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6211499000','Other','KGM',0,0,0 UNION ALL



Select '6212101100','Mastectomy bra (post breast surgery bra)','KGM',0,0,0 UNION ALL
Select '6212101900','Other','KGM',0,0,0 UNION ALL

Select '6212109100','Mastectomy bra (post breast surgery bra)','KGM',0,0,0 UNION ALL
Select '6212109900','Other','KGM',0,0,0 UNION ALL

Select '6212201000','Of cotton','KGM',0,0,0 UNION ALL
Select '6212209000','Of other textile materials','KGM',0,0,0 UNION ALL

Select '6212301000','Of cotton','KGM',0,0,0 UNION ALL
Select '6212309000','Of other textile materials','KGM',0,0,0 UNION ALL


Select '6212901100','Compression garments of a kind used for the treatment of scar tissue and skin grafts','KGM',0,0,0 UNION ALL
Select '6212901200','Athletic supporters  ','KGM',0,0,0 UNION ALL
Select '6212901900','Other','KGM',0,0,0 UNION ALL

Select '6212909100','Compression garments of a kind used for the treatment of scar tissue and skin grafts','KGM',0,0,0 UNION ALL
Select '6212909200','Athletic supporters  ','KGM',0,0,0 UNION ALL
Select '6212909900','Other','KGM',0,0,0 UNION ALL


Select '6213201000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6213209000','Other','KGM',0,0,0 UNION ALL


Select '6213901100','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6213901900','Other','KGM',0,0,0 UNION ALL

Select '6213909100','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6213909900','Other','KGM',0,0,0 UNION ALL


Select '6214101000','Printed by the traditional batik process','UNT',0,0,0 UNION ALL
Select '6214109000','Other','UNT',0,0,0 UNION ALL
Select '6214200000','Of wool or fine animal hair','UNT',0,0,0 UNION ALL

Select '6214301000','Printed by the traditional batik process','UNT',0,0,0 UNION ALL
Select '6214309000','Other','UNT',0,0,0 UNION ALL

Select '6214401000','Printed by the traditional batik process','UNT',0,0,0 UNION ALL
Select '6214409000','Other','UNT',0,0,0 UNION ALL

Select '6214901000','Printed by the traditional batik process','UNT',0,0,0 UNION ALL
Select '6214909000','Other','UNT',0,0,0 UNION ALL


Select '6215101000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6215109000','Other','KGM',0,0,0 UNION ALL

Select '6215201000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6215209000','Other','KGM',0,0,0 UNION ALL

Select '6215901000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6215909000','Other','KGM',0,0,0 UNION ALL

Select '6216001000','Protective work gloves, mittens and mitts','KGM',0,0,0 UNION ALL

Select '6216009100','Of wool or fine animal hair','KGM',0,0,0 UNION ALL
Select '6216009200','Of cotton','KGM',0,0,0 UNION ALL
Select '6216009900','Other','KGM',0,0,0 UNION ALL


Select '6217101000','Judo belts','KGM',0,0,0 UNION ALL
Select '6217109000','Other','KGM',0,0,0 UNION ALL
Select '6217900000','Parts','KGM',20,0,0 UNION ALL


Select '6301100000','Electric blankets','UNT',0,0,0 UNION ALL
Select '6301200000','Blankets (other than electric blankets) and travelling rugs, of wool or of fine animal hair','KGM',0,0,0 UNION ALL

Select '6301301000','Printed by traditional batik process','KGM',0,0,0 UNION ALL
Select '6301309000','Other','KGM',0,0,0 UNION ALL

Select '6301401000','Of nonwoven fabrics','KGM',0,0,0 UNION ALL
Select '6301409000','Other','KGM',0,0,0 UNION ALL

Select '6301901000','Of nonwoven fabrics','KGM',0,0,0 UNION ALL
Select '6301909000','Other','KGM',0,0,0 UNION ALL

Select '6302100000','Bed linen, knitted or crocheted','KGM',0,0,0 UNION ALL

Select '6302210000','Of cotton','KGM',0,0,0 UNION ALL

Select '6302221000','Of nonwoven fabrics','KGM',0,0,0 UNION ALL
Select '6302229000','Other','KGM',0,0,0 UNION ALL
Select '6302290000','Of other textile materials','KGM',0,0,0 UNION ALL

Select '6302310000','Of cotton','KGM',0,0,0 UNION ALL

Select '6302321000','Of nonwoven fabrics','KGM',0,0,0 UNION ALL
Select '6302329000','Other','KGM',0,0,0 UNION ALL
Select '6302390000','Of other textile materials','KGM',0,0,0 UNION ALL
Select '6302400000',' Table linen, knitted or crocheted','KGM',0,0,0 UNION ALL


Select '6302511000','Printed by the traditional batik process','KGM',0,0,0 UNION ALL
Select '6302519000','Other','KGM',0,0,0 UNION ALL
Select '6302530000','Of manmade fibres','KGM',0,0,0 UNION ALL

Select '6302591000','Of flax','KGM',0,0,0 UNION ALL
Select '6302599000','Other','KGM',0,0,0 UNION ALL
Select '6302600000','Toilet linen and kitchen linen, of terry towelling or similar terry   fabrics, of cotton','KGM',0,0,0 UNION ALL

Select '6302910000','Of cotton','KGM',0,0,0 UNION ALL
Select '6302930000','Of manmade fibres','KGM',0,0,0 UNION ALL

Select '6302991000','Of flax','KGM',0,0,0 UNION ALL
Select '6302999000','Other','KGM',0,0,0 UNION ALL


Select '6303120000','Of synthetic fibres','KGM',0,0,0 UNION ALL

Select '6303191000','Of cotton','KGM',0,0,0 UNION ALL
Select '6303199000','Other','KGM',0,0,0 UNION ALL

Select '6303910000','Of cotton','KGM',0,0,0 UNION ALL
Select '6303920000','Of synthetic fibres','KGM',0,0,0 UNION ALL
Select '6303990000','Of other textile materials','KGM',0,0,0 UNION ALL


Select '6304110000','Knitted or crocheted','KGM',0,0,0 UNION ALL

Select '6304191000','Of cotton','KGM',0,0,0 UNION ALL
Select '6304192000','Other, nonwoven','KGM',0,0,0 UNION ALL
Select '6304199000','Other','KGM',0,0,0 UNION ALL
Select '6304200000','Bed nets specified in Subheading Note 1 to this Chapter','KGM',0,0,0 UNION ALL


Select '6304911000','Mosquito nets','KGM',0,0,0 UNION ALL
Select '6304919000','Other','KGM',0,0,0 UNION ALL
Select '6304920000','Not knitted or crocheted, of cotton','KGM',0,0,0 UNION ALL
Select '6304930000','Not knitted or crocheted, of synthetic fibres','KGM',0,0,0 UNION ALL
Select '6304990000','Not knitted or crocheted, of other textile materials','KGM',0,0,0 UNION ALL



Select '6305101100','Of jute','KGM',0,0,0 UNION ALL
Select '6305101900','Other','KGM',20,0,0 UNION ALL

Select '6305102100','Of jute','KGM',0,0,0 UNION ALL
Select '6305102900','Other','KGM',20,0,0 UNION ALL
Select '6305200000','Of cotton','KGM',20,0,0 UNION ALL


Select '6305321000','Nonwoven','KGM',20,0,0 UNION ALL
Select '6305322000','Knitted or crocheted','KGM',20,0,0 UNION ALL
Select '6305329000','Other','KGM',20,0,0 UNION ALL

Select '6305331000','Knitted or crocheted','KGM',20,0,0 UNION ALL
Select '6305332000','Of woven fabrics of strip or the like','KGM',20,0,0 UNION ALL
Select '6305339000','Other','KGM',20,0,0 UNION ALL

Select '6305391000','Nonwoven','KGM',20,0,0 UNION ALL
Select '6305392000','Knitted or crocheted','KGM',20,0,0 UNION ALL
Select '6305399000','Other','KGM',20,0,0 UNION ALL

Select '6305901000','Of hemp of heading 53.05','KGM',20,0,0 UNION ALL
Select '6305902000','Of coconut (coir) of heading 53.05  ','KGM',20,0,0 UNION ALL
Select '6305909000','Other','KGM',20,0,0 UNION ALL


Select '6306120000','Of synthetic fibres','KGM',20,0,0 UNION ALL

Select '6306191000','Of vegetable textile fibres of heading 53.05','KGM',20,0,0 UNION ALL
Select '6306192000','Of cotton','KGM',20,0,0 UNION ALL
Select '6306199000','Other','KGM',20,0,0 UNION ALL

Select '6306220000','Of synthetic fibres','KGM',20,0,0 UNION ALL

Select '6306291000','Of cotton','KGM',20,0,0 UNION ALL
Select '6306299000','Other','KGM',20,0,0 UNION ALL
Select '6306300000','Sails','KGM',20,0,0 UNION ALL

Select '6306401000','Of cotton','KGM',20,0,0 UNION ALL
Select '6306409000','Other','KGM',20,0,0 UNION ALL

Select '6306901000','Of nonwoven','KGM',20,0,0 UNION ALL

Select '6306909100','Of cotton','KGM',20,0,0 UNION ALL
Select '6306909900','Other','KGM',20,0,0 UNION ALL


Select '6307101000','Nonwoven other than felt ','KGM',20,0,0 UNION ALL
Select '6307102000','Of felt','KGM',20,0,0 UNION ALL
Select '6307109000','Other ','KGM',20,0,0 UNION ALL
Select '6307200000','�Lifejackets and lifebelts ','KGM',20,0,0 UNION ALL

Select '6307903000','Umbrella covers in precut triangular form','KGM',20,0,0 UNION ALL
Select '6307904000','Surgical masks','KGM',20,0,0 UNION ALL

Select '6307906100','Suitable for industrial use','KGM',20,0,0 UNION ALL
Select '6307906900','Other','KGM',20,0,0 UNION ALL
Select '6307907000','Fans and handscreens','KGM',0,0,0 UNION ALL
Select '6307908000','Laces for shoes, boots, corsets and the like','KGM',0,0,0 UNION ALL
Select '6307909000','Other','KGM',20,0,0 UNION ALL


Select '6308000000','Sets consisting of woven fabric and yarn, whether or not with accessories, for making up into rugs, tapestries, embroidered table cloths or serviettes, or similar textile articles, put up in packings for retail sale.','KGM',0,0,0 UNION ALL


Select '6309000000','Worn clothing and other worn articles.','KGM',0,0,0 UNION ALL


Select '6310101000','Used or new rags','KGM',5,0,0 UNION ALL
Select '6310109000','Other','KGM',5,0,0 UNION ALL

Select '6310901000','Used or new rags','KGM',5,0,0 UNION ALL
Select '6310909000','Other','KGM',5,0,0 UNION ALL

Select '6401100000','Footwear incorporating a protective metal toecap','PAI',0,0,0 UNION ALL

Select '6401920000','Covering the ankle but not covering the knee','PAI',0,0,0 UNION ALL

Select '6401991000','Covering the knee','PAI',0,0,0 UNION ALL
Select '6401999000','Other','PAI',0,0,0 UNION ALL


Select '6402120000','Skiboots, crosscountry ski footwear and snowboard boots','PAI',0,0,0 UNION ALL

Select '6402191000','Wrestling footwear','PAI',0,0,0 UNION ALL
Select '6402199000','Other','PAI',0,0,0 UNION ALL
Select '6402200000','Footwear with  upper  straps or   thongs assembled to the sole by means of plugs','PAI',0,0,0 UNION ALL


Select '6402911000','Diving boots','PAI',0,0,0 UNION ALL

Select '6402919100','Incorporating a protective metal toecap','PAI',0,0,0 UNION ALL
Select '6402919900','Other','PAI',0,0,0 UNION ALL

Select '6402991000','Incorporating a protective metal toecap','PAI',0,0,0 UNION ALL
Select '6402999000','Other','PAI',0,0,0 UNION ALL


Select '6403120000','Skiboots, crosscountry ski footwear and snowboard boots','PAI',0,0,0 UNION ALL

Select '6403191000','Fitted with spikes, cleats or the like','PAI',0,0,0 UNION ALL
Select '6403192000','Riding boots ; bowling shoes','PAI',0,0,0 UNION ALL
Select '6403193000','Footwear for wrestling, weightlifing or gymnastics','PAI',0,0,0 UNION ALL
Select '6403199000','Other ','PAI',0,0,0 UNION ALL
Select '6403200000','Footwear with outer soles of leather, and uppers which consist of leather straps across the instep and around the big toe','PAI',0,0,0 UNION ALL
Select '6403400000','Other footwear, incorporating a protective metal toe cap','PAI',0,0,0 UNION ALL

Select '6403510000','Covering the ankle','PAI',0,0,0 UNION ALL

Select '6403591000','Bowling shoes','PAI',0,0,0 UNION ALL
Select '6403599000','Other ','PAI',0,0,0 UNION ALL


Select '6403911000','Footwear made on a base or platform of wood, not having an inner sole or protective metal toe cap','PAI',0,0,0 UNION ALL
Select '6403912000','Riding boots','PAI',0,0,0 UNION ALL
Select '6403919000','Other','PAI',0,0,0 UNION ALL

Select '6403991000','Footwear made on a base or platform of wood, not having an inner sole or protective metal toe cap','PAI',0,0,0 UNION ALL
Select '6403992000','Bowling shoes','PAI',0,0,0 UNION ALL
Select '6403999000','Other','PAI',0,0,0 UNION ALL



Select '6404111000','Fitted with spikes, cleats or the like','PAI',0,0,0 UNION ALL
Select '6404112000','Footwear for wrestling, weightlifing or gymnastics','PAI',0,0,0 UNION ALL
Select '6404119000','Other','PAI',0,0,0 UNION ALL
Select '6404190000','Other','PAI',0,0,0 UNION ALL
Select '6404200000','Footwear with outer soles of leather or composition leather','PAI',0,0,0 UNION ALL

Select '6405100000','With uppers of leather or composition leather','PAI',0,0,0 UNION ALL
Select '6405200000','With uppers of textile materials','PAI',0,0,0 UNION ALL
Select '6405900000','Other','PAI',0,0,0 UNION ALL


Select '6406101000','Metal toecaps','KGM',0,0,0 UNION ALL
Select '6406109000','Other','KGM',0,0,0 UNION ALL

Select '6406201000','Of rubber','KGM',25,0,0 UNION ALL
Select '6406202000','Of plastics','KGM',25,0,0 UNION ALL

Select '6406901000','Of wood','KGM',5,0,0 UNION ALL

Select '6406902100','Of iron or steel','KGM',0,0,0 UNION ALL
Select '6406902200','Of copper ','KGM',2,0,0 UNION ALL
Select '6406902300','Of aluminium ','KGM',20,0,0 UNION ALL
Select '6406902900','Other','KGM',25,0,0 UNION ALL

Select '64069031','Insoles:','',0,0,0 UNION ALL
Select '6406903110','Of plastics','KGM',10,0,0 UNION ALL
Select '6406903120','Of rubber','KGM',25,0,0 UNION ALL
Select '6406903200','Complete soles','KGM',30,0,0 UNION ALL
Select '6406903300','Other, of plastics','KGM',10,0,0 UNION ALL
Select '6406903900','Other','KGM',25,0,0 UNION ALL

Select '6406909100','Gaiters, legging and similar articles and parts thereof','KGM',5,0,0 UNION ALL
Select '6406909900','Other','KGM',25,0,0 UNION ALL

Select '6501000000','Hatforms, hat bodies and hoods of felt, neither blocked to shape nor with made brims; plateaux and manchons (including slit manchons), of felt.','KGM',0,0,0 UNION ALL

Select '6502000000','Hatshapes, plaited or made by assembling strips of any material, neither blocked to shape, nor with made brims, nor lined, nor trimmed.','KGM',0,0,0 UNION ALL

Select '6504000000','Hats and other headgear, plaited or made by assembling strips of any material, whether or not lined or trimmed.','KGM',0,0,0 UNION ALL

Select '6505001000','Headgear of a kind used for religious purposes','KGM',0,0,0 UNION ALL
Select '6505002000','Hairnets','KGM',0,0,0 UNION ALL
Select '6505009000','Other','KGM',0,0,0 UNION ALL


Select '6506101000','Helmets for motorcyclists','UNT',0,0,0 UNION ALL
Select '6506102000','Industrial safety helmets and firefighters"helmets, excluding steel helmets','UNT',0,0,0 UNION ALL
Select '6506103000','Steel helmets','UNT',0,0,0 UNION ALL
Select '6506104000','Waterpolo headgear','UNT',0,0,0 UNION ALL
Select '6506109000','Other','UNT',0,0,0 UNION ALL

Select '6506910000','Of rubber or of plastics','KGM',0,0,0 UNION ALL

Select '6506991000','Of furskin','KGM',0,0,0 UNION ALL
Select '6506999000','Other','KGM',0,0,0 UNION ALL

Select '6507000000','Headbands, linings, covers, hat foundations, hat frames, peaks and chinstraps, for headgear.','KGM',0,0,0 UNION ALL

Select '6601100000','Garden or similar umbrellas','UNT',20,0,0 UNION ALL

Select '6601910000','Having a telescopic shaft','UNT',20,0,0 UNION ALL
Select '6601990000','Other','UNT',20,0,0 UNION ALL

Select '6602000000','Walkingsticks, seatsticks, whips, ridingcrops and the like.','UNT',0,0,0 UNION ALL

Select '6603200000','Umbrella frames, including frames mounted on shafts (sticks)','KGM',20,0,0 UNION ALL

Select '6603901000','For articles of heading 66.01','KGM',20,0,0 UNION ALL
Select '6603902000','For articles of heading 66.02','KGM',5,0,0 UNION ALL

Select '6701000000','Skins and other parts of birds with their feathers or down, feathers, parts of feathers, down and articles thereof (other than goods of heading 05.05 and worked quills and scapes).','KGM',5,0,0 UNION ALL

Select '6702100000','Of plastics','KGM',20,0,0 UNION ALL

Select '6702901000','Of paper','KGM',20,0,0 UNION ALL
Select '6702902000','Of textile materials','KGM',10,0,0 UNION ALL
Select '6702909000','Other','KGM',5,0,0 UNION ALL

Select '6703000000','Human hair, dressed, thinned, bleached or otherwise worked; wool or other animal hair or other textile materials, prepared for use in making wigs or the like.','KGM',0,0,0 UNION ALL


Select '6704110000','Complete wigs','KGM',10,0,0 UNION ALL
Select '6704190000','Other','KGM',10,0,0 UNION ALL
Select '6704200000','Of human hair','KGM',10,0,0 UNION ALL
Select '6704900000','Of other materials','KGM',10,0,0 UNION ALL

Select '6801000000','Setts, curbstones and flagstones, of natural stone (except slate).','KGM',0,0,0 UNION ALL

Select '68021000','Tiles, cubes and similar articles, whether or not rectangular (including square), the largest surface area of which is capable of being enclosed in a square the side of which is less than 7 cm; artificially coloured granules, chippings and powder:','',0,0,0 UNION ALL
Select '6802100010','Of marble or slate','KGM',30,0,0 UNION ALL
Select '6802100090','Other','KGM',30,0,0 UNION ALL

Select '6802210000','Marble, travertine and alabaster','KGM',30,0,0 UNION ALL
Select '6802230000','Granite','KGM',30,0,0 UNION ALL

Select '6802291000','Other calcareous stone','KGM',30,0,0 UNION ALL
Select '6802299000','Other','KGM',30,0,0 UNION ALL


Select '6802911000','Marble','KGM',30,0,0 UNION ALL
Select '6802919000','Other','KGM',25,0,0 UNION ALL
Select '6802920000','Other calcareous stone','KGM',30,0,0 UNION ALL

Select '6802931000','Polished slabs','KGM',30,0,0 UNION ALL
Select '6802939000','Other','KGM',30,0,0 UNION ALL
Select '6802990000','Other stone','KGM',30,0,0 UNION ALL

Select '6803000000','Worked slate and articles of slate or of agglomerated slate.','KGM',25,0,0 UNION ALL

Select '6804100000','Millstones and grindstones for milling, grinding or pulping','KGM',0,0,0 UNION ALL

Select '6804210000','Of agglomerated synthetic or natural diamond','KGM',0,0,0 UNION ALL
Select '6804220000','Of other agglomerated abrasives or of ceramics','KGM',0,0,0 UNION ALL
Select '6804230000','Of natural stone','KGM',0,0,0 UNION ALL
Select '6804300000','Hand sharpening or polishing stones','KGM',5,0,0 UNION ALL

Select '6805100000','On a base of woven textile fabric only','KGM',25,0,0 UNION ALL
Select '6805200000','On a base of paper or paperboard only','KGM',25,0,0 UNION ALL
Select '6805300000','On a base of other materials','KGM',25,0,0 UNION ALL

Select '6806100000','Slag wool, rock wool and similar  mineral wools (including intermixtures thereof), in bulk, sheets or rolls','KGM',25,0,0 UNION ALL
Select '6806200000','Exfoliated vermiculite,  expanded  clays,  foamed  slag and similar expanded mineral materials (including intermixtures  thereof)','KGM',25,0,0 UNION ALL
Select '6806900000','Other','KGM',25,0,0 UNION ALL

Select '6807100000','In rolls','KGM',5,5,0 UNION ALL

Select '6807901000','Tiles','KGM',25,0,0 UNION ALL
Select '6807909000','Other','KGM',5,0,0 UNION ALL

Select '6808002000','Roofing tiles','KGM',30,5,0 UNION ALL
Select '68080030','Panels, boards, blocks and similar articles:','',0,0,0 UNION ALL
Select '6808003010','Of straw or of shavings, chips, particles, sawdust or other waste, of wood, agglomerated with cement, plaster or other mineral binders','KGM',30,0,0 UNION ALL
Select '6808003020','Of vegetable fibre','KGM',10,0,0 UNION ALL
Select '6808009000','Other','KGM',10,0,0 UNION ALL


Select '6809110000','Faced or reinforced with paper or paperboard only','KGM',30,0,0 UNION ALL

Select '6809191000','Tiles','KGM',25,0,0 UNION ALL
Select '6809199000','Other','KGM',25,0,0 UNION ALL

Select '6809901000','Dental moulds of plaster','KGM',5,0,0 UNION ALL
Select '6809909000','Other','KGM',5,0,0 UNION ALL


Select '6810110000','Building blocks and bricks','KGM',20,0,0 UNION ALL

Select '6810191000','Tiles','KGM',30,0,0 UNION ALL
Select '6810199000','Other','KGM',10,0,0 UNION ALL

Select '6810910000','Prefabricated structural components for building or civil engineering','KGM',20,0,0 UNION ALL
Select '6810990000','Other','KGM',20,0,0 UNION ALL


Select '6811401000','Corrugated sheets','KGM',20,0,0 UNION ALL

Select '6811402100','Floor or wall tiles containing plastics','KGM',25,0,0 UNION ALL
Select '6811402200','For roofing, facing or partitioning','KGM',10,0,0 UNION ALL
Select '6811402900','Other','KGM',20,0,0 UNION ALL
Select '6811403000','Tubes or pipes','KGM',10,0,0 UNION ALL
Select '6811404000','Tube or pipe fittings','KGM',20,0,0 UNION ALL
Select '6811405000','Other articles, of a kind used for building construction','KGM',20,0,0 UNION ALL
Select '6811409000','Other','KGM',10,0,0 UNION ALL

Select '6811810000','Corrugated sheets','KGM',20,0,0 UNION ALL

Select '6811821000','Floor or wall tiles containing plastics','KGM',25,0,0 UNION ALL
Select '6811822000','For roofing, facing or partitioning','KGM',10,0,0 UNION ALL
Select '6811829000','Other','KGM',20,0,0 UNION ALL

Select '6811891000','Tubes or pipes','KGM',10,0,0 UNION ALL
Select '6811892000','Tube or pipe fittings','KGM',20,0,0 UNION ALL
Select '6811893000','Other articles, of a kind used for building construction','KGM',20,0,0 UNION ALL
Select '6811899000','Other','KGM',10,0,0 UNION ALL


Select '6812802000','Clothing','KGM',10,0,0 UNION ALL
Select '6812803000','Paper, millboard and felt','KGM',0,0,0 UNION ALL
Select '6812804000','Floor or wall tiles','KGM',0,0,0 UNION ALL
Select '6812805000','Clothing accessories, footwear and headgear; fabricated crocidolite fibres; mixtures with a basis of crocidolite or with a basis of crocidolite and magnesium carbonate; yarn and thread; cords and strings, whether or not plaited; woven or knitted fabrics','KGM',0,0,0 UNION ALL
Select '6812809000','Other','KGM',0,0,0 UNION ALL


Select '6812911000','Clothing','KGM',10,0,0 UNION ALL
Select '6812919000','Other','KGM',0,0,0 UNION ALL
Select '6812920000','Paper, millboard and felt','KGM',0,0,0 UNION ALL
Select '6812930000','Compressed asbestos fibre jointing, in sheets or rolls','KGM',0,0,0 UNION ALL


Select '6812991100','Mixtures with a basis of asbestos or with a basis of asbestos and magnesium carbonate of a kind used for the manufacture of goods of heading 68.13','KGM',30,0,0 UNION ALL
Select '6812991900','Other','KGM',0,0,0 UNION ALL
Select '6812992000',' Floor or wall tiles','KGM',0,0,0 UNION ALL
Select '6812999000','Other','KGM',0,0,0 UNION ALL


Select '6813201000','Brake linings and pads','KGM',30,0,0 UNION ALL
Select '6813209000','Other','KGM',30,0,0 UNION ALL

Select '6813810000','Brake linings and pads','KGM',30,0,0 UNION ALL
Select '6813890000','Other','KGM',30,0,0 UNION ALL

Select '6814100000','Plates, sheets and strips of agglomerated or reconstituted mica,whether or not on a support','KGM',0,0,0 UNION ALL
Select '6814900000','Other','KGM',0,0,0 UNION ALL


Select '6815101000','Yarn or thread','KGM',0,0,0 UNION ALL
Select '6815102000','Bricks, paving slabs, floor tiles and similar articles','KGM',0,0,0 UNION ALL

Select '6815109100','Carbon fibres','KGM',0,0,0 UNION ALL
Select '6815109900','Other','KGM',0,0,0 UNION ALL
Select '6815200000','Articles of peat','KGM',0,0,0 UNION ALL

Select '6815910000','Containing magnesite, dolomite or chromite','KGM',0,0,0 UNION ALL
Select '6815990000','Other','KGM',0,0,0 UNION ALL


Select '6901000000','Bricks, blocks, tiles and other ceramic goods of siliceous fossil meals (for example, kieselguhr, tripolite or diatomite) or of similar siliceous earths.','KGM',25,0,0 UNION ALL

Select '6902100000','Containing by weight, singly or together, more than 50% of the elements Mg, Ca or Cr, expressed as MgO, CaO or Cr2O3','KGM',20,0,0 UNION ALL
Select '6902200000','Containing by weight more than 50% of alumina (AI2O3), of silica (SiO2) or of a mixture or compound of these products','KGM',20,0,0 UNION ALL
Select '6902900000','Other','KGM',20,0,0 UNION ALL

Select '6903100000','Containing by weight more than 50% of graphite or other carbon or of a mixture of these products','KGM',0,0,0 UNION ALL
Select '6903200000','Containing by weight more than 50 % of alumina (Al2O3) or of a mixture or compound of alumina and of silica (SiO2)','KGM',0,0,0 UNION ALL
Select '6903900000','Other','KGM',0,0,0 UNION ALL


Select '6904100000','Building bricks','UNT',30,0,0 UNION ALL
Select '6904900000','Other','KGM',30,0,0 UNION ALL

Select '6905100000','Roofing tiles','KGM',30,0,0 UNION ALL
Select '6905900000','Other','KGM',10,0,0 UNION ALL

Select '6906000000','Ceramic pipes, conduits, guttering and pipe fittings.','KGM',30,0,0 UNION ALL



Select '6907211000','Lining tiles of a kind used for grinding mills, unglazed','MTK',10,0,0 UNION ALL

Select '6907212100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907212200','Other, unglazed','MTK',5,0,0 UNION ALL
Select '6907212300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907212400','Other, glazed','MTK',5,0,0 UNION ALL

Select '6907219100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907219200','Other, unglazed','MTK',10,0,0 UNION ALL
Select '6907219300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907219400','Other, glazed','MTK',10,0,0 UNION ALL


Select '6907221100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907221200','Other, unglazed','MTK',5,0,0 UNION ALL
Select '6907221300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907221400','Other, glazed','MTK',5,0,0 UNION ALL

Select '6907229100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907229200','Other, unglazed','MTK',10,0,0 UNION ALL
Select '6907229300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907229400','Other, glazed','MTK',10,0,0 UNION ALL


Select '6907231100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907231200','Other, unglazed','MTK',5,0,0 UNION ALL
Select '6907231300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907231400','Other, glazed','MTK',5,0,0 UNION ALL

Select '6907239100','Paving, hearth or wall tiles, unglazed','MTK',50,0,0 UNION ALL
Select '6907239200','Other, unglazed','MTK',10,0,0 UNION ALL
Select '6907239300','Paving, hearth or wall tiles, glazed','MTK',60,0,0 UNION ALL
Select '6907239400','Other, glazed','MTK',10,0,0 UNION ALL


Select '6907301100','Having a largest surface area of which is capable of being enclosed in a square the side of which is less than 7 cm','MTK',5,0,0 UNION ALL
Select '6907301900','Other','MTK',10,0,0 UNION ALL

Select '6907309100','Having a largest surface area of which is capable of being enclosed in a square the side of which is less than 7 cm','MTK',5,0,0 UNION ALL
Select '6907309900','Other
','MTK',10,0,0 UNION ALL

Select '6907401000','Of a kind used for lining grinding mills, unglazed','MTK',10,0,0 UNION ALL

Select '6907402100','Unglazed','MTK',5,0,0 UNION ALL
Select '6907402200','Glazed','MTK',5,0,0 UNION ALL

Select '6907409100','Unglazed','MTK',10,0,0 UNION ALL
Select '6907409200','Glazed','MTK',10,0,0 UNION ALL


Select '6909110000','Of porcelain or china','KGM',5,0,0 UNION ALL
Select '6909120000','Articles  having  a hardness  equivalent  to 9 or more on the Mohs scale','KGM',5,0,0 UNION ALL
Select '6909190000','Other','KGM',5,0,0 UNION ALL
Select '6909900000','Other','KGM',5,0,0 UNION ALL

Select '6910100000','Of porcelain or china','UNT',25,0,0 UNION ALL
Select '6910900000','Other','UNT',25,0,0 UNION ALL

Select '6911100000','Tableware and kitchenware','KGM',30,0,0 UNION ALL
Select '6911900000','Other','KGM',30,0,0 UNION ALL

Select '6912000000','Ceramic tableware, kitchenware, other household articles and toilet articles, other than of porcelain or china.','KGM',30,0,0 UNION ALL


Select '6913101000','Ornamental cigarette boxes and ashtrays','KGM',25,0,0 UNION ALL
Select '6913109000','Other','KGM',20,0,0 UNION ALL

Select '6913901000','Ornamental cigarette boxes and ashtrays','KGM',25,0,0 UNION ALL
Select '6913909000','Other','KGM',20,0,0 UNION ALL

Select '6914100000','Of porcelain or china','KGM',5,0,0 UNION ALL
Select '6914900000','Other','KGM',5,0,0 UNION ALL

Select '7001000000','Cullet and other waste and scrap of glass; glass in the mass.','KGM',0,0,0 UNION ALL

Select '7002100000','Balls','KGM',0,0,0 UNION ALL
Select '7002200000','Rods','KGM',0,0,0 UNION ALL


Select '7002311000','Of a kind used to manufacture vacuum tubes','KGM',0,0,0 UNION ALL
Select '7002319000','Other ','KGM',0,0,0 UNION ALL

Select '7002321000','Of a kind used to manufacture vacuum tubes','KGM',0,0,0 UNION ALL
Select '7002323000','Borosilicate glass tubes of a kind used to manufacture vial/ampoules','KGM',0,0,0 UNION ALL
Select '7002324000','Other, of clear neutral borosilicate glass, with a diameter of 3 mm or more but not more than 22 mm','KGM',0,0,0 UNION ALL
Select '7002329000','Other ','KGM',0,0,0 UNION ALL

Select '7002391000','Of a kind used to manufacture vacuum tubes','KGM',0,0,0 UNION ALL
Select '7002392000','Other, of clear neutral borosilicate glass, with a diameter of 3 mm or more but not more than 22 mm','KGM',0,0,0 UNION ALL
Select '7002399000','Other ','KGM',0,0,0 UNION ALL



Select '7003121000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '7003122000','Other, in squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7003129000','Other ','MTK',30,0,0 UNION ALL

Select '7003191000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '70031990','Other:','',0,0,0 UNION ALL
Select '7003199010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7003199090','Other','MTK',30,0,0 UNION ALL

Select '7003201000','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7003209000','Other','MTK',30,0,0 UNION ALL

Select '7003301000','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7003309000','Other','MTK',30,0,0 UNION ALL


Select '7004201000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '70042090','Other:','',0,0,0 UNION ALL
Select '7004209010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7004209090','Other','MTK',30,0,0 UNION ALL

Select '7004901000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '70049090','Other:','',0,0,0 UNION ALL
Select '7004909010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7004909090','Other','MTK',30,0,0 UNION ALL


Select '7005101000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '7005109000','Other','MTK',30,0,0 UNION ALL


Select '7005211000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '70052190','Other:','',0,0,0 UNION ALL
Select '7005219010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7005219090','Other','MTK',30,0,0 UNION ALL

Select '7005291000','Optical glass, not optically worked','MTK',0,0,0 UNION ALL
Select '70052990','Other:','',0,0,0 UNION ALL
Select '7005299010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7005299090','Other','MTK',30,0,0 UNION ALL
Select '70053000','Wired glass:','',0,0,0 UNION ALL
Select '7005300010','In squares or rectangular shape including 1 or more corners cut','MTK',30,0,0 UNION ALL
Select '7005300090','Other','MTK',30,0,0 UNION ALL


Select '7006001000','Optical glass, not optically worked','KGM',0,0,0 UNION ALL
Select '7006009000','Other','KGM',30,0,0 UNION ALL



Select '7007111000','Suitable for vehicles of Chapter 87','KGM',30,0,0 UNION ALL
Select '7007112000','Suitable for aircraft or spacecraft of Chapter 88','KGM',30,0,0 UNION ALL
Select '7007113000','Suitable for railway or tramway locomotives or rolling stock of  Chapter 86','KGM',30,0,0 UNION ALL
Select '7007114000','Suitable for vessels of Chapter 89','KGM',30,0,0 UNION ALL

Select '7007191000','Suitable for machinery of heading 84.29 or 84.30','MTK',30,0,0 UNION ALL
Select '7007199000','Other','MTK',30,0,0 UNION ALL


Select '7007211000','Suitable for vehicles of Chapter 87','KGM',30,0,0 UNION ALL
Select '7007212000','Suitable for aircraft or spacecraft of Chapter 88','KGM',30,0,0 UNION ALL
Select '7007213000','Suitable for railway or tramway locomotives or rolling stock of Chapter 86','KGM',30,0,0 UNION ALL
Select '7007214000','Suitable for vessels of Chapter 89','KGM',30,0,0 UNION ALL

Select '7007291000','Suitable for machinery of heading 84.29 or 84.30','MTK',30,0,0 UNION ALL
Select '7007299000','Other','MTK',30,0,0 UNION ALL

Select '7008000000','Multiplewalled insulating units of glass.','KGM',30,0,0 UNION ALL

Select '7009100000','Rearview mirrors for vehicles','KGM',5,0,0 UNION ALL

Select '7009910000','Unframed','KGM',30,0,0 UNION ALL
Select '7009920000','Framed','KGM',30,0,0 UNION ALL

Select '7010100000','Ampoules','KGM',0,0,0 UNION ALL
Select '7010200000','Stoppers, lids and other closures','KGM',0,0,0 UNION ALL

Select '7010901000','Carboys or demijohns','KGM',30,0,0 UNION ALL
Select '7010904000','Bottles and phials, of a kind used for antibiotics, serums and other injectable liquids; bottles of a kind used for intravenous fluids','KGM',30,0,0 UNION ALL

Select '7010909100','Of a capacity exceeding 1l','KGM',30,0,0 UNION ALL
Select '7010909900','Other','KGM',30,0,0 UNION ALL


Select '7011101000','Stems','KGM',0,0,0 UNION ALL
Select '7011109000','Other','KGM',0,0,0 UNION ALL
Select '7011200000','For cathoderay tubes','KGM',0,0,0 UNION ALL
Select '7011900000','Other','KGM',0,0,0 UNION ALL

Select '7013100000','Of glassceramics','KGM',30,0,0 UNION ALL

Select '7013220000','Of lead crystal','KGM',30,0,0 UNION ALL
Select '7013280000','Other','KGM',30,0,0 UNION ALL

Select '7013330000','Of lead crystal','KGM',30,0,0 UNION ALL
Select '7013370000','Other','KGM',30,0,0 UNION ALL

Select '7013410000','Of lead crystal','KGM',30,0,0 UNION ALL
Select '7013420000','Of  glass   having   a   linear   coefficient   of   expansion   not  exceeding 5x106 per Kelvin within a temperature range  of 0oC to 300oC','KGM',30,0,0 UNION ALL
Select '7013490000','Other','KGM',30,0,0 UNION ALL

Select '7013910000','Of lead crystal','KGM',30,0,0 UNION ALL
Select '7013990000','Other','KGM',30,0,0 UNION ALL

Select '7014001000','Of a kind suitable for use in motor vehicles','KGM',0,0,0 UNION ALL
Select '7014009000','Other ','KGM',0,0,0 UNION ALL

Select '7015100000','Glasses for corrective spectacles','KGM',0,0,0 UNION ALL

Select '7015901000','Clock or watch glasses','KGM',20,0,0 UNION ALL
Select '7015909000','Other','KGM',20,0,0 UNION ALL

Select '7016100000','Glass cubes and other glass smallwares, whether or not on a backing, for mosaics or similar decorative purposes','KGM',0,0,0 UNION ALL
Select '7016900000','Other','KGM',20,0,0 UNION ALL


Select '7017101000','Quartz reactor tubes and holders designed for insertion into  diffusion and oxidation furnaces for production of semiconductor wafers ','KGM',0,0,0 UNION ALL
Select '7017109000','Other','KGM',0,0,0 UNION ALL
Select '7017200000','Of other  glass  having  a  linear  coefficient  of  expansion  not exceeding 5 x 106 per Kelvin within a temperature range of 0oC  to 300oC','KGM',0,0,0 UNION ALL
Select '7017900000','Other','KGM',0,0,0 UNION ALL


Select '7018101000','Glass beads','KGM',0,0,0 UNION ALL
Select '7018109000','Other','KGM',0,0,0 UNION ALL
Select '7018200000','Glass microspheres not exceeding 1 mm in diameter','KGM',20,0,0 UNION ALL

Select '7018901000','Glass eyes','KGM',20,0,0 UNION ALL
Select '7018909000','Other','KGM',20,0,0 UNION ALL


Select '7019110000','Chopped strands, of a length of not more than 50 mm','KGM',25,0,0 UNION ALL
Select '7019120000','Rovings','KGM',20,0,0 UNION ALL

Select '7019191000','Yarn','KGM',0,0,0 UNION ALL
Select '7019199000','Other ','KGM',20,0,0 UNION ALL

Select '7019310000','Mats','KGM',20,0,0 UNION ALL
Select '7019320000','Thin sheets (voiles)','KGM',0,0,0 UNION ALL

Select '7019391000','Asphalt or coaltar impregnated glassfibre outerwrap of a kind used for pipelines','KGM',20,0,0 UNION ALL
Select '7019399000','Other','KGM',20,0,0 UNION ALL
Select '7019400000','Woven fabrics of rovings','KGM',20,0,0 UNION ALL

Select '7019510000','Of a width not exceeding 30 cm','KGM',20,0,0 UNION ALL
Select '7019520000','Of a width exceeding 30 cm, plain weave, weighing less  than  250g/m2, of filaments measuring per single yarn not more than  136 tex','KGM',20,0,0 UNION ALL
Select '7019590000','Other','KGM',20,0,0 UNION ALL

Select '7019901000','Glass fibres (including glass wool) ','KGM',20,0,0 UNION ALL
Select '7019902000','Blinds','KGM',20,0,0 UNION ALL
Select '7019909000','Other','KGM',5,0,0 UNION ALL



Select '7020001100','Of a kind used for the manufacture of acrylic goods','KGM',30,0,0 UNION ALL
Select '7020001900','Other ','KGM',30,0,0 UNION ALL
Select '7020002000','Quartz reactor  tubes  and  holders  designed  for  insertion  into diffusion and oxidation furnaces for production of semiconductor wafers ','KGM',30,0,0 UNION ALL
Select '7020003000','Glass inners for vacuum flasks or other vacuum vessels','KGM',0,0,0 UNION ALL
Select '7020004000','Evacuated tubes for solar energy collectors','KGM',30,0,0 UNION ALL
Select '7020009000','Other','KGM',30,0,0 UNION ALL


Select '7101100000','Natural pearls','KGM',0,0,0 UNION ALL

Select '7101210000','Unworked','KGM',0,0,0 UNION ALL
Select '7101220000','Worked','KGM',0,0,0 UNION ALL

Select '7102100000','Unsorted','CTM',0,0,0 UNION ALL

Select '7102210000','Unworked or simply sawn, cleaved or bruted','CTM',0,0,0 UNION ALL
Select '7102290000','Other','CTM',0,0,0 UNION ALL

Select '7102310000','Unworked or simply sawn, cleaved or bruted','CTM',0,0,0 UNION ALL
Select '7102390000','Other','CTM',0,0,0 UNION ALL


Select '7103101000','Rubies','KGM',0,0,0 UNION ALL
Select '7103102000','Jade (nephrite and jadeite)','KGM',0,0,0 UNION ALL
Select '7103109000','Other','KGM',0,0,0 UNION ALL


Select '7103911000','Rubies','CTM',0,0,0 UNION ALL
Select '7103919000','Other','CTM',0,0,0 UNION ALL
Select '7103990000','Other','CTM',0,0,0 UNION ALL


Select '7104101000','Unworked','KGM',0,0,0 UNION ALL
Select '7104102000','Worked ','KGM',0,0,0 UNION ALL
Select '7104200000','Other, unworked or simply sawn or roughly shaped','KGM',0,0,0 UNION ALL
Select '7104900000','Other','KGM',0,0,0 UNION ALL

Select '7105100000','Of diamonds ','CTM',0,0,0 UNION ALL
Select '7105900000','Other','KGM',0,0,0 UNION ALL


Select '7106100000','Powder','KGM',0,5,0 UNION ALL

Select '7106910000','Unwrought','KGM',0,5,0 UNION ALL
Select '7106920000','Semimanufactured','KGM',0,5,0 UNION ALL

Select '7107000000','Base metals clad with silver, not further worked than semimanufactured.','KGM',0,5,0 UNION ALL


Select '7108110000','Powder','KGM',0,0,0 UNION ALL

Select '7108121000','In lumps, ingots or cast bars','KGM',0,0,0 UNION ALL
Select '7108129000','Other','KGM',0,0,0 UNION ALL
Select '7108130000','Other semimanufactured forms','KGM',0,0,0 UNION ALL
Select '7108200000','Monetary','KGM',0,0,0 UNION ALL

Select '7109000000','Base metals or silver, clad with gold, not further worked than semimanufactured.','KGM',0,0,0 UNION ALL



Select '7110111000','In lumps, ingots, cast bars or in powder form','KGM',0,5,0 UNION ALL
Select '7110119000','Other','KGM',0,5,0 UNION ALL
Select '7110190000','Other','KGM',0,5,0 UNION ALL


Select '7110211000','In lumps, ingots, cast bars or in powder form','KGM',0,5,0 UNION ALL
Select '7110219000','Other','KGM',0,5,0 UNION ALL
Select '7110290000','Other','KGM',0,5,0 UNION ALL


Select '7110311000','In lumps, ingots, cast bars or in powder form','KGM',0,5,0 UNION ALL
Select '7110319000','Other','KGM',0,5,0 UNION ALL
Select '7110390000','Other','KGM',0,5,0 UNION ALL


Select '7110411000','In lumps, ingots, cast bars or in powder form','KGM',0,5,0 UNION ALL
Select '7110419000','Other','KGM',0,5,0 UNION ALL
Select '7110490000','Other','KGM',0,5,0 UNION ALL

Select '7111001000','Silver or gold, clad with platinum','KGM',0,5,0 UNION ALL
Select '7111009000','Other','KGM',0,5,0 UNION ALL

Select '7112300000','Ash containing precious metal or precious metal compounds','KGM',0,0,0 UNION ALL

Select '7112910000','Of gold, including metal clad with gold but excluding sweepings containing other precious metals','KGM',0,0,0 UNION ALL
Select '7112920000','Of platinum, including metal clad with platinum but excluding   sweepings containing other precious metals','KGM',0,0,0 UNION ALL

Select '7112991000','Of silver, including metal clad with silver but excluding  sweepings containing other precious metals','KGM',0,0,0 UNION ALL
Select '7112999000','Other','KGM',0,0,0 UNION ALL




Select '7113111000','Parts','KGM',0,0,0 UNION ALL
Select '7113119000','Other','KGM',0,0,0 UNION ALL

Select '7113191000','Parts','KGM',0,0,0 UNION ALL
Select '7113199000','Other','KGM',0,0,0 UNION ALL

Select '7113201000','Parts','KGM',0,0,0 UNION ALL
Select '7113209000','Other','KGM',0,0,0 UNION ALL


Select '7114110000','Of silver, whether or not plated or clad with other precious metal','KGM',0,0,0 UNION ALL
Select '7114190000','Of other  precious metal, whether  or not plated  or  clad  with precious metal','KGM',0,0,0 UNION ALL
Select '7114200000','Of base metal clad with precious metal','KGM',0,0,0 UNION ALL

Select '7115100000','Catalysts in the form of wire cloth or grill, of platinum','KGM',5,0,0 UNION ALL

Select '7115901000','Of gold or silver','KGM',10,0,0 UNION ALL
Select '7115902000','Of metal clad with gold or silver','KGM',10,0,0 UNION ALL
Select '7115909000','Other','KGM',10,0,0 UNION ALL

Select '7116100000','Of natural or cultured pearls','KGM',0,0,0 UNION ALL
Select '7116200000','Of precious or semiprecious stones (natural, synthetic or  reconstructed)','KGM',0,0,0 UNION ALL



Select '7117111000','Parts','KGM',0,0,0 UNION ALL
Select '7117119000','Other','KGM',0,0,0 UNION ALL

Select '7117191000','Bangles','KGM',0,0,0 UNION ALL
Select '7117192000','Other imitation jewellery','KGM',0,0,0 UNION ALL
Select '7117199000','Parts','KGM',0,0,0 UNION ALL


Select '7117901100','Wholly of plastics or glass','KGM',0,0,0 UNION ALL
Select '7117901200','Wholly of wood, worked tortoise shell, ivory, bone, horn, coral, mother of pearl and other animal carving material, worked vegetable carving material or worked mineral carving material','KGM',0,0,0 UNION ALL
Select '7117901300',' Wholly of porcelain or china','KGM',0,0,0 UNION ALL
Select '7117901900','Other','KGM',0,0,0 UNION ALL

Select '7117902100','Wholly of plastics or glass','KGM',0,0,0 UNION ALL
Select '7117902200','Wholly of wood, worked tortoise shell, ivory, bone, horn, coral, mother of pearl and other animal carving material, worked vegetable carving material or worked mineral carving material','KGM',0,0,0 UNION ALL
Select '7117902300',' Wholly of porcelain or china','KGM',0,0,0 UNION ALL
Select '7117902900','Other','KGM',0,0,0 UNION ALL

Select '7117909100','Wholly of plastics or glass','KGM',0,0,0 UNION ALL
Select '7117909200','Wholly of wood, worked tortoise shell, ivory, bone, horn, coral, mother of pearl and other animal carving material, worked vegetable carving material or worked mineral carving material','KGM',0,0,0 UNION ALL
Select '7117909300',' Wholly of porcelain or china','KGM',0,0,0 UNION ALL
Select '7117909900','Other','KGM',0,0,0 UNION ALL


Select '7118101000','Silver coin','KGM',5,0,0 UNION ALL
Select '7118109000','Other','KGM',5,0,0 UNION ALL

Select '7118901000','Gold coin, whether or not legal tender','KGM',0,0,0 UNION ALL
Select '7118902000','Silver coin, being legal tender','KGM',5,0,0 UNION ALL
Select '7118909000','Other','KGM',5,0,0 UNION ALL


Select '7201100000','Nonalloy pig iron containing by weight 0.5% or less of phosphorus','KGM',0,0,0 UNION ALL
Select '7201200000','Nonalloy pig iron containing by weight more than 0.5% of phosphorus','KGM',0,0,0 UNION ALL
Select '7201500000','Alloy pig iron; spiegeleisen','KGM',0,0,0 UNION ALL


Select '7202110000','Containing by weight more than 2% of carbon','KGM',0,0,0 UNION ALL
Select '7202190000','Other','KGM',0,0,0 UNION ALL

Select '7202210000','Containing by weight more than 55% of silicon','KGM',0,0,0 UNION ALL
Select '7202290000','Other','KGM',0,0,0 UNION ALL
Select '7202300000','Ferrosilicomanganese','KGM',0,0,0 UNION ALL

Select '7202410000','Containing by weight more than 4% of carbon','KGM',0,0,0 UNION ALL
Select '7202490000','Other','KGM',0,0,0 UNION ALL
Select '7202500000','Ferrosilicochromium','KGM',0,0,0 UNION ALL
Select '7202600000','Ferronickel','KGM',0,0,0 UNION ALL
Select '7202700000','Ferromolybdenum','KGM',0,0,0 UNION ALL
Select '7202800000','Ferrotungsten and ferrosilicotungsten','KGM',0,0,0 UNION ALL

Select '7202910000','Ferrotitanium and ferrosilicotitanium','KGM',0,0,0 UNION ALL
Select '7202920000','Ferrovanadium','KGM',0,0,0 UNION ALL
Select '7202930000','Ferroniobium','KGM',0,0,0 UNION ALL
Select '7202990000','Other','KGM',0,0,0 UNION ALL

Select '7203100000','Ferrous products obtained by direct reduction of iron ore','KGM',0,0,0 UNION ALL
Select '7203900000','Other','KGM',0,0,0 UNION ALL

Select '7204100000','Waste and scrap of cast iron','KGM',0,10,0 UNION ALL

Select '7204210000','Of stainless steel','KGM',0,10,0 UNION ALL
Select '7204290000','Other','KGM',0,10,0 UNION ALL
Select '7204300000','Waste and scrap of tinned iron or steel','KGM',0,10,0 UNION ALL

Select '7204410000','Turnings,  shavings,  chips,  milling waste,  sawdust,  filings,  trimmings and stampings, whether or not in bundles','KGM',0,10,0 UNION ALL
Select '7204490000','Other','KGM',0,10,0 UNION ALL
Select '7204500000','Remelting scrap ingots','KGM',0.05,10,0 UNION ALL

Select '7205100000','Granules','KGM',0,0,0 UNION ALL

Select '7205210000','Of alloy steel','KGM',0,0,0 UNION ALL
Select '7205290000','Other','KGM',0,0,0 UNION ALL



Select '7206101000','Containing by weight more than 0.6% of carbon','KGM',5,0,0 UNION ALL
Select '7206109000','Other','KGM',0,0,0 UNION ALL
Select '7206900000','Other','KGM',0,0,0 UNION ALL


Select '7207110000','Of  rectangular  (including square)  crosssection, the  width measuring less than twice the thickness','KGM',0,0,0 UNION ALL

Select '7207121000','Slabs','KGM',0,0,0 UNION ALL
Select '7207129000','Other','KGM',0,0,0 UNION ALL
Select '7207190000','Other','KGM',0,0,0 UNION ALL


Select '7207201000','Slabs','KGM',0,0,0 UNION ALL

Select '7207202100','Blocks roughly shaped by forging; sheet bars','KGM',0,0,0 UNION ALL
Select '7207202900','Other','KGM',0,0,0 UNION ALL

Select '7207209100','Slabs','KGM',5,0,0 UNION ALL

Select '7207209200','Blocks roughly shaped by forging; sheet bars','KGM',5,0,0 UNION ALL
Select '7207209900','Other','KGM',5,0,0 UNION ALL

Select '7208100000','In coils, not further worked than hotrolled, with patterns in relief','KGM',15,0,0 UNION ALL

Select '7208250000','Of a thickness of 4.75 mm or more','KGM',15,0,0 UNION ALL
Select '7208260000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',15,0,0 UNION ALL


Select '7208271100','Containing by weight 0.6% or more of carbon','KGM',15,0,0 UNION ALL
Select '7208271900','Other','KGM',15,0,0 UNION ALL
Select '7208279100','Containing by weight 0.6% or more of carbon','KGM',15,0,0 UNION ALL
Select '7208279900','Other','KGM',15,0,0 UNION ALL

Select '7208360000','Of a thickness exceeding 10 mm','KGM',15,0,0 UNION ALL
Select '7208370000','Of a thickness of 4.75 mm or more but not exceeding 10 mm','KGM',15,0,0 UNION ALL
Select '7208380000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',15,0,0 UNION ALL

Select '7208391000','Containing by weight less than 0.6% of carbon and of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '7208399000','Other','KGM',15,0,0 UNION ALL
Select '7208400000','Not in coils, not further worked than hotrolled, with patterns in relief','KGM',15,0,0 UNION ALL

Select '7208510000','Of a thickness exceeding 10 mm','KGM',15,0,0 UNION ALL
Select '7208520000','Of a thickness of 4.75 mm or more but not exceeding 10 mm','KGM',15,0,0 UNION ALL
Select '7208530000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',15,0,0 UNION ALL

Select '7208541000','Containing by weight less than 0.6% of carbon and of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '7208549000','Other','KGM',15,0,0 UNION ALL

Select '7208901000','Corrugated','KGM',15,0,0 UNION ALL
Select '7208902000','Other, containing by weight less than 0.6% of carbon and of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '7208909000','Other','KGM',15,0,0 UNION ALL


Select '7209150000','Of a thickness of 3 mm or more','KGM',15,0,0 UNION ALL

Select '7209161000','Of a width not exceeding 1,250 mm','KGM',15,0,0 UNION ALL
Select '7209169000','Other','KGM',15,0,0 UNION ALL

Select '7209171000','Of a width not exceeding 1,250 mm','KGM',15,0,0 UNION ALL
Select '7209179000','Other','KGM',15,0,0 UNION ALL

Select '7209181000','Tinmill blackplate','KGM',15,0,0 UNION ALL

Select '7209189100','Containing by weight less than 0.6% of carbon and of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '7209189900','Other','KGM',15,0,0 UNION ALL

Select '7209250000','Of a thickness of 3 mm or more','KGM',15,0,0 UNION ALL

Select '7209261000','Of a width not exceeding 1,250 mm','KGM',15,0,0 UNION ALL
Select '7209269000','Other','KGM',15,0,0 UNION ALL

Select '7209271000','Of a width not exceeding 1,250 mm','KGM',15,0,0 UNION ALL
Select '7209279000','Other','KGM',15,0,0 UNION ALL

Select '7209281000','Containing by weight less than 0.6% of carbon and of a  thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '72092890','Other:','',0,0,0 UNION ALL
Select '7209289020','Containing by weight less than 0.6% of carbon ','KGM',15,0,0 UNION ALL
Select '7209289090','Other','KGM',15,0,0 UNION ALL

Select '7209901000','Corrugated','KGM',15,0,0 UNION ALL
Select '7209909000','Other','KGM',15,0,0 UNION ALL



Select '7210111000','Containing by weight 0.6% or more of carbon','KGM',15,0,0 UNION ALL
Select '7210119000','Other','KGM',5,0,0 UNION ALL

Select '7210121000','Containing by weight 0.6% or more of carbon','KGM',15,0,0 UNION ALL
Select '7210129000',' Other','KGM',5,0,0 UNION ALL

Select '7210201000','Containing by weight less than 0.6% of carbon and of a thickness of 1.5 mm or less','KGM',15,0,0 UNION ALL
Select '7210209000','Other','KGM',15,0,0 UNION ALL


Select '7210301100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210301200','Of a thickness exceeding 1.2 mm but not exceeding 1.5 mm','KGM',15,0,0 UNION ALL
Select '7210301900','Other','KGM',15,0,0 UNION ALL

Select '7210309100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210309900','Other','KGM',15,0,0 UNION ALL



Select '7210411100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210411200','Of a thickness exceeding 1.2 mm but not exceeding 1.5 mm','KGM',15,0,0 UNION ALL
Select '7210411900','Other','KGM',15,0,0 UNION ALL

Select '7210419100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210419900','Other','KGM',15,0,0 UNION ALL


Select '7210491100','Coated with zinc by the ironzinc alloyed coating method, containing by weight less than 0.04% of carbon and of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210491200','Other, of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210491300','Of a thickness exceeding 1.2 mm but not exceeding 1.5 mm','KGM',15,0,0 UNION ALL
Select '7210491900','Other','KGM',15,0,0 UNION ALL

Select '7210499100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210499900','Other','KGM',15,0,0 UNION ALL
Select '7210500000','Plated or coated with chromium oxides or with chromium and chromium oxides','KGM',15,0,0 UNION ALL



Select '7210611100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210611200','Of a thickness exceeding 1.2 mm but not exceeding 1.5 mm','KGM',15,0,0 UNION ALL
Select '7210611900','Other','KGM',15,0,0 UNION ALL

Select '7210619100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210619200','Other, corrugated','KGM',15,0,0 UNION ALL
Select '7210619900','Other','KGM',15,0,0 UNION ALL


Select '7210691100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210691200','Of a thickness exceeding 1.2 mm but not exceeding 1.5 mm','KGM',15,0,0 UNION ALL
Select '7210691900','Other','KGM',15,0,0 UNION ALL

Select '7210699100','Of a thickness not exceeding 1.2 mm','KGM',15,0,0 UNION ALL
Select '7210699900','Other','KGM',15,0,0 UNION ALL


Select '7210701100','Painted','KGM',15,0,0 UNION ALL
Select '7210701900','Other','KGM',15,0,0 UNION ALL

Select '72107091','Painted:','',0,0,0 UNION ALL
Select '7210709110','Containing by weight less than 0.6% of carbon ','KGM',15,0,0 UNION ALL
Select '7210709190','Other','KGM',15,0,0 UNION ALL
Select '72107099','Other:','',0,0,0 UNION ALL
Select '7210709910','Containing by weight less than 0.6% of carbon ','KGM',15,0,0 UNION ALL
Select '7210709990','Other','KGM',15,0,0 UNION ALL

Select '7210901000','Containing by weight less than 0.6% of carbon and of a thickness of 1.5 mm or less','KGM',15,0,0 UNION ALL
Select '7210909000','Other','KGM',15,0,0 UNION ALL




Select '7211131100','Hoop and strip; universal plates ','KGM',15,0,0 UNION ALL
Select '7211131200','Corrugated','KGM',15,0,0 UNION ALL
Select '7211131900','Other','KGM',15,0,0 UNION ALL

Select '72111391','Hoop and strip; universal plates:','',0,0,0 UNION ALL
Select '7211139110','Hoop and strip, of a width exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7211139190','Other','KGM',15,0,0 UNION ALL
Select '7211139900','Other','KGM',15,0,0 UNION ALL


Select '7211141300','Hoop and strip; universal plates','KGM',15,0,0 UNION ALL
Select '7211141400','Corrugated','KGM',15,0,0 UNION ALL
Select '7211141500','Coils for rerolling','KGM',15,0,0 UNION ALL
Select '7211141900','Other','KGM',15,0,0 UNION ALL

Select '7211149100','Hoop and strip; universal plates ','KGM',15,0,0 UNION ALL
Select '7211149200','Corrugated','KGM',15,0,0 UNION ALL
Select '7211149300','Coils for rerolling','KGM',15,0,0 UNION ALL
Select '7211149900','Other','KGM',15,0,0 UNION ALL


Select '7211191300','Hoop and strip; universal plates','KGM',15,0,0 UNION ALL
Select '7211191400','Corrugated','KGM',15,0,0 UNION ALL
Select '7211191500','Coils for rerolling','KGM',15,0,0 UNION ALL
Select '72111919','Other:','',0,0,0 UNION ALL
Select '7211191910','Of a thickness of 0.17 mm or less   ','KGM',15,0,0 UNION ALL
Select '7211191990','Other','KGM',15,0,0 UNION ALL

Select '7211199100','Hoop and strip; universal plates','KGM',15,0,0 UNION ALL
Select '7211199200','Corrugated','KGM',15,0,0 UNION ALL
Select '7211199300','Coils for rerolling','KGM',15,0,0 UNION ALL
Select '7211199900','Other ','KGM',15,0,0 UNION ALL


Select '7211231000','Corrugated','KGM',15,0,0 UNION ALL
Select '72112320','Hoop and strip, of a width not exceeding 400 mm:','',0,0,0 UNION ALL
Select '7211232010','Of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7211232090','Other','KGM',15,0,0 UNION ALL
Select '7211233000','Other, of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '72112390','Other:','',0,0,0 UNION ALL
Select '7211239010','Hoop and strip, of a width exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7211239090','Other','KGM',15,0,0 UNION ALL

Select '7211291000','Corrugated','KGM',15,0,0 UNION ALL
Select '7211292000','Hoop and strip, of a width not exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7211293000','Other, of a thickness of 0.17 mm or less','KGM',15,0,0 UNION ALL
Select '7211299000','Other','KGM',15,0,0 UNION ALL


Select '7211901100','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7211901200','Hoop and strip, of a width exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7211901300','Corrugated','KGM',15,0,0 UNION ALL
Select '7211901400','Other, of a thickness of 0.17mm or less','KGM',15,0,0 UNION ALL
Select '7211901900','Other','KGM',15,0,0 UNION ALL

Select '7211909100','Of a thickness of 0.17mm or less','KGM',15,0,0 UNION ALL
Select '7211909900','Other','KGM',15,0,0 UNION ALL



Select '7212101100','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7212101300','Hoop and strip, of a width exceeding 25mm but not exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '72121019','Other:','',0,0,0 UNION ALL
Select '7212101910','Hoop and strip, of a width exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7212101990','Other','KGM',5,0,0 UNION ALL

Select '7212109200','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7212109300','Hoop and strip, of a width exceeding 25mm but not exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7212109900','Other','KGM',15,0,0 UNION ALL

Select '72122010','Hoop and strip, of a width not exceeding 400 mm:','',0,0,0 UNION ALL
Select '7212201010','Containing by weight less than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212201090','Other','KGM',15,0,0 UNION ALL
Select '7212202000','Other, containing by weight less than 0.6% of carbon and of a thickness of 1.5 mm or less','KGM',15,0,0 UNION ALL
Select '7212209000','Other','KGM',15,0,0 UNION ALL


Select '7212301100','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7212301200','Hoop and strip, of a width exceeding 25 mm and not exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7212301300','Other, of a thickness of 1.5 mm or less','KGM',15,0,0 UNION ALL
Select '7212301400','Other, coated with zinc  by the ironzinc alloy coating method, containing by weight less than 0.04% of carbon','KGM',15,0,0 UNION ALL
Select '7212301900','Other','KGM',15,0,0 UNION ALL
Select '7212309000','Other','KGM',15,0,0 UNION ALL


Select '72124011','Hoop and strip, of a width not exceeding 400 mm:','',0,0,0 UNION ALL
Select '7212401110','Of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7212401190','Other','KGM',15,0,0 UNION ALL
Select '7212401200','Other hoop and strip','KGM',15,0,0 UNION ALL
Select '7212401900','Other','KGM',15,0,0 UNION ALL

Select '72124091','Hoop and strip, of a width not exceeding 400 mm:','',0,0,0 UNION ALL
Select '7212409110','Of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '7212409190','Other','KGM',15,0,0 UNION ALL
Select '72124092','Other hoop and strip; universal plates:','',0,0,0 UNION ALL
Select '7212409210','Hoop and strip','KGM',15,0,0 UNION ALL
Select '7212409220','Universal plates','KGM',15,0,0 UNION ALL
Select '7212409900','Other','KGM',15,0,0 UNION ALL


Select '7212501300','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '72125014','Other hoop and strip; universal plates:','',0,0,0 UNION ALL
Select '7212501410','Hoop and strip, of a width exceeding 400 mm, containing by weight more than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212501490','Other','KGM',15,0,0 UNION ALL
Select '72125019','Other:','',0,0,0 UNION ALL
Select '7212501910','Containing by weight less than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212501990','Other','KGM',15,0,0 UNION ALL

Select '7212502300','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '72125024','Other hoop and strip; universal plates :','',0,0,0 UNION ALL
Select '7212502410','Hoop and strip, of a width exceeding 400 mm, containing by weight more than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212502490','Other','KGM',15,0,0 UNION ALL
Select '72125029','Other:','',0,0,0 UNION ALL
Select '7212502910','Containing by weight less than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212502990','Other','KGM',15,0,0 UNION ALL

Select '7212509300','Hoop and strip, of a width not exceeding 25 mm','KGM',15,0,0 UNION ALL
Select '72125094','Other hoop and strip ; universal plates: ','',0,0,0 UNION ALL
Select '7212509410','Hoop and strip, of a width exceeding 400 mm, containing by weight more than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212509490','Other','KGM',15,0,0 UNION ALL
Select '72125099','Other:','',0,0,0 UNION ALL
Select '7212509910','Containing by weight less than 0.6% of carbon','KGM',15,0,0 UNION ALL
Select '7212509990','Other','KGM',15,0,0 UNION ALL


Select '7212601100','Hoop and strip','KGM',15,0,0 UNION ALL
Select '7212601200','Other, of a thickness of 1.5 mm or less','KGM',15,0,0 UNION ALL
Select '7212601900','Other','KGM',15,0,0 UNION ALL

Select '72126091','Hoop and strip:','',0,0,0 UNION ALL
Select '7212609110','Hoop and strip,  of a width exceeding 400 mm','KGM',15,0,0 UNION ALL
Select '7212609190','Other','KGM',15,0,0 UNION ALL
Select '7212609900','Other','KGM',15,0,0 UNION ALL


Select '7213101000','Of circular crosssection measuring not exceeding 50mm in diameter','KGM',5,0,0 UNION ALL
Select '7213109000','Other','KGM',5,0,0 UNION ALL
Select '7213200000','Other, of freecutting steel','KGM',5,0,0 UNION ALL


Select '7213911000','Of a kind used for producing soldering sticks','KGM',5,0,0 UNION ALL
Select '7213912000','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7213919000','Other','KGM',5,0,0 UNION ALL

Select '7213991000','Of a kind used for producing soldering sticks','KGM',5,0,0 UNION ALL
Select '7213992000','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7213999000','Other','KGM',5,0,0 UNION ALL



Select '7214101100','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7214101900','Other','KGM',5,0,0 UNION ALL

Select '7214102100','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7214102900','Other','KGM',0,0,0 UNION ALL



Select '7214203100','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7214203900','Other','KGM',5,0,0 UNION ALL

Select '7214204100','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7214204900','Other','KGM',5,0,0 UNION ALL


Select '7214205100','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7214205900','Other','KGM',5,0,0 UNION ALL

Select '7214206100','Of a kind used for concrete reinforcement (rebars)','KGM',0,0,0 UNION ALL
Select '7214206900','Other','KGM',0,0,0 UNION ALL

Select '7214301000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7214309000','Other','KGM',5,0,0 UNION ALL



Select '7214911100','Containing by weight 0.38% or more of carbon and less than 1.15% of manganese ','KGM',5,0,0 UNION ALL
Select '7214911200','Containing by weight 0.17% or more but not more than 0.46% of carbon and 1,2% or more but less than 1.65% of manganese','KGM',5,0,0 UNION ALL
Select '7214911900','Other','KGM',5,0,0 UNION ALL

Select '7214912100','Containing by weight less than 1.15% of manganese','KGM',0,0,0 UNION ALL
Select '7214912900','Other','KGM',0,0,0 UNION ALL


Select '7214991100','Containing by weight less than 1.15% of manganese','KGM',0,0,0 UNION ALL
Select '7214991900','Other','KGM',0,0,0 UNION ALL

Select '7214999100','Containing by weight less than 0.38% of carbon, not more than 0.05% of phosphorus and not more than 0.05% of sulfur','KGM',5,0,0 UNION ALL
Select '7214999200','Containing by weight 0.38% or more of carbon and less than 1.15% of manganese','KGM',5,0,0 UNION ALL
Select '7214999300','Containing by weight 0.17% or more but less than 0.46% of carbon and  1.2% or more but less than 1.65% of manganese','KGM',5,0,0 UNION ALL
Select '7214999900','Other','KGM',5,0,0 UNION ALL


Select '7215101000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7215109000','Other','KGM',5,0,0 UNION ALL

Select '7215501000','Containing by weight 0.6% or more of carbon, other than of  circular crosssection','KGM',0,0,0 UNION ALL

Select '7215509100','Of a kind used for concrete reinforcement (rebars)','KGM',5,0,0 UNION ALL
Select '7215509900','Other','KGM',5,0,0 UNION ALL

Select '72159010','Of a kind used for concrete reinforcement (rebars):','',0,0,0 UNION ALL
Select '7215901010','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7215901090','Other','KGM',5,0,0 UNION ALL
Select '72159090','Other:','',0,0,0 UNION ALL
Select '7215909010','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7215909090','Other','KGM',5,0,0 UNION ALL

Select '7216100000','U, I or H sections, not further worked than hotrolled, hotdrawn or extruded, of a height of less than 80 mm','KGM',5,0,0 UNION ALL


Select '7216211000','Containing by weight less than 0.6% of carbon','KGM',5,0,0 UNION ALL
Select '7216219000','Other','KGM',5,0,0 UNION ALL
Select '7216220000','T sections','KGM',5,0,0 UNION ALL


Select '7216311000','Containing by weight 0.6% or more of carbon','KGM',5,0,0 UNION ALL
Select '7216319000','Other','KGM',5,0,0 UNION ALL

Select '7216321000','Of a thickness of 5mm or less','KGM',5,0,0 UNION ALL
Select '7216329000','Other','KGM',5,0,0 UNION ALL


Select '7216331100','Thickness of the flange not less than thickness of the web','KGM',5,0,0 UNION ALL
Select '7216331900','Other','KGM',5,0,0 UNION ALL
Select '7216339000','Other','KGM',5,0,0 UNION ALL

Select '7216401000','Containing by weight 0.6% or more of carbon','KGM',5,0,0 UNION ALL
Select '7216409000','Other','KGM',5,0,0 UNION ALL


Select '7216501100','Containing by weight 0.6% or more of carbon','KGM',5,0,0 UNION ALL
Select '72165019','Other:','',0,0,0 UNION ALL
Select '7216501910','Angles','KGM',5,0,0 UNION ALL
Select '7216501990','Other','KGM',5,0,0 UNION ALL

Select '7216509100','Containing by weight 0.6% or more of carbon','KGM',5,0,0 UNION ALL
Select '7216509900','Other','KGM',5,0,0 UNION ALL

Select '72166100','Obtained from flatrolled products:','',0,0,0 UNION ALL

Select '7216610014','Containing by weight less than 0.6% of carbon, of a height less than 80mm','KGM',5,0,0 UNION ALL
Select '7216610019','Other','KGM',5,0,0 UNION ALL
Select '7216610020','Shapes and sections','KGM',5,0,0 UNION ALL
Select '72166900','Other: ','',0,0,0 UNION ALL

Select '7216690014','Containing by weight less than 0.6% of carbon, of a height less than 80mm','KGM',5,0,0 UNION ALL
Select '7216690019','Other','KGM',5,0,0 UNION ALL
Select '7216690020','Shapes and sections','KGM',5,0,0 UNION ALL


Select '7216911000','Angles, other than slotted angles, containing by weight 0.6% or more of carbon','KGM',5,0,0 UNION ALL
Select '72169190','Other:','',0,0,0 UNION ALL
Select '7216919010','Angles, other than slotted angles, of a height less than 80mm','KGM',5,0,0 UNION ALL
Select '7216919090','Other','KGM',5,0,0 UNION ALL
Select '72169900','Other :','',0,0,0 UNION ALL
Select '7216990010','Angles, other than slotted angles,  containing by weight less than 0.6% of carbon, of a height less than 80mm','KGM',5,0,0 UNION ALL
Select '7216990090','Other','KGM',5,0,0 UNION ALL


Select '7217101000','Containing by weight less than 0.25% of carbon','KGM',5,0,0 UNION ALL

Select '7217102200','Bead wire; reed wire; prestressed concrete steel wire; freecutting steel wire','KGM',5,0,0 UNION ALL
Select '7217102900','Other ','KGM',5,0,0 UNION ALL

Select '7217103200','Spokes wire; bead wire; reed wire; freecutting steel wire','KGM',5,0,0 UNION ALL
Select '7217103300','Wire of a kind used for making strands for prestressing concrete','KGM',5,0,0 UNION ALL
Select '7217103900','Other ','KGM',5,0,0 UNION ALL

Select '7217201000','Containing by weight less than 0.25% carbon ','KGM',5,0,0 UNION ALL
Select '7217202000','Containing by weight 0.25% or more but less than 0.45% of  carbon','KGM',5,0,0 UNION ALL

Select '7217209100','Steel core wire of a kind used for steel reinforced aluminium conductors (ACSR) ','KGM',5,0,0 UNION ALL
Select '7217209900','Other','KGM',5,0,0 UNION ALL


Select '7217301100','Plated or coated with tin','KGM',5,0,0 UNION ALL
Select '7217301900','Other','KGM',5,0,0 UNION ALL

Select '7217302100','Plated or coated with tin','KGM',5,0,0 UNION ALL
Select '7217302900','Other','KGM',5,0,0 UNION ALL

Select '7217303300','Brass coated steel wire of a kind used in the manufacture of pneumatic rubber tyres','KGM',5,0,0 UNION ALL
Select '7217303400','Other copper alloy coated steel wire of a kind used in the manufacture of pneumatic rubber tyres','KGM',5,0,0 UNION ALL
Select '7217303500','Other, plated or coated with tin','KGM',5,0,0 UNION ALL
Select '7217303900','Other','KGM',5,0,0 UNION ALL

Select '7217901000','Containing by weight less than 0.25% of carbon','KGM',5,0,0 UNION ALL
Select '7217909000','Other','KGM',5,0,0 UNION ALL


Select '7218100000','Ingots and other primary forms','KGM',0,0,0 UNION ALL

Select '7218910000','Of rectangular (other than square) crosssection','KGM',5,0,0 UNION ALL
Select '7218990000','Other','KGM',5,0,0 UNION ALL


Select '7219110000','Of a thickness exceeding 10 mm','KGM',0,0,0 UNION ALL
Select '7219120000','Of a thickness of 4.75 mm or more but not exceeding 10 mm','KGM',0,0,0 UNION ALL
Select '7219130000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',0,0,0 UNION ALL
Select '7219140000','Of a thickness of less than 3 mm','KGM',0,0,0 UNION ALL

Select '7219210000','Of a thickness exceeding 10 mm','KGM',0,0,0 UNION ALL
Select '7219220000','Of a thickness of 4.75 mm or more but not exceeding 10 mm','KGM',0,0,0 UNION ALL
Select '7219230000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',0,0,0 UNION ALL
Select '7219240000','Of a thickness of less than 3 mm','KGM',0,0,0 UNION ALL

Select '7219310000','Of a thickness of 4.75 mm or more','KGM',0,0,0 UNION ALL
Select '7219320000','Of a thickness of 3 mm or more but less than 4.75 mm','KGM',0,0,0 UNION ALL
Select '7219330000','Of a thickness exceeding 1 mm but less than 3 mm','KGM',0,0,0 UNION ALL
Select '7219340000','Of a thickness of 0.5 mm or more but not exceeding 1 mm','KGM',0,0,0 UNION ALL
Select '7219350000','Of a thickness of less than 0.5 mm','KGM',0,0,0 UNION ALL
Select '7219900000','Other','KGM',0,0,0 UNION ALL



Select '7220111000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7220119000','Other','KGM',0,0,0 UNION ALL

Select '7220121000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7220129000','Other','KGM',0,0,0 UNION ALL

Select '7220201000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7220209000','Other','KGM',0,0,0 UNION ALL

Select '7220901000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7220909000','Other','KGM',0,0,0 UNION ALL

Select '7221000000','Bars and rods, hotrolled, in irregularly wound coils, of stainless steel.','KGM',5,0,0 UNION ALL


Select '7222110000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7222190000','Other','KGM',0,0,0 UNION ALL

Select '7222201000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7222209000','Other','KGM',0,0,0 UNION ALL

Select '7222301000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7222309000','Other','KGM',0,0,0 UNION ALL

Select '7222401000','Not further worked than hotrolled, hotdrawn or extruded','KGM',5,0,0 UNION ALL
Select '7222409000','Other','KGM',5,0,0 UNION ALL

Select '7223001000','Of crosssection exceeding 13 mm','KGM',5,0,0 UNION ALL
Select '7223009000','Other','KGM',5,0,0 UNION ALL



Select '7224100000','Ingots and other primary forms','KGM',5,0,0 UNION ALL
Select '7224900000','Other','KGM',5,0,0 UNION ALL


Select '7225110000','Grainoriented','KGM',0,0,0 UNION ALL
Select '7225190000','Other','KGM',0,0,0 UNION ALL

Select '7225301000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225309000','Other','KGM',0,0,0 UNION ALL

Select '7225401000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225409000','Other','KGM',0,0,0 UNION ALL

Select '7225501000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225509000','Other','KGM',0,0,0 UNION ALL


Select '7225911000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225919000','Other','KGM',0,0,0 UNION ALL

Select '7225921000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225929000','Other','KGM',0,0,0 UNION ALL

Select '7225991000','Of high speed steel','KGM',0,0,0 UNION ALL
Select '7225999000','Other','KGM',0,0,0 UNION ALL



Select '7226111000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7226119000','Other','KGM',0,0,0 UNION ALL

Select '7226191000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7226199000','Other','KGM',0,0,0 UNION ALL

Select '7226201000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7226209000','Other','KGM',0,0,0 UNION ALL


Select '7226911000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7226919000','Other','KGM',0,0,0 UNION ALL

Select '7226921000','Hoop and strip, of a width not exceeding 400 mm','KGM',10,0,0 UNION ALL
Select '7226929000','Other','KGM',0,0,0 UNION ALL


Select '7226991100','Plated or coated with zinc','KGM',10,0,0 UNION ALL
Select '7226991900','Other','KGM',10,0,0 UNION ALL

Select '7226999100','Plated or coated with zinc','KGM',0,0,0 UNION ALL
Select '7226999900','Other','KGM',0,0,0 UNION ALL

Select '7227100000','Of high speed steel','KGM',5,0,0 UNION ALL
Select '7227200000','Of silicomanganese steel','KGM',5,0,0 UNION ALL
Select '7227900000','Other','KGM',5,0,0 UNION ALL


Select '7228101000','Of circular crosssection','KGM',0,0,0 UNION ALL
Select '7228109000','Other','KGM',0,0,0 UNION ALL


Select '7228201100','Not further worked than hotrolled, hotdrawn or extruded','KGM',5,0,0 UNION ALL
Select '7228201900','Other','KGM',5,0,0 UNION ALL

Select '7228209100','Not further worked than hotrolled, hotdrawn or extruded','KGM',0,0,0 UNION ALL
Select '7228209900','Other','KGM',0,0,0 UNION ALL

Select '7228301000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7228309000','Other','KGM',0,0,0 UNION ALL

Select '7228401000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7228409000','Other','KGM',0,0,0 UNION ALL

Select '7228501000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7228509000','Other','KGM',0,0,0 UNION ALL

Select '7228601000','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7228609000','Other','KGM',0,0,0 UNION ALL

Select '7228701000','Not further worked than hotrolled, hotdrawn or extruded','KGM',5,0,0 UNION ALL
Select '7228709000','Other','KGM',5,0,0 UNION ALL


Select '7228801100','Of circular crosssection','KGM',5,0,0 UNION ALL
Select '7228801900','Other','KGM',0,0,0 UNION ALL
Select '7228809000',' Other','KGM',5,0,0 UNION ALL

Select '7229200000','Of silicomanganese steel','KGM',5,0,0 UNION ALL

Select '7229902000','Of crosssection not exceeding 5.5 mm','KGM',5,0,0 UNION ALL
Select '7229903000','Other, of high speed steel','KGM',5,0,0 UNION ALL

Select '7229909100','Containing by weight 0.5% or more of chromium','KGM',5,0,0 UNION ALL
Select '7229909900','Other','KGM',5,0,0 UNION ALL

Select '7301100000','Sheet piling','KGM',5,0,0 UNION ALL
Select '7301200000','Angles, shapes and sections','KGM',5,0,0 UNION ALL

Select '7302100000','Rails','KGM',5,0,0 UNION ALL
Select '7302300000','Switchblades, crossing frogs, point rods and other crossing pieces','KGM',0,0,0 UNION ALL
Select '7302400000','Fishplates and sole plates','KGM',0,0,0 UNION ALL

Select '7302901000','Sleepers (crossties)','KGM',5,0,0 UNION ALL
Select '7302909000','Other','KGM',0,0,0 UNION ALL


Select '7303001100','Hubless tubes and pipes','KGM',15,0,0 UNION ALL
Select '7303001900','Other','KGM',15,0,0 UNION ALL

Select '7303009100','With an external diameter not exceeding 100 mm','KGM',15,0,0 UNION ALL
Select '7303009900','Other','KGM',15,0,0 UNION ALL


Select '7304110000','Of stainless steel','KGM',15,0,0 UNION ALL
Select '7304190000','Other','KGM',15,0,0 UNION ALL


Select '7304221000','With yield strength less than 80,000 psi and not threaded pipeend','KGM',15,0,0 UNION ALL
Select '7304229000','Other','KGM',15,0,0 UNION ALL

Select '7304231000','With yield strength less than 80,000 psi and not threaded pipeend','KGM',15,0,0 UNION ALL
Select '7304239000','Other','KGM',15,0,0 UNION ALL

Select '7304241000','Casing and tubing with yield strength less than 80,000 psi and not threaded end','KGM',15,0,0 UNION ALL
Select '7304249000','Other','KGM',15,0,0 UNION ALL

Select '7304291000','Casing and tubing with yield strength less than 80,000 psi and not threaded end','KGM',15,0,0 UNION ALL
Select '7304299000','Other','KGM',15,0,0 UNION ALL


Select '7304311000','Drillrod casing and tubing with pin and box threads','KGM',15,0,0 UNION ALL
Select '7304312000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7304314000','Other, having an external diameter of less than 140 mm and containing less than 0.45% by weight of carbon','KGM',15,0,0 UNION ALL
Select '7304319000','Other','KGM',15,0,0 UNION ALL

Select '7304392000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7304394000','Other, having an external diameter of less than 140 mm and containing less than 0.45% by weight of carbon','KGM',15,0,0 UNION ALL
Select '7304399000','Other','KGM',15,0,0 UNION ALL

Select '7304410000','Colddrawn or coldrolled (coldreduced)','KGM',15,0,0 UNION ALL
Select '7304490000','Other','KGM',15,0,0 UNION ALL


Select '7304511000','Drillrod casing and tubing with pin and box threads','KGM',15,0,0 UNION ALL
Select '7304512000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7304519000','Other','KGM',15,0,0 UNION ALL

Select '7304591000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7304599000','Other','KGM',15,0,0 UNION ALL

Select '7304901000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7304903000','Other, having an external diameter of less than 140 mm and containing less than 0.45% by weight of carbon','KGM',15,0,0 UNION ALL
Select '7304909000','Other','KGM',15,0,0 UNION ALL


Select '7305110000','Longitudinally submerged arc welded','KGM',15,0,0 UNION ALL

Select '7305121000','Electric resistance welded (ERW)','KGM',15,0,0 UNION ALL
Select '7305129000','Other','KGM',15,0,0 UNION ALL

Select '7305191000','Spiral or helical submerged arc welded','KGM',15,0,0 UNION ALL
Select '7305199000','Other','KGM',15,0,0 UNION ALL
Select '7305200000','Casing of a kind used in drilling for oil or gas','KGM',15,0,0 UNION ALL


Select '7305311000','Stainless steel pipes and tubes','KGM',15,0,0 UNION ALL
Select '7305319000','Other','KGM',15,0,0 UNION ALL

Select '7305391000','Highpressure pipe capable of withstanding a pressure of not less than 42,000 psi','KGM',15,0,0 UNION ALL
Select '7305399000','Other','KGM',15,0,0 UNION ALL
Select '7305900000','Other','KGM',15,0,0 UNION ALL



Select '7306111000','Longitudinally electric resistance welded (ERW)','KGM',15,0,0 UNION ALL
Select '7306112000','Spiral or helical submerged arc welded','KGM',15,0,0 UNION ALL
Select '7306119000','Other','KGM',15,0,0 UNION ALL

Select '7306191000','Longitudinally electric resistance welded (ERW)','KGM',15,0,0 UNION ALL
Select '7306192000','Spiral or helical submerged arc welded','KGM',15,0,0 UNION ALL
Select '7306199000','Other','KGM',15,0,0 UNION ALL

Select '7306210000','Welded, of stainless steel','KGM',15,0,0 UNION ALL
Select '7306290000','Other','KGM',15,0,0 UNION ALL


Select '7306301100','With external diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306301900','Other','KGM',15,0,0 UNION ALL

Select '7306302100','With external diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306302900','Other','KGM',15,0,0 UNION ALL
Select '7306303000','Pipe of a kind used to make sheath pipe (heater pipe) for heating elements of electric flat irons or rice cookers, with an external diameter not exceeding 12 mm','KGM',15,0,0 UNION ALL

Select '7306304100','With external diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306304900','Other','KGM',15,0,0 UNION ALL

Select '7306309100','With an internal diameter of 12.5 mm or more, an external diameter less than 140 mm and containing by weight less than 0.45 % of carbon','KGM',15,0,0 UNION ALL
Select '7306309200','With an internal diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306309900','Other','KGM',15,0,0 UNION ALL


Select '7306401100','With an external diameter not exceeding 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306401900','Other','KGM',15,0,0 UNION ALL
Select '7306402000','Stainless steel  pipes and tubes, with an external diameter exceeding 105 mm','KGM',15,0,0 UNION ALL
Select '7306403000','Pipes and tubes containing by weight at least 30% of nickel, with an external diameter not exceeding 10 mm','KGM',15,0,0 UNION ALL
Select '7306409000','Other','KGM',15,0,0 UNION ALL


Select '7306501100','With an external diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306501900','Other','KGM',15,0,0 UNION ALL

Select '7306509100','With an external diameter less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306509900','Other','KGM',15,0,0 UNION ALL


Select '7306611000','With external diagonal crosssection less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306619000','Other','KGM',15,0,0 UNION ALL

Select '7306691000','With external diagonal crosssection less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306699000','Other','KGM',15,0,0 UNION ALL


Select '7306901100','With external diagonal crosssection less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306901900','Other','KGM',15,0,0 UNION ALL

Select '7306909100','High pressure pipes capable of withstanding a pressure of not less than 42,000 psi, with an internal diameter less than 12.5mm','KGM',15,0,0 UNION ALL
Select '7306909200','Other high pressure pipes','KGM',15,0,0 UNION ALL
Select '7306909300','Other, with external diagonal crosssection less than 12.5 mm','KGM',15,0,0 UNION ALL
Select '7306909900','Other','KGM',15,0,0 UNION ALL



Select '7307111000','Hubless tube or pipe fittings','KGM',15,0,0 UNION ALL
Select '7307119000','Other','KGM',15,0,0 UNION ALL
Select '7307190000','Other','KGM',15,0,0 UNION ALL


Select '7307211000','Having an internal diameter of less than 15 cm','KGM',10,0,0 UNION ALL
Select '7307219000','Other','KGM',5,0,0 UNION ALL

Select '7307221000','Having an internal diameter of less than 15 cm','KGM',5,0,0 UNION ALL
Select '7307229000','Other','KGM',5,0,0 UNION ALL

Select '7307231000','Having an internal diameter of less than 15 cm','KGM',10,0,0 UNION ALL
Select '7307239000','Other','KGM',5,0,0 UNION ALL

Select '7307291000','Having an internal diameter of less than 15 cm','KGM',15,0,0 UNION ALL
Select '7307299000','Other','KGM',5,0,0 UNION ALL


Select '7307911000','Having an internal diameter of less than 15 cm','KGM',15,0,0 UNION ALL
Select '7307919000','Other','KGM',5,0,0 UNION ALL

Select '7307921000','Having an internal diameter of less than 15 cm','KGM',15,0,0 UNION ALL
Select '7307929000','Other','KGM',5,0,0 UNION ALL

Select '7307931000','Having an internal diameter of less than 15 cm','KGM',15,0,0 UNION ALL
Select '7307939000','Other','KGM',5,0,0 UNION ALL

Select '7307991000','Having an internal diameter of less than 15 cm','KGM',15,0,0 UNION ALL
Select '7307999000','Other','KGM',5,0,0 UNION ALL


Select '7308101000','Prefabricated modular type joined by shear connectors','KGM',15,0,0 UNION ALL
Select '7308109000','Other','KGM',15,0,0 UNION ALL


Select '7308201100','Prefabricated modular type joined by shear connectors','KGM',15,0,0 UNION ALL
Select '7308201900','Other','KGM',15,0,0 UNION ALL

Select '7308202100','Prefabricated modular type joined by shear connectors','KGM',15,0,0 UNION ALL
Select '7308202900','Other','KGM',15,0,0 UNION ALL

Select '7308301000','Doors, of a thickness of 6 mm or more but not exceeding 8 mm','KGM',15,0,0 UNION ALL
Select '7308309000','Other','KGM',15,0,0 UNION ALL

Select '7308401000','Prefabricated modular type joined by shear connectors','KGM',15,0,0 UNION ALL
Select '7308409000','Other','KGM',15,0,0 UNION ALL

Select '7308902000','Prefabricated modular type joined by shear connectors','KGM',15,0,0 UNION ALL
Select '7308904000','Corrugated and curved galvanised plates or sheets prepared for use in conduits, culverts or tunnels','KGM',15,0,0 UNION ALL
Select '7308905000','Rails for ships','KGM',15,0,0 UNION ALL
Select '7308906000','Perforated cable trays','KGM',10,0,0 UNION ALL

Select '7308909200','Guardrails','KGM',15,0,0 UNION ALL
Select '7308909900','Other','KGM',15,0,0 UNION ALL


Select '7309001100','Lined or heatinsulated ','KGM',5,0,0 UNION ALL
Select '7309001900','Other','KGM',5,0,0 UNION ALL

Select '7309009100','Lined or heatinsulated ','KGM',10,0,0 UNION ALL
Select '7309009900','Other','KGM',10,0,0 UNION ALL


Select '7310101000','Of tinplate','KGM',15,0,0 UNION ALL

Select '7310109100','Casting, forging or stamping, in the rough state','KGM',5,0,0 UNION ALL
Select '7310109900','Other','KGM',5,0,0 UNION ALL


Select '73102110','Of a capacity of less than 1 l:','',0,0,0 UNION ALL
Select '7310211020','Of tinplate','KGM',15,0,0 UNION ALL
Select '7310211090','Other','KGM',5,0,0 UNION ALL

Select '7310219100','Of tinplate','KGM',15,0,0 UNION ALL
Select '7310219900','Other','KGM',5,0,0 UNION ALL

Select '73102910','Of a capacity of less than 1 l:','',0,0,0 UNION ALL
Select '7310291020','Of tinplate','KGM',15,0,0 UNION ALL
Select '7310291090','Other','KGM',5,0,0 UNION ALL

Select '7310299100','Of tinplate','KGM',15,0,0 UNION ALL
Select '7310299200','Other casting, forging or stamping, in the rough state','KGM',5,0,0 UNION ALL
Select '7310299900','Other','KGM',5,0,0 UNION ALL


Select '7311002300','Of a capacity of less than 30 l, for Liquified Petroleum Gas (LPG)','KGM',0,0,0 UNION ALL
Select '7311002400','Of a capacity of 30 l or more, but less than 110 l, for Liquified Petroleum Gas (LPG)','KGM',0,0,0 UNION ALL
Select '7311002500','Other, for Liquified Petroleum Gas (LPG)','KGM',0,0,0 UNION ALL
Select '7311002600','Other, of a capacity of less than 30 l','KGM',0,0,0 UNION ALL
Select '7311002700','Other, of a capacity of 30 l or more, but less than 110 l','KGM',0,0,0 UNION ALL
Select '7311002900','Other','KGM',0,0,0 UNION ALL

Select '73110091','Of a capacity not exceeding 7.3 l :','',0,0,0 UNION ALL
Select '7311009110','Containers for mechanical  lighters which do not constitute parts of mechanical lighters','KGM',0,0,0 UNION ALL
Select '7311009190','Other','KGM',15,0,0 UNION ALL
Select '73110092','Of a capacity more than 7.3 l but less than 30 l','',0,0,0 UNION ALL
Select '7311009210','Automotive liquefied petroleum gas (LPG) cylinders , welded','KGM',0,0,0 UNION ALL
Select '7311009290','Other','KGM',15,0,0 UNION ALL
Select '73110094','Of a capacity of 30 l or more, but less than 110 l:','',0,0,0 UNION ALL
Select '7311009410','Automotive liquefied petroleum gas (LPG) cylinders , welded','KGM',0,0,0 UNION ALL
Select '7311009490','Other','KGM',15,0,0 UNION ALL
Select '73110099','Other:','',0,0,0 UNION ALL
Select '7311009910','Automotive liquefied petroleum gas (LPG) cylinders , welded','KGM',0,0,0 UNION ALL
Select '7311009990','Other','KGM',15,0,0 UNION ALL


Select '7312101000','Locked coil, flattened strands and nonrotating wire ropes','KGM',5,0,0 UNION ALL
Select '7312102000','Plated or coated with brass and of a diameter not exceeding 3 mm','KGM',5,0,0 UNION ALL

Select '7312109100','Stranded steel wires for prestressing concrete','KGM',5,0,0 UNION ALL
Select '7312109900','Other','KGM',5,0,0 UNION ALL
Select '7312900000','Other','KGM',5,0,0 UNION ALL

Select '7313000000','Barbed wire of iron or steel; twisted hoop or single flat wire, barbed or not, and loosely twisted double wire, of a kind used for fencing, of iron or steel.','KGM',5,0,0 UNION ALL


Select '7314120000','Endless bands for machinery, of stainless steel','KGM',5,0,0 UNION ALL
Select '7314140000','Other woven cloth, of stainless steel','KGM',5,0,0 UNION ALL

Select '7314191000','Endless bands for machinery other than of stainless steel','KGM',5,0,0 UNION ALL
Select '7314199000','Other','KGM',5,0,0 UNION ALL
Select '7314200000','Grill, netting and fencing, welded at the intersection, of wire with a maximum crosssectional dimension of 3 mm or more and having  a mesh size of  l00 cm2 or more ','KGM',5,0,0 UNION ALL

Select '7314310000','Plated or coated with zinc','KGM',5,0,0 UNION ALL
Select '7314390000','Other','KGM',5,0,0 UNION ALL

Select '7314410000','Plated or coated with zinc','KGM',5,0,0 UNION ALL
Select '7314420000','Coated with plastics','KGM',5,0,0 UNION ALL
Select '7314490000','Other','KGM',5,0,0 UNION ALL
Select '7314500000','Expanded metal','KGM',5,0,0 UNION ALL



Select '73151110','Bicycle or motorcycle chain:','',0,0,0 UNION ALL
Select '7315111010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315111090','Other','KGM',5,0,0 UNION ALL

Select '7315119100','Transmission type, of a pitch length of not less than 6 mm and not more than 32 mm','KGM',5,0,0 UNION ALL
Select '73151199','Other:','',0,0,0 UNION ALL
Select '7315119920','Industrial or conveyor type of pitch length not less than 75mm but not more than 152mm','KGM',5,0,0 UNION ALL
Select '7315119930','Other, of mild steel','KGM',5,0,0 UNION ALL
Select '7315119990','Other','KGM',5,0,0 UNION ALL

Select '73151210','Bicycle or motorcycle chain:','',0,0,0 UNION ALL
Select '7315121010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315121090','Other','KGM',5,0,0 UNION ALL
Select '73151290','Other:','',0,0,0 UNION ALL
Select '7315129010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315129090','Other','KGM',5,0,0 UNION ALL

Select '73151910','Of bicycle or motorcycle chain:','',0,0,0 UNION ALL
Select '7315191010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315191090','Other','KGM',5,0,0 UNION ALL
Select '73151990','Other:','',0,0,0 UNION ALL
Select '7315199010','Transmission type, of a pitch length of not less than 6 mm and not more than 32 mm','KGM',5,0,0 UNION ALL
Select '7315199020','Other, of mild steel','KGM',5,0,0 UNION ALL
Select '7315199090','Other','KGM',5,0,0 UNION ALL
Select '73152000','Skid chain:','',0,0,0 UNION ALL
Select '7315200010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315200090','Other','KGM',5,0,0 UNION ALL

Select '73158100','Studlink:','',0,0,0 UNION ALL
Select '7315810010','Mild steel link chain','KGM',5,0,0 UNION ALL
Select '7315810090','Other','KGM',5,0,0 UNION ALL
Select '73158200','Other, welded link:','',0,0,0 UNION ALL
Select '7315820010','Mild steel link chain','KGM',5,0,0 UNION ALL
Select '7315820090','Other','KGM',5,0,0 UNION ALL

Select '73158910','Bicycle or motorcycle chain:','',0,0,0 UNION ALL
Select '7315891010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315891090','Other','KGM',5,0,0 UNION ALL
Select '73158990','Other:','',0,0,0 UNION ALL
Select '7315899010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315899090','Other','KGM',5,0,0 UNION ALL

Select '73159020','Of bicycle or motorcycle chain:','',0,0,0 UNION ALL
Select '7315902010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315902090','Other','KGM',5,0,0 UNION ALL
Select '73159090','Other:','',0,0,0 UNION ALL
Select '7315909010','Of mild steel','KGM',5,0,0 UNION ALL
Select '7315909090','Other','KGM',5,0,0 UNION ALL

Select '7316000000','Anchors, grapnels and parts thereof, of iron or steel.','KGM',5,0,0 UNION ALL

Select '7317001000','Wire nails','KGM',5,0,0 UNION ALL
Select '7317002000','Staples','KGM',5,0,0 UNION ALL
Select '7317003000','Dog spikes for rail sleepers; gang nails ','KGM',5,0,0 UNION ALL
Select '7317009000','Other','KGM',5,0,0 UNION ALL


Select '7318110000','Coach screws','KGM',5,0,0 UNION ALL

Select '7318121000','Having a shank of an external diameter not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318129000','Other','KGM',5,0,0 UNION ALL
Select '7318130000','Screw hooks and screw rings','KGM',5,0,0 UNION ALL

Select '7318141000','Having a shank of an external diameter not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318149000','Other','KGM',5,0,0 UNION ALL

Select '7318151000','Having a shank of an external diameter not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318159000','Other','KGM',5,0,0 UNION ALL

Select '7318161000','For bolts having a shank of an  external diameter not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318169000','Other','KGM',5,0,0 UNION ALL

Select '7318191000','Having a shank of an external diameter  not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318199000','Other','KGM',5,0,0 UNION ALL

Select '7318210000','Spring washers and other lock washers','KGM',5,0,0 UNION ALL
Select '7318220000','Other washers','KGM',5,0,0 UNION ALL

Select '7318231000','Having an external diameter  not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318239000','Other','KGM',5,0,0 UNION ALL
Select '7318240000','Cotters and cotterpins  ','KGM',5,0,0 UNION ALL

Select '7318291000','Having a shank of an external diameter not exceeding 16 mm','KGM',5,0,0 UNION ALL
Select '7318299000','Other','KGM',5,0,0 UNION ALL


Select '7319401000','Safety pins','KGM',5,0,0 UNION ALL
Select '7319402000','Other pins','KGM',5,0,0 UNION ALL

Select '7319901000','Sewing, darning or embroidery needles','KGM',5,0,0 UNION ALL
Select '7319909000','Other','KGM',5,0,0 UNION ALL



Select '7320101100','Suitable for use on motor vehicles of heading 87.02, 87.03 or 87.04','KGM',5,0,0 UNION ALL
Select '7320101200','Suitable for use on other motor vehicles','KGM',5,0,0 UNION ALL
Select '7320101900','Other','KGM',5,0,0 UNION ALL
Select '7320109000','Other','KGM',5,0,0 UNION ALL


Select '7320201100','For motor vehicles','KGM',5,0,0 UNION ALL
Select '7320201200','For earthmoving machinery','KGM',5,0,0 UNION ALL
Select '7320201900','Other','KGM',5,0,0 UNION ALL
Select '7320209000','Other','KGM',5,0,0 UNION ALL

Select '7320901000','Suitable for use on motor vehicles','KGM',5,0,0 UNION ALL
Select '7320909000','Other','KGM',5,0,0 UNION ALL


Select '7321110000','For gas fuel or for both gas and other fuels','UNT',15,0,0 UNION ALL
Select '7321120000','For liquid fuel','UNT',15,0,0 UNION ALL

Select '7321191000','For solid fuel','UNT',15,0,0 UNION ALL
Select '7321199000','Other','UNT',15,0,0 UNION ALL

Select '7321810000','For gas fuel or for both gas and other fuels','UNT',15,0,0 UNION ALL
Select '7321820000','For liquid fuel','UNT',15,0,0 UNION ALL
Select '7321890000','Other, including appliances for solid fuel','UNT',15,0,0 UNION ALL

Select '7321901000','Of kerosene stoves','KGM',5,0,0 UNION ALL
Select '73219020','Of cooking appliances and plate warmers using gas fuel:','',0,0,0 UNION ALL
Select '7321902010','Pressed parts, whether enamelled or not','KGM',15,0,0 UNION ALL
Select '7321902020','Burner','KGM',15,0,0 UNION ALL
Select '7321902090','Other','KGM',5,0,0 UNION ALL
Select '7321909000','Other','KGM',5,0,0 UNION ALL


Select '7322110000','Of cast iron','KGM',0,0,0 UNION ALL
Select '7322190000','Other','KGM',0,0,0 UNION ALL
Select '7322900000','Other','KGM',0,0,0 UNION ALL

Select '7323100000','Iron or steel wool; pot scourers and scouring or polishing pads, gloves and the like','KGM',10,0,0 UNION ALL


Select '7323911000','Kitchenware','KGM',15,0,0 UNION ALL
Select '7323912000','Ashtrays','KGM',15,0,0 UNION ALL
Select '7323919000','Other','KGM',15,0,0 UNION ALL
Select '7323920000','Of cast iron, enamelled','KGM',15,0,0 UNION ALL

Select '7323931000','Kitchenware','KGM',15,0,0 UNION ALL
Select '7323932000','Ashtrays','KGM',15,0,0 UNION ALL
Select '7323939000','Other','KGM',15,0,0 UNION ALL
Select '7323940000','Of iron (other than cast iron) or steel, enamelled','KGM',15,0,0 UNION ALL

Select '7323991000','Kitchenware','KGM',15,0,0 UNION ALL
Select '7323992000','Ashtrays','KGM',15,0,0 UNION ALL
Select '7323999000','Other','KGM',15,0,0 UNION ALL


Select '7324101000','Kitchen sinks','KGM',15,0,0 UNION ALL
Select '7324109000','Other','KGM',5,0,0 UNION ALL


Select '7324211000','Bathtubs having rectangular or oblong interior shape','KGM',15,0,0 UNION ALL
Select '7324219000','Other','KGM',5,0,0 UNION ALL

Select '7324291000','Bathtubs having rectangular or oblong interior shape','KGM',15,0,0 UNION ALL
Select '7324299000','Other','KGM',5,0,0 UNION ALL

Select '7324901000','Flushing water closets or urinals (fixed type) ','KGM',0,0,0 UNION ALL
Select '7324903000','Bedpans and portable urinals ','KGM',0,0,0 UNION ALL

Select '73249091','Parts of kitchen sinks or bathtubs:','',0,0,0 UNION ALL
Select '7324909140','Of kitchen sinks; of bathtubs having rectangular or oblong interior shape','KGM',15,0,0 UNION ALL
Select '7324909150','Of other bathtubs','KGM',5,0,0 UNION ALL
Select '7324909300','Parts of flushing water closets or urinals (fixed type)','KGM',0,0,0 UNION ALL
Select '73249099','Other:','',0,0,0 UNION ALL
Select '7324909910','Kitchen sinks , of steel','KGM',15,0,0 UNION ALL
Select '7324909990','Other','KGM',5,0,0 UNION ALL


Select '7325102000','Manhole covers, gratings and frames therefor','KGM',15,0,0 UNION ALL
Select '7325103000','Spouts and cups for latex collection','KGM',5,0,0 UNION ALL
Select '7325109000','Other','KGM',5,0,0 UNION ALL

Select '7325910000','Grinding balls and similar articles for mills','KGM',5,0,0 UNION ALL

Select '7325992000','Manhole covers, gratings and frames thereof','KGM',15,0,0 UNION ALL
Select '7325999000','Other','KGM',5,0,0 UNION ALL


Select '7326110000','Grinding balls and similar articles for mills','KGM',5,0,0 UNION ALL
Select '7326190000','Other','KGM',5,0,0 UNION ALL

Select '7326205000','Poultry cages and the like','KGM',0,0,0 UNION ALL
Select '7326206000','Rat traps','KGM',5,0,0 UNION ALL
Select '7326209000','Other','KGM',5,0,0 UNION ALL

Select '7326901000','Ships'' rudders','KGM',5,0,0 UNION ALL
Select '7326902000','Spouts and cups for latex collection','KGM',5,0,0 UNION ALL
Select '7326903000','Stainless steel clamp assemblies with rubber sleeves of a kind used for hubless cast iron pipes and pipe fittings','KGM',5,0,0 UNION ALL
Select '7326906000','Bunsen burners','KGM',15,0,0 UNION ALL
Select '7326907000','Horseshoes; riding boots spurs','KGM',0,0,0 UNION ALL

Select '7326909100','Cigarette cases and boxes','KGM',15,0,0 UNION ALL
Select '73269099','Other:','',0,0,0 UNION ALL
Select '7326909930','Blinds ','KGM',15,0,0 UNION ALL
Select '7326909990','Other','KGM',5,0,0 UNION ALL

Select '7401000000','Copper mattes; cement copper (precipitated copper).','KGM',0,5,0 UNION ALL

Select '7402000000','Unrefined copper; copper anodes for electrolytic refining.','KGM',0,5,0 UNION ALL


Select '7403110000','Cathodes and sections of cathodes','KGM',0,5,0 UNION ALL
Select '7403120000','Wirebars','KGM',0,5,0 UNION ALL
Select '7403130000','Billets','KGM',0,5,0 UNION ALL
Select '7403190000','Other','KGM',0,5,0 UNION ALL

Select '7403210000','Copperzinc base alloys (brass)','KGM',0,5,0 UNION ALL
Select '7403220000','Coppertin base alloys (bronze)','KGM',0,5,0 UNION ALL
Select '7403290000','Other copper alloys (other than master alloys of heading 74.05)','KGM',0,5,0 UNION ALL

Select '7404000000','Copper waste and scrap.','KGM',0,10,0 UNION ALL

Select '7405000000','Master alloys of copper.','KGM',0,10,0 UNION ALL

Select '7406100000','Powders of nonlamellar structure','KGM',0,0,0 UNION ALL
Select '7406200000','Powders of lamellar structure; flakes','KGM',0,0,0 UNION ALL


Select '7407103000','Profiles','KGM',0,0,0 UNION ALL
Select '7407104000','Bars and rods','KGM',18,0,0 UNION ALL

Select '7407210000','Of copperzinc base alloys (brass)','KGM',0,0,0 UNION ALL
Select '7407290000','Other','KGM',0,0,0 UNION ALL



Select '7408111000','Of which the maximum crosssectional dimension does not exceed 14 mm','KGM',25,0,0 UNION ALL
Select '7408119000','Other','KGM',25,0,0 UNION ALL
Select '7408190000','Other','KGM',25,0,0 UNION ALL

Select '7408210000','Of copperzinc base alloys (brass)','KGM',0,0,0 UNION ALL
Select '7408220000','Of coppernickel base alloys (cupronickel) or coppernickelzinc base alloys (nickel silver)','KGM',0,0,0 UNION ALL
Select '7408290000','Other','KGM',0,0,0 UNION ALL


Select '7409110000','In coils','KGM',0,0,0 UNION ALL
Select '7409190000','Other','KGM',0,0,0 UNION ALL

Select '7409210000','In coils','KGM',0,0,0 UNION ALL
Select '7409290000','Other','KGM',0,0,0 UNION ALL

Select '7409310000','In coils','KGM',0,0,0 UNION ALL
Select '7409390000','Other','KGM',0,0,0 UNION ALL
Select '7409400000','Of coppernickel base alloys (cupronickel) or coppernickelzinc base alloys (nickel silver)','KGM',0,0,0 UNION ALL
Select '7409900000','Of other copper alloys','KGM',0,0,0 UNION ALL


Select '7410110000','Of refined copper','KGM',0,0,0 UNION ALL
Select '7410120000','Of copper alloys','KGM',0,0,0 UNION ALL


Select '7410211000','Copper clad laminate for printed circuit boards','KGM',0,0,0 UNION ALL
Select '7410219000','Other','KGM',0,0,0 UNION ALL
Select '7410220000','Of copper alloys','KGM',0,0,0 UNION ALL

Select '7411100000','Of refined copper','KGM',0,0,0 UNION ALL

Select '7411210000','Of copperzinc base alloys (brass)','KGM',0,0,0 UNION ALL
Select '7411220000','Of coppernickel base alloys (cupronickel) or coppernickel zinc base alloys (nickel silver)','KGM',0,0,0 UNION ALL
Select '7411290000','Other','KGM',0,0,0 UNION ALL

Select '7412100000','Of refined copper','KGM',0,0,0 UNION ALL

Select '7412202000','Hose fittings','KGM',0,0,0 UNION ALL

Select '7412209100','Of copperzinc base alloys (brass)','KGM',0,0,0 UNION ALL
Select '7412209900','Other','KGM',5,0,0 UNION ALL

Select '7413001000','Of a diameter not exceeding 28.28 mm','KGM',25,0,0 UNION ALL
Select '7413009000','Other','KGM',25,0,0 UNION ALL


Select '7415101000','Nails','KGM',0,0,0 UNION ALL
Select '7415102000','Staples','KGM',0,0,0 UNION ALL
Select '7415109000','Other','KGM',0,0,0 UNION ALL

Select '7415210000','Washers (including spring washers)','KGM',0,0,0 UNION ALL
Select '7415290000','Other','KGM',0,0,0 UNION ALL


Select '7415331000','Screws','KGM',0,0,0 UNION ALL
Select '7415332000','Bolts and nuts','KGM',0,0,0 UNION ALL
Select '7415390000','Other','KGM',0,0,0 UNION ALL


Select '7418101000','Pot scourers and scouring or polishing pads, gloves and the like','KGM',25,0,0 UNION ALL
Select '7418103000','Cooking or heating apparatus of a kind used for household purposes, nonelectric and parts thereof','KGM',5,0,0 UNION ALL
Select '7418109000','Other','KGM',25,0,0 UNION ALL
Select '7418200000','Sanitary ware and parts thereof','KGM',5,0,0 UNION ALL

Select '7419100000','Chain and parts thereof','KGM',0,0,0 UNION ALL


Select '7419911000','Reservoirs, tanks, vats and similar containers not fitted with mechanical or thermal equipment','KGM',0,0,0 UNION ALL
Select '7419919000','Other','KGM',0,0,0 UNION ALL


Select '7419993100','For machinery','KGM',5,0,0 UNION ALL
Select '7419993900','Other','KGM',5,0,0 UNION ALL
Select '7419994000','Springs','KGM',0,0,0 UNION ALL
Select '7419995000','Cigarette cases or boxes ','KGM',25,0,0 UNION ALL
Select '7419996000','Cooking or heating apparatus, other than of a kind used for domestic purposes, and parts thereof','KGM',5,0,0 UNION ALL
Select '7419997000','Articles specially designed for use during religious rites','KGM',0,0,0 UNION ALL

Select '7419999100','Electroplating anodes; capacity measures (other than for domestic use)','KGM',0,0,0 UNION ALL
Select '7419999200','Reservoirs, tanks, vats and similar containers not fitted with mechanical or thermal equipment of a capacity of 300 l or less','KGM',0,0,0 UNION ALL
Select '7419999900','Other','KGM',0,0,0 UNION ALL

Select '7501100000','Nickel mattes','KGM',0,10,0 UNION ALL
Select '7501200000','Nickel oxide sinters and other intermediate products of nickel  metallurgy','KGM',0,10,0 UNION ALL

Select '7502100000','Nickel, not alloyed ','KGM',0,10,0 UNION ALL
Select '7502200000','Nickel alloys','KGM',0,10,0 UNION ALL

Select '7503000000','Nickel waste and scrap.','KGM',0,0,0 UNION ALL

Select '7504000000','Nickel powders and flakes. ','KGM',0,0,0 UNION ALL


Select '7505110000','Of nickel, not alloyed','KGM',0,0,0 UNION ALL
Select '7505120000','Of nickel alloys','KGM',0,0,0 UNION ALL

Select '7505210000','Of nickel, not alloyed','KGM',0,0,0 UNION ALL
Select '7505220000','Of nickel alloys','KGM',0,0,0 UNION ALL

Select '7506100000','Of nickel, not alloyed','KGM',0,0,0 UNION ALL
Select '7506200000','Of nickel alloys','KGM',0,0,0 UNION ALL


Select '7507110000','Of nickel, not alloyed','KGM',0,0,0 UNION ALL
Select '7507120000','Of nickel alloys','KGM',0,0,0 UNION ALL
Select '7507200000','Tube or pipe fittings','KGM',0,0,0 UNION ALL

Select '7508100000','Cloth, grill and netting, of nickel wire','KGM',0,0,0 UNION ALL

Select '7508903000','Bolts and nuts','KGM',0,0,0 UNION ALL
Select '7508905000','Electroplating anodes, including those produced by electrolysis','KGM',0,0,0 UNION ALL
Select '7508909000','Other','KGM',0,0,0 UNION ALL

Select '7601100000','Aluminium, not alloyed','KGM',0,0,0 UNION ALL
Select '7601200000','Aluminium alloys','KGM',0,0,0 UNION ALL

Select '7602000000','Aluminium waste or scrap.','KGM',0,10,0 UNION ALL

Select '7603100000','Powders of nonlamellar structure','KGM',0,0,0 UNION ALL

Select '7603201000','Flakes','KGM',0,0,0 UNION ALL
Select '7603202000','Powders of lamellar structure','KGM',0,0,0 UNION ALL


Select '7604101000','Bars and rods','KGM',25,0,0 UNION ALL
Select '7604109000','Other ','KGM',25,0,0 UNION ALL


Select '7604211000','Perforated tube profiles of a kind suitable for use in evaporator coils of motor vehicle air conditioning machines','KGM',25,0,0 UNION ALL
Select '7604212000','Aluminium spacers (hollow profiles with one side having light perforations along its whole length)','KGM',25,0,0 UNION ALL
Select '7604219000','Other','KGM',25,0,0 UNION ALL

Select '7604291000','Extruded bars and rods','KGM',25,0,0 UNION ALL
Select '7604293000','Yshaped profiles for zip fasteners, in coils','KGM',25,0,0 UNION ALL
Select '7604299000','Other','KGM',25,0,0 UNION ALL


Select '7605110000','Of which the maximum crosssectional dimension exceeds 7 mm','KGM',25,0,0 UNION ALL

Select '7605191000','Of a diameter not exceeding 0.0508 mm','KGM',25,0,0 UNION ALL
Select '7605199000','Other','KGM',25,0,0 UNION ALL

Select '7605210000','Of which the maximum crosssectional dimension exceeds 7 mm','KGM',25,0,0 UNION ALL

Select '7605291000','Of a diameter not exceeding 0.254 mm ','KGM',25,0,0 UNION ALL
Select '7605299000','Other','KGM',25,0,0 UNION ALL



Select '7606111000','Plain or figured by rolling or pressing, not otherwise surface treated','KGM',30,0,0 UNION ALL
Select '7606119000','Other','KGM',30,0,0 UNION ALL

Select '7606122000','Aluminium plates, not sensitised, of a kind used in the printing industry','KGM',30,0,0 UNION ALL

Select '7606123200','For making can stock including end stock and tab stock, in coils','KGM',30,0,0 UNION ALL
Select '7606123300','Other, of aluminium alloy 5082 or 5182, exceeding 1 m in width, in coils','KGM',30,0,0 UNION ALL
Select '7606123400','Other, litho grade sheet alloy HA 1052 hardness temper H19 dan alloy HA 1050 hardness temper H18','KGM',30,0,0 UNION ALL
Select '7606123500','Other, surface worked','KGM',30,0,0 UNION ALL
Select '7606123900','Other','KGM',30,0,0 UNION ALL
Select '7606129000','Other','KGM',30,0,0 UNION ALL

Select '7606910000','Of aluminium, not alloyed','KGM',30,0,0 UNION ALL
Select '7606920000','Of aluminium alloys','KGM',30,0,0 UNION ALL


Select '7607110000','Rolled but not further worked','KGM',25,0,0 UNION ALL
Select '7607190000','Other','KGM',25,0,0 UNION ALL

Select '7607201000','Thermal insulation foil','KGM',30,0,0 UNION ALL
Select '7607209000','Other','KGM',30,0,0 UNION ALL

Select '7608100000','Of aluminium, not alloyed','KGM',25,0,0 UNION ALL
Select '7608200000','Of aluminium alloys','KGM',25,0,0 UNION ALL

Select '7609000000','Aluminium tube or pipe fittings (for example, couplings, elbows, sleeves).','KGM',5,0,0 UNION ALL


Select '7610101000','Doors and their frames and thresholds for doors','KGM',25,0,0 UNION ALL
Select '7610109000','Other','KGM',25,0,0 UNION ALL

Select '7610903000','Internal or external floating roofs for petroleum storage tanks','KGM',25,0,0 UNION ALL

Select '7610909100','Bridges and bridge sections; towers or lattice masts','KGM',25,0,0 UNION ALL
Select '7610909900','Other','KGM',25,0,0 UNION ALL

Select '7611000000','Aluminium reservoirs, tanks, vats and similar containers, for any material (other than compressed or liquefied gas), of a capacity exceeding 300 l, whether or not lined or heatinsulated, but not fitted with mechanical or thermal equipment.','KGM',20,0,0 UNION ALL

Select '7612100000','Collapsible tubular containers','KGM',20,0,0 UNION ALL

Select '7612901000','Seamless containers of a kind suitable for fresh milk','KGM',20,0,0 UNION ALL
Select '7612909000','Other','KGM',20,0,0 UNION ALL

Select '7613000000','Aluminium containers for compressed or liquefied gas.','KGM',5,0,0 UNION ALL



Select '7614101100','Of a diameter not exceeding 25.3 mm','KGM',30,0,0 UNION ALL
Select '7614101200','Of a diameter exceeding 25.3 mm but not exceeding 28.28 mm','KGM',30,0,0 UNION ALL
Select '7614101900','Other','KGM',30,0,0 UNION ALL
Select '7614109000','Other','KGM',30,0,0 UNION ALL


Select '7614901100','Of a diameter not exceeding 25.3 mm','KGM',30,0,0 UNION ALL
Select '7614901200','Of a diameter exceeding 25.3 mm but not exceeding 28.28 mm','KGM',30,0,0 UNION ALL
Select '7614901900','Other','KGM',30,0,0 UNION ALL
Select '7614909000','Other','KGM',30,0,0 UNION ALL


Select '7615101000','Pot scourers and scouring or polishing pads, gloves and the like ','KGM',30,0,0 UNION ALL
Select '7615109000','Other','KGM',30,0,0 UNION ALL

Select '7615202000','Bedpans, urinals and chamberpots','KGM',0,0,0 UNION ALL
Select '7615209000','Other','KGM',25,0,0 UNION ALL


Select '7616101000','Nails','KGM',20,0,0 UNION ALL
Select '7616102000','Staples and hooks; bolts and nuts','KGM',20,0,0 UNION ALL
Select '7616109000','Other','KGM',20,0,0 UNION ALL

Select '7616910000','Cloth, grill, netting and fencing, of aluminium wire','KGM',5,0,0 UNION ALL

Select '7616992000','Ferrules of a kind suitable for use in the manufacture of pencils','KGM',20,0,0 UNION ALL
Select '7616993000','Slugs,  round,  of such dimension that the thickness exceeds  onetenth of the diameter','KGM',20,0,0 UNION ALL
Select '7616994000','Bobbins, spools, reels and similar supports for textile yarn','KGM',20,0,0 UNION ALL

Select '7616995100','Venetian blinds','KGM',25,0,0 UNION ALL
Select '7616995900','Other','KGM',25,0,0 UNION ALL
Select '7616996000','Spouts and cups of a kind used for latex collection','KGM',20,0,0 UNION ALL
Select '7616997000','Cigarette cases or boxes','KGM',25,0,0 UNION ALL
Select '7616998000','Expanded metal','KGM',5,0,0 UNION ALL
Select '7616999000','Other','KGM',20,0,0 UNION ALL

Select '7801100000','Refined lead','KGM',0,0,0 UNION ALL

Select '7801910000','Containing  by weight antimony as the principal other element','KGM',15,0,0 UNION ALL
Select '7801990000','Other','KGM',15,15,0 UNION ALL

Select '7802000000','Lead waste and scrap.','KGM',0,15,0 UNION ALL



Select '7804111000','Of a thickness  not exceeding 0.15 mm','KGM',0,0,0 UNION ALL
Select '7804119000','Other','KGM',0,0,0 UNION ALL
Select '7804190000','Other','KGM',0,0,0 UNION ALL
Select '7804200000','Powders and flakes','KGM',0,0,0 UNION ALL

Select '7806002000','Bars, rods, profiles and wire','KGM',0,0,0 UNION ALL
Select '7806003000','Tubes, pipes and tube or pipe fittings (for example, couplings, elbows, sleeves)','KGM',0,0,0 UNION ALL
Select '7806004000','Lead wool; washers; electroplating anodes','KGM',0,0,0 UNION ALL
Select '7806009000','Other','KGM',0,0,0 UNION ALL


Select '7901110000','Containing by weight 99.99% or more of zinc','KGM',0,5,0 UNION ALL
Select '7901120000','Containing by weight less than 99.99% of zinc','KGM',0,5,0 UNION ALL
Select '7901200000','Zinc alloys','KGM',0,5,0 UNION ALL

Select '7902000000','Zinc waste and scrap.','KGM',0,0,0 UNION ALL

Select '7903100000','Zinc dust','KGM',0,0,0 UNION ALL
Select '7903900000','Other','KGM',0,0,0 UNION ALL

Select '7904000000','Zinc bars, rods, profiles and wire.','KGM',0,0,0 UNION ALL

Select '7905004000','Of a thickness not exceeding 0.15 mm','KGM',0,0,0 UNION ALL
Select '7905009000','Other','KGM',20,0,0 UNION ALL

Select '7907003000','Gutters, roof capping, skylight frames and other fabricated building components','KGM',20,0,0 UNION ALL
Select '7907004000','Tubes, pipes and tube or pipe fittings (for example couplings, elbows, sleeves)','KGM',0,0,0 UNION ALL

Select '7907009100','Cigarette cases or boxes; ashtrays','KGM',25,0,0 UNION ALL
Select '7907009200','Other household articles','KGM',20,0,0 UNION ALL
Select '7907009300','Electroplating anodes; stencil plates; nails, tacks, nuts, bolts, screws, rivets and similar fastening; zinc calots for battery cells ','KGM',5,0,0 UNION ALL
Select '7907009900','Other','KGM',5,0,0 UNION ALL

Select '8001100000','Tin, not alloyed','KGM',0,0,0 UNION ALL
Select '8001200000','Tin alloys','KGM',0,0,0 UNION ALL

Select '8002000000','Tin waste and scrap.','KGM',0,0,0 UNION ALL

Select '8003001000','Soldering bars','KGM',0,0,0 UNION ALL
Select '8003009000','Other','KGM',0,0,0 UNION ALL

Select '8007002000','Plates, sheets and strip, of a thickness exceeding 0.2 mm','KGM',0,0,0 UNION ALL
Select '8007003000','Foil (whether or not printed or backed with paper, paperboard, plastics or similar backing materials), of a thickness (excluding any backing) not exceeding 0.2 mm; powders and flakes','KGM',0,0,0 UNION ALL
Select '8007004000','Tubes, pipes and tube or pipe fittings (for example, couplings, elbows, sleeves)','KGM',0,0,0 UNION ALL

Select '8007009100','Cigarette cases or boxes; ashtrays','KGM',25,0,0 UNION ALL
Select '8007009200','Other household articles','KGM',20,0,0 UNION ALL
Select '8007009300','Collapsible tubes ','KGM',5,0,0 UNION ALL
Select '8007009900','Other','KGM',5,0,0 UNION ALL

Select '8101100000','Powders','KGM',0,0,0 UNION ALL

Select '8101940000','Unwrought tungsten, including bars and rods obtained simply by sintering','KGM',0,0,0 UNION ALL
Select '8101960000','Wire','KGM',0,0,0 UNION ALL
Select '8101970000','Waste and scrap','KGM',0,0,0 UNION ALL

Select '8101991000','Bars and rods, other than those obtained simply by sintering; profiles, sheets, strip and foil','KGM',0,0,0 UNION ALL
Select '8101999000','Other','KGM',0,0,0 UNION ALL

Select '8102100000','Powders','KGM',0,0,0 UNION ALL

Select '8102940000','Unwrought molybdenum, including bars and rods obtained simply by sintering','KGM',0,0,0 UNION ALL
Select '8102950000','Bars and rods, other than those obtained simply by sintering,  profiles, plates, sheets, strip and foil','KGM',0,0,0 UNION ALL
Select '8102960000','Wire','KGM',0,0,0 UNION ALL
Select '8102970000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8102990000','Other','KGM',0,0,0 UNION ALL

Select '8103200000','Unwrought tantalum, including bars and rods obtained simply by sintering; powders','KGM',0,0,0 UNION ALL
Select '8103300000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8103900000','Other','KGM',0,0,0 UNION ALL


Select '8104110000','Containing at least 99.8% by weight of magnesium','KGM',0,0,0 UNION ALL
Select '8104190000','Other','KGM',0,0,0 UNION ALL
Select '8104200000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8104300000','Raspings, turnings and granules, graded according to size; powders','KGM',0,0,0 UNION ALL
Select '8104900000','Other','KGM',0,0,0 UNION ALL


Select '8105201000','Unwrought cobalt','KGM',0,0,0 UNION ALL
Select '8105209000','Other','KGM',0,0,0 UNION ALL
Select '8105300000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8105900000','Other','KGM',0,0,0 UNION ALL

Select '8106001000','Unwrought bismuth; waste and scrap; powders ','KGM',0,0,0 UNION ALL
Select '8106009000','Other','KGM',0,0,0 UNION ALL

Select '8107200000','Unwrought cadmium; powders','KGM',0,0,0 UNION ALL
Select '8107300000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8107900000','Other','KGM',0,0,0 UNION ALL

Select '8108200000','Unwrought titanium; powders','KGM',0,0,0 UNION ALL
Select '8108300000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8108900000','Other','KGM',0,0,0 UNION ALL

Select '8109200000','Unwrought zirconium; powders','KGM',0,0,0 UNION ALL
Select '8109300000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8109900000','Other','KGM',0,0,0 UNION ALL

Select '8110100000','Unwrought antimony; powders','KGM',0,0,0 UNION ALL
Select '8110200000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8110900000','Others','KGM',0,0,0 UNION ALL

Select '8111001000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8111009000','Other','KGM',0,0,0 UNION ALL


Select '8112120000','Unwrought; powders','KGM',0,0,0 UNION ALL
Select '8112130000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8112190000','Other','KGM',0,0,0 UNION ALL

Select '8112210000','Unwrought; powders ','KGM',0,0,0 UNION ALL
Select '8112220000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8112290000','Other','KGM',0,0,0 UNION ALL

Select '8112510000','Unwrought; powders','KGM',0,0,0 UNION ALL
Select '8112520000','Waste and scrap','KGM',0,0,0 UNION ALL
Select '8112590000','Other','KGM',0,0,0 UNION ALL

Select '8112920000','Unwrought; waste and scrap; powders','KGM',0,0,0 UNION ALL
Select '8112990000','Other','KGM',0,0,0 UNION ALL

Select '8113000000','Cermets and articles thereof, including waste and scrap.','KGM',0,0,0 UNION ALL

Select '8201100000','Spades and shovels','KGM',0,0,0 UNION ALL

Select '8201301000','Hoes or rakes','KGM',0,0,0 UNION ALL
Select '8201309000','Other','KGM',0,0,0 UNION ALL
Select '8201400000','Axes, bill hooks and similar hewing tools','KGM',0,0,0 UNION ALL
Select '8201500000','Secateurs and similar onehanded pruners and shears (including poultry shears)','KGM',0,0,0 UNION ALL
Select '8201600000','Hedge shears, twohanded pruning shears and similar twohanded shears','KGM',0,0,0 UNION ALL
Select '8201900000','Other hand tools of a kind used in agriculture, horticulture or forestry','KGM',0,0,0 UNION ALL

Select '8202100000','Hand saws','KGM',5,0,0 UNION ALL
Select '8202200000','Band saw blades','KGM',5,0,0 UNION ALL

Select '8202310000','With working part of steel','KGM',5,0,0 UNION ALL
Select '8202390000','Other, including parts','KGM',5,0,0 UNION ALL
Select '8202400000','Chain saw blades','KGM',5,0,0 UNION ALL

Select '8202910000','Straight saw blades, for working metal','KGM',5,0,0 UNION ALL

Select '8202991000','Straight saw blades','KGM',5,0,0 UNION ALL
Select '8202999000','Other','KGM',0,0,0 UNION ALL

Select '8203100000','Files, rasps and similar tools','KGM',5,0,0 UNION ALL
Select '8203200000','Pliers (including cutting pliers), pincers, tweezers and similar tools','KGM',5,0,0 UNION ALL
Select '8203300000','Metal cutting shears and similar tools','KGM',5,0,0 UNION ALL
Select '8203400000','Pipecutters, bolt croppers, perforating punches and similar tools','KGM',5,0,0 UNION ALL


Select '8204110000','Nonadjustable','KGM',5,0,0 UNION ALL
Select '8204120000','Adjustable','KGM',5,0,0 UNION ALL
Select '8204200000','Interchangeable spanner sockets, with or without handles','KGM',5,0,0 UNION ALL

Select '8205100000','Drilling, threading or tapping tools','KGM',5,0,0 UNION ALL
Select '8205200000','Hammers and sledge hammers','KGM',5,0,0 UNION ALL
Select '8205300000','Planes, chisels, gouges and similar cutting tools for working wood','KGM',5,0,0 UNION ALL
Select '8205400000','Screwdrivers','KGM',5,0,0 UNION ALL

Select '8205510000','Household tools','KGM',5,0,0 UNION ALL
Select '8205590000','Other','KGM',5,0,0 UNION ALL
Select '8205600000','Blow lamps','KGM',5,0,0 UNION ALL
Select '8205700000','Vices, clamps and the like','KGM',5,0,0 UNION ALL
Select '8205900000','Other, including sets of articles of two or more of  subheadings of this heading','KGM',5,0,0 UNION ALL

Select '8206000000','Tools of two or more of the headings 82.02 to 82.05, put up in sets for retail sale.','KGM',5,0,0 UNION ALL


Select '8207130000','With working part of cermets','KGM',5,0,0 UNION ALL
Select '8207190000','Other, including parts','KGM',5,0,0 UNION ALL
Select '8207200000','Dies for drawing or extruding metal','KGM',5,0,0 UNION ALL
Select '8207300000','Tools for pressing, stamping or punching','KGM',5,0,0 UNION ALL
Select '8207400000','Tools for tapping or threading','KGM',5,0,0 UNION ALL
Select '8207500000','Tools for drilling, other than for rock drilling','KGM',5,0,0 UNION ALL
Select '8207600000','Tools for boring or broaching','KGM',5,0,0 UNION ALL
Select '8207700000','Tools for milling','KGM',5,0,0 UNION ALL
Select '8207800000','Tools for turning','KGM',5,0,0 UNION ALL
Select '8207900000','Other interchangeable tools','KGM',5,0,0 UNION ALL

Select '8208100000','For metal working','KGM',5,0,0 UNION ALL
Select '8208200000','For wood working','KGM',5,0,0 UNION ALL
Select '8208300000','For kitchen appliances or for machines used by the food industry','KGM',5,0,0 UNION ALL
Select '8208400000','For agricultural, horticultural or forestry machines','KGM',5,0,0 UNION ALL
Select '8208900000','Other','KGM',5,0,0 UNION ALL

Select '8209000000','Plates, sticks, tips and the like for tools, unmounted, of cermets.','KGM',5,0,0 UNION ALL

Select '8210000000','Handoperated mechanical appliances, weighing 10 kg or less, used in the preparation, conditioning or serving of food or drink.','KGM',5,0,0 UNION ALL

Select '8211100000','Sets of assorted articles ','KGM',25,0,0 UNION ALL

Select '8211910000','Table knives having fixed blades ','KGM',25,0,0 UNION ALL

Select '8211925000','Of a kind used for agriculture, horticulture or forestry','KGM',5,0,0 UNION ALL

Select '8211929100','Flick knives or spring knives; hunting knives, diving knives and scouts'' knives; penknives with blades of 15 cm or more in length','KGM',25,0,0 UNION ALL
Select '8211929900','Other','KGM',25,0,0 UNION ALL


Select '8211932100','With handle of base metal','KGM',5,0,0 UNION ALL
Select '8211932900','Other','KGM',5,0,0 UNION ALL
Select '8211939000','Other ','KGM',25,0,0 UNION ALL

Select '8211941000','For knives of a kind used for agriculture, horticulture or forestry','KGM',5,0,0 UNION ALL
Select '8211949000','Other','KGM',25,0,0 UNION ALL
Select '8211950000','Handles of base metal','KGM',25,0,0 UNION ALL

Select '8212100000','Razors','UNT',0,0,0 UNION ALL

Select '8212201000','Doubleedged razor blades','UNT',0,0,0 UNION ALL
Select '8212209000','Other ','UNT',0,0,0 UNION ALL
Select '8212900000','Other parts','KGM',0,0,0 UNION ALL

Select '8213000000','Scissors, tailors"shears and similar shears, and blades therefor.','KGM',25,0,0 UNION ALL

Select '8214100000','Paper knives, letter openers, erasing knives, pencil sharpeners and blades therefor','KGM',25,0,0 UNION ALL
Select '8214200000','Manicure or pedicure sets and instruments (including nail files)','KGM',25,0,0 UNION ALL
Select '8214900000','Other','KGM',25,0,0 UNION ALL

Select '8215100000','Sets of assorted articles containing at least one article plated with precious metal','KGM',25,0,0 UNION ALL
Select '8215200000','Other sets of assorted articles','KGM',25,0,0 UNION ALL

Select '8215910000','Plated with precious metal','KGM',25,0,0 UNION ALL
Select '8215990000','Other','KGM',25,0,0 UNION ALL

Select '8301100000','Padlocks ','KGM',25,0,0 UNION ALL
Select '8301200000','Locks of a kind used for motor vehicles','KGM',25,0,0 UNION ALL
Select '8301300000','Locks of a kind used for furniture','KGM',0,0,0 UNION ALL

Select '8301401000','Handcuffs','KGM',25,0,0 UNION ALL
Select '8301402000','Door locks','KGM',25,0,0 UNION ALL
Select '8301409000','Other','KGM',25,0,0 UNION ALL
Select '8301500000','Clasps and frames with clasps, incorporating locks','KGM',25,0,0 UNION ALL
Select '8301600000','Parts','KGM',25,0,0 UNION ALL
Select '8301700000','Keys presented separately','KGM',25,0,0 UNION ALL

Select '8302100000','Hinges','KGM',15,0,0 UNION ALL

Select '8302201000','Of a diameter (including tyres) exceeding 100 mm, but not exceeding 250 mm','KGM',15,0,0 UNION ALL
Select '8302209000','Other','KGM',20,0,0 UNION ALL

Select '8302301000','Hasps','KGM',15,0,0 UNION ALL
Select '8302309000','Other','KGM',20,0,0 UNION ALL



Select '8302413100','Hasps','KGM',15,0,0 UNION ALL
Select '8302413900','Other','KGM',20,0,0 UNION ALL
Select '8302419000','Other','KGM',20,0,0 UNION ALL

Select '8302422000','Hasps','KGM',15,0,0 UNION ALL
Select '8302429000','Other','KGM',0,0,0 UNION ALL

Select '8302491000','Of a kind suitable for saddlery','KGM',0,0,0 UNION ALL

Select '8302499100','Hasps','KGM',15,0,0 UNION ALL
Select '8302499900','Other','KGM',20,0,0 UNION ALL
Select '8302500000','Hatracks, hatpegs, brackets and similar fixtures','KGM',20,0,0 UNION ALL
Select '8302600000','Automatic door closers','KGM',20,0,0 UNION ALL

Select '8303000000','Armoured or reinforced safes, strongboxes and doors and safe deposit lockers for strongrooms, cash or deed boxes and the like, of base metal.','KGM',20,0,0 UNION ALL

Select '8304001000','Filing cabinets and cardindex cabinets','KGM',25,0,0 UNION ALL

Select '8304009100','Of aluminium','KGM',25,0,0 UNION ALL
Select '8304009200','Of nickel','KGM',25,0,0 UNION ALL
Select '8304009300','Of copper or of lead','KGM',25,0,0 UNION ALL
Select '8304009900','Other','KGM',25,0,0 UNION ALL


Select '8305101000','For double loop wire binders','KGM',0,0,0 UNION ALL
Select '8305109000','Other','KGM',0,0,0 UNION ALL

Select '8305201000','Of a kind for office use','KGM',25,0,0 UNION ALL
Select '8305202000','Other, of iron or steel','KGM',25,0,0 UNION ALL
Select '8305209000','Other','KGM',5,0,0 UNION ALL

Select '8305901000','Paper clips','KGM',15,0,0 UNION ALL
Select '8305909000','Other','KGM',5,0,0 UNION ALL

Select '8306100000','Bells, gongs and the like','KGM',5,0,0 UNION ALL

Select '8306210000','Plated with precious metal','KGM',5,0,0 UNION ALL

Select '8306291000','Of copper or lead ','KGM',0,0,0 UNION ALL
Select '8306292000','Of nickel','KGM',15,0,0 UNION ALL
Select '8306293000','Of aluminium','KGM',20,0,0 UNION ALL
Select '8306299000','Other ','KGM',5,0,0 UNION ALL

Select '8306301000','Of copper','KGM',5,0,0 UNION ALL

Select '8306309100','Metallic mirrors reflecting traffic views at road intersections or sharp corners','KGM',20,0,0 UNION ALL
Select '8306309900','Other ','KGM',20,0,0 UNION ALL

Select '8307100000','Of iron or steel','KGM',5,0,0 UNION ALL
Select '8307900000','Of other base metal','KGM',5,0,0 UNION ALL

Select '8308100000','Hooks, eye and eyelets','KGM',0,0,0 UNION ALL
Select '8308200000','Tubular or bifurcated rivets','KGM',5,0,0 UNION ALL

Select '8308901000','Beads','KGM',0,0,0 UNION ALL
Select '8308902000','Spangles','KGM',0,0,0 UNION ALL
Select '8308909000','Other','KGM',0,0,0 UNION ALL

Select '8309100000','Crown corks','KGM',25,0,0 UNION ALL

Select '8309901000','Capsules for bottles','KGM',5,0,0 UNION ALL
Select '8309902000','Top ends of aluminium cans','KGM',5,0,0 UNION ALL
Select '8309906000','Aerosol can ends, of tinplate','KGM',5,0,0 UNION ALL
Select '8309907000',' Other caps for cans','KGM',5,0,0 UNION ALL

Select '8309908100','Bottle caps; screw caps','KGM',25,0,0 UNION ALL
Select '8309908900','Other','KGM',5,0,0 UNION ALL

Select '8309909100','Bottle caps; screw caps','KGM',25,0,0 UNION ALL
Select '8309909200','Bungs for metal drums; bung covers; seals; case corner protectors','KGM',5,0,0 UNION ALL
Select '8309909900','Other','KGM',5,0,0 UNION ALL

Select '8310000000','Signplates, nameplates, addressplates and similar plates, numbers, letters and other symbols, of base metal, excluding those of heading 94.05.','KGM',20,0,0 UNION ALL


Select '8311101000','In rolls','KGM',30,0,0 UNION ALL
Select '8311109000','Other','KGM',30,0,0 UNION ALL


Select '8311202100','In rolls','KGM',30,0,0 UNION ALL
Select '8311202900','Other','KGM',30,0,0 UNION ALL
Select '8311209000','Other','KGM',30,0,0 UNION ALL


Select '8311302100','In rolls','KGM',30,0,0 UNION ALL
Select '8311302900','Other','KGM',30,0,0 UNION ALL

Select '8311309100','In rolls','KGM',30,0,0 UNION ALL
Select '8311309900','Other','KGM',30,0,0 UNION ALL
Select '8311900000','Other','KGM',30,0,0 UNION ALL

Select '8401100000','Nuclear reactors','KGM',0,0,0 UNION ALL
Select '8401200000','Machinery and apparatus for isotopic separation, and parts thereof','KGM',0,0,0 UNION ALL
Select '8401300000','Fuel elements (cartridges), nonirradiated','KGM',0,0,0 UNION ALL
Select '8401400000','Parts of nuclear reactors','KGM',0,0,0 UNION ALL



Select '8402111000','Electrically operated','KGM',20,0,0 UNION ALL
Select '8402112000','Not electrically operated ','KGM',20,0,0 UNION ALL


Select '8402121100','Boilers with a steam production exceeding 15 t per hour','KGM',20,0,0 UNION ALL
Select '8402121900','Other ','KGM',20,0,0 UNION ALL

Select '8402122100','Boilers with a steam production exceeding 15 t per hour','KGM',20,0,0 UNION ALL
Select '8402122900','Other ','KGM',20,0,0 UNION ALL


Select '8402191100','Boilers with a steam production exceeding 15 t per hour','KGM',20,0,0 UNION ALL
Select '8402191900','Other ','KGM',20,0,0 UNION ALL

Select '8402192100','Boilers with a steam production exceeding 15 t per hour','KGM',20,0,0 UNION ALL
Select '8402192900','Other ','KGM',20,0,0 UNION ALL

Select '8402201000','Electrically operated','KGM',10,0,0 UNION ALL
Select '8402202000','Not electrically operated ','KGM',10,0,0 UNION ALL

Select '8402901000','Boiler bodies, shells or casings','KGM',5,0,0 UNION ALL
Select '8402909000','Other','KGM',5,0,0 UNION ALL

Select '8403100000','Boilers','UNT',0,0,0 UNION ALL

Select '8403901000','Boiler bodies, shells or casings','KGM',0,0,0 UNION ALL
Select '8403909000','Other','KGM',0,0,0 UNION ALL



Select '8404101100','Soot removers (soot blowers)','KGM',5,0,0 UNION ALL
Select '8404101900','Other','KGM',5,0,0 UNION ALL
Select '8404102000','For use with boilers of heading 84.03','KGM',0,0,0 UNION ALL
Select '8404200000','Condensers for steam or other vapour power units','KGM',5,0,0 UNION ALL


Select '8404901100','Bodies, shells or casings','KGM',5,0,0 UNION ALL
Select '8404901900','Other','KGM',5,0,0 UNION ALL

Select '8404902100','Bodies, shells or casings','KGM',0,0,0 UNION ALL
Select '8404902900','Other','KGM',0,0,0 UNION ALL
Select '8404909000','Other','KGM',5,0,0 UNION ALL

Select '8405100000','Producer gas or water gas generators, with or without their purifiers; acetylene gas generators and similar water process gas generators, with or without their purifiers','KGM',0,0,0 UNION ALL
Select '8405900000','Parts','KGM',0,0,0 UNION ALL

Select '8406100000','Turbines for marine propulsion','UNT',0,0,0 UNION ALL

Select '8406810000','Of an output exceeding 40 MW','UNT',0,0,0 UNION ALL

Select '8406821000','Of an output not exceeding 5 MW','UNT',0,0,0 UNION ALL
Select '8406829000','Other','UNT',0,0,0 UNION ALL
Select '8406900000','Parts','KGM',0,0,0 UNION ALL

Select '8407100000','Aircraft engines','UNT',0,0,0 UNION ALL


Select '8407211000','Of a power not exceeding 22.38 kW (30 hp)','UNT',0,0,0 UNION ALL
Select '8407219000','Other','UNT',0,0,0 UNION ALL

Select '8407292000','Of a power not exceeding 22.38 kW (30 hp)','UNT',0,0,0 UNION ALL
Select '8407299000','Other','UNT',0,0,0 UNION ALL

Select '8407310000','Of a cylinder capacity not exceeding 50 cc','UNT',25,0,0 UNION ALL


Select '8407321100','For vehicles of heading 87.01','UNT',30,0,0 UNION ALL
Select '8407321200','For vehicles of heading 87.11','UNT',30,0,0 UNION ALL
Select '8407321900','Other','UNT',30,0,0 UNION ALL

Select '8407322100','For vehicles of heading 87.01','UNT',5,0,0 UNION ALL
Select '8407322200','For vehicles of heading 87.11','UNT',5,0,0 UNION ALL
Select '8407322900','Other','UNT',5,0,0 UNION ALL

Select '8407331000','For vehicles of heading 87.01','UNT',5,0,0 UNION ALL
Select '8407332000','For vehicles of heading 87.11','UNT',5,0,0 UNION ALL
Select '8407339000','Other','UNT',5,0,0 UNION ALL


Select '8407344000','For pedestrian controlled tractors, of a cylinder capacity not exceeding 1,100 cc','UNT',5,0,0 UNION ALL
Select '8407345000','For other vehicles of heading 87.01','UNT',5,0,0 UNION ALL
Select '8407346000','For vehicles of heading 87.11','UNT',5,0,0 UNION ALL

Select '8407347100','Of a cylinder capacity not exceeding 2,000 cc','UNT',5,0,0 UNION ALL
Select '8407347200','Of a cylinder capacity exceeding 2,000 cc but not exceeding 3,000 cc','UNT',5,0,0 UNION ALL
Select '8407347300','Of a cylinder capacity exceeding 3,000 cc','UNT',5,0,0 UNION ALL

Select '8407349100','For pedestrian controlled tractors, of a cylinder capacity not exceeding 1,100 cc','UNT',5,0,0 UNION ALL
Select '8407349200','For other vehicles of heading 87.01','UNT',5,0,0 UNION ALL
Select '8407349300','For vehicles of heading 87.11','UNT',5,0,0 UNION ALL

Select '8407349400','Of a cylinder capacity not exceeding 2,000 cc','UNT',5,0,0 UNION ALL
Select '8407349500','Of a cylinder capacity exceeding 2,000 cc but not exceeding 3,000 cc','UNT',5,0,0 UNION ALL
Select '8407349900','Of a cylinder capacity exceeding 3,000 cc','UNT',5,0,0 UNION ALL

Select '8407901000','Of a power not exceeding 18.65 kW','UNT',5,0,0 UNION ALL
Select '8407902000','Of a power exceeding 18.65 kW but not exceeding 22.38 kW','UNT',5,0,0 UNION ALL
Select '8407909000','Other','UNT',5,0,0 UNION ALL


Select '8408101000','Of a power not exceeding 22.38 kW ','UNT',0,0,0 UNION ALL
Select '8408102000','Of a power exceeding 22.38 kW but not exceeding 100 kW ','UNT',0,0,0 UNION ALL
Select '8408103000','Of a power exceeding 100 kW but not exceeding 750 kW','UNT',0,0,0 UNION ALL
Select '8408109000','Other','UNT',0,0,0 UNION ALL


Select '8408201000','For vehicles of subheading 8701.10','UNT',0,0,0 UNION ALL

Select '8408202100','Of a cylinder capacity  not exceeding 2,000 cc','UNT',0,0,0 UNION ALL
Select '8408202200','Of a cylinder capacity exceeding 2,000 cc but not exceeding 3,500 cc','UNT',0,0,0 UNION ALL
Select '8408202300','Of a cylinder capacity  exceeding 3,500 cc','UNT',0,0,0 UNION ALL

Select '8408209300','For the vehicles of subheading 8701.10','UNT',0,0,0 UNION ALL

Select '8408209400','Of a cylinder capacity  not exceeding 2,000 cc','UNT',0,0,0 UNION ALL
Select '8408209500','Of a cylinder capacity  exceeding 2,000 cc but not exceeding 3,500 cc','UNT',0,0,0 UNION ALL
Select '8408209600','Of a cylinder capacity  exceeding 3,500 cc','UNT',0,0,0 UNION ALL

Select '8408901000','Of a power not exceeding 18.65 kW','UNT',0,0,0 UNION ALL

Select '8408905100','Of a kind used for machinery of heading 84.29 or 84.30','UNT',0,0,0 UNION ALL
Select '8408905200','Of a kind used for railway locomotives or tramway vehicles','UNT',0,0,0 UNION ALL
Select '8408905900','Other','UNT',0,0,0 UNION ALL

Select '8408909100','For machinery of heading 84.29 or 84.30','UNT',0,0,0 UNION ALL
Select '8408909200','Of a kind used for railway locomotives or tramway vehicles','UNT',0,0,0 UNION ALL
Select '8408909900','Other','UNT',0,0,0 UNION ALL

Select '8409100000','For aircraft engines','KGM',0,0,0 UNION ALL



Select '8409911100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409911200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409911300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409911400','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409911500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409911600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409911700','Other pistons','KGM',5,0,0 UNION ALL
Select '8409911800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409911900','Other','KGM',5,0,0 UNION ALL

Select '8409912100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409912200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409912300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409912400','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409912600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409912800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409912900','Other','KGM',5,0,0 UNION ALL

Select '8409913100','Carburettors and parts thereof','KGM',30,0,0 UNION ALL
Select '8409913200','Cylinder blocks; crank cases','KGM',30,0,0 UNION ALL
Select '8409913400','Cylinder liners','KGM',30,0,0 UNION ALL
Select '8409913500','Cylinder heads and head covers','KGM',30,0,0 UNION ALL
Select '8409913700','Pistons','KGM',30,0,0 UNION ALL
Select '8409913800','Piston rings and gudgeon pins','KGM',30,0,0 UNION ALL
Select '8409913900','Other','KGM',30,0,0 UNION ALL

Select '8409914100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409914200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409914300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409914400','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409914500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409914600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409914700','Other pistons','KGM',5,0,0 UNION ALL
Select '8409914800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409914900','Other','KGM',5,0,0 UNION ALL


Select '8409915100','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409915200','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409915300','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409915400','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409915500','Other pistons','KGM',5,0,0 UNION ALL
Select '8409915900','Other','KGM',5,0,0 UNION ALL

Select '8409916100','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409916200','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409916300','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409916400','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409916900','Other','KGM',5,0,0 UNION ALL

Select '8409917100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409917200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409917300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409917400','Other cylinder liners','KGM',5,0,0 UNION ALL
Select '8409917600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409917700','Other pistons','KGM',5,0,0 UNION ALL
Select '8409917800','Pistons rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409917900','Other','KGM',5,0,0 UNION ALL


Select '8409991100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409991200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409991300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409991400','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409991500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409991600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409991700','Other pistons','KGM',0,0,0 UNION ALL
Select '8409991800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409991900','Other','KGM',5,0,0 UNION ALL

Select '8409992100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409992200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409992300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409992400','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409992500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409992600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409992700','Other pistons','KGM',0,0,0 UNION ALL
Select '8409992800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409992900','Other','KGM',5,0,0 UNION ALL

Select '8409993100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409993200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '84099933','Cylinder liners:','',0,0,0 UNION ALL
Select '8409993310','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm ','KGM',30,0,0 UNION ALL
Select '8409993390','Other','KGM',0,0,0 UNION ALL
Select '8409993400','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '84099935','Pistons:','',0,0,0 UNION ALL
Select '8409993510','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm ','KGM',30,0,0 UNION ALL
Select '8409993590','Other','KGM',0,0,0 UNION ALL
Select '8409993600','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409993900','Other','KGM',5,0,0 UNION ALL

Select '8409994100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409994200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409994300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409994400','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409994500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409994600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409994700','Other pistons','KGM',0,0,0 UNION ALL
Select '8409994800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409994900','Other','KGM',5,0,0 UNION ALL


Select '8409995100','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409995200','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409995300','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409995400','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409995500','Other pistons','KGM',0,0,0 UNION ALL
Select '8409995900','Other','KGM',5,0,0 UNION ALL

Select '8409996100','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409996200','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409996300','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409996400','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409996500','Other pistons','KGM',0,0,0 UNION ALL
Select '8409996900','Other','KGM',5,0,0 UNION ALL

Select '8409997100','Carburettors and parts thereof','KGM',5,0,0 UNION ALL
Select '8409997200','Cylinder blocks; crank cases','KGM',0,0,0 UNION ALL
Select '8409997300','Cylinder liners, with an internal diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409997400','Other cylinder liners','KGM',0,0,0 UNION ALL
Select '8409997500','Cylinder heads and head covers','KGM',5,0,0 UNION ALL
Select '8409997600','Pistons, with an external diameter of 50 mm or more, but not exceeding 155 mm','KGM',30,0,0 UNION ALL
Select '8409997700','Other pistons','KGM',0,0,0 UNION ALL
Select '8409997800','Piston rings and gudgeon pins','KGM',5,0,0 UNION ALL
Select '8409997900','Other','KGM',5,0,0 UNION ALL


Select '8410110000','Of a power not exceeding 1,000 kW','UNT',0,0,0 UNION ALL
Select '8410120000','Of a power exceeding 1,000 kW but not exceeding 10,000 kW                 ','UNT',0,0,0 UNION ALL
Select '8410130000','Of a power exceeding 10,000 kW','UNT',0,0,0 UNION ALL
Select '8410900000','Parts, including regulators','KGM',0,0,0 UNION ALL


Select '8411110000','Of a thrust not exceeding 25 kN','UNT',0,0,0 UNION ALL
Select '8411120000','Of a thrust exceeding 25 kN','UNT',0,0,0 UNION ALL

Select '8411210000','Of a power not exceeding 1,100 kW','UNT',0,0,0 UNION ALL
Select '8411220000','Of a power exceeding 1,100 kW','UNT',0,0,0 UNION ALL

Select '8411810000','Of a power not exceeding 5,000 kW','UNT',0,0,0 UNION ALL
Select '8411820000','Of a power exceeding 5,000 kW','UNT',0,0,0 UNION ALL

Select '8411910000','Of turbojets or turbopropellers','KGM',0,0,0 UNION ALL
Select '8411990000','Other','KGM',0,0,0 UNION ALL

Select '8412100000','Reaction engines other than turbojets','UNT',0,0,0 UNION ALL

Select '8412210000','Linear acting (cylinders)','UNT',0,0,0 UNION ALL
Select '8412290000','Other','UNT',0,0,0 UNION ALL

Select '8412310000','Linear acting (cylinders)','UNT',0,0,0 UNION ALL
Select '8412390000','Other','UNT',0,0,0 UNION ALL
Select '8412800000','Other','UNT',0,0,0 UNION ALL

Select '8412901000','Of engines of subheading 8412.10','KGM',0,0,0 UNION ALL
Select '8412902000','For steam or other vapour power units incorporating boilers ','KGM',0,0,0 UNION ALL
Select '8412909000','Other','KGM',0,0,0 UNION ALL


Select '8413110000','Pumps for dispensing fuel or lubricants, of the type used in fillingstations or in garages','UNT',0,0,0 UNION ALL

Select '8413191000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8413192000','Not electrically operated','UNT',5,0,0 UNION ALL

Select '8413201000','Water pumps','UNT',5,0,0 UNION ALL
Select '8413202000','Breast pump','UNT',0,0,0 UNION ALL
Select '8413209000','Other','UNT',5,0,0 UNION ALL

Select '8413303000','Fuel pumps of a kind used for engines of motor vehicles of heading 87.02, 87.03 or 87.04','UNT',0,0,0 UNION ALL
Select '8413304000','Water pumps of a kind used for engines of motor vehicles of heading 87.02, 87.03 or 87.04','UNT',25,0,0 UNION ALL

Select '8413305100','With an inlet diameter not exceeding 200 mm','UNT',25,0,0 UNION ALL
Select '8413305200','With an inlet diameter exceeding 200 mm','UNT',20,0,0 UNION ALL
Select '8413309000','Other','UNT',0,0,0 UNION ALL
Select '8413400000','Concrete pumps','UNT',0,0,0 UNION ALL


Select '8413503100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8413503200','Not electrically operated','UNT',0,0,0 UNION ALL
Select '8413504000','Water pumps, with a flow rate exceeding 8,000 m3/h but not exceeding 13,000 m3/h','UNT',0,0,0 UNION ALL
Select '8413509000','Other','UNT',0,0,0 UNION ALL


Select '8413603100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8413603200','Not electrically operated','UNT',0,0,0 UNION ALL
Select '8413604000','Water pumps, with a flow rate exceeding 8,000 m3/h but not exceeding 13,000 m3/h','UNT',0,0,0 UNION ALL
Select '8413609000','Other','UNT',0,0,0 UNION ALL


Select '8413701100','With an inlet diameter not exceeding 200 mm','UNT',20,0,0 UNION ALL
Select '8413701900','Other','UNT',10,0,0 UNION ALL

Select '8413703100','With an inlet diameter not exceeding 200 mm','UNT',20,0,0 UNION ALL
Select '8413703900','Other','UNT',10,0,0 UNION ALL

Select '8413704200','With inlet diameter not exceeding 200 mm, electrically operated','UNT',20,0,0 UNION ALL
Select '8413704300','With inlet diameter not exceeding 200 mm, not electrically operated','UNT',20,0,0 UNION ALL
Select '8413704900','Other','UNT',10,0,0 UNION ALL

Select '8413705100','With an inlet diameter not exceeding 200 mm','UNT',20,0,0 UNION ALL
Select '8413705900','Other','UNT',10,0,0 UNION ALL

Select '8413709100','With an inlet diameter not exceeding 200 mm','UNT',20,0,0 UNION ALL
Select '8413709900','Other','UNT',10,0,0 UNION ALL


Select '8413811300','Water pumps with a flow rate not exceeding 8,000 m3/h , electrically operated','UNT',0,0,0 UNION ALL
Select '8413811400','Water pumps with a flow rate not exceeding 8,000 m3/h, not electrically operated','UNT',0,0,0 UNION ALL
Select '8413811500','Water pumps, with a flow rate exceeding 8,000 m3/h but not exceeding 13,000 m3/h','UNT',0,0,0 UNION ALL
Select '8413811900','Other','UNT',0,0,0 UNION ALL

Select '8413821000','Electrically operated','UNT',5,0,0 UNION ALL
Select '8413822000','Not electrically operated','UNT',5,0,0 UNION ALL


Select '8413911000','Of pumps of subheading 8413.20.10','KGM',5,0,0 UNION ALL
Select '8413912000','Of pumps of subheading 8413.20.20 and 8413.20.90','KGM',5,0,0 UNION ALL
Select '8413913000','Of pumps of subheadings 8413.70.11 and 8413.70.19','KGM',5,0,0 UNION ALL
Select '8413914000','Of other centrifugal pumps','KGM',5,0,0 UNION ALL
Select '8413919000','Of other pumps','KGM',5,0,0 UNION ALL
Select '8413920000','Of liquid elevators','KGM',5,0,0 UNION ALL

Select '8414100000','Vacuum pumps','UNT',0,0,0 UNION ALL

Select '8414201000','Bicycle pumps','UNT',5,0,0 UNION ALL
Select '8414209000','Other','UNT',0,0,0 UNION ALL

Select '8414304000','With a refrigeration capacity exceeding 21.10 kW, or with a displacement per revolution of 220 cc or more','UNT',0,0,0 UNION ALL
Select '8414309000','Other','UNT',0,0,0 UNION ALL
Select '8414400000','Air compressors mounted on a wheeled chassis for towing','UNT',0,0,0 UNION ALL


Select '8414511000','Table fans and box fans','UNT',20,0,0 UNION ALL

Select '8414519100','With protective screen','UNT',30,0,0 UNION ALL
Select '8414519900','Other','UNT',20,0,0 UNION ALL


Select '8414592000','Explosionproof air fans, of a kind used in underground mining','UNT',20,0,0 UNION ALL
Select '8414593000','Blowers','UNT',20,0,0 UNION ALL

Select '8414594100','With protective screen','UNT',30,0,0 UNION ALL
Select '84145949','Other:','',0,0,0 UNION ALL
Select '8414594910','Fans of a kind used solely or principlally for cooling microprocessors, telecomunication apparatus, automatic data processing machines or units of automatic data processing machines.','UNT',20,0,0 UNION ALL
Select '8414594990','Other','UNT',20,0,0 UNION ALL

Select '8414595000','Blowers','UNT',20,0,0 UNION ALL

Select '8414599100','With protective screen','UNT',30,0,0 UNION ALL
Select '8414599200','Explosionproof air fans, of a kind used in underground mining','UNT',20,0,0 UNION ALL
Select '8414599900','Other','UNT',20,0,0 UNION ALL


Select '8414601100','Laminar airflow cabinets','UNT',0,0,0 UNION ALL
Select '8414601900','Other','UNT',0,0,0 UNION ALL

Select '8414609100','Suitable for industrial use','UNT',20,0,0 UNION ALL
Select '8414609900','Other','UNT',35,0,0 UNION ALL



Select '8414801100','Laminar airflow cabinets','UNT',0,0,0 UNION ALL
Select '8414801900','Other','UNT',0,0,0 UNION ALL

Select '8414802100','Suitable for industrial use','UNT',20,0,0 UNION ALL
Select '8414802900','Other','UNT',30,0,0 UNION ALL
Select '8414803000','Freepiston generators for gas turbines','UNT',5,0,0 UNION ALL

Select '8414804100','Gas compression modules suitable for use in oil drilling operations','UNT',25,0,0 UNION ALL
Select '8414804200','Of a kind used for automotive air conditioners','UNT',60.00,0,0 UNION ALL
Select '8414804300','Sealed units for air conditioning machines','UNT',5,0,0 UNION ALL
Select '8414804900','Other','UNT',0,0,0 UNION ALL
Select '8414805000','Air pumps','UNT',0,0,0 UNION ALL
Select '8414809000','Other','UNT',0,0,0 UNION ALL


Select '8414902100','Of goods of heading 84.15, 84.18, 85.09 or 85.16','KGM',30,0,0 UNION ALL
Select '8414902200','Of blowers','KGM',20,0,0 UNION ALL
Select '8414902900','Other','KGM',20,0,0 UNION ALL

Select '8414903100','Of goods of subheading 8414.60','KGM',0,0,0 UNION ALL
Select '8414903200','Of goods of subheading 8414.80','KGM',0,0,0 UNION ALL

Select '8414904100','For electrically operated machines','KGM',5,0,0 UNION ALL
Select '8414904200','For nonelectrically operated machines','KGM',5,0,0 UNION ALL
Select '8414905000','Of goods of subheading 8414.20','KGM',5,0,0 UNION ALL
Select '8414906000','Of goods of subheading 8414.30','KGM',5,0,0 UNION ALL

Select '8414907100','For electrically operated machines','KGM',5,0,0 UNION ALL
Select '8414907200','For nonelectrically operated machines','KGM',5,0,0 UNION ALL

Select '8414909100','For electrically operated machines','KGM',5,0,0 UNION ALL
Select '8414909200','For nonelectrically operated machines','KGM',5,0,0 UNION ALL


Select '8415101000','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415109000','Other','UNT',30,0,0 UNION ALL

Select '8415201000','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415209000','Other','UNT',30,0,0 UNION ALL



Select '8415811100','Of a cooling capacity  not exceeding 21.10 kW','UNT',0,0,0 UNION ALL
Select '8415811200','Of a cooling capacity exceeding 21.10 kW and with an air flow rate of each evaporator unit exceeding 67.96 m3/min','UNT',0,0,0 UNION ALL
Select '8415811900','Other','UNT',0,0,0 UNION ALL

Select '8415812100','Of a cooling capacity not exceeding 26.38 kW','UNT',0,0,0 UNION ALL
Select '8415812900','Other','UNT',0,0,0 UNION ALL

Select '8415813100','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415813900','Other','UNT',30,0,0 UNION ALL

Select '8415819100','Of a cooling capacity exceeding 21.10 kW and with an air flow rate of each evaporator unit exceeding 67.96 m3/min','UNT',30,0,0 UNION ALL

Select '8415819300','Of a cooling capacity not exceeding 21.10 kW','UNT',30,0,0 UNION ALL
Select '8415819400','Of a cooling capacity exceeding 21.10 kW but not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415819900','Other','UNT',30,0,0 UNION ALL


Select '8415821100','Of a cooling capacity exceeding 21.10 kW and with an air flow rate of each evaporator unit exceeding 67.96 m3/min','UNT',0,0,0 UNION ALL
Select '8415821900','Other','UNT',0,0,0 UNION ALL

Select '8415822100','Of a cooling capacity not exceeding 26.38 kW','UNT',0,0,0 UNION ALL
Select '8415822900','Other','UNT',0,0,0 UNION ALL

Select '8415823100','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415823900','Other','UNT',30,0,0 UNION ALL

Select '8415829100','Of an output not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415829900','Other','UNT',30,0,0 UNION ALL


Select '8415831100','Of a cooling capacity exceeding 21.10 kW and with an air flow rate of each evaporator unit exceeding 67.96 m3/min','UNT',0,0,0 UNION ALL
Select '8415831900','Other','UNT',0,0,0 UNION ALL

Select '8415832100','Of a cooling capacity not exceeding 26.38 kW','UNT',0,0,0 UNION ALL
Select '8415832900','Other','UNT',0,0,0 UNION ALL

Select '8415833100','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415833900','Other','UNT',30,0,0 UNION ALL

Select '8415839100','Of a cooling capacity not exceeding 26.38 kW','UNT',30,0,0 UNION ALL
Select '8415839900','Other','UNT',30,0,0 UNION ALL


Select '8415901300','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415901400','Evaporators or condensers for airconditioning machines for motor vehicles','KGM',30,0,0 UNION ALL
Select '8415901500','Chassis, welded and painted, other than of subheading 8415.90.13 ','KGM',30,0,0 UNION ALL
Select '8415901900','Other','KGM',30,0,0 UNION ALL


Select '8415902400','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415902500','Other','KGM',30,0,0 UNION ALL

Select '8415902600','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415902900','Other','KGM',30,0,0 UNION ALL


Select '8415903400','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415903500','Other','KGM',30,0,0 UNION ALL

Select '8415903600','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415903900','Other','KGM',30,0,0 UNION ALL


Select '8415904400','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415904500','Other','KGM',30,0,0 UNION ALL

Select '8415904600','Of a kind used in aircraft or railway rolling stock','KGM',0,0,0 UNION ALL
Select '8415904900','Other','KGM',30,0,0 UNION ALL

Select '8416100000','Furnace burners for liquid fuel','KGM',0,0,0 UNION ALL
Select '8416200000','Other furnace burners, including combination burners','KGM',0,0,0 UNION ALL
Select '8416300000','Mechanical stokers, including their mechanical grates, mechanical ash dischargers and similar appliances','KGM',0,0,0 UNION ALL
Select '8416900000','Parts ','KGM',0,0,0 UNION ALL

Select '8417100000','Furnaces and ovens for the roasting, melting or other heattreatment of ores, pyrites or of metals','UNT',0,0,0 UNION ALL
Select '8417200000','Bakery ovens, including biscuit ovens','UNT',0,0,0 UNION ALL
Select '8417800000','Other','UNT',0,0,0 UNION ALL
Select '8417900000','Parts','KGM',0,0,0 UNION ALL



Select '8418101100','Of a capacity not exceeding 230 l','UNT',30,0,0 UNION ALL
Select '8418101900','Other','UNT',30,0,0 UNION ALL
Select '8418102000','Other, of a capacity not exceeding 350 l','UNT',30,0,0 UNION ALL
Select '8418109000','Other','UNT',30,0,0 UNION ALL


Select '8418211000','Of a capacity not exceeding 230 l','UNT',30,0,0 UNION ALL
Select '8418219000','Other','UNT',30,0,0 UNION ALL
Select '8418290000','Other','UNT',30,0,0 UNION ALL

Select '8418301000','Of a capacity not exceeding 200 l ','UNT',30,0,0 UNION ALL
Select '8418309000','Other','UNT',30,0,0 UNION ALL

Select '8418401000','Of a capacity not exceeding 200 l ','UNT',30,0,0 UNION ALL
Select '8418409000','Other','UNT',30,0,0 UNION ALL


Select '8418501100','Of a kind suitable for medical, surgical or laboratory use','UNT',30,0,0 UNION ALL
Select '8418501900','Other','UNT',30,0,0 UNION ALL

Select '8418509100','Of a kind suitable for medical, surgical or laboratory use','UNT',30,0,0 UNION ALL
Select '8418509900','Other','UNT',30,0,0 UNION ALL

Select '8418610000','Heat pumps other than air conditioning machines of heading 84.15','UNT',30,0,0 UNION ALL

Select '8418691000','Beverage coolers','KGM',30,0,0 UNION ALL
Select '8418693000','Cold water dispenser','KGM',30,0,0 UNION ALL

Select '8418694100','For air conditioning machines','KGM',30,0,0 UNION ALL
Select '8418694900','Other','KGM',30,0,0 UNION ALL
Select '8418695000','Scale icemaker units ','KGM',30,0,0 UNION ALL
Select '8418699000','Other','KGM',30,0,0 UNION ALL

Select '8418910000','Furniture designed to receive refrigerating or freezing equipment','KGM',30,0,0 UNION ALL

Select '8418991000','Evaporators or condensers','KGM',30,0,0 UNION ALL
Select '8418994000','Aluminium rollbonded panels of a kind used for the goods of subheading 8418.10.11, 8418.10.19, 8418.21.10, 8418.21.90 or 8418.29.00','KGM',30,0,0 UNION ALL
Select '8418999000','Other','KGM',30,0,0 UNION ALL



Select '8419111000','Household type','UNT',30,0,0 UNION ALL
Select '8419119000','Other ','UNT',0,0,0 UNION ALL

Select '8419191000','Household type','UNT',5,0,0 UNION ALL
Select '8419199000','Other ','UNT',0,0,0 UNION ALL
Select '8419200000','Medical, surgical or laboratory sterilisers','UNT',0,0,0 UNION ALL


Select '8419313000','Evaporators','UNT',0,0,0 UNION ALL
Select '8419314000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8419319000','Other','UNT',0,0,0 UNION ALL

Select '8419321000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8419322000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8419391100','Machinery for the treatment of materials by a process involving heating, for the manufacture of printed circuit boards, printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '8419391900','Other ','UNT',0,0,0 UNION ALL
Select '8419392000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8419401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8419402000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8419501000','Cooling towers','UNT',20,0,0 UNION ALL

Select '8419509100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8419509200','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8419601000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8419602000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8419811000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8419812000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8419891300','Machinery for the treatment of material by a process involving heating, for the manufacture of printed circuit boards, printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '8419891900','Other ','UNT',0,0,0 UNION ALL
Select '8419892000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8419901200','Parts of machinery for the treatment of materials by a process involving heating, for the manufacture of printed circuit boards, printed wiring boards or printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '8419901300','Casings for cooling towers','KGM',5,0,0 UNION ALL
Select '8419901900','Other','KGM',0,0,0 UNION ALL

Select '8419902200','Of instantaneous gas water heaters, household type','KGM',5,0,0 UNION ALL
Select '8419902900','Other ','KGM',0,0,0 UNION ALL


Select '8420101000','Apparatus for the application of dry film or liquid photo resist, photosensitive layers, soldering pastes, solder or adhesive materials on printed circuit boards or printed wiring boards or their components','UNT',0,0,0 UNION ALL
Select '8420102000','Ironing machines or wringers suitable for domestic use','UNT',0,0,0 UNION ALL
Select '8420109000','Other','UNT',0,0,0 UNION ALL


Select '8420911000','Of goods of subheading 8420.10.10','KGM',0,0,0 UNION ALL
Select '8420919000','Other','KGM',0,0,0 UNION ALL

Select '8420991000','Of goods of subheading 8420.10.10','KGM',0,0,0 UNION ALL
Select '8420999000','Other','KGM',0,0,0 UNION ALL


Select '8421110000','Cream separators ','UNT',0,0,0 UNION ALL
Select '8421120000','Clothesdryers','UNT',0,0,0 UNION ALL

Select '8421191000','Of a kind used for sugar manufacture','UNT',0,0,0 UNION ALL
Select '8421199000','Other','UNT',0,0,0 UNION ALL



Select '8421211100','Filtering machinery and apparatus for domestic use','UNT',0,0,0 UNION ALL
Select '8421211900','Other ','UNT',0,0,0 UNION ALL

Select '8421212200','Electrically operated','UNT',0,0,0 UNION ALL
Select '8421212300','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8421223000','Electrically operated, of a capacity exceeding 500 l/h','UNT',0,0,0 UNION ALL
Select '8421229000','Other','UNT',0,0,0 UNION ALL


Select '8421231100','Oil filters ','UNT',25,0,0 UNION ALL
Select '8421231900','Other ','UNT',0,0,0 UNION ALL

Select '8421232100','Oil filters ','UNT',25,0,0 UNION ALL
Select '8421232900','Other ','UNT',0,0,0 UNION ALL

Select '8421239100','Oil filters ','UNT',25,0,0 UNION ALL
Select '8421239900','Other ','UNT',0,0,0 UNION ALL

Select '8421291000','Of a kind suitable for medical, surgical or laboratory use','UNT',0,0,0 UNION ALL
Select '8421292000','Of a kind used for sugar manufacture','UNT',0,0,0 UNION ALL
Select '8421293000','Of a kind used in oil drilling operations','UNT',5,0,0 UNION ALL
Select '8421294000','Other, petrol filters','UNT',0,0,0 UNION ALL
Select '8421295000','Other, oil filters','UNT',5,0,0 UNION ALL
Select '8421299000','Other','UNT',0,0,0 UNION ALL


Select '8421311000','For machinery of heading 84.29 or 84.30','UNT',25,0,0 UNION ALL
Select '8421312000','For motor vehicles of Chapter 87','UNT',25,0,0 UNION ALL
Select '8421319000','Other ','UNT',25,0,0 UNION ALL

Select '8421392000','Air purifiers','UNT',5,0,0 UNION ALL
Select '8421399000','Other','UNT',5,0,0 UNION ALL


Select '8421911000','Of goods of subheading 8421.12.00 ','KGM',0,0,0 UNION ALL
Select '8421912000','Of goods of subheading 8421.19.10','KGM',0,0,0 UNION ALL
Select '8421919000','Of goods of subheading 8421.11.00 or 8421.19.90','KGM',0,0,0 UNION ALL


Select '8421992100','For subheading 8421.23.11 or 8421.23.21  ','KGM',5,0,0 UNION ALL
Select '8421992900','Other','KGM',0,0,0 UNION ALL
Select '8421993000','Of goods of subheading 8421.31','KGM',5,0,0 UNION ALL

Select '8421999100','Of goods of subheading 8421.29.20','KGM',0,0,0 UNION ALL
Select '8421999400','Of goods of subheading 8421.21.11','KGM',0,0,0 UNION ALL
Select '8421999600','Of goods of subheading 8421.23.11 or 8421.23.91','KGM',5,0,0 UNION ALL
Select '8421999700','Of goods of subheading 8421.23.19 or 8421.23.99','KGM',0,0,0 UNION ALL
Select '8421999800','Of goods of subheading 8421.29.50 or  8421.39.90','KGM',5,0,0 UNION ALL
Select '8421999900','Other','KGM',0,0,0 UNION ALL


Select '84221100','Of the household type:','',0,0,0 UNION ALL
Select '8422110010','Electrically operated','UNT',20,0,0 UNION ALL
Select '8422110020','Not electrically operated','UNT',5,0,0 UNION ALL
Select '8422190000','Other','UNT',0,0,0 UNION ALL
Select '8422200000','Machinery for cleaning or drying bottles or other containers','UNT',0,0,0 UNION ALL
Select '8422300000','Machinery for filling, closing, sealing or labelling bottles, cans, boxes, bags or other containers; machinery for capsuling bottles, jars, tubes and similar containers; machinery for aerating beverages','UNT',0,0,0 UNION ALL
Select '8422400000','Other packing or wrapping machinery (including heatshrink wrapping machinery)','UNT',0,0,0 UNION ALL

Select '84229010','Of machines of subheading 8422.11:','',0,0,0 UNION ALL
Select '8422901010','For subheading 8422.11.00 10','KGM',20,0,0 UNION ALL
Select '8422901020','For subheading 8422.11.00 20 ','KGM',0,0,0 UNION ALL
Select '8422909000','Other','KGM',0,0,0 UNION ALL


Select '8423101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8423102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8423201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8423202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8423301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8423302000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8423811000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8423812000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8423821100','Having a maximum weighing capacity not exceeding 1,000 kg','UNT',0,0,0 UNION ALL
Select '8423821900','Other','UNT',0,0,0 UNION ALL

Select '8423822100','Having a maximum weighing capacity not exceeding 1,000 kg','UNT',0,0,0 UNION ALL
Select '8423822900','Other','UNT',0,0,0 UNION ALL

Select '8423891000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8423892000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8423901000','Weighing machine weights','KGM',0,0,0 UNION ALL

Select '8423902100','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8423902900','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8424101000','Of a kind suitable for aircraft use','UNT',25,0,0 UNION ALL
Select '8424109000','Other','UNT',25,0,0 UNION ALL


Select '8424201100','Agricultural or horticultural','UNT',0,0,0 UNION ALL
Select '8424201900','Other ','UNT',0,0,0 UNION ALL

Select '8424202100','Agricultural or horticultural','UNT',0,0,0 UNION ALL
Select '8424202900','Other ','UNT',0,0,0 UNION ALL
Select '8424300000','Steam or sand blasting machines and similar jet projecting machines','UNT',0,0,0 UNION ALL


Select '8424411000','Handoperated insecticide sprayers','UNT',0,0,0 UNION ALL
Select '8424412000','Other, not electrically operated','UNT',0,0,0 UNION ALL
Select '8424419000','Other','UNT',0,0,0 UNION ALL

Select '8424491000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8424492000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8424821000','Drip irrigation systems','UNT',0,0,0 UNION ALL
Select '8424822000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8424823000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '8424891000','Handoperated household sprayers of a capacity not exceeding 3 l','UNT',0,0,0 UNION ALL
Select '8424892000','Spray heads with dip tubes','UNT',0,0,0 UNION ALL
Select '8424894000','Wet processing equipment, by projecting, dispersing or spraying, of chemical or electrochemical solutions for the application on printed circuit boards or printed wiring boards substrates; apparatus for the spot application of liquids, soldering pastes, solder ball, adhesives or sealant to printed circuit boards or printed wiring boards or their components; apparatus for the application of dry film or liquid photo resist, photo sensitive layers, soldering pastes, solder or adhesive materials on printed circuit boards or printed wiring boards substrates or their components','UNT',0,0,0 UNION ALL
Select '8424895000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8424899000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '8424901000','Of fire extinguishers','KGM',0,0,0 UNION ALL


Select '8424902100','Of goods of subheading 8424.20.11','KGM',0,0,0 UNION ALL
Select '8424902300','Other ','KGM',0,0,0 UNION ALL

Select '8424902400','Of goods of subheading 8424.20.21','KGM',0,0,0 UNION ALL
Select '8424902900','Other ','KGM',0,0,0 UNION ALL
Select '8424903000','Of  steam  or  sand  blasting  machines  and  similar  jet projecting machines','KGM',0,0,0 UNION ALL

Select '8424909300','Of goods of subheading 8424.82.10','KGM',0,0,0 UNION ALL
Select '8424909400','Of goods of subheading 8424.41.10, 8424.41.20, 8424.49.20  or 8424.82.30','KGM',0,0,0 UNION ALL
Select '8424909500','Of goods of subheading 8424.41.90 8424.49.10 or 8424.82.20','KGM',0,0,0 UNION ALL
Select '8424909900','Other','KGM',0,0,0 UNION ALL


Select '8425110000','Powered by electric motor','UNT',5,0,0 UNION ALL
Select '8425190000','Other ','UNT',5,0,0 UNION ALL

Select '8425310000','Powered by electric motor ','UNT',5,0,0 UNION ALL
Select '8425390000','Other ','UNT',5,0,0 UNION ALL

Select '8425410000','Builtin jacking systems of a type used in garages','UNT',5,0,0 UNION ALL

Select '8425421000','Jacks of a kind used in tipping mechanisms for lorries','UNT',5,0,0 UNION ALL
Select '8425429000','Other ','UNT',5,0,0 UNION ALL

Select '8425491000','Electrically operated','UNT',5,0,0 UNION ALL
Select '8425492000','Not electrically operated','UNT',5,0,0 UNION ALL


Select '8426110000','Overhead travelling cranes on fixed support','UNT',20,0,0 UNION ALL
Select '8426120000','Mobile lifting frames on tyres and straddle carriers','UNT',5,0,0 UNION ALL

Select '8426192000','Bridge cranes','UNT',20,0,0 UNION ALL
Select '8426193000','Gantry cranes','UNT',5,0,0 UNION ALL
Select '8426199000','Other','UNT',5,0,0 UNION ALL
Select '8426200000','Tower cranes','UNT',5,0,0 UNION ALL
Select '8426300000','Portal or pedestal jib cranes','UNT',0,0,0 UNION ALL

Select '8426410000','On tyres','UNT',5,0,0 UNION ALL
Select '8426490000','Other','UNT',5,0,0 UNION ALL

Select '8426910000','Designed for mounting on road vehicles','UNT',5,0,0 UNION ALL
Select '8426990000','Other','UNT',5,0,0 UNION ALL

Select '8427100000','Selfpropelled trucks powered by an electric motor','UNT',5,0,0 UNION ALL
Select '8427200000','Other selfpropelled trucks','UNT',5,0,0 UNION ALL
Select '8427900000','Other trucks','UNT',5,0,0 UNION ALL



Select '8428103100','For passengers','UNT',5,0,0 UNION ALL
Select '8428103900','Other','UNT',5,0,0 UNION ALL
Select '8428104000','Skip hoists','UNT',5,0,0 UNION ALL

Select '8428201000','Of a kind used for agriculture','UNT',5,0,0 UNION ALL
Select '8428209000','Other ','UNT',5,0,0 UNION ALL

Select '8428310000','Specially designed for underground use','UNT',5,0,0 UNION ALL

Select '8428321000','Of a kind used for agriculture','UNT',5,0,0 UNION ALL
Select '8428329000','Other ','UNT',5,0,0 UNION ALL

Select '8428331000','Of a kind used for agriculture','UNT',5,0,0 UNION ALL
Select '8428339000','Other ','UNT',5,0,0 UNION ALL

Select '8428391000','Of a kind used for agriculture','UNT',5,0,0 UNION ALL
Select '8428399000','Other ','UNT',5,0,0 UNION ALL
Select '8428400000','Escalators and moving walkways','UNT',5,0,0 UNION ALL
Select '8428600000','Teleferics, chairlifts, skidraglines; traction mechanisms for  funiculars','UNT',5,0,0 UNION ALL

Select '8428902000','Automated machines for the transport, handling and storage of printed circuit boards, printed wiring boards or printed circuit assemblies ','UNT',0,0,0 UNION ALL
Select '8428903000','Mine wagon pusher, locomotive or wagon traversers, wagon tippers and similar railway wagon handling equipment','UNT',5,0,0 UNION ALL
Select '8428909000','Other','UNT',0,0,0 UNION ALL


Select '8429110000','Track laying','UNT',20,0,0 UNION ALL
Select '8429190000','Other','UNT',20,0,0 UNION ALL
Select '8429200000','Graders and levellers','UNT',20,0,0 UNION ALL
Select '8429300000','Scrapers','UNT',5,0,0 UNION ALL

Select '8429403000','Tamping machines ','UNT',5,0,0 UNION ALL
Select '8429404000','Vibratory smooth drum rollers, with a centrifugal force drum not exceeding 20 t by weight','UNT',25,0,0 UNION ALL
Select '8429405000','Other vibratory road rollers','UNT',25,0,0 UNION ALL
Select '8429409000','Other ','UNT',5,0,0 UNION ALL

Select '8429510000','Frontend shovel loaders','UNT',5,0,0 UNION ALL
Select '8429520000','Machinery with a 360o revolving superstructure','UNT',5,0,0 UNION ALL
Select '8429590000','Other','UNT',10,0,0 UNION ALL

Select '8430100000','Piledrivers and pileextractors','UNT',20,0,0 UNION ALL
Select '8430200000','Snowploughs and snowblowers','UNT',0,0,0 UNION ALL

Select '8430310000','Selfpropelled','UNT',20,0,0 UNION ALL
Select '8430390000','Other','UNT',20,0,0 UNION ALL

Select '8430410000','Selfpropelled','UNT',5,0,0 UNION ALL

Select '8430491000','Wellhead platforms and integrated production modules suitable for use in drilling operations','UNT',20,0,0 UNION ALL
Select '8430499000','Other ','UNT',5,0,0 UNION ALL
Select '8430500000','Other machinery, selfpropelled','UNT',20,0,0 UNION ALL

Select '8430610000','Tamping or compacting machinery','UNT',10,0,0 UNION ALL
Select '8430690000','Other','UNT',20,0,0 UNION ALL



Select '8431101300','Of goods of subheading 8425.11.00, 8425.31.00 or 8425.49.10  ','KGM',5,0,0 UNION ALL
Select '8431101900','Other ','KGM',5,0,0 UNION ALL

Select '8431102200','Of goods of subheading 8425.19.00, 8425.39.00, 8425.41.00, 8425.42.10 or 8425.42.90','KGM',5,0,0 UNION ALL
Select '8431102900','Other ','KGM',5,0,0 UNION ALL

Select '8431201000','Of subheading 8427.10 or 8427.20','KGM',5,0,0 UNION ALL
Select '8431209000','Other','KGM',5,0,0 UNION ALL


Select '8431311000','Of goods of subheading 8428.10.39 or 8428.10.40','KGM',5,0,0 UNION ALL
Select '8431312000','Of goods of subheading 8428.10.31 or 8428.40.00','KGM',5,0,0 UNION ALL

Select '8431391000','Of goods of subheading 8428.20.10, 8428.32.10, 8428.33.10 or 8428.39.10','KGM',0,0,0 UNION ALL
Select '8431394000','Of automated machines for the transport, handling and strorage of printed circuit boards, printed wiring boards or printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '8431395000','Other, of goods of subheading 8428.90','KGM',0,0,0 UNION ALL
Select '8431399000','Other ','KGM',0,0,0 UNION ALL


Select '8431411000','For machinery of heading 84.26','KGM',5,0,0 UNION ALL
Select '8431419000','Other','KGM',10,0,0 UNION ALL
Select '8431420000','Bulldozer or angledozer blades','KGM',10,0,0 UNION ALL
Select '8431430000','Parts for boring or sinking machinery of subheading 8430.41 or 8430.49','KGM',5,0,0 UNION ALL

Select '8431491000','Parts of machinery of heading 84.26','KGM',5,0,0 UNION ALL
Select '8431492000','Cutting edges or end bits of a kind used for scrapers, graders or levellers','KGM',10,0,0 UNION ALL
Select '8431494000','Cutting edges or end bits of a kind used for bulldozer or angledozer blades','KGM',10,0,0 UNION ALL
Select '8431495000','Of road rollers','KGM',5,0,0 UNION ALL
Select '8431496000','Of goods of subheading 8430.20.00','KGM',5,0,0 UNION ALL
Select '8431499000','Other','KGM',10,0,0 UNION ALL

Select '8432100000','Ploughs','UNT',0,0,0 UNION ALL

Select '8432210000','Disc harrows','UNT',0,0,0 UNION ALL
Select '8432290000','Other','UNT',0,0,0 UNION ALL

Select '8432310000','Notill direct seeders, planters and transplanters','UNT',0,0,0 UNION ALL
Select '8432390000','Other','UNT',0,0,0 UNION ALL

Select '8432410000','Manure spreaders','UNT',0,0,0 UNION ALL
Select '8432420000','Fertiliser distributors','UNT',0,0,0 UNION ALL

Select '8432801000','Agricultural or horticultural ','UNT',0,0,0 UNION ALL
Select '8432802000','Lawn or sportsground rollers','UNT',0,0,0 UNION ALL
Select '8432809000','Other','UNT',0,0,0 UNION ALL

Select '8432901000','Of machinery of subheading 8432.80.90','KGM',0,0,0 UNION ALL
Select '8432902000','Of lawn or sportsground rollers','KGM',0,0,0 UNION ALL
Select '8432909000','Other','KGM',0,0,0 UNION ALL


Select '8433110000','Powered, with the cutting device rotating in a horizontal plane','UNT',15,0,0 UNION ALL

Select '8433191000','Not powered','UNT',15,0,0 UNION ALL
Select '8433199000','Other','UNT',15,0,0 UNION ALL
Select '8433200000','Other mowers, including cutter bars for tractor mounting','UNT',0,0,0 UNION ALL
Select '8433300000','Other haymaking machinery','UNT',0,0,0 UNION ALL
Select '8433400000','Straw or fodder balers, including pickup balers','UNT',0,0,0 UNION ALL

Select '8433510000','Combine harvesterthreshers','UNT',0,0,0 UNION ALL
Select '8433520000','Other threshing machinery','UNT',0,0,0 UNION ALL
Select '8433530000','Root or tuber harvesting machines','UNT',0,0,0 UNION ALL

Select '8433592000','Cotton pickers','UNT',0,0,0 UNION ALL
Select '8433599000','Other ','UNT',0,0,0 UNION ALL

Select '8433601000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8433602000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8433901000','Castors wheel, of a diameter (including tyres) exceeding 100 mm but not exceeding 250 mm, provided that the width of any wheel or tyre fitted thereto exceeds 30 mm','KGM',30,0,0 UNION ALL
Select '8433902000','Other, of goods of subheading 8433.11 or 8433.19.90','KGM',0,0,0 UNION ALL
Select '8433903000','Other, of goods of subheading 8433.19.10','KGM',0,0,0 UNION ALL
Select '8433909000','Other','KGM',0,0,0 UNION ALL


Select '8434101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8434102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8434201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8434202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8434901000','Suitable for use solely or principally with electrically operated machines','KGM',0,0,0 UNION ALL
Select '8434909000','Other','KGM',0,0,0 UNION ALL


Select '8435101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8435102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8435901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8435902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8436101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8436102000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8436211000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8436212000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8436291000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8436292000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8436801100','Agricultural or horticultural ','UNT',0,0,0 UNION ALL
Select '8436801900','Other ','UNT',0,0,0 UNION ALL

Select '8436802100','Agricultural or horticultural ','UNT',0,0,0 UNION ALL
Select '8436802900','Other ','UNT',0,0,0 UNION ALL


Select '8436911000','Of electrically operated machines and equipment','KGM',0,0,0 UNION ALL
Select '8436912000','Of nonelectrically operated machines and equipment','KGM',0,0,0 UNION ALL


Select '8436991100','Agricultural or horticultural ','KGM',0,0,0 UNION ALL
Select '8436991900','Other','KGM',0,0,0 UNION ALL

Select '8436992100','Agricultural or horticultural ','KGM',0,0,0 UNION ALL
Select '8436992900','Other','KGM',0,0,0 UNION ALL


Select '8437101000','For grains, electrically operated; winnowing and similar cleaning machines, electrically operated','UNT',0,0,0 UNION ALL
Select '8437102000','For grains, not electrically operated; winnowing and  similar cleaning machines, not electrically operated      ','UNT',0,0,0 UNION ALL
Select '8437103000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8437104000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '8437801000','Rice hullers and cone type rice mills, electrically operated','UNT',0,0,0 UNION ALL
Select '8437802000','Rice hullers and cone type rice mills, not electrically operated','UNT',0,0,0 UNION ALL
Select '8437803000','Industrial type coffee and corn mills, electrically operated','UNT',0,0,0 UNION ALL
Select '8437804000','Industrial type coffee and corn mills, not electrically operated','UNT',0,0,0 UNION ALL

Select '8437805100','Polishing machines for rice, sifting and sieving machines, bran cleaning machines and husking machines','UNT',0,0,0 UNION ALL
Select '8437805900','Other ','UNT',0,0,0 UNION ALL

Select '8437806100','Polishing machines for rice, sifting and sieving machines, bran cleaning machines and husking machines','UNT',0,0,0 UNION ALL
Select '8437806900','Other ','UNT',0,0,0 UNION ALL


Select '8437901100','Of machines of subheading 8437.10','KGM',0,0,0 UNION ALL
Select '8437901900','Other ','KGM',0,0,0 UNION ALL

Select '8437902100','Of machines of subheading 8437.10','KGM',0,0,0 UNION ALL
Select '8437902900','Other ','KGM',0,0,0 UNION ALL


Select '8438101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8438201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8438301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438302000','Not electrically operated','UNT',0,0,0 UNION ALL
Select '8438400000','Brewery machinery','UNT',0,0,0 UNION ALL

Select '8438501000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438502000','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8438601000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438602000','Not electrically operated ','UNT',0,0,0 UNION ALL


Select '8438801100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438801200','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8438802100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438802200','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8438809100','Electrically operated','UNT',0,0,0 UNION ALL
Select '8438809200','Not electrically operated ','UNT',0,0,0 UNION ALL


Select '8438901100','Of goods of subheading 8438.30.10 ','KGM',0,0,0 UNION ALL
Select '8438901200','Of coffee pulpers','KGM',0,0,0 UNION ALL
Select '8438901900','Other','KGM',0,0,0 UNION ALL

Select '8438902100','Of goods of subheading 8438.30.20 ','KGM',0,0,0 UNION ALL
Select '8438902200','Of coffee pulpers','KGM',0,0,0 UNION ALL
Select '8438902900','Other','KGM',0,0,0 UNION ALL

Select '8439100000','Machinery for making pulp of fibrous cellulosic material','UNT',0,0,0 UNION ALL
Select '8439200000','Machinery for making paper or paperboard','UNT',0,0,0 UNION ALL
Select '8439300000','Machinery for finishing paper or paperboard','UNT',0,0,0 UNION ALL

Select '8439910000','Of machinery for making pulp of fibrous cellulosic material','KGM',0,0,0 UNION ALL
Select '8439990000','Other','KGM',0,0,0 UNION ALL


Select '8440101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8440102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8440901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8440902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8441101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8441102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8441201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8441202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8441301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8441302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8441401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8441402000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8441801000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8441802000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8441901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8441902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8442301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8442302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8442401000','Of electrically operated machines, apparatus or equipment','KGM',0,0,0 UNION ALL
Select '8442402000','Of  nonelectrically operated  machines, apparatus  or equipment','KGM',0,0,0 UNION ALL
Select '8442500000','Plates, cylinders and other printing components; plates, cylinders and lithographic stones, prepared for printing purposes (for example, planed, grained or polished)','KGM',0,0,0 UNION ALL


Select '8443110000','Offset printing machinery, reelfed','UNT',0,0,0 UNION ALL
Select '8443120000','Offset printing machinery, sheetfed, office type (using sheets with one side not exceeding 22 cm and the other side not exceeding 36 cm in the unfolded state)','UNT',0,0,0 UNION ALL
Select '8443130000','Other offset printing machinery','UNT',0,0,0 UNION ALL
Select '8443140000','Letterpress printing machinery, reelfed, excluding flexographic printing','UNT',0,0,0 UNION ALL
Select '8443150000','Letterpress printing machinery, other than reelfed, excluding flexographic printing','UNT',0,0,0 UNION ALL
Select '8443160000','Flexographic printing machinery','UNT',0,0,0 UNION ALL
Select '8443170000','Gravure printing machinery','UNT',0,0,0 UNION ALL
Select '8443190000','Other','UNT',0,0,0 UNION ALL



Select '8443311100','Colour','UNT',0,0,0 UNION ALL
Select '8443311900','Other','UNT',0,0,0 UNION ALL

Select '8443312100','Colour','UNT',0,0,0 UNION ALL
Select '8443312900','Other','UNT',0,0,0 UNION ALL

Select '8443313100','Colour','UNT',0,0,0 UNION ALL
Select '8443313900','Other','UNT',0,0,0 UNION ALL

Select '8443319100','Combination printercopierscannerfacsimile machines
','UNT',0,0,0 UNION ALL
Select '8443319900','Other','UNT',0,0,0 UNION ALL


Select '8443321100','Colour','UNT',0,0,0 UNION ALL
Select '8443321900','Other','UNT',0,0,0 UNION ALL

Select '8443322100','Colour','UNT',0,0,0 UNION ALL
Select '8443322900','Other','UNT',0,0,0 UNION ALL

Select '8443323100','Colour','UNT',0,0,0 UNION ALL
Select '8443323900','Other','UNT',0,0,0 UNION ALL

Select '8443324100','Colour','UNT',0,0,0 UNION ALL
Select '8443324900','Other','UNT',0,0,0 UNION ALL
Select '8443325000','Screen printing machinery for the manufacture of printed circuit boards or printed wiring boards','UNT',0,0,0 UNION ALL
Select '8443326000','Plotters','UNT',0,0,0 UNION ALL
Select '8443329000','Other','UNT',0,0,0 UNION ALL

Select '8443391000','Electrostatic photocopying apparatus operating by reproducing the original image directly onto the copy (direct process)','UNT',0,0,0 UNION ALL
Select '8443392000','Electrostatic photocopying apparatus, operating by reproducing the original image via an intermediate onto the copy (indirect process)','UNT',0,0,0 UNION ALL
Select '8443393000','Other photocopying apparatus incorporating an optical system','UNT',0,0,0 UNION ALL
Select '8443394000','Inkjet printers  ','UNT',0,0,0 UNION ALL
Select '8443399000','Other','UNT',0,0,0 UNION ALL

Select '8443910000','Parts and accessories of printing machinery used for printing by means of plates, cylinders and other printing components of heading 84.42','KGM',0,0,0 UNION ALL

Select '8443991000','Of screen printing machinery for the manufacture of printed circuit boards or printed wiring boards','UNT',0,0,0 UNION ALL
Select '8443992000','Inkfilled printer cartridges','UNT',0,0,0 UNION ALL
Select '8443993000','Paper feeders; paper sorters','UNT',0,0,0 UNION ALL
Select '8443999000','Other','KGM',0,0,0 UNION ALL

Select '8444001000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8444002000','Not electrically operated','UNT',0,0,0 UNION ALL



Select '8445111000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445112000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445121000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445122000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445131000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445132000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445193000','Cotton gins ','UNT',0,0,0 UNION ALL
Select '8445194000','Other, electrically operated ','UNT',0,0,0 UNION ALL
Select '8445195000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '8445201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445402000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8445901000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8445902000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8446101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8446102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8446210000','Power looms','UNT',0,0,0 UNION ALL
Select '8446290000','Other','UNT',0,0,0 UNION ALL
Select '8446300000','For weaving fabrics of a width exceeding 30 cm, shuttleless type','UNT',0,0,0 UNION ALL



Select '8447111000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8447112000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8447121000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8447122000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8447201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8447202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8447901000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8447902000','Not electrically operated','UNT',0,0,0 UNION ALL



Select '8448111000','Electrically operated','KGM',0,0,0 UNION ALL
Select '8448112000','Not electrically operated','KGM',0,0,0 UNION ALL

Select '8448191000','Electrically operated','KGM',0,0,0 UNION ALL
Select '8448192000','Not electrically operated','KGM',0,0,0 UNION ALL
Select '8448200000','Parts and accessories of machines of heading 84.44 or of their auxiliary machinery','KGM',0,0,0 UNION ALL

Select '8448310000','Card clothing','KGM',0,0,0 UNION ALL
Select '8448320000','Of machines for preparing textile fibres, other than card clothing','KGM',0,0,0 UNION ALL
Select '8448330000','Spindles, spindle flyers, spinning rings and ring travellers','KGM',0,0,0 UNION ALL
Select '8448390000','Other','KGM',0,0,0 UNION ALL

Select '8448420000','Reeds for looms, healds and healdframes','KGM',0,0,0 UNION ALL


Select '8448491100','For electrically operated machines ','KGM',0,0,0 UNION ALL
Select '8448491200','For notelectrically operated machines','KGM',0,0,0 UNION ALL

Select '8448499100','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8448499200','Of notelectrically operated machines','KGM',0,0,0 UNION ALL

Select '8448510000','Sinkers, needles and other articles used in forming stitches','KGM',0,0,0 UNION ALL
Select '8448590000','Other','KGM',0,0,0 UNION ALL

Select '8449001000','Electrically operated','KGM',0,0,0 UNION ALL
Select '8449002000','Not electrically operated','KGM',0,0,0 UNION ALL



Select '8450111000','Each of a dry linen capacity not exceeding 6 kg','UNT',25,0,0 UNION ALL
Select '8450119000','Other','UNT',25,0,0 UNION ALL

Select '8450121000','Each of a dry linen capacity not exceeding 6 kg','UNT',25,0,0 UNION ALL
Select '8450129000','Other','UNT',25,0,0 UNION ALL


Select '8450191100','Each of a dry linen capacity not exceeding 6 kg','UNT',25,0,0 UNION ALL
Select '8450191900','Other','UNT',25,0,0 UNION ALL

Select '8450199100','Each of a dry linen capacity not exceeding 6 kg','UNT',5,0,0 UNION ALL
Select '8450199900','Other','UNT',5,0,0 UNION ALL
Select '8450200000','Machines, each of a dry linen capacity exceeding 10 kg','UNT',0,0,0 UNION ALL

Select '8450901000','Of machines of subheading 8450.20.00 ','KGM',0,0,0 UNION ALL
Select '8450902000','Of machines of subheadings 8450.11, 8450.12 or 8450.19','KGM',0,0,0 UNION ALL

Select '8451100000','Drycleaning machines','UNT',0,0,0 UNION ALL

Select '8451210000','Each of a dry linen capacity not exceeding 10 kg  ','UNT',0,0,0 UNION ALL
Select '8451290000','Other','UNT',0,0,0 UNION ALL

Select '8451301000','Single roller type domestic ironing machines','UNT',20,0,0 UNION ALL
Select '8451309000','Other','UNT',0,0,0 UNION ALL
Select '8451400000','Washing, bleaching or dyeing machines','UNT',0,0,0 UNION ALL
Select '8451500000','Machines for reeling, unreeling, folding, cutting or pinking textile fabrics ','UNT',0,0,0 UNION ALL
Select '8451800000','Other machinery','UNT',0,0,0 UNION ALL

Select '8451901000','Of machines of a dry linen capacity not exceeding 10 kg','KGM',30,0,0 UNION ALL
Select '8451909000','Other','KGM',0,0,0 UNION ALL

Select '8452100000','Sewing machines of the household type','UNT',17,0,0 UNION ALL

Select '8452210000','Automatic units','UNT',0,0,0 UNION ALL
Select '8452290000','Other','UNT',0,0,0 UNION ALL
Select '8452300000','Sewing machine needles','KGM',0,0,0 UNION ALL


Select '8452901100','Arms and beds; stands with or without centre frames;  flywheels; belt guards; treadles or pedals','KGM',20,0,0 UNION ALL
Select '8452901200','Furniture, bases and covers and parts thereof','KGM',20,0,0 UNION ALL
Select '8452901900','Other','KGM',0,0,0 UNION ALL

Select '8452909100','Arms and beds; stands with or without centre frames;  flywheels; belt guards; treadles or pedals','KGM',20,0,0 UNION ALL
Select '8452909200','Furniture, bases and covers and parts thereof','KGM',20,0,0 UNION ALL
Select '8452909900','Other','KGM',0,0,0 UNION ALL


Select '8453101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8453102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8453201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8453202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8453801000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8453802000','Not electrically operated','UNT',0,0,0 UNION ALL
Select '8453900000','Parts','KGM',0,0,0 UNION ALL

Select '8454100000','Converters','UNT',0,0,0 UNION ALL
Select '8454200000','Ingot moulds and ladles','UNT',0,0,0 UNION ALL
Select '8454300000','Casting machines','UNT',0,0,0 UNION ALL
Select '8454900000','Parts','KGM',0,0,0 UNION ALL

Select '8455100000','Tube mills','UNT',0,0,0 UNION ALL

Select '8455210000','Hot or combination hot and cold','UNT',0,0,0 UNION ALL
Select '8455220000','Cold','UNT',0,0,0 UNION ALL
Select '8455300000','Rolls for rolling mills','UNT',0,0,0 UNION ALL
Select '8455900000','Other parts','KGM',0,0,0 UNION ALL


Select '8456110000','Operated by laser','UNT',0,0,0 UNION ALL
Select '8456120000','Operated by other light or photon beam processes','UNT',0,0,0 UNION ALL
Select '8456200000','Operated by ultrasonic processes','UNT',0,0,0 UNION ALL
Select '8456300000','Operated by electrodischarge processes','UNT',0,0,0 UNION ALL

Select '8456401000','Machine tools, numerically controlled, for working any material by removal of material, by plasma arc processes, for the manufacture of printed circuit boards or printed wiring boards','UNT',0,0,0 UNION ALL
Select '8456409000','Other ','UNT',0,0,0 UNION ALL
Select '8456500000','Waterjet cutting machines','UNT',0,0,0 UNION ALL

Select '8456902000','Wet processing equipments for the application by immersion of electrochemical solutions, for the purpose of removing material on printed circuit boards or printed wiring boards ','UNT',0,0,0 UNION ALL
Select '8456909000','Other ','UNT',0,0,0 UNION ALL


Select '8457101000','Of spindle power not exceeding 4kW','UNT',0,0,0 UNION ALL
Select '8457109000','Other','UNT',0,0,0 UNION ALL
Select '8457200000','Unit construction machines (single station)','UNT',0,0,0 UNION ALL
Select '8457300000','Multistation transfer machines','UNT',0,0,0 UNION ALL



Select '8458111000','Of spindle power not exceeding 4kW','UNT',0,0,0 UNION ALL
Select '8458119000','Other','UNT',0,0,0 UNION ALL

Select '8458191000','With the distance between the main spindle centre and the bed not exceeding 300 mm','UNT',0,0,0 UNION ALL
Select '8458199000','Other','UNT',0,0,0 UNION ALL

Select '8458910000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8458991000','With the distance between the main spindle centre and the bed not exceeding 300 mm','UNT',0,0,0 UNION ALL
Select '8458999000','Other','UNT',0,0,0 UNION ALL


Select '8459101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459210000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8459291000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459292000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459310000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8459391000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459392000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459410000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8459491000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459492000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459510000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8459591000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459592000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459610000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8459691000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459692000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8459701000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8459702000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8460120000','Numerically controlled','UNT',0,0,0 UNION ALL
Select '8460190000','Other','UNT',0,0,0 UNION ALL

Select '8460220000','Centreless grinding machines, numerically controlled','UNT',0,0,0 UNION ALL
Select '8460230000','Other cylindrical grinding machines, numerically controlled','UNT',0,0,0 UNION ALL
Select '8460240000','Other, numerically controlled','UNT',0,0,0 UNION ALL

Select '8460291000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8460292000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8460311000','Machine  tools,  numerically  controlled,  for sharpening carbide drilling bits with a shank diameter not exceeding 3.175 mm, provided with fixed collets and having a power not  exceeding 0.74 kW ','UNT',0,0,0 UNION ALL
Select '8460319000','Other ','UNT',0,0,0 UNION ALL

Select '8460391000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8460392000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8460401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8460402000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8460901000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8460902000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8461201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8461202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8461301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8461302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8461401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8461402000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8461501000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8461502000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8461901100','Planing machines','UNT',0,0,0 UNION ALL
Select '8461901900','Other','UNT',0,0,0 UNION ALL

Select '8461909100','Planing machines','UNT',0,0,0 UNION ALL
Select '8461909900','Other','UNT',0,0,0 UNION ALL


Select '8462101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8462102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8462210000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8462291000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8462292000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8462310000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8462391000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8462392000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8462410000','Numerically controlled','UNT',0,0,0 UNION ALL

Select '8462491000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8462492000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8462910000','Hydraulic presses','UNT',0,0,0 UNION ALL

Select '8462991000','Machines for the manufacture of boxes, cans and similar containers of tin plate, electrically operated','UNT',0,0,0 UNION ALL
Select '8462992000','Machines for the manufacture of boxes, cans and similar containers of tin plate, not electrically operated ','UNT',0,0,0 UNION ALL
Select '8462995000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8462996000','Other, not electrically operated','UNT',0,0,0 UNION ALL


Select '8463101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8463102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8463201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8463202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8463301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8463302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8463901000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8463902000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8464101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8464102000','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8464201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8464202000','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8464901000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8464902000','Not electrically operated ','UNT',0,0,0 UNION ALL

Select '8465100000','Machines which can carry out different types of machining operations without tool change between such operations','UNT',0,0,0 UNION ALL
Select '8465200000','Machining centres','UNT',0,0,0 UNION ALL


Select '8465911000','Of a kind used for scoring printed circuit boards or printed wiring boards or printed circuit board or printed wiring board substrates, electrically operated','UNT',0,0,0 UNION ALL
Select '8465912000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8465913000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465921000','For routing printed circuit boards or  printed wiring boards or printed circuit board or  printed wiring board substrates,  accepting router bits with a shank diameter not exceeding 3.175 mm, for scoring printed circuit boards or  printed wiring   boards or printed circuit board or  printed wiring board substrates ','UNT',0,0,0 UNION ALL
Select '8465922000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8465923000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465931000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8465932000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465941000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8465942000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465951000','Drilling machines for the manufacture of printed circuit  boards  or printed wiring boards, with a spindle speed exceeding 50,000 rpm and accepting drill bits of a shank  diameter not exceeding 3.175 mm ','UNT',0,0,0 UNION ALL
Select '8465953000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8465954000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465961000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8465962000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8465993000','Lathes, electrically operated','UNT',0,0,0 UNION ALL
Select '8465994000','Lathes, not electrically operated','UNT',0,0,0 UNION ALL
Select '8465995000','Machines for deburring the surfaces of printed circuit boards or printed wiring boards during manufacturing; machines for scoring printed  circuit boards or  printed wiring  boards or printed circuit board or printed wiring board  substrates; laminating presses for the manufacture of printed circuit boards or  printed wiring boards ','UNT',0,0,0 UNION ALL
Select '8465996000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8465999000','Other ','UNT',0,0,0 UNION ALL


Select '8466101000','For the machinetools of subheading 8456.40.10, 8456.90.20, 8460.31.10, 8465.91.10, 8465.92.10, 8465.95.10 or 8465.99.50','KGM',0,0,0 UNION ALL
Select '8466109000','Other ','KGM',0,0,0 UNION ALL

Select '8466201000','For the machines of subheading 8456.40.10, 8456.90.20, 8460.31.10, 8465.91.10, 8465.92.10, 8465.95.10 or 8465.99.50 ','KGM',0,0,0 UNION ALL
Select '8466209000','Other ','KGM',0,0,0 UNION ALL

Select '8466301000','For the machines of subheading 8456.40.10, 8456.90.20,  8460.31.10, 8465.91.10, 8465.92.10, 8465.95.10 or 8465.99.50 ','KGM',0,0,0 UNION ALL
Select '8466309000','Other ','KGM',0,0,0 UNION ALL

Select '8466910000','For machines of heading 84.64','KGM',0,0,0 UNION ALL

Select '8466921000','For the machines of subheading 8465.91.10, 8465.92.10, 8465.95.10 or 8465.99.50 ','KGM',0,0,0 UNION ALL
Select '8466929000','Other ','KGM',0,0,0 UNION ALL

Select '8466932000','For machines of subheading 8456.40.10, 8456.90.20 or 8460.31.10 ','KGM',0,0,0 UNION ALL
Select '8466939000','Other ','KGM',0,0,0 UNION ALL
Select '8466940000','For machines of heading 84.62 or 84.63','KGM',0,0,0 UNION ALL


Select '8467110000','Rotary type (including combined rotarypercussion)','UNT',0,0,0 UNION ALL
Select '8467190000','Other','UNT',0,0,0 UNION ALL

Select '8467210000','Drills of all kinds  ','UNT',0,0,0 UNION ALL
Select '8467220000','Saws','UNT',0,0,0 UNION ALL
Select '8467290000','Other','UNT',0,0,0 UNION ALL

Select '8467810000','Chain saws','UNT',0,0,0 UNION ALL
Select '8467890000','Other','UNT',0,0,0 UNION ALL


Select '8467911000','Of electromechanical type','KGM',0,0,0 UNION ALL
Select '8467919000','Other','KGM',0,0,0 UNION ALL
Select '8467920000','Of pneumatic tools','KGM',0,0,0 UNION ALL

Select '8467991000','Of goods of subheadings 8467.21.00, 8467.22.00 or 8467.29.00','KGM',0,0,0 UNION ALL
Select '8467999000','Other','KGM',0,0,0 UNION ALL

Select '8468100000','Handheld blow pipes','UNT',0,0,0 UNION ALL

Select '8468201000','Handoperated (not handheld) gas welding or brazing appliances for metal','UNT',0,0,0 UNION ALL
Select '8468209000','Other ','UNT',0,0,0 UNION ALL
Select '8468800000','Other machinery and apparatus','UNT',0,0,0 UNION ALL

Select '8468902000','Of goods of subheading 8468.20.10','KGM',0,0,0 UNION ALL
Select '8468909000','Other ','KGM',0,0,0 UNION ALL

Select '8470100000','Electronic calculators capable of operation without an external source of electric power and pocketsize data recording, reproducing and displaying machines with calculating functions','UNT',0,0,0 UNION ALL

Select '8470210000','Incorporating a printing device','UNT',0,0,0 UNION ALL
Select '8470290000','Other','UNT',0,0,0 UNION ALL
Select '8470300000','Other calculating machines','UNT',0,0,0 UNION ALL
Select '8470500000','Cash registers ','UNT',0,0,0 UNION ALL

Select '8470901000','Postagefranking machines','UNT',0,0,0 UNION ALL
Select '8470902000','Accounting machines','UNT',0,0,0 UNION ALL
Select '8470909000','Other','UNT',0,0,0 UNION ALL


Select '8471302000','Laptops including notebooks and subnotebooks','UNT',0,0,0 UNION ALL
Select '8471309000','Other','UNT',0,0,0 UNION ALL


Select '8471411000','Personal computers excluding portable computers of  subheading 8471.30       ','UNT',0,0,0 UNION ALL
Select '8471419000','Other','UNT',0,0,0 UNION ALL

Select '8471491000','Personal computers excluding portable computers of  subheading 8471.30','UNT',0,0,0 UNION ALL
Select '8471499000','Other','UNT',0,0,0 UNION ALL

Select '8471501000','Processing units for personal (including portable) computers','UNT',0,0,0 UNION ALL
Select '8471509000','Other ','UNT',0,0,0 UNION ALL

Select '8471603000','Computer keyboards ','UNT',0,0,0 UNION ALL
Select '8471604000','XY coordinate input devices, including mouses, light pens, joysticks, track balls, and touch sensitive screens','UNT',0,0,0 UNION ALL
Select '8471609000','Other ','UNT',0,0,0 UNION ALL

Select '8471701000','Floppy disk drives','UNT',0,0,0 UNION ALL
Select '8471702000','Hard disk drives','UNT',0,0,0 UNION ALL
Select '8471703000','Tape drives','UNT',0,0,0 UNION ALL
Select '8471704000','Optical disk drives, including CDROM drives, DVD drives and  CDR drives ','UNT',0,0,0 UNION ALL
Select '8471705000','Proprietary format storage devices including media therefor for  automatic data processing machines, with or without removable media and whether magnetic, optical or other technology ','UNT',0,0,0 UNION ALL

Select '8471709100','Automated backup systems','UNT',0,0,0 UNION ALL
Select '8471709900','Other','UNT',0,0,0 UNION ALL

Select '8471801000','Control and adaptor units ','UNT',0,0,0 UNION ALL
Select '8471807000','Sound cards or video cards','UNT',0,0,0 UNION ALL
Select '8471809000','Other ','UNT',0,0,0 UNION ALL

Select '8471901000','Bar code readers','UNT',0,0,0 UNION ALL
Select '8471903000','Electronic fingerprint identification systems','UNT',0,0,0 UNION ALL
Select '8471904000','Other optical character readers','UNT',0,0,0 UNION ALL
Select '8471909000','Other ','UNT',0,0,0 UNION ALL


Select '8472101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8472102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8472301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8472302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8472901000','Automatic teller machines ','UNT',0,0,0 UNION ALL

Select '8472904100','Automatic','UNT',0,0,0 UNION ALL
Select '8472904900','Other ','UNT',0,0,0 UNION ALL
Select '8472905000','Wordprocessing machines','UNT',0,0,0 UNION ALL
Select '8472906000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '8472909000','Other, not electrically operated','UNT',0,0,0 UNION ALL


Select '8473210000','Of the electronic calculating machines of subheading 8470.10.00, 8470.21.00 or 8470.29.00','KGM',0,0,0 UNION ALL
Select '8473290000','Other ','KGM',0,0,0 UNION ALL

Select '8473301000','Assembled printed circuit boards','KGM',0,0,0 UNION ALL
Select '8473309000','Other ','KGM',0,0,0 UNION ALL

Select '8473401000','For electrically operated machines','KGM',0,0,0 UNION ALL
Select '8473402000','For nonelectrically operated machines','KGM',0,0,0 UNION ALL

Select '8473501000','Suitable for use with the machines of  heading 84.71','KGM',0,0,0 UNION ALL
Select '8473509000','Other ','KGM',0,0,0 UNION ALL


Select '8474101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8474102000','Not electrically operated','UNT',0,0,0 UNION ALL


Select '8474201100','For stone','UNT',0,0,0 UNION ALL
Select '8474201900','Other','UNT',0,0,0 UNION ALL

Select '8474202100','For stone','UNT',0,0,0 UNION ALL
Select '8474202900','Other','UNT',0,0,0 UNION ALL


Select '8474311000','Electrically operated','UNT',20,0,0 UNION ALL
Select '8474312000','Not electrically operated','UNT',20,0,0 UNION ALL


Select '8474321100','Of an output capacity not exceeding 80 t /h','UNT',0,0,0 UNION ALL
Select '8474321900','Other','UNT',0,0,0 UNION ALL

Select '8474322100','Of an output capacity not exceeding 80 t /h','UNT',0,0,0 UNION ALL
Select '8474322900','Other','UNT',0,0,0 UNION ALL

Select '8474391000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8474392000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8474801000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8474802000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8474901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8474902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8475101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8475102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8475210000','Machines for making optical fibres and preforms thereof','UNT',0,0,0 UNION ALL
Select '8475290000','Other','UNT',0,0,0 UNION ALL

Select '8475901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8475902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8476210000','Incorporating heating or refrigerating devices','UNT',0,0,0 UNION ALL
Select '8476290000','Other','UNT',0,0,0 UNION ALL

Select '8476810000','Incorporating heating or refrigerating devices','UNT',0,0,0 UNION ALL
Select '8476890000','Other','UNT',0,0,0 UNION ALL
Select '8476900000','Parts','KGM',0,0,0 UNION ALL


Select '8477101000','For moulding rubber','UNT',0,0,0 UNION ALL

Select '8477103100','Poly (vinyl chloride) injection moulding machines','UNT',0,0,0 UNION ALL
Select '8477103900','Other','UNT',0,0,0 UNION ALL

Select '8477201000','For extruding rubber','UNT',0,0,0 UNION ALL
Select '8477202000','For extruding plastics','UNT',0,0,0 UNION ALL
Select '8477300000','Blow moulding machines','UNT',0,0,0 UNION ALL

Select '8477401000','For moulding or forming rubber','UNT',0,0,0 UNION ALL
Select '8477402000','For moulding or forming plastics','UNT',0,0,0 UNION ALL

Select '8477510000','For moulding or retreading pneumatic tyres or for moulding or  otherwise forming inner tubes','UNT',0,0,0 UNION ALL

Select '8477591000','For rubber','UNT',0,0,0 UNION ALL
Select '8477592000','For plastics','UNT',0,0,0 UNION ALL

Select '8477801000','For working rubber or for the manufacture of products from  rubber, electrically operated ','UNT',0,0,0 UNION ALL
Select '8477802000','For working rubber or for the manufacture of products from   rubber, not electrically operated','UNT',0,0,0 UNION ALL

Select '8477803100','Lamination  presses  for  the manufacture of printed circuit boards or  printed wiring boards ','UNT',0,0,0 UNION ALL
Select '8477803900','Other','UNT',0,0,0 UNION ALL
Select '8477804000','For working plastics or for the manufacture of products from  plastics, not electrically operated','UNT',0,0,0 UNION ALL

Select '8477901000','Of electrically operated machines for working rubber or for the manufacture of products from rubber','KGM',0,0,0 UNION ALL
Select '8477902000','Of nonelectrically operated machines for working rubber or for the manufacture of products from rubber','KGM',0,0,0 UNION ALL

Select '8477903200','Parts  of  lamination  presses  for the manufacture of printed  circuit boards or  printed wiring boards ','KGM',0,0,0 UNION ALL
Select '8477903900','Other','KGM',0,0,0 UNION ALL
Select '8477904000','Of nonelectrically operated machines for working plastics or for the manufacture of products from plastic materials','KGM',0,0,0 UNION ALL


Select '8478101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8478102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8478901000','Of electrically operated machines','KGM',0,0,0 UNION ALL
Select '8478902000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL


Select '8479101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8479201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8479301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8479401000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479402000','Not electrically operated','UNT',0,0,0 UNION ALL
Select '8479500000','Industrial robots, not elsewhere specified or included','UNT',0,0,0 UNION ALL
Select '8479600000','Evaporative air coolers','UNT',0,0,0 UNION ALL

Select '8479710000','Of a kind used in airports','UNT',0,0,0 UNION ALL
Select '8479790000','Other','UNT',0,0,0 UNION ALL


Select '8479811000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479812000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8479821000','Electrically operated','UNT',0,0,0 UNION ALL
Select '8479822000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '8479892000','Machinery for assembling central processing unit (CPU) daughter boards in plastic cases or housings; apparatus for the  regeneration of chemical solutions used in the manufacture of  printed circuit boards or printed wiring boards; equipment for mechanically cleaning the surfaces  of printed circuit boards or printed wiring boards during manufacturing; automated  machines for the placement or  the removal of components or  contact elements on printed  circuit boards or printed wiring  boards or other substrates; registration equipment for the  alignment of printed circuit boards or printed wiring boards or  printed  circuit assemblies in the manufacturing process','UNT',0,0,0 UNION ALL

Select '8479893100','Automatic servicevending machines','UNT',0,0,0 UNION ALL
Select '8479893900','Other','UNT',0,0,0 UNION ALL
Select '8479894000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '8479902000','Of goods of subheading 8479.89.20','KGM',0,0,0 UNION ALL
Select '8479903000','Of other electrically operated machines','KGM',0,0,0 UNION ALL
Select '8479904000','Of nonelectrically operated machines','KGM',0,0,0 UNION ALL

Select '8480100000','Moulding boxes for metal foundry','KGM',0,0,0 UNION ALL
Select '8480200000','Mould bases','KGM',0,0,0 UNION ALL

Select '8480301000','Of copper','KGM',0,0,0 UNION ALL
Select '8480309000','Other','KGM',5,0,0 UNION ALL

Select '8480410000','Injection or compression types','KGM',0,0,0 UNION ALL
Select '8480490000','Other','KGM',0,0,0 UNION ALL
Select '8480500000','Moulds for glass','KGM',0,0,0 UNION ALL
Select '8480600000','Moulds for mineral materials','KGM',0,0,0 UNION ALL


Select '8480711000','Moulds for footwear soles','KGM',0,0,0 UNION ALL
Select '8480719000','Other','KGM',0,0,0 UNION ALL

Select '8480791000','Moulds for footwear soles','KGM',0,0,0 UNION ALL
Select '8480799000','Other','KGM',0,0,0 UNION ALL



Select '8481101100','Manually operated sluice or gate valves with inlets or outlets of an internal diameter exceeding 5cm but not exceeding 40 cm','KGM',20,0,0 UNION ALL
Select '8481101900','Other','KGM',0,0,0 UNION ALL

Select '8481102100','With an internal diameter of 2.5 cm or less','KGM',25,0,0 UNION ALL
Select '8481102200','With an internal diameter of over 2.5 cm','KGM',0,0,0 UNION ALL

Select '8481109100','Of plastics, with an internal diameter of not less than 1 cm and not more than 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481109900','Other','KGM',0,0,0 UNION ALL

Select '84812010','Manually operated sluice or gate valves with inlets or outlets of an internal diameter exceeding 5 cm but not exceeding 40 cm:','',0,0,0 UNION ALL
Select '8481201010','Of iron or steel','KGM',20,0,0 UNION ALL
Select '8481201090','Other','KGM',0,0,0 UNION ALL
Select '8481202000','Of copper or copper alloys, with an internal diameter of 2.5 cm or less, or of plastics, with an internal diameter of not less than 1 cm and not more than 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481209000','Other','KGM',0,0,0 UNION ALL

Select '8481301000','Swing checkvalves, of cast iron, with an inlet of internal diameter of 4 cm or more but not exceeding 60 cm','KGM',0,0,0 UNION ALL
Select '8481302000','Of copper or copper alloys, with an internal diameter of 2.5 cm or less','KGM',25,0,0 UNION ALL
Select '8481304000','Of plastics, with an internal diameter of not less than 1 cm and not more than 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481309000','Other','KGM',0,0,0 UNION ALL

Select '8481401000','Of copper or copper alloys, with an internal diameter of 2.5 cm or less','KGM',25,0,0 UNION ALL
Select '8481403000','Of plastics, with an internal diameter of not less than 1 cm and not more than 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481409000','Other','KGM',0,0,0 UNION ALL


Select '8481801100','Of copper or copper alloys','KGM',10,0,0 UNION ALL
Select '8481801200','Of other materials','KGM',0,0,0 UNION ALL

Select '8481801300','Of copper or copper alloys','KGM',10,0,0 UNION ALL
Select '8481801400','Of other materials','KGM',0,0,0 UNION ALL

Select '8481802100','Having inlet or outlet internal diameters not exceeding 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481802200','Having inlet or outlet internal diameters exceeding 2.5 cm','KGM',0,0,0 UNION ALL
Select '8481803000','Cocks and valves, whether or not fitted with piezoelectric igniters, for gas stoves or ranges','KGM',0,0,0 UNION ALL
Select '84818040','Aerated or carbonated liquid bottle valves; valves for gas operated beer dispensing units:','',0,0,0 UNION ALL
Select '8481804010','Of plastics and of not less than 1 cm and not more than 2.5 cm  in internal diameter ','KGM',25,0,0 UNION ALL
Select '8481804090','Other','KGM',0,0,0 UNION ALL
Select '84818050','Mixing taps and valves:','',0,0,0 UNION ALL
Select '8481805010','Of plastics and of not less than 1 cm and not more than 2.5 cm in internal diameter','KGM',25,0,0 UNION ALL
Select '8481805090','Other','KGM',0,0,0 UNION ALL


Select '8481806100','Manually operated gate valves with an internal diameter exceeding 5 cm but not exceeding 40 cm','KGM',20,0,0 UNION ALL
Select '8481806200','Other','KGM',0,0,0 UNION ALL
Select '8481806300','Other','KGM',0,0,0 UNION ALL

Select '8481806400','Of plastics and of not less than 1 cm and not more than 2.5 cm  in internal diameter','KGM',25,0,0 UNION ALL
Select '8481806500','Other','KGM',0,0,0 UNION ALL

Select '8481806600','Of plastics and of not less than 1 cm and not more than 2.5 cm in internal diameter','KGM',25,0,0 UNION ALL
Select '8481806700','Other','KGM',0,0,0 UNION ALL


Select '8481807100','Of plastics and of not less than 1 cm and not more than  2.5 cm in internal diameter','KGM',25,0,0 UNION ALL
Select '8481807200','Other','KGM',0,0,0 UNION ALL

Select '8481807300','Having inlet and outlet internal diameters of more than 5 cm   but not more than 40 cm','KGM',20,0,0 UNION ALL
Select '8481807400','Having  inlet  and  outlet internal diameters of more than 40 cm','KGM',0,0,0 UNION ALL
Select '8481807700','Having  inlet  and  outlet internal diameters of not more than 5 cm','KGM',0,0,0 UNION ALL

Select '8481807800','Of plastics and of not less than 1 cm and not more than 2.5 cm in internal diameter','KGM',25,0,0 UNION ALL
Select '8481807900','Other','KGM',0,0,0 UNION ALL

Select '8481808100','Of plastics and of not less than 1 cm and not more than 2.5 cm in internal diameter','KGM',25,0,0 UNION ALL
Select '8481808200','Other','KGM',0,0,0 UNION ALL

Select '84818085','Having an inlet diameter of not less than 1 cm: ','',0,0,0 UNION ALL
Select '8481808510','With an internal diameter not more than 2.5 cm','KGM',25,0,0 UNION ALL
Select '8481808590','Other','KGM',0,0,0 UNION ALL

Select '8481808700','Fuel cutoff valves for vehicles of heading 87.02, 87.03 or 87.04','KGM',0,0,0 UNION ALL
Select '8481808800','Other','KGM',0,0,0 UNION ALL
Select '8481808900','Other, manually operated, weighing less than 3 kg, surface  treated or made of stainless steel or nickel','KGM',0,0,0 UNION ALL

Select '8481809100','Water taps of copper or copper alloy, with an internal diameter of 2.5 cm or less ','KGM',25,0,0 UNION ALL

Select '8481809200','Fuel cutoff valves for vehicles of heading 87.02, 87.03 or 87.04','KGM',0,0,0 UNION ALL
Select '8481809900','Other','KGM',0,0,0 UNION ALL

Select '8481901000','Housings for sluice or gate valves with inlet or outlet of an internal diameter exceeding 50 mm but not exceeding 400 mm ','KGM',20,0,0 UNION ALL

Select '8481902100','Bodies, for water taps','KGM',25,0,0 UNION ALL
Select '8481902200','Bodies, for Liquefied Petroleum Gas (LPG) cylinder valves','KGM',25,0,0 UNION ALL
Select '8481902300','Bodies, other','KGM',0,0,0 UNION ALL
Select '8481902900','Other','KGM',25,0,0 UNION ALL

Select '8481903100','Of copper or copper alloys','KGM',10,0,0 UNION ALL
Select '8481903900','Other','KGM',5,0,0 UNION ALL

Select '8481904100','Of copper or copper alloys','KGM',0,0,0 UNION ALL
Select '8481904900','Other','KGM',5,0,0 UNION ALL
Select '8481909000','Other','KGM',5,0,0 UNION ALL

Select '8482100000','Ball bearings','UNT',0,0,0 UNION ALL
Select '8482200000','Tapered roller bearings, including cone and tapered roller assemblies  ','UNT',0,0,0 UNION ALL
Select '8482300000','Spherical roller bearings','UNT',0,0,0 UNION ALL
Select '8482400000','Needle roller bearings ','UNT',0,0,0 UNION ALL
Select '8482500000','Other cylindrical roller bearings','UNT',0,0,0 UNION ALL
Select '8482800000','Other, including combined ball/roller bearings','UNT',0,0,0 UNION ALL

Select '8482910000','Balls, needles and rollers','KGM',0,0,0 UNION ALL
Select '8482990000','Other','KGM',0,0,0 UNION ALL


Select '8483101000','For machinery of heading 84.29 or 84.30','UNT',5,0,0 UNION ALL

Select '8483102400','For vehicles of heading 87.11','UNT',5,0,0 UNION ALL

Select '8483102500','For vehicles of a cylinder capacity not exceeding 2,000 cc','UNT',5,0,0 UNION ALL
Select '8483102600','For vehicles of a cylinder capacity exceeding 2,000 cc but not exceeding 3,000 cc','UNT',5,0,0 UNION ALL
Select '8483102700','For vehicles of a cylinder capacity exceeding 3,000 cc','UNT',5,0,0 UNION ALL

Select '8483103100','Of an output not exceeding 22.38 kW','UNT',5,0,0 UNION ALL
Select '8483103900','Other','UNT',5,0,0 UNION ALL
Select '8483109000','Other','UNT',5,0,0 UNION ALL

Select '8483202000','For machinery of heading 84.29 or 84.30','UNT',5,0,0 UNION ALL
Select '8483203000','For engines of vehicles of Chapter 87','UNT',5,0,0 UNION ALL
Select '8483209000','Other','UNT',5,0,0 UNION ALL

Select '8483303000','For engines of vehicles of Chapter 87','UNT',5,0,0 UNION ALL
Select '8483309000','Other','UNT',5,0,0 UNION ALL

Select '8483402000','For marine vessels','UNT',5,0,0 UNION ALL
Select '8483403000','For machinery of heading 84.29 or 84.30','UNT',5,0,0 UNION ALL
Select '8483404000','For vehicles of Chapter 87','UNT',5,0,0 UNION ALL
Select '8483409000','Other','UNT',5,0,0 UNION ALL
Select '8483500000','Flywheels and pulleys, including pulley blocks','UNT',5,0,0 UNION ALL
Select '8483600000','Clutches and shaft couplings (including universal joints)','UNT',5,0,0 UNION ALL


Select '8483901100','For tractors of subheading 8701.10 or 8701.91 to 8701.95','KGM',5,0,0 UNION ALL
Select '8483901300','For other tractors of heading 87.01','KGM',5,0,0 UNION ALL
Select '8483901400','For goods of heading 87.11','KGM',5,0,0 UNION ALL
Select '8483901500','For other goods of Chapter 87','KGM',5,0,0 UNION ALL
Select '8483901900','Other','KGM',5,0,0 UNION ALL

Select '8483909100','For goods of subheading 8701.10 or 8701.91 to 8701.95','KGM',5,0,0 UNION ALL
Select '8483909300','For other tractors of heading 87.01','KGM',5,0,0 UNION ALL
Select '8483909400','For goods of heading 87.11','KGM',5,0,0 UNION ALL
Select '8483909500','For other goods of Chapter 87','KGM',5,0,0 UNION ALL
Select '8483909900','Other','KGM',5,0,0 UNION ALL

Select '8484100000','Gaskets and similar joints of metal sheeting combined with other material or of two or more layers of metal','KGM',5,0,0 UNION ALL
Select '8484200000','Mechanical seals','KGM',5,0,0 UNION ALL
Select '8484900000','Other','KGM',5,0,0 UNION ALL


Select '8486101000','Apparatus for rapid heating of semiconductor wafers  ','UNT',0,0,0 UNION ALL
Select '8486102000','Spin dryers for semiconductor wafer processing ','UNT',0,0,0 UNION ALL
Select '8486103000','Machines for working any material by removal of material, by laser or other light or photon beam in the production of semiconductor wafers ','UNT',0,0,0 UNION ALL
Select '8486104000','Machines and apparatus for sawing monocrystal semiconductor boules into slices, or wafers into chips ','UNT',0,0,0 UNION ALL
Select '8486105000','Grinding, polishing and lapping machines for processing of semiconductor wafers  ','UNT',0,0,0 UNION ALL
Select '8486106000','Apparatus for growing or pulling monocrystal semiconductor boules','UNT',0,0,0 UNION ALL
Select '8486109000','Other','UNT',0,0,0 UNION ALL


Select '8486201100','Chemical vapour deposition apparatus for semiconductor   production ','UNT',0,0,0 UNION ALL
Select '8486201200','Epitaxial deposition machines for semiconductor wafers; spinners for coating photographic emulsions on semiconductor wafers  ','UNT',0,0,0 UNION ALL
Select '8486201300','Apparatus for physical deposition by sputtering on semiconductor wafers; physical deposition apparatus for   semiconductor production           ','UNT',0,0,0 UNION ALL
Select '8486201900','Other','UNT',0,0,0 UNION ALL

Select '8486202100','Ion implanters for doping semiconductor materials ','UNT',0,0,0 UNION ALL
Select '8486202900','Other','UNT',0,0,0 UNION ALL

Select '8486203100','Deflash machines for cleaning and removing contaminants  from the metal leads of semiconductor packages prior to the electroplating process; spraying appliances for etching, stripping or cleaning semiconductor wafers ','UNT',0,0,0 UNION ALL
Select '8486203200','Equipment for dryetching patterns on semiconductor materials ','UNT',0,0,0 UNION ALL
Select '8486203300','Apparatus for wet etching, developing, stripping or cleaning semiconductor wafers ','UNT',0,0,0 UNION ALL
Select '8486203900','Other','UNT',0,0,0 UNION ALL

Select '8486204100','Direct writeonwafer apparatus  ','UNT',0,0,0 UNION ALL
Select '8486204200','Step and repeat aligners  ','UNT',0,0,0 UNION ALL
Select '8486204900','Other  ','UNT',0,0,0 UNION ALL

Select '8486205100','Dicing machines for scribing or scoring semiconductor wafers  ','UNT',0,0,0 UNION ALL
Select '8486205900','Other','UNT',0,0,0 UNION ALL

Select '8486209100','Lasercutters for cutting contacting tracks in semiconductor production by laser beam ','UNT',0,0,0 UNION ALL
Select '8486209200','Machines for bending, folding and straightening semiconductor  leads ','UNT',0,0,0 UNION ALL
Select '8486209300','Resistance heated furnaces and ovens for the manufacture of  semiconductor devices on semiconductor wafers ','UNT',0,0,0 UNION ALL
Select '8486209400','Inductance or dielectric furnaces and ovens for the manufacture of semiconductor devices on semiconductor wafers  ','UNT',0,0,0 UNION ALL
Select '8486209500','Automated machines for the placement or the removal of components or contact elements on semiconductor materials ','UNT',0,0,0 UNION ALL
Select '8486209900','Other','UNT',0,0,0 UNION ALL

Select '8486301000','Apparatus for dry etching patterns on flat panel display substrates ','UNT',0,0,0 UNION ALL
Select '8486302000','Apparatus for wet etching, developing, stripping or cleaning flat panel displays ','UNT',0,0,0 UNION ALL
Select '8486303000','Chemical vapour deposition apparatus for flat panel display production; spinners for coating photosensitive emulsions on flat  panel display substrates; apparatus for physical deposition on flat panel display substrates ','UNT',0,0,0 UNION ALL
Select '8486309000','Other','UNT',0,0,0 UNION ALL

Select '8486401000','Focused ion beam milling machines to produce or repair masks  and reticles for patterns on semiconductor devices ','UNT',0,0,0 UNION ALL
Select '8486402000','Die attach apparatus, tape automated bonders, wire bonders and  encapsulation equipment for the assembly of semiconductors; automated machines for transport, handling and storage of semiconductor wafers, wafer cassettes, wafer boxes and other materials for semiconductor devices','UNT',0,0,0 UNION ALL
Select '8486403000','Moulds for manufacture of semiconductor devices ','UNT',0,0,0 UNION ALL
Select '8486404000','Optical stereoscopic microscopes fitted with equipment  specifically designed for the handling and transport of semiconductor wafers or reticles  ','UNT',0,0,0 UNION ALL
Select '8486405000','Photomicrographic microscopes fitted with equipment specifically designed for the handling and transport of semiconductor wafers or reticles  ','UNT',0,0,0 UNION ALL
Select '8486406000','Electron beam microscopes fitted with equipment specifically designed for the handling and transport of semiconductor wafers or reticles  ','UNT',0,0,0 UNION ALL
Select '8486407000','Pattern generating apparatus of a kind used for producing masks  or reticles from photoresist coated substrates ','UNT',0,0,0 UNION ALL
Select '8486409000','Other','UNT',0,0,0 UNION ALL


Select '8486901100','Of apparatus for rapid heating of semiconductor wafers','UNT',0,0,0 UNION ALL
Select '8486901200','Of spin dryers for semiconductor wafer processing','UNT',0,0,0 UNION ALL
Select '8486901300','Of machines for working any material by removal of material, by laser or other light or photon beam in the production of semiconductor wafers','UNT',0,0,0 UNION ALL

Select '8486901400','Tool holders and selfopening dieheads; work holders; dividing heads and other special attachments for machine  tools','UNT',0,0,0 UNION ALL
Select '8486901500','Other','UNT',0,0,0 UNION ALL
Select '8486901600','Of grinding, polishing and lapping machines for processing of semiconductor wafers','UNT',0,0,0 UNION ALL
Select '8486901700','Of apparatus for growing or pulling monocrystal semiconductor boules','UNT',0,0,0 UNION ALL
Select '8486901900','Other','UNT',0,0,0 UNION ALL

Select '8486902100','Of chemical vapour deposition apparatus for  semiconductor production','UNT',0,0,0 UNION ALL
Select '8486902200','Of epitaxial deposition machines for semiconductor wafers; of  spinners for coating photographic emulsions on semiconductor  wafers','UNT',0,0,0 UNION ALL
Select '8486902300','Of ion implanters for doping semiconductor materials; of  apparatus for physical deposition by sputtering on semiconductor wafers; of physical deposition apparatus for semiconductor production; of direct writeonwafer apparatus, step and repeat aligners and other lithography equipment ','UNT',0,0,0 UNION ALL

Select '8486902400','Tool holders and selfopening dieheads; work holders; dividing heads and other special attachments for machine  tools','UNT',0,0,0 UNION ALL
Select '8486902500','Other','UNT',0,0,0 UNION ALL

Select '8486902600','Tool holders and selfopening dieheads; workholders;  dividing heads and other special attachments for machine tools','UNT',0,0,0 UNION ALL
Select '8486902700','Other','UNT',0,0,0 UNION ALL
Select '8486902800','Of resistance heated furnaces and ovens for the manufacture of  semiconductor devices on semiconductor wafers; of inductance or dielectric furnaces and ovens for the manufacture of  semiconductor devices on semiconductor wafers ','UNT',0,0,0 UNION ALL
Select '8486902900','Other','UNT',0,0,0 UNION ALL

Select '8486903100','Of apparatus for dry etching patterns on flat panel display substrates','UNT',0,0,0 UNION ALL

Select '8486903200','Tool holders and selfopening dieheads; work holders; dividing heads and other special attachments for machine  tools','UNT',0,0,0 UNION ALL
Select '8486903300','Other','UNT',0,0,0 UNION ALL
Select '8486903400','Of chemical vapour deposition apparatus for flat panel display production','UNT',0,0,0 UNION ALL
Select '8486903500','Of spinners for coating photosensitive emulsions on flat panel display substrates ','UNT',0,0,0 UNION ALL
Select '8486903600','Of apparatus for physical deposition on flat panel display  substrates','UNT',0,0,0 UNION ALL
Select '8486903900','Other','UNT',0,0,0 UNION ALL

Select '8486904100','Of focused ion beam milling machine to produce or repair  masks and reticles for patterns on semiconductor devices','UNT',0,0,0 UNION ALL
Select '8486904200','Of die attach apparatus, tape automated bonders, wire bonders and of encapsulation equipment for assembly of semiconductors','UNT',0,0,0 UNION ALL
Select '8486904300','Of automated machines for transport, handling and storage of semiconductor wafers, wafer cassettes, wafer boxes and other  materials for semiconductor devices','UNT',0,0,0 UNION ALL
Select '8486904400','Of optical stereoscopic and photomicrographic microscopes  fitted with equipment specifically designed for the handling  and transport of semiconductor wafers or reticles','UNT',0,0,0 UNION ALL
Select '8486904500','Of electron beam microscopes fitted with equipment specifically designed for the handling and transport of semiconductor wafers or reticles','UNT',0,0,0 UNION ALL
Select '8486904600','Of pattern generating apparatus of a kind used for producing masks or reticles from photoresist coated substrates, including printed circuit assemblies  ','UNT',0,0,0 UNION ALL
Select '8486904900','Other','UNT',0,0,0 UNION ALL

Select '8487100000','Ships"or boats"propellers and blades therefor','KGM',0,0,0 UNION ALL
Select '8487900000','Other','KGM',0,0,0 UNION ALL




Select '8501102100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501102200','Other, of an output not exceeding 5 W','UNT',0,0,0 UNION ALL
Select '8501102900','Other','UNT',0,0,0 UNION ALL
Select '8501103000','Spindle motors','UNT',0,0,0 UNION ALL

Select '8501104100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501104900','Other','UNT',0,0,0 UNION ALL


Select '8501105100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501105900','Other','UNT',0,0,0 UNION ALL
Select '8501106000','Spindle motors','UNT',0,0,0 UNION ALL

Select '8501109100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501109900','Other','UNT',0,0,0 UNION ALL


Select '8501201200','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501201900','Other','UNT',0,0,0 UNION ALL

Select '8501202100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501202900','Other','UNT',0,0,0 UNION ALL


Select '8501313000','Motors of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501314000','Other motors ','UNT',0,0,0 UNION ALL
Select '8501315000','Generators','UNT',0,0,0 UNION ALL


Select '8501322100','Motors of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501322200','Other motors','UNT',0,0,0 UNION ALL
Select '8501322300','Generators','UNT',0,0,0 UNION ALL

Select '8501323100','Motors of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501323200','Other motors','UNT',0,0,0 UNION ALL
Select '8501323300','Generators','UNT',0,0,0 UNION ALL
Select '8501330000','Of an output exceeding 75 kW but not exceeding 375 kW ','UNT',0,0,0 UNION ALL
Select '8501340000','Of an output exceeding 375 kW','UNT',0,0,0 UNION ALL


Select '8501401100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501401900','Other','UNT',0,0,0 UNION ALL

Select '8501402100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501402900','Other','UNT',0,0,0 UNION ALL


Select '8501511100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',0,0,0 UNION ALL
Select '8501511900','Other','UNT',0,0,0 UNION ALL


Select '8501521100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',15,0,0 UNION ALL
Select '8501521900','Other','UNT',15,0,0 UNION ALL

Select '8501522100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',15,0,0 UNION ALL
Select '8501522900','Other','UNT',15,0,0 UNION ALL

Select '8501523100','Of a kind used for the goods of heading 84.15, 84.18, 84.50, 85.09 or 85.16','UNT',15,0,0 UNION ALL
Select '8501523900','Other','UNT',15,0,0 UNION ALL
Select '8501530000','Of an output exceeding 75 kW','UNT',0,0,0 UNION ALL


Select '8501611000','Of an output not exceeding 12.5 kVA','UNT',0,0,0 UNION ALL
Select '8501612000','Of an output exceeding 12.5 kVA','UNT',0,0,0 UNION ALL

Select '8501621000','Of an output exceeding 75 kVA but not exceeding 150 kVA','UNT',0,0,0 UNION ALL
Select '8501622000','Of an output exceeding 150 kVA but not exceeding 375 kVA','UNT',0,0,0 UNION ALL
Select '8501630000','Of  an  output  exceeding  375 kVA but not exceeding 750 kVA ','UNT',0,0,0 UNION ALL
Select '8501640000','Of an output exceeding 750 kVA','UNT',0,0,0 UNION ALL


Select '8502110000','Of an output not exceeding 75 kVA','UNT',0,0,0 UNION ALL

Select '8502121000','Of an ouput exceeding 75 kVA but not exceeding 125 kVA','UNT',0,0,0 UNION ALL
Select '8502122000','Of an output exceeding 125 kVA but not exceeding 375 kVA','UNT',0,0,0 UNION ALL

Select '8502132000','Of an output of 12,500 kVA or more','UNT',0,0,0 UNION ALL
Select '8502139000','Other','UNT',0,0,0 UNION ALL

Select '8502201000','Of an output not exceeding 75 kVA','UNT',0,0,0 UNION ALL
Select '8502202000','Of an output exceeding 75 kVA but not exceeding 100 kVA','UNT',0,0,0 UNION ALL
Select '8502203000','Of an output exceeding 100 kVA but not exceeding 10,000 kVA ','UNT',0,0,0 UNION ALL

Select '8502204200','Of an output of 12,500 kVA or more','UNT',0,0,0 UNION ALL
Select '8502204900','Other','UNT',0,0,0 UNION ALL


Select '8502311000','Of an output not exceeding 10,000 kVA','UNT',0,0,0 UNION ALL
Select '8502312000','Of an output exceeding 10,000 kVA','UNT',0,0,0 UNION ALL

Select '8502391000','Of an output not exceeding 10 kVA','UNT',0,0,0 UNION ALL
Select '8502392000','Of an output exceeding 10 kVA but not exceeding 10,000 kVA ','UNT',0,0,0 UNION ALL

Select '8502393200','Of an output of 12,500 kVA  or more','UNT',0,0,0 UNION ALL
Select '8502393900','Other','UNT',0,0,0 UNION ALL
Select '8502400000','Electric rotary converters','UNT',0,0,0 UNION ALL


Select '8503002000','Parts of generators (including generating sets) of heading 85.01 or 85.02, of an output of 12,500 kVA or more','KGM',0,0,0 UNION ALL
Select '8503009000','Other','KGM',0,0,0 UNION ALL

Select '8504100000','Ballasts for discharge lamps or tubes ','UNT',5,0,0 UNION ALL



Select '8504211100','Instrument transformers with a power handling capacity not exceeding 1 kVA and of a high side voltage of 110kV or more','UNT',5,0,0 UNION ALL
Select '8504211900','Other','UNT',5,0,0 UNION ALL

Select '8504219200','Having a power handling capacity exceeding 10 kVA and of a high side voltage of 110kV or more','UNT',5,0,0 UNION ALL
Select '8504219300','Having a power handling capacity exceeding 10 kVA and of a high side voltage of 66kV or more, but less than 110kV','UNT',5,0,0 UNION ALL
Select '8504219900','Other','UNT',5,0,0 UNION ALL


Select '8504221100','Of a high side voltage of 66 kV or more','UNT',5,0,0 UNION ALL
Select '8504221900','Other','UNT',5,0,0 UNION ALL

Select '8504229200','Of a high side voltage of 110 kV or more','UNT',5,0,0 UNION ALL
Select '8504229300','Of a high side voltage of 66kV or more, but less than 110 kV','UNT',5,0,0 UNION ALL
Select '8504229900','Other','UNT',5,0,0 UNION ALL

Select '8504231000','Having a power handling capacity not exceeding 15,000 kVA','UNT',5,0,0 UNION ALL

Select '8504232100','Not exceeding 20,000 kVA','UNT',5,0,0 UNION ALL
Select '8504232200','Exceeding 20,000 kVA but not exceeding 30,000 kVA','UNT',5,0,0 UNION ALL
Select '8504232900','Other','UNT',5,0,0 UNION ALL



Select '8504311100','With a voltage rating of 110 kV or more','UNT',0,0,0 UNION ALL
Select '8504311200','With a voltage rating of 66 kV or more, but less than 110 kV','UNT',0,0,0 UNION ALL
Select '8504311300','With a voltage rating of 1 kV or more, but less than 66 kV','UNT',0,0,0 UNION ALL
Select '8504311900','Other','UNT',0,0,0 UNION ALL


Select '8504312100','Ring type current transformers with a voltage rating not exceeding 220 kV','UNT',0,0,0 UNION ALL
Select '8504312200','Other','UNT',0,0,0 UNION ALL
Select '8504312300','With a voltage rating of 66 kV or more, but less than 110 kV','UNT',0,0,0 UNION ALL
Select '8504312400','With a voltage rating of 1 kV or more, but less than 66 kV','UNT',0,0,0 UNION ALL
Select '8504312900','Other','UNT',0,0,0 UNION ALL
Select '8504313000','Flyback transformers','UNT',0,0,0 UNION ALL
Select '8504314000','Intermediate frequency transformers','UNT',0,0,0 UNION ALL

Select '8504319100','Of a kind used with toys, scale models or similar recreational models','UNT',5,0,0 UNION ALL
Select '8504319200','Other matching transformers','UNT',5,0,0 UNION ALL
Select '8504319300','Step up / down transformers; slide regulators','UNT',0,0,0 UNION ALL
Select '8504319900','Other','UNT',0,0,0 UNION ALL


Select '8504321100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504321900','Other','UNT',5,0,0 UNION ALL
Select '8504322000','Other, of a kind used with toys, scale models or similar recreational models','UNT',0,0,0 UNION ALL
Select '8504323000','Other, having a minimum frequency of 3 MHz','UNT',5,0,0 UNION ALL

Select '8504324100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504324900','Other','UNT',5,0,0 UNION ALL

Select '8504325100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504325900','Other','UNT',5,0,0 UNION ALL


Select '8504331100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504331900','Other','UNT',5,0,0 UNION ALL

Select '8504339100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504339900','Other','UNT',5,0,0 UNION ALL



Select '8504341100','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504341200','Explosion proof dry type transformers','UNT',5,0,0 UNION ALL
Select '8504341300','Other','UNT',5,0,0 UNION ALL

Select '8504341400','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504341500','Explosion proof dry type transformers','UNT',5,0,0 UNION ALL
Select '8504341600','Other','UNT',5,0,0 UNION ALL


Select '8504342200','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504342300','Explosion proof dry type transformers','UNT',5,0,0 UNION ALL
Select '8504342400','Other','UNT',5,0,0 UNION ALL

Select '8504342500','Matching transformers','UNT',5,0,0 UNION ALL
Select '8504342600','Explosion proof dry type transformers','UNT',5,0,0 UNION ALL
Select '8504342900','Other','UNT',5,0,0 UNION ALL


Select '8504401100','Uninterruptible power supplies (UPS)','UNT',0,0,0 UNION ALL
Select '8504401900','Other','UNT',0,0,0 UNION ALL
Select '8504402000','Battery chargers having a rating exceeding 100 kVA','UNT',0,0,0 UNION ALL
Select '8504403000','Other rectifiers','UNT',0,0,0 UNION ALL
Select '8504404000','Inverters','UNT',0,0,0 UNION ALL
Select '8504409000','Other','UNT',0,0,0 UNION ALL

Select '8504501000','Inductors for power supplies for automatic data processing machines and units thereof, and for telecommunication apparatus ','UNT',0,0,0 UNION ALL
Select '8504502000','Chip type fixed inductors','UNT',0,0,0 UNION ALL

Select '8504509300','Having a power handling capacity not exceeding 2,500 kVA','UNT',0,0,0 UNION ALL
Select '8504509400','Having a power handling capacity exceeding 2,500 kVA but not exceeding 10,000 kVA','UNT',0,0,0 UNION ALL
Select '8504509500','Having a power handling capacity exceeding 10,000 kVA','UNT',0,0,0 UNION ALL

Select '8504901000','Of goods of subheading of 8504.10','KGM',0,0,0 UNION ALL
Select '8504902000','Printed circuit assemblies for the goods of subheading  8504.40.11, 8504.40.19 or 8504.50.10','KGM',0,0,0 UNION ALL

Select '8504903100','Radiator panels; flat tube radiator assemblies of a kind used for distribution and power transformers ','KGM',0,0,0 UNION ALL
Select '8504903900','Other','KGM',0,0,0 UNION ALL

Select '8504904100','Radiator panels; flat tube radiator assemblies of a kind used for distribution and power transformers ','KGM',0,0,0 UNION ALL
Select '8504904900','Other ','KGM',0,0,0 UNION ALL
Select '8504909000','Other','KGM',0,0,0 UNION ALL


Select '8505110000','Of metal','KGM',0,0,0 UNION ALL
Select '8505190000','Other','KGM',0,0,0 UNION ALL
Select '8505200000','Electromagnetic couplings, clutches and brakes','KGM',0,0,0 UNION ALL
Select '8505900000','Other, including parts','KGM',0,0,0 UNION ALL


Select '8506101000','Having an external volume not exceeding 300 cm3','UNT',0,0,0 UNION ALL
Select '8506109000','Other','UNT',0,0,0 UNION ALL
Select '8506300000','Mercuric oxide','UNT',0,0,0 UNION ALL
Select '8506400000','Silver oxide','UNT',0,0,0 UNION ALL
Select '8506500000','Lithium','UNT',0,0,0 UNION ALL

Select '8506601000','Having an external volume not exceeding 300 cm3','UNT',0,0,0 UNION ALL
Select '8506609000','Other','UNT',0,0,0 UNION ALL

Select '8506801000','Zinc carbon, having an external volume not exceeding 300 cm3','UNT',0,0,0 UNION ALL
Select '8506802000','Zinc carbon, having an external volume exceeding 300 cm3','UNT',0,0,0 UNION ALL

Select '8506809100','Having an external volume not exceeding 300 cm3','UNT',0,0,0 UNION ALL
Select '8506809900','Other','UNT',0,0,0 UNION ALL
Select '8506900000','Parts','KGM',0,0,0 UNION ALL


Select '8507101000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL


Select '8507109200','Of a height (excluding  terminals and handles) not exceeding 13 cm','UNT',25,0,0 UNION ALL
Select '8507109500','Of  a height (excluding  terminals and handles) exceeding 13 cm but not exceeding 23 cm','UNT',20,0,0 UNION ALL
Select '8507109600','Of  a height (excluding  terminals and handles) exceeding  23 cm','UNT',20,0,0 UNION ALL

Select '8507109700','Of a height (excluding  terminals and handles) not exceeding 13 cm','UNT',20,0,0 UNION ALL
Select '8507109800','Of  a height (excluding  terminals and handles) exceeding 13 cm but not exceeding 23 cm','UNT',20,0,0 UNION ALL
Select '8507109900','Of  a height (excluding  terminals and handles) exceeding  23 cm','UNT',20,0,0 UNION ALL

Select '8507201000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL


Select '8507209400','Of a height (excluding  terminals and handles) not exceeding 13 cm','UNT',20,0,0 UNION ALL
Select '8507209500','Of  a height (excluding  terminals and handles) exceeding 13 cm but not exceeding 23 cm','UNT',25,0,0 UNION ALL
Select '8507209600','Of  a height (excluding  terminals and handles) exceeding  23 cm','UNT',20,0,0 UNION ALL

Select '8507209700','Of a height (excluding  terminals and handles) not exceeding 13 cm','UNT',20,0,0 UNION ALL
Select '8507209800','Of  a height (excluding  terminals and handles) exceeding 13 cm but not exceeding 23 cm','UNT',20,0,0 UNION ALL
Select '8507209900','Of  a height (excluding  terminals and handles) exceeding  23 cm','UNT',20,0,0 UNION ALL

Select '8507301000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL
Select '8507309000','Other','UNT',20,0,0 UNION ALL

Select '8507401000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL
Select '8507409000','Other','UNT',20,0,0 UNION ALL

Select '8507501000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL
Select '8507509000','Other','UNT',20,0,0 UNION ALL

Select '8507601000','Of a kind used for laptops including notebooks and subnotebooks','UNT',0,0,0 UNION ALL

Select '8507602000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL
Select '8507609000','Other','UNT',20,0,0 UNION ALL

Select '8507801000','Of a kind used for aircraft','UNT',0,0,0 UNION ALL
Select '8507802000','Of a kind used for laptops including notebooks and subnotebooks','UNT',0,0,0 UNION ALL
Select '8507809000','Other','UNT',20,0,0 UNION ALL


Select '8507901100','Of goods of subheading 8507.10.92, 8507.10.95, 8507.10.96, 8507.10.97, 8507.10.98 or 8507.10.99','KGM',0,0,0 UNION ALL
Select '8507901200','Of a kind used for aircraft','KGM',0,0,0 UNION ALL
Select '8507901900','Other','KGM',0,0,0 UNION ALL

Select '8507909100','Of a kind used for aircraft','KGM',0,0,0 UNION ALL
Select '8507909200','Battery separators, ready for use, of materials other than poly(vinyl chloride) ','KGM',0,0,0 UNION ALL
Select '8507909300','Other, of goods of subheading 8507.10.92, 8507.10.95, 8507.10.96, 8507.10.97, 8507.10.98 or 8507.10.99','KGM',0,0,0 UNION ALL
Select '8507909900','Other','KGM',0,0,0 UNION ALL


Select '8508110000','Of a power not exceeding 1,500 W and having a dust bag  or  other receptacle capacity not exceeding 20 l','UNT',20,0,0 UNION ALL

Select '8508191000','Of a kind suitable for domestic use','UNT',20,0,0 UNION ALL
Select '8508199000','Other','UNT',20,0,0 UNION ALL
Select '8508600000','Other vacuum cleaners','UNT',0,0,0 UNION ALL

Select '8508701000','Of vacuum cleaners of subheading 8508.11.00 or 8508.19.10 ','KGM',0,0,0 UNION ALL
Select '8508709000','Other','KGM',0,0,0 UNION ALL

Select '8509400000','Food grinders and mixers; fruit or vegetable juice extractors','UNT',20,0,0 UNION ALL

Select '8509801000','Floor polishers','UNT',20,0,0 UNION ALL
Select '8509802000','Kitchen waste disposers','UNT',20,0,0 UNION ALL
Select '8509809000','Other ','UNT',20,0,0 UNION ALL

Select '8509901000','Of goods of subheading 8509.80.10','KGM',0,0,0 UNION ALL
Select '8509909000','Other ','KGM',0,0,0 UNION ALL

Select '8510100000','Shavers','UNT',0,0,0 UNION ALL
Select '8510200000','Hair clippers','UNT',0,0,0 UNION ALL
Select '8510300000','Hairremoving appliances','UNT',0,0,0 UNION ALL
Select '8510900000','Parts','KGM',0,0,0 UNION ALL


Select '8511101000','Of a kind suitable for aircraft engines','UNT',10,0,0 UNION ALL
Select '8511102000','Of a kind suitable for motor vehicle engines','UNT',10,0,0 UNION ALL
Select '8511109000','Other','UNT',10,0,0 UNION ALL

Select '8511201000','Of a kind suitable for aircraft engines','UNT',5,0,0 UNION ALL

Select '8511202100','Unassembled','UNT',5,0,0 UNION ALL
Select '8511202900','Other','UNT',5,0,0 UNION ALL

Select '8511209100','Unassembled','UNT',5,0,0 UNION ALL
Select '8511209900','Other','UNT',5,0,0 UNION ALL

Select '8511303000','Of a kind suitable for aircraft engines','UNT',0,0,0 UNION ALL

Select '8511304100','Unassembled','UNT',0,0,0 UNION ALL
Select '8511304900','Other','UNT',0,0,0 UNION ALL

Select '8511309100','Unassembled','UNT',0,0,0 UNION ALL
Select '8511309900','Other','UNT',0,0,0 UNION ALL

Select '8511401000','Of a kind used for aircraft engines','UNT',0,0,0 UNION ALL

Select '8511402100','For engines of vehicles of heading 87.02, 87.03, 87.04 or 87.05','UNT',0,0,0 UNION ALL
Select '8511402900','Other','UNT',0,0,0 UNION ALL

Select '8511403100','For engines of vehicles of heading 87.01','UNT',0,0,0 UNION ALL
Select '8511403200','For engines of vehicles of heading 87.02, 87.03 or 87.04','UNT',0,0,0 UNION ALL
Select '8511403300','For engines of vehicles of heading 87.05','UNT',0,0,0 UNION ALL

Select '8511409100','For engines of vehicles of heading 87.02, 87.03, 87.04 or 87.05','UNT',0,0,0 UNION ALL
Select '8511409900','Other','UNT',0,0,0 UNION ALL

Select '8511501000','Of a kind used for aircraft engines','UNT',0,0,0 UNION ALL

Select '8511502100','For engines of vehicles of heading 87.02, 87.03, 87.04 or 87.05','UNT',0,0,0 UNION ALL
Select '8511502900','Other','UNT',0,0,0 UNION ALL

Select '8511503100','For engines of vehicles of heading 87.01','UNT',0,0,0 UNION ALL
Select '8511503200','For engines of vehicles of heading 87.02, 87.03 or 87.04','UNT',0,0,0 UNION ALL
Select '8511503300','For engines of vehicles of heading 87.05','UNT',0,0,0 UNION ALL

Select '8511509100','For engines of vehicles of heading 87.02, 87.03, 87.04 or 87.05','UNT',0,0,0 UNION ALL
Select '8511509900','Other','UNT',0,0,0 UNION ALL

Select '8511801000','Of a kind used for aircraft engines','UNT',0,0,0 UNION ALL
Select '8511802000','Of a kind suitable for motor vehicles engines','UNT',0,0,0 UNION ALL
Select '8511809000','Other','UNT',0,0,0 UNION ALL

Select '8511901000','Of a kind used for aircraft engines','UNT',0,0,0 UNION ALL
Select '8511902000','Of a kind suitable for motor vehicles engines','UNT',0,0,0 UNION ALL
Select '8511909000','Other','UNT',0,0,0 UNION ALL

Select '8512100000','Lighting or visual signalling equipment of a kind used on bicycles','UNT',0,0,0 UNION ALL

Select '8512202000','Unassembled lighting or visual signalling equipment','UNT',0,0,0 UNION ALL

Select '8512209100','For motorcycles','UNT',0,0,0 UNION ALL
Select '8512209900','Other','UNT',0,0,0 UNION ALL

Select '8512301000','Horns and sirens, assembled','UNT',0,0,0 UNION ALL
Select '8512302000','Unassembled sound signalling equipment','UNT',0,0,0 UNION ALL

Select '8512309100','Obstacle detection (warning) devices for vehicles ','UNT',0,0,0 UNION ALL
Select '8512309900','Other','UNT',0,0,0 UNION ALL
Select '8512400000','Windscreen wipers, defrosters and demisters','UNT',0,0,0 UNION ALL

Select '8512901000','Of goods of subheading 8512.10','KGM',0,0,0 UNION ALL
Select '8512902000','Of goods of subheading 8512.20, 8512.30 or 8512.40','KGM',0,0,0 UNION ALL


Select '8513103000','Miners"helmet lamps and quarrymen''s lamps','UNT',0,0,0 UNION ALL
Select '8513109000','Other','UNT',0,0,0 UNION ALL

Select '8513901000','Of miners"helmet lamps or quarrymen''s lamps','KGM',0,0,0 UNION ALL
Select '8513903000','Flashlight reflectors; flashlight switch slides of plastics','KGM',0,0,0 UNION ALL
Select '8513909000','Other','KGM',0,0,0 UNION ALL

Select '8514100000','Resistance heated furnaces and ovens','UNT',0,0,0 UNION ALL

Select '8514202000','Electric furnaces or ovens for the manufacture of printed circuit boards/printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '8514209000','Other','UNT',0,0,0 UNION ALL

Select '8514302000','Electric furnaces or ovens for the manufacture of printed circuit boards/printed wiring boards or printed circuits  assemblies   ','UNT',0,0,0 UNION ALL
Select '8514309000','Other','UNT',0,0,0 UNION ALL
Select '8514400000','Other equipment for the heat treatment of materials by  induction or dielectric loss','UNT',0,0,0 UNION ALL

Select '8514902000','Parts of industrial or laboratory electric furnaces or ovens for the manufacture of printed circuit boards/printed wiring boards or printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '8514909000','Other ','KGM',0,0,0 UNION ALL


Select '8515110000','Soldering irons and guns','UNT',0,0,0 UNION ALL

Select '8515191000','Machines and apparatus for soldering components on printed circuit boards/printed wiring boards','UNT',0,0,0 UNION ALL
Select '8515199000','Other ','UNT',0,0,0 UNION ALL

Select '8515210000','Fully or partly automatic','UNT',0,0,0 UNION ALL
Select '8515290000','Other','UNT',0,0,0 UNION ALL

Select '8515310000',' Fully or partly automatic','UNT',0,0,0 UNION ALL

Select '8515391000',' AC arc welders, transformer type','UNT',0,0,0 UNION ALL
Select '8515399000','Other','UNT',0,0,0 UNION ALL

Select '8515801000','Electric machines and apparatus for hot spraying of metals or sintered metal carbides','UNT',0,0,0 UNION ALL
Select '8515809000','Other','UNT',0,0,0 UNION ALL

Select '8515901000','Of AC arc welders, transformer type','KGM',0,0,0 UNION ALL
Select '8515902000','Parts of machine apparatus for soldering components on printed circuit boards/printed wiring boards','KGM',0,0,0 UNION ALL
Select '8515909000','Other','KGM',0,0,0 UNION ALL



Select '8516101100','Water dispenser fitted only with water heater for domestic use','UNT',20,0,0 UNION ALL
Select '8516101900','Other','UNT',20,0,0 UNION ALL
Select '8516103000','Immersion heaters','UNT',0,0,0 UNION ALL

Select '8516210000','Storage heating radiators','UNT',20,0,0 UNION ALL
Select '8516290000','Other','UNT',20,0,0 UNION ALL

Select '8516310000','Hair dryers','UNT',0,0,0 UNION ALL
Select '8516320000','Other hairdressing apparatus','UNT',0,0,0 UNION ALL
Select '8516330000','Handdrying apparatus','UNT',20,0,0 UNION ALL

Select '8516401000','Of a kind designed to use steam from industrial boilers','UNT',20,0,0 UNION ALL
Select '8516409000','Other','UNT',20,0,0 UNION ALL
Select '8516500000','Microwave ovens','UNT',20,0,0 UNION ALL

Select '8516601000','Rice cookers ','UNT',20,0,0 UNION ALL
Select '8516609000','Other','UNT',20,0,0 UNION ALL

Select '8516710000','Coffee or tea makers','UNT',20,0,0 UNION ALL
Select '8516720000','Toasters','UNT',20,0,0 UNION ALL

Select '8516791000','Kettles ','UNT',20,0,0 UNION ALL
Select '8516799000','Other','UNT',20,0,0 UNION ALL

Select '8516801000','For typefounding or typesetting machines; for industrial furnaces ','UNT',0,0,0 UNION ALL
Select '8516803000','For domestic appliances','UNT',0,0,0 UNION ALL
Select '8516809000','Other','UNT',0,0,0 UNION ALL


Select '8516902100','Sealed hotplates for domestic appliances','KGM',0,0,0 UNION ALL
Select '8516902900','Other','KGM',0,0,0 UNION ALL
Select '8516903000','Of goods of subheading 8516.10','KGM',0,0,0 UNION ALL
Select '8516904000','Of electric heating resistors for typefounding or typesetting machines','KGM',0,0,0 UNION ALL
Select '8516909000','Other','KGM',0,0,0 UNION ALL


Select '8517110000','Line telephone sets with cordless handsets','UNT',0,0,0 UNION ALL
Select '8517120000','Telephones for cellular networks or for other wireless networks','UNT',0,0,0 UNION ALL
Select '8517180000','Other','UNT',0,0,0 UNION ALL

Select '8517610000','Base stations','UNT',0,0,0 UNION ALL

Select '8517621000','Radio transmitters and radio receivers of a kind used for simultaneous interpretation at multilingual conferences','UNT',0,0,0 UNION ALL

Select '8517622100','Control and adaptor units, including gateways, bridges and routers','UNT',0,0,0 UNION ALL
Select '8517622900','Other','UNT',0,0,0 UNION ALL
Select '8517623000','Telephonic or telegraphic switching apparatus','UNT',0,0,0 UNION ALL

Select '8517624100','Modems including cable modems and modem cards','UNT',0,0,0 UNION ALL
Select '8517624200','Concentrators or multiplexers','UNT',0,0,0 UNION ALL
Select '8517624900','Other','UNT',0,0,0 UNION ALL

Select '8517625100','Wireless LANs','UNT',0,0,0 UNION ALL
Select '8517625200','Transmission and reception apparatus of a kind used for  simultaneous interpretation at multilingual conferences','UNT',0,0,0 UNION ALL
Select '8517625300','Other transmission apparatus for radiotelephony or   radiotelegraphy','UNT',0,0,0 UNION ALL
Select '8517625900','Other','UNT',0,0,0 UNION ALL

Select '8517626100','For radiotelephony or radiotelegraphy','UNT',0,0,0 UNION ALL
Select '8517626900','Other','UNT',0,0,0 UNION ALL

Select '8517629100','Portable receivers for calling, alerting or paging and paging alert devices, including pagers','UNT',0,0,0 UNION ALL
Select '8517629200','For radiotelephony or radiotelegraphy','UNT',0,0,0 UNION ALL
Select '8517629900','Other','UNT',0,0,0 UNION ALL
Select '8517690000','Other','UNT',0,0,0 UNION ALL

Select '8517701000','Of control and adaptor units including gateways, bridges and routers','KGM',0,0,0 UNION ALL

Select '8517702100','Of cellular telephones','KGM',0,0,0 UNION ALL
Select '8517702900','Other','KGM',0,0,0 UNION ALL

Select '8517703100','Of goods for line telephony or line telegraphy','KGM',0,0,0 UNION ALL
Select '8517703200','Of goods for radiotelephony or radiotelegraphy','KGM',0,0,0 UNION ALL
Select '8517703900','Other','KGM',0,0,0 UNION ALL
Select '8517704000','Aerials or antennae of a kind used with apparatus for radiotelephony and radiotelegraphy','KGM',0,0,0 UNION ALL

Select '8517709100','Of goods for line telephony or line telegraphy','KGM',0,0,0 UNION ALL
Select '8517709200','Of goods for radiotelephony or radiotelegraphy','KGM',0,0,0 UNION ALL
Select '8517709900','Other','KGM',0,0,0 UNION ALL



Select '8518101100','Microphones having a frequency range of 300 Hz to 3,400 Hz, with a diameter not exceeding 10 mm and a  height not exceeding 3 mm, for telecommunication use','UNT',0,0,0 UNION ALL
Select '8518101900','Other microphones, whether or not with their stands','UNT',0,0,0 UNION ALL
Select '8518109000','Other','UNT',0,0,0 UNION ALL


Select '8518211000','Box speaker type','UNT',15,0,0 UNION ALL
Select '8518219000','Other','UNT',15,0,0 UNION ALL

Select '8518221000','Box speaker type','UNT',15,0,0 UNION ALL
Select '8518229000','Other','UNT',15,0,0 UNION ALL

Select '8518292000','Loudspeakers, without enclosure, having a frequency range of 300 Hz to 3,400 Hz, with a diameter not exceeding 50 mm, for telecommunication use','UNT',0,0,0 UNION ALL
Select '8518299000','Other','UNT',0,0,0 UNION ALL

Select '8518301000','Headphones','UNT',0,0,0 UNION ALL
Select '8518302000','Earphones','UNT',0,0,0 UNION ALL
Select '8518304000','Line telephone handsets','UNT',0,0,0 UNION ALL

Select '8518305100','For goods of subheading 8517.12.00','UNT',0,0,0 UNION ALL
Select '8518305900','Other','UNT',15,0,0 UNION ALL
Select '8518309000','Other','UNT',15,0,0 UNION ALL

Select '8518402000','Used as repeaters in line telephony ','UNT',15,0,0 UNION ALL
Select '8518403000','Used as repeaters in telephony other than line telephony ','UNT',15,0,0 UNION ALL
Select '8518404000','Other, having 6 or more input signal lines, with or without elements for capacity amplifiers','UNT',15,0,0 UNION ALL
Select '8518409000','Other','UNT',15,0,0 UNION ALL

Select '8518501000','Having a power rating of 240 W or more','UNT',15,0,0 UNION ALL
Select '8518502000','Other, with loudspeakers, of a kind suitable for broadcasting, having a voltage rating of 50 V or more but not exceeding 100 V','UNT',15,0,0 UNION ALL
Select '8518509000','Other','UNT',15,0,0 UNION ALL

Select '8518901000','Of goods of subheading 8518.10.11, 8518.29.20, 8518.30.40  or 8518.40.20, including printed circuit assemblies ','KGM',0,0,0 UNION ALL
Select '8518902000','Of goods of subheading 8518.40.40','KGM',0,0,0 UNION ALL
Select '8518903000','Of goods of subheading 8518.21 or 8518.22 ','KGM',0,0,0 UNION ALL
Select '8518904000','Of goods of subheading 8518.29.90','KGM',0,0,0 UNION ALL
Select '8518909000','Other','KGM',0,0,0 UNION ALL


Select '8519201000','Coins, tokens or disc operated record players','UNT',0,0,0 UNION ALL
Select '8519209000','Other','UNT',10,0,0 UNION ALL
Select '8519300000','Turntables (recorddecks)','UNT',0,0,0 UNION ALL
Select '8519500000','Telephone answering machines ','UNT',0,0,0 UNION ALL


Select '8519811000','Pocketsize cassette recorders, the dimensions of which do not exceed 170 mm x 100 mm x 45 mm ','UNT',0,0,0 UNION ALL
Select '8519812000','Cassette recorders, with builtin amplifiers and one or more  builtin loudspeakers, operating only with an external source of power ','UNT',0,0,0 UNION ALL
Select '8519813000','Compact disc players','UNT',7,0,0 UNION ALL

Select '8519814100','Of a kind suitable for cinematography or broadcasting','UNT',0,0,0 UNION ALL
Select '8519814900','Other','UNT',0,0,0 UNION ALL
Select '8519815000','Dictating machines not capable of operating without an  external source of power','UNT',0,0,0 UNION ALL

Select '8519816100','Of a kind suitable for cinematography or broadcasting','UNT',0,0,0 UNION ALL
Select '8519816900','Other','UNT',0,0,0 UNION ALL

Select '8519817100','Of a kind suitable for cinematography or broadcasting','UNT',0,0,0 UNION ALL
Select '8519817900','Other','UNT',0,0,0 UNION ALL

Select '8519819100','Of a kind suitable for cinematography or broadcasting','UNT',0,0,0 UNION ALL
Select '8519819900','Other','UNT',0,0,0 UNION ALL


Select '8519891100','For film of a width of less than 16 mm','UNT',0,0,0 UNION ALL
Select '8519891200','For film of a width of 16 mm or more','UNT',0,0,0 UNION ALL
Select '8519892000','Recordplayers with or without loudspeakers','UNT',0,0,0 UNION ALL
Select '8519893000','Of a kind suitable for cinematography or broadcasting','UNT',0,0,0 UNION ALL
Select '85198990','Other:','',0,0,0 UNION ALL
Select '8519899030','Other sound reproducing apparatus ','UNT',7,0,0 UNION ALL
Select '8519899090','Other','UNT',0,0,0 UNION ALL


Select '8521101000','Of a kind used in cinematography or television broadcasting','UNT',0,0,0 UNION ALL
Select '8521109000','Other','UNT',0,0,0 UNION ALL


Select '8521901100','Of a kind used in cinematography or television broadcasting','UNT',0,0,0 UNION ALL
Select '8521901900','Other','UNT',0,0,0 UNION ALL

Select '8521909100','Of a kind used in cinematography or television broadcasting','UNT',0,0,0 UNION ALL
Select '8521909900','Other','UNT',0,0,0 UNION ALL

Select '8522100000','Pickup cartridges','KGM',0,0,0 UNION ALL

Select '8522902000','Printed circuit board assemblies for telephone answering   machines ','KGM',0,0,0 UNION ALL
Select '8522903000','Printed circuit board assemblies for cinematographic sound recorders or reproducers','KGM',0,0,0 UNION ALL
Select '8522904000','Audio or video tapedecks and compact disc mechanisms','KGM',0,0,0 UNION ALL
Select '8522905000','Audio or video reproduction heads, magnetic type; magnetic erasing heads and rods','KGM',0,0,0 UNION ALL

Select '8522909100','Other parts and accessories of cinematographic sound recorders or reproducers   ','KGM',0,0,0 UNION ALL
Select '8522909200','Other parts of telephone answering machines','KGM',0,0,0 UNION ALL
Select '8522909300','Other parts and accessories for goods of subheading 8519.81 or heading 85.21','KGM',0,0,0 UNION ALL
Select '8522909900','Other','KGM',0,0,0 UNION ALL



Select '8523211000','Unrecorded','UNT',0,0,0 UNION ALL
Select '8523219000','Other','UNT',0,0,0 UNION ALL



Select '8523291100','Computer tapes','UNT',0,0,0 UNION ALL
Select '8523291900','Other','UNT',0,0,0 UNION ALL

Select '8523292100','Video tapes','UNT',0,0,0 UNION ALL
Select '8523292900','Other','UNT',0,0,0 UNION ALL


Select '8523293100','Computer tapes','UNT',0,0,0 UNION ALL
Select '8523293300','Video tapes','UNT',0,0,0 UNION ALL
Select '8523293900','Other','UNT',0,0,0 UNION ALL

Select '8523294100','Computer tapes','UNT',0,0,0 UNION ALL
Select '8523294200','Of a kind suitable for cinematography ','UNT',0,0,0 UNION ALL
Select '8523294300','Other video tapes','UNT',0,0,0 UNION ALL
Select '8523294900','Other','UNT',0,0,0 UNION ALL


Select '8523295100','Computer tapes','UNT',0,0,0 UNION ALL
Select '8523295200','Video tapes','UNT',0,0,0 UNION ALL
Select '8523295900','Other','UNT',0,0,0 UNION ALL

Select '8523296100','Of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523296200','Of a kind suitable for cinematography','UNT',0,0,0 UNION ALL
Select '8523296300','Other video tapes','UNT',0,0,0 UNION ALL
Select '8523296900','Other','UNT',0,0,0 UNION ALL


Select '8523297100','Computer hard disks and diskettes','UNT',0,0,0 UNION ALL
Select '8523297900','Other','UNT',0,0,0 UNION ALL


Select '8523298100','Of a kind suitable for computer use ','UNT',0,0,0 UNION ALL
Select '8523298200','Other','UNT',0,0,0 UNION ALL
Select '8523298300','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523298500','Other, containing cinematographic movies other than newsreels, travelogues, technical, scientific movies, and other documentary movies','UNT',0,0,0 UNION ALL
Select '8523298600','Other, of a kind suitable for cinematography','UNT',0,0,0 UNION ALL
Select '8523298900','Other','UNT',0,0,0 UNION ALL


Select '8523299100','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523299200','Other','UNT',0,0,0 UNION ALL


Select '8523299300','Of a kind suitable for computer use ','UNT',0,0,0 UNION ALL
Select '8523299400','Other','UNT',0,0,0 UNION ALL
Select '8523299500','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523299900','Other','UNT',0,0,0 UNION ALL


Select '8523411000','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523419000','Other','UNT',0,0,0 UNION ALL


Select '8523491100','Of a kind used for reproducing phenomena other than sound or image','UNT',0,0,0 UNION ALL

Select '8523491200','Educational, technical, scientific, historical or cultural discs','UNT',0,0,0 UNION ALL
Select '8523491300','Other','UNT',0,0,0 UNION ALL
Select '8523491400','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523491500','Other, containing cinematographic movies other than newsreels, travelogues, technical, scientific movies, and other documentary movies','UNT',0,0,0 UNION ALL
Select '8523491600','Other, of a kind suitable for cinematography','UNT',0,0,0 UNION ALL
Select '8523491900','Other','UNT',0,0,0 UNION ALL

Select '8523499100','Of a kind used for reproducing phenomena other than sound or image','UNT',0,0,0 UNION ALL
Select '8523499200','Of a kind used for reproducing sound only','UNT',0,0,0 UNION ALL
Select '8523499300','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523499900','Other','UNT',0,0,0 UNION ALL



Select '8523511100','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523511900','Other','UNT',0,0,0 UNION ALL


Select '8523512100','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523512900','Other','UNT',0,0,0 UNION ALL
Select '8523513000','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL

Select '8523519100','Other, containing cinematographic movies other than newsreels, travelogues, technical, scientific movies, and other documentary movies','UNT',0,0,0 UNION ALL
Select '8523519200','Other, of a kind suitable for cinematography','UNT',0,0,0 UNION ALL
Select '8523519900','Other','UNT',0,0,0 UNION ALL
Select '8523520000','"Smart cards"','UNT',0,0,0 UNION ALL

Select '8523591000','Proximity cards and tags ','UNT',0,0,0 UNION ALL

Select '8523592100','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523592900','Other','UNT',0,0,0 UNION ALL

Select '8523593000','Of a kind used for reproducing phenomena other than sound or image','UNT',0,0,0 UNION ALL
Select '8523594000','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523599000','Other','UNT',0,0,0 UNION ALL

Select '8523804000','Gramophone records','UNT',0,0,0 UNION ALL

Select '8523805100','Of a kind suitable for computer use','UNT',0,0,0 UNION ALL
Select '8523805900','Other','UNT',0,0,0 UNION ALL

Select '8523809100','Of a kind used for reproducing phenomena other than sound or image','UNT',0,0,0 UNION ALL
Select '8523809200','Other, of a kind used for reproducing representations of instructions, data, sound and image, recorded in a machine readable binary form, and capable of being manipulated or providing interactivity to a user, by means of an automatic data processing machine; proprietary format storage (recorded) media ','UNT',0,0,0 UNION ALL
Select '8523809900','Other','UNT',0,0,0 UNION ALL

Select '8525500000','Transmission apparatus','UNT',0,0,0 UNION ALL
Select '8525600000','Transmission apparatus incorporating reception apparatus ','UNT',0,0,0 UNION ALL

Select '8525801000','Web cameras ','UNT',0,0,0 UNION ALL

Select '8525803100','Of a kind used in broadcasting ','UNT',0,0,0 UNION ALL
Select '8525803900','Other','UNT',0,0,0 UNION ALL
Select '8525804000','Television cameras','UNT',0,0,0 UNION ALL

Select '8525805100','Digital single lens reflex (DSLR)','UNT',0,0,0 UNION ALL
Select '8525805900','Other','UNT',0,0,0 UNION ALL


Select '8526101000','Radar apparatus, ground based, or of a kind for use in civil aircraft, or of a kind used solely on seagoing vessels ','UNT',0,0,0 UNION ALL
Select '8526109000','Other','UNT',0,0,0 UNION ALL


Select '8526911000','Radio navigational aid apparatus, of a kind for use in civil aircraft, or of a kind used solely on seagoing vessels ','UNT',0,0,0 UNION ALL
Select '8526919000','Other','UNT',0,0,0 UNION ALL
Select '8526920000','Radio remote control apparatus','UNT',0,0,0 UNION ALL


Select '8527120000','Pocketsize radio cassetteplayers','UNT',0,0,0 UNION ALL

Select '8527131000','Portable','UNT',0,0,0 UNION ALL
Select '8527139000','Other','UNT',15,0,0 UNION ALL

Select '8527192000','Portable','UNT',0,0,0 UNION ALL
Select '8527199000','Other','UNT',15,0,0 UNION ALL

Select '85272100','Combined with sound recording or reproducing apparatus:','',0,0,0 UNION ALL
Select '8527210010','Capable of receiving and decoding digital radio data system signals','UNT',15,0,0 UNION ALL
Select '8527210090','Other','UNT',20,0,0 UNION ALL
Select '8527290000','Other','UNT',15,0,0 UNION ALL


Select '8527911000','Portable','UNT',0,0,0 UNION ALL
Select '8527919000','Other','UNT',15,0,0 UNION ALL

Select '8527922000','Mains operated','UNT',15,0,0 UNION ALL
Select '8527929000','Other','UNT',0,0,0 UNION ALL

Select '8527992000','Mains operated','UNT',15,0,0 UNION ALL
Select '8527999000','Other','UNT',0,0,0 UNION ALL


Select '8528420000','Capable of directly connecting to and designed for use with an automatic data processing machine of heading 84.71','UNT',0,0,0 UNION ALL

Select '8528491000','Colour','UNT',18,0,0 UNION ALL
Select '8528492000','Monochrome','UNT',18,0,0 UNION ALL

Select '8528520000','Capable of directly connecting to and designed for use with an automatic data processing machine of heading 84.71','UNT',0,0,0 UNION ALL

Select '8528591000','Colour','UNT',25,0,0 UNION ALL
Select '8528592000','Monochrome','UNT',25,0,0 UNION ALL

Select '8528620000','Capable of directly connecting to and designed for use with an automatic data processing machine of heading 84.71 ','UNT',0,0,0 UNION ALL

Select '8528691000','Having the capability of projecting onto a screen diagonally measuring 300 inches or more','UNT',0,0,0 UNION ALL
Select '8528699000','Other','UNT',0,0,0 UNION ALL



Select '8528711100','Mains operated','UNT',22,0,0 UNION ALL
Select '8528711900','Other','UNT',0,0,0 UNION ALL

Select '8528719100','Mains operated','UNT',22,0,0 UNION ALL
Select '8528719900','Other','UNT',0,0,0 UNION ALL

Select '8528721000','Battery operated','UNT',0,0,0 UNION ALL

Select '8528729100','Cathoderay tube type','UNT',30,0,0 UNION ALL
Select '8528729200','Liquid crystal device (LCD), light emitting diode (LED) and other flat panel display type','UNT',30,0,0 UNION ALL
Select '8528729900','Other','UNT',30,0,0 UNION ALL
Select '8528730000','Other, monochrome','UNT',0,0,0 UNION ALL



Select '8529102100','For television reception','KGM',0,0,0 UNION ALL
Select '8529102900','Other','KGM',0,0,0 UNION ALL
Select '85291030','Telescopic, rabbit and dipole antennae for television or radio receivers:','',0,0,0 UNION ALL
Select '8529103010','For television','KGM',0,0,0 UNION ALL
Select '8529103020','For radio','KGM',0,0,0 UNION ALL
Select '85291040','Aerial filters and separators:','',0,0,0 UNION ALL
Select '8529104010','For television','KGM',0,0,0 UNION ALL
Select '8529104020','For radio','KGM',0,0,0 UNION ALL
Select '85291060','Feed horn (wave guide):','',0,0,0 UNION ALL
Select '8529106010','For television','KGM',0,0,0 UNION ALL
Select '8529106020','For radio','KGM',0,0,0 UNION ALL

Select '85291092','Of a kind used with transmission apparatus for radiobroadcasting or television:','',0,0,0 UNION ALL
Select '8529109210','For television','KGM',0,0,0 UNION ALL
Select '8529109220','For radio','KGM',0,0,0 UNION ALL
Select '8529109900','Other','KGM',0,0,0 UNION ALL

Select '8529902000','Of decoders','KGM',0,0,0 UNION ALL
Select '8529904000','Of digital cameras or video camera recorders','KGM',0,0,0 UNION ALL

Select '8529905100','For goods of subheading 8525.50 or 8525.60','KGM',0,0,0 UNION ALL
Select '8529905200','For goods of subheading 8527.13, 8527.19, 8527.21, 8527.29, 8527.91 or 8527.99','KGM',0,0,0 UNION ALL

Select '8529905300','For flat panel displays','KGM',0,0,0 UNION ALL
Select '8529905400','Other, for television receivers','KGM',0,0,0 UNION ALL
Select '8529905500','Other','KGM',0,0,0 UNION ALL
Select '8529905900','Other','KGM',0,0,0 UNION ALL

Select '8529909100','For television receivers','KGM',0,0,0 UNION ALL
Select '8529909400','For flat panel displays','KGM',0,0,0 UNION ALL
Select '8529909900','Other','KGM',0,0,0 UNION ALL

Select '8530100000','Equipment for railways or tramways','UNT',0,0,0 UNION ALL
Select '8530800000','Other equipment','UNT',0,0,0 UNION ALL
Select '8530900000','Parts','KGM',0,0,0 UNION ALL


Select '8531101000','Burglar alarms','UNT',0,0,0 UNION ALL
Select '8531102000','Fire alarms','UNT',0,0,0 UNION ALL
Select '8531103000','Smoke alarms; portable personal alarms (shrill alarms)','UNT',0,0,0 UNION ALL
Select '8531109000','Other','UNT',0,0,0 UNION ALL
Select '8531200000','Indicator panels incorporating liquid crystal devices (LCD) or  light emitting diodes (LED)  ','UNT',0,0,0 UNION ALL

Select '8531801000','Electronic bells and other sound signalling apparatus','UNT',0,0,0 UNION ALL

Select '8531802100','Vacuum fluorescent display panels','UNT',0,0,0 UNION ALL
Select '8531802900','Other','UNT',0,0,0 UNION ALL
Select '8531809000','Other','UNT',0,0,0 UNION ALL

Select '8531901000','Parts including printed circuit assemblies of subheading 8531.20, 8531.80.21 or 8531.80.29 ','KGM',0,0,0 UNION ALL
Select '8531902000','Of door bells  or other  door sound signalling apparatus','KGM',0,0,0 UNION ALL
Select '8531903000','Of other bells or sound signaling apparatus','KGM',0,0,0 UNION ALL
Select '8531909000','Other','KGM',0,0,0 UNION ALL

Select '8532100000','Fixed capacitors designed for use in 50/60 Hz circuits and   having a reactive power handling capacity of not less than  0.5 kvar (power capacitors)','KGM',0,0,0 UNION ALL

Select '8532210000','Tantalum ','KGM',0,0,0 UNION ALL
Select '8532220000','Aluminium electrolytic ','KGM',0,0,0 UNION ALL
Select '8532230000','Ceramic dielectric, single layer ','KGM',0,0,0 UNION ALL
Select '8532240000','Ceramic dielectric, multilayer ','KGM',0,0,0 UNION ALL
Select '8532250000','Dielectric of paper or plastics ','KGM',0,0,0 UNION ALL
Select '8532290000','Other ','KGM',0,0,0 UNION ALL
Select '8532300000','Variable or adjustable (preset) capacitors ','KGM',0,0,0 UNION ALL
Select '8532900000','Parts ','KGM',0,0,0 UNION ALL


Select '8533101000','Surface mounted','KGM',0,0,0 UNION ALL
Select '8533109000','Other','KGM',0,0,0 UNION ALL

Select '8533210000','For a power handling capacity not exceeding 20 W ','KGM',0,0,0 UNION ALL
Select '8533290000','Other','KGM',0,0,0 UNION ALL

Select '8533310000','For a power handling capacity not exceeding 20 W ','KGM',0,0,0 UNION ALL
Select '8533390000','Other ','KGM',0,0,0 UNION ALL
Select '8533400000','Other variable resistors, including rheostats and potentiometers ','KGM',0,0,0 UNION ALL
Select '8533900000','Parts ','KGM',0,0,0 UNION ALL

Select '8534001000','Singlesided','KGM',0,0,0 UNION ALL
Select '8534002000','Doublesided','KGM',0,0,0 UNION ALL
Select '8534003000','Multilayer','KGM',0,0,0 UNION ALL
Select '8534009000','Other','KGM',0,0,0 UNION ALL

Select '8535100000','Fuses','KGM',15,0,0 UNION ALL


Select '8535211000','Moulded case type','KGM',15,0,0 UNION ALL
Select '8535212000','Earth leakage circuit breaker ','KGM',15,0,0 UNION ALL
Select '8535219000','Other','KGM',15,0,0 UNION ALL

Select '8535291000','Earth leakage circuit breaker ','KGM',15,0,0 UNION ALL
Select '8535299000','Other','KGM',15,0,0 UNION ALL


Select '8535301100','Disconnectors having a voltage of less than 36 kV','KGM',15,0,0 UNION ALL
Select '8535301900','Other','KGM',15,0,0 UNION ALL
Select '8535302000','For a voltage of 66 kV or more','KGM',15,0,0 UNION ALL
Select '8535309000','Other','KGM',15,0,0 UNION ALL
Select '8535400000','Lightning arresters, voltage limiters and surge suppressors','KGM',0,0,0 UNION ALL

Select '8535901000','Bushing assemblies and tap changer assemblies for electricity distribution or power transformers','KGM',15,0,0 UNION ALL
Select '8535902000','Changeover switches of a kind used for starting electric motors','KGM',15,0,0 UNION ALL
Select '8535909000','Other','KGM',15,0,0 UNION ALL



Select '8536101100','Suitable for use in electric fans','KGM',15,0,0 UNION ALL
Select '8536101200','Other, for a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536101300','Fuse blocks, of a kind used for motor vehicles','KGM',0,0,0 UNION ALL
Select '8536101900','Other','KGM',0,0,0 UNION ALL

Select '8536109100','Suitable for use in electric fans','KGM',15,0,0 UNION ALL
Select '8536109200','Other, for a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536109300','Fuse blocks, of a kind used for motor vehicles','KGM',0,0,0 UNION ALL
Select '8536109900','Other','KGM',0,0,0 UNION ALL


Select '8536201100','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536201200','For a current of 16 A or more, but not more than 32 A','KGM',0,0,0 UNION ALL
Select '8536201300','For a current of more than 32 A, but not more than 1,000 A','KGM',0,0,0 UNION ALL
Select '8536201900','Other','KGM',0,0,0 UNION ALL
Select '8536202000','Of a kind incorporated into electrothermic domestic appliances of heading 85.16  ','KGM',15,0,0 UNION ALL

Select '8536209100','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536209900','Other','KGM',0,0,0 UNION ALL

Select '8536301000','Lightning arresters','KGM',13,0,0 UNION ALL
Select '8536302000','Of a kind used in radio equipment or in electric fans','KGM',0,0,0 UNION ALL
Select '8536309000','Other','KGM',13,0,0 UNION ALL


Select '8536411000','Digital relays','KGM',0,0,0 UNION ALL
Select '8536412000','Of a kind used in radio equipment ','KGM',5,0,0 UNION ALL
Select '8536413000','Of a kind used in electric fans','KGM',15,0,0 UNION ALL
Select '8536414000','Other, for a current of less than 16 A','KGM',15,0,0 UNION ALL

Select '8536419100','Semiconductor or electromagnetic relays of voltage not exceeding 28 V','KGM',0,0,0 UNION ALL
Select '8536419900','Other','KGM',0,0,0 UNION ALL

Select '8536491000','Digital relays','KGM',0,0,0 UNION ALL
Select '8536499000','Other','KGM',0,0,0 UNION ALL

Select '8536502000','Overcurrent and residualcurrent automatic switches','KGM',0,0,0 UNION ALL

Select '8536503200','Of a kind suitable for use in electric fans or in radio equipment','KGM',0,0,0 UNION ALL
Select '8536503300','Other, of a rated current carrying capacity of less than 16 A','KGM',13,0,0 UNION ALL
Select '8536503900','Other','KGM',0,0,0 UNION ALL
Select '8536504000','Miniature switches suitable for use in rice cookers or toaster ovens','KGM',13,0,0 UNION ALL

Select '8536505100','For a current of less than 16 A ','KGM',13,0,0 UNION ALL
Select '8536505900','Other','KGM',0,0,0 UNION ALL

Select '8536506100','For a current of less than 16 A ','KGM',13,0,0 UNION ALL
Select '8536506900','Other','KGM',0,0,0 UNION ALL

Select '8536509200','Of a kind suitable for use in electric fans','KGM',0,0,0 UNION ALL
Select '8536509500','Other, changeover switches of a kind used for starting electric motors; fuse switches ','KGM',15,0,0 UNION ALL
Select '85365099','Other:','',0,0,0 UNION ALL
Select '8536509920','For  current of less than 16 amps ','KGM',13,0,0 UNION ALL
Select '8536509990','Other','KGM',0,0,0 UNION ALL


Select '85366110','Of a kind used for compact lamps or halogen lamps:','',0,0,0 UNION ALL
Select '8536611010','For a current of less than 16 amps ','KGM',15,0,0 UNION ALL
Select '8536611090','Other','KGM',0,0,0 UNION ALL

Select '8536619100','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536619900','Other','KGM',0,0,0 UNION ALL


Select '8536691100','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536691900','Other','KGM',0,0,0 UNION ALL

Select '8536692300','For a current not exceeding 1.5 A ','KGM',15,0,0 UNION ALL
Select '8536692400','For a current exceeding 1.5 A but less than 16 A','KGM',15,0,0 UNION ALL
Select '8536692900','Other','KGM',0,0,0 UNION ALL

Select '8536693200','For a current of less than 16 A ','KGM',15,0,0 UNION ALL
Select '8536693900','Other','KGM',0,0,0 UNION ALL

Select '8536699200','For a current of less than 16 A ','KGM',15,0,0 UNION ALL
Select '8536699900','Other','KGM',0,0,0 UNION ALL

Select '8536701000','Of ceramics','KGM',0,0,0 UNION ALL
Select '8536702000','Of copper','KGM',0,0,0 UNION ALL
Select '8536709000','Other','KGM',0,0,0 UNION ALL


Select '8536901200','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536901900','Other','KGM',0,0,0 UNION ALL

Select '8536902200','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536902900','Other','KGM',0,0,0 UNION ALL

Select '8536903200','For a current of less than 16 A','KGM',15,0,0 UNION ALL
Select '8536903900','Other','KGM',0,0,0 UNION ALL


Select '8536909300','Telephone patch panels','KGM',15,0,0 UNION ALL
Select '8536909400','Other','KGM',15,0,0 UNION ALL
Select '8536909900','Other','KGM',0,0,0 UNION ALL



Select '8537101100','Control panels of a kind suitable for use in distributed control systems ','KGM',15,0,0 UNION ALL
Select '8537101200','Control panels fitted with a programmable processor','KGM',15,0,0 UNION ALL
Select '8537101300','Other control panels of a kind suitable for goods of heading 84.15, 84.18, 84.50, 85.08, 85.09 or 85.16','KGM',15,0,0 UNION ALL
Select '8537101900','Other','KGM',15,0,0 UNION ALL
Select '8537102000','Distribution boards (including back panels and back planes) for use solely or principally with goods of heading 84.71, 85.17 or 85.25 ','KGM',15,0,0 UNION ALL
Select '8537103000','Programmable logic controllers for automated machines for transport, handling and storage of dies for semiconductor devices ','KGM',15,0,0 UNION ALL

Select '8537109100','Of a kind used in radio equipment or in electric fans','KGM',0,0,0 UNION ALL
Select '8537109200','Of a kind suitable for use in distributed control systems','KGM',15,0,0 UNION ALL
Select '8537109900','Other','KGM',15,0,0 UNION ALL


Select '8537201100','Incorporating electrical instruments for breaking, connecting or protecting electrical circuits for a voltage of 66 kV or more','KGM',15,0,0 UNION ALL
Select '8537201900','Other','KGM',15,0,0 UNION ALL

Select '8537202100','Incorporating electrical instruments for breaking, connecting or protecting electrical circuits for a voltage of 66 kV or more','KGM',15,0,0 UNION ALL
Select '8537202900','Other','KGM',15,0,0 UNION ALL
Select '8537209000','Other','KGM',15,0,0 UNION ALL



Select '8538101100','Parts of programmable logic controllers for automated  machines for transport, handling and storage of dies for  semiconductor devices ','KGM',0,0,0 UNION ALL
Select '8538101200','Of a kind used in radio equipment','KGM',0,0,0 UNION ALL
Select '8538101900','Other','KGM',0,0,0 UNION ALL

Select '8538102100','Parts of programmable logic controllers for automated  machines for transport, handling and storage of dies for semiconductor devices ','KGM',0,0,0 UNION ALL
Select '8538102200','Of a kind used in radio equipment','KGM',0,0,0 UNION ALL
Select '8538102900','Other','KGM',0,0,0 UNION ALL


Select '8538901100','Parts including printed circuit assemblies for telephone plugs; connection and contact elements for wires and cables; wafer probers','KGM',0,0,0 UNION ALL
Select '8538901200','Parts of goods of subheading 8536.50.51, 8536.50.59, 8536.69.32, 8536.69.39, 8536.90.12 or 8536.90.19','KGM',0,0,0 UNION ALL
Select '8538901300','Parts of goods of subheading 8537.10.20','KGM',0,0,0 UNION ALL
Select '8538901900','Other','KGM',0,0,0 UNION ALL
Select '8538902000','For a voltage exceeding 1,000 V','KGM',0,0,0 UNION ALL


Select '8539101000','For motor vehicles of Chapter 87','UNT',5,0,0 UNION ALL
Select '8539109000','Other','UNT',15,0,0 UNION ALL


Select '8539212000','Of a kind used in medical equipment','UNT',15,0,0 UNION ALL
Select '8539213000','Of a kind used for motor vehicles','UNT',15,0,0 UNION ALL
Select '8539214000','Other reflector lamp bulbs','UNT',15,0,0 UNION ALL
Select '8539219000','Other','UNT',15,0,0 UNION ALL

Select '8539222000','Of a kind used in medical equipment','UNT',15,0,0 UNION ALL

Select '8539223100','Of a kind used in decorative illumination, of a power not exceeding 60 W','UNT',15,0,0 UNION ALL
Select '8539223200','Of a kind used in decorative illumination, of a power exceeding 60 W','UNT',15,0,0 UNION ALL
Select '8539223300','Other, for domestic lighting','UNT',15,0,0 UNION ALL
Select '8539223900','Other','UNT',15,0,0 UNION ALL

Select '8539229100','Of a kind used in decorative illumination, of a power not exceeding 60 W','UNT',15,0,0 UNION ALL
Select '8539229200','Of a kind used in decorative illumination, of a power exceeding 60 W','UNT',15,0,0 UNION ALL
Select '8539229300','Other, for domestic lighting','UNT',15,0,0 UNION ALL
Select '8539229900','Other','UNT',15,0,0 UNION ALL


Select '8539291100','Operation theater lamps','UNT',15,0,0 UNION ALL
Select '8539291900','Other','UNT',15,0,0 UNION ALL
Select '8539292000','Of a kind used for motor vehicles','UNT',15,0,0 UNION ALL
Select '8539293000','Other reflector lamp bulbs','UNT',15,0,0 UNION ALL

Select '8539294100','Of a kind suitable for medical equipment ','UNT',15,0,0 UNION ALL
Select '8539294900','Other','UNT',15,0,0 UNION ALL
Select '8539295000','Other, having a capacity exceeding 200 W but not exceeding 300 W and a voltage exceeding 100 V ','UNT',15,0,0 UNION ALL
Select '8539296000','Other, having a capacity not exceeding 200 W and a voltage  not exceeding 100 V  ','UNT',15,0,0 UNION ALL
Select '8539299000','Other','UNT',15,0,0 UNION ALL


Select '8539311000','Tubes for compact fluorescent lamps','UNT',15,0,0 UNION ALL
Select '8539312000','Other, straight tubes for other fluorescent lamps','UNT',15,0,0 UNION ALL
Select '8539313000','Compact fluorescent lamps with builtin ballast','UNT',15,0,0 UNION ALL
Select '8539319000','Other','UNT',15,0,0 UNION ALL
Select '8539320000','Mercury or sodium vapour lamps; metal halide lamps','UNT',15,0,0 UNION ALL

Select '8539391000','Tubes for compact fluorescent lamps','UNT',15,0,0 UNION ALL
Select '85393930','Other fluorescent cold cathode types:','',0,0,0 UNION ALL
Select '8539393010','Coldcathode fluorescent lamps (CCFLs) for backlighting of flat panel displays','UNT',12,0,0 UNION ALL
Select '8539393090','Other','UNT',15,0,0 UNION ALL
Select '8539399000','Other','UNT',15,0,0 UNION ALL

Select '8539410000','Arclamps','UNT',5,0,0 UNION ALL
Select '8539490000','Other','UNT',5,0,0 UNION ALL
Select '8539500000','Lightemitting diode (LED) lamps','KGM',0,0,0 UNION ALL

Select '8539901000','Aluminium end caps for fluorescent lamps; aluminium screw caps for incandescent lamps ','KGM',0,0,0 UNION ALL
Select '8539902000','Other, suitable for lamps of motor vehicles','KGM',0,0,0 UNION ALL
Select '8539909000','Other','KGM',0,0,0 UNION ALL


Select '8540110000','Colour','UNT',0,0,0 UNION ALL
Select '8540120000','Monochrome','UNT',0,0,0 UNION ALL
Select '8540200000','Television camera tubes; image converters and intensifiers; other photocathode tubes ','UNT',0,0,0 UNION ALL

Select '8540401000','Data/graphic display tubes, colour, of a kind used for articles of heading 85.25','UNT',0,0,0 UNION ALL
Select '8540409000','Other','UNT',0,0,0 UNION ALL
Select '8540600000','Other cathoderay tubes ','UNT',0,0,0 UNION ALL

Select '8540710000','Magnetrons','UNT',0,0,0 UNION ALL
Select '8540790000','Other','UNT',0,0,0 UNION ALL

Select '8540810000','Receiver or amplifier valves and tubes','UNT',0,0,0 UNION ALL
Select '8540890000','Other','UNT',0,0,0 UNION ALL

Select '8540910000','Of cathoderay tubes','KGM',0,0,0 UNION ALL

Select '8540991000','Of microwave tubes','KGM',0,0,0 UNION ALL
Select '8540999000','Other','KGM',0,0,0 UNION ALL

Select '8541100000','Diodes, other than photosensitive or lightemitting diodes (LED) ','UNT',0,0,0 UNION ALL

Select '8541210000','With a dissipation rate of less than 1 W ','UNT',0,0,0 UNION ALL
Select '8541290000','Other ','UNT',0,0,0 UNION ALL
Select '8541300000','Thyristors, diacs and triacs, other than photosensitive devices ','UNT',0,0,0 UNION ALL

Select '8541401000','Lightemitting diodes','UNT',0,0,0 UNION ALL

Select '8541402100','Photovoltaic cells, not assembled ','UNT',0,0,0 UNION ALL
Select '8541402200','Photovoltaic cells assembled in modules or made up into panels','UNT',0,0,0 UNION ALL
Select '8541402900','Other','UNT',0,0,0 UNION ALL
Select '8541409000','Other','UNT',0,0,0 UNION ALL
Select '8541500000','Other semiconductor devices ','UNT',0,0,0 UNION ALL
Select '8541600000','Mounted piezoelectric crystals ','UNT',0,0,0 UNION ALL
Select '8541900000','Parts ','KGM',0,0,0 UNION ALL


Select '8542310000','Processors and controllers, whether or not combined with memories, converters, logic circuits, amplifiers, clock and timing circuits, or other circuits','UNT',0,0,0 UNION ALL
Select '8542320000','Memories','UNT',0,0,0 UNION ALL
Select '8542330000','Amplifiers','UNT',0,0,0 UNION ALL
Select '8542390000','Other ','UNT',0,0,0 UNION ALL
Select '8542900000','Parts ','KGM',0,0,0 UNION ALL

Select '8543100000','Particle accelerators','UNT',0,0,0 UNION ALL
Select '8543200000','Signal generators','UNT',0,0,0 UNION ALL

Select '8543302000','Wet processing equipment for the application by immersion of chemical or electrochemical solutions, whether or not for the purpose of removing material on printed circuit board/printed wiring board substrates ','UNT',0,0,0 UNION ALL
Select '8543309000','Other','UNT',0,0,0 UNION ALL

Select '8543701000','Electric fence energisers','UNT',5,0,0 UNION ALL
Select '8543702000','Remote control apparatus, other than radio remote control apparatus','UNT',0,0,0 UNION ALL
Select '8543703000','Electrical machines and appartus with translation or dictionary functions ','UNT',0,0,0 UNION ALL
Select '8543704000','Equipment for the removal of dust particles or the elimination of electrostatic charge during the manufacture of printed circuit boards/printed wiring boards or printed circuit assemblies; machines for curing material by ultraviolet light for the manufacture of printed circuit boards/printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '8543709000','Other','UNT',0,0,0 UNION ALL

Select '8543901000','Of goods of subheading 8543.10 or 8543.20','KGM',0,0,0 UNION ALL
Select '8543902000','Of goods of subheading 8543.30.20 ','KGM',0,0,0 UNION ALL
Select '8543903000','Of goods of subheading 8543.70.30 ','KGM',0,0,0 UNION ALL
Select '8543904000','Of goods of subheading 8543.70.40  ','KGM',0,0,0 UNION ALL
Select '8543909000','Other','KGM',0,0,0 UNION ALL



Select '8544112000','With an outer coating or covering of paper, textiles or poly(vinyl chloride)','KGM',5,0,0 UNION ALL
Select '8544113000','With an outer coating of lacquer ','KGM',5,0,0 UNION ALL
Select '8544114000','With an outer coating of enamel','KGM',25,0,0 UNION ALL
Select '8544119000','Other','KGM',5,0,0 UNION ALL
Select '8544190000','Other','KGM',5,0,0 UNION ALL


Select '8544201100','Insulated with rubber or plastics','KGM',30,0,0 UNION ALL
Select '8544201900','Other','KGM',5,0,0 UNION ALL

Select '8544202100','Insulated with rubber or plastics','KGM',30,0,0 UNION ALL
Select '8544202900','Other','KGM',5,0,0 UNION ALL

Select '8544203100','Insulated with rubber or plastics','KGM',30,0,0 UNION ALL
Select '8544203900','Other','KGM',5,0,0 UNION ALL

Select '8544204100','Insulated with rubber or plastics','KGM',30,0,0 UNION ALL
Select '8544204900','Other','KGM',5,0,0 UNION ALL



Select '8544301200','Of a kind use for vehicles of heading 87.02, 87.03, 87.04 or 87.11','KGM',30,0,0 UNION ALL
Select '8544301300','Other','KGM',30,0,0 UNION ALL

Select '8544301400','Of a kind used for vehicles of heading 87.02, 87.03, 87.04 or 87.11','KGM',5,0,0 UNION ALL
Select '8544301900','Other','KGM',5,0,0 UNION ALL

Select '8544309100','Insulated with rubber or plastics','KGM',30,0,0 UNION ALL
Select '8544309900','Other','KGM',5,0,0 UNION ALL



Select '8544421100','Submarine telephone cables; submarine telegraph cables; submarine radio relay cables','KGM',5,0,0 UNION ALL
Select '8544421300','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544421900',' Other','KGM',5,0,0 UNION ALL

Select '8544422100','Submarine telephone cables; submarine telegraph cables; submarine radio relay cables','KGM',5,0,0 UNION ALL
Select '8544422300','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544422900','Other','KGM',5,0,0 UNION ALL


Select '8544423200','For vehicles of heading 87.02, 87.03, 87.04 or 87.11','KGM',30,0,0 UNION ALL
Select '8544423300','Other','KGM',30,0,0 UNION ALL

Select '8544423400','For vehicles of heading 87.02, 87.03, 87.04 or 87.11','KGM',30,0,0 UNION ALL
Select '8544423900','Other','KGM',30,0,0 UNION ALL

Select '8544429400','Electric cables insulated with plastics, having a core diameter not exceeding 5 mm','KGM',30,0,0 UNION ALL
Select '8544429500','Electric cables insulated with plastics, having a core diameter more than 5 mm but not exceeding 19.5 mm','KGM',30,0,0 UNION ALL
Select '8544429600','Other electric cables insulated with plastics','KGM',30,0,0 UNION ALL
Select '8544429700','Electric cables insulated with rubber or paper','KGM',30,0,0 UNION ALL
Select '85444298','Flat data cables having two lines or more:','',0,0,0 UNION ALL
Select '8544429810','Insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544429890','Other','KGM',5,0,0 UNION ALL
Select '8544429900','Other','KGM',5,0,0 UNION ALL


Select '8544491100','Submarine telephone cables; submarine telegraph cables; submarine radio relay cables','KGM',5,0,0 UNION ALL
Select '8544491300','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544491900','Other','KGM',5,0,0 UNION ALL

Select '85444921','Shielded wire of a kind used in the manufacture of automotive wiring harnesses:','',0,0,0 UNION ALL
Select '8544492110','Insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544492190','Other','KGM',5,0,0 UNION ALL

Select '8544492200','Electric cables insulated with plastics having a core diameter not exceeding 19.5 mm','KGM',30,0,0 UNION ALL
Select '8544492300','Other electric cables insulated with plastics','KGM',30,0,0 UNION ALL
Select '8544492400','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544492900','Other','KGM',5,0,0 UNION ALL

Select '8544493100','Submarine telephone cables; submarine telegraph cables; submarine radio relay cables','KGM',5,0,0 UNION ALL
Select '8544493200','Other, insulated with plastics','KGM',30,0,0 UNION ALL
Select '85444939','Other:','',0,0,0 UNION ALL
Select '8544493910','Insulated with rubber or paper','KGM',30,0,0 UNION ALL
Select '8544493920','Other','KGM',5,0,0 UNION ALL

Select '8544494100','Cables insulated with plastics','KGM',30,0,0 UNION ALL
Select '8544494200','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544494900','Other','KGM',5,0,0 UNION ALL


Select '8544601100','Cables insulated with plastics having a core diameter of less than 22.7 mm','KGM',30,0,0 UNION ALL
Select '8544601200','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544601900','Other','KGM',5,0,0 UNION ALL

Select '8544602100','Cables insulated with plastics having a core diameter of less than 22.7 mm','KGM',30,0,0 UNION ALL
Select '8544602200','Other, insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544602900','Other','KGM',5,0,0 UNION ALL

Select '8544603100','Insulated with rubber, plastics or paper','KGM',30,0,0 UNION ALL
Select '8544603900','Other','KGM',5,0,0 UNION ALL

Select '8544701000','Submarine telephone cables; submarine telegraph cables; submarine radio relay cables','KGM',0,0,0 UNION ALL
Select '8544709000','Other','KGM',0,0,0 UNION ALL


Select '8545110000','Of a kind used for furnaces','KGM',0,0,0 UNION ALL
Select '8545190000','Other','KGM',0,0,0 UNION ALL
Select '8545200000','Brushes','KGM',0,0,0 UNION ALL
Select '8545900000','Other','KGM',0,0,0 UNION ALL

Select '8546100000','Of glass','KGM',0,0,0 UNION ALL

Select '8546201000','Transformer bushings and circuit breaker insulators','KGM',0,0,0 UNION ALL
Select '8546209000','Other','KGM',0,0,0 UNION ALL
Select '8546900000','Other','KGM',0,0,0 UNION ALL

Select '8547100000','Insulating fittings of ceramics','KGM',0,0,0 UNION ALL
Select '8547200000','Insulating fittings of plastics','KGM',0,0,0 UNION ALL

Select '8547901000','Electric conduit tubing and joints therefor, of base metal lined with insulating material','KGM',0,0,0 UNION ALL
Select '8547909000','Other','KGM',0,0,0 UNION ALL



Select '85481013','6 volts and 12 volts electric accumulators of a height (excluding terminals and handles) not more than 23 cm:','',0,0,0 UNION ALL
Select '8548101310','Of a kind used in aircraft','KGM',0,0,0 UNION ALL
Select '8548101390','Other','KGM',25,0,0 UNION ALL
Select '85481019','Other :','',0,0,0 UNION ALL
Select '8548101910','Of a kind used in aircraft','KGM',0,0,0 UNION ALL
Select '8548101990','Other','KGM',20,0,0 UNION ALL
Select '85481020','Waste and scrap containing mainly iron: ','',0,0,0 UNION ALL
Select '8548102010','Of primary cells and primary batteries','KGM',5,0,0 UNION ALL
Select '8548102020','Of electric accumulators of a kind used in aircraft','KGM',0,0,0 UNION ALL

Select '8548102031','Of nominal voltage of 6 volts and 12 volts, of a height  (excluding terminals and handles) not more than 23 cm','KGM',25,0,0 UNION ALL
Select '8548102939','Other','KGM',20,0,0 UNION ALL
Select '85481030','Waste and scrap containing mainly copper:','',0,0,0 UNION ALL
Select '8548103010','Of primary cells and primary batteries','KGM',5,0,0 UNION ALL
Select '8548103020','Of electric accumulators of a kind used in aircraft','KGM',0,0,0 UNION ALL

Select '8548103031','Of nominal voltage of 6 volts and 12 volts, of a height  (excluding terminals and handles) not more than 23 cm','KGM',25,0,0 UNION ALL
Select '8548103039','Other','KGM',20,0,0 UNION ALL

Select '8548109100','Of primary cells and primary batteries  ','KGM',5,0,0 UNION ALL
Select '8548109200','Of electric accumulators of a kind used in aircraft','KGM',0,0,0 UNION ALL
Select '85481099','Other:','',0,0,0 UNION ALL
Select '8548109911','Of nominal voltage of 6 volts and 12 volts, of a height  (excluding terminals and handles) not more than 23 cm','KGM',25,0,0 UNION ALL
Select '8548109919','Other','KGM',20,0,0 UNION ALL

Select '8548901000','Image sensors of the contact type comprising a photoconductive sensor element, an electric charge storage condenser, a light source of light emitting diodes, thinfilm transistor matrix and a scanning condenser, capable of scanning text ','KGM',0,0,0 UNION ALL
Select '8548902000','Printed circuit assemblies including such assemblies for external connections  ','KGM',0,0,0 UNION ALL
Select '8548909000','Other','KGM',0,0,0 UNION ALL

Select '8601100000','Powered from an external source of electricity','UNT',5,0,0 UNION ALL
Select '8601200000','Powered by electric accumulators','UNT',5,0,0 UNION ALL

Select '8602100000','Dieselelectric locomotives','UNT',5,0,0 UNION ALL
Select '8602900000','Other','UNT',5,0,0 UNION ALL

Select '8603100000','Powered from an external source of electricity','UNT',5,0,0 UNION ALL
Select '8603900000','Other','UNT',5,0,0 UNION ALL

Select '8604000000','Railway or tramway maintenance or service vehicles, whether or not selfpropelled (for example, workshops, cranes, ballast tampers, trackliners, testing coaches and track inspection vehicles).','UNT',5,0,0 UNION ALL

Select '8605000000','Railway or tramway passenger coaches, not selfpropelled; luggage vans, post office coaches and other special purpose railway or tramway coaches, not selfpropelled (excluding those of heading 86.04).','UNT',5,0,0 UNION ALL

Select '8606100000','Tank wagons and the like','UNT',5,0,0 UNION ALL
Select '8606300000','Selfdischarging vans and wagons, other than those of subheading 8606.10','UNT',5,0,0 UNION ALL

Select '8606910000','Covered and closed','UNT',5,0,0 UNION ALL
Select '8606920000','Open, with nonremovable sides of a height exceeding 60 cm','UNT',5,0,0 UNION ALL
Select '8606990000','Other','UNT',5,0,0 UNION ALL


Select '8607110000','Driving bogies and bisselbogies','KGM',5,0,0 UNION ALL
Select '8607120000','Other bogies and bisselbogies','KGM',5,0,0 UNION ALL
Select '8607190000','Other, including parts','KGM',0,0,0 UNION ALL

Select '8607210000','Air brakes and parts thereof','KGM',0,0,0 UNION ALL
Select '8607290000','Other','KGM',0,0,0 UNION ALL
Select '8607300000','Hooks and other coupling devices, buffers, and parts thereof','KGM',5,0,0 UNION ALL

Select '8607910000','Of locomotives','KGM',0,0,0 UNION ALL
Select '8607990000','Other','KGM',0,0,0 UNION ALL


Select '8608002000','Electromechanical equipment','KGM',0,0,0 UNION ALL
Select '8608009000','Other','KGM',0,0,0 UNION ALL


Select '8609001000','Of base metal','UNT',0,0,0 UNION ALL
Select '8609009000','Other','UNT',0,0,0 UNION ALL



Select '8701101100','For agricultural use','UNT',0,0,0 UNION ALL
Select '8701101900','Other','UNT',5,0,0 UNION ALL

Select '8701109100','For agricultural use','UNT',0,0,0 UNION ALL
Select '8701109900','Other','UNT',5,0,0 UNION ALL

Select '8701201000','Completely Knocked Down','UNT',0,0,0 UNION ALL

Select '8701209100','New','UNT',30,0,0 UNION ALL
Select '8701209900','Other','UNT',30,0,0 UNION ALL
Select '8701300000','Tracklaying tractors','UNT',5,0,0 UNION ALL


Select '8701911000','Agricultural tractors','UNT',0,0,0 UNION ALL
Select '8701919000','Other','UNT',5,0,0 UNION ALL

Select '8701921000','Agricultural tractors','UNT',0,0,0 UNION ALL
Select '8701929000','Other','UNT',5,0,0 UNION ALL

Select '8701931000','Agricultural tractors','UNT',0,0,0 UNION ALL
Select '8701939000','Other','UNT',5,0,0 UNION ALL

Select '8701941000','Agricultural tractors','UNT',0,0,0 UNION ALL
Select '8701949000','Other','UNT',5,0,0 UNION ALL

Select '8701951000','Agricultural tractors','UNT',0,0,0 UNION ALL
Select '8701959000','Other','UNT',5,0,0 UNION ALL



Select '8702101000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',10,0,0 UNION ALL

Select '8702104100','Of a g.v.w of at least 6 t but not exceeding 18 t','UNT',0,0,0 UNION ALL
Select '8702104200','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702104900','Other','UNT',0,0,0 UNION ALL
Select '8702105000','Other','UNT',10,0,0 UNION ALL

Select '8702106000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL

Select '87021071','Of a g.v.w of at least 6 t but not exceeding 18 t:','',0,0,0 UNION ALL
Select '8702107110','New','UNT',30,0,0 UNION ALL
Select '8702107190','Other','UNT',30,0,0 UNION ALL
Select '87021072','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702107210','New','UNT',30,0,0 UNION ALL
Select '8702107290','Other','UNT',30,0,0 UNION ALL
Select '87021079','Other:','',0,0,0 UNION ALL
Select '8702107910','New','UNT',30,0,0 UNION ALL
Select '8702107990','Other','UNT',30,0,0 UNION ALL

Select '87021081','Of a g.v.w of at least 6 t but not exceeding 18 t:','',0,0,0 UNION ALL
Select '8702108110','New','UNT',30,0,0 UNION ALL
Select '8702108190','Other','UNT',30,0,0 UNION ALL
Select '87021082','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702108210','New','UNT',30,0,0 UNION ALL
Select '8702108290','Other','UNT',30,0,0 UNION ALL
Select '87021089','Other:','',0,0,0 UNION ALL
Select '8702108910','New','UNT',30,0,0 UNION ALL
Select '8702108990','Other','UNT',30,0,0 UNION ALL

Select '8702109100','Of a g.v.w. exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8702109900','Other','UNT',30,0,0 UNION ALL


Select '8702201000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',10,0,0 UNION ALL

Select '8702202100','Of a g.v.w. exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702202900','Other','UNT',0,0,0 UNION ALL

Select '8702203100','Of a g.v.w of at least 6 t but not exceeding 18 t','UNT',0,0,0 UNION ALL
Select '8702203200','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702203900','Other','UNT',0,0,0 UNION ALL

Select '8702204100','Of a g.v.w exceeding 24 t','UNT',10,0,0 UNION ALL
Select '8702204900','Other','UNT',10,0,0 UNION ALL

Select '8702205000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL

Select '87022061','Of a g.v.w. of at least 6 t but not exceeding 18 t:','',0,0,0 UNION ALL
Select '8702206110','New','UNT',30,0,0 UNION ALL
Select '8702206190','Other','UNT',30,0,0 UNION ALL
Select '87022062','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702206210','New','UNT',30,0,0 UNION ALL
Select '8702206290','Other','UNT',30,0,0 UNION ALL
Select '87022069','Other:','',0,0,0 UNION ALL
Select '8702206910','New','UNT',30,0,0 UNION ALL
Select '8702206990','Other','UNT',30,0,0 UNION ALL

Select '87022071','Of a g.v.w of at least 6 t but not exceeding 18 t:','',0,0,0 UNION ALL
Select '8702207110','New','UNT',30,0,0 UNION ALL
Select '8702207190','Other','UNT',30,0,0 UNION ALL
Select '87022072','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702207210','New','UNT',30,0,0 UNION ALL
Select '8702207290','Other','UNT',30,0,0 UNION ALL
Select '87022079','Other:','',0,0,0 UNION ALL
Select '8702207910','New','UNT',30,0,0 UNION ALL
Select '8702207990','Other','UNT',30,0,0 UNION ALL

Select '8702209100','Of a g.v.w exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8702209900','Other','UNT',30,0,0 UNION ALL


Select '8702301000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',10,0,0 UNION ALL

Select '8702302100','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702302900','Other','UNT',0,0,0 UNION ALL

Select '8702303100','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702303900','Other','UNT',0,0,0 UNION ALL

Select '8702304100','Of a g.v.w exceeding 24 t','UNT',10,0,0 UNION ALL
Select '8702304900','Other','UNT',10,0,0 UNION ALL

Select '8702305000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL

Select '87023061','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702306110','New','UNT',30,0,0 UNION ALL
Select '8702306190','Other','UNT',30,0,0 UNION ALL
Select '87023069','Other:','',0,0,0 UNION ALL
Select '8702306910','New','UNT',30,0,0 UNION ALL
Select '8702306990','Other','UNT',30,0,0 UNION ALL

Select '87023071','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702307110','New','UNT',30,0,0 UNION ALL
Select '8702307190','Other','UNT',30,0,0 UNION ALL
Select '87023079','Other:','',0,0,0 UNION ALL
Select '8702307910','New','UNT',30,0,0 UNION ALL
Select '8702307990','Other','UNT',30,0,0 UNION ALL

Select '8702309100','Of a g.v.w exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8702309900','Other','UNT',30,0,0 UNION ALL


Select '8702401000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',10,0,0 UNION ALL

Select '8702402100','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702402900','Other','UNT',0,0,0 UNION ALL

Select '8702403100','Of a g.v.w exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702403900','Other','UNT',0,0,0 UNION ALL

Select '8702404100','Of a g.v.w exceeding 24 t','UNT',10,0,0 UNION ALL
Select '8702404900','Other','UNT',10,0,0 UNION ALL

Select '8702405000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL

Select '87024061','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702406110','New','UNT',30,0,0 UNION ALL
Select '8702406190','Other','UNT',30,0,0 UNION ALL
Select '87024069','Other:','',0,0,0 UNION ALL
Select '8702406910','New','UNT',30,0,0 UNION ALL
Select '8702406990','Other','UNT',30,0,0 UNION ALL

Select '87024071','Of a g.v.w exceeding 24 t:','',0,0,0 UNION ALL
Select '8702407110','New','UNT',30,0,0 UNION ALL
Select '8702407190','Other','UNT',30,0,0 UNION ALL
Select '87024079','Other:','',0,0,0 UNION ALL
Select '8702407910','New','UNT',30,0,0 UNION ALL
Select '8702407990','Other','UNT',30,0,0 UNION ALL

Select '8702409100','Of a g.v.w exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8702409900','Other','UNT',30,0,0 UNION ALL


Select '8702901000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',10,0,0 UNION ALL

Select '8702902100','Of a g.v.w. exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702902900','Other','UNT',0,0,0 UNION ALL

Select '8702903100','Of a g.v.w. exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8702903200','Other','UNT',0,0,0 UNION ALL
Select '8702903300','Other, of a g.v.w. exceeding 24 t','UNT',10,0,0 UNION ALL
Select '8702903900','Other','UNT',10,0,0 UNION ALL

Select '8702904000','Motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL

Select '87029051','Of a g.v.w. not exceeding 24 t:','',0,0,0 UNION ALL
Select '8702905110','New','UNT',30,0,0 UNION ALL
Select '8702905190','Other','UNT',30,0,0 UNION ALL
Select '87029059','Other:','',0,0,0 UNION ALL
Select '8702905910','New','UNT',30,0,0 UNION ALL
Select '8702905990','Other','UNT',30,0,0 UNION ALL

Select '87029061','Of a g.v.w. not exceeding 24 t:','',0,0,0 UNION ALL
Select '8702906110','New','UNT',30,0,0 UNION ALL
Select '8702906190','Other','UNT',30,0,0 UNION ALL
Select '87029069','Other:','',0,0,0 UNION ALL
Select '8702906910','New','UNT',30,0,0 UNION ALL
Select '8702906990','Other','UNT',30,0,0 UNION ALL

Select '87029071','Of a g.v.w. not exceeding 24 t:','',0,0,0 UNION ALL
Select '8702907110','New','UNT',30,0,0 UNION ALL
Select '8702907190','Other','UNT',30,0,0 UNION ALL
Select '87029079','Other:','',0,0,0 UNION ALL
Select '8702907910','New','UNT',30,0,0 UNION ALL
Select '8702907990','Other','UNT',30,0,0 UNION ALL
Select '8702908000','Other, of a g.v.w. exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8702909000','Other','UNT',30,0,0 UNION ALL


Select '8703101000','Golf cars (including golf buggies) and similar vehicles','UNT',0,0,0 UNION ALL
Select '8703109000','Other','UNT',30,0,0 UNION ALL



Select '8703211100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703211200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703211300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703211400','Motorhomes','UNT',10,0,0 UNION ALL
Select '8703211500','Sedan','UNT',10,0,0 UNION ALL

Select '8703212100','Of fourwheel drive','UNT',10,0,0 UNION ALL
Select '8703212900','Other','UNT',10,0,0 UNION ALL
Select '8703213000','Other','UNT',10,0,0 UNION ALL

Select '8703214100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703214200','AllTerrain Vehicles (ATV)','UNT',30,0,0.65 UNION ALL
Select '8703214300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703214400','Motorhomes','UNT',35,0,0 UNION ALL
Select '87032145','Sedan:','',0,0,0 UNION ALL
Select '8703214510','New','UNT',30,0,0.75 UNION ALL
Select '8703214590','Other','UNT',30,0,0.75 UNION ALL

Select '87032151','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703215110','New','UNT',30,0,0.75 UNION ALL
Select '8703215190','Other','UNT',30,0,0.75 UNION ALL
Select '87032159','Other:','',0,0,0 UNION ALL
Select '8703215910','New','UNT',30,0,0.75 UNION ALL
Select '8703215990','Other','UNT',30,0,0.75 UNION ALL
Select '87032190','Other:','',0,0,0 UNION ALL
Select '8703219010','New','UNT',30,0,0.65 UNION ALL
Select '8703219090','Other','UNT',30,0,0.65 UNION ALL


Select '8703221100','Gokarts','UNT',10,0,0 UNION ALL
Select '8703221200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703221300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703221400','Hearses','UNT',10,0,0 UNION ALL
Select '8703221500','Prison vans','UNT',10,0,0 UNION ALL
Select '8703221600','Motorhomes','UNT',10,0,0 UNION ALL
Select '8703221700','Sedan','UNT',10,0,0 UNION ALL

Select '8703222100','Of fourwheel drive','UNT',10,0,0 UNION ALL
Select '8703222900','Other','UNT',10,0,0 UNION ALL
Select '8703223000','Other','UNT',10,0,0 UNION ALL

Select '8703224100','Gokarts','UNT',30,0,0 UNION ALL
Select '8703224200','AllTerrain Vehicles (ATV)','UNT',30,0,0.65 UNION ALL
Select '8703224300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703224400','Hearses','UNT',30,0,0.65 UNION ALL
Select '8703224500','Prison vans','UNT',30,0,0.65 UNION ALL
Select '8703224600','Motorhomes','UNT',35,0,0 UNION ALL
Select '87032247','Sedan:','',0,0,0 UNION ALL
Select '8703224710','New','UNT',30,0,0.75 UNION ALL
Select '8703224790','Other','UNT',30,0,0.75 UNION ALL

Select '87032251','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703225110','New','UNT',30,0,0.75 UNION ALL
Select '8703225190','Other','UNT',30,0,0.75 UNION ALL
Select '87032259','Other:','',0,0,0 UNION ALL
Select '8703225910','New','UNT',30,0,0.75 UNION ALL
Select '8703225990','Other','UNT',30,0,0.75 UNION ALL
Select '87032290','Other:','',0,0,0 UNION ALL
Select '8703229010','New','UNT',30,0,0.65 UNION ALL
Select '8703229090','Other','UNT',30,0,0.65 UNION ALL


Select '8703231100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703231200','Hearses','UNT',10,0,0 UNION ALL
Select '8703231300','Prison vans','UNT',10,0,0 UNION ALL
Select '8703231400','Motorhomes','UNT',35,0,0 UNION ALL

Select '8703232100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703232200','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703232300','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',10,0,0 UNION ALL
Select '8703232400','Of a cylinder capacity exceeding 2,500 cc','UNT',10,0,0 UNION ALL

Select '8703233100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703233200','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703233300','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',10,0,0 UNION ALL
Select '8703233400','Of a cylinder capacity exceeding 2,500 cc','UNT',10,0,0 UNION ALL

Select '8703233500','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703233600','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703234100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703234200','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703235100','Ambulances','UNT',5,0,0 UNION ALL
Select '87032352','Hearses:','',0,0,0 UNION ALL
Select '8703235210','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703235220','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703235230','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703235240','Of a cylinder capacity exceeding 2,500 cc','UNT',30,0,1.05 UNION ALL
Select '87032353','Prison vans:','',0,0,0 UNION ALL
Select '8703235310','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703235320','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703235330','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703235340','Of a cylinder capacity exceeding 2,500 cc','UNT',30,0,1.05 UNION ALL
Select '8703235400','Motorhomes','UNT',35,0,0 UNION ALL

Select '87032355','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703235510','New','UNT',30,0,0.75 UNION ALL
Select '8703235590','Other','UNT',30,0,0.75 UNION ALL
Select '87032356','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703235610','New','UNT',30,0,0.8 UNION ALL
Select '8703235690','Other','UNT',30,0,0.8 UNION ALL
Select '87032357','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703235710','New','UNT',30,0,0.9 UNION ALL
Select '8703235790','Other','UNT',30,0,0.9 UNION ALL
Select '87032358','Of a cylinder capacity exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703235810','New','UNT',30,0,1.05 UNION ALL
Select '8703235890','Other','UNT',30,0,1.05 UNION ALL

Select '87032361','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703236110','New','UNT',30,0,0.75 UNION ALL
Select '8703236190','Other','UNT',30,0,0.75 UNION ALL
Select '87032362','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703236210','New','UNT',30,0,0.8 UNION ALL
Select '8703236290','Other','UNT',30,0,0.8 UNION ALL
Select '87032363','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703236310','New','UNT',30,0,0.9 UNION ALL
Select '8703236390','Other','UNT',30,0,0.9 UNION ALL
Select '87032364','Of a cylinder capacity exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703236410','New','UNT',30,0,1.05 UNION ALL
Select '8703236490','Other','UNT',30,0,1.05 UNION ALL

Select '87032365','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703236510','New','UNT',30,0,0.75 UNION ALL
Select '8703236590','Other','UNT',30,0,0.75 UNION ALL
Select '87032366','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703236610','New','UNT',30,0,0.8 UNION ALL
Select '8703236690','Other','UNT',30,0,0.8 UNION ALL
Select '87032367','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703236710','New','UNT',30,0,0.9 UNION ALL
Select '8703236790','Other','UNT',30,0,0.9 UNION ALL
Select '87032368','Of a cylinder capacity exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703236810','New','UNT',30,0,1.05 UNION ALL
Select '8703236890','Other','UNT',30,0,1.05 UNION ALL

Select '87032371','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703237110','New','UNT',30,0,0.65 UNION ALL
Select '8703237190','Other','UNT',30,0,0.65 UNION ALL
Select '87032372','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703237210','New','UNT',30,0,0.75 UNION ALL
Select '8703237290','Other','UNT',30,0,0.75 UNION ALL
Select '87032373','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703237310','New','UNT',30,0,0.9 UNION ALL
Select '8703237390','Other','UNT',30,0,0.9 UNION ALL
Select '87032374','Of a cylinder capacity exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703237410','New','UNT',30,0,1.05 UNION ALL
Select '8703237490','Other','UNT',30,0,1.05 UNION ALL


Select '8703241100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703241200','Hearses','UNT',10,0,0 UNION ALL
Select '8703241300','Prison vans','UNT',10,0,0 UNION ALL
Select '8703241400','Motorhomes','UNT',35,0,0 UNION ALL
Select '8703241500','Sedan','UNT',10,0,0 UNION ALL

Select '8703242100','Of fourwheel drive','UNT',10,0,0 UNION ALL
Select '8703242900','Other','UNT',10,0,0 UNION ALL
Select '8703243000','Other','UNT',10,0,0 UNION ALL

Select '8703244100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703244200','Hearses','UNT',30,0,1.05 UNION ALL
Select '8703244300','Prison vans','UNT',30,0,1.05 UNION ALL
Select '8703244400','Motorhomes','UNT',35,0,0 UNION ALL

Select '87032445','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703244510','New','UNT',30,0,1.05 UNION ALL
Select '8703244590','Other','UNT',30,0,1.05 UNION ALL
Select '87032449','Other:','',0,0,0 UNION ALL
Select '8703244910','New','UNT',30,0,1.05 UNION ALL
Select '8703244990','Other','UNT',30,0,1.05 UNION ALL

Select '87032451','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703245110','New','UNT',30,0,1.05 UNION ALL
Select '8703245190','Other','UNT',30,0,1.05 UNION ALL
Select '87032459','Other:','',0,0,0 UNION ALL
Select '8703245910','New','UNT',30,0,1.05 UNION ALL
Select '8703245990','Other','UNT',30,0,1.05 UNION ALL

Select '87032461','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703246110','New','UNT',30,0,1.05 UNION ALL
Select '8703246190','Other','UNT',30,0,1.05 UNION ALL
Select '87032469','Other:','',0,0,0 UNION ALL
Select '8703246910','New','UNT',30,0,1.05 UNION ALL
Select '8703246990','Other','UNT',30,0,1.05 UNION ALL



Select '8703311100','Gokarts','UNT',10,0,0 UNION ALL
Select '8703311200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703311300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703311400','Hearses','UNT',10,0,0 UNION ALL
Select '8703311500','Prison vans','UNT',10,0,0 UNION ALL
Select '8703311600','Motorhomes','UNT',35,0,0 UNION ALL
Select '8703311700','Sedan','UNT',10,0,0 UNION ALL

Select '8703312100','Of fourwheel drive','UNT',10,0,0 UNION ALL
Select '8703312900','Other','UNT',10,0,0 UNION ALL
Select '8703313000','Other','UNT',10,0,0 UNION ALL

Select '8703314100','Gokarts','UNT',30,0,0 UNION ALL
Select '8703314200','AllTerrain Vehicles (ATV)','UNT',30,0,0.65 UNION ALL
Select '8703314300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703314400','Hearses','UNT',30,0,0.65 UNION ALL
Select '8703314500','Prison vans','UNT',30,0,0.65 UNION ALL
Select '8703314600','Motorhomes','UNT',35,0,0 UNION ALL
Select '87033147','Sedan:','',0,0,0 UNION ALL
Select '8703314710','New','UNT',30,0,0.75 UNION ALL
Select '8703314790','Other','UNT',30,0,0.75 UNION ALL

Select '87033151','Of fourwheel drive:','',0,0,0 UNION ALL
Select '8703315110','New','UNT',30,0,0.75 UNION ALL
Select '8703315190','Other','UNT',30,0,0.75 UNION ALL
Select '87033159','Other:','',0,0,0 UNION ALL
Select '8703315910','New','UNT',30,0,0.75 UNION ALL
Select '8703315990','Other','UNT',30,0,0.75 UNION ALL
Select '87033190','Other:','',0,0,0 UNION ALL
Select '8703319010','New','UNT',30,0,0.65 UNION ALL
Select '8703319090','Other','UNT',30,0,0.65 UNION ALL


Select '8703321100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703321200','Hearses','UNT',10,0,0 UNION ALL
Select '8703321300','Prison vans','UNT',10,0,0 UNION ALL
Select '8703321400','Motorhomes','UNT',35,0,0 UNION ALL

Select '8703322100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703322200','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703322300','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703323100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703323200','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703323300','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703323400','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703323500','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703323600','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703324100','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',10,0,0 UNION ALL
Select '8703324200','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703324300','Of a cylinder capacity exceeding 2,000 cc ','UNT',10,0,0 UNION ALL

Select '8703325100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703325200','Hearses','UNT',30,0,90 UNION ALL
Select '8703325300','Prison vans','UNT',30,0,90 UNION ALL
Select '8703325400','Motorhomes','UNT',35,0,0 UNION ALL

Select '87033261','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703326110','New','UNT',30,0,0.75 UNION ALL
Select '8703326190','Other','UNT',30,0,0.75 UNION ALL
Select '87033262','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703326210','New','UNT',30,0,0.8 UNION ALL
Select '8703326290','Other','UNT',30,0,0.8 UNION ALL
Select '87033263','Of a cylinder capacity exceeding 2,000 cc :','',0,0,0 UNION ALL
Select '8703326310','New','UNT',30,0,0.9 UNION ALL
Select '8703326390','Other','UNT',30,0,0.9 UNION ALL

Select '87033271','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703327110','New','UNT',30,0,0.75 UNION ALL
Select '8703327190','Other','UNT',30,0,0.75 UNION ALL
Select '87033272','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703327210','New','UNT',30,0,0.8 UNION ALL
Select '8703327290','Other','UNT',30,0,0.8 UNION ALL
Select '87033273','Of a cylinder capacity exceeding 2,000 cc: ','',0,0,0 UNION ALL
Select '8703327310','New','UNT',30,0,0.9 UNION ALL
Select '8703327390','Other','UNT',30,0,0.9 UNION ALL

Select '87033274','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703327410','New','UNT',30,0,0.75 UNION ALL
Select '8703327490','Other','UNT',30,0,0.75 UNION ALL
Select '87033275','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703327510','New','UNT',30,0,0.8 UNION ALL
Select '8703327590','Other','UNT',30,0,0.8 UNION ALL
Select '87033276','Of a cylinder capacity exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703327610','New','UNT',30,0,0.9 UNION ALL
Select '8703327690','Other','UNT',30,0,0.9 UNION ALL

Select '87033281','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703328110','New','UNT',30,0,0.65 UNION ALL
Select '8703328190','Other','UNT',30,0,0.65 UNION ALL
Select '87033282','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703328210','New','UNT',30,0,0.75 UNION ALL
Select '8703328290','Other','UNT',30,0,0.75 UNION ALL
Select '87033283','Of a cylinder capacity exceeding 2,000 cc: ','',0,0,0 UNION ALL
Select '8703328310','New','UNT',30,0,0.9 UNION ALL
Select '8703328390','Other','UNT',30,0,0.9 UNION ALL


Select '8703331100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703331200','Hearses','UNT',10,0,0 UNION ALL
Select '8703331300','Prison vans','UNT',10,0,0 UNION ALL
Select '8703331400','Motorhomes','UNT',35,0,0 UNION ALL

Select '8703332100','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',10,0,0 UNION ALL
Select '8703332200','Of a cylinder capacity exceeding 3,000 cc','UNT',10,0,0 UNION ALL

Select '8703333100','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',10,0,0 UNION ALL
Select '8703333200','Of a cylinder capacity exceeding 3,000 cc','UNT',10,0,0 UNION ALL

Select '8703333300','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',10,0,0 UNION ALL
Select '8703333400','Of a cylinder capacity exceeding 3,000 cc','UNT',10,0,0 UNION ALL
Select '8703334000','Other','UNT',10,0,0 UNION ALL

Select '8703335100','Ambulances','UNT',5,0,0 UNION ALL
Select '8703335200','Hearses','UNT',30,0,1.05 UNION ALL
Select '8703335300','Prison vans','UNT',30,0,1.05 UNION ALL
Select '8703335400','Motorhomes','UNT',35,0,0 UNION ALL

Select '87033361','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703336110','New','UNT',30,0,1.05 UNION ALL
Select '8703336190','Other','UNT',30,0,1.05 UNION ALL
Select '87033362','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703336210','New','UNT',30,0,1.05 UNION ALL
Select '8703336290','Other','UNT',30,0,1.05 UNION ALL

Select '87033371','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703337110','New','UNT',30,0,1.05 UNION ALL
Select '8703337190','Other','UNT',30,0,1.05 UNION ALL
Select '87033372','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703337210','New','UNT',30,0,1.05 UNION ALL
Select '8703337290','Other','UNT',30,0,1.05 UNION ALL
Select '87033380','Other motor cars (including station wagons and sports cars, but not including vans), not of fourwheeldrive:','',0,0,0 UNION ALL
Select '8703338010','New','UNT',30,0,1.05 UNION ALL
Select '8703338090','Other','UNT',30,0,1.05 UNION ALL
Select '87033390','Other:','',0,0,0 UNION ALL
Select '8703339010','New','UNT',30,0,1.05 UNION ALL
Select '8703339090','Other','UNT',30,0,1.05 UNION ALL


Select '8703401100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703401200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703401300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703401400','Hearses','UNT',10,0,0 UNION ALL
Select '8703401500','Prison vans','UNT',10,0,0 UNION ALL

Select '8703401600','Of a cylinder capacity not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703401700','Of a cylinder capacity exceeding 2,000 cc','UNT',35,0,0 UNION ALL

Select '8703401800','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703401900','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703402100','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703402200','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703402300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703402400','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703402500','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703402600','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703402700','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703402800','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703403100','Gokarts','UNT',0,0,0 UNION ALL

Select '8703403200','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703403300','Of a cylinder capacity exceeding 1,000 cc ','UNT',30,0,0.65 UNION ALL

Select '8703403400','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',5,0,0 UNION ALL
Select '8703403500','Of a cylinder capacity exceeding 1,500 cc but not exceeding 3,000 cc','UNT',5,0,0 UNION ALL
Select '8703403600','Other','UNT',5,0,0 UNION ALL

Select '8703404100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703404200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703404300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703404400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703404500','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703404600','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL
Select '8703404700','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703405100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703405200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703405300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703405400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 3,000 cc','UNT',30,0,0.9 UNION ALL
Select '8703405500','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703405600','Of a cylinder capacity not exceeding 1,500 cc','UNT',35,0,0 UNION ALL
Select '8703405700','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703405800','Of a cylinder capacity exceeding 2,000 cc ','UNT',35,0,0 UNION ALL

Select '87034061','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703406110','New','UNT',30,0,0.75 UNION ALL
Select '8703406190','Other','UNT',30,0,0.75 UNION ALL
Select '87034062','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703406210','New','UNT',30,0,0.75 UNION ALL
Select '8703406290','Other','UNT',30,0,0.75 UNION ALL
Select '87034063','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703406310','New','UNT',30,0,0.75 UNION ALL
Select '8703406390','Other','UNT',30,0,0.75 UNION ALL
Select '87034064','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703406410','New','UNT',30,0,0.8 UNION ALL
Select '8703406490','Other','UNT',30,0,0.8 UNION ALL
Select '87034065','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703406510','New','UNT',30,0,0.9 UNION ALL
Select '8703406590','Other','UNT',30,0,0.9 UNION ALL
Select '87034066','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703406610','New','UNT',30,0,1.05 UNION ALL
Select '8703406690','Other','UNT',30,0,1.05 UNION ALL
Select '87034067','Of a cylinder capacity exceeding 3,000 cc, of fourwheel drive:','',0,0,0 UNION ALL
Select '8703406710','New','UNT',30,0,1.05 UNION ALL
Select '8703406790','Other','UNT',30,0,1.05 UNION ALL
Select '87034068','Of a cylinder capacity exceeding 3,000 cc, not of fourwheel drive:','',0,0,0 UNION ALL
Select '8703406810','New','UNT',30,0,1.05 UNION ALL
Select '8703406890','Other','UNT',30,0,1.05 UNION ALL

Select '87034071','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703407110','New','UNT',30,0,0.75 UNION ALL
Select '8703407190','Other','UNT',30,0,0.75 UNION ALL
Select '87034072','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703407210','New','UNT',30,0,0.75 UNION ALL
Select '8703407290','Other','UNT',30,0,0.75 UNION ALL
Select '87034073','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703407310','New','UNT',30,0,0.75 UNION ALL
Select '8703407390','Other','UNT',30,0,0.75 UNION ALL
Select '87034074','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703407410','New','UNT',30,0,0.8 UNION ALL
Select '8703407490','Other','UNT',30,0,0.8 UNION ALL
Select '87034075','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703407510','New','UNT',30,0,0.9 UNION ALL
Select '8703407590','Other','UNT',30,0,0.9 UNION ALL
Select '87034076','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703407610','New','UNT',30,0,1.05 UNION ALL
Select '8703407690','Other','UNT',30,0,1.05 UNION ALL
Select '87034077','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703407710','New','UNT',30,0,1.05 UNION ALL
Select '8703407790','Other','UNT',30,0,1.05 UNION ALL

Select '87034081','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703408110','New','UNT',30,0,0.75 UNION ALL
Select '8703408190','Other','UNT',30,0,0.75 UNION ALL
Select '87034082','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703408210','New','UNT',30,0,0.75 UNION ALL
Select '8703408290','Other','UNT',30,0,0.75 UNION ALL
Select '87034083','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703408310','New','UNT',30,0,0.75 UNION ALL
Select '8703408390','Other','UNT',30,0,0.75 UNION ALL
Select '87034084','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703408410','New','UNT',30,0,0.8 UNION ALL
Select '8703408490','Other','UNT',30,0,0.8 UNION ALL
Select '87034085','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703408510','New','UNT',30,0,0.9 UNION ALL
Select '8703408590','Other','UNT',30,0,0.9 UNION ALL
Select '87034086','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703408610','New','UNT',30,0,1.05 UNION ALL
Select '8703408690','Other','UNT',30,0,1.05 UNION ALL
Select '87034087','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703408710','New','UNT',30,0,1.05 UNION ALL
Select '8703408790','Other','UNT',30,0,1.05 UNION ALL

Select '87034091','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703409110','New','UNT',30,0,0.65 UNION ALL
Select '8703409190','Other','UNT',30,0,0.65 UNION ALL
Select '87034092','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703409210','New','UNT',30,0,0.65 UNION ALL
Select '8703409290','Other','UNT',30,0,0.65 UNION ALL
Select '87034093','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703409310','New','UNT',30,0,0.65 UNION ALL
Select '8703409390','Other','UNT',30,0,0.65 UNION ALL
Select '87034094','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703409410','New','UNT',30,0,0.75 UNION ALL
Select '8703409490','Other','UNT',30,0,0.75 UNION ALL
Select '87034095','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703409510','New','UNT',30,0,0.9 UNION ALL
Select '8703409590','Other','UNT',30,0,0.9 UNION ALL
Select '87034096','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703409610','New','UNT',30,0,1.05 UNION ALL
Select '8703409690','Other','UNT',30,0,1.05 UNION ALL
Select '87034097','Of a cylinder capacity exceeding 3,000 cc, of fourwheel drive:','',0,0,0 UNION ALL
Select '8703409710','New','UNT',30,0,1.05 UNION ALL
Select '8703409790','Other','UNT',30,0,1.05 UNION ALL
Select '87034098','Of a cylinder capacity exceeding 3,000 cc, not of fourwheel drive:','',0,0,0 UNION ALL
Select '8703409810','New','UNT',30,0,1.05 UNION ALL
Select '8703409890','Other','UNT',30,0,1.05 UNION ALL


Select '8703501100','Gokarts','UNT',10,0,0 UNION ALL
Select '8703501200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703501300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703501400','Hearses','UNT',10,0,0 UNION ALL
Select '8703501500','Prison vans','UNT',10,0,0 UNION ALL

Select '8703501600','Of a cylinder capacity not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703501700','Of a cylinder capacity exceeding 2,000 cc','UNT',35,0,0 UNION ALL

Select '8703501800','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703501900','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703502100','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703502200','Of a cylinder capacity not exceeding 1,500 cc ','UNT',10,0,0 UNION ALL
Select '8703502300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703502400','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703502500','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703502600','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703502700','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703502800','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703503100','Gokarts','UNT',30,0,0 UNION ALL

Select '8703503200','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703503300','Of a cylinder capacity exceeding 1,000 cc ','UNT',30,0,0.65 UNION ALL

Select '8703503400','Of a cylinder capacity not exceeding 1,500 cc ','UNT',5,0,0 UNION ALL
Select '8703503500','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,500 cc','UNT',5,0,0 UNION ALL
Select '8703503600','Of a cylinder capacity exceeding 2,500 cc','UNT',5,0,0 UNION ALL

Select '8703504100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703504200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703504300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703504400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703504500','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703504600','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL
Select '8703504700','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703505100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703505200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703505300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703505400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703505500','Of a cylinder capacity exceeding 2,500 cc','UNT',30,0,1.05 UNION ALL

Select '8703505600','Of a cylinder capacity not exceeding 1,500 cc','UNT',35,0,0 UNION ALL
Select '8703505700','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703505800','Of a cylinder capacity exceeding 2,000 cc ','UNT',35,0,0 UNION ALL

Select '87035061','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703506110','New','UNT',30,0,0.75 UNION ALL
Select '8703506190','Other','UNT',30,0,0.75 UNION ALL
Select '87035062','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703506210','New','UNT',30,0,0.75 UNION ALL
Select '8703506290','Other','UNT',30,0,0.75 UNION ALL
Select '87035063','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703506310','New','UNT',30,0,0.75 UNION ALL
Select '8703506390','Other','UNT',30,0,0.75 UNION ALL
Select '87035064','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703506410','New','UNT',30,0,0.8 UNION ALL
Select '8703506490','Other','UNT',30,0,0.8 UNION ALL
Select '87035065','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703506510','New','UNT',30,0,0.9 UNION ALL
Select '8703506590','Other','UNT',30,0,0.9 UNION ALL
Select '87035066','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703506610','New','UNT',30,0,1.05 UNION ALL
Select '8703506690','Other','UNT',30,0,1.05 UNION ALL
Select '87035067','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703506710','New','UNT',30,0,1.05 UNION ALL
Select '8703506790','Other','UNT',30,0,1.05 UNION ALL

Select '87035071','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703507110','New','UNT',30,0,0.75 UNION ALL
Select '8703507190','Other','UNT',30,0,0.75 UNION ALL
Select '87035072','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703507210','New','UNT',30,0,0.75 UNION ALL
Select '8703507290','Other','UNT',30,0,0.75 UNION ALL
Select '87035073','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703507310','New','UNT',30,0,0.75 UNION ALL
Select '8703507390','Other','UNT',30,0,0.75 UNION ALL
Select '87035074','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703507410','New','UNT',30,0,0.8 UNION ALL
Select '8703507490','Other','UNT',30,0,0.8 UNION ALL
Select '87035075','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703507510','New','UNT',30,0,0.9 UNION ALL
Select '8703507590','Other','UNT',30,0,0.9 UNION ALL
Select '87035076','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703507610','New','UNT',30,0,1.05 UNION ALL
Select '8703507690','Other','UNT',30,0,1.05 UNION ALL
Select '87035077','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703507710','New','UNT',30,0,1.05 UNION ALL
Select '8703507790','Other','UNT',30,0,1.05 UNION ALL

Select '87035081','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703508110','New','UNT',30,0,0.75 UNION ALL
Select '8703508190','Other','UNT',30,0,0.75 UNION ALL
Select '87035082','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703508210','New','UNT',30,0,0.75 UNION ALL
Select '8703508290','Other','UNT',30,0,0.75 UNION ALL
Select '87035083','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703508310','New','UNT',30,0,0.75 UNION ALL
Select '8703508390','Other','UNT',30,0,0.75 UNION ALL
Select '87035084','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703508410','New','UNT',30,0,0.8 UNION ALL
Select '8703508490','Other','UNT',30,0,0.8 UNION ALL
Select '87035085','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703508510','New','UNT',30,0,0.9 UNION ALL
Select '8703508590','Other','UNT',30,0,0.9 UNION ALL
Select '87035086','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703508610','New','UNT',30,0,1.05 UNION ALL
Select '8703508690','Other','UNT',30,0,1.05 UNION ALL
Select '87035087','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703508710','New','UNT',30,0,1.05 UNION ALL
Select '8703508790','Other','UNT',30,0,1.05 UNION ALL

Select '87035091','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703509110','New','UNT',30,0,0.65 UNION ALL
Select '8703509190','Other','UNT',30,0,0.65 UNION ALL
Select '87035092','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703509210','New','UNT',30,0,0.65 UNION ALL
Select '8703509290','Other','UNT',30,0,0.65 UNION ALL
Select '87035093','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703509310','New','UNT',30,0,0.65 UNION ALL
Select '8703509390','Other','UNT',30,0,0.65 UNION ALL
Select '87035094','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703509410','New','UNT',30,0,0.75 UNION ALL
Select '8703509490','Other','UNT',30,0,0.75 UNION ALL
Select '87035095','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703509510','New','UNT',30,0,0.9 UNION ALL
Select '8703509590','Other','UNT',30,0,0.9 UNION ALL
Select '87035096','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703509610','New','UNT',30,0,1.05 UNION ALL
Select '8703509690','Other','UNT',30,0,1.05 UNION ALL
Select '87035097','Of a cylinder capacity exceeding 3,000 cc','',0,0,0 UNION ALL
Select '8703509710','New','UNT',30,0,1.05 UNION ALL
Select '8703509790','Other','UNT',30,0,1.05 UNION ALL


Select '8703601100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703601200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703601300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703601400','Hearses','UNT',10,0,0 UNION ALL
Select '8703601500','Prison vans','UNT',10,0,0 UNION ALL

Select '8703601600','Of a cylinder capacity not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703601700','Of a cylinder capacity exceeding 2,000 cc','UNT',35,0,0 UNION ALL

Select '8703601800','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703601900','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703602100','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703602200','Of a cylinder capacity not exceeding 1,500 cc ','UNT',10,0,0 UNION ALL
Select '8703602300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703602400','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703602500','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703602600','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703602700','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703602800','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703603100','Gokarts','UNT',0,0,0 UNION ALL

Select '8703603200','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703603300','Of a cylinder capacity exceeding 1,000 cc ','UNT',30,0,0.65 UNION ALL

Select '8703603400','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',5,0,0 UNION ALL
Select '8703603500','Of a cylinder capacity exceeding 1,500 cc but not exceeding 3,000 cc','UNT',5,0,0 UNION ALL
Select '8703603600','Other','UNT',5,0,0 UNION ALL

Select '8703604100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703604200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703604300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703604400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703604500','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703604600','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL
Select '8703604700','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703605100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703605200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703605300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703605400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 3,000 cc','UNT',30,0,0.9 UNION ALL
Select '8703605500','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703605600','Of a cylinder capacity not exceeding 1,500 cc','UNT',35,0,0 UNION ALL
Select '8703605700','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703605800','Of a cylinder capacity exceeding 2,000 cc ','UNT',35,0,0 UNION ALL

Select '87036061','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703606110','New','UNT',30,0,0.75 UNION ALL
Select '8703606190','Other','UNT',30,0,0.75 UNION ALL
Select '87036062','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703606210','New','UNT',30,0,0.75 UNION ALL
Select '8703606290','Other','UNT',30,0,0.75 UNION ALL
Select '87036063','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703606310','New','UNT',30,0,0.75 UNION ALL
Select '8703606390','Other','UNT',30,0,0.75 UNION ALL
Select '87036064','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703606410','New','UNT',30,0,0.8 UNION ALL
Select '8703606490','Other','UNT',30,0,0.8 UNION ALL
Select '87036065','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703606510','New','UNT',30,0,0.9 UNION ALL
Select '8703606590','Other','UNT',30,0,0.9 UNION ALL
Select '87036066','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703606610','New','UNT',30,0,1.05 UNION ALL
Select '8703606690','Other','UNT',30,0,1.05 UNION ALL
Select '87036067','Of a cylinder capacity exceeding 3,000 cc, of fourwheel drive:','',0,0,0 UNION ALL
Select '8703606790','Other','UNT',30,0,1.05 UNION ALL
Select '87036068','Of a cylinder capacity exceeding 3,000 cc, not of fourwheel drive:','',0,0,0 UNION ALL
Select '8703606810','New','UNT',30,0,1.05 UNION ALL
Select '8703606890','Other','UNT',30,0,1.05 UNION ALL

Select '87036071','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703607110','New','UNT',30,0,0.75 UNION ALL
Select '8703607190','Other','UNT',30,0,0.75 UNION ALL
Select '87036072','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703607210','New','UNT',30,0,0.75 UNION ALL
Select '8703607290','Other','UNT',30,0,0.75 UNION ALL
Select '87036073','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703607310','New','UNT',30,0,0.75 UNION ALL
Select '8703607390','Other','UNT',30,0,0.75 UNION ALL
Select '87036074','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703607410','New','UNT',30,0,0.8 UNION ALL
Select '8703607490','Other','UNT',30,0,0.8 UNION ALL
Select '87036075','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703607510','New','UNT',30,0,0.9 UNION ALL
Select '8703607590','Other','UNT',30,0,0.9 UNION ALL
Select '87036076','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703607610','New','UNT',30,0,1.05 UNION ALL
Select '8703607690','Other','UNT',30,0,1.05 UNION ALL
Select '87036077','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703607710','New','UNT',30,0,1.05 UNION ALL
Select '8703607790','Other','UNT',30,0,1.05 UNION ALL

Select '87036081','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703608110','New','UNT',30,0,0.75 UNION ALL
Select '8703608190','Other','UNT',30,0,0.75 UNION ALL
Select '87036082','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703608210','New','UNT',30,0,0.75 UNION ALL
Select '8703608290','Other','UNT',30,0,0.75 UNION ALL
Select '87036083','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703608310','New','UNT',30,0,0.75 UNION ALL
Select '8703608390','Other','UNT',30,0,0.75 UNION ALL
Select '87036084','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703608410','New','UNT',30,0,0.8 UNION ALL
Select '8703608490','Other','UNT',30,0,0.8 UNION ALL
Select '87036085','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703608510','New','UNT',30,0,0.9 UNION ALL
Select '8703608590','Other','UNT',30,0,0.9 UNION ALL
Select '87036086','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703608610','New','UNT',30,0,1.05 UNION ALL
Select '8703608690','Other','UNT',30,0,1.05 UNION ALL
Select '87036087','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703608710','New','UNT',30,0,1.05 UNION ALL
Select '8703608790','Other','UNT',30,0,1.05 UNION ALL

Select '87036091','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703609110','New','UNT',30,0,0 UNION ALL
Select '8703609190','Other','UNT',30,0,0 UNION ALL
Select '87036092','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703609210','New','UNT',30,0,0 UNION ALL
Select '8703609290','Other','UNT',30,0,0 UNION ALL
Select '87036093','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703609310','New','UNT',30,0,0.65 UNION ALL
Select '8703609390','Other','UNT',30,0,0.65 UNION ALL
Select '87036094','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703609410','New','UNT',30,0,0.75 UNION ALL
Select '8703609490','Other','UNT',30,0,0.75 UNION ALL
Select '87036095','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703609510','New','UNT',30,0,0.9 UNION ALL
Select '8703609590','Other','UNT',30,0,0.9 UNION ALL
Select '87036096','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703609610','New','UNT',30,0,1.05 UNION ALL
Select '8703609690','Other','UNT',30,0,1.05 UNION ALL
Select '87036097','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703609710','New','UNT',30,0,1.05 UNION ALL
Select '8703609790','Other','UNT',30,0,1.05 UNION ALL
Select '87036098','Of a cylinder capacity exceeding 3,000 cc, not of fourwheel drive:','',0,0,0 UNION ALL
Select '8703609810','New','UNT',30,0,1.05 UNION ALL
Select '8703609890','Other','UNT',30,0,1.05 UNION ALL


Select '8703701100','Gokarts','UNT',10,0,0 UNION ALL
Select '8703701200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703701300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703701400','Hearses','UNT',10,0,0 UNION ALL
Select '8703701500','Prison vans','UNT',10,0,0 UNION ALL

Select '8703701600','Of a cylinder capacity not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703701700','Of a cylinder capacity exceeding 2,000 cc','UNT',35,0,0 UNION ALL

Select '8703701800','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703701900','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703702100','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703702200','Of a cylinder capacity not exceeding 1,500 cc','UNT',10,0,0 UNION ALL
Select '8703702300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703702400','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703702500','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703702600','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703702700','Of a cylinder capacity not exceeding 2,000 cc','UNT',10,0,0 UNION ALL
Select '8703702800','Of a cylinder capacity exceeding 2,000 cc','UNT',10,0,0 UNION ALL

Select '8703703100','Gokarts','UNT',30,0,0 UNION ALL

Select '8703703200','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703703300','Of a cylinder capacity exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL

Select '8703703400','Of a cylinder capacity not exceeding 1,500 cc ','UNT',5,0,0 UNION ALL
Select '8703703500','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,500 cc','UNT',5,0,0 UNION ALL
Select '8703703600','Of a cylinder capacity exceeding 2,500 cc','UNT',5,0,0 UNION ALL

Select '8703704100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703704200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703704300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703704400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc','UNT',30,0,0.75 UNION ALL
Select '8703704500','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703704600','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL
Select '8703704700','Of a cylinder capacity exceeding 3,000 cc','UNT',30,0,1.05 UNION ALL

Select '8703705100','Of a cylinder capacity not exceeding 1,000 cc','UNT',30,0,0.65 UNION ALL
Select '8703705200','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc','UNT',30,0,0.65 UNION ALL
Select '8703705300','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc','UNT',30,0,0.65 UNION ALL
Select '8703705400','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,500 cc','UNT',30,0,0.9 UNION ALL
Select '8703705500','Of a cylinder capacity exceeding 2,500 cc','UNT',30,0,1.05 UNION ALL

Select '8703705600','Of a cylinder capacity not exceeding 1,500 cc','UNT',35,0,0 UNION ALL
Select '8703705700','Of a cylinder capacity exceeding 1,500 cc but not exceeding 2,000 cc','UNT',35,0,0 UNION ALL
Select '8703705800','Of a cylinder capacity exceeding 2,000 cc ','UNT',35,0,0 UNION ALL

Select '87037061','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703706110','New','UNT',30,0,0.75 UNION ALL
Select '8703706190','Other','UNT',30,0,0.75 UNION ALL
Select '87037062','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703706210','New','UNT',30,0,0.75 UNION ALL
Select '8703706290','Other','UNT',30,0,0.75 UNION ALL
Select '87037063','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703706310','New','UNT',30,0,0.75 UNION ALL
Select '8703706390','Other','UNT',30,0,0.75 UNION ALL
Select '87037064','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703706410','New','UNT',30,0,0.8 UNION ALL
Select '8703706490','Other','UNT',30,0,0.8 UNION ALL
Select '87037065','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703706510','New','UNT',30,0,0.9 UNION ALL
Select '8703706590','Other','UNT',30,0,0.9 UNION ALL
Select '87037066','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703706610','New','UNT',30,0,1.05 UNION ALL
Select '8703706690','Other','UNT',30,0,1.05 UNION ALL
Select '87037067','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL

Select '87037071','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703707110','New','UNT',30,0,0.75 UNION ALL
Select '8703707190','Other','UNT',30,0,0.75 UNION ALL
Select '87037072','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703707210','New','UNT',30,0,0.75 UNION ALL
Select '8703707290','Other','UNT',30,0,0.75 UNION ALL
Select '87037073','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703707310','New','UNT',30,0,0.75 UNION ALL
Select '8703707390','Other','UNT',30,0,0.75 UNION ALL
Select '87037074','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703707410','New','UNT',30,0,0.8 UNION ALL
Select '8703707490','Other','UNT',30,0,0.8 UNION ALL
Select '87037075','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703707510','New','UNT',30,0,0.9 UNION ALL
Select '8703707590','Other','UNT',30,0,0.9 UNION ALL
Select '87037076','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703707610','New','UNT',30,0,1.05 UNION ALL
Select '8703707690','Other','UNT',30,0,1.05 UNION ALL
Select '87037077','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703707710','New','UNT',30,0,1.05 UNION ALL
Select '8703707790','Other','UNT',30,0,1.05 UNION ALL

Select '87037081','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703708110','New','UNT',30,0,0.75 UNION ALL
Select '8703708190','Other','UNT',30,0,0.75 UNION ALL
Select '87037082','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703708210','New','UNT',30,0,0.75 UNION ALL
Select '8703708290','Other','UNT',30,0,0.75 UNION ALL
Select '87037083','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703708310','New','UNT',30,0,0.75 UNION ALL
Select '8703708390','Other','UNT',30,0,0.75 UNION ALL
Select '87037084','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703708410','New','UNT',30,0,0.8 UNION ALL
Select '8703708490','Other','UNT',30,0,0.8 UNION ALL
Select '87037085','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703708510','New','UNT',30,0,0.9 UNION ALL
Select '8703708590','Other','UNT',30,0,0.9 UNION ALL
Select '87037086','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc','',0,0,0 UNION ALL
Select '8703708610','New','UNT',30,0,1.05 UNION ALL
Select '8703708690','Other','UNT',30,0,1.05 UNION ALL
Select '87037087','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703708710','New','UNT',30,0,1.05 UNION ALL
Select '8703708790','Other','UNT',30,0,1.05 UNION ALL

Select '87037091','Of a cylinder capacity not exceeding 1,000 cc:','',0,0,0 UNION ALL
Select '8703709110','New','UNT',30,0,0.65 UNION ALL
Select '8703709190','Other','UNT',30,0,0.65 UNION ALL
Select '87037092','Of a cylinder capacity exceeding 1,000 cc but not exceeding 1,500 cc:','',0,0,0 UNION ALL
Select '8703709210','New','UNT',30,0,0.65 UNION ALL
Select '8703709290','Other','UNT',30,0,0.65 UNION ALL
Select '87037093','Of a cylinder capacity exceeding 1,500 cc but not exceeding 1,800 cc:','',0,0,0 UNION ALL
Select '8703709310','New','UNT',30,0,0.65 UNION ALL
Select '8703709390','Other','UNT',30,0,0.65 UNION ALL
Select '87037094','Of a cylinder capacity exceeding 1,800 cc but not exceeding 2,000 cc:','',0,0,0 UNION ALL
Select '8703709410','New','UNT',30,0,0.75 UNION ALL
Select '8703709490','Other','UNT',30,0,0.75 UNION ALL
Select '87037095','Of a cylinder capacity exceeding 2,000 cc but not exceeding 2,500 cc:','',0,0,0 UNION ALL
Select '8703709510','New','UNT',30,0,0.9 UNION ALL
Select '8703709590','Other','UNT',30,0,0.9 UNION ALL
Select '87037096','Of a cylinder capacity exceeding 2,500 cc but not exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703709610','New','UNT',30,0,1.05 UNION ALL
Select '8703709690','Other','UNT',30,0,1.05 UNION ALL
Select '87037097','Of a cylinder capacity exceeding 3,000 cc:','',0,0,0 UNION ALL
Select '8703709710','New','UNT',30,0,1.05 UNION ALL
Select '8703709790','Other','UNT',30,0,1.05 UNION ALL


Select '8703801100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703801200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703801300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703801400','Hearses','UNT',10,0,0 UNION ALL
Select '8703801500','Prison vans','UNT',10,0,0 UNION ALL
Select '8703801600','Motorhomes','UNT',35,0,0 UNION ALL
Select '8703801700','Sedan','UNT',10,0,0 UNION ALL
Select '8703801800','Other motor cars (including station wagons and sports cars, but not including vans)','UNT',10,0,0 UNION ALL
Select '8703801900','Other','UNT',10,0,0 UNION ALL

Select '8703809100','Gokarts','UNT',0,0,0 UNION ALL
Select '8703809200','AllTerrain Vehicles (ATV)','UNT',30,0,0.1 UNION ALL
Select '8703809300','Ambulances','UNT',5,0,0 UNION ALL
Select '8703809400','Hearses','UNT',30,0,0.1 UNION ALL
Select '8703809500','Prison vans','UNT',30,0,0.1 UNION ALL
Select '8703809600','Motorhomes','UNT',35,0,0 UNION ALL
Select '87038097','Sedan:','',0,0,0 UNION ALL
Select '8703809710','New ','UNT',30,0,0.1 UNION ALL
Select '8703809790','Other','UNT',30,0,0.1 UNION ALL
Select '87038098','Other motor cars (including station wagons and sports cars, but not including vans):','',0,0,0 UNION ALL
Select '8703809810','New ','UNT',30,0,0.1 UNION ALL
Select '8703809890','Other','UNT',30,0,0.1 UNION ALL
Select '87038099','Other:','',0,0,0 UNION ALL
Select '8703809910','New ','UNT',30,0,0.1 UNION ALL
Select '8703809990','Other','UNT',30,0,0.1 UNION ALL


Select '8703901100','Gokarts','UNT',10,0,0 UNION ALL
Select '8703901200','AllTerrain Vehicles (ATV)','UNT',10,0,0 UNION ALL
Select '8703901300','Ambulances','UNT',10,0,0 UNION ALL
Select '8703901400','Hearses','UNT',10,0,0 UNION ALL
Select '8703901500','Prison vans','UNT',10,0,0 UNION ALL
Select '8703901600','Motorhomes','UNT',10,0,0 UNION ALL
Select '8703901700','Sedan','UNT',10,0,0 UNION ALL
Select '8703901800','Other motor cars (including station wagons and sports cars, but not including vans)','UNT',10,0,0 UNION ALL
Select '8703901900','Other','UNT',10,0,0 UNION ALL

Select '8703909100','Gokarts','UNT',30,0,0 UNION ALL
Select '8703909200','AllTerrain Vehicles (ATV)','UNT',30,0,0.65 UNION ALL
Select '8703909300','Ambulances','UNT',30,0,0 UNION ALL
Select '8703909400','Hearses','UNT',30,0,0.65 UNION ALL
Select '8703909500','Prison vans','UNT',30,0,0.65 UNION ALL
Select '8703909600','Motorhomes','UNT',35,0,0 UNION ALL
Select '87039097','Sedan:','',0,0,0 UNION ALL
Select '8703909710','New ','UNT',30,0,0.6 UNION ALL
Select '8703909790','Other','UNT',30,0,0.6 UNION ALL
Select '87039098','Other motor cars (including station wagons and sports cars, but not including vans):','',0,0,0 UNION ALL
Select '8703909810','New ','UNT',30,0,0.6 UNION ALL
Select '8703909890','Other','UNT',30,0,0.6 UNION ALL
Select '87039099','Other:','',0,0,0 UNION ALL
Select '8703909910','New ','UNT',30,0,0.65 UNION ALL
Select '8703909990','Other','UNT',30,0,0.65 UNION ALL



Select '8704101300','g.v.w. not exceeding 5 t','UNT',0,0,0 UNION ALL
Select '8704101400','g.v.w. exceeding 5 t but not exceeding 10 t','UNT',0,0,0 UNION ALL
Select '8704101500','g.v.w. exceeding 10 t but not exceeding 20 t','UNT',0,0,0 UNION ALL
Select '8704101600','g.v.w. exceeding 20 t but not exceeding 24 t','UNT',0,0,0 UNION ALL
Select '8704101700','g.v.w. exceeding 24 t but not exceeding 45 t','UNT',0,0,0 UNION ALL
Select '8704101800','g.v.w. exceeding 45 t','UNT',0,0,0 UNION ALL

Select '87041031','g.v.w. not exceeding 5 t:','',0,0,0 UNION ALL
Select '8704103110','New','UNT',30,0,0 UNION ALL
Select '8704103190','Other','UNT',30,0,0 UNION ALL
Select '87041032','g.v.w. exceeding 5 t but not exceeding 10 t:','',0,0,0 UNION ALL
Select '8704103210','New','UNT',30,0,0 UNION ALL
Select '8704103290','Other','UNT',30,0,0 UNION ALL
Select '87041033','g.v.w. exceeding 10 t but not exceeding 20 t:','',0,0,0 UNION ALL
Select '8704103310','New','UNT',30,0,0 UNION ALL
Select '8704103390','Other','UNT',30,0,0 UNION ALL
Select '87041034','g.v.w. exceeding 20 t but not exceeding 24 t:','',0,0,0 UNION ALL
Select '8704103410','New','UNT',30,0,0 UNION ALL
Select '8704103490','Other','UNT',30,0,0 UNION ALL
Select '87041035','g.v.w. exceeding 24 t but not exceeding 38 t:','',0,0,0 UNION ALL
Select '8704103510','New','UNT',30,0,0 UNION ALL
Select '8704103590','Other','UNT',30,0,0 UNION ALL
Select '87041036','g.v.w exceeding 38, but not exceeding 45 t:','',0,0,0 UNION ALL
Select '8704103610','New','UNT',5,0,0 UNION ALL
Select '8704103690','Other','UNT',30,0,0 UNION ALL
Select '87041037','g.v.w. exceeding 45 t:','',0,0,0 UNION ALL
Select '8704103710','New','UNT',5,0,0 UNION ALL
Select '8704103790','Other','UNT',30,0,0 UNION ALL



Select '8704211100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704211900','Other','UNT',0,0,0 UNION ALL

Select '87042121','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704212110','New','UNT',30,0,0 UNION ALL
Select '8704212190','Other','UNT',30,0,0 UNION ALL
Select '87042122','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704212210','New','UNT',30,0,0 UNION ALL
Select '8704212290','Other','UNT',30,0,0 UNION ALL
Select '87042123','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704212310','New','UNT',30,0,0 UNION ALL
Select '8704212390','Other','UNT',30,0,0 UNION ALL
Select '87042124','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704212410','New','UNT',30,0,0 UNION ALL
Select '8704212490','Other','UNT',30,0,0 UNION ALL
Select '87042125','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704212510','New','UNT',30,0,0 UNION ALL
Select '8704212590','Other','UNT',30,0,0 UNION ALL
Select '87042129','Other:','',0,0,0 UNION ALL
Select '8704212910','New','UNT',30,0,0 UNION ALL
Select '8704212990','Other','UNT',30,0,0 UNION ALL



Select '8704221100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704221900','Other','UNT',0,0,0 UNION ALL

Select '87042221','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704222110','New','UNT',30,0,0 UNION ALL
Select '8704222190','Other','UNT',30,0,0 UNION ALL
Select '87042222','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704222210','New','UNT',30,0,0 UNION ALL
Select '8704222290','Other','UNT',30,0,0 UNION ALL
Select '87042223','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704222310','New','UNT',30,0,0 UNION ALL
Select '8704222390','Other','UNT',30,0,0 UNION ALL
Select '87042224','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704222410','New','UNT',30,0,0 UNION ALL
Select '8704222490','Other','UNT',30,0,0 UNION ALL
Select '87042225','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704222510','New','UNT',30,0,0 UNION ALL
Select '8704222590','Other','UNT',30,0,0 UNION ALL
Select '87042229','Other:','',0,0,0 UNION ALL
Select '8704222910','New','UNT',30,0,0 UNION ALL
Select '8704222990','Other','UNT',30,0,0 UNION ALL


Select '8704223100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704223900','Other','UNT',0,0,0 UNION ALL

Select '87042241','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704224110','New','UNT',30,0,0 UNION ALL
Select '8704224190','Other','UNT',30,0,0 UNION ALL
Select '87042242','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704224210','New','UNT',30,0,0 UNION ALL
Select '8704224290','Other','UNT',30,0,0 UNION ALL
Select '87042243','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704224310','New','UNT',30,0,0 UNION ALL
Select '8704224390','Other','UNT',30,0,0 UNION ALL
Select '87042245','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704224510','New','UNT',30,0,0 UNION ALL
Select '8704224590','Other','UNT',30,0,0 UNION ALL
Select '87042246','Armoured cargo vehicles for transporting valuables, g.v.w. exceeding 6 t but not exceeding 10 t:','',0,0,0 UNION ALL
Select '8704224610','New','UNT',30,0,0 UNION ALL
Select '8704224690','Other','UNT',30,0,0 UNION ALL
Select '87042247','Armoured cargo vehicles for transporting valuables, g.v.w. exceeding 10t but not exceeding 20 t: ','',0,0,0 UNION ALL
Select '8704224710','New','UNT',30,0,0 UNION ALL
Select '8704224790','Other','UNT',30,0,0 UNION ALL

Select '87042251','g.v.w. exceeding 6 t but not exceeding 10 t:','',0,0,0 UNION ALL
Select '8704225110','New','UNT',30,0,0 UNION ALL
Select '8704225190','Other','UNT',30,0,0 UNION ALL
Select '87042259','Other:','',0,0,0 UNION ALL
Select '8704225910','New','UNT',30,0,0 UNION ALL
Select '8704225990','Other','UNT',30,0,0 UNION ALL



Select '8704231100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704231900','Other','UNT',0,0,0 UNION ALL

Select '87042321','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704232110','New','UNT',30,0,0 UNION ALL
Select '8704232190','Other','UNT',30,0,0 UNION ALL
Select '87042322','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704232210','New','UNT',30,0,0 UNION ALL
Select '8704232290','Other','UNT',30,0,0 UNION ALL
Select '87042323','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704232310','New','UNT',30,0,0 UNION ALL
Select '8704232390','Other','UNT',30,0,0 UNION ALL
Select '87042324','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704232410','New','UNT',30,0,0 UNION ALL
Select '8704232490','Other','UNT',30,0,0 UNION ALL
Select '87042325','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704232510','New','UNT',30,0,0 UNION ALL
Select '8704232590','Other','UNT',30,0,0 UNION ALL
Select '87042329','Other:','',0,0,0 UNION ALL
Select '8704232910','New','UNT',30,0,0 UNION ALL
Select '8704232990','Other','UNT',30,0,0 UNION ALL


Select '8704235100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704235900','Other','UNT',0,0,0 UNION ALL

Select '87042361','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704236110','New','UNT',30,0,0 UNION ALL
Select '8704236190','Other','UNT',30,0,0 UNION ALL
Select '87042362','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704236210','New','UNT',30,0,0 UNION ALL
Select '8704236290','Other','UNT',30,0,0 UNION ALL
Select '87042363','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704236310','New','UNT',30,0,0 UNION ALL
Select '8704236390','Other','UNT',30,0,0 UNION ALL
Select '87042364','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704236410','New','UNT',30,0,0 UNION ALL
Select '8704236490','Other','UNT',30,0,0 UNION ALL
Select '87042365','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704236510','New','UNT',30,0,0 UNION ALL
Select '8704236590','Other','UNT',30,0,0 UNION ALL
Select '87042366','Dumpers:','',0,0,0 UNION ALL
Select '8704236610','New','UNT',30,0,0 UNION ALL
Select '8704236690','Other','UNT',30,0,0 UNION ALL
Select '87042369','Other:','',0,0,0 UNION ALL
Select '8704236910','New','UNT',30,0,0 UNION ALL
Select '8704236990','Other','UNT',30,0,0 UNION ALL


Select '8704237100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704237900','Other','UNT',0,0,0 UNION ALL

Select '87042381','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704238110','New','UNT',30,0,0 UNION ALL
Select '8704238190','Other','UNT',30,0,0 UNION ALL
Select '87042382','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704238210','New','UNT',30,0,0 UNION ALL
Select '8704238290','Other','UNT',30,0,0 UNION ALL
Select '87042384','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704238410','New','UNT',30,0,0 UNION ALL
Select '8704238490','Other','UNT',30,0,0 UNION ALL
Select '87042385','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704238510','New','UNT',30,0,0 UNION ALL
Select '8704238590','Other','UNT',30,0,0 UNION ALL
Select '87042386','Dumpers:','',0,0,0 UNION ALL
Select '8704238610','New','UNT',30,0,0 UNION ALL
Select '8704238690','Other','UNT',30,0,0 UNION ALL
Select '87042389','Other:','',0,0,0 UNION ALL
Select '8704238910','New','UNT',30,0,0 UNION ALL
Select '8704238990','Other','UNT',30,0,0 UNION ALL



Select '8704311100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704311900','Other','UNT',0,0,0 UNION ALL

Select '87043121','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704312110','New','UNT',30,0,0 UNION ALL
Select '8704312190','Other','UNT',30,0,0 UNION ALL
Select '87043122','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704312210','New','UNT',30,0,0 UNION ALL
Select '8704312290','Other','UNT',30,0,0 UNION ALL
Select '87043123','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704312310','New','UNT',30,0,0 UNION ALL
Select '8704312390','Other','UNT',30,0,0 UNION ALL
Select '87043124','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704312410','New','UNT',30,0,0 UNION ALL
Select '8704312490','Other','UNT',30,0,0 UNION ALL
Select '87043125','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704312510','New','UNT',30,0,0 UNION ALL
Select '8704312590','Other','UNT',30,0,0 UNION ALL
Select '87043129','Other:','',0,0,0 UNION ALL
Select '8704312910','New','UNT',30,0,0 UNION ALL
Select '8704312990','Other','UNT',30,0,0 UNION ALL



Select '8704321100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704321900','Other','UNT',0,0,0 UNION ALL

Select '87043221','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704322110','New','UNT',30,0,0 UNION ALL
Select '8704322190','Other','UNT',30,0,0 UNION ALL
Select '87043222','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704322210','New','UNT',30,0,0 UNION ALL
Select '8704322290','Other','UNT',30,0,0 UNION ALL
Select '87043223','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704322310','New','UNT',30,0,0 UNION ALL
Select '8704322390','Other','UNT',30,0,0 UNION ALL
Select '87043224','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704322410','New','UNT',30,0,0 UNION ALL
Select '8704322490','Other','UNT',30,0,0 UNION ALL
Select '87043225','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704322510','New','UNT',30,0,0 UNION ALL
Select '8704322590','Other','UNT',30,0,0 UNION ALL
Select '87043229','Other:','',0,0,0 UNION ALL
Select '8704322910','New','UNT',30,0,0 UNION ALL
Select '8704322990','Other','UNT',30,0,0 UNION ALL


Select '8704323100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704323900','Other','UNT',0,0,0 UNION ALL

Select '87043241','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704324110','New','UNT',30,0,0 UNION ALL
Select '8704324190','Other','UNT',30,0,0 UNION ALL
Select '87043242','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704324210','New','UNT',30,0,0 UNION ALL
Select '8704324290','Other','UNT',30,0,0 UNION ALL
Select '87043243','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704324310','New','UNT',30,0,0 UNION ALL
Select '8704324390','Other','UNT',30,0,0 UNION ALL
Select '87043244','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704324410','New','UNT',30,0,0 UNION ALL
Select '8704324490','Other','UNT',30,0,0 UNION ALL
Select '87043245','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704324510','New','UNT',30,0,0 UNION ALL
Select '8704324590','Other','UNT',30,0,0 UNION ALL
Select '87043248','Other, of a g.v.w. exceeding 6 t but not exceeding 10 t:','',0,0,0 UNION ALL
Select '8704324810','New','UNT',30,0,0 UNION ALL
Select '8704324890','Other','UNT',30,0,0 UNION ALL
Select '87043249','Other:','',0,0,0 UNION ALL
Select '8704324910','New','UNT',30,0,0 UNION ALL
Select '8704324990','Other','UNT',30,0,0 UNION ALL


Select '8704325100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704325900','Other','UNT',0,0,0 UNION ALL

Select '87043261','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704326110','New','UNT',30,0,0 UNION ALL
Select '8704326190','Other','UNT',30,0,0 UNION ALL
Select '87043262','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704326210','New','UNT',30,0,0 UNION ALL
Select '8704326290','Other','UNT',30,0,0 UNION ALL
Select '87043263','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704326310','New','UNT',30,0,0 UNION ALL
Select '8704326390','Other','UNT',30,0,0 UNION ALL
Select '87043264','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704326410','New','UNT',30,0,0 UNION ALL
Select '8704326490','Other','UNT',30,0,0 UNION ALL
Select '87043265','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704326510','New','UNT',30,0,0 UNION ALL
Select '8704326590','Other','UNT',30,0,0 UNION ALL
Select '87043269','Other:','',0,0,0 UNION ALL
Select '8704326910','New','UNT',30,0,0 UNION ALL
Select '8704326990','Other','UNT',30,0,0 UNION ALL


Select '8704327200','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704327900','Other','UNT',0,0,0 UNION ALL

Select '87043281','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704328110','New','UNT',30,0,0 UNION ALL
Select '8704328190','Other','UNT',30,0,0 UNION ALL
Select '87043282','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704328210','New','UNT',30,0,0 UNION ALL
Select '8704328290','Other','UNT',30,0,0 UNION ALL
Select '87043283','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704328310','New','UNT',30,0,0 UNION ALL
Select '8704328390','Other','UNT',30,0,0 UNION ALL
Select '87043284','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704328410','New','UNT',30,0,0 UNION ALL
Select '8704328490','Other','UNT',30,0,0 UNION ALL
Select '87043285','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704328510','New','UNT',30,0,0 UNION ALL
Select '8704328590','Other','UNT',30,0,0 UNION ALL
Select '87043286','Dumpers:','',0,0,0 UNION ALL
Select '8704328610','New','UNT',30,0,0 UNION ALL
Select '8704328690','Other','UNT',30,0,0 UNION ALL
Select '87043289','Other:','',0,0,0 UNION ALL
Select '8704328910','New','UNT',30,0,0 UNION ALL
Select '8704328990','Other','UNT',30,0,0 UNION ALL


Select '8704329100','Refrigerated lorries (trucks)','UNT',0,0,0 UNION ALL
Select '8704329200','Other','UNT',0,0,0 UNION ALL

Select '87043293','Refrigerated lorries (trucks):','',0,0,0 UNION ALL
Select '8704329310','New','UNT',30,0,0 UNION ALL
Select '8704329390','Other','UNT',30,0,0 UNION ALL
Select '87043294','Refuse/garbage collection vehicles having a refuse compressing device:','',0,0,0 UNION ALL
Select '8704329410','New','UNT',30,0,0 UNION ALL
Select '8704329490','Other','UNT',30,0,0 UNION ALL
Select '87043295','Tanker vehicles; bulkcement lorries (trucks):','',0,0,0 UNION ALL
Select '8704329510','New','UNT',30,0,0 UNION ALL
Select '8704329590','Other','UNT',30,0,0 UNION ALL
Select '87043296','Armoured cargo vehicles for transporting valuables:','',0,0,0 UNION ALL
Select '8704329610','New','UNT',30,0,0 UNION ALL
Select '8704329690','Other','UNT',30,0,0 UNION ALL
Select '87043297','Hooklift lorries (trucks):','',0,0,0 UNION ALL
Select '8704329710','New','UNT',30,0,0 UNION ALL
Select '8704329790','Other','UNT',30,0,0 UNION ALL
Select '87043298','Dumpers:','',0,0,0 UNION ALL
Select '8704329810','New','UNT',30,0,0 UNION ALL
Select '8704329890','Other','UNT',30,0,0 UNION ALL
Select '87043299','Other:','',0,0,0 UNION ALL
Select '8704329910','New','UNT',30,0,0 UNION ALL
Select '8704329990','Other','UNT',30,0,0 UNION ALL

Select '8704901000','Completely Knocked Down','UNT',0,0,0 UNION ALL

Select '87049091','g.v.w. not exceeding 5 t:','',0,0,0 UNION ALL
Select '8704909110','New','UNT',30,0,0 UNION ALL
Select '8704909190','Other','UNT',30,0,0 UNION ALL
Select '87049092','g.v.w. exceeding 5 t but not exceeding 10 t:','',0,0,0 UNION ALL
Select '8704909210','New','UNT',30,0,0 UNION ALL
Select '8704909290','Other','UNT',30,0,0 UNION ALL
Select '87049093','g.v.w. exceeding 10 t but not exceeding 20 t:','',0,0,0 UNION ALL
Select '8704909310','New','UNT',30,0,0 UNION ALL
Select '8704909390','Other','UNT',30,0,0 UNION ALL
Select '87049094','g.v.w. exceeding 20 t but not exceeding 45 t:','',0,0,0 UNION ALL
Select '8704909410','New','UNT',30,0,0 UNION ALL
Select '8704909490','Other','UNT',30,0,0 UNION ALL
Select '87049095','g.v.w exceeding 45 t:','',0,0,0 UNION ALL
Select '8704909510','New','UNT',30,0,0 UNION ALL
Select '8704909590','Other','UNT',30,0,0 UNION ALL

Select '8705100000','Crane lorries','UNT',30,0,0 UNION ALL
Select '8705200000','Mobile drilling derricks','UNT',30,0,0 UNION ALL
Select '8705300000','Fire fighting vehicles','UNT',30,0,0 UNION ALL
Select '8705400000','Concretemixer lorries','UNT',30,0,0 UNION ALL

Select '8705905000','Street cleaning vehicles, cesspool emptiers; mobile clinics; spraying lorries of all kinds','UNT',30,0,0 UNION ALL
Select '8705906000','Mobile explosive production vehicles','UNT',30,0,0 UNION ALL
Select '8705909000','Other','UNT',30,0,0 UNION ALL


Select '8706001100','For agricultural tractors of subheading  8701.10.11, 8701.10.91, 8701.91.10, 8701.92.10, 8701.93.10, 8701.94.10 or 8701.95.10','UNT',0,0,0 UNION ALL
Select '8706001900','Other','UNT',0,0,0 UNION ALL

Select '8706002100','For motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL
Select '8706002200','Other, for vehicles with g.v.w. not exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8706002300','Other, for vehicles with g.v.w. exceeding 24 t','UNT',30,0,0 UNION ALL

Select '8706003100','For gokarts and golf cars (including golf buggies) and similar vehicles ','UNT',30,0,0 UNION ALL
Select '8706003200','For ambulances','UNT',0,0,0 UNION ALL
Select '8706003300','For motor cars (including station wagons, and sports cars, but not including vans)','UNT',30,0,0 UNION ALL
Select '8706003900','Other','UNT',30,0,0 UNION ALL

Select '8706004100','For vehicles with g.v.w. not exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8706004200','For vehicles with g.v.w. exceeding 24 t','UNT',30,0,0 UNION ALL
Select '8706005000','For vehicles of heading 87.05','UNT',30,0,0 UNION ALL


Select '8707101000','For gokarts and golf cars (including golf buggies) and similar vehicles','UNT',30,0,0 UNION ALL
Select '8707102000','For ambulances','UNT',0,0,0 UNION ALL
Select '8707109000','Other','UNT',30,0,0 UNION ALL


Select '8707901100','Driver''s cabin for vehicles of subheading 8701.20','UNT',0,0,0 UNION ALL
Select '8707901900','Other','UNT',0,0,0 UNION ALL

Select '8707902100','For motor cars (including stretch limousines but not including coaches, buses, minibuses or vans)','UNT',30,0,0 UNION ALL
Select '8707902900','Other','UNT',30,0,0 UNION ALL
Select '8707903000','For vehicles of heading 87.05','UNT',30,0,0 UNION ALL
Select '8707904000','Driver''s cabin for dumpers designed for offhighway use','UNT',30,0,0 UNION ALL
Select '8707909000','Other','UNT',30,0,0 UNION ALL


Select '8708101000','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708109000','Other','KGM',25,0,0 UNION ALL

Select '8708210000','Safety seat belts','KGM',30,0,0 UNION ALL


Select '8708291100','For vehicles of heading 87.01    ','KGM',0,0,0 UNION ALL
Select '8708291500','Door armrests for vehicles of heading 87.03','KGM',25,0,0 UNION ALL
Select '8708291600','Other, for vehicles of heading 87.03','KGM',25,0,0 UNION ALL
Select '8708291700','For vehicles of subheading 8704.10','KGM',25,0,0 UNION ALL
Select '8708291800','For vehicles of heading 87.02 or other vehicles of heading 87.04','KGM',25,0,0 UNION ALL
Select '8708291900','Other','KGM',25,0,0 UNION ALL
Select '87082920','Parts of safety seat belts:','',0,0,0 UNION ALL
Select '8708292010','For vehicles of heading 87.01    ','KGM',0,0,0 UNION ALL
Select '8708292090','Other','KGM',25,0,0 UNION ALL

Select '8708299200','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL

Select '8708299300','Interior trim fittings; mudguards','KGM',25,0,0 UNION ALL
Select '8708299400','Hood rods','KGM',25,0,0 UNION ALL
Select '8708299500','Other','KGM',25,0,0 UNION ALL

Select '8708299600','Interior trim fittings; mudguards','KGM',25,0,0 UNION ALL
Select '8708299700','Hood rods','KGM',25,0,0 UNION ALL
Select '8708299800','Other','KGM',25,0,0 UNION ALL
Select '8708299900','Other','KGM',25,0,0 UNION ALL

Select '8708301000','For vehicles of heading 87.01  ','KGM',5,0,0 UNION ALL

Select '8708302100','Brake drums, brake discs or brake pipes ','KGM',30,0,0 UNION ALL
Select '8708302900','Other','KGM',30,0,0 UNION ALL
Select '8708303000','Brake drums, brake discs or brake pipes for vehicles of heading 87.02 or 87.04','KGM',30,0,0 UNION ALL
Select '8708309000','Other','KGM',30,0,0 UNION ALL


Select '8708401100','For vehicles of heading 87.03  ','KGM',25,0,0 UNION ALL
Select '8708401300','For vehicles of heading 87.04 or 87.05','KGM',25,0,0 UNION ALL
Select '8708401400','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708401900','Other','KGM',25,0,0 UNION ALL

Select '8708402500','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708402600','For vehicles of heading 87.03 ','KGM',25,0,0 UNION ALL
Select '8708402700','For vehicles of heading 87.04 or 87.05','KGM',25,0,0 UNION ALL
Select '8708402900','Other','KGM',25,0,0 UNION ALL

Select '8708409100','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708409200','For vehicles of heading 87.03','KGM',25,0,0 UNION ALL
Select '8708409900','Other','KGM',25,0,0 UNION ALL


Select '8708501100','For vehicles of heading 87.03  ','KGM',25,0,0 UNION ALL
Select '8708501300','For vehicles of heading 87.04 or 87.05','KGM',25,0,0 UNION ALL
Select '8708501500','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708501900','Other','KGM',25,0,0 UNION ALL

Select '8708502500','For vehicles of heading 87.01','KGM',0,0,0 UNION ALL
Select '8708502600','For vehicles of heading 87.03 ','KGM',25,0,0 UNION ALL
Select '8708502700','For vehicles of heading 87.04 or 87.05','KGM',25,0,0 UNION ALL
Select '8708502900','Other','KGM',25,0,0 UNION ALL


Select '8708509100','Crown wheels and pinions','KGM',0,0,0 UNION ALL
Select '8708509200','Other','KGM',0,0,0 UNION ALL

Select '8708509400','Crown wheels and pinions','KGM',30,0,0 UNION ALL
Select '8708509500','Other','KGM',25,0,0 UNION ALL

Select '8708509600','Crown wheels and pinions','KGM',30,0,0 UNION ALL
Select '8708509900','Other','KGM',25,0,0 UNION ALL


Select '8708701500','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708701600','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL

Select '8708701700','For vehicles of subheading 8704.10','KGM',30,0,0 UNION ALL
Select '8708701800','Other','KGM',30,0,0 UNION ALL
Select '8708701900','Other','KGM',30,0,0 UNION ALL

Select '8708702100','For vehicles of heading 87.01','KGM',30,0,0 UNION ALL
Select '8708702200','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708702300','For vehicles of heading 87.02 or 87.04 (excluding subheading 8704.10)','KGM',30,0,0 UNION ALL
Select '8708702900','Other','KGM',30,0,0 UNION ALL

Select '8708703100','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708703200','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708703300','For dumpers designed for offhighway use with g.v.w. exceeding 45 t','KGM',30,0,0 UNION ALL
Select '8708703400','For vehicles of heading 87.02 or other vehicles of heading 87.04','KGM',30,0,0 UNION ALL
Select '8708703900','Other','KGM',30,0,0 UNION ALL

Select '8708709500','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708709600','For vehicles of heading 87.02 or 87.04','KGM',30,0,0 UNION ALL
Select '8708709700','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708709900','Other','KGM',30,0,0 UNION ALL


Select '8708801500','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708801600','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708801700','For vehicles of subheading 8704.10 or heading 87.05','KGM',30,0,0 UNION ALL
Select '8708801900','Other','KGM',30,0,0 UNION ALL

Select '8708809100','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708809200','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708809900','Other','KGM',30,0,0 UNION ALL



Select '8708911500','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708911600','For vehicles of heading 87.03 ','KGM',25,0,0 UNION ALL

Select '8708911700','For vehicles of subheading 8704.10','KGM',25,0,0 UNION ALL
Select '8708911800','Other','KGM',25,0,0 UNION ALL
Select '8708911900','Other','KGM',25,0,0 UNION ALL

Select '8708919100','For vehicles of heading 87.01','KGM',5,0,0 UNION ALL
Select '8708919300','Drain plugs, for vehicles of heading 87.03','KGM',25,0,0 UNION ALL
Select '8708919400','Drain plugs, for vehicles of heading 87.02 or 87.04 (excluding subheading 8704.10)','KGM',25,0,0 UNION ALL
Select '8708919500','Other, for vehicles of heading 87.03','KGM',25,0,0 UNION ALL
Select '8708919900','Other','KGM',25,0,0 UNION ALL

Select '8708921000','For vehicles of heading 87.01  ','KGM',5,0,0 UNION ALL
Select '8708922000','For vehicles of heading 87.03  ','KGM',25,0,0 UNION ALL

Select '8708925100','Silencers (mufflers) and exhaust pipes','KGM',25,0,0 UNION ALL
Select '8708925200','Parts','KGM',25,0,0 UNION ALL

Select '8708926100','Silencers (mufflers) and exhaust pipes','KGM',25,0,0 UNION ALL
Select '8708926200','Parts','KGM',25,0,0 UNION ALL
Select '8708929000','Other','KGM',25,0,0 UNION ALL

Select '8708935000','For vehicles of heading 87.01 ','KGM',5,0,0 UNION ALL
Select '8708936000','For vehicles of heading 87.03  ','KGM',25,0,0 UNION ALL
Select '8708937000','For vehicles of heading 87.04 or 87.05','KGM',25,0,0 UNION ALL
Select '8708939000','Other','KGM',25,0,0 UNION ALL


Select '8708941100','For vehicles of heading 87.01 ','KGM',0,0,0 UNION ALL
Select '8708941900','Other','KGM',25,0,0 UNION ALL

Select '8708949400','For vehicles of heading 87.01 ','KGM',0,0,0 UNION ALL
Select '8708949500','For vehicles of heading 87.03  ','KGM',25,0,0 UNION ALL
Select '8708949900','Other','KGM',25,0,0 UNION ALL

Select '8708951000','Safety airbags with inflater system','KGM',30,0,0 UNION ALL
Select '8708959000','Parts','KGM',30,0,0 UNION ALL


Select '8708991100','Unassembled fuel tanks; engine brackets','KGM',5,0,0 UNION ALL
Select '8708991900','Other','KGM',5,0,0 UNION ALL


Select '8708992100','Fuel tanks','KGM',30,0,0 UNION ALL
Select '8708992400','Lower half of the fuel tank; fuel caps; filler pipes; filler hose assembly; fuel tank bands','KGM',30,0,0 UNION ALL
Select '8708992500','Other parts','KGM',30,0,0 UNION ALL
Select '8708993000','Accelerator, brake or clutch pedals','KGM',30,0,0 UNION ALL
Select '8708994000','Battery carriers or trays and brackets therefor','KGM',30,0,0 UNION ALL
Select '8708995000','Radiator shrouds','KGM',30,0,0 UNION ALL

Select '8708996100','For vehicles of heading 87.02','KGM',30,0,0 UNION ALL
Select '8708996200','For vehicles of heading 87.03','KGM',30,0,0 UNION ALL
Select '8708996300','For vehicles of heading 87.04','KGM',30,0,0 UNION ALL
Select '8708997000','Engine brackets','KGM',30,0,0 UNION ALL
Select '8708998000','Other','KGM',30,0,0 UNION ALL

Select '8708999100','Unassembled fuel tanks; engine brackets','KGM',30,0,0 UNION ALL
Select '8708999900','Other','KGM',30,0,0 UNION ALL


Select '8709110000','Electrical','UNT',5,0,0 UNION ALL
Select '8709190000','Other','UNT',5,0,0 UNION ALL
Select '8709900000','Parts','KGM',5,0,0 UNION ALL

Select '8710000000','Tanks and other armoured fighting vehicles, motorised, whether or not fitted with weapons, and parts of such vehicles.','UNT',0,0,0 UNION ALL



Select '8711101200','Mopeds and motorised bicycles','UNT',0,0,0 UNION ALL
Select '8711101400','Powered kick scooters; "pocket motorcycles"','UNT',0,0,0 UNION ALL
Select '8711101500','Other motorcycles and motor scooters','UNT',0,0,0 UNION ALL
Select '8711101900','Other','UNT',0,0,0 UNION ALL

Select '8711109200','Mopeds and motorised bicycles','UNT',30,0,0.2 UNION ALL
Select '87111094','Powered kick scooters; "pocket motorcycles":','',0,0,0 UNION ALL
Select '8711109410','New','UNT',30,0,0.2 UNION ALL
Select '8711109490','Other','UNT',30,0,0.2 UNION ALL
Select '87111095','Other motorcycles and motor scooters:','',0,0,0 UNION ALL
Select '8711109510','New','UNT',30,0,0.2 UNION ALL
Select '8711109590','Other','UNT',30,0,0.2 UNION ALL
Select '87111099','Other:','',0,0,0 UNION ALL
Select '8711109910','New','UNT',30,0,0.2 UNION ALL
Select '8711109990','Other','UNT',30,0,0.2 UNION ALL


Select '8711201100','Motocross motorcycles','UNT',0,0,0 UNION ALL
Select '8711201200','Mopeds and motorised bicycles ','UNT',0,0,0 UNION ALL
Select '8711201300','"Pocket motorcycles"','UNT',0,0,0 UNION ALL

Select '8711201400','Of a cylinder capacity exceeding 150 cc but not exceeding 200 cc','UNT',0,0,0 UNION ALL
Select '8711201500','Of a cylinder capacity exceeding 200 cc but not exceeding 250 cc','UNT',0,0,0 UNION ALL
Select '8711201600','Other','UNT',0,0,0 UNION ALL
Select '8711201900','Other','UNT',0,0,0 UNION ALL

Select '8711209100','Motocross motorcycles','UNT',0,0,0 UNION ALL
Select '8711209200','Mopeds and motorised bicycles ','UNT',30,0,0.2 UNION ALL
Select '87112093','"Pocket motorcycles":','',0,0,0 UNION ALL

Select '8711209311','New','UNT',30,0,0.2 UNION ALL
Select '8711209319','Other','UNT',30,0,0.2 UNION ALL

Select '8711209391','New','UNT',30,0,0.3 UNION ALL
Select '8711209399','Other','UNT',30,0,0.3 UNION ALL

Select '87112094','Of a cylinder capacity exceeding 150 cc but not exceeding 200 cc:','',0,0,0 UNION ALL
Select '8711209410','New','UNT',30,0,0.3 UNION ALL
Select '8711209490','Other','UNT',30,0,0.3 UNION ALL
Select '87112095','Of a cylinder capacity exceeding 200 cc but not exceeding 250 cc:','',0,0,0 UNION ALL
Select '8711209510','New','UNT',30,0,0.3 UNION ALL
Select '8711209590','Other','UNT',30,0,0.3 UNION ALL
Select '87112096','Other:','',0,0,0 UNION ALL
Select '8711209610','New','UNT',30,0,0.2 UNION ALL
Select '8711209690','Other','UNT',30,0,0.2 UNION ALL
Select '87112099','Other:','',0,0,0 UNION ALL

Select '8711209911','New','UNT',30,0,0.2 UNION ALL
Select '8711209919','Other','UNT',30,0,0.2 UNION ALL

Select '8711209921','New','UNT',30,0,0.3 UNION ALL
Select '8711209929','Other','UNT',30,0,0.3 UNION ALL


Select '8711301100','Completely Knocked Down','UNT',0,0,0 UNION ALL
Select '8711301900','Other','UNT',0,0,0 UNION ALL
Select '8711303000','Other, Completely Knocked Down','UNT',5,0,0 UNION ALL
Select '87113090','Other:','',0,0,0 UNION ALL
Select '8711309010','New','UNT',30,0,0.3 UNION ALL
Select '8711309090','Other','UNT',30,0,0.3 UNION ALL


Select '8711401100','Completely Knocked Down','UNT',0,0,0 UNION ALL
Select '8711401900','Other','UNT',0,0,0 UNION ALL
Select '8711402000','Other, Completely Knocked Down','UNT',10,0,0 UNION ALL
Select '87114090','Other:','',0,0,0 UNION ALL
Select '8711409010','New','UNT',30,0,0.3 UNION ALL
Select '8711409090','Other','UNT',30,0,0.3 UNION ALL

Select '8711502000','Completely Knocked Down','UNT',10,0,0 UNION ALL
Select '87115090','Other:','',0,0,0 UNION ALL
Select '8711509010','New','UNT',30,0,0.3 UNION ALL
Select '8711509090','Other','UNT',30,0,0.3 UNION ALL


Select '8711601100','Bicycles','UNT',0,0,0 UNION ALL
Select '8711601200','Kick scooters; selfbalancing cycle; "pocket motorcycles"','UNT',5,0,0 UNION ALL
Select '8711601300','Other motorcycles','UNT',5,0,0 UNION ALL
Select '8711601900','Other','UNT',5,0,0 UNION ALL

Select '8711609100','Bicycles','UNT',30,0,0.1 UNION ALL
Select '8711609200','Kick scooters; selfbalancing cycle; "pocket motorcycles"','UNT',30,0,0.1 UNION ALL
Select '8711609300','Other motorcycles','UNT',30,0,0.1 UNION ALL
Select '8711609900','Other','UNT',30,0,0.3 UNION ALL

Select '8711904000','Sidecars','UNT',50,0,0 UNION ALL
Select '8711906000','Other, Completely Knocked Down','UNT',0,0,0 UNION ALL
Select '87119090','Other:','',0,0,0 UNION ALL
Select '8711909010','New','UNT',30,0,0.3 UNION ALL
Select '8711909090','Other','UNT',30,0,0.3 UNION ALL

Select '8712001000','Racing bicycles','UNT',0,0,0 UNION ALL
Select '8712002000','Bicycles designed to be ridden by children','UNT',0,0,0 UNION ALL
Select '8712003000','Other bicycles','UNT',25,0,0 UNION ALL
Select '8712009000','Other','UNT',0,0,0 UNION ALL

Select '8713100000','Not mechanically propelled','UNT',0,0,0 UNION ALL
Select '8713900000','Other','UNT',0,0,0 UNION ALL


Select '8714101000','Saddles','KGM',25,0,0 UNION ALL
Select '8714102000','Spokes and nipples','KGM',30,0,0 UNION ALL
Select '8714103000','Frame and forks including telescopic fork, rear suspension and parts thereof','KGM',25,0,0 UNION ALL
Select '8714104000','Gearing, gearbox, clutch and other transmission equipment and parts thereof','KGM',25,0,0 UNION ALL
Select '8714105000','Wheel rims','KGM',25,0,0 UNION ALL
Select '8714106000','Brakes and parts thereof','KGM',25,0,0 UNION ALL
Select '8714107000','Silencer (muffler) and parts thereof','KGM',25,0,0 UNION ALL
Select '8714109000','Other','KGM',25,0,0 UNION ALL


Select '8714201100','Of a diameter (including tyres) exceeding 75 mm but not exceeding 100 mm, provided that the width of any wheel or tyre fitted thereto is not less than 30 mm','KGM',0,0,0 UNION ALL
Select '8714201200','Of a diameter (including tyres) exceeding 100 mm but not exceeding 250 mm, provided that the width of any wheel or tyre fitted thereto is not less than 30 mm','KGM',0,0,0 UNION ALL
Select '8714201900','Other','KGM',0,0,0 UNION ALL
Select '8714209000','Other','KGM',0,0,0 UNION ALL


Select '8714911000','For bicycles of subheading 8712.00.20','KGM',0,0,0 UNION ALL

Select '8714919100','Parts for forks','KGM',5,0,0 UNION ALL
Select '8714919900','Other','KGM',25,0,0 UNION ALL

Select '8714921000','For bicycles of subheading 8712.00.20 ','KGM',0,0,0 UNION ALL
Select '8714929000','Other','KGM',30,0,0 UNION ALL

Select '8714931000','For bicycles of subheading 8712.00.20','KGM',0,0,0 UNION ALL
Select '8714939000','Other','KGM',5,0,0 UNION ALL

Select '8714941000','For bicycles of subheading 8712.00.20','KGM',0,0,0 UNION ALL
Select '8714949000','Other','KGM',5,0,0 UNION ALL

Select '8714951000','For bicycles of subheading 8712.00.20','UNT',0,0,0 UNION ALL
Select '8714959000','Other','KGM',25,0,0 UNION ALL

Select '8714961000','For bicycles of subheading 8712.00.20','KGM',0,0,0 UNION ALL
Select '8714969000','Other','KGM',5,0,0 UNION ALL


Select '8714991100','Handle bars, pillars, mudguards, reflectors, carriers, control cables, lamp brackets or bracket lugs; other accessories','KGM',0,0,0 UNION ALL
Select '8714991200','Chain wheels and cranks; other parts','KGM',0,0,0 UNION ALL

Select '8714999100','Handle bars, pillars, mudguards, reflectors, carriers, control cables, lamp brackets or bracket lugs; other accessories','KGM',25,0,0 UNION ALL
Select '8714999300','Nipples for spokes','KGM',30,0,0 UNION ALL
Select '8714999400','Chain wheels and cranks; other parts','KGM',5,0,0 UNION ALL

Select '8715000000','Baby carriages and parts thereof. ','KGM',0,0,0 UNION ALL

Select '8716100000','Trailers and semitrailers of the caravan type, for housing or camping','UNT',25,0,0 UNION ALL
Select '8716200000','Selfloading or selfunloading trailers and semitrailers for agricultural purposes','UNT',25,0,0 UNION ALL

Select '8716310000','Tanker trailers and tanker semitrailers','UNT',25,0,0 UNION ALL

Select '8716394000','Agricultural trailers and semitrailers','UNT',25,0,0 UNION ALL

Select '8716399100','Having a carrying capacity (payload) exceeding 200 t','UNT',25,0,0 UNION ALL
Select '8716399900','Other','UNT',25,0,0 UNION ALL
Select '8716400000','Other trailers and semitrailers','UNT',5,0,0 UNION ALL

Select '8716801000','Carts and wagons, sack trucks, hand trolleys and similar handpropelled vehicles of a kind used in factories or workshops, except wheel barrows','UNT',5,0,0 UNION ALL
Select '8716802000','Wheelbarrows','UNT',25,0,0 UNION ALL
Select '8716809000','Other','UNT',5,0,0 UNION ALL


Select '8716901300','For goods of subheading 8716.20','KGM',5,0,0 UNION ALL
Select '8716901900','Other','KGM',5,0,0 UNION ALL

Select '8716902100','Castors wheels, of a diameter (including tyres) exceeding 100mm but not more than 250mm provided the width of the wheel or tyre fitted thereto is more than 30mm','KGM',30,0,0 UNION ALL
Select '8716902200','Other castor wheels','KGM',25,0,0 UNION ALL
Select '8716902300','Other, for goods of subheading 8716.80.10','KGM',5,0,0 UNION ALL
Select '8716902400','Other, for goods of subheading 8716.80.20','KGM',25,0,0 UNION ALL

Select '8716909400','Spokes and nipples','KGM',30,0,0 UNION ALL
Select '8716909500','Castors wheels, for goods of subheading 8716.80.90, of a diameter(including tyres) exceeding 100mm but not more than 250mm provided the width of the wheel or tyre fitted thereto is more than 30mm','KGM',30,0,0 UNION ALL
Select '8716909600','Other castors wheels','KGM',25,0,0 UNION ALL
Select '8716909900','Other','KGM',5,0,0 UNION ALL

Select '8801000000','Balloons and dirigibles; gliders, hang gliders and other nonpowered aircraft.','UNT',0,0,0 UNION ALL


Select '8802110000','Of an unladen weight not exceeding 2,000 kg','UNT',0,0,0 UNION ALL
Select '8802120000','Of an unladen weight exceeding 2,000 kg','UNT',0,0,0 UNION ALL

Select '8802201000','Aeroplanes','UNT',0,0,0 UNION ALL
Select '8802209000','Other','UNT',0,0,0 UNION ALL

Select '8802301000','Aeroplanes','UNT',0,0,0 UNION ALL
Select '8802309000','Other','UNT',0,0,0 UNION ALL

Select '8802401000','Aeroplanes','UNT',0,0,0 UNION ALL
Select '8802409000','Other','UNT',0,0,0 UNION ALL
Select '8802600000','Spacecraft (including satellites) and suborbital and spacecraft  launch vehicles','UNT',0,0,0 UNION ALL

Select '8803100000','Propellers and rotors and parts thereof','KGM',0,0,0 UNION ALL
Select '8803200000','Undercarriages and parts thereof','KGM',0,0,0 UNION ALL
Select '8803300000','Other parts of aeroplanes or helicopters','KGM',0,0,0 UNION ALL

Select '8803901000','Of telecommunications satellites ','KGM',0,0,0 UNION ALL
Select '8803909000','Other','KGM',0,0,0 UNION ALL


Select '8804001000','Rotochutes and parts thereof','KGM',5,0,0 UNION ALL
Select '8804009000','Other','KGM',25,0,0 UNION ALL

Select '8805100000','Aircraft launching gear and parts thereof; deckarrestor or similar  gear and parts thereof','KGM',0,0,0 UNION ALL

Select '8805210000','Air combat simulators and parts thereof','KGM',0,0,0 UNION ALL

Select '8805291000','Ground flying trainers','KGM',0,0,0 UNION ALL
Select '8805299000','Other','KGM',0,0,0 UNION ALL


Select '8901101000','Of  a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL
Select '8901102000','Of  a gross tonnage exceeding 26 but not exceeding 500','UNT',0,0,0 UNION ALL
Select '8901106000','Of a gross tonnage exceeding 500 but not exceeding 1,000','UNT',0,0,0 UNION ALL
Select '8901107000','Of a gross tonnage exceeding 1,000 but not exceeding 4,000','UNT',0,0,0 UNION ALL
Select '8901108000','Of a gross tonnage exceeding 4,000 but not exceeding 5,000','UNT',0,0,0 UNION ALL
Select '8901109000','Of a gross tonnage exceeding 5,000','UNT',0,0,0 UNION ALL

Select '8901205000','Of a gross tonnage not exceeding 5,000','UNT',0,0,0 UNION ALL
Select '8901207000','Of a gross tonnage exceeding 5,000 but not exceeding 50,000','UNT',0,0,0 UNION ALL
Select '8901208000','Of a gross tonnage exceeding 50,000','UNT',0,0,0 UNION ALL

Select '8901305000','Of a gross tonnage not exceeding 5,000','UNT',0,0,0 UNION ALL
Select '8901307000','Of a gross tonnage exceeding 5,000 but not exceeding 50,000','UNT',0,0,0 UNION ALL
Select '8901308000','Of a gross tonnage exceeding 50,000','UNT',0,0,0 UNION ALL


Select '8901901100','Of  a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL
Select '8901901200','Of a  gross tonnage exceeding 26 but not exceeding 500','UNT',0,0,0 UNION ALL
Select '8901901400','Of a  gross tonnage exceeding 500','UNT',0,0,0 UNION ALL

Select '8901903100','Of a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL
Select '8901903200','Of a gross tonnage exceeding 26 but not exceeding 500','UNT',0,0,0 UNION ALL
Select '8901903300','Of a gross tonnage exceeding 500 but not exceeding 1,000','UNT',0,0,0 UNION ALL
Select '8901903400','Of a gross tonnage exceeding 1,000 but not exceeding 4,000','UNT',0,0,0 UNION ALL
Select '8901903500','Of a gross tonnage exceeding 4,000 but not exceeding 5,000','UNT',0,0,0 UNION ALL
Select '8901903600','Of a gross tonnage exceeding 5,000 but not exceeding 50,000','UNT',0,0,0 UNION ALL
Select '8901903700','Of a gross tonnage exceeding 50,000','UNT',0,0,0 UNION ALL



Select '8902003100','Of a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL
Select '8902003200','Of a gross tonnage exceeding 26 but less than 40','UNT',0,0,0 UNION ALL
Select '8902003300','Of a gross tonnage of 40 or more but not exceeding 101','UNT',0,0,0 UNION ALL
Select '8902003400','Of a gross tonnage of 101 but not exceeding 250','UNT',0,0,0 UNION ALL
Select '8902003500','Of a gross tonnage exceeding 250 but not exceeding 1,000','UNT',0,0,0 UNION ALL
Select '8902003600','Of a gross tonnage exceeding 1,000 but not exceeding 4,000','UNT',0,0,0 UNION ALL
Select '8902003700','Of a gross tonnage exceeding 4,000','UNT',0,0,0 UNION ALL

Select '8902004100','Of a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL
Select '8902004200','Of a gross tonnage exceeding 26 but less than 40','UNT',0,0,0 UNION ALL
Select '8902004300','Of a gross tonnage of 40 or more but not exceeding 101','UNT',0,0,0 UNION ALL
Select '8902004400','Of a gross tonnage of 101 but not exceeding 250','UNT',0,0,0 UNION ALL
Select '8902004500','Of a gross tonnage exceeding 250 but not exceeding 1,000','UNT',0,0,0 UNION ALL
Select '8902004600','Of a gross tonnage exceeding 1,000 but not exceeding 4,000','UNT',0,0,0 UNION ALL
Select '8902004700','Of a gross tonnage exceeding 4000','UNT',0,0,0 UNION ALL

Select '8903100000','Inflatable','UNT',0,0,0 UNION ALL

Select '8903910000','Sailboats, with or without auxiliary motor','UNT',0,0,0 UNION ALL
Select '8903920000','Motorboats, other than outboard motorboats ','UNT',0,0,0 UNION ALL
Select '8903990000','Other ','UNT',0,0,0 UNION ALL


Select '8904001000','Of a gross tonnage not exceeding 26','UNT',25,0,0 UNION ALL

Select '8904003100','Of a power not exceeding 4,000 hp','UNT',0,0,0 UNION ALL
Select '8904003900','Other','UNT',0,0,0 UNION ALL

Select '8905100000','Dredgers','UNT',0,0,0 UNION ALL
Select '8905200000','Floating or submersible drilling or production platforms','UNT',0,0,0 UNION ALL

Select '8905901000','Floating docks ','UNT',0,0,0 UNION ALL
Select '8905909000','Other ','UNT',0,0,0 UNION ALL

Select '8906100000','Warships','UNT',0,0,0 UNION ALL

Select '8906901000','Of a displacement not exceeding 30 t','UNT',0,0,0 UNION ALL
Select '8906902000','Of a displacement exceeding 30 t but not exceeding 300 t','UNT',0,0,0 UNION ALL
Select '8906909000','Other','UNT',0,0,0 UNION ALL

Select '8907100000','Inflatable rafts','UNT',0,0,0 UNION ALL

Select '8907901000','Buoys','UNT',0,0,0 UNION ALL
Select '8907909000','Other','UNT',0,0,0 UNION ALL

Select '8908000000','Vessels and other floating structures for breaking up.','UNT',0,0,0 UNION ALL


Select '9001101000','For telecommunications and other electrical uses','KGM',0,0,0 UNION ALL
Select '9001109000','Other','KGM',0,0,0 UNION ALL
Select '9001200000','Sheets and plates of polarising material','KGM',0,0,0 UNION ALL
Select '9001300000','Contact lenses','UNT',0,0,0 UNION ALL
Select '9001400000','Spectacle lenses of glass','UNT',0,0,0 UNION ALL
Select '9001500000','Spectacle lenses of other materials','UNT',0,0,0 UNION ALL

Select '9001901000','For photographic or cinematographic cameras or projectors','KGM',0,0,0 UNION ALL
Select '9001909000','Other','KGM',0,0,0 UNION ALL


Select '9002110000','For  cameras,  projectors  or  photographic  enlargers or reducers','KGM',0,0,0 UNION ALL
Select '9002190000','Other','KGM',0,0,0 UNION ALL

Select '9002201000','For cinematographic projectors','KGM',0,0,0 UNION ALL
Select '9002202000','For cinematographic cameras,  photographic cameras and  other    projectors','KGM',0,0,0 UNION ALL
Select '9002209000','Other','KGM',0,0,0 UNION ALL

Select '9002902000','For cinematographic projectors','KGM',0,0,0 UNION ALL
Select '9002903000','For cinematographic cameras,  photographic cameras and  other  projectors','KGM',0,0,0 UNION ALL
Select '9002909000','Other','KGM',0,0,0 UNION ALL


Select '9003110000','Of plastics','UNT',0,0,0 UNION ALL
Select '9003190000','Of other materials','UNT',0,0,0 UNION ALL
Select '9003900000','Parts','KGM',0,0,0 UNION ALL

Select '9004100000','Sunglasses','UNT',0,0,0 UNION ALL

Select '9004901000','Corrective spectacles','UNT',0,0,0 UNION ALL
Select '90049050','Protective goggles:','',0,0,0 UNION ALL
Select '9004905010','Rubber goggles','UNT',0,0,0 UNION ALL
Select '9004905090','Other','UNT',0,0,0 UNION ALL
Select '90049090','Other:','',0,0,0 UNION ALL
Select '9004909010','Rubber goggles','UNT',0,0,0 UNION ALL
Select '9004909090','Other','UNT',0,0,0 UNION ALL

Select '9005100000','Binoculars','UNT',0,0,0 UNION ALL

Select '9005801000','Astronomical instruments, excluding instruments for radioastronomy','UNT',0,0,0 UNION ALL
Select '9005809000','Other','UNT',0,0,0 UNION ALL

Select '9005901000','For astronomical instruments, excluding instruments for radioastronomy','KGM',0,0,0 UNION ALL
Select '9005909000','Other','KGM',0,0,0 UNION ALL

Select '9006300000','Cameras specially designed for underwater use, for aerial survey or for medical or surgical examination of internal organs; comparison cameras for forensic or criminological purposes','UNT',0,0,0 UNION ALL
Select '9006400000','Instant print cameras','UNT',0,0,0 UNION ALL

Select '9006510000','With a throughthelens viewfinder (single lens reflex (SLR)), for roll film of a width not exceeding 35 mm','UNT',0,0,0 UNION ALL
Select '9006520000','Other, for roll film of a width  less than 35 mm','UNT',0,0,0 UNION ALL
Select '9006530000','Other, for roll film of a width of 35 mm','UNT',0,0,0 UNION ALL


Select '9006592100','Laser photoplotters ','UNT',0,0,0 UNION ALL
Select '9006592900','Other','UNT',0,0,0 UNION ALL
Select '9006593000','Laser photoplotters or image setters with a raster image processor','UNT',0,0,0 UNION ALL
Select '9006599000','Other','UNT',0,0,0 UNION ALL

Select '9006610000','Discharge lamp ("electronic") flashlight apparatus','UNT',0,0,0 UNION ALL
Select '9006690000','Other','UNT',0,0,0 UNION ALL


Select '9006911000','For laser photoplotters of subheading 9006.59.21','KGM',0,0,0 UNION ALL
Select '9006913000','Other, for cameras of subheadings 9006.40 to 9006.53','KGM',0,0,0 UNION ALL
Select '9006919000','Other','KGM',0,0,0 UNION ALL

Select '9006991000','For photographic flashlight apparatus','KGM',0,0,0 UNION ALL
Select '9006999000','Other','KGM',0,0,0 UNION ALL

Select '9007100000','Cameras','UNT',0,0,0 UNION ALL

Select '9007201000','For film of less than 16 mm in width','UNT',0,0,0 UNION ALL
Select '9007209000','Other','UNT',0,0,0 UNION ALL

Select '9007910000','For cameras','KGM',0,0,0 UNION ALL
Select '9007920000','For projectors','KGM',0,0,0 UNION ALL


Select '9008501000','Microfilm, microfiche or other microform readers, whether or not capable of producing copies','UNT',0,0,0 UNION ALL
Select '9008509000','Other','UNT',0,0,0 UNION ALL

Select '9008902000','Of photographic (other than cinematographic) enlargers and reducers','KGM',0,0,0 UNION ALL
Select '9008909000','Other','KGM',5,0,0 UNION ALL

Select '9010100000','Apparatus  and  equipment  for  automatically  developing  photographic (including cinematographic) film or paper in rolls or   for automatically exposing developed film to rolls of photographic paper','UNT',0,0,0 UNION ALL

Select '9010501000','Apparatus for the projection or drawing of circuit patterns on sensitised substrates for the manufacture of printed circuit boards/printed wiring boards','UNT',17,0,0 UNION ALL
Select '9010502000','Cinematographic editing and titling equipment; negatoscopes for Xray; contact exposure unit used for the preparation of film in colour separation process; automatic plate maker','UNT',0,0,0 UNION ALL
Select '9010509000','Other','UNT',17,0,0 UNION ALL

Select '9010601000','Of 300 inches or more','UNT',0,0,0 UNION ALL
Select '9010609000','Other','UNT',0,0,0 UNION ALL

Select '9010901000','Of goods of subheading 9010.10 or 9010.60','KGM',0,0,0 UNION ALL
Select '9010903000','Parts and accessories of apparatus for the projection or drawing of circuit patterns on sensitised substrates for the manufacture of printed circuit boards/printed wiring boards','KGM',0,0,0 UNION ALL
Select '9010909000','Other','KGM',0,0,0 UNION ALL

Select '9011100000','Stereoscopic microscopes','UNT',0,0,0 UNION ALL
Select '9011200000','Other microscopes, for photomicrography, cinephotomicrography  or microprojection','UNT',0,0,0 UNION ALL
Select '9011800000','Other microscopes','UNT',0,0,0 UNION ALL
Select '9011900000','Parts and accessories','KGM',0,0,0 UNION ALL

Select '9012100000','Microscopes other than optical microscopes; diffraction apparatus ','UNT',0,0,0 UNION ALL
Select '9012900000','Parts and accessories','KGM',0,0,0 UNION ALL

Select '9013100000','Telescopic sights for fitting to arms; periscopes; telescopes designed to form parts of machines, appliances, instruments or apparatus of this Chapter or Section XVI','UNT',0,0,0 UNION ALL
Select '9013200000','Lasers, other than laser diodes','UNT',0,0,0 UNION ALL

Select '9013801000','Optical error verification and repair apparatus for printed circuit boards/printed wiring boards and printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '9013802000','Liquid crystal devices','UNT',0,0,0 UNION ALL
Select '9013809000','Other','UNT',0,0,0 UNION ALL

Select '9013901000','Of goods of subheading 9013.20','KGM',0,0,0 UNION ALL
Select '9013905000','Of goods of subheading 9013.80.20','KGM',0,0,0 UNION ALL
Select '9013906000','Of goods of subheading 9013.80.10','KGM',0,0,0 UNION ALL
Select '9013909000','Other','KGM',0,0,0 UNION ALL

Select '9014100000','Direction finding compasses','UNT',0,0,0 UNION ALL
Select '9014200000','Instruments and appliances for aeronautical or space navigation (other than compasses)','UNT',0,0,0 UNION ALL


Select '9014801100','Sonar or echo sounder','UNT',0,0,0 UNION ALL
Select '9014801900','Other','UNT',0,0,0 UNION ALL
Select '9014809000','Other','UNT',0,0,0 UNION ALL

Select '9014901000','Of instruments and apparatus, of a kind used on ships, working in conjunction with an automatic data processing machine','KGM',0,0,0 UNION ALL
Select '9014909000','Other','KGM',0,0,0 UNION ALL


Select '9015101000','Of a kind used in photography or cinematography','UNT',0,0,0 UNION ALL
Select '9015109000','Other','UNT',5,0,0 UNION ALL
Select '9015200000','Theodolites and tachymeters (tacheometers)','UNT',5,0,0 UNION ALL
Select '9015300000','Levels','UNT',5,0,0 UNION ALL
Select '9015400000','Photogrammetrical surveying instruments and appliances','KGM',5,0,0 UNION ALL

Select '9015801000','Radiosonde and radio wind apparatus','UNT',5,0,0 UNION ALL
Select '9015809000','Other','UNT',5,0,0 UNION ALL
Select '9015900000','Parts and accessories','KGM',0,0,0 UNION ALL

Select '9016000000','Balances of a sensitivity of 5 cg or better, with or without weights.','KGM',0,0,0 UNION ALL


Select '9017101000','Plotters ','UNT',5,0,0 UNION ALL
Select '9017109000','Other','UNT',5,0,0 UNION ALL

Select '9017201000','Rulers','UNT',0,0,0 UNION ALL
Select '9017203000','Apparatus for the projection or drawing of circuit patterns on sensitised substrates for the manufacture of printed circuit boards/printed wiring boards','UNT',0,0,0 UNION ALL
Select '9017204000','Photoplotters for the manufacture of printed circuit boards/printed wiring boards','UNT',0,0,0 UNION ALL
Select '9017205000','Other plotters','UNT',0,0,0 UNION ALL
Select '9017209000','Other','UNT',0,0,0 UNION ALL
Select '9017300000','Micrometers, callipers and gauges','UNT',0,0,0 UNION ALL
Select '9017800000','Other instruments','UNT',5,0,0 UNION ALL

Select '9017902000','Parts and accessories of apparatus for the projection or drawing of circuit patterns on sensitised substrates for the manufacture of printed circuit boards/printed wiring boards','KGM',0,0,0 UNION ALL
Select '9017903000','Parts and accessories of photoplotters for the manufacture of printed circuit boards/printed wiring boards','KGM',0,0,0 UNION ALL
Select '9017904000','Parts and accessories, including printed circuit assemblies, of  other plotters ','KGM',0,0,0 UNION ALL
Select '9017909000','Other','KGM',0,0,0 UNION ALL


Select '9018110000','Electrocardiographs','UNT',0,0,0 UNION ALL
Select '9018120000','Ultrasonic scanning apparatus','UNT',0,0,0 UNION ALL
Select '9018130000','Magnetic resonance imaging apparatus','UNT',0,0,0 UNION ALL
Select '9018140000','Scintigraphic apparatus','UNT',0,0,0 UNION ALL
Select '9018190000','Other','UNT',0,0,0 UNION ALL
Select '9018200000','Ultraviolet or infrared ray apparatus','KGM',0,0,0 UNION ALL


Select '9018311000','Disposable syringes','UNT',0,0,0 UNION ALL
Select '9018319000','Other','UNT',0,0,0 UNION ALL
Select '9018320000','Tubular metal needles and needles for sutures','KGM',0,0,0 UNION ALL

Select '9018391000','Catheters','UNT',0,0,0 UNION ALL
Select '9018399000','Other','UNT',0,0,0 UNION ALL

Select '9018410000','Dental drill engines, whether or not combined on a single base with other dental equipment','KGM',0,0,0 UNION ALL
Select '9018490000','Other','UNT',0,0,0 UNION ALL
Select '9018500000','Other ophthalmic instruments and appliances','KGM',0,0,0 UNION ALL

Select '9018902000','Intravenous administration sets ','UNT',0,0,0 UNION ALL
Select '9018903000','Electronic instruments and appliances','UNT',0,0,0 UNION ALL
Select '9018909000','Other','UNT',0,0,0 UNION ALL


Select '9019101000','Electrically operated','KGM',0,0,0 UNION ALL
Select '9019109000','Other','KGM',0,0,0 UNION ALL
Select '9019200000','Ozone therapy, oxygen therapy, aerosol therapy, artificial respiration or other therapeutic respiration apparatus','KGM',0,0,0 UNION ALL

Select '9020000000','Other breathing appliances and gas masks, excluding protective masks having neither mechanical parts nor replaceable filters. ','KGM',0,0,0 UNION ALL

Select '9021100000','Orthopaedic or fracture appliances','KGM',0,0,0 UNION ALL

Select '9021210000','Artificial teeth','KGM',0,0,0 UNION ALL
Select '9021290000','Other','KGM',0,0,0 UNION ALL

Select '9021310000','Artificial joints','KGM',0,0,0 UNION ALL
Select '9021390000','Other','KGM',0,0,0 UNION ALL
Select '9021400000','Hearing aids, excluding parts and accessories','UNT',0,0,0 UNION ALL
Select '9021500000','Pacemakers for stimulating heart muscles, excluding parts and accessories','UNT',0,0,0 UNION ALL
Select '9021900000','Other','KGM',0,0,0 UNION ALL


Select '9022120000','Computed tomography apparatus','UNT',0,0,0 UNION ALL
Select '9022130000','Other, for dental uses','UNT',0,0,0 UNION ALL
Select '9022140000','Other, for medical, surgical or veterinary uses','UNT',0,0,0 UNION ALL

Select '9022191000','Xray apparatus for the physical inspection of solder joints on printed circuit board/printed wiring board assemblies ','UNT',0,0,0 UNION ALL
Select '9022199000','Other','UNT',0,0,0 UNION ALL

Select '9022210000','For medical, surgical, dental or veterinary uses','UNT',0,0,0 UNION ALL
Select '9022290000','For other uses','UNT',0,0,0 UNION ALL
Select '9022300000','Xray tubes','UNT',0,0,0 UNION ALL

Select '9022901000','Parts and accessories of Xray apparatus for the physical inspection of solder joints on printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '9022909000','Other','KGM',0,0,0 UNION ALL

Select '9023000000','Instruments, apparatus and models, designed for demonstrational purposes (for example, in education or exhibitions), unsuitable for other uses.','KGM',0,0,0 UNION ALL


Select '9024101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9024102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9024801000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9024802000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9024901000','For electrically operated machines and appliances','KGM',0,0,0 UNION ALL
Select '9024902000','For nonelectrically operated machines and appliances','KGM',0,0,0 UNION ALL


Select '9025110000','Liquidfilled, for direct reading','UNT',0,0,0 UNION ALL


Select '9025191100','Temperature gauges for motor vehicles','UNT',0,0,0 UNION ALL
Select '9025191900','Other','UNT',0,0,0 UNION ALL
Select '9025192000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9025802000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9025803000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9025901000','For electrically operated instruments','KGM',0,0,0 UNION ALL
Select '9025902000','For nonelectrically operated instruments','KGM',0,0,0 UNION ALL


Select '9026101000','Level gauges for motor vehicles, electrically operated','UNT',0,0,0 UNION ALL
Select '9026102000','Level gauges for motor vehicles, not electrically operated','UNT',0,0,0 UNION ALL
Select '9026103000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '9026109000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '9026201000','Pressure gauges for motor vehicles, electrically operated','UNT',0,0,0 UNION ALL
Select '9026202000','Pressure gauges for motor vehicles, not electrically operated','UNT',0,0,0 UNION ALL
Select '9026203000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '9026204000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '9026801000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9026802000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9026901000','For electrically operated instruments and apparatus','KGM',0,0,0 UNION ALL
Select '9026902000','For nonelectrically operated instruments and apparatus','KGM',0,0,0 UNION ALL


Select '9027101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9027102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9027201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9027202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9027301000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9027302000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9027501000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9027502000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9027801000','Exposure meters','UNT',0,0,0 UNION ALL
Select '9027803000','Other, electrically operated','UNT',0,0,0 UNION ALL
Select '9027804000','Other, not electrically operated','UNT',0,0,0 UNION ALL

Select '9027901000','Parts and accessories, including printed circuit assemblies for products of heading 90.27, other than for gas or smoke analysis apparatus or microtomes ','KGM',0,0,0 UNION ALL

Select '9027909100','Electrically operated','KGM',0,0,0 UNION ALL
Select '9027909900','Other','KGM',0,0,0 UNION ALL


Select '9028101000','Gas meters of a kind mounted on gas containers','UNT',0,0,0 UNION ALL
Select '9028109000','Other','UNT',0,0,0 UNION ALL

Select '9028202000','Water meters ','UNT',25,0,0 UNION ALL
Select '9028209000','Other','UNT',0,0,0 UNION ALL

Select '9028301000','Kilowatthour meters','UNT',0,0,0 UNION ALL
Select '9028309000','Other','UNT',0,0,0 UNION ALL

Select '9028901000','Water meter housings or bodies','KGM',21,0,0 UNION ALL
Select '9028909000','Other','KGM',0,0,0 UNION ALL


Select '9029102000','Taximeters','UNT',0,0,0 UNION ALL
Select '9029109000','Other','UNT',0,0,0 UNION ALL

Select '9029201000','Speedometers for motor vehicles','UNT',0,0,0 UNION ALL
Select '9029202000','Tachometers for motor vehicles','UNT',0,0,0 UNION ALL
Select '9029209000','Other','UNT',0,0,0 UNION ALL

Select '9029901000','Of goods of subheading 9029.10; of stroboscopes of  subheading 9029.20 ','KGM',0,0,0 UNION ALL
Select '9029902000','Of other goods of subheading 9029.20','KGM',0,0,0 UNION ALL

Select '9030100000','Instruments and apparatus for measuring or detecting ionising radiations','UNT',0,0,0 UNION ALL
Select '9030200000','Oscilloscopes and oscillographs','UNT',0,0,0 UNION ALL

Select '9030310000','Multimeters without a recording device','UNT',0,0,0 UNION ALL
Select '9030320000','Multimeters with a recording device','UNT',0,0,0 UNION ALL

Select '9030331000','Instruments and apparatus for measuring or checking voltage, current, resistance or power on printed circuit boards/printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '9030332000','Impedancemeasuring instruments and apparatus designed to provide visual and/or audible warning of electrostatic discharge conditions that can damage electronic circuits; apparatus for testing electrostatic control equipment and electrostatic grounding devices/fixtures ','UNT',0,0,0 UNION ALL
Select '9030333000','Ammeters and voltmeters for motor vehicles','UNT',0,0,0 UNION ALL
Select '9030339000','Other','UNT',0,0,0 UNION ALL
Select '9030390000','Other, with a recording device','UNT',0,0,0 UNION ALL
Select '9030400000','Other instruments and apparatus, specially designed for telecommunications (for example, crosstalk meters, gain  measuring instruments, distortion factor meters, psophometers)','UNT',0,0,0 UNION ALL


Select '9030821000','Wafer probers','UNT',0,0,0 UNION ALL
Select '9030829000','Other','UNT',0,0,0 UNION ALL

Select '9030841000','Instruments and apparatus for measuring or checking electrical quantities on printed circuit boards/printed wiring boards and printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '9030849000','Other','UNT',0,0,0 UNION ALL

Select '9030891000','Instruments and apparatus, without a recording device, for measuring or checking electrical quantities on printed circuit boards/printed wiring boards and printed circuit assemblies, other than those covered within subheading 9030.39 ','UNT',0,0,0 UNION ALL
Select '9030899000','Other','UNT',0,0,0 UNION ALL

Select '9030901000','Parts and accessories (including printed circuit assemblies) of goods of subheading 9030.40 or 9030.82','KGM',0,0,0 UNION ALL
Select '9030903000','Parts and accessories of optical instruments and appliances for measuring or checking printed circuit boards/printed wiring boards and printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '9030904000','Parts and accessories of other instruments and apparatus for measuring or checking electrical quantities on printed circuit boards/printed wiring boards and printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '9030909000','Other','KGM',0,0,0 UNION ALL


Select '9031101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9031102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9031201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9031202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9031410000','For inspecting semiconductor wafers or devices or for  inspecting photomasks or reticles used in manufacturing  semiconductor devices','UNT',0,0,0 UNION ALL

Select '9031491000','Optical instruments and appliances for measuring surface particulate contamination on semiconductor wafers','UNT',0,0,0 UNION ALL
Select '9031492000','Optical error verification and repair apparatus for printed circuit boards/printed wiring boards and printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '9031493000','Other optical instruments and appliances for measuring or checking printed circuit boards/printed wiring boards and printed circuit assemblies','UNT',0,0,0 UNION ALL
Select '9031499000','Other','UNT',0,0,0 UNION ALL

Select '9031801000','Cable testers','UNT',0,0,0 UNION ALL
Select '9031809000','Other','UNT',0,0,0 UNION ALL


Select '9031901100','Parts and accessories including printed circuit assemblies of optical instruments and appliances for inspecting semiconductor wafers or devices or for inspecting masks, photomasks or reticles used in manufacturing semiconductor devices; parts and accessories of optical instruments and appliances for measuring surface particulate contamination on semiconductor wafers ','KGM',0,0,0 UNION ALL
Select '9031901200','Of optical error verification and repair apparatus for printed circuit boards/printed wiring boards and printed circuit assemblies','KGM',0,0,0 UNION ALL
Select '9031901300','Of other optical instruments and appliances for measuring or checking printed circuit boards/printed wiring boards and printed circuit assemblies ','KGM',0,0,0 UNION ALL
Select '9031901900','Other','KGM',0,0,0 UNION ALL
Select '9031902000','For nonelectrically operated equipment','KGM',0,0,0 UNION ALL


Select '9032101000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9032102000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9032201000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9032202000','Not electrically operated','UNT',0,0,0 UNION ALL

Select '9032810000','Hydraulic or pneumatic','UNT',0,0,0 UNION ALL

Select '9032891000','Instruments and apparatus incorporating or working in conjunction with an automatic data processing machine, for automatically regulating or controlling the propulsion, ballast or cargo handling systems of ships ','UNT',0,0,0 UNION ALL
Select '9032892000','Automatic instruments and apparatus for regulating or controlling chemical or electrochemical solutions in the manufacture of printed circuit boards/printed wiring boards or printed circuit assemblies','UNT',0,0,0 UNION ALL

Select '9032893100','Automatic regulating voltage units (stabilisers)','UNT',0,0,0 UNION ALL
Select '9032893900','Other','UNT',0,0,0 UNION ALL
Select '9032899000','Other','UNT',0,0,0 UNION ALL

Select '9032901000','Of goods of subheading 9032.89.10','KGM',0,0,0 UNION ALL
Select '9032902000','Of goods of subheading 9032.89.20','KGM',0,0,0 UNION ALL
Select '9032903000','Of other electrically operated goods','KGM',0,0,0 UNION ALL
Select '9032909000','Other','KGM',0,0,0 UNION ALL


Select '9033001000','For electrically operated equipment','KGM',0,0,0 UNION ALL
Select '9033002000','For nonelectrically operated equipment','KGM',0,0,0 UNION ALL


Select '9101110000','With mechanical display only','UNT',0,0,0 UNION ALL
Select '9101190000','Other','UNT',0,0,0 UNION ALL

Select '9101210000','With automatic winding','UNT',0,0,0 UNION ALL
Select '9101290000','Other','UNT',0,0,0 UNION ALL

Select '9101910000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9101990000','Other','UNT',0,0,0 UNION ALL


Select '9102110000','With mechanical display only ','UNT',0,0,0 UNION ALL
Select '9102120000','With optoelectronic display only','UNT',0,0,0 UNION ALL
Select '9102190000','Other','UNT',0,0,0 UNION ALL

Select '9102210000','With automatic winding','UNT',0,0,0 UNION ALL
Select '9102290000','Other ','UNT',0,0,0 UNION ALL

Select '9102910000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9102990000','Other','UNT',0,0,0 UNION ALL

Select '9103100000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9103900000','Other','UNT',0,0,0 UNION ALL


Select '9104001000','For vehicles ','UNT',0,0,0 UNION ALL
Select '9104009000','Other','UNT',0,0,0 UNION ALL


Select '9105110000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9105190000','Other','UNT',0,0,0 UNION ALL

Select '9105210000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9105290000','Other','UNT',0,0,0 UNION ALL


Select '9105911000','Marine chronometers ','UNT',0,0,0 UNION ALL
Select '9105919000','Other','UNT',0,0,0 UNION ALL

Select '9105991000','Marine chronometers ','UNT',0,0,0 UNION ALL
Select '9105999000','Other','UNT',0,0,0 UNION ALL

Select '9106100000','Timeregisters; timerecorders','UNT',20,0,0 UNION ALL

Select '9106901000','Parking meters   ','UNT',5,0,0 UNION ALL
Select '9106909000','Other','UNT',35,0,0 UNION ALL

Select '9107000000','Time switches with clock or watch movement or with synchronous motor.','UNT',0,0,0 UNION ALL


Select '9108110000','With mechanical display only or with a device  to which a mechanical display can be incorporated','UNT',0,0,0 UNION ALL
Select '9108120000','With optoelectronic display only','UNT',0,0,0 UNION ALL
Select '9108190000','Other','UNT',0,0,0 UNION ALL
Select '9108200000','With automatic winding','UNT',0,0,0 UNION ALL
Select '9108900000','Other','UNT',0,0,0 UNION ALL

Select '9109100000','Electrically operated','UNT',0,0,0 UNION ALL
Select '9109900000','Other','UNT',0,0,0 UNION ALL


Select '9110110000','Complete movements, unassembled or partly assembled   (movement sets)','UNT',0,0,0 UNION ALL
Select '9110120000','Incomplete movements, assembled','KGM',0,0,0 UNION ALL
Select '9110190000','Rough movements','KGM',0,0,0 UNION ALL
Select '9110900000','Other','KGM',0,0,0 UNION ALL

Select '9111100000','Cases of precious metal or of metal clad with precious metal','UNT',0,0,0 UNION ALL
Select '9111200000','Cases of base metal, whether or not gold or silverplated','UNT',0,0,0 UNION ALL
Select '9111800000','Other cases','UNT',0,0,0 UNION ALL
Select '9111900000','Parts','KGM',0,0,0 UNION ALL

Select '9112200000','Cases ','UNT',0,0,0 UNION ALL
Select '9112900000','Parts','KGM',0,0,0 UNION ALL

Select '9113100000','Of precious metal or of metal clad with precious metal ','KGM',0,0,0 UNION ALL
Select '9113200000','Of base metal, whether or not gold or silverplated','KGM',0,0,0 UNION ALL
Select '9113900000','Other','KGM',0,0,0 UNION ALL

Select '9114100000','Springs, including hairsprings','KGM',0,0,0 UNION ALL
Select '9114300000','Dials','KGM',0,0,0 UNION ALL
Select '9114400000','Plates and bridges','KGM',0,0,0 UNION ALL
Select '9114900000','Other','KGM',0,0,0 UNION ALL

Select '9201100000','Upright pianos','UNT',15,0,0 UNION ALL
Select '9201200000','Grand pianos','UNT',0,0,0 UNION ALL
Select '9201900000','Other','UNT',0,0,0 UNION ALL

Select '9202100000','Played with a bow','UNT',0,0,0 UNION ALL
Select '9202900000','Other','UNT',0,0,0 UNION ALL

Select '9205100000','Brasswind instruments','UNT',0,0,0 UNION ALL

Select '9205901000','Keyboard pipe organs; harmoniums and similar keyboard  instruments with free metal reeds ','UNT',0,0,0 UNION ALL
Select '9205909000','Other','UNT',0,0,0 UNION ALL

Select '9206000000','Percussion musical instruments (for example, drums, xylophones, cymbals, castanets, maracas).','UNT',0,0,0 UNION ALL

Select '9207100000','Keyboard instruments, other than accordions','UNT',0,0,0 UNION ALL
Select '9207900000','Other','UNT',0,0,0 UNION ALL

Select '9208100000','Musical boxes','UNT',0,0,0 UNION ALL

Select '9208901000','Decoy calls, whistles, call horns and other mouthblown sound signalling instruments','UNT',0,0,0 UNION ALL
Select '9208909000','Other','UNT',0,0,0 UNION ALL

Select '9209300000','Musical instrument strings','KGM',0,0,0 UNION ALL


Select '9209911000','Strung backs, keyboards and metal frames for upright pianos','KGM',0,0,0 UNION ALL
Select '9209919000','Other','KGM',0,0,0 UNION ALL
Select '9209920000','Parts and accessories for the musical instruments of heading 92.02','KGM',0,0,0 UNION ALL
Select '9209940000','Parts and accessories for the musical instruments of heading 92.07','KGM',0,0,0 UNION ALL
Select '9209990000','Other','KGM',0,0,0 UNION ALL

Select '9301100000','Artillery weapons (for example, guns, howitzers and mortars)','UNT',5,0,0 UNION ALL
Select '9301200000','Rocket launchers; flamethrowers; grenade launchers;   torpedo  tubes and similar projectors','UNT',5,0,0 UNION ALL
Select '9301900000','Other','UNT',5,0,0 UNION ALL

Select '9302000000','Revolvers and pistols, other than those of heading 93.03 or 93.04.','UNT',30,0,0 UNION ALL

Select '9303100000','Muzzleloading firearms','UNT',30,0,0 UNION ALL

Select '9303201000','Hunting shotguns','UNT',30,0,0 UNION ALL
Select '9303209000','Other','UNT',30,0,0 UNION ALL

Select '9303301000','Hunting rifles','UNT',30,0,0 UNION ALL
Select '9303309000','Other','UNT',30,0,0 UNION ALL
Select '9303900000','Other','UNT',30,0,0 UNION ALL


Select '9304001000','Air guns, operating at a pressure of less than 7 kgf/cm2','UNT',30,0,0 UNION ALL
Select '9304009000','Other','UNT',30,0,0 UNION ALL

Select '9305100000','Of revolvers or pistols','KGM',5,0,0 UNION ALL
Select '9305200000','Of shotguns or rifles of heading 93.03','KGM',5,0,0 UNION ALL


Select '9305911000','Of leather or textile material','KGM',30,0,0 UNION ALL
Select '9305919000','Other','KGM',5,0,0 UNION ALL


Select '9305991100','Of leather or textile material','KGM',30,0,0 UNION ALL
Select '9305991900','Other','KGM',5,0,0 UNION ALL

Select '9305999100','Of leather or textile material','KGM',30,0,0 UNION ALL
Select '9305999900','Other','KGM',5,0,0 UNION ALL


Select '9306210000','Cartridges','KGM',5,0,0 UNION ALL
Select '9306290000','Other','KGM',5,0,0 UNION ALL


Select '9306301100','.22 calibre cartridges','KGM',5,0,0 UNION ALL
Select '9306301900','Other','KGM',0,0,0 UNION ALL
Select '9306302000','Cartridges for riveting or similar tools or for captivebolt humane killers; parts thereof','KGM',0,0,0 UNION ALL
Select '9306303000','For sporting, hunting or targetshooting guns, rifles and carbines, other than shotguns','KGM',5,0,0 UNION ALL

Select '9306309100','.22 calibre cartridges','KGM',5,0,0 UNION ALL
Select '9306309900','Other','KGM',0,0,0 UNION ALL
Select '9306900000','Other','KGM',0,0,0 UNION ALL

Select '9307000000','Swords, cutlasses, bayonets, lances and similar arms and parts thereof and scabbards and sheaths therefor.','KGM',5,0,0 UNION ALL

Select '9401100000','Seats of a kind used for aircraft','UNT',0,0,0 UNION ALL

Select '9401201000','For vehicles of heading 87.02, 87.03 or 87.04','UNT',30,0,0 UNION ALL
Select '9401209000','Other','UNT',30,0,0 UNION ALL
Select '9401300000','Swivel seats with variable height adjustment','UNT',0,0,0 UNION ALL
Select '9401400000','Seats other than garden seats or camping equipment, convertible into beds','UNT',0,0,0 UNION ALL

Select '9401520000','Of bamboo ','UNT',0,0,0 UNION ALL
Select '9401530000','Of rattan','UNT',0,0,0 UNION ALL
Select '9401590000','Other','UNT',0,0,0 UNION ALL

Select '9401610000','Upholstered','UNT',0,0,0 UNION ALL

Select '9401691000','With backrest and / or the seat made of rattan','UNT',0,0,0 UNION ALL
Select '9401699000','Other','UNT',0,0,0 UNION ALL

Select '9401710000','Upholstered','UNT',0,0,0 UNION ALL

Select '9401791000','With backrest and / or the seat made of rattan','UNT',0,0,0 UNION ALL
Select '9401799000','Other','UNT',0,0,0 UNION ALL
Select '9401800000','Other seats','UNT',0,0,0 UNION ALL

Select '9401901000','Of seats of subheading 9401.10.00','KGM',0,0,0 UNION ALL

Select '9401903100','Headrest stiffeners for seats of subheading 9401.20.10','KGM',20,0,0 UNION ALL
Select '9401903900','Other','KGM',20,0,0 UNION ALL
Select '9401904000','Of seats of subheading 9401.30.00','KGM',0,0,0 UNION ALL

Select '9401909200','Of plastics','KGM',0,0,0 UNION ALL
Select '9401909900','Other','KGM',0,0,0 UNION ALL


Select '9402101000','Dentists'' chairs and parts thereof','KGM',0,0,0 UNION ALL
Select '9402103000','Barbers"chairs and parts thereof','KGM',0,0,0 UNION ALL

Select '9402901000','Furniture specially designed for medical, surgical or veterinary purposes and parts thereof','KGM',0,0,0 UNION ALL
Select '9402902000','Commodes','KGM',0,0,0 UNION ALL
Select '9402909000','Other','KGM',0,0,0 UNION ALL

Select '9403100000','Metal furniture of a kind used in offices','KGM',0,0,0 UNION ALL

Select '9403201000','Fume cupboards','KGM',0,0,0 UNION ALL
Select '9403209000','Other','KGM',0,0,0 UNION ALL
Select '9403300000','Wooden furniture of a kind used in offices','UNT',0,0,0 UNION ALL
Select '9403400000','Wooden furniture of a kind used in the kitchen','UNT',0,0,0 UNION ALL
Select '9403500000','Wooden furniture of a kind used in the bedroom','UNT',0,0,0 UNION ALL

Select '9403601000','Fume cupboards','UNT',0,0,0 UNION ALL
Select '9403609000','Other','UNT',0,0,0 UNION ALL

Select '9403701000','Baby walkers','KGM',0,0,0 UNION ALL
Select '9403702000','Fume cupboards','KGM',0,0,0 UNION ALL
Select '9403709000','Other','KGM',0,0,0 UNION ALL

Select '9403820000','Of bamboo ','KGM',0,0,0 UNION ALL
Select '9403830000','Of rattan','KGM',0,0,0 UNION ALL

Select '9403891000','Fume cupboards','KGM',0,0,0 UNION ALL
Select '9403899000','Other','KGM',0,0,0 UNION ALL

Select '9403901000','Of baby walkers of subheading 9403.70.10 ','KGM',0,0,0 UNION ALL
Select '9403909000','Other','KGM',0,0,0 UNION ALL

Select '9404100000','Mattress supports','KGM',20,0,0 UNION ALL


Select '9404211000','Of cellular rubber, whether or not covered','UNT',20,0,0 UNION ALL
Select '9404212000','Of cellular plastics, whether or not covered','UNT',20,0,0 UNION ALL

Select '9404291000','Spring mattresses','UNT',20,0,0 UNION ALL
Select '9404292000','Other, hyperthermia / hypothermia type','UNT',0,0,0 UNION ALL
Select '9404299000','Other','UNT',20,0,0 UNION ALL
Select '9404300000','Sleeping bags','UNT',20,0,0 UNION ALL

Select '9404901000','Quilts, bedspreads and mattressprotectors','KGM',20,0,0 UNION ALL
Select '9404909000','Other','KGM',20,0,0 UNION ALL


Select '9405102000','Lamps for operating rooms','KGM',20,0,0 UNION ALL

Select '9405109100','Spotlights','KGM',20,0,0 UNION ALL
Select '9405109200','Fluorescent lamps and lighting fittings','KGM',25,0,0 UNION ALL
Select '9405109900','Other','KGM',20,0,0 UNION ALL

Select '9405201000','Lamps for operating rooms','KGM',20,0,0 UNION ALL
Select '9405209000','Other','KGM',20,0,0 UNION ALL
Select '9405300000','Lighting sets of a kind used for Christmas trees','KGM',5,0,0 UNION ALL

Select '9405402000','Searchlights ','KGM',5,0,0 UNION ALL
Select '9405404000','Other spotlights','KGM',5,0,0 UNION ALL
Select '9405405000','Other, of a kind used for lighting public open spaces or thoroughfares','KGM',25,0,0 UNION ALL
Select '9405406000','Other exterior lighting','KGM',5,0,0 UNION ALL
Select '9405407000','Nonflashing aerodrome beacons; lamps for railway rolling stock, locomotives, aircraft, ships, or lighthouses, of base metal','KGM',20,0,0 UNION ALL
Select '9405408000','Pilot lamps with fittings for electrothermic domestic appliances of heading 85.16','KGM',20,0,0 UNION ALL

Select '9405409100','Fibreoptic headband lamps of a kind designed for medical use','KGM',0,0,0 UNION ALL
Select '9405409900','Other','KGM',20,0,0 UNION ALL


Select '9405501100','Of brass of a kind used for religious rites','KGM',0,0,0 UNION ALL
Select '9405501900','Other','KGM',15,0,0 UNION ALL
Select '9405504000','Hurricane lamps','KGM',5,0,0 UNION ALL
Select '9405505000','Miners'' lamps or quarrymen''s lamps','KGM',5,0,0 UNION ALL
Select '9405509000','Other ','KGM',5,0,0 UNION ALL

Select '9405601000','Warning signs, street name signs, road and traffic signs','KGM',20,0,0 UNION ALL
Select '9405609000','Other','KGM',20,0,0 UNION ALL


Select '9405911000','For lamps for operating rooms','KGM',20,0,0 UNION ALL
Select '9405912000','For spotlights','KGM',5,0,0 UNION ALL
Select '9405914000','Globes or chimneys','KGM',20,0,0 UNION ALL
Select '9405915000','For searchlights','KGM',5,0,0 UNION ALL
Select '9405919000','Other','KGM',20,0,0 UNION ALL

Select '9405921000','For lamps for operating rooms','KGM',20,0,0 UNION ALL
Select '9405922000','For spotlights','KGM',5,0,0 UNION ALL
Select '9405923000','For searchlights','KGM',5,0,0 UNION ALL
Select '9405929000','Other','KGM',20,0,0 UNION ALL

Select '9405991000','Lampshades of textile material','KGM',20,0,0 UNION ALL
Select '9405992000','Lampshades of other material','KGM',20,0,0 UNION ALL
Select '9405993000','Of lamps of subheading 9405.50.11 or 9405.50.19','KGM',15,0,0 UNION ALL
Select '9405994000','For searchlights or spotlights','KGM',5,0,0 UNION ALL
Select '9405995000','Other, of ceramic; other, of metal ','KGM',20,0,0 UNION ALL
Select '9405999000','Other','KGM',5,0,0 UNION ALL



Select '9406101000','Greenhouses fitted with mechanical or thermal equipment ','KGM',5,0,0 UNION ALL
Select '9406109000','Other','KGM',25,0,0 UNION ALL


Select '9406901100','Of iron or of steel','KGM',5,0,0 UNION ALL
Select '94069019','Other:','',0,0,0 UNION ALL
Select '9406901910','Of plastics','KGM',30,0,0 UNION ALL
Select '9406901990','Other','KGM',5,0,0 UNION ALL
Select '9406902000','Other, of plastics or of aluminium','KGM',30,0,0 UNION ALL
Select '9406903000','Other, of iron or of steel','KGM',20,0,0 UNION ALL
Select '9406904000','Other, of cement, of concrete, or of artificial stone','KGM',10,0,0 UNION ALL
Select '9406909000','Other','KGM',5,0,0 UNION ALL


Select '9503001000','Tricycles, scooters, pedal cars and similar wheeled toys; dolls'' carriages','KGM',0,0,0 UNION ALL

Select '9503002100','Dolls, whether or not dressed','KGM',0,0,0 UNION ALL

Select '9503002200','Garments and garment accessories; footwear and headgear','KGM',0,0,0 UNION ALL
Select '9503002900','Other','KGM',0,0,0 UNION ALL
Select '9503003000','Electric trains, including tracks, signals and other accessories  therefor','UNT',0,0,0 UNION ALL
Select '9503004000','Reduced size ("scale") models and similar recreational models, working or not','UNT',0,0,0 UNION ALL
Select '9503005000','Other construction sets and constructional toys, of materials other than plastics','UNT',0,0,0 UNION ALL
Select '9503006000','Stuffed toys representing animals or nonhuman creatures','UNT',0,0,0 UNION ALL
Select '9503007000','Puzzles of all kinds','UNT',0,0,0 UNION ALL

Select '9503009100','Numerical, alphabetical or animal blocks or cutouts; word builder sets; word making and talking sets; toy printing sets; toy  counting frames (abaci);  toy sewing machines; toy typewriters ','UNT',0,0,0 UNION ALL
Select '9503009200','Skipping ropes','UNT',0,0,0 UNION ALL
Select '9503009300','Marbles','UNT',0,0,0 UNION ALL
Select '9503009400','Other toys,of rubber','UNT',0,0,0 UNION ALL
Select '9503009900','Other','UNT',0,0,0 UNION ALL


Select '9504202000','Tables for billiards of all kinds','KGM',30,0,0 UNION ALL
Select '9504203000','Billiard chalks','KGM',15,0,0 UNION ALL
Select '9504209000','Other','KGM',30,0,0 UNION ALL

Select '95043010','Pintables or slot machines:','',0,0,0 UNION ALL
Select '9504301010','Having a chance that immediately return a monetary award','UNT',30,0,0 UNION ALL
Select '9504301090','Other','UNT',26,0,0 UNION ALL
Select '9504302000','Parts of wood, paper or plastics','UNT',25,0,0 UNION ALL
Select '9504309000','Other','UNT',5,0,0 UNION ALL
Select '9504400000','Playing cards','PCK',10,0,0.1 UNION ALL

Select '9504501000','Of a kind used with a television receiver','UNT',0,0,0 UNION ALL
Select '9504509000','Other','UNT',10,0,0 UNION ALL

Select '9504901000','Bowling requisites of all kinds','UNT',0,0,0 UNION ALL

Select '9504902100','Of wood or of paper or of plastics','UNT',30,0,0 UNION ALL
Select '9504902900','Other','UNT',10,0,0 UNION ALL

Select '9504903200','Tables designed for use with casino games, of wood or of plastics','UNT',30,0,0 UNION ALL
Select '9504903300','Other tables designed for use with casino games','UNT',10,0,0 UNION ALL
Select '9504903400','Mahjong tiles, of wood or of paper or of plastics','UNT',20,0,0.1 UNION ALL
Select '9504903500','Other Mahjong tiles','UNT',5,0,0.05 UNION ALL
Select '9504903600','Other, of wood or of paper or of plastics','UNT',30,0,0 UNION ALL
Select '9504903900','Other ','UNT',10,0,0 UNION ALL


Select '9504909200','Of wood or of plastics','UNT',30,0,0 UNION ALL
Select '9504909300','Other','UNT',10,0,0 UNION ALL

Select '9504909500','Of wood, of paper  or of plastics','UNT',30,0,0 UNION ALL
Select '9504909900','Other','UNT',10,0,0 UNION ALL

Select '9505100000','Articles for Christmas festivities','KGM',0,0,0 UNION ALL
Select '9505900000','Other','KGM',0,0,0 UNION ALL


Select '9506110000','Skis','PAI',0,0,0 UNION ALL
Select '9506120000','Skifastenings (skibindings) ','KGM',0,0,0 UNION ALL
Select '9506190000','Other','KGM',0,0,0 UNION ALL

Select '9506210000','Sailboards','UNT',0,0,0 UNION ALL
Select '9506290000','Other','UNT',0,0,0 UNION ALL

Select '9506310000','Clubs, complete','UNT',0,0,0 UNION ALL
Select '9506320000','Balls','UNT',0,0,0 UNION ALL
Select '9506390000','Other','KGM',0,0,0 UNION ALL

Select '9506401000','Tables','KGM',0,0,0 UNION ALL
Select '9506409000','Other ','KGM',0,0,0 UNION ALL

Select '9506510000','Lawntennis rackets, whether or not strung','UNT',0,0,0 UNION ALL
Select '9506590000','Other','UNT',0,0,0 UNION ALL

Select '9506610000','Lawntennis balls','UNT',0,0,0 UNION ALL
Select '9506620000','Inflatable','UNT',0,0,0 UNION ALL
Select '9506690000','Other','UNT',0,0,0 UNION ALL
Select '9506700000','Ice skates and roller skates, including skating boots with skates attached','PAI',0,0,0 UNION ALL

Select '9506910000','Articles and equipment for general physical exercise, gymnastics or athletics','KGM',0,0,0 UNION ALL

Select '9506991000','Bows (including crossbows) and arrows','UNT',0,0,0 UNION ALL
Select '9506992000','Nets, cricket pads and shin guards','UNT',0,0,0 UNION ALL
Select '9506993000','Shuttlecocks','UNT',0,0,0 UNION ALL
Select '9506999000','Other','UNT',0,0,0 UNION ALL

Select '9507100000','Fishing rods','UNT',0,0,0 UNION ALL
Select '9507200000','Fishhooks, whether or not snelled','KGM',0,0,0 UNION ALL
Select '9507300000','Fishing reels','UNT',0,0,0 UNION ALL
Select '9507900000','Other','UNT',0,0,0 UNION ALL

Select '9508100000','Travelling circuses and travelling menageries','KGM',30,0,0 UNION ALL
Select '9508900000','Other','KGM',30,0,0 UNION ALL


Select '9601101000','Cigar or cigarette cases, tobacco jars; ornamental articles ','KGM',15,0,0 UNION ALL
Select '9601109000','Other','KGM',5,0,0 UNION ALL


Select '9601901100','Cigar or cigarette cases, tobacco jars; ornamental articles ','KGM',15,0,0 UNION ALL
Select '9601901200','Pearl Nucleus ','KGM',5,0,0 UNION ALL
Select '9601901900','Other','KGM',5,0,0 UNION ALL
Select '96019090','Other:','',0,0,0 UNION ALL
Select '9601909010','Cigar or cigarette cases, tobacco jars; ornamental articles ','KGM',15,0,0 UNION ALL
Select '9601909090','Other','KGM',5,0,0 UNION ALL


Select '9602001000','Gelatin capsules for pharmaceutical products','KGM',5,0,0 UNION ALL
Select '9602002000','Cigar or cigarette cases, tobacco jars; ornamental articles','KGM',15,0,0 UNION ALL
Select '9602009000','Other','KGM',5,0,0 UNION ALL


Select '9603101000','Brushes','UNT',15,0,0 UNION ALL
Select '9603102000','Brooms','UNT',15,0,0 UNION ALL

Select '9603210000','Tooth brushes, including dentalplate brushes','UNT',20,0,0 UNION ALL
Select '9603290000','Other','UNT',15,0,0 UNION ALL
Select '9603300000','Artists"brushes, writing brushes and similar brushes for the application of cosmetics','UNT',5,0,0 UNION ALL
Select '9603400000','Paint, distemper, varnish or similar brushes (other than brushes of subheading 9603.30); paint pads and rollers','UNT',15,0,0 UNION ALL
Select '9603500000','Other brushes constituting parts of machines, appliances or vehicles','UNT',15,0,0 UNION ALL

Select '9603901000','Prepared knots and tufts for broom or brush making','UNT',15,0,0 UNION ALL
Select '9603902000','Handoperated mechanical floor sweepers, not motorised','UNT',0,0,0 UNION ALL
Select '9603904000','Other brushes','UNT',15,0,0 UNION ALL
Select '9603909000','Other','UNT',15,0,0 UNION ALL

Select '9604001000','Of metal','UNT',25,0,0 UNION ALL
Select '9604009000','Other','UNT',25,0,0 UNION ALL

Select '9605000000','Travel sets for personal toilet, sewing or shoe or clothes cleaning.','UNT',5,0,0 UNION ALL


Select '9606101000','Of plastics','KGM',30,0,0 UNION ALL
Select '9606109000','Other','KGM',5,0,0 UNION ALL

Select '9606210000','Of plastics, not covered with textile material','KGM',25,0,0 UNION ALL
Select '9606220000','Of base metal, not covered with textile material','KGM',25,0,0 UNION ALL
Select '9606290000','Other','KGM',5,0,0 UNION ALL

Select '9606301000','Of plastics','KGM',25,0,0 UNION ALL
Select '9606309000','Other','KGM',5,0,0 UNION ALL


Select '9607110000','Fitted with chain scoops of base metal','KGM',5,0,0 UNION ALL
Select '9607190000','Other','KGM',5,0,0 UNION ALL
Select '9607200000','Parts','KGM',5,0,0 UNION ALL


Select '9608101000','Of plastics','UNT',25,0,0 UNION ALL
Select '9608109000','Other','UNT',0,0,0 UNION ALL
Select '9608200000','Felt tipped and other poroustipped pens and markers','UNT',25,0,0 UNION ALL

Select '9608302000','Fountain pens','UNT',0,0,0 UNION ALL
Select '9608309000','Other','UNT',15,0,0 UNION ALL
Select '9608400000','Propelling or sliding pencils','UNT',15,0,0 UNION ALL
Select '9608500000','Sets of articles from two or more of the foregoing subheadings','UNT',15,0,0 UNION ALL

Select '9608601000','Of plastics','UNT',25,0,0 UNION ALL
Select '9608609000','Other','UNT',0,0,0 UNION ALL


Select '9608911000','Of gold or goldplated','UNT',0,0,0 UNION ALL
Select '9608919000','Other','UNT',0,0,0 UNION ALL

Select '9608991000','Duplicating stylos','KGM',0,0,0 UNION ALL

Select '9608999100','Parts of ball point pens, of plastics','KGM',25,0,0 UNION ALL
Select '9608999900','Other','KGM',0,0,0 UNION ALL


Select '9609101000','Black pencils','KGM',0,0,0 UNION ALL
Select '9609109000','Other','KGM',0,0,0 UNION ALL
Select '9609200000','Pencil leads, black or coloured','KGM',0,0,0 UNION ALL

Select '9609901000','Slate pencils for school slates','KGM',15,0,0 UNION ALL
Select '9609903000','Pencils and crayons other than those of subheading 9609.10','KGM',0,0,0 UNION ALL

Select '9609909100','Writing and drawing chalks','KGM',30,0,0 UNION ALL
Select '9609909900','Other','KGM',15,0,0 UNION ALL


Select '9610001000','School slates','KGM',0,0,0 UNION ALL
Select '9610009000','Other','KGM',0,0,0 UNION ALL

Select '9611000000','Date, sealing or numbering stamps, and the like (including devices for printing or embossing labels), designed for operating in the hand; handoperated composing sticks, and hand printing sets incorporating such composing sticks.','KGM',0,0,0 UNION ALL


Select '9612101000','Of textile fabric','UNT',20,0,0 UNION ALL
Select '9612109000','Other','UNT',25,0,0 UNION ALL
Select '9612200000','Inkpads','UNT',5,0,0 UNION ALL


Select '9613101000','Of plastics ','UNT',50,0,0 UNION ALL
Select '9613109000','Other','UNT',0,0,0 UNION ALL

Select '9613201000','Of plastics','UNT',50,0,0 UNION ALL
Select '9613209000','Other','UNT',0,0,0 UNION ALL

Select '9613801000','Piezoelectric lighters for stoves and ranges','UNT',30,0,0 UNION ALL
Select '9613802000','Cigarette lighters or table lighters of plastics','UNT',50,0,0 UNION ALL
Select '9613803000','Cigarette lighters or table lighters, other than of plastics','UNT',0,0,0 UNION ALL
Select '9613809000','Other','UNT',30,0,0 UNION ALL

Select '9613901000','Refillable cartridges or other receptacles, which constitute parts of mechanical lighters, containing liquid fuel or liquefied gases','KGM',5,0,0 UNION ALL
Select '9613909000','Other','KGM',30,0,0 UNION ALL

Select '9614001000','Roughly shaped blocks of wood or root for the manufacture of  pipes','UNT',30,0,0 UNION ALL
Select '9614009000','Other','KGM',30,0,0 UNION ALL



Select '9615112000','Of hard rubber','KGM',5,0,0 UNION ALL
Select '9615113000','Of plastics','KGM',20,0,0 UNION ALL
Select '9615190000','Other','KGM',5,0,0 UNION ALL


Select '9615901100','Of aluminium','KGM',20,0,0 UNION ALL
Select '9615901200','Of iron or steel','KGM',5,0,0 UNION ALL
Select '9615901300','Of plastics','KGM',20,0,0 UNION ALL
Select '9615901900','Other','KGM',10,0,0 UNION ALL

Select '9615902100','Of plastics','KGM',20,0,0 UNION ALL
Select '9615902200','Of iron or steel','KGM',5,0,0 UNION ALL
Select '9615902300','Of aluminium','KGM',20,0,0 UNION ALL
Select '9615902900','Other','KGM',10,0,0 UNION ALL

Select '9615909100','Of aluminium','KGM',20,0,0 UNION ALL
Select '9615909200','Of iron or steel','KGM',5,0,0 UNION ALL
Select '9615909300','Of plastics','KGM',20,0,0 UNION ALL
Select '9615909900','Other','KGM',10,0,0 UNION ALL


Select '9616101000','Scent sprays and similar toilet sprays','KGM',5,0,0 UNION ALL
Select '9616102000','Mounts and heads ','KGM',5,0,0 UNION ALL
Select '9616200000','Powderpuffs and pads for the application of cosmetics or toilet preparations','KGM',20,0,0 UNION ALL


Select '9617001000','Vacuum flask and Other Vacuum vessels, complete with cases','KGM',20,0,0 UNION ALL
Select '9617002000',' Parts thereof, other than glass inners','KGM',20,0,0 UNION ALL

Select '9618000000','Tailors dummies and other lay figures; automata and other animated displays used for shop window dressing.','KGM',5,0,0 UNION ALL


Select '9619001100','With an absorbent core of wadding of textile materials','KGM',20,0,0 UNION ALL
Select '9619001200','Sanitary towels and tampons of paper, paper pulp, cellulose wadding or webs of cellulose fibers','KGM',30,0,0 UNION ALL
Select '9619001300','Baby napkins, pads for incontinence, of paper, paper pulp, cellulose wadding or webs of cellulose fibers','KGM',0,0,0 UNION ALL
Select '9619001400','Other, of paper, paper pulp, cellulose wadding or webs of cellulose fibers','KGM',20,0,0 UNION ALL
Select '9619001900','Other','KGM',20,0,0 UNION ALL

Select '96190091','Knitted or crocheted:','',0,0,0 UNION ALL
Select '9619009110','Sanitary towels, of other made up articles','KGM',20,0,0 UNION ALL
Select '9619009190','Other','KGM',0,0,0 UNION ALL
Select '96190099','Other:','',0,0,0 UNION ALL
Select '9619009910','Sanitary towels, of other made up articles','KGM',20,0,0 UNION ALL
Select '9619009990','Other','KGM',0,0,0 UNION ALL

Select '9620001000','Of plastics','KGM',20,0,0 UNION ALL
Select '9620002000','Of carbon and graphite','KGM',0,0,0 UNION ALL
Select '9620003000','Of iron and steel','KGM',5,0,0 UNION ALL
Select '9620004000','Of aluminium','KGM',20,0,0 UNION ALL
Select '96200090','Other:','',0,0,0 UNION ALL
Select '9620009010','Of wood','KGM',20,0,0 UNION ALL
Select '9620009090','Other','KGM',5,0,0 UNION ALL

Select '9701100000','Paintings,  drawings  and  pastels','UNT',0,0,0 UNION ALL
Select '9701900000','Other','KGM',5,0,0 UNION ALL

Select '9702000000','Original engravings, prints and lithographs.','UNT',0,0,0 UNION ALL

Select '9703000000','Original sculptures and statuary, in any material.','UNT',0,0,0 UNION ALL

Select '9704000000','Postage or revenue stamps, stamppostmarks, first day covers, postal stationery (stamped paper), and the like, used or unused, other than those of heading 49.07.','KGM',0,0,0 UNION ALL

Select '9705001000','Of historical, archaeological, palaeontological and ethnographic interest','KGM',0,0,0 UNION ALL
Select '9705009000','Other','KGM',0,0,0 UNION ALL

Select '9706000000','Antiques of an age exceeding one hundred years.','KGM',0,0,0 UNION ALL

Select '98000000','Special provisions.','',0,0,0 UNION ALL
Select '9800000010','Mail bags','UNT',0,0,0 UNION ALL
Select '9800000020','Coffins containing human corpes','UNT',0,0,0 UNION ALL
Select '9800000030','Urns containing human ashes','UNT',0,0,0 UNION ALL
Select '9800000040','Used personal effects','KGM',0,0,0 UNION ALL
Select '9800000050','Used household effects','KGM',0,0,0 UNION ALL
Select '9800000060','Trade samples','KGM',0,0,0 UNION ALL
Select '9800000070','Ship''s spares','KGM',0,0,0 UNION ALL
Select '9800000080','Aircraft spares','KGM',0,0,0 

 