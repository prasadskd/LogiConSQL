
CREATE TABLE [Master].[CustomDeclarant](
	[BranchID] [bigint] NOT NULL,
	[ID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Designation] [nvarchar](50) NULL,
	[NRIC] [nvarchar](20) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_CustomDeclarant] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

