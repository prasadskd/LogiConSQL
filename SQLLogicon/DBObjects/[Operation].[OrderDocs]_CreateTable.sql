CREATE TABLE [Operation].[OrderDocs](
	[BranchID] [bigint] NOT NULL,
	[OrderNo] [nvarchar] (70) NOT NULL,
	[ItemNo] [smallint] NOT NULL CONSTRAINT [DF_OrderEntryDocs_ItemNo]  DEFAULT ((0)),
	[FileName] [nvarchar](255) NOT NULL CONSTRAINT [DF_OrderEntryDocs_FileName]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_OrderEntryDocs_CreatedOn]  DEFAULT (getutcdate()),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_OrderEntryDocs_ModifiedBy]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_OrderEntryDocs_ModifiedOn]  DEFAULT (getutcdate()),
	CONSTRAINT [PK_OrderEntryDocs] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[OrderNo] ASC,
	[ItemNo]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


