

ALTER TABLE Operation.OrderHeader
ADD ShippingAgentAddressID int NOT NULL DEFAULT 0

GO

ALTER TABLE Operation.OrderHeader
ADD FwdAgentAddressID int NOT NULL DEFAULT 0