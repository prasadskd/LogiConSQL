

/****** Object:  Table [Master].[CustomCurrencyRate]    Script Date: 09-04-2017 16:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Master].[CustomCurrencyRate](
	[CountryCode] [varchar](2) NOT NULL,
	[CurrencyCode] [nvarchar](3) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[ImportRate] [decimal](18, 4) NULL,
	[ExportRate] [decimal](18, 4) NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_CustomCurrencyRate] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[CurrencyCode] ASC,
	[StartDate] ASC,
	[EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

