ALTER TABLE [Config].[EmailSettings] ALTER COLUMN [ModifiedBy] nvarchar(100) NULL
ALTER TABLE [Config].[EmailSettings] ALTER COLUMN [ModifiedOn] datetime NULL