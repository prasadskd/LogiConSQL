
/****** Object:  Table [Billing].[CostSheet]    Script Date: 07-05-2017 19:18:27 ******/
DROP TABLE [Billing].[CostSheet]
GO

/****** Object:  Table [Billing].[CostSheet]    Script Date: 07-05-2017 19:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Billing].[CostSheet](
	[BranchID] [bigint] NOT NULL,
	[Module] [smallint] NOT NULL,
	[OrderNo] [varchar](50) NOT NULL,
	[ContainerKey] [varchar](50) NOT NULL,
	[JobNo] [varchar](50) NOT NULL,
	[ChargeCode] [nvarchar](20) NOT NULL,
	[BillingUnit] [smallint] NOT NULL,
	[ProductCode] [nvarchar](20) NOT NULL,
	[MovementCode] [nvarchar](50) NOT NULL,
	[PaymentTerm] [smallint] NULL,
	[PaymentTo] [smallint] NULL,
	[Size] [nvarchar](10) NULL,
	[Type] [nvarchar](10) NULL,
	[ProductCategory] [smallint] NULL,
	[CargoCategory] [smallint] NULL,
	[TruckCategory] [smallint] NULL,
	[VendorCode] [bigint] NULL,
	[CurrencyCode] [varchar](3) NULL,
	[ExRate] [decimal](18, 4) NULL,
	[Qty] [decimal](18, 4) NULL,
	[Price] [decimal](18, 4) NULL,
	[CostRate] [decimal](18, 4) NULL,
	[ForeignAmount] [decimal](18, 4) NULL,
	[LocalAmount] [decimal](18, 4) NULL,
	[DiscountType] [smallint] NULL,
	[DiscountRate] [decimal](5, 2) NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[ActualAmount] [decimal](18, 4) NULL,
	[TaxAmount] [decimal](18, 4) NULL,
	[TotalAmount]  AS ([ActualAmount]+[TaxAmount]),
	[IsSlabRate] [bit] NULL,
	[SlabFrom] [smallint] NULL,
	[SlabTo] [smallint] NULL,
	[IsVAS] [bit] NULL,
	[IsWaived] [bit] NULL,
	[IsAutoLoad] [bit] NULL,
	[IsInternalCost] [bit] NULL,
	[IsUserInput] [bit] NULL,
	[QuotationNo] [varchar](50) NULL,
	[Remark] [nvarchar](100) NULL,
	[InvoiceNo] [varchar](25) NULL,
	[InvoiceDate] [datetime] NULL,
	[PaidQty] [decimal](18, 4) NULL,
	[PaidAmount] [decimal](18, 4) NULL,
	[Status] [smallint] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[OutputGSTCode]  [nvarchar](10) NULL,
	[OutputGSTRate] [decimal](18, 2) NULL,
	[InputGSTCode]  [nvarchar](10) NULL,
	[InputGSTRate] [decimal](18, 2) NULL
 CONSTRAINT [PK_CostSheet] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[Module] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC,
	[JobNo] ASC,
	[ChargeCode] ASC,
	[BillingUnit] ASC,
	[ProductCode] ASC,
	[MovementCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

