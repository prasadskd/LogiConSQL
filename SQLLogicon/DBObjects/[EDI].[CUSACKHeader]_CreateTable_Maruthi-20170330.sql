USE [LogiCon]
GO

/****** Object:  Table [EDI].[CUSACKHeader]    Script Date: 3/31/2017 11:21:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[CUSACKHeader](
	[BranchId] [bigint] NOT NULL,
	[ReferenceNo] [uniqueidentifier] NOT NULL,
	[FileName] [nvarchar](50) NULL,
	[DocumentDate] [datetime] NULL,
	[SenderID] [nvarchar](50) NULL,
	[RecieverID] [nvarchar](50) NULL,
	[MessageType] [nvarchar](10) NULL,
	[MessageName] [nvarchar](50) NULL,
	[MessageFunction] [tinyint] NULL,
	[AgentCode] [nvarchar](50) NULL,
	[ProcessIndicator] [nvarchar](10) NULL,
	[StatusCode] [nvarchar](10) NULL,
	[CustomName] [nvarchar](50) NULL,
	[CustomAddress] [nvarchar](250) NULL,
	[DeclarantName] [nvarchar](50) NULL,
	[DeclarantAddress] [nvarchar](250) NULL,
	[AgentName] [nvarchar](50) NULL,
	[AgentAddress] [nvarchar](250) NULL,
	[CustomStationCode] [nvarchar](50) NULL,
	[PortOfDischarge] [nvarchar](50) NULL,
	[DeclarationDate] [datetime] NULL,
	[GISIndicator1] [tinyint] NULL,
	[GISIndicator2] [tinyint] NULL,
	[GISTransactionType] [nvarchar](5) NULL,
	[GISStatus] [tinyint] NULL,
	[NoOfPackages] [nvarchar](50) NULL,
	[PackageID] [nvarchar](50) NULL,
	[TaxType] [nvarchar](50) NULL,
	[TaxAmount] [nvarchar](50) NULL,
	[DeclarationNo] [nvarchar](50) NULL,
	[RegistrationDate] [datetime] NULL,
	[ReceiptNo] [nvarchar](50) NULL,
	[ReceiptDate] [datetime] NULL,
	[DeclarantRefNo] [nvarchar](50) NULL,
	[VesselID] [nvarchar](50) NULL,
	[VoyageNo] [nvarchar](50) NULL,
	[FlightNo] [nvarchar](50) NULL,
	[PTJCode] [nvarchar](50) NULL,
	[ConsignmentNote1] [nvarchar](50) NULL,
	[ConsignmentNote2] [nvarchar](50) NULL,
	[ConsignmentNote3] [nvarchar](50) NULL,
	[ContainerStatus] [tinyint] NULL,
	[ContainerNo] [nvarchar](20) NULL,
 CONSTRAINT [PK_CUSACKHeader] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC,
	[ReferenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


