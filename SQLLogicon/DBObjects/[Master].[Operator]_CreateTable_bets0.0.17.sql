USE [LogiCon]
GO

/****** Object:  Table [Master].[Operator]    Script Date: 26-02-2017 17:50:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[Operator](
	[OperatorType] [smallint] NOT NULL,
	[CountryCode] [nvarchar](3) NOT NULL,
	[OperatorID] [bigint] IDENTITY(10000,1) NOT NULL,
	[OperatorCode] [nvarchar](20) NOT NULL,
	[OperatorName] [nvarchar](100) NULL,
	[ROCNo] [nvarchar](50) NULL,
	[Address] [nvarchar](250) NULL,
	[TelephoneNo] [nvarchar](20) NULL,
	[FaxNo] [nvarchar](20) NULL,
	[PostCode] [nvarchar](50) NULL,
	[LicenseNo] [nvarchar](50) NULL,
	[LicenseStartDate] [datetime] NULL,
	[LicenseEndDate] [datetime] NULL,
	[Status] [bit] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Operator_1] PRIMARY KEY CLUSTERED 
(
	[OperatorType] ASC,
	[CountryCode] ASC,
	[OperatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

