USE [LogiCon]
GO

ALTER TABLE [EDI].[CUSERRHeader] DROP CONSTRAINT [DF_CUSERRHeader_DocumentDate]
GO

ALTER TABLE [EDI].[CUSERRHeader] DROP CONSTRAINT [DF_CUSERRHeader_FileName]
GO

ALTER TABLE [EDI].[CUSERRHeader] DROP CONSTRAINT [DF_CUSERRHeader_EDIType]
GO

/****** Object:  Table [EDI].[CUSERRHeader]    Script Date: 31-03-2017 10:16:12 ******/
DROP TABLE [EDI].[CUSERRHeader]
GO

/****** Object:  Table [EDI].[CUSERRHeader]    Script Date: 31-03-2017 10:16:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[CUSERRHeader](
	[BranchID] [bigint] NOT NULL,
	[ReferenceNo] [uniqueidentifier] NOT NULL,
	[EDIType] [smallint] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[DocumentDate] [datetime] NOT NULL,
	[SenderID] [nvarchar](35) NULL,
	[ReceiverID] [nvarchar](35) NULL,
	[MessageRefernceNo] [nvarchar](20) NULL,
	[MessageType] [smallint] NULL,
	[MessageFunction] [tinyint] NULL,
	[ProcessIndicator] [nvarchar](3) NULL,
	[StatusCode] [nvarchar](3) NULL,
	[CustomsName] [nvarchar](50) NULL,
	[CustomsAddress] [nvarchar](250) NULL,
	[DeclarantName] [nvarchar](50) NULL,
	[DeclarantAddress] [nvarchar](250) NULL,
	[AgentName] [nvarchar](50) NULL,
	[AgentAddress] [nvarchar](250) NULL,
	[ShippingAgentName] [nvarchar](50) NULL,
	[ShippingAgentAddress] [nvarchar](250) NULL,
	[CustomsStationCode] [nvarchar](50) NULL,
	[PlaceOfDischarge] [nvarchar](50) NULL,
	[DateType] [smallint] NULL,
	[PresentationDate] [datetime] NULL,
	[GISIndicator1] [tinyint] NULL,
	[GISIndicator2] [tinyint] NULL,
	[ResponseCode] [nvarchar](50) NULL,
	[ResponseOGACode] [nvarchar](50) NULL,
	[PackageType] [nvarchar](10) NULL,
	[PackageCode] [nvarchar](10) NULL,
	[PackageCount] [nvarchar](10) NULL,
	[MarksAndNumbers] [nvarchar](200) NULL,
	[OrderType] [smallint] NULL,
	[RegistrationDate] [datetime] NULL,
	[DeclarationNo] [nvarchar](50) NULL,
	[DeclarationRefNo] [nvarchar](50) NULL,
	[VesselID] [nvarchar](20) NULL,
	[VoyageNo] [nvarchar](10) NULL,
	[FlightNo] [nvarchar](10) NULL,
	[PTJCode] [nvarchar](10) NULL,
	[WareHouseLicenceNo] [nvarchar](10) NULL,
	[FileStatus] [bit] NULL,
	[IsProcessed] [bit] NULL,
	[ProcessRemarks] [nvarchar](100) NULL,
	[OrderNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_CUSERRHeader_1] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ReferenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [EDI].[CUSERRHeader] ADD  CONSTRAINT [DF_CUSERRHeader_EDIType]  DEFAULT ((0)) FOR [EDIType]
GO

ALTER TABLE [EDI].[CUSERRHeader] ADD  CONSTRAINT [DF_CUSERRHeader_FileName]  DEFAULT ('') FOR [FileName]
GO

ALTER TABLE [EDI].[CUSERRHeader] ADD  CONSTRAINT [DF_CUSERRHeader_DocumentDate]  DEFAULT (getutcdate()) FOR [DocumentDate]
GO

