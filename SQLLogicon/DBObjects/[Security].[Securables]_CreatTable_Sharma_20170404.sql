 

/****** Object:  Table [Security].[Securables]    Script Date: 04-04-2017 15:32:49 ******/
DROP TABLE [Security].[Securables]
GO

/****** Object:  Table [Security].[Securables]    Script Date: 04-04-2017 15:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Security].[Securables](
	[RegistrationType] [bigint] NOT NULL,
	[PageID] [nvarchar](100) NOT NULL,
	[SecurableItem] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_Securables_1] PRIMARY KEY CLUSTERED 
(
	[RegistrationType] ASC,
	[PageID] ASC,
	[SecurableItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

