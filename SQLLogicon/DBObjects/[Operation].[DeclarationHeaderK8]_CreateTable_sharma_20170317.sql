/****** Object:  Table [Operation].[DeclarationHeaderK8]    Script Date: 17-03-2017 16:29:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Operation].[DeclarationHeaderK8](
	[BranchID] [bigint] NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[K1DeclarationNo] [nvarchar](50) NOT NULL,
	[DeclarationDate] [datetime] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_DeclarationDate]  DEFAULT (getutcdate()),
	[DeclarationType] [smallint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_DeclarationType]  DEFAULT ((0)),
	[OpenDate] [datetime] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_OpenDate]  DEFAULT (getutcdate()),
	[ImportDate] [datetime] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_ImportDate]  DEFAULT (getutcdate()),
	[OrderNo] [nvarchar](50) NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_OrderNo]  DEFAULT (''),
	[TransportMode] [smallint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_TransportMode]  DEFAULT ((0)),
	[ShipmentType] [smallint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_ShipmentType]  DEFAULT ((0)),
	[TransactionType] [smallint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_TransactionType]  DEFAULT ((0)),
	[Importer] [bigint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_LocalTrader]  DEFAULT ((0)),
	[Exporter] [nvarchar](50) NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_OverseasTrader]  DEFAULT (''),
	[ExporterAddress1] [nvarchar](50) NULL,
	[ExporterAddress2] [nvarchar](50) NULL,
	[ExporterCity] [nvarchar](50) NULL,
	[ExporterState] [nvarchar](50) NULL,
	[ExporterCountry] [nvarchar](2) NULL,
	[ExporterOrganizationType] [smallint] NULL,
	[ExporterTelNo] [nvarchar](20) NULL,
	[SMKCode] [smallint] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_SMKCode]  DEFAULT ((0)),
	[CustomStationCode] [nvarchar](10) NULL,
	[ShippingAgent] [bigint] NULL,
	[DeclarantID] [nvarchar](35) NULL,
	[DeclarantName] [nvarchar](35) NULL,
	[DeclarantDesignation] [nvarchar](35) NULL,
	[DeclarantNRIC] [nvarchar](35) NULL,
	[DeclarantAddress1] [nvarchar](35) NULL,
	[DeclarantAddress2] [nvarchar](35) NULL,
	[DeclarantAddressCity] [nvarchar](35) NULL,
	[DeclarantAddressState] [nvarchar](35) NULL,
	[DeclarantAddressCountry] [nvarchar](2) NULL,
	[DeclarantAddressPostCode] [nvarchar](10) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_IsActive]  DEFAULT ((1)),
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsApproved] [bit] NOT NULL CONSTRAINT [DF_DeclarationHeaderK8_IsApproved]  DEFAULT ((0)),
	[ApprovedBy] [nvarchar](60) NULL,
	[ApprovedOn] [datetime] NULL,
	
 CONSTRAINT [PK_DeclarationHeaderK8] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


