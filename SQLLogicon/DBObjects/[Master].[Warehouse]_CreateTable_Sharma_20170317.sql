/****** Object:  Table [Master].[Warehouse]    Script Date: 17-03-2017 17:30:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[Warehouse](
	[CountryCode] [nvarchar](3) NOT NULL,
	[WareHouseID] [nvarchar](50) NOT NULL,
	[WarehouseCode] [nvarchar](5) NULL,
	[WarehouseType] [nvarchar](5) NULL,
	[LicenseNo] [nvarchar](50) NULL,
	[WarehouseName] [nvarchar](255) NULL,
	[WarehouseROCNo] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Warehouse_Status]  DEFAULT ((1)),
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Warehouse] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[WareHouseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

