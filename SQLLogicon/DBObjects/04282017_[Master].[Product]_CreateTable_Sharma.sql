

/****** Object:  Table [Master].[Product]    Script Date: 28-04-2017 21:25:37 ******/
DROP TABLE [Master].[Product]
GO

/****** Object:  Table [Master].[Product]    Script Date: 28-04-2017 21:25:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[Product](
	[BranchID] [bigint] NOT NULL,
	[PrincipalCode] [nvarchar](10) NOT NULL,
	[ProductCode] [nvarchar](20) NOT NULL,
	[TariffCode] [nvarchar](20) NULL,
	[Description] [nvarchar](50) NULL,
	[ProductCategory] [smallint] NULL,
	[CommodityCode] [nvarchar](10) NULL,
	[BaseUOM] [nvarchar](20) NULL,
	[BaseVolume] [float] NULL,
	[BaseWeight] [float] NULL,
	[BaseWidth] [float] NULL,
	[BaseHeight] [float] NULL,
	[BaseDepth] [float] NULL,
	[BaseSQFt] [float] NULL,
	[UnitSize] [int] NULL,
	[StorageUOM] [nvarchar](20) NULL,
	[IsConsignmentControl] [bit] NULL,
	[IsBatchNoControl] [bit] NULL,
	[IsSerialNoControl] [bit] NULL,
	[IsExpiryDateControl] [bit] NULL,
	[IsPONoControl] [bit] NULL,
	[IsInventoryItem] [bit] NULL,
	[ReplenishRule] [smallint] NULL,
	[ReplenishLevel] [int] NULL,
	[ReplenishQty] [int] NULL,
	[ShelfLife] [int] NULL,
	[CountFrequency] [int] NULL,
	[IsActive] [bit] NULL,
	[ConsignmentControlMode] [smallint] NULL,
	[BatchNoControlMode] [smallint] NULL,
	[SerialNoControlMode] [smallint] NULL,
	[ExpiryDateControlMode] [smallint] NULL,
	[PONoControlMode] [smallint] NULL,
	[BarCode] [nvarchar](20) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[PrincipalCode] ASC,
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


