 
/****** Object:  Table [Port].[TruckMovementHeader]    Script Date: 20-02-2017 09:55:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[TruckMovementHeader](
	[BranchID] [bigint] NOT NULL,
	[TransactionNo] [nvarchar](50) NOT NULL,
	[HaulierCode] [bigint] NULL,
	[TruckNo] [nvarchar](20) NULL,
	[TruckCategory] [smallint] NULL,
	[TransactionDate] [datetime] NULL,
	[Status] [bit] NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_TruckMovementHeader_1] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[TransactionNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

