

/****** Object:  Table [EDI].[InsuranceHeader]    Script Date: 17-03-2017 11:40:20 ******/
DROP TABLE [EDI].[InsuranceHeader]
GO

/****** Object:  Table [EDI].[InsuranceHeader]    Script Date: 17-03-2017 11:40:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[InsuranceHeader](
	[BranchID] [bigint] NOT NULL,
	[InsuranceReferenceNo] [bigint] IDENTITY(10000,1) NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[CreateDateTime] [datetime] NULL,
	[MessageFunction] [nvarchar](3) NULL,
	[EDIDateTime] [datetime] NULL,
	[FileName] [nvarchar](50) NULL,
 CONSTRAINT [PK_InsuranceHeader] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[InsuranceReferenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

