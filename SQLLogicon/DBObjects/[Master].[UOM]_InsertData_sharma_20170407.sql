TRUNCATE TABLE [Master].[UOM] 
GO

INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'AA', N'100KG', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'AAA', N'SYAHRIL UOM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ACR', N'ACRE (4840 YD2)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'AMH', N'AMPER-HOUR (3,6 KC)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'AMP', N'AMPERE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ANN', N'YEAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'APZ', N'OUNCE GB,US(31,10348 G)(SYN.:TROY OUNCE)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ARE', N'ARE (100 M2)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ASM', N'ALCOHOLIC STRENGTH BY MASS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ASV', N'ALCOHOLIC STRENGTH BY VOLUME', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ATM', N'STANDARD ATMOSPHERE (101325 PA)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ATT', N'TECNICAL ATMOSPHERE (98066,5 PA)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'AV', N'CAPSULE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BAR', N'BAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BCH', N'INTERMEDIATE BULK CONTAINER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BE', N'BUNDLE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BFT', N'BOARD FOOT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BG', N'BAG', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BHP', N'BRAKE HORSE POWER (245,5 WATTS)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BIL', N'BILLION EUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BJ', N'BUCKET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BLD', N'DRY BARREL (115,627 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BLL', N'BARREL (PETROLEUM) (158,987 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BO', N'BOTTLE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BOX', N'BOXES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BQL', N'BECQUEREL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BRS', N'BARES, MACHINES, BLOW MOULDINGS, DRYERS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BTU', N'BRITISH THERMAL UNIT (1,055 KILOJOULES)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BUA', N'BUSHEL (35,2391 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BUI', N'BUSHEL (36,36874 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'BX', N'BOX', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CA', N'CAN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CAD', N'CARDS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CAN', N'CANS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CCT', N'CARRYING CAPACITY IN METRIC TONNES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CDL', N'CANDELA', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CEL', N'DEGREE CELSIUS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CEN', N'HUNDRED', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CG', N'CARD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CGM', N'CENTIGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CHM', N'UNIT OF HUNDRED METER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CKG', N'COULOMB PER KILOGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CLF', N'HUNDRED LEAVES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CLT', N'CENTILITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CMK', N'SQUARE CENTIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CMQ', N'CUBIC CENTIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CMT', N'CENTIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CNE', N'CONE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CNP', N'HUNDRED PACKS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CNT', N'CENTAL GB (45,359237 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'COI', N'COIL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'COU', N'COULOMB', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CRP', N'CRATES (24 BOTTLES)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CRQ', N'CRATES (12 BOTTLES)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CSE', N'CASE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CT', N'CARTON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CTM', N'METRIC CARAT (200 MG = 2.10-4 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CUR', N'CURIE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CWA', N'HUNDREDWEIGHT US (45,3592 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'CWI', N'(LONG) HUNDREDWEIGHT, GB (50,802345 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DAA', N'DECARE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DAD', N'TEN DAYS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DAL', N'DECA LITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DAY', N'DAY', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DEC', N'DECADE (TEN YEARS)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DLT', N'DECILITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DMK', N'SQUARE DECIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DMQ', N'CUBIC DECIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DMT', N'DECIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DPC', N'DOZEN PIECES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DPR', N'DOZEN PAIRS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DPT', N'DISPLACEMENT TONNAGE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DRA', N'DRAM US (3,887935 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DRI', N'DRAM GB (1,771745 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DRL', N'DOZEN ROLLS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DRM', N'DRACHM GB (3,887935 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DTH', N'HECTOKILOGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DTN', N'CENTNER,METRIC (100 KG)(SYN.:HECTOKG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DWT', N'PENNYWEIGHT GB, US (1,555174 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DZN', N'DOZEN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'DZP', N'DOZEN PACKS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'E12', N'MILE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'E2', N'BELT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'E27', N'DOSE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ECH', N'EACH', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'FAH', N'DEGREE FAHRENHEIT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'FAR', N'FARAD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'FOT', N'FOOT (0,3048 M)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'FTK', N'SQUARE FOOT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'FTQ', N'CUBIC FOOT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GAL', N'GALLON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GBQ', N'GIGABECQUEREL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GFI', N'GRAM OF FISSILE ISOTOPES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GIA', N'GILL (11,8294 CM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GII', N'GILL (0,142065 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GLD', N'DRY GALLON (4,404884 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GLI', N'GALLON UK(4,546092 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GLL', N'GALLON US(3,78541 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GRM', N'GRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GRN', N'GRAIN GB, US (64,798910 MG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GRO', N'GROSS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GRT', N'GROSS [REGISTER] TON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GWH', N'GIGAWATT-HOUR (1 MILLION KW/H)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GWT', N'GROSS WEIGHT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'GY', N'GROSS YARD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'H87', N'PIECE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HAR', N'HECTARE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HBA', N'HECTOBAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HCT', N'100 CONTAINER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HEA', N'HEAD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HGM', N'HECTOGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HIU', N'HUNDRED INTERNATIONAL UNITS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HLT', N'HECTOLITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HMQ', N'MILLION CUBIC METRES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HMT', N'HECTOMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HPA', N'HECTOLITRE OF PURE ALCOHOL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HTZ', N'HERTZ', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'HUR', N'HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'INH', N'INCH (25,4 MM)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'INK', N'SQUARE INCH', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'INQ', N'CUBIC INCH', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'JOU', N'JOULE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'JR', N'JAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'K6', N'KILOLITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KAR', N'KARTUS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KBA', N'KILOBAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KBT', N'KILOBITE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KEG', N'KEG Liquor', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KEL', N'KELVIN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KGM', N'KILOGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KGS', N'KILOGRAM PER SECOND', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KHZ', N'KILOHERTZ', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KJO', N'KILOJOULE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KMH', N'KILOMETRE PER HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KMK', N'SQUARE KILOMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KMQ', N'KILOGRAM PER CUBIC METER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KMT', N'KILOMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KNI', N'KILOGRAM OF NITROGEN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KNS', N'KILOGRAM OF NAMED SUBSTANCE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KNT', N'KNOT (1 N MILE PER HOUR)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KPA', N'KILOPASCAL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KPH', N'KILOGRAM OF POTASSIUM HYDROXIDE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KPO', N'KILOGRAM OF POTASSIUM OXIDE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KPP', N'KILOGRAM OF PHOSPHORUS PENTOXIDE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KSD', N'KILOGRAM OF SUBSTANCE 90 PER CENT DRY', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KSH', N'KILOGRAM OF SODIUM HYDROXIDE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KTN', N'KILOTONNE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KUR', N'KILOGRAM OF URANIUM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KVA', N'KILOVOLT-AMPERE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KVR', N'KILOVAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KVT', N'KILOVOLT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KWH', N'KILOWATT-HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'KWT', N'KILOWATT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LBR', N'POUND GB, US (0,45359237 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LBT', N'TROY POUND, US (373,242 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LEF', N'LEAF', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LEN', N'LENGTH', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LO', N'LOT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LPA', N'LITRE OF PURE ALCOHOL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LTN', N'LONG TON GB, US (1,0160469 T)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LTR', N'LITRE (1 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LUM', N'LUMEN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'LUX', N'LUX', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'M3', N'CUBIC METRE AT PRESSURE 1013 BAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MAL', N'MEGALITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MAM', N'MEGAMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MAW', N'MEGAWATT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MBA', N'MEGABITE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MBE', N'THOUSAND STANDARD BRICK EQUIVALENT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MBF', N'THOUSAND BOARD FEET (2,36 M3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MBR', N'MILLIBAR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MCU', N'MILLICURIE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MGM', N'MILLIGRAM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MHZ', N'MEGAHERTZ', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MIK', N'SQUARE MILE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MIN', N'MINUTE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MIO', N'MILLION', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MIU', N'MILLION INTERNATIONAL UNITS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MLD', N'BILLION US', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MLT', N'MILLILITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MMK', N'SQUARE MILLIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MMQ', N'CUBIC MILLIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MMT', N'MILLIMETRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MON', N'MONTH', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MPA', N'MAGEPASCAL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MQH', N'CUBIC METER PER HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MQS', N'CUBIC METRE PER SECOND', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MSK', N'METRE PER SECOND SQUARED', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MTK', N'SQUARE METRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MTQ', N'CUBIC METRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MTR', N'METRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MTS', N'METRE PER SECOND', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MVA', N'MEGAVOLT-AMPERE (1000 KVA)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'MWH', N'MEGAWATT-HOUR(1000 KW/H)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NAR', N'NUMBER OF ARTICLES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NBB', N'NUMBERS OF BOBBINS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NCL', N'NUMBER OF CELLS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NEW', N'NEWTON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NIU', N'NUMBER OF INTERNATIONAL UNITS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NMB', N'NUMBER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NMI', N'NAUTICAL MILE (1852 M)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NMP', N'NUMBER OF PACKS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NPL', N'NUMBER OF PARCELS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NPR', N'NUMBER OF PAIRS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NPT', N'NUMBER OF PARTS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NRL', N'NUMBER OF ROLLS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'NTT', N'NET [REGISTER] TON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'OHM', N'OHM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ONE', N'UNIT OF ONE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ONZ', N'OUNCE GB, US (28,349523 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'OZA', N'FLUID OUNCE (29,5735 CM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'OZI', N'FLUID OUNCE (28,413 CM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PAL', N'PASCAL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PCE', N'PIECE(FOR PCO)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PCK', N'PACK', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PGL', N'PROOF GALLON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PIN', N'PINT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PK', N'PACKAGE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PKT', N'PACKET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PLT', N'PALLET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PR', N'PAIR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PTD', N'DRY PINT (0,55061 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PTI', N'PINT (0,568262 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'PTL', N'LIQUID PINT (0,473176 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'QAN', N'QUARTER (OF A YEAR)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'QTD', N'DRY QUART (1,101221 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'QTI', N'QUART (1,136523 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'QTL', N'LIQUID QUART (0,946353 DM3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'QTR', N'QUARTER, GB (12,700586 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RD', N'ROD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'REL', N'REEL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RIM', N'REEMS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RMS', N'ROOMS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RO', N'ROLL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RPM', N'REVOLUTION PER MINUTE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'RPS', N'REVOLUTION PER SECOND', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SAC', N'SACK', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SAN', N'HALF YEAR (SIX MONTHS)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SCO', N'SCORE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SCR', N'SCRUPLE GB, US (1,295982 G)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SEC', N'SECOND', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SHT', N'SHIPPING TON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SIE', N'SIEMENS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SLB', N'SLAB', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SME', N'SQUARE METRE EQUIVALENT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SMI', N'(STATUTE) MILE (1609,344 M)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SMM', N'SQUARE METRE OF 5 MM', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SPL', N'SPOOL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SST', N'SHORT STANDARD (7200 MATCHES)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'ST', N'SHEET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'STI', N'STONE, GB (6,350293 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'STK', N'STICK', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'STN', N'TON (US)OR SHORT TON (UK/US)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'STW', N'STRAW', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'SYR', N'SYRINGE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'T3', N'THOUSAND PIECES', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TAB', N'TABLET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TAH', N'THOUSAND AMPERE-HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TEN', N'UNIT OF TEN', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TNE', N'TONNE (1000 KG)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TNK', N'TANK', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TON', N'TON', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TPR', N'TEN PAIRS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TPT', N'UNIT OF TEN PACKET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TQD', N'TOUSAND CUBIC METERS PER DAY', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TRL', N'TRILLION EUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TRY', N'TRAY', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TSD', N'TONNE OF SUBSTANCE 90 % DRY', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TSH', N'TON OF STEAM PER HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TST', N'UNIT OF TEN SET', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TTS', N'TEN THOUSAND OF STICKS', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'TUB', N'TUBE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'UNT', N'UNIT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'VAL', N'VALUE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'VIA', N'VIAL', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'VLT', N'VOLT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'VPL', N'100% VOLUME PER LITRE', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WCD', N'CORD (3,63 M3)', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WEB', N'WEBER', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WEE', N'WEEK', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WHR', N'WATT-HOUR', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WSD', N'STANDARD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'WTT', N'WATT', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'YDK', N'SQUARE YARD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'YDQ', N'CUBIC YARD', N'', N'')
GO
INSERT [Master].[UOM] ([UOMCode], [UOMDescription], [MappingCode], [ISOCode]) VALUES (N'Z11', N'HANGING CONTAINER', N'', N'')
GO
