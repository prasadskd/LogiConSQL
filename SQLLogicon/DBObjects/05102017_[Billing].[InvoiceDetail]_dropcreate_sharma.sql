

/****** Object:  Table [Billing].[InvoiceDetail]    Script Date: 10-05-2017 14:59:13 ******/
DROP TABLE [Billing].[InvoiceDetail]
GO

/****** Object:  Table [Billing].[InvoiceDetail]    Script Date: 10-05-2017 14:59:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Billing].[InvoiceDetail](
	[BranchID] [bigint] NOT NULL,
	[InvoiceNo] [varchar](25) NOT NULL,
	[Module] [smallint] NOT NULL,
	[OrderNo] [varchar](50) NOT NULL,
	[ContainerKey] [varchar](50) NOT NULL,
	[JobNo] [varchar](50) NOT NULL,
	[ChargeCode] [nvarchar](20) NOT NULL,
	[BillingUnit] [smallint] NOT NULL,
	[ProductCode] [nvarchar](20) NOT NULL,
	[MovementCode] [nvarchar](50) NOT NULL,
	[Size] [nvarchar](10) NULL,
	[Type] [nvarchar](10) NULL,
	[ProductCategory] [smallint] NULL,
	[CargoCategory] [smallint] NULL,
	[TruckCategory] [smallint] NULL,
	[CurrencyCode] [varchar](3) NULL,
	[ExRate] [decimal](18, 4) NULL,
	[Qty] [decimal](18, 4) NULL,
	[Price] [decimal](18, 4) NULL,
	[CostRate] [decimal](18, 4) NULL,
	[ForeignAmount] [decimal](18, 4) NULL,
	[LocalAmount] [decimal](18, 4) NULL,
	[DiscountType] [smallint] NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[ActualAmount] [decimal](18, 4) NULL,
	[TaxAmount] [decimal](18, 4) NULL,
	[TotalAmount]  AS ([ActualAmount]+[TaxAmount]),
	[IsSlabRate] [bit] NULL,
	[SlabFrom] [smallint] NOT NULL CONSTRAINT [DF_InvoiceDetail_SlabFrom]  DEFAULT ((0)),
	[SlabTo] [smallint] NOT NULL CONSTRAINT [DF_InvoiceDetail_SlabTo]  DEFAULT ((0)),
	[InputGSTCode] [nvarchar](10) NULL,
	[InputGSTRate] [decimal](5, 2) NULL,
	[OutputGSTCode] [nvarchar](10) NULL,
	[OutputGSTRate] [decimal](5, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[InvoiceNo] ASC,
	[Module] ASC,
	[OrderNo] ASC,
	[ContainerKey] ASC,
	[JobNo] ASC,
	[ChargeCode] ASC,
	[BillingUnit] ASC,
	[ProductCode] ASC,
	[MovementCode] ASC,
	[SlabFrom] ASC,
	[SlabTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

