ALTER TABLE Operation.OrderHeader 
Add InsuranceTokenNo nvarchar(255) NULL
GO

ALTER TABLE Operation.OrderHeader 
Add StowageCode smallint NULL
GO



ALTER TABLE Operation.OrderHeader 
Add ExtraCargoDescription nvarchar(300) NULL
GO

ALTER TABLE EDI.InsuranceHeader
Add InsuranceTokenNo nvarchar(255) NULL
 GO
  


 ALTER TABLE Master.HSCode
 ADD OldPDK nvarchar(20) NULL, 
     OldAHTN nvarchar(20) NULL