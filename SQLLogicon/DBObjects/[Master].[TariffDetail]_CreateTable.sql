 

/****** Object:  Table [Master].[TariffDetail]    Script Date: 12-02-2017 10:18:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[TariffDetail](
	[QuotationNo] [nvarchar](50) NOT NULL,
	[QuotationType] [smallint] NOT NULL,
	[Module] [smallint] NOT NULL,
	[Association] [smallint] NOT NULL,
	[TariffMode] [smallint] NULL,
	[Transactions] [smallint] NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[SlabFrom] [int] NOT NULL,
	[SlabTo] [int] NOT NULL,
	[Percentage] [decimal](18, 2) NULL,
	[CreatedBy] [nvarchar](60) NOT NULL CONSTRAINT [DF_TariffDetail_CreatedBy]  DEFAULT (''),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TariffDetail_CreatedOn]  DEFAULT (getutcdate()),
	[ModifiedBy] [nvarchar](60) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_TariffDetail] PRIMARY KEY CLUSTERED 
(
	[QuotationNo] ASC,
	[QuotationType] ASC,
	[Module] ASC,
	[Association] ASC,
	[SlabFrom] ASC,
	[SlabTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Master].[TariffDetail] ADD  CONSTRAINT [DF_TariffDetail_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [Master].[TariffDetail] ADD  CONSTRAINT [DF_TariffDetail_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO


