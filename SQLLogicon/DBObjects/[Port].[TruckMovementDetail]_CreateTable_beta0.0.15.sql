 

/****** Object:  Table [Port].[TruckMovementDetail]    Script Date: 20-02-2017 09:55:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Port].[TruckMovementDetail](
	[BranchID] [bigint] NOT NULL,
	[TransactionNo] [nvarchar](50) NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[ContainerKey] [nvarchar](50) NULL,
	[TripType] [smallint] NULL,
	[MovementCode] [nvarchar](50) NULL,
	[Size] [nvarchar](10) NULL,
	[Type] [nvarchar](10) NULL,
	[DateIn] [datetime] NULL,
	[DateOut] [datetime] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_TruckMovementDetail] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[TransactionNo] ASC,
	[ItemNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

