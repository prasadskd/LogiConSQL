
/****** Object:  Table [EDI].[K1Declaration]    Script Date: 18-03-2017 16:41:16 ******/
DROP TABLE [EDI].[K1Declaration]
GO

/****** Object:  Table [EDI].[K1Declaration]    Script Date: 18-03-2017 16:41:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[K1Declaration](
	[BranchID] [bigint] NOT NULL,
	[ReferenceNo] [bigint] IDENTITY(1000,1) NOT NULL,
	[DeclarationNo] [nvarchar](50) NOT NULL,
	[MessageType] [nvarchar](3) NULL,
	[CreateDate] [datetime] NULL,
	[EDIDateTime] [datetime] NULL,
	[FileName] [nvarchar](100) NULL,
 CONSTRAINT [PK_K1Declaration] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ReferenceNo] ASC,
	[DeclarationNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

