 
/****** Object:  Table [Master].[CustomStationMailBox]    Script Date: 08-04-2017 15:38:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Master].[CustomStationMailBox](
	[CustomStationCode] [nvarchar](10) NOT NULL,
	[CountryCode] [varchar](2) NOT NULL,
	[DeclarationType] [bigint] NOT NULL,
	[MailBoxID] [nvarchar](35) NOT NULL,
 CONSTRAINT [PK_CustomStationMailBox] PRIMARY KEY CLUSTERED 
(
	[CustomStationCode] ASC,
	[CountryCode] ASC,
	[DeclarationType] ASC,
	[MailBoxID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

