USE [LogiCon]
GO

/****** Object:  Table [EDI].[CUSERRDetail]    Script Date: 31-03-2017 10:18:00 ******/
DROP TABLE [EDI].[CUSERRDetail]
GO

/****** Object:  Table [EDI].[CUSERRDetail]    Script Date: 31-03-2017 10:18:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[CUSERRDetail](
	[BranchID] [bigint] NOT NULL,
	[Referenceno] [uniqueidentifier] NOT NULL,
	[ItemNo] [smallint] NOT NULL,
	[MessageNo] [nvarchar](50) NULL,
	[ErrorCode] [nvarchar](10) NULL,
	[CustomeIndicator] [nvarchar](10) NULL,
	[ResponseAgency] [nvarchar](10) NULL,
	[TaxType] [nvarchar](10) NULL,
	[TaxAmount] [nvarchar](10) NULL,
 CONSTRAINT [PK_CUSERRDetail_1] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[Referenceno] ASC,
	[ItemNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

