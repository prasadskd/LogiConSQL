 

/****** Object:  Table [Master].[AssociationHeader]    Script Date: 10-02-2017 16:31:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Master].[AssociationHeader](
	[AssociationID] [smallint] NOT NULL,
	[CountryCode] [nvarchar](3) NOT NULL,
	[AssociationName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_AssociationHeader_1] PRIMARY KEY CLUSTERED 
(
	[AssociationID] ASC,
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Master].[AssociationHeader] ADD  CONSTRAINT [DF_AssociationHeader_Description]  DEFAULT ('') FOR [Description]
GO


