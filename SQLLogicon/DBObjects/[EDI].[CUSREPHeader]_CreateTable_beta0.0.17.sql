 
/****** Object:  Table [EDI].[CUSREPHeader]    Script Date: 28-02-2017 00:11:38 ******/
DROP TABLE [EDI].[CUSREPHeader]
GO

/****** Object:  Table [EDI].[CUSREPHeader]    Script Date: 28-02-2017 00:11:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [EDI].[CUSREPHeader](
	[BranchID] [bigint] NOT NULL,
	[ReferenceNo] [bigint] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL CONSTRAINT [DF_CUSREPHeader_FileName]  DEFAULT (''),
	[ReferenceText] [nvarchar](50) NULL,
	[DocumentType] [smallint] NOT NULL CONSTRAINT [DF_CUSREPHeader_DocumentType]  DEFAULT ((0)),
	[MessageType] [tinyint] NOT NULL CONSTRAINT [DF_CUSREPHeader_MessageType]  DEFAULT ((0)),
	[SenderCode] [nvarchar](50) NULL CONSTRAINT [DF_CUSREPHeader_SenderCode]  DEFAULT (''),
	[ReceiverCode] [nvarchar](50) NULL CONSTRAINT [DF_CUSREPHeader_ReceiverCode]  DEFAULT (''),
	[PreparationDateTime] [datetime] NULL,
	[POP] [nvarchar](50) NULL,
	[POT] [nvarchar](50) NULL,
	[PlaceOfArrival] [nvarchar](50) NULL,
	[ETA] [datetime] NULL,
	[ETD] [datetime] NULL,
	[PlaceOfRegistration] [nvarchar](50) NULL,
	[DischargePort] [nvarchar](50) NULL,
	[DestinationPort] [nvarchar](50) NULL,
	[AgentCode] [nvarchar](50) NULL,
	[AgentContact] [nvarchar](100) NULL,
	[AgentTel] [nvarchar](50) NULL,
	[AgentFax] [nvarchar](50) NULL,
	[PSACode] [nvarchar](50) NULL,
	[PSAName] [nvarchar](100) NULL,
	[PSAAddress] [nvarchar](255) NULL,
	[VesselName] [nvarchar](100) NULL,
	[VesselCode] [nvarchar](50) NULL,
	[VoyageNo] [nvarchar](50) NULL,
	[BerthNumber] [nvarchar](50) NULL,
	[ShipCallNumber] [nvarchar](50) NULL,
	[GrossRegisteredTonnage] [decimal](18, 0) NULL,
	[NettRegisteredTonnage] [decimal](18, 0) NULL,
	[OverallLength] [decimal](18, 0) NULL,
	[DeadWeight] [decimal](18, 0) NULL,
	[StandardDraft] [decimal](18, 0) NULL,
	[VesselCapacity] [decimal](18, 0) NULL,
	[ShipClass] [nvarchar](50) NULL,
	[FlagShip] [nvarchar](3) NULL,
	[YearBuilt] [smallint] NULL,
	[CountryCode] [nvarchar](50) NULL,
	[LoadingRemarks] [nvarchar](50) NULL,
	[DischargeRemarks] [nvarchar](50) NULL,
	[ChangeRemarks] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](60) NULL,
	[CreatedOn] [datetime] NULL,
	[IsProcessed] [bit] NOT NULL CONSTRAINT [DF_CUSREPHeader_IsProcessed]  DEFAULT ((0)),
	[Remarks] [nvarchar](max) NULL,
 CONSTRAINT [PK_CUSREPHeader] PRIMARY KEY CLUSTERED 
(
	[BranchID] ASC,
	[ReferenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

