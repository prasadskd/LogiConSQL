 

-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [ContainerMovement] Record based on [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_ContainerMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementSelect] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementSelect] 
    @BranchID bigint,
    @DocumentNo nvarchar(50)
AS 
 

BEGIN
	
	;with MovementTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator'),
	EFIndicatorDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='EFIndicator'),
	TempratureTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TempratureType'),
	VentilationTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='VentilationType'),
	ContainerMaterialDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerMaterial'),
	ContainerHeightDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerHeight'),
	BookingTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='JobType')
	SELECT	Mv.[BranchID], Mv.[DocumentNo],Mv.[MovementDate], Mv.[TruckTransactionNo], Mv.[OrderNo],Mv.[ContainerKey], Mv.[MovementCode], Mv.[MovementType], 
			Mv.[EFIndicator], Mv.BookingType, Mv.[AgentCode], 
			Mv.[CustomerCode], Mv.[VesselID], Mv.[VesselName], Mv.[VoyageNo], Mv.[ShipCallNo], Mv.[ContainerNo], Mv.[Size], Mv.[Type], 
			Mv.[ContainerGrade], Mv.[ContainerStatus], Mv.[Temperature], Mv.[TemperatureType], Mv.[Vent], Mv.[VentType], Mv.[Material], 
			Mv.[Height], Mv.[TareWeight], Mv.[MGW], Mv.[SealNo1], Mv.[SealNo2], Mv.[YearBuilt], Mv.[Remarks], Mv.[Status], Mv.[EDIDateTime], 
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn],
			ISNULL(MT.LookupDescription,'') As MovementTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TT.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VT.LookupDescription,'') As VentilationTypeDescription,
			ISNULL(CM.LookupDescription,'') As ContainerMaterialDescription,
			ISNULL(CH.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(BkD.LookupDescription,'') As BookingTypeDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cust.MerchantName,'') As CustomerName 
	FROM   [Port].[ContainerMovement] Mv
	Left Outer Join MovementTypeDescription	MT ON 
		Mv.MovementCode = MT.LookupID
	Left Outer Join EFIndicatorDescription	EF ON 
		Mv.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TT ON 
		Mv.TemperatureType = TT.LookupID
	Left Outer Join VentilationTypeDescription VT ON 
		Mv.VentType = VT.LookupID
	Left Outer Join ContainerMaterialDescription CM ON 
		Mv.Material = CM.LookupID
	Left Outer Join ContainerHeightDescription CH ON 
		Mv.Height = CH.LookupID
	Left Outer Join Master.Merchant Cust ON 
		Mv.CustomerCode = Cust.MerchantCode
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
	Left Outer Join BookingTypeDescription BkD On 
		Mv.BookingType = Bkd.LookupID
	WHERE  Mv.[BranchID] = @BranchID
	       AND Mv.[DocumentNo] = @DocumentNo

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [ContainerMovement] Records from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_ContainerMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementList] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementList] 
    @BranchID bigint 

AS 
 
BEGIN
	;with MovementTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator'),
	EFIndicatorDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='EFIndicator'),
	TempratureTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TempratureType'),
	VentilationTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='VentilationType'),
	ContainerMaterialDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerMaterial'),
	ContainerHeightDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerHeight'),
	BookingTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='JobType')
	SELECT	Mv.[BranchID], Mv.[DocumentNo],Mv.[MovementDate], Mv.[TruckTransactionNo], Mv.[OrderNo],Mv.[ContainerKey], Mv.[MovementCode], Mv.[MovementType], 
			Mv.[EFIndicator], Mv.BookingType, Mv.[AgentCode], 
			Mv.[CustomerCode], Mv.[VesselID], Mv.[VesselName], Mv.[VoyageNo], Mv.[ShipCallNo], Mv.[ContainerNo], Mv.[Size], Mv.[Type], 
			Mv.[ContainerGrade], Mv.[ContainerStatus], Mv.[Temperature], Mv.[TemperatureType], Mv.[Vent], Mv.[VentType], Mv.[Material], 
			Mv.[Height], Mv.[TareWeight], Mv.[MGW], Mv.[SealNo1], Mv.[SealNo2], Mv.[YearBuilt], Mv.[Remarks], Mv.[Status], Mv.[EDIDateTime], 
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn],
			ISNULL(MT.LookupDescription,'') As MovementTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TT.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VT.LookupDescription,'') As VentilationTypeDescription,
			ISNULL(CM.LookupDescription,'') As ContainerMaterialDescription,
			ISNULL(CH.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(BkD.LookupDescription,'') As BookingTypeDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cust.MerchantName,'') As CustomerName 
	FROM   [Port].[ContainerMovement] Mv
	Left Outer Join MovementTypeDescription	MT ON 
		Mv.MovementCode = MT.LookupID
	Left Outer Join EFIndicatorDescription	EF ON 
		Mv.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TT ON 
		Mv.TemperatureType = TT.LookupID
	Left Outer Join VentilationTypeDescription VT ON 
		Mv.VentType = VT.LookupID
	Left Outer Join ContainerMaterialDescription CM ON 
		Mv.Material = CM.LookupID
	Left Outer Join ContainerHeightDescription CH ON 
		Mv.Height = CH.LookupID
	Left Outer Join Master.Merchant Cust ON 
		Mv.CustomerCode = Cust.MerchantCode
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
	Left Outer Join BookingTypeDescription BkD On 
		Mv.BookingType = Bkd.LookupID
	WHERE  Mv.[BranchID] = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [ContainerMovement] PageView Records from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_ContainerMovementPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementPageView]
END 
GO
CREATE PROC [Port].[usp_ContainerMovementPageView] 
    @BranchID bigint,
	@fetchrows bigint
AS 
BEGIN

	;with MovementTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator'),
	EFIndicatorDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='EFIndicator'),
	TempratureTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TempratureType'),
	VentilationTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='VentilationType'),
	ContainerMaterialDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerMaterial'),
	ContainerHeightDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerHeight'),
	BookingTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='JobType')
	SELECT	Mv.[BranchID], Mv.[DocumentNo],Mv.[MovementDate], Mv.[TruckTransactionNo], Mv.[OrderNo],Mv.[ContainerKey], Mv.[MovementCode], Mv.[MovementType], 
			Mv.[EFIndicator], Mv.BookingType, Mv.[AgentCode], 
			Mv.[CustomerCode], Mv.[VesselID], Mv.[VesselName], Mv.[VoyageNo], Mv.[ShipCallNo], Mv.[ContainerNo], Mv.[Size], Mv.[Type], 
			Mv.[ContainerGrade], Mv.[ContainerStatus], Mv.[Temperature], Mv.[TemperatureType], Mv.[Vent], Mv.[VentType], Mv.[Material], 
			Mv.[Height], Mv.[TareWeight], Mv.[MGW], Mv.[SealNo1], Mv.[SealNo2], Mv.[YearBuilt], Mv.[Remarks], Mv.[Status], Mv.[EDIDateTime], 
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn],
			ISNULL(MT.LookupDescription,'') As MovementTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TT.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VT.LookupDescription,'') As VentilationTypeDescription,
			ISNULL(CM.LookupDescription,'') As ContainerMaterialDescription,
			ISNULL(CH.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(BkD.LookupDescription,'') As BookingTypeDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cust.MerchantName,'') As CustomerName 
	FROM   [Port].[ContainerMovement] Mv
	Left Outer Join MovementTypeDescription	MT ON 
		Mv.MovementCode = MT.LookupID
	Left Outer Join EFIndicatorDescription	EF ON 
		Mv.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TT ON 
		Mv.TemperatureType = TT.LookupID
	Left Outer Join VentilationTypeDescription VT ON 
		Mv.VentType = VT.LookupID
	Left Outer Join ContainerMaterialDescription CM ON 
		Mv.Material = CM.LookupID
	Left Outer Join ContainerHeightDescription CH ON 
		Mv.Height = CH.LookupID
	Left Outer Join Master.Merchant Cust ON 
		Mv.CustomerCode = Cust.MerchantCode
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
	Left Outer Join BookingTypeDescription BkD On 
		Mv.BookingType = Bkd.LookupID
	WHERE  Mv.[BranchID] = @BranchID
	ORDER BY [DocumentNo]  
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_ContainerMovementRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementRecordCount] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementRecordCount] 
    @BranchID bigint 

AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Port].[ContainerMovement]
	Where BranchID = @BranchID
	

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [ContainerMovement] auto-complete search based on [ContainerMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_ContainerMovementAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementAutoCompleteSearch]
END 
GO
CREATE PROC [Port].[usp_ContainerMovementAutoCompleteSearch]
    @BranchID bigint,
    @DocumentNo nvarchar(50)
     
AS 

BEGIN
	;with MovementTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator'),
	EFIndicatorDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='EFIndicator'),
	TempratureTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='TempratureType'),
	VentilationTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='VentilationType'),
	ContainerMaterialDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerMaterial'),
	ContainerHeightDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='ContainerHeight'),
	BookingTypeDescription  As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='JobType')
	SELECT	Mv.[BranchID], Mv.[DocumentNo],Mv.[MovementDate], Mv.[TruckTransactionNo], Mv.[OrderNo],Mv.[ContainerKey], Mv.[MovementCode], Mv.[MovementType], 
			Mv.[EFIndicator], Mv.BookingType, Mv.[AgentCode], 
			Mv.[CustomerCode], Mv.[VesselID], Mv.[VesselName], Mv.[VoyageNo], Mv.[ShipCallNo], Mv.[ContainerNo], Mv.[Size], Mv.[Type], 
			Mv.[ContainerGrade], Mv.[ContainerStatus], Mv.[Temperature], Mv.[TemperatureType], Mv.[Vent], Mv.[VentType], Mv.[Material], 
			Mv.[Height], Mv.[TareWeight], Mv.[MGW], Mv.[SealNo1], Mv.[SealNo2], Mv.[YearBuilt], Mv.[Remarks], Mv.[Status], Mv.[EDIDateTime], 
			Mv.[CreatedBy], Mv.[CreatedOn], Mv.[ModifiedBy], Mv.[ModifiedOn],
			ISNULL(MT.LookupDescription,'') As MovementTypeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TT.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VT.LookupDescription,'') As VentilationTypeDescription,
			ISNULL(CM.LookupDescription,'') As ContainerMaterialDescription,
			ISNULL(CH.LookupDescription,'') As ContainerHeightDescription,
			ISNULL(BkD.LookupDescription,'') As BookingTypeDescription,
			ISNULL(Agnt.MerchantName,'') As AgentName,
			ISNULL(Cust.MerchantName,'') As CustomerName 
	FROM   [Port].[ContainerMovement] Mv
	Left Outer Join MovementTypeDescription	MT ON 
		Mv.MovementCode = MT.LookupID
	Left Outer Join EFIndicatorDescription	EF ON 
		Mv.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TT ON 
		Mv.TemperatureType = TT.LookupID
	Left Outer Join VentilationTypeDescription VT ON 
		Mv.VentType = VT.LookupID
	Left Outer Join ContainerMaterialDescription CM ON 
		Mv.Material = CM.LookupID
	Left Outer Join ContainerHeightDescription CH ON 
		Mv.Height = CH.LookupID
	Left Outer Join Master.Merchant Cust ON 
		Mv.CustomerCode = Cust.MerchantCode
	Left Outer Join Master.Merchant Agnt ON 
		Mv.AgentCode = Agnt.MerchantCode
	Left Outer Join BookingTypeDescription BkD On 
		Mv.BookingType = Bkd.LookupID
	WHERE  [BranchID] = @BranchID
	       AND [DocumentNo] LIKE '%' +  @DocumentNo + '%'
END
-- ========================================================================================================================================
-- END  											[Port].[usp_ContainerMovementAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_ContainerMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementInsert] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementInsert] 
    @BranchID bigint,
    @DocumentNo nvarchar(50),
	@MovementDate datetime,
	@TruckTransactionNo nvarchar(50)=NULL,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @MovementCode nvarchar(50) = NULL,
    @MovementType smallint = NULL,
    @EFIndicator smallint = NULL,
	@BookingType smallint = NULL,
    @AgentCode bigint = NULL,
    @CustomerCode bigint = NULL,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @ContainerNo nvarchar(15) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @ContainerGrade smallint = NULL,
    @ContainerStatus nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TemperatureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @Material smallint = NULL,
    @Height smallint = NULL,
    @TareWeight decimal(18, 4) = NULL,
    @MGW decimal(18, 4) = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @YearBuilt nvarchar(10) = NULL,
    @Remarks nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewDocumentNo nvarchar(60) OUTPUT

AS 
  

BEGIN
	
	INSERT INTO [Port].[ContainerMovement] (
			[BranchID], [DocumentNo],[MovementDate],[TruckTransactionNo], [OrderNo], [ContainerKey], [MovementCode], [MovementType], [EFIndicator], 
			BookingType,[AgentCode], [CustomerCode], [VesselID], [VesselName], [VoyageNo], [ShipCallNo], [ContainerNo], 
			[Size], [Type], [ContainerGrade], [ContainerStatus], [Temperature], [TemperatureType], 
			[Vent], [VentType], [Material], [Height], [TareWeight], [MGW], [SealNo1], [SealNo2], [YearBuilt], 
			[Remarks], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @DocumentNo,@MovementDate ,@TruckTransactionNo, @OrderNo, @ContainerKey, @MovementCode, @MovementType, @EFIndicator, 
			@BookingType,@AgentCode, @CustomerCode, @VesselID, @VesselName, @VoyageNo, @ShipCallNo, @ContainerNo, 
			@Size, @Type, @ContainerGrade, @ContainerStatus, @Temperature, @TemperatureType, 
			@Vent, @VentType, @Material, @Height, @TareWeight, @MGW, @SealNo1, @SealNo2, @YearBuilt, 
			@Remarks, CAST(1 as bit), @CreatedBy, GETUTCDATE()
	

	Select @NewDocumentNo = @DocumentNo
               
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_ContainerMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementUpdate] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementUpdate] 
    @BranchID bigint,
    @DocumentNo nvarchar(50),
	@MovementDate datetime,
	@TruckTransactionNo nvarchar(50)=NULL,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @MovementCode nvarchar(50) = NULL,
    @MovementType smallint = NULL,
    @EFIndicator smallint = NULL,
	@BookingType smallint = NULL,
    @AgentCode bigint = NULL,
    @CustomerCode bigint = NULL,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @ContainerNo nvarchar(15) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @ContainerGrade smallint = NULL,
    @ContainerStatus nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TemperatureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @Material smallint = NULL,
    @Height smallint = NULL,
    @TareWeight decimal(18, 4) = NULL,
    @MGW decimal(18, 4) = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @YearBuilt nvarchar(10) = NULL,
    @Remarks nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewDocumentNo nvarchar(60) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Port].[ContainerMovement]
	SET		TruckTransactionNo = @TruckTransactionNo,[OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [MovementCode] = @MovementCode, 
			[MovementType] = @MovementType, [EFIndicator] = @EFIndicator, BookingType = @BookingType,[AgentCode] = @AgentCode, 
			[CustomerCode] = @CustomerCode, [VesselID] = @VesselID, [VesselName] = @VesselName, 
			[VoyageNo] = @VoyageNo, [ShipCallNo] = @ShipCallNo, [ContainerNo] = @ContainerNo, 
			[Size] = @Size, [Type] = @Type, [ContainerGrade] = @ContainerGrade, 
			[ContainerStatus] = @ContainerStatus, [Temperature] = @Temperature, [TemperatureType] = @TemperatureType, 
			[Vent] = @Vent, [VentType] = @VentType, [Material] = @Material, [Height] = @Height, 
			[TareWeight] = @TareWeight, [MGW] = @MGW, [SealNo1] = @SealNo1, [SealNo2] = @SealNo2, 
			[YearBuilt] = @YearBuilt, [Remarks] = @Remarks, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(),MovementDate = @MovementDate
	WHERE	[BranchID] = @BranchID
			AND [DocumentNo] = @DocumentNo
	

	Select @NewDocumentNo = @DocumentNo
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [ContainerMovement] Record Into [ContainerMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_ContainerMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementSave] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementSave] 
    @BranchID bigint,
    @DocumentNo nvarchar(50),
	@MovementDate datetime,
	@TruckTransactionNo nvarchar(50)=NULL,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @MovementCode nvarchar(50) = NULL,
    @MovementType smallint = NULL,
    @EFIndicator smallint = NULL,
	@BookingType smallint = NULL,
    @AgentCode bigint = NULL,
    @CustomerCode bigint = NULL,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @ContainerNo nvarchar(15) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @ContainerGrade smallint = NULL,
    @ContainerStatus nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TemperatureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @Material smallint = NULL,
    @Height smallint = NULL,
    @TareWeight decimal(18, 4) = NULL,
    @MGW decimal(18, 4) = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @YearBuilt nvarchar(10) = NULL,
    @Remarks nvarchar(100) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewDocumentNo nvarchar(60) OUTPUT

AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[ContainerMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [DocumentNo] = @DocumentNo)>0
	BEGIN
	    Exec [Port].[usp_ContainerMovementUpdate] 
			@BranchID, @DocumentNo,@MovementDate,@TruckTransactionNo, @OrderNo, @ContainerKey, @MovementCode, @MovementType, @EFIndicator,@BookingType, @AgentCode, 
			@CustomerCode, @VesselID, @VesselName, @VoyageNo, @ShipCallNo, @ContainerNo, @Size, @Type, 
			@ContainerGrade, @ContainerStatus, @Temperature, @TemperatureType, @Vent, @VentType, @Material, @Height, 
			@TareWeight, @MGW, @SealNo1, @SealNo2, @YearBuilt, @Remarks, @CreatedBy, @ModifiedBy,@NewDocumentNo = @NewDocumentNo OUTPUT


	END
	ELSE
	BEGIN

		Declare @Dt datetime,
				@MovementTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@MovementTypeDesc = LookupDescription,@DocID='',@DocumentNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@MovementType
		
	 
		
		IF @MovementTypeDesc='INWARD' 
			SET @DocID ='Port\EIR(IN)'
		ELSE IF @MovementTypeDesc='OUTWARD' 
			SET @DocID ='Port\EIR(OUT)'
		 
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Port\EIR(IN)', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		--select @BranchID, @DocID, @Dt ,@CreatedBy
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @DocumentNo OUTPUT

	    Exec [Port].[usp_ContainerMovementInsert] 
			@BranchID, @DocumentNo,@MovementDate,@TruckTransactionNo, @OrderNo, @ContainerKey, @MovementCode, @MovementType, @EFIndicator,@BookingType, @AgentCode, 
			@CustomerCode, @VesselID, @VesselName, @VoyageNo, @ShipCallNo, @ContainerNo, @Size, @Type, 
			@ContainerGrade, @ContainerStatus, @Temperature, @TemperatureType, @Vent, @VentType, @Material, @Height, 
			@TareWeight, @MGW, @SealNo1, @SealNo2, @YearBuilt, @Remarks, @CreatedBy, @ModifiedBy,@NewDocumentNo = @NewDocumentNo OUTPUT


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[ContainerMovementSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Port].[usp_ContainerMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [ContainerMovement] Record  based on [ContainerMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_ContainerMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_ContainerMovementDelete] 
END 
GO
CREATE PROC [Port].[usp_ContainerMovementDelete] 
    @BranchID bigint,
    @DocumentNo nvarchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Port].[ContainerMovement]
	SET	[Status] = CAST(0 as bit),ModifiedOn = GETUTCDATE(), ModifiedBy = @ModifiedBy
	WHERE 	[BranchID] = @BranchID
	       AND [DocumentNo] = @DocumentNo

	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_ContainerMovementDelete]
-- ========================================================================================================================================

GO 
