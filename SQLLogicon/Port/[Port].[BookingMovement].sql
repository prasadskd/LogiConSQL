 


-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [BookingMovement] Record based on [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingMovementSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementSelect] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementSelect] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @Sequence smallint
AS 
 

BEGIN

	SELECT	[BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [MovementDate], [TransactionNo], [TruckTransactionNo], 
			[Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Port].[BookingMovement]
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo  
			AND [ContainerKey] = @ContainerKey  
			AND [MovementCode] = @MovementCode 
			AND [Sequence] = @Sequence 

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingMovementSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [BookingMovement] Records from [BookingMovement] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingMovementList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementList] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementList] 
    @BranchID bigint,
    @OrderNo nvarchar(50)
AS 
 
BEGIN
	SELECT	[BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [MovementDate], [TransactionNo], [TruckTransactionNo], 
			[Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn] 
	FROM	[Port].[BookingMovement]
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo  

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingMovementList] 
-- ========================================================================================================================================

GO

 

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingMovementInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementInsert] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementInsert] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @Sequence smallint,
    @MovementDate datetime = NULL,
    @TransactionNo nvarchar(50) = NULL,
    @TruckTransactionNo nvarchar(50) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL
AS 
  

BEGIN
	
	INSERT INTO [Port].[BookingMovement] (
			[BranchID], [OrderNo], [ContainerKey], [MovementCode], [Sequence], [MovementDate], [TransactionNo], [TruckTransactionNo], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @MovementDate, @TransactionNo, @TruckTransactionNo, CAST(1 as bit), @CreatedBy, GETUTCDATE()
	
               
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingMovementInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingMovementUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementUpdate] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementUpdate] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @Sequence smallint,
    @MovementDate datetime = NULL,
    @TransactionNo nvarchar(50) = NULL,
    @TruckTransactionNo nvarchar(50) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL
AS 
 
	
BEGIN

	UPDATE	[Port].[BookingMovement]
	SET		[MovementDate] = @MovementDate, [TransactionNo] = @TransactionNo, [TruckTransactionNo] = @TruckTransactionNo, 
			[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
			AND [MovementCode] = @MovementCode
			AND [Sequence] = @Sequence
	

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingMovementUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [BookingMovement] Record Into [BookingMovement] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingMovementSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementSave] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementSave] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @MovementCode nvarchar(50),
    @Sequence smallint,
    @MovementDate datetime = NULL,
    @TransactionNo nvarchar(50) = NULL,
    @TruckTransactionNo nvarchar(50) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[BookingMovement] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey
	       AND [MovementCode] = @MovementCode
	       AND [Sequence] = @Sequence)>0
	BEGIN
	    Exec [Port].[usp_BookingMovementUpdate] 
			@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @MovementDate, @TransactionNo, @TruckTransactionNo, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Port].[usp_BookingMovementInsert] 
			@BranchID, @OrderNo, @ContainerKey, @MovementCode, @Sequence, @MovementDate, @TransactionNo, @TruckTransactionNo, @CreatedBy, @ModifiedBy 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[BookingMovementSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_BookingMovementDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [BookingMovement] Record  based on [BookingMovement]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingMovementDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingMovementDelete] 
END 
GO
CREATE PROC [Port].[usp_BookingMovementDelete] 
    @BranchID bigint,
    @OrderNo nvarchar(50)
AS 

	
BEGIN

	DELETE	[Port].[BookingMovement]
	
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	      
	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingMovementDelete]
-- ========================================================================================================================================

GO 
