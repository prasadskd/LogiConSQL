

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSearch]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	21-Feb-2017
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table

-- Exec [Port].[usp_BookingHeaderSearch] 1007001,NULL,NULL,NULL,NULL,NULL,NULL,NULL
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSearch]
END 
GO
CREATE PROC [Port].[usp_BookingHeaderSearch]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@BookingNo nvarchar(50),
	@CustomerName nvarchar(100),
	@DateFrom datetime=NULL,
	@DateTo datetime = NULL

AS 
 

BEGIN
	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
	declare @sql2 nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';
	SELECT BHd.[BranchID], BHd.[OrderNo], BHd.[OrderDate], BHd.[Voyageno], BHd.[BookingNo],
			BHd.[CustomerName], BHd.[VesselName]
	FROM	[Port].[BookingHeader] BHd	
	WHERE	BHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND ISNULL(IsCancel,0) =CAST(0 as bit) '    

			 
			 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  BHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@BookingNo)) > 0) set @sql = @sql + ' And BHd.BookingNo LIKE ''%' + @BookingNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And BHd.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And BHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@CustomerName)) > 0) set @sql = @sql + ' And BHd.CustomerName LIKE ''%' + @CustomerName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',BHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',BHd.OrderDate)'

	print @sql;

	exec sp_executesql @sql , N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderSearch]
-- ========================================================================================================================================

GO

 
-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSearchCount]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	21-Feb-2017
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table

-- Exec [Port].[usp_BookingHeaderSearchCount] 1007001,NULL,NULL,'098',NULL,NULL,NULL,NULL
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderSearchCount]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSearchCount]
END 
GO 
CREATE PROC [Port].[usp_BookingHeaderSearchCount]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@BookingNo nvarchar(50),
	@CustomerName nvarchar(100),
	@DateFrom datetime=NULL,
	@DateTo datetime = NULL

AS 
 

BEGIN
	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
	declare @sql2 nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';
	SELECT Count(0) as Count
	FROM	[Port].[BookingHeader] BHd	
	WHERE	BHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND ISNULL(IsCancel,0) = CAST(0 as bit) '     		 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  BHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@BookingNo)) > 0) set @sql = @sql + ' And BHd.BookingNo LIKE ''%' + @BookingNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And BHd.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And BHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@CustomerName)) > 0) set @sql = @sql + ' And BHd.CustomerName LIKE ''%' + @CustomerName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',BHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',BHd.OrderDate)'

	print @sql;

	exec sp_executesql @sql , N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;

END


-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderSearchCount]
-- ========================================================================================================================================

GO

 
-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSearchPageView]
-- ========================================================================================================================================
-- Author:		Prasad
-- Create date: 	21-Feb-2017
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table

-- Exec [Port].[usp_BookingHeaderSearchPageView] 1001001,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-01','2017-01-12'
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderSearchPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSearchPageView]
END 
GO 
CREATE PROC [Port].[usp_BookingHeaderSearchPageView]
    @BranchID BIGINT,
    @OrderNo VARCHAR(70),
	@VesselName nvarchar(100),
	@VoyageNo nvarchar(20),
	@BookingNo nvarchar(50),
	@CustomerName nvarchar(100),
	@DateFrom datetime=NULL,
	@DateTo datetime = NULL,
	@Skip int,
	@Limit int

AS 
 

BEGIN
	declare @maxSearchRecord int;

	declare @sql nvarchar(max);
	declare @sql2 nvarchar(max);
 

	select @maxSearchRecord = 500;


	set @sql = N';
	SELECT BHd.[BranchID], BHd.[OrderNo], BHd.[OrderDate], BHd.[Voyageno], BHd.[BookingNo],
			BHd.[CustomerName], BHd.[VesselName]
	FROM	[Port].[BookingHeader] BHd	
	WHERE	BHd.[BranchID] = ' +  Convert(varchar(20),@BranchID) +' AND ISNULL(IsCancel,0) = CAST(0 as bit) '	 


	if (len(rtrim(@OrderNo)) > 0) set @sql = @sql+ ' AND  BHd.[OrderNo] LIKE ''%' + @OrderNo + '%'''
	if (len(rtrim(@BookingNo)) > 0) set @sql = @sql + ' And BHd.BookingNo LIKE ''%' + @BookingNo  + '%'''
	if (len(rtrim(@VesselName)) > 0) set @sql = @sql + ' And BHd.VesselName LIKE ''%' + @VesselName  + '%'''
	if (len(rtrim(@VoyageNo)) > 0) set @sql = @sql + ' And BHd.VoyageNo LIKE ''%' + @VoyageNo  + '%'''
	if (len(rtrim(@CustomerName)) > 0) set @sql = @sql + ' And BHd.CustomerName LIKE ''%' + @CustomerName  + '%'''
	
	if (ISNULL(@DateFrom,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) >= ISNULL(''' + Convert(Char(10),@DateFrom,120) + ''',BHd.OrderDate)'
	if (ISNULL(@DateTo,'') > 0) set @sql = @sql + ' And Convert(Char(10),BHd.OrderDate,120) <= ISNULL(''' + Convert(Char(10),@DateTo,120) + ''',BHd.OrderDate)'

	Set @sql += ' Order By BHd.OrderDate DESC OFFSET ' + CONVERT(varchar(5), @Skip) + ' ROWS FETCH NEXT ' + Convert(varchar(5), @Limit) + ' ROWS ONLY'

	print @sql;
	exec sp_executesql @sql , N'@maxSearchRecord int', @maxSearchRecord = @maxSearchRecord;

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderSearchPageView]
-- ========================================================================================================================================

GO