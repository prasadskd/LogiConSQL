 


-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [BookingContainer] Record based on [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingContainerSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerSelect] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerSelect] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50)
AS 
 

BEGIN

	;with ContainerGradeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ContainerGrade'),
	EFIndicatorDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='EFIndicator'),
	TempratureTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TempratureType'),
	VentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='VentilationType'),
	CargoCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='CargoCategory'),
	PickUpModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='PickupMode')
	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [EFIndicator], [IMOCode], [UNNo], 
			[Temperature], [TempratureType], [Vent], [VentType], [CargoCategory], [PickupDate], [SealNo1], [SealNo2], [VGM], [StowageCell], 
			[PUDOMode], [Remarks], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [Commodity], [SpecialInstructions],
			ISNULL(CGD.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TD.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VD.LookupDescription,'') As VentTypeDescription,
			ISNULL(CD.LookupDescription,'') As CargoCategoryDescription,
			ISNULL(PKD.LookupDescription,'') As PickUpModeDescription
	FROM	[Port].[BookingContainer] BkDt
	Left Outer Join ContainerGradeDescription CGD ON 
		BkDt.ContainerGrade = CGD.LookupID
	Left Outer Join EFIndicatorDescription EF ON 
		BkDt.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TD ON
		BkDt.TempratureType = TD.LookupID
	Left Outer Join VentTypeDescription VD ON
		BkDt.VentType = vd.LookupID
	Left Outer Join CargoCategoryDescription CD On 
		BkDt.CargoCategory = CD.LookupID
	Left Outer Join PickUpModeDescription PKD ON 
		BkDt.PUDOMode = PKD.LookupID
	WHERE  BkDt.[BranchID] = @BranchID   
	       AND BkDt.[OrderNo] = @OrderNo  
	       AND BkDt.[ContainerKey] = @ContainerKey  

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [BookingContainer] Records from [BookingContainer] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingContainerList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerList] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerList] 
    @BranchID bigint,
    @OrderNo nvarchar(50)

AS 
 
BEGIN
	;with ContainerGradeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ContainerGrade'),
	EFIndicatorDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='EFIndicator'),
	TempratureTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TempratureType'),
	VentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='VentilationType'),
	CargoCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='CargoCategory'),
	PickUpModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='PickupMode')
	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [EFIndicator], [IMOCode], [UNNo], 
			[Temperature], [TempratureType], [Vent], [VentType], [CargoCategory], [PickupDate], [SealNo1], [SealNo2], [VGM], [StowageCell], 
			[PUDOMode], [Remarks], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [Commodity], [SpecialInstructions],
			ISNULL(CGD.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TD.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VD.LookupDescription,'') As VentTypeDescription,
			ISNULL(CD.LookupDescription,'') As CargoCategoryDescription,
			ISNULL(PKD.LookupDescription,'') As PickUpModeDescription
	FROM	[Port].[BookingContainer] BkDt
	Left Outer Join ContainerGradeDescription CGD ON 
		BkDt.ContainerGrade = CGD.LookupID
	Left Outer Join EFIndicatorDescription EF ON 
		BkDt.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TD ON
		BkDt.TempratureType = TD.LookupID
	Left Outer Join VentTypeDescription VD ON
		BkDt.VentType = vd.LookupID
	Left Outer Join CargoCategoryDescription CD On 
		BkDt.CargoCategory = CD.LookupID
	Left Outer Join PickUpModeDescription PKD ON 
		BkDt.PUDOMode = PKD.LookupID
	WHERE  BkDt.[BranchID] = @BranchID   
	       AND BkDt.[OrderNo] = @OrderNo 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerList] 
-- ========================================================================================================================================

GO

IF OBJECT_ID('[Port].[usp_BookingContainerListByContainerNo]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerListByContainerNo] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerListByContainerNo] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
	@ContainerNo nvarchar(50)
AS 
 
BEGIN
	;with ContainerGradeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='ContainerGrade'),
	EFIndicatorDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='EFIndicator'),
	TempratureTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='TempratureType'),
	VentTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='VentilationType'),
	CargoCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='CargoCategory'),
	PickUpModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory ='PickupMode')
	SELECT	[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [EFIndicator], [IMOCode], [UNNo], 
			[Temperature], [TempratureType], [Vent], [VentType], [CargoCategory], [PickupDate], [SealNo1], [SealNo2], [VGM], [StowageCell], 
			[PUDOMode], [Remarks], [Status], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [Commodity], [SpecialInstructions],
			ISNULL(CGD.LookupDescription,'') As ContainerGradeDescription,
			ISNULL(EF.LookupDescription,'') As EFIndicatorDescription,
			ISNULL(TD.LookupDescription,'') As TempratureTypeDescription,
			ISNULL(VD.LookupDescription,'') As VentTypeDescription,
			ISNULL(CD.LookupDescription,'') As CargoCategoryDescription,
			ISNULL(PKD.LookupDescription,'') As PickUpModeDescription
	FROM	[Port].[BookingContainer] BkDt
	Left Outer Join ContainerGradeDescription CGD ON 
		BkDt.ContainerGrade = CGD.LookupID
	Left Outer Join EFIndicatorDescription EF ON 
		BkDt.EFIndicator = EF.LookupID
	Left Outer Join TempratureTypeDescription TD ON
		BkDt.TempratureType = TD.LookupID
	Left Outer Join VentTypeDescription VD ON
		BkDt.VentType = vd.LookupID
	Left Outer Join CargoCategoryDescription CD On 
		BkDt.CargoCategory = CD.LookupID
	Left Outer Join PickUpModeDescription PKD ON 
		BkDt.PUDOMode = PKD.LookupID
	WHERE  BkDt.[BranchID] = @BranchID   
	       AND BkDt.[OrderNo] = @OrderNo 
		   AND BkDt.[ContainerNo] like '%' + @ContainerNo + '%'
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerListByContainerNo] 
-- ========================================================================================================================================

GO


 

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingContainerInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerInsert] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerInsert] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @ContainerNo nvarchar(15),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ContainerGrade smallint = NULL,
    @EFIndicator smallint = NULL,
    @IMOCode nvarchar(20) = NULL,
    @UNNo nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TempratureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @CargoCategory smallint = NULL,
    @PickupDate datetime = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @VGM decimal(18, 4) = NULL,
    @StowageCell nvarchar(20) = NULL,
    @PUDOMode smallint = NULL,
    @Remarks nvarchar(MAX) = NULL,
	@Commodity nvarchar(MAX) = NULL,
	@SpecialInstructions nvarchar(MAX) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
  

BEGIN
	
	INSERT INTO [Port].[BookingContainer] (
			[BranchID], [OrderNo], [ContainerKey], [ContainerNo], [Size], [Type], [ContainerGrade], [EFIndicator], [IMOCode], [UNNo], [Temperature], [TempratureType], 
			[Vent], [VentType], [CargoCategory], [PickupDate], [SealNo1], [SealNo2], [VGM], [StowageCell], [PUDOMode], [Remarks], [Commodity], [SpecialInstructions], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @EFIndicator, @IMOCode, @UNNo, @Temperature, @TempratureType, 
			@Vent, @VentType, @CargoCategory, @PickupDate, @SealNo1, @SealNo2, @VGM, @StowageCell, @PUDOMode, @Remarks, @Commodity, @SpecialInstructions, CAST(1 as bit), @CreatedBy, GETUTCDATE()	
               
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingContainerUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerUpdate] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerUpdate] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @ContainerNo nvarchar(15),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ContainerGrade smallint = NULL,
    @EFIndicator smallint = NULL,
    @IMOCode nvarchar(20) = NULL,
    @UNNo nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TempratureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @CargoCategory smallint = NULL,
    @PickupDate datetime = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @VGM decimal(18, 4) = NULL,
    @StowageCell nvarchar(20) = NULL,
    @PUDOMode smallint = NULL,
    @Remarks nvarchar(MAX) = NULL,
	@Commodity nvarchar(MAX) = NULL,
	@SpecialInstructions nvarchar(MAX) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 
	
BEGIN

	UPDATE	[Port].[BookingContainer]
	SET		[ContainerNo] = @ContainerNo, [Size] = @Size, [Type] = @Type, [ContainerGrade] = @ContainerGrade, [EFIndicator] = @EFIndicator, [IMOCode] = @IMOCode, 
			[UNNo] = @UNNo, [Temperature] = @Temperature, [TempratureType] = @TempratureType, [Vent] = @Vent, [VentType] = @VentType, [CargoCategory] = @CargoCategory, 
			[PickupDate] = @PickupDate, [SealNo1] = @SealNo1, [SealNo2] = @SealNo2, [VGM] = @VGM, [StowageCell] = @StowageCell, [PUDOMode] = @PUDOMode, 
			[Remarks] = @Remarks,  [Commodity] = @Commodity, [SpecialInstructions] = @SpecialInstructions,[ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
			AND [ContainerKey] = @ContainerKey
	

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerUpdate]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [BookingContainer] Record Into [BookingContainer] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingContainerSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerSave] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerSave] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @ContainerKey nvarchar(50),
    @ContainerNo nvarchar(15),
    @Size nvarchar(10),
    @Type nvarchar(10),
    @ContainerGrade smallint = NULL,
    @EFIndicator smallint = NULL,
    @IMOCode nvarchar(20) = NULL,
    @UNNo nvarchar(20) = NULL,
    @Temperature decimal(18, 2) = NULL,
    @TempratureType smallint = NULL,
    @Vent decimal(18, 2) = NULL,
    @VentType smallint = NULL,
    @CargoCategory smallint = NULL,
    @PickupDate datetime = NULL,
    @SealNo1 nvarchar(20) = NULL,
    @SealNo2 nvarchar(20) = NULL,
    @VGM decimal(18, 4) = NULL,
    @StowageCell nvarchar(20) = NULL,
    @PUDOMode smallint = NULL,
    @Remarks nvarchar(MAX) = NULL,
	@Commodity nvarchar(MAX) = NULL,
	@SpecialInstructions nvarchar(MAX) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL 
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[BookingContainer] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo
	       AND [ContainerKey] = @ContainerKey)>0
	BEGIN
	    Exec [Port].[usp_BookingContainerUpdate] 
		@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @EFIndicator, @IMOCode, @UNNo, @Temperature, @TempratureType, 
		@Vent, @VentType, @CargoCategory, @PickupDate, @SealNo1, @SealNo2, @VGM, @StowageCell, @PUDOMode, @Remarks, @Commodity, @SpecialInstructions, @CreatedBy, @ModifiedBy 


	END
	ELSE
	BEGIN
	    Exec [Port].[usp_BookingContainerInsert] 
		@BranchID, @OrderNo, @ContainerKey, @ContainerNo, @Size, @Type, @ContainerGrade, @EFIndicator, @IMOCode, @UNNo, @Temperature, @TempratureType, 
		@Vent, @VentType, @CargoCategory, @PickupDate, @SealNo1, @SealNo2, @VGM, @StowageCell, @PUDOMode, @Remarks, @Commodity, @SpecialInstructions, @CreatedBy, @ModifiedBy 

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[BookingContainerSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_BookingContainerDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [BookingContainer] Record  based on [BookingContainer]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingContainerDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingContainerDelete] 
END 
GO
CREATE PROC [Port].[usp_BookingContainerDelete] 
    @BranchID bigint,
    @OrderNo nvarchar(50)
 
AS 

	
BEGIN

 
	DELETE
	FROM   [Port].[BookingContainer]
	WHERE  [BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo  
	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingContainerDelete]
-- ========================================================================================================================================

GO 
