 

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [BookingHeader] Record based on [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSelect] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderSelect] 
    @BranchID bigint,
    @OrderNo nvarchar(50)
AS 
 

BEGIN

	;with BookingTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TransportMode')
	SELECT	Hd.[BranchID], Hd.[OrderNo], Hd.[BookingNo], Hd.[BLNo], Hd.[OrderDate], Hd.[BookingType], Hd.[TransportType],Hd.[OrderType], 
			Hd.[CustomerCode], COALESCE(Hd.[CustomerName],Cust.MerchantName) As CustomerName, 
			Hd.[ForwarderCode], COALESCE(Hd.[ForwarderName],Fwd.MerchantName) As ForwarderName, 
			Hd.[ShippingAgent], COALESCE(Hd.[ShippingAgentName],Liner.MerchantName) As ShippingAgentName, Hd.[OwnerCode], 
			Hd.[LoadingPort], Hd.[DischargePort], Hd.[DestinationPort], Hd.[VesselScheduleID], Hd.[VesselID], 
			COALESCE(Hd.[VesselName],Vsl.VesselName) As VesselName, Hd.[VoyageNo], 
			Hd.[ShipCallNo], Hd.[Wharf], Hd.[IsAllowLateGate], Hd.[ETA], Hd.[ETD], Hd.[PortCutOffDry], Hd.[PortCutOffReefer], Hd.[FlightNo], Hd.[ARNNo], 
			Hd.[VehicleNo1], Hd.[VehicleNo2], Hd.[WagonNo], Hd.[JKNo],
			Hd.TotalQty,Hd.UOM,Hd.TotalVolume,Hd.TotalWeight,Hd.Commodity,Hd.MarksNos,Hd.SpecialInstructions,Hd.Remarks,
			Hd.[IsEDI], Hd.[IsCancel], Hd.[IsBillable], Hd.[IsPrinted], Hd.[EDIDateTime], 
			Hd.[CancelDateTime], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[RelationBranchID],
			btd.LookupDescription As BookingTypeDescription,
			ttd.LookupDescription As TransportTypeDescription,
			LP.PortName As LoadingPortName,
			DP.PortName As DischargePortName,
			DTP.PortName As DestinationPortName,
			Vsl.CallSignNo As CallSignNo
	FROM	[Port].[BookingHeader] Hd
	Left Outer Join BookingTypeDescription Btd On
		Hd.BookingType = btd.LookupID
	Left Outer Join TransportModeDescription ttd on 
		Hd.TransportType = ttd.LookupID
	Left Outer Join Master.Port LP ON 
		Hd.LoadingPort = Lp.PortCode
	Left Outer Join Master.Port DP ON 
		Hd.DischargePort = DP.PortCode 
	Left Outer Join Master.Port DTP ON 
		Hd.DischargePort = DTP.PortCode
	Left Outer Join Master.Merchant Cust ON 
		Hd.CustomerCode = Cust.MerchantCode 
	Left Outer Join Master.Merchant Fwd ON 
		Hd.ForwarderCode = Fwd.MerchantCode 
	Left Outer Join Master.Merchant Liner ON 
		Hd.ShippingAgent = Liner.MerchantCode
	Left Outer Join Master.Vessel Vsl On 
		Hd.VesselID = Vsl.VesselID
	WHERE	[BranchID] = @BranchID  
			AND [OrderNo] = @OrderNo  

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSelectByBLNo]
-- ========================================================================================================================================
-- Author:		Vijay
-- Create date: 	20-Feb-2017
-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingHeaderSelectByBLNo]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSelectByBLNo] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderSelectByBLNo] 
    @BranchID bigint,
    @BookingBLNo nvarchar(50)
AS 
 

BEGIN

	;with BookingTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TransportMode')
	SELECT	Hd.[BranchID], Hd.[OrderNo], Hd.[BookingNo], Hd.[BLNo], Hd.[OrderDate], Hd.[BookingType], Hd.[TransportType],Hd.[OrderType], 
			Hd.[CustomerCode], COALESCE(Hd.[CustomerName],Cust.MerchantName) As CustomerName, 
			Hd.[ForwarderCode], COALESCE(Hd.[ForwarderName],Fwd.MerchantName) As ForwarderName, 
			Hd.[ShippingAgent], COALESCE(Hd.[ShippingAgentName],Liner.MerchantName) As ShippingAgentName, Hd.[OwnerCode], 
			Hd.[LoadingPort], Hd.[DischargePort], Hd.[DestinationPort], Hd.[VesselScheduleID], Hd.[VesselID], 
			COALESCE(Hd.[VesselName],Vsl.VesselName) As VesselName, Hd.[VoyageNo], 
			Hd.[ShipCallNo], Hd.[Wharf], Hd.[IsAllowLateGate], Hd.[ETA], Hd.[ETD], Hd.[PortCutOffDry], Hd.[PortCutOffReefer], Hd.[FlightNo], Hd.[ARNNo], 
			Hd.[VehicleNo1], Hd.[VehicleNo2], Hd.[WagonNo], Hd.[JKNo],
			Hd.TotalQty,Hd.UOM,Hd.TotalVolume,Hd.TotalWeight,Hd.Commodity,Hd.MarksNos,Hd.SpecialInstructions,Hd.Remarks,
			Hd.[IsEDI], Hd.[IsCancel], Hd.[IsBillable], Hd.[IsPrinted], Hd.[EDIDateTime], 
			Hd.[CancelDateTime], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[RelationBranchID],
			btd.LookupDescription As BookingTypeDescription,
			ttd.LookupDescription As TransportTypeDescription,
			LP.PortName As LoadingPortName,
			DP.PortName As DischargePortName,
			DTP.PortName As DestinationPortName,
			Vsl.CallSignNo As CallSignNo
	FROM	[Port].[BookingHeader] Hd
	Left Outer Join BookingTypeDescription Btd On
		Hd.BookingType = btd.LookupID
	Left Outer Join TransportModeDescription ttd on 
		Hd.TransportType = ttd.LookupID
	Left Outer Join Master.Port LP ON 
		Hd.LoadingPort = Lp.PortCode
	Left Outer Join Master.Port DP ON 
		Hd.DischargePort = DP.PortCode 
	Left Outer Join Master.Port DTP ON 
		Hd.DischargePort = DTP.PortCode
	Left Outer Join Master.Merchant Cust ON 
		Hd.CustomerCode = Cust.MerchantCode 
	Left Outer Join Master.Merchant Fwd ON 
		Hd.ForwarderCode = Fwd.MerchantCode 
	Left Outer Join Master.Merchant Liner ON 
		Hd.ShippingAgent = Liner.MerchantCode
	Left Outer Join Master.Vessel Vsl On 
		Hd.VesselID = Vsl.VesselID
	WHERE	[BranchID] = @BranchID  
			AND [BLNo] like '%' + @BookingBLNo + '%' 

END
-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderSelectByBLNo]
-- ========================================================================================================================================

GO


IF OBJECT_ID('[Port].[usp_BookingHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderList] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderList] 
    @BranchID bigint

AS 
 
BEGIN
	;with BookingTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TransportMode')
	SELECT	Hd.[BranchID], Hd.[OrderNo], Hd.[BookingNo], Hd.[BLNo], Hd.[OrderDate], Hd.[BookingType], Hd.[TransportType],Hd.[OrderType], 
			Hd.[CustomerCode], COALESCE(Hd.[CustomerName],Cust.MerchantName) As CustomerName, 
			Hd.[ForwarderCode], COALESCE(Hd.[ForwarderName],Fwd.MerchantName) As ForwarderName, 
			Hd.[ShippingAgent], COALESCE(Hd.[ShippingAgentName],Liner.MerchantName) As ShippingAgentName, Hd.[OwnerCode], 
			Hd.[LoadingPort], Hd.[DischargePort], Hd.[DestinationPort], Hd.[VesselScheduleID], Hd.[VesselID], 
			COALESCE(Hd.[VesselName],Vsl.VesselName) As VesselName, Hd.[VoyageNo], 
			Hd.[ShipCallNo], Hd.[Wharf], Hd.[IsAllowLateGate], Hd.[ETA], Hd.[ETD], Hd.[PortCutOffDry], Hd.[PortCutOffReefer], Hd.[FlightNo], Hd.[ARNNo], 
			Hd.[VehicleNo1], Hd.[VehicleNo2], Hd.[WagonNo], Hd.[JKNo],
			Hd.TotalQty,Hd.UOM,Hd.TotalVolume,Hd.TotalWeight,Hd.Commodity,Hd.MarksNos,Hd.SpecialInstructions,Hd.Remarks,
			Hd.[IsEDI], Hd.[IsCancel], Hd.[IsBillable], Hd.[IsPrinted], Hd.[EDIDateTime], 
			Hd.[CancelDateTime], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[RelationBranchID],
			btd.LookupDescription As BookingTypeDescription,
			ttd.LookupDescription As TransportTypeDescription,
			LP.PortName As LoadingPortName,
			DP.PortName As DischargePortName,
			DTP.PortName As DestinationPortName,
			Vsl.CallSignNo As CallSignNo
	FROM	[Port].[BookingHeader] Hd
	Left Outer Join BookingTypeDescription Btd On
		Hd.BookingType = btd.LookupID
	Left Outer Join TransportModeDescription ttd on 
		Hd.TransportType = ttd.LookupID
	Left Outer Join Master.Port LP ON 
		Hd.LoadingPort = Lp.PortCode
	Left Outer Join Master.Port DP ON 
		Hd.DischargePort = DP.PortCode 
	Left Outer Join Master.Port DTP ON 
		Hd.DischargePort = DTP.PortCode
	Left Outer Join Master.Merchant Cust ON 
		Hd.CustomerCode = Cust.MerchantCode 
	Left Outer Join Master.Merchant Fwd ON 
		Hd.ForwarderCode = Fwd.MerchantCode 
	Left Outer Join Master.Merchant Liner ON 
		Hd.ShippingAgent = Liner.MerchantCode
	Left Outer Join Master.Vessel Vsl On 
		Hd.VesselID = Vsl.VesselID
 
		WHERE  Hd.[BranchID] = @BranchID  


END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [BookingHeader] PageView Records from [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderPageView]
END 
GO
CREATE PROC [Port].[usp_BookingHeaderPageView] 
    @BranchID bigint,
	@fetchrows bigint
AS 
BEGIN

	;with BookingTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TransportMode')
	SELECT	Hd.[BranchID], Hd.[OrderNo], Hd.[BookingNo], Hd.[BLNo], Hd.[OrderDate], Hd.[BookingType], Hd.[TransportType],Hd.[OrderType], 
			Hd.[CustomerCode], COALESCE(Hd.[CustomerName],Cust.MerchantName) As CustomerName, 
			Hd.[ForwarderCode], COALESCE(Hd.[ForwarderName],Fwd.MerchantName) As ForwarderName, 
			Hd.[ShippingAgent], COALESCE(Hd.[ShippingAgentName],Liner.MerchantName) As ShippingAgentName, Hd.[OwnerCode], 
			Hd.[LoadingPort], Hd.[DischargePort], Hd.[DestinationPort], Hd.[VesselScheduleID], Hd.[VesselID], 
			COALESCE(Hd.[VesselName],Vsl.VesselName) As VesselName, Hd.[VoyageNo], 
			Hd.[ShipCallNo], Hd.[Wharf], Hd.[IsAllowLateGate], Hd.[ETA], Hd.[ETD], Hd.[PortCutOffDry], Hd.[PortCutOffReefer], Hd.[FlightNo], Hd.[ARNNo], 
			Hd.[VehicleNo1], Hd.[VehicleNo2], Hd.[WagonNo], Hd.[JKNo],
			Hd.TotalQty,Hd.UOM,Hd.TotalVolume,Hd.TotalWeight,Hd.Commodity,Hd.MarksNos,Hd.SpecialInstructions,Hd.Remarks,
			Hd.[IsEDI], Hd.[IsCancel], Hd.[IsBillable], Hd.[IsPrinted], Hd.[EDIDateTime], 
			Hd.[CancelDateTime], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[RelationBranchID],
			btd.LookupDescription As BookingTypeDescription,
			ttd.LookupDescription As TransportTypeDescription,
			LP.PortName As LoadingPortName,
			DP.PortName As DischargePortName,
			DTP.PortName As DestinationPortName,
			Vsl.CallSignNo As CallSignNo
	FROM	[Port].[BookingHeader] Hd
	Left Outer Join BookingTypeDescription Btd On
		Hd.BookingType = btd.LookupID
	Left Outer Join TransportModeDescription ttd on 
		Hd.TransportType = ttd.LookupID
	Left Outer Join Master.Port LP ON 
		Hd.LoadingPort = Lp.PortCode
	Left Outer Join Master.Port DP ON 
		Hd.DischargePort = DP.PortCode 
	Left Outer Join Master.Port DTP ON 
		Hd.DischargePort = DTP.PortCode
	Left Outer Join Master.Merchant Cust ON 
		Hd.CustomerCode = Cust.MerchantCode 
	Left Outer Join Master.Merchant Fwd ON 
		Hd.ForwarderCode = Fwd.MerchantCode 
	Left Outer Join Master.Merchant Liner ON 
		Hd.ShippingAgent = Liner.MerchantCode
	Left Outer Join Master.Vessel Vsl On 
		Hd.VesselID = Vsl.VesselID
 
	WHERE  Hd.[BranchID] = @BranchID  
	ORDER BY Hd.[OrderNo] 
	        
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderRecordCount] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderRecordCount] 
    @BranchID bigint

AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Port].[BookingHeader]
		WHERE  [BranchID] = @BranchID  

	

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [BookingHeader] auto-complete search based on [BookingHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_BookingHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderAutoCompleteSearch]
END 
GO
CREATE PROC [Port].[usp_BookingHeaderAutoCompleteSearch]
    @BranchID bigint,
    @OrderNo nvarchar(50)
     
AS 

BEGIN
	;with BookingTypeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='JobType'),
	TransportModeDescription As (Select LookupID,LookupDescription From Config.Lookup Where LookupCategory='TransportMode')
	SELECT	Hd.[BranchID], Hd.[OrderNo], Hd.[BookingNo], Hd.[BLNo], Hd.[OrderDate], Hd.[BookingType], Hd.[TransportType],Hd.[OrderType], 
			Hd.[CustomerCode], COALESCE(Hd.[CustomerName],Cust.MerchantName) As CustomerName, 
			Hd.[ForwarderCode], COALESCE(Hd.[ForwarderName],Fwd.MerchantName) As ForwarderName, 
			Hd.[ShippingAgent], COALESCE(Hd.[ShippingAgentName],Liner.MerchantName) As ShippingAgentName, Hd.[OwnerCode], 
			Hd.[LoadingPort], Hd.[DischargePort], Hd.[DestinationPort], Hd.[VesselScheduleID], Hd.[VesselID], 
			COALESCE(Hd.[VesselName],Vsl.VesselName) As VesselName, Hd.[VoyageNo], 
			Hd.[ShipCallNo], Hd.[Wharf], Hd.[IsAllowLateGate], Hd.[ETA], Hd.[ETD], Hd.[PortCutOffDry], Hd.[PortCutOffReefer], Hd.[FlightNo], Hd.[ARNNo], 
			Hd.[VehicleNo1], Hd.[VehicleNo2], Hd.[WagonNo], Hd.[JKNo],
			Hd.TotalQty,Hd.UOM,Hd.TotalVolume,Hd.TotalWeight,Hd.Commodity,Hd.MarksNos,Hd.SpecialInstructions,Hd.Remarks,
			Hd.[IsEDI], Hd.[IsCancel], Hd.[IsBillable], Hd.[IsPrinted], Hd.[EDIDateTime], 
			Hd.[CancelDateTime], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn], Hd.[RelationBranchID],
			btd.LookupDescription As BookingTypeDescription,
			ttd.LookupDescription As TransportTypeDescription,
			LP.PortName As LoadingPortName,
			DP.PortName As DischargePortName,
			DTP.PortName As DestinationPortName,
			Vsl.CallSignNo As CallSignNo
	FROM	[Port].[BookingHeader] Hd
	Left Outer Join BookingTypeDescription Btd On
		Hd.BookingType = btd.LookupID
	Left Outer Join TransportModeDescription ttd on 
		Hd.TransportType = ttd.LookupID
	Left Outer Join Master.Port LP ON 
		Hd.LoadingPort = Lp.PortCode
	Left Outer Join Master.Port DP ON 
		Hd.DischargePort = DP.PortCode 
	Left Outer Join Master.Port DTP ON 
		Hd.DischargePort = DTP.PortCode
	Left Outer Join Master.Merchant Cust ON 
		Hd.CustomerCode = Cust.MerchantCode 
	Left Outer Join Master.Merchant Fwd ON 
		Hd.ForwarderCode = Fwd.MerchantCode 
	Left Outer Join Master.Merchant Liner ON 
		Hd.ShippingAgent = Liner.MerchantCode
	Left Outer Join Master.Vessel Vsl On 
		Hd.VesselID = Vsl.VesselID
	WHERE  [BranchID] = @BranchID  
			AND [OrderNo] LIKE '%' +  @OrderNo + '%' 
END
-- ========================================================================================================================================
-- END  											[Port].[usp_BookingHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [BookingHeader] Record Into [BookingHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderInsert] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderInsert] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @BookingNo nvarchar(50) = NULL,
    @BLNo nvarchar(50) = NULL,
    @OrderDate datetime = NULL,
    @BookingType smallint,
    @TransportType smallint,
	@OrderType nvarchar(15),
    @CustomerCode bigint,
    @CustomerName nvarchar(50) = NULL,
    @ForwarderCode bigint = NULL,
    @ForwarderName nvarchar(50) = NULL,
    @ShippingAgent bigint = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @OwnerCode bigint = NULL,
    @LoadingPort nvarchar(10) = NULL,
    @DischargePort nvarchar(10) = NULL,
    @DestinationPort nvarchar(10) = NULL,
    @VesselScheduleID int,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @Wharf bigint = NULL,
    @IsAllowLateGate bit = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PortCutOffDry datetime = NULL,
    @PortCutOffReefer datetime = NULL,
    @FlightNo nvarchar(35) = NULL,
    @ARNNo nvarchar(35) = NULL,
    @VehicleNo1 nvarchar(35) = NULL,
    @VehicleNo2 nvarchar(35) = NULL,
    @WagonNo nvarchar(35) = NULL,
    @JKNo nvarchar(35) = NULL,
	@TotalQty nvarchar(20) = NULL,
	@UOM nvarchar(20) = NULL,
	@TotalVolume nvarchar(20) = NULL,
	@TotalWeight nvarchar(20) = NULL,
	@Commodity nvarchar(200) = NULL,
	@MarksNos nvarchar(200) = NULL,
	@SpecialInstructions nvarchar(200)=NULL,
	@Remarks nvarchar(200) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @RelationBranchID bigint = NULL,
	@NewOrderNo nvarchar(50) OUTPUT
AS 
  

BEGIN
	
	INSERT INTO [Port].[BookingHeader] (
			[BranchID], [OrderNo], [BookingNo], [BLNo], [OrderDate], [BookingType], [TransportType],[OrderType], [CustomerCode], [CustomerName], 
			[ForwarderCode], [ForwarderName], [ShippingAgent], [ShippingAgentName], [OwnerCode], [LoadingPort], [DischargePort], [DestinationPort], 
			[VesselScheduleID], [VesselID], [VesselName], [VoyageNo], [ShipCallNo], [Wharf], [IsAllowLateGate], [ETA], [ETD], [PortCutOffDry], 
			[PortCutOffReefer], [FlightNo], [ARNNo], [VehicleNo1], [VehicleNo2], [WagonNo], [JKNo],
			[TotalQty],[UOM],[TotalVolume],[TotalWeight],[Commodity],[MarksNos],[SpecialInstructions],[Remarks],
			[CreatedBy], [CreatedOn],[RelationBranchID])
	SELECT	@BranchID, @OrderNo, @BookingNo, @BLNo, @OrderDate, @BookingType, @TransportType,@OrderType, @CustomerCode, @CustomerName, 
			@ForwarderCode, @ForwarderName, @ShippingAgent, @ShippingAgentName, @OwnerCode, @LoadingPort, @DischargePort, @DestinationPort, 
			@VesselScheduleID, @VesselID, @VesselName, @VoyageNo, @ShipCallNo, @Wharf, @IsAllowLateGate, @ETA, @ETD, @PortCutOffDry, 
			@PortCutOffReefer, @FlightNo, @ARNNo, @VehicleNo1, @VehicleNo2, @WagonNo, @JKNo, 
			@TotalQty,@UOM,@TotalVolume,@TotalWeight,@Commodity,@MarksNos,@SpecialInstructions,@Remarks,
			@CreatedBy, GETUTCDATE(), @RelationBranchID
	
      
	select @NewOrderNo = @OrderNo
	           
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [BookingHeader] Record Into [BookingHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderUpdate] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderUpdate] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @BookingNo nvarchar(50) = NULL,
    @BLNo nvarchar(50) = NULL,
    @OrderDate datetime = NULL,
    @BookingType smallint,
    @TransportType smallint,
	@OrderType nvarchar(15),
    @CustomerCode bigint,
    @CustomerName nvarchar(50) = NULL,
    @ForwarderCode bigint = NULL,
    @ForwarderName nvarchar(50) = NULL,
    @ShippingAgent bigint = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @OwnerCode bigint = NULL,
    @LoadingPort nvarchar(10) = NULL,
    @DischargePort nvarchar(10) = NULL,
    @DestinationPort nvarchar(10) = NULL,
    @VesselScheduleID int,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @Wharf bigint = NULL,
    @IsAllowLateGate bit = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PortCutOffDry datetime = NULL,
    @PortCutOffReefer datetime = NULL,
    @FlightNo nvarchar(35) = NULL,
    @ARNNo nvarchar(35) = NULL,
    @VehicleNo1 nvarchar(35) = NULL,
    @VehicleNo2 nvarchar(35) = NULL,
    @WagonNo nvarchar(35) = NULL,
    @JKNo nvarchar(35) = NULL,
	@TotalQty nvarchar(20) = NULL,
	@UOM nvarchar(20) = NULL,
	@TotalVolume nvarchar(20) = NULL,
	@TotalWeight nvarchar(20) = NULL,
	@Commodity nvarchar(200) = NULL,
	@MarksNos nvarchar(200) = NULL,
	@SpecialInstructions nvarchar(200)=NULL,
	@Remarks nvarchar(200) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @RelationBranchID bigint = NULL,
	@NewOrderNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Port].[BookingHeader]
	SET		[BranchID] = @BranchID, [OrderNo] = @OrderNo, [BookingNo] = @BookingNo, [BLNo] = @BLNo, [OrderDate] = @OrderDate, [BookingType] = @BookingType, 
			[TransportType] = @TransportType, [OrderType]=@OrderType, [CustomerCode] = @CustomerCode, [CustomerName] = @CustomerName, [ForwarderCode] = @ForwarderCode, 
			[ForwarderName] = @ForwarderName, [ShippingAgent] = @ShippingAgent, [ShippingAgentName] = @ShippingAgentName, [OwnerCode] = @OwnerCode, 
			[LoadingPort] = @LoadingPort, [DischargePort] = @DischargePort, [DestinationPort] = @DestinationPort, [VesselScheduleID] = @VesselScheduleID, 
			[VesselID] = @VesselID, [VesselName] = @VesselName, [VoyageNo] = @VoyageNo, [ShipCallNo] = @ShipCallNo, [Wharf] = @Wharf, [IsAllowLateGate] = @IsAllowLateGate, 
			[ETA] = @ETA, [ETD] = @ETD, [PortCutOffDry] = @PortCutOffDry, [PortCutOffReefer] = @PortCutOffReefer, [FlightNo] = @FlightNo, [ARNNo] = @ARNNo, 
			[VehicleNo1] = @VehicleNo1, [VehicleNo2] = @VehicleNo2, [WagonNo] = @WagonNo, [JKNo] = @JKNo, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE(), [RelationBranchID] = @RelationBranchID
			,TotalQty=@TotalQty,UOM=@UOM,TotalVolume=@TotalVolume,TotalWeight=@TotalWeight,
			Commodity=@Commodity,MarksNos=@MarksNos,SpecialInstructions=@SpecialInstructions,Remarks=@Remarks
	
	WHERE	[BranchID] = @BranchID
			AND [OrderNo] = @OrderNo
	
		select @NewOrderNo = @OrderNo

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [BookingHeader] Record Into [BookingHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderSave] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderSave] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
    @BookingNo nvarchar(50) = NULL,
    @BLNo nvarchar(50) = NULL,
    @OrderDate datetime = NULL,
    @BookingType smallint,
    @TransportType smallint,
	@OrderType nvarchar(15),
    @CustomerCode bigint,
    @CustomerName nvarchar(50) = NULL,
    @ForwarderCode bigint = NULL,
    @ForwarderName nvarchar(50) = NULL,
    @ShippingAgent bigint = NULL,
    @ShippingAgentName nvarchar(50) = NULL,
    @OwnerCode bigint = NULL,
    @LoadingPort nvarchar(10) = NULL,
    @DischargePort nvarchar(10) = NULL,
    @DestinationPort nvarchar(10) = NULL,
    @VesselScheduleID int,
    @VesselID nvarchar(20) = NULL,
    @VesselName nvarchar(100) = NULL,
    @VoyageNo nvarchar(20) = NULL,
    @ShipCallNo nvarchar(20) = NULL,
    @Wharf bigint = NULL,
    @IsAllowLateGate bit = NULL,
    @ETA datetime = NULL,
    @ETD datetime = NULL,
    @PortCutOffDry datetime = NULL,
    @PortCutOffReefer datetime = NULL,
    @FlightNo nvarchar(35) = NULL,
    @ARNNo nvarchar(35) = NULL,
    @VehicleNo1 nvarchar(35) = NULL,
    @VehicleNo2 nvarchar(35) = NULL,
    @WagonNo nvarchar(35) = NULL,
    @JKNo nvarchar(35) = NULL,
 	@TotalQty nvarchar(20) = NULL,
	@UOM nvarchar(20) = NULL,
	@TotalVolume nvarchar(20) = NULL,
	@TotalWeight nvarchar(20) = NULL,
	@Commodity nvarchar(200) = NULL,
	@MarksNos nvarchar(200) = NULL,
	@SpecialInstructions nvarchar(200)=NULL,
	@Remarks nvarchar(200) = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @RelationBranchID bigint = NULL,
	@NewOrderNo nvarchar(50) OUTPUT
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[BookingHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo)>0
	BEGIN
	    Exec [Port].[usp_BookingHeaderUpdate] 
		@BranchID, @OrderNo, @BookingNo, @BLNo, @OrderDate, @BookingType, @TransportType, @OrderType,
		@CustomerCode, @CustomerName, @ForwarderCode, @ForwarderName, @ShippingAgent, @ShippingAgentName, 
		@OwnerCode, @LoadingPort, @DischargePort, @DestinationPort, @VesselScheduleID, @VesselID, 
		@VesselName, @VoyageNo, @ShipCallNo, @Wharf, @IsAllowLateGate, @ETA, @ETD, @PortCutOffDry, 
		@PortCutOffReefer, @FlightNo, @ARNNo, @VehicleNo1, @VehicleNo2, @WagonNo, @JKNo, 
		@TotalQty,@UOM,@TotalVolume,@TotalWeight,@Commodity,@MarksNos,@SpecialInstructions,@Remarks,
		@CreatedBy,  @ModifiedBy,  @RelationBranchID,@NewOrderNo = @NewOrderNo OUTPUT

	END
	ELSE
	BEGIN


		Declare @Dt datetime,
				@BookingTypeDesc nvarchar(50),
				@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(),@BookingTypeDesc = LookupDescription,@OrderNo=''
		FROM	[Config].[Lookup] 
		WHERE	[LookupID]=@BookingType
		
	 
		
		IF @BookingTypeDesc='Import' 
			SET @DocID ='Port\Booking(Imp)'
		ELSE IF @BookingTypeDesc='Export' 
			SET @DocID ='Port\Booking(Exp)'
		 
		

		 
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Port\Booking(Exp)', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		--select @BranchID, @DocID, @Dt ,@CreatedBy
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @OrderNo OUTPUT


	    Exec [Port].[usp_BookingHeaderInsert] 
		@BranchID, @OrderNo, @BookingNo, @BLNo, @OrderDate, @BookingType, @TransportType, @OrderType,
		@CustomerCode, @CustomerName, @ForwarderCode, @ForwarderName, @ShippingAgent, @ShippingAgentName, 
		@OwnerCode, @LoadingPort, @DischargePort, @DestinationPort, @VesselScheduleID, @VesselID, 
		@VesselName, @VoyageNo, @ShipCallNo, @Wharf, @IsAllowLateGate, @ETA, @ETD, @PortCutOffDry, 
		@PortCutOffReefer, @FlightNo, @ARNNo, @VehicleNo1, @VehicleNo2, @WagonNo, @JKNo, 
		@TotalQty,@UOM,@TotalVolume,@TotalWeight,@Commodity,@MarksNos,@SpecialInstructions,@Remarks,
		@CreatedBy,  @ModifiedBy,  @RelationBranchID,@NewOrderNo = @NewOrderNo OUTPUT

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[BookingHeaderSave]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_BookingHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [BookingHeader] Record  based on [BookingHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_BookingHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_BookingHeaderDelete] 
END 
GO
CREATE PROC [Port].[usp_BookingHeaderDelete] 
    @BranchID bigint,
    @OrderNo nvarchar(50),
	@CancelledBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Port].[BookingHeader]
	SET	IsCancel = CAST(1 as bit),CancelDateTime = GETUTCDATE(), ModifiedBy = @CancelledBy
	WHERE 	[BranchID] = @BranchID
	       AND [OrderNo] = @OrderNo

	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_BookingHeaderDelete]
-- ========================================================================================================================================

GO
 