
-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [TruckMovementDetail] Record based on [TruckMovementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementDetailSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailSelect] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailSelect] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @ItemNo smallint
AS 
 

BEGIN

	;with TripTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[ItemNo], Dt.[OrderNo], Dt.[ContainerKey], Dt.[TripType], Dt.[MovementCode], 
			Dt.[Size], Dt.[Type],Dt.[DateIn], Dt.[DateOut], Dt.[Status],
			ISNULL(TR.LookupDescription,'') As TripTypeDescription
	FROM	[Port].[TruckMovementDetail] Dt 
	Left Outer Join TripTypeDescription TR ON 
		Dt.TripType = TR.LookupID
	WHERE  [BranchID] = @BranchID   
	       AND [TransactionNo] = @TransactionNo  
		   AND [ItemNo] = @ItemNo  


END
-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementDetailSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [TruckMovementDetail] Records from [TruckMovementDetail] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementDetailList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailList] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailList] 
    @BranchID bigint,
    @TransactionNo nvarchar(50)

AS 
 
BEGIN
	;with TripTypeDescription As (Select LookupId,LookupDescription From Config.Lookup Where LookupCategory='MovementIndicator')
	SELECT	Dt.[BranchID], Dt.[TransactionNo], Dt.[ItemNo], Dt.[OrderNo], Dt.[ContainerKey], Dt.[TripType], Dt.[MovementCode], 
			Dt.[Size], Dt.[Type],Dt.[DateIn], Dt.[DateOut], Dt.[Status],
			ISNULL(TR.LookupDescription,'') As TripTypeDescription
	FROM	[Port].[TruckMovementDetail] Dt 
	Left Outer Join TripTypeDescription TR ON 
		Dt.TripType = TR.LookupID
	WHERE  [BranchID] = @BranchID   
	       AND [TransactionNo] = @TransactionNo  

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementDetailList] 
-- ========================================================================================================================================

GO

 
-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [TruckMovementDetail] Record Into [TruckMovementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementDetailInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailInsert] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailInsert] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @TripType smallint = NULL,
    @MovementCode nvarchar(50) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @DateIn datetime = NULL,
    @DateOut datetime = NULL,
    @Status bit = NULL
AS 
  

BEGIN
	
	INSERT INTO [Port].[TruckMovementDetail] (
			[BranchID], [TransactionNo], [ItemNo], [OrderNo], [ContainerKey], [TripType], [MovementCode], 
			[Size], [Type], [DateIn], [DateOut], [Status])
	SELECT	@BranchID, @TransactionNo, @ItemNo, @OrderNo, @ContainerKey, @TripType, @MovementCode, 
			@Size, @Type, @DateIn, @DateOut, CAST(1 as bit)	
	
               
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementDetailInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [TruckMovementDetail] Record Into [TruckMovementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementDetailUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailUpdate] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailUpdate] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @TripType smallint = NULL,
    @MovementCode nvarchar(50) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @DateIn datetime = NULL,
    @DateOut datetime = NULL 
 AS 
 
	
BEGIN

	UPDATE	[Port].[TruckMovementDetail]
	SET		[OrderNo] = @OrderNo, [ContainerKey] = @ContainerKey, [TripType] = @TripType, [MovementCode] = @MovementCode, 
			[Size] = @Size, [Type] = @Type, [DateIn] = @DateIn, [DateOut] = @DateOut 
	WHERE	[BranchID] = @BranchID
			AND [TransactionNo] = @TransactionNo
			AND [ItemNo] = @ItemNo
	

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementDetailUpdate]
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [TruckMovementDetail] Record Into [TruckMovementDetail] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementDetailSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailSave] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailSave] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @ItemNo smallint,
    @OrderNo nvarchar(50) = NULL,
    @ContainerKey nvarchar(50) = NULL,
    @TripType smallint = NULL,
    @MovementCode nvarchar(50) = NULL,
    @Size nvarchar(10) = NULL,
    @Type nvarchar(10) = NULL,
    @DateIn datetime = NULL,
    @DateOut datetime = NULL 
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[TruckMovementDetail] 
		WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo
	       AND [ItemNo] = @ItemNo)>0
	BEGIN
	    Exec [Port].[usp_TruckMovementDetailUpdate] 
			@BranchID, @TransactionNo, @ItemNo, @OrderNo, @ContainerKey, @TripType, @MovementCode, 
			@Size, @Type, @DateIn, @DateOut 


	END
	ELSE
	BEGIN
	    Exec [Port].[usp_TruckMovementDetailInsert] 
			@BranchID, @TransactionNo, @ItemNo, @OrderNo, @ContainerKey, @TripType, @MovementCode, 
			@Size, @Type, @DateIn, @DateOut 


	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[TruckMovementDetailSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementDetailDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [TruckMovementDetail] Record  based on [TruckMovementDetail]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementDetailDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementDetailDelete] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementDetailDelete] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @ItemNo smallint
AS 

	
BEGIN

	UPDATE	[Port].[TruckMovementDetail]
	SET	[Status] = CAST(0 as bit)
	WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo
	       AND [ItemNo] = @ItemNo

	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementDetailDelete]
-- ========================================================================================================================================

GO 
