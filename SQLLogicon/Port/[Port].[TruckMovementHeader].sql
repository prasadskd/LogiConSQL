

-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderSelect]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select the [TruckMovementHeader] Record based on [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementHeaderSelect]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderSelect] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderSelect] 
    @BranchID bigint,
    @TransactionNo nvarchar(50)
AS 
 

BEGIN

	;with TruckCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.[HaulierCode], Hd.[TruckNo], Hd.[TruckCategory], Hd.[TransactionDate], 
			Hd.[Status], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn] ,
			ISNULL(Hl.MerchantName,'') As HaulierName,
			ISNULL(TC.LookupDescription,'') As TruckCategoryDescription
	FROM	[Port].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hl ON 
		Hd.HaulierCode = Hl.MerchantCode
	Left Outer Join TruckCategoryDescription TC ON 
		Hd.TruckCategory = TC.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[TransactionNo] = @TransactionNo  
END
-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderSelect]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderList]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Select all the [TruckMovementHeader] Records from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementHeaderList]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderList] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderList] 
	@BranchID bigint
AS 
 
BEGIN
	;with TruckCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.[HaulierCode], Hd.[TruckNo], Hd.[TruckCategory], Hd.[TransactionDate], 
			Hd.[Status], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn] ,
			ISNULL(Hl.MerchantName,'') As HaulierName,
			ISNULL(TC.LookupDescription,'') As TruckCategoryDescription
	FROM	[Port].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hl ON 
		Hd.HaulierCode = Hl.MerchantCode
	Left Outer Join TruckCategoryDescription TC ON 
		Hd.TruckCategory = TC.LookupID
	WHERE	Hd.[BranchID] = @BranchID  

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderList] 
-- ========================================================================================================================================

GO



-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderPageView]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select all the [TruckMovementHeader] PageView Records from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementHeaderPageView]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderPageView]
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderPageView] 
	@BranchID bigint,
	@fetchrows bigint
AS 
BEGIN

	;with TruckCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.[HaulierCode], Hd.[TruckNo], Hd.[TruckCategory], Hd.[TransactionDate], 
			Hd.[Status], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn] ,
			ISNULL(Hl.MerchantName,'') As HaulierName,
			ISNULL(TC.LookupDescription,'') As TruckCategoryDescription
	FROM	[Port].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hl ON 
		Hd.HaulierCode = Hl.MerchantCode
	Left Outer Join TruckCategoryDescription TC ON 
		Hd.TruckCategory = TC.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
	ORDER BY  Hd.[TransactionNo] 
	OFFSET @fetchrows ROWS 
	FETCH NEXT 10 ROWS ONLY

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderPageView]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderRecordCount]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select Record Count from [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementHeaderRecordCount]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderRecordCount] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderRecordCount] 
	@BranchID bigint
AS 
BEGIN

	SELECT COUNT(0) 
	FROM    [Port].[TruckMovementHeader]
	Where BranchID = @BranchID

END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderRecordCount]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderAutoCompleteSearch]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-Jun-2016
-- Description:	Select the [TruckMovementHeader] auto-complete search based on [TruckMovementHeader] table
-- ========================================================================================================================================


IF OBJECT_ID('[Port].[usp_TruckMovementHeaderAutoCompleteSearch]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderAutoCompleteSearch]
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderAutoCompleteSearch]
    @BranchID bigint,
    @TransactionNo nvarchar(50)
     
AS 

BEGIN
		;with TruckCategoryDescription As (Select LookupID,LookupDescription From Config.Lookup where LookupCategory='TruckCategory')
	SELECT	Hd.[BranchID], Hd.[TransactionNo], Hd.[HaulierCode], Hd.[TruckNo], Hd.[TruckCategory], Hd.[TransactionDate], 
			Hd.[Status], Hd.[CreatedBy], Hd.[CreatedOn], Hd.[ModifiedBy], Hd.[ModifiedOn] ,
			ISNULL(Hl.MerchantName,'') As HaulierName,
			ISNULL(TC.LookupDescription,'') As TruckCategoryDescription
	FROM	[Port].[TruckMovementHeader] Hd
	Left Outer Join Master.Merchant Hl ON 
		Hd.HaulierCode = Hl.MerchantCode
	Left Outer Join TruckCategoryDescription TC ON 
		Hd.TruckCategory = TC.LookupID
	WHERE	Hd.[BranchID] = @BranchID  
			AND Hd.[TransactionNo] LIKE '%' +  @TransactionNo + '%' 
END
-- ========================================================================================================================================
-- END  											[Port].[usp_TruckMovementHeaderAutoCompleteSearch]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderInsert]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Inserts the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementHeaderInsert]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderInsert] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderInsert] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @HaulierCode bigint = NULL,
    @TruckNo nvarchar(20) = NULL,
    @TruckCategory smallint = NULL,
    @TransactionDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewTransactionNo nvarchar(50) OUTPUT
AS 
  

BEGIN
	
	INSERT INTO [Port].[TruckMovementHeader] (
			[BranchID], [TransactionNo], [HaulierCode], [TruckNo], [TruckCategory], [TransactionDate], [Status], [CreatedBy], [CreatedOn])
	SELECT	@BranchID, @TransactionNo, @HaulierCode, @TruckNo, @TruckCategory, @TransactionDate, CAST(1 as bit), @CreatedBy, GETUTCDATE()
	
       
	Select @NewTransactionNo = @TransactionNo        
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderInsert]
-- ========================================================================================================================================

GO

-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderUpdate]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	updates the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementHeaderUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderUpdate] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderUpdate] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @HaulierCode bigint = NULL,
    @TruckNo nvarchar(20) = NULL,
    @TruckCategory smallint = NULL,
    @TransactionDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewTransactionNo nvarchar(50) OUTPUT
AS 
 
	
BEGIN

	UPDATE	[Port].[TruckMovementHeader]
	SET		[HaulierCode] = @HaulierCode, [TruckNo] = @TruckNo, [TruckCategory] = @TruckCategory, 
			[TransactionDate] = @TransactionDate, [ModifiedBy] = @ModifiedBy, [ModifiedOn] = GETUTCDATE()
	WHERE	[BranchID] = @BranchID
			AND [TransactionNo] = @TransactionNo
	
	Select @NewTransactionNo = @TransactionNo
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderUpdate]
-- ========================================================================================================================================

GO


-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderSave]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Either INSERT or UPDATE the [TruckMovementHeader] Record Into [TruckMovementHeader] Table.

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementHeaderSave]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderSave] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderSave] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
    @HaulierCode bigint = NULL,
    @TruckNo nvarchar(20) = NULL,
    @TruckCategory smallint = NULL,
    @TransactionDate datetime = NULL,
    @CreatedBy nvarchar(60) = NULL,
    @ModifiedBy nvarchar(60) = NULL,
    @NewTransactionNo nvarchar(50) OUTPUT
AS 
 

BEGIN

	IF (SELECT COUNT(0) FROM [Port].[TruckMovementHeader] 
		WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo)>0
	BEGIN
	    Exec [Port].[usp_TruckMovementHeaderUpdate] 
			@BranchID, @TransactionNo, @HaulierCode, @TruckNo, @TruckCategory, @TransactionDate, 
			@CreatedBy, @ModifiedBy,@NewTransactionNo = @NewTransactionNo OUTPUT


	END
	ELSE
	BEGIN

	Declare @Dt datetime,
			@DocID nvarchar(50)
		
		SELECT @Dt = GETDATE(), @TransactionNo='',@DocID ='Port\Truck'
		
		/*
		declare @ord varchar(50)
		declare @dt datetime
		set @dt =GETDATE()
		Exec [Utility].[usp_GenerateDocumentNumber] 1000001, 'Port\Truck', @dt ,'system', @ord   OUTPUT
		print @ord
		*/
		
		--select @BranchID, @DocID, @Dt ,@CreatedBy
		
		Exec [Utility].[usp_GenerateDocumentNumber] @BranchID, @DocID, @Dt ,@CreatedBy, @TransactionNo OUTPUT


		

	    Exec [Port].[usp_TruckMovementHeaderInsert] 
			@BranchID, @TransactionNo, @HaulierCode, @TruckNo, @TruckCategory, @TransactionDate, 
			@CreatedBy, @ModifiedBy,@NewTransactionNo = @NewTransactionNo OUTPUT

	END
	

END

	

-- ========================================================================================================================================
-- END  											 [Port].usp_[TruckMovementHeaderSave]
-- ========================================================================================================================================

GO




-- ========================================================================================================================================
-- START											 [Port].[usp_TruckMovementHeaderDelete]
-- ========================================================================================================================================
-- Author:		Sharma
-- Create date: 	01-May-2012
-- Description:	Deletes the [TruckMovementHeader] Record  based on [TruckMovementHeader]

-- ========================================================================================================================================

IF OBJECT_ID('[Port].[usp_TruckMovementHeaderDelete]') IS NOT NULL
BEGIN 
    DROP PROC [Port].[usp_TruckMovementHeaderDelete] 
END 
GO
CREATE PROC [Port].[usp_TruckMovementHeaderDelete] 
    @BranchID bigint,
    @TransactionNo nvarchar(50),
	@ModifiedBy nvarchar(60)
AS 

	
BEGIN

	UPDATE	[Port].[TruckMovementHeader]
	SET	[Status] = CAST(0 as bit),ModifiedBy = @ModifiedBy , ModifiedOn = GETUTCDATE()
	WHERE 	[BranchID] = @BranchID
	       AND [TransactionNo] = @TransactionNo

	 
END

-- ========================================================================================================================================
-- END  											 [Port].[usp_TruckMovementHeaderDelete]
-- ========================================================================================================================================

GO 
